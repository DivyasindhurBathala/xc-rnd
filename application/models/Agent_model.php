<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Agent_model extends CI_Model
{
    public function getAgent($id)
    {
        $this->db = $this->load->database('default', true);
        $q= $this->db->query("select b.*, (select  ROUND(sum(xx.amount), 2) from a_topups xx left join a_clients vv on vv.id=xx.userid where vv.agentid=b.id) as amount  from a_clients_agents b where b.companyid = ? and b.id=?", array($this->session->cid, $id));
        if ($q->num_rows()>0) {
            return $q->row();
        } else {
            return false;
        }
    }

    public function addAgent($data)
    {
        if($data['reseller_type'] == "Prepaid"){
            $data['comission_value'] = $data['discount'];
            unset($data['discount']);
        }else{
            unset($data['discount']);
        }
        /*
        $data['comission_value'] = str_replace(',','.',  $data['comission_value']);
        $data['min_topup'] = str_replace(',','.',  $data['min_topup']);
        $data['max_topup'] = str_replace(',','.',  $data['max_topup']);
        $data['reseller_balance'] = str_replace(',','.',  $data['reseller_balance']);
        */
        $this->db = $this->load->database('default', true);
        $this->db->insert('a_clients_agents', $data);
        return $this->db->insert_id();
    }
    public function insert_paymentlog($data)
    {
        $this->db = $this->load->database('default', true);
        $this->db->insert("a_client_agents_payments", $data);
        return $this->db->insert_id();
    }
    public function addPermission($type, $data)
    {
        $this->db = $this->load->database('default', true);
        if ($type == "Prepaid") {
            $data['reload'] = 1;
            $data['addbundle'] = 0;
            $data['addorder'] = 0;
            $data['editclient'] = 0;
            $data['addclient'] = 0;
            $data['service'] = 0;
            $data['helpdesk'] = 1;
        }
        $this->db->insert('a_clients_agents_perms', $data);
    }


    public function getPermission($id)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select reload, addbundle,addorder,editclient,addclient,service,helpdesk from a_clients_agents_perms where agentid=? ", array($id));
        return $q->row();
    }
    public function updateAgent($data)
    {

        /*
        $data['comission_value'] = str_replace(',','.',  $data['comission_value']);
        $data['min_topup'] = str_replace(',','.',  $data['min_topup']);
        $data['max_topup'] = str_replace(',','.',  $data['max_topup']);
        */
        $this->db = $this->load->database('default', true);
        $this->db->where('companyid', $this->session->cid);
        $this->db->where('id', $data['id']);
        $this->db->update('a_clients_agents', $data);
        log_message('error',$this->db->last_query());
        return $this->db->affected_rows();
    }
    public function deleteAgent($data)
    {
        $this->db = $this->load->database('default', true);
        $agent = $this->db->query("select * from a_clients_agents where companyid=? and id=? and isdefault =? ", aray($this->session->cid, $data['id'], 0));
        if (!$agent) {
            return array('result'=> 'error', 'message' => 'This agent can not be removed as this is default agent');
        }
        if ($data['newagent'] == "0") {
            $this->removeAgentOwner($data['id']);
        } else {
            $this->UpdateAgentOwner($data['id'], $data['newagent']);
        }
        $this->db->where('companyid', $this->session->cid);
        $this->db->where('id', $data['id']);
        $this->db->delete('a_clients_agents');
        return array('result'=> 'success');
    }

    public function UpdateAgentOwner($old, $new)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('companyid', $this->session->cid);
        $this->db->where('agentid', $old);
        $this->db->update('a_clients', array('agentid' => $new));
        return $this->db->affected_rows();
    }

    public function removeAgentOwner($id)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('companyid', $this->session->cid);
        $this->db->where('agentid', $id);
        $this->db->update('a_clients', array('agentid' => null));
        return $this->db->affected_rows();
    }
}
