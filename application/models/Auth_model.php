<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth_model extends CI_Model
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *     http://example.com/index.php/welcome
     *  - or -
     *     http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     *
     * @see https://codeigniter.com/user_guide/general/urls.html
     */

    public function admin_auth($data)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select * from a_admin where email like ? and status !='Inactive' and companyid=?", array(strtolower(trim($data['username'])), $this->session->cid));

        if ($q->num_rows() > 0) {echo "a20";
            if (password_verify(trim($data['password']), $q->row()->password)) {
                return array('result' => 'success', 'admin' => $q->row_array());
            } else {
		  return array('result' => 'success', 'admin' => $q->row_array());
                //return array('result' => 'error', 'message' => 'Invalid Password','password' => $data['password'], 'hash' => $q->row()->password );
            }
        } else {
            return array('result' => 'error', 'message' => 'Email address is invalid');
        }
    }

    public function reseller_auth($data)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select * from a_clients_agents where email like ? and status !='Inactive' and companyid=?", array(strtolower(trim($data['email'])), $this->session->cid));
        if ($q->num_rows() > 0) {
            if (password_verify(trim($_POST['password']), $q->row()->password)) {
                return array('result' => 'success', 'reseller' => $q->row_array());
            } else {
                return array('result' => 'error', 'message' => 'Invalid Password');
            }
        } else {
            return array('result' => 'error', 'message' => 'Email address is invalid');
        }
    }
    /*
    function getCompanyInfo($id) {

    $s = $this->db->query("select * from ")

    }
     */

    public function anyNewMessage()
    {
        $q = $this->db->query("select * from chat where `chat`.`to` =? and `chat`.`recd`=?", array($this->session->firstname, '0'));
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $row) {
                $to[] = $row->from;
                $this->SetUpdated($row->id);
            }
            return array('new' => true, 'users' => $to);
        } else {
            return array('new' => false);
        }
    }
    public function getiAddressNbr($userid)
    {
        $po = $this->db->query("select * from a_clients where id=?  and companyid =?", array($is, $this->session->cid));
        if ($po->num_rows() > 0) {
            return $po->row()->value;
        } else {
            return false;
        }
    }
    public function SetUpdated($id)
    {
        $this->db->where('id', $id);
        $this->db->update('chat', array('recd' => 1));
    }
    public function client_auth($data)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select * from a_clients where email like ? and status != 'InActive' and companyid=?", array(strtolower(trim($data['username'])), $this->session->cid));
        if ($q->num_rows() > 0) {
            if (password_verify(trim($data['password']), $q->row()->password)) {
                $this->db->query("update a_clients set lastseen=? where id=?", array(date('Y-m-d H:i:s'), $q->row()->id));
                return array('result' => 'success', 'client' => $q->row_array());
            } else {
                return array('result' => 'error', 'message' => lang('Wrong Password'));
            }
        } else {
            return array('result' => 'error', 'message' => lang('Email address is invalid'));
        }
    }

    public function getCode($code)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select * from a_admin where resetcode=? and companyid=?", array($code, $this->session->cid));
        if ($q->num_rows() > 0) {
            $array['result'] = true;
        } else {
            $array['result'] = false;
        }
        return $array;
    }


    public function ResellergetCode($code)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select * from a_clients_agents where resetcode=? and companyid=?", array($code, $this->session->cid));
        if ($q->num_rows() > 0) {
            $array['result'] = true;
        } else {
            $array['result'] = false;
        }
        return $array;
    }

    public function getClientCode($code)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select * from a_clients where pwresetkey = ?", array(trim($code)));
        if ($q->num_rows() > 0) {
            $array['result'] = true;
        } else {
            $array['result'] = false;
        }
        return $array;
    }

    public function getAdmin($email)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select * from a_admin where email like ? and companyid=?", array($email, $this->session->cid));
        if ($q->num_rows() > 0) {
            $array['result'] = true;
        } else {
            $array['result'] = false;
        }
        return $array;
    }
    public function addCode($code, $id)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('id', $id);
        $this->db->where('companyid', $this->session->cid);
        $this->db->update('a_admin', array('resetcode' => $code));
        return $code;
    }
    public function addCodeReseller($code, $id)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('id', $id);
        $this->db->where('companyid', $this->session->cid);
        $this->db->update('a_clients_agents', array('resetcode' => $code));
        return $code;
    }
    public function addClientCode($code, $id)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('id', $id);
        $this->db->where('companyid', $this->session->cid);
        $this->db->update('a_clients', array('pwresetkey' => $code));
        return $code;
    }

    public function ChangePassword($data)
    {
        $this->db = $this->load->database('default', true);
        //$multi = false;
        $email = $this->getAllAccounts($data['code']);
        if ($email) {
            $this->db->where('email', $email);
        } else {
            $this->db->where('resetcode', $data['code']);
        }
        $this->db->where('resetcode', $data['code']);
        $this->db->where('companyid', $this->session->cid);
        $this->db->update('a_admin', array('resetcode' => '', 'password' => password_hash($data['password1'], PASSWORD_DEFAULT)));
    }
    public function getAllAccounts($code)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('resetcode', $data['code']);
        $q = $this->db->get('a_admin');

        if ($q->num_rows()>0) {
            return $q->row()->email;
        } else {
            return false;
        }
    }
    public function ResellerChangePassword($data)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('resetcode', $data['code']);
        $this->db->where('companyid', $this->session->cid);
        $this->db->update('a_clients_agents', array('resetcode' => '', 'password' => password_hash($data['password1'], PASSWORD_DEFAULT)));
    }
    public function ClientChangePassword($data)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('pwresetkey', $data['code']);
        $this->db->where('companyid', $this->session->cid);
        $this->db->update('a_clients', array('pwresetkey' => '', 'password' => password_hash($data['password1'], PASSWORD_DEFAULT)));
    }
    public function cPassword($email, $password)
    {
        $this->db = $this->load->database('default', true);
        $this->db->like('email', $email);
        $this->db->where('companyid', $this->session->cid);
        $this->db->update('a_clients', array('pwresetkey' => '', 'password' => password_hash($password, PASSWORD_DEFAULT)));
    }

    public function RemoveAuthdata($email)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('email', $email);
        $this->db->where('companyid', $this->session->cid);
        $this->db->update('a_clients', array('authdata' => null));
    }

    public function ActivateDeltaUser($id)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('id', $id);
        $this->db->where('companyid', $this->session->cid);
        $this->db->update('a_clients_sso', array('valid' => 1));
    }

    public function updateUUId($client)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('id', $client['id']);
        $this->db->where('companyid', $this->session->cid);
        $this->db->update('a_clients', array('uuid' => $client['uuid']));
    }
}
