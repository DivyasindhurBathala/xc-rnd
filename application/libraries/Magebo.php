<?php
/**
 * Artilium Library for Soap Interfacses
 */
class Magebo
{
    private $CI;
    private $vars;
    private $setting;
    private $mvno_setting;
    private $companyid;
    public function __construct($vars)
    {
        $this->CI =& get_instance();
        $this->db  = $this->CI->load->database('magebo', true);
        $this->db1 = $this->CI->load->database('default', true);
        //$this->dbv5 = $this->load->database('magebov5', true);
        $this->CI->load->helper('globalvars');
        $this->CI->load->model('Admin_model');
        $this->companyid    = $vars['companyid'];
        $this->error        = array(
            'result' => 'error'
        );
        $this->success      = array(
            'result' => 'success'
        );
        $this->setting      = getCompanySetting($vars['companyid']);
        $this->mvno_setting = globofix($vars['companyid']);
    }
    public function set_response($data, $type = false)
    {
        if ($type) {
            return (object) array_merge($this->success, $data);
        } else {
            return (object) array_merge($this->error, array(
                'message' => $data
            ));
        }
    }
    /*public function getInvoicesByMonth($cid){
    $this->db = $this->load->database('default', true);
    $q = $this->db->query("SELECT a.dInvoiceDate, COUNT(a.iInvoiceNbr) as total FROM tblInvoice a Left join tblAddress b on b.iAddressNbr=a.iAddressNbr where a.dInvoiceDate > '2018-09-01' and b.iCompanyNbr=? GROUP BY a.dInvoiceDate order by a.dInvoiceDate asc", array($cid));
    if($q->num_rows()>0){

    foreach($q->result_array() as $row){


    $row['DateGroup'] = substr($row['dInvoiceDate'], 0, 7);

    $result[] = $row;


    }


    foreach($result as $r){

    $qd[$r['date']] =  ++$r['total'];

    }

    $html[] ="['Month', 'Invoices']";
    foreach($qd as $ke$row){

    $html[] = "['".$row['date']."', ".$row['total']."]";

    }

    return $html;



    }else{
    return array();
    }
    //return $q->result_array();
    }
    */
    public function getAvarageInvoice()
    {
        $q = $this->db->query("SELECT AVG(a.mInvoiceAmount) as avg ,count(*) as total FROM tblInvoice a left join tblAddress b on b.iAddressNbr=a.iAddressNbr WHERE b.iCompanyNbr=?", array(
            $this->companyid
        ));
        if ($q->num_rows() > 0) {
            return $q->row();
        } else {
            return (object) array(
                'avg' => '0',
                'total' => '0'
            );
        }
    }
    public function isInvoicePaid($iInvoiceNbr)
    {
        $q = $this->db->query("select * from tblInvoice where iInvoiceNbr=? and iInvoiceStatus = ?", array(
            $iInvoiceNbr,
            54
        ));
        if ($q->num_rows() > 0) {
            return $q->row();
        } else {
            return false;
        }
    }
    public function isInvoiceUnPaid($iInvoiceNbr)
    {
        $q = $this->db->query("select * from tblInvoice where iInvoiceNbr=? and iInvoiceStatus = ?", array(
            $iInvoiceNbr,
            52
        ));
        if ($q->num_rows() > 0) {
            return $q->row();
        } else {
            return false;
        }
    }
    public function getInvoiceBalance($iInvoiceNbr, $iAddressNbr)
    {
        $q = $this->db->query("select sum(mAssignedAmount) as amount from tblPaymentAssignment where iInvoiceNbr=? and iAddressNbr=?", array(
            $iInvoiceNbr,
            $iAddressNbr
        ));
        if ($q->num_rows() > 0) {
            return $q->row()->amount;
        } else {
            return 0;
        }
    }
    public function getInvoicesbyDate($date)
    {
        $q = $this->db->query("select a.iAddressNbr,a.mInvoiceAmount from tblInvoice a left join tblAddress b on b.iAddressNbr=a.iAddressNbr where a.dInvoiceDate = ? and b.iCompanyNbr=?", array(
            $date,
            $this->companyid
        ));
        if ($q->num_rows() > 0) {
            return $q->result_array();
        } else {
            return array();
        }
    }

    public function getLastMonthInvoicesbyDate($month)
    {
        $lastdate =  date('Y-m-d', strtotime('last day of previous month'));
        $firstdate = date('Y-m-d', strtotime('first day of previous month'));
        $q = $this->db->query("select a.iAddressNbr,a.mInvoiceAmount from tblInvoice a left join tblAddress b on b.iAddressNbr=a.iAddressNbr where a.dInvoiceDate >= ? and a.dInvoiceDate <= ?  and b.iCompanyNbr=?", array(
            $firstdate,
            $lastdate,
            $this->companyid
        ));
        if ($q->num_rows() > 0) {
            return $q->result_array();
        } else {
            return array();
        }
    }
    public function addSimBlocking($simcard)
    {
        $this->db->query("UPDATE tblC_SimCard SET bBarPartial=0,bBarFull=1,bActive=0 WHERE cSIMCardNbr=?", array(
            $simcard
        ));
        $this->db->query("EXEC spInsertSIMCardLog '$simcard',602,NULL,NULL,NULL,NULL,'FullyBlocked',NULL");
        return (object) array(
            'result' => 'success'
        );
    }
    public function getMandateId($id)
    {
        $res = array(
            'SEPA_MANDATE' => '',
            'IBAN' => '',
            'BIC' => '',
            'SEPA_MANDATE_SIGNATURE_DATE' => '',
            'SEPA_STATUS' => ''
        );
        $q   = $this->db->query("SELECT a.iAddressDataIndex, a.iTypeNbr, b.cTypeDescription, a.cAddressData FROM tblAddressData a left join tblType b on b.iTypeNbr=a.iTypeNbr where  a.bPreferredOrActive=1 and a.iAddressNbr=? and b.iTypeGroupNbr=?", array(
            $id,
            5
        ));
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $row) {
                if (!empty($row)) {
                    $res[str_replace(' ', '_', $row->cTypeDescription)] = $row->cAddressData;
                }
            }
            return (object) $res;
        } else {
            return (object) array(
                'SEPA_MANDATE' => '',
                'IBAN' => '',
                'BIC' => '',
                'SEPA_MANDATE_SIGNATURE_DATE' => '',
                'SEPA_STATUS' => ''
            );
        }
    }
    public function GetFinancial($msisdn)
    {
        $q = $this->db->query("SELECT C.cCompany Company,dbo.fnGetFinancialCondition(A.iAddressNbr) FCondition,AD4.cAddressData EmailAddress,AD3.cAddressData VATNumber,AD1.cAddressData TelephoneNumber,AD2.cAddressData GsmNumber,P.iPincode,P.iLocationIndex,L.iAddressNbr,A.cName,A.cStreet,A.iZipCode,A.cCity\
                FROM tblC_Pincode P LEFT JOIN tblLocation L ON L.iLocationIndex=P.iLocationIndex LEFT JOIN tblAddress A ON A.iAddressNbr=L.iAddressNbr \
                LEFT JOIN tblAddressData AD1 ON AD1.iTypeNbr=18 AND AD1.iAddressNbr=A.iAddressNbr LEFT JOIN tblAddressData AD2 ON AD2.iTypeNbr=20 AND AD2.iAddressNbr=A.iAddressNbr \
                LEFT JOIN tblAddressData AD3 ON AD3.iTypeNbr=23 AND AD3.iAddressNbr=A.iAddressNbr LEFT JOIN tblAddressData AD4 ON AD4.iTypeNbr=34 AND AD4.iAddressNbr=A.iAddressNbr LEFT JOIN tblCompany C On C.iCompanyNbr=A.iCompanyNbr where iPincode=?", array(
            $msisdn
        ));
        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return false;
        }
    }
    public function viewPayments($iAddressNbr)
    {
        $query = "execute spViewPayment " . $iAddressNbr . ", 2, 0";
        $q     = $this->db->query($query);
        return $q->result_array();
    }
    public function getPayments($iAddressNbr)
    {
        $query = "execute spViewPayment " . $iAddressNbr . ", 2, 0";
        $q     = $this->db->query($query);
        $res   = array();
        if ($q->result_array()) {
            foreach ($q->result_array() as $row) {
                if ($row['Assigned'] == 1) {
                    $row['Assigned'] = "Assigned";
                } else {
                    $row['Assigned'] = "Open";
                }
                if ($row['Paymentform'] == "SEPA REOCCURRING") {
                    $row['Paymentform'] = "SR";
                } elseif ($row['Paymentform'] == "SEPA FIRST") {
                    $row['Paymentform'] = "SF";
                } elseif ($row['Paymentform'] == "BANK TRANSACTION") {
                    $row['Paymentform'] = "BT";
                }
            }
            $res[] = $row;
            unset($row);
        }
        return $res;
    }
    public function GetPaymentInfo($paymentid)
    {
        $q                      = $this->db->query('select * from tblPayment where iPaymentNbr=?', array(
            $paymentid
        ));
        $s                      = $this->db->query("select P.mPaymentAmount from tblPayment P left join ( select iPaymentNbr, sum(mAssignedAmount) as TotalUsed from tblPaymentAssignment  where iPaymentNbr = ? group by iPaymentNbr ) A ON A.iPaymentNbr = P.iPaymentNbr where P.iPaymentNbr = ?", array(
            $paymentid,
            $paymentid
        ));
        $t                      = $this->db->query("select A.TotalUsed from tblPayment P left join ( select iPaymentNbr, sum(mAssignedAmount) as TotalUsed from tblPaymentAssignment where iPaymentNbr = ? group by iPaymentNbr ) A ON A.iPaymentNbr = P.iPaymentNbr where P.iPaymentNbr = ?", array(
            $paymentid,
            $paymentid
        ));
        $u                      = $this->db->query("select P.mPaymentAmount-A.TotalUsed as AmountLefted
        from tblPayment P left join ( select iPaymentNbr, sum(mAssignedAmount) as TotalUsed from tblPaymentAssignment where iPaymentNbr = ?  group by iPaymentNbr ) A ON A.iPaymentNbr = P.iPaymentNbr where P.iPaymentNbr = ?", array(
            $paymentid,
            $paymentid
        ));
        $PaymentDate            = $q->row()->dPaymentDate;
        $iAddressNbr            = $q->row()->iAddressNbr;
        $PaymentFormDescription = $q->row()->cPaymentFormDescription;
        $PaymentAmount          = $s->row()->mPaymentAmount;
        $Totalused              = $t->row()->TotalUsed;
        $AmountLeft             = $u->row()->AmountLefted;
        $PaymentRemark          = 'Split Amount : ' . str_replace('.', ',', $PaymentAmount);
        if ($PaymentAmount > $Totalused) {
            $this->db->query("update tblPayment set mPaymentAmount = ?, bTotalAssigned = 1, cPaymentRemark = ? where iPaymentNbr = ?", array(
                $Totalused,
                $PaymentRemark,
                $paymentid
            ));
        }
        log_message('error', 'payment refund :'. print_r(array(
            'PaymentDate' => $PaymentDate,
            'iAddressNbr' => $iAddressNbr,
            'PaymentFormDescription' => $PaymentFormDescription,
            'PaymentAmount' => $PaymentAmount,
            'Totalused' => $Totalused,
            'AmountLeft' => $AmountLeft,
            'PaymentRemark' => $PaymentRemark
        ), true));
        return array(
            'PaymentDate' => $PaymentDate,
            'iAddressNbr' => $iAddressNbr,
            'PaymentFormDescription' => $PaymentFormDescription,
            'PaymentAmount' => $PaymentAmount,
            'Totalused' => $Totalused,
            'AmountLeft' => $AmountLeft,
            'PaymentRemark' => $PaymentRemark
        );
    }
    public function updateRefundExec($amount, $iPaymentNbr1, $iPaymentNbr2)
    {
        $q1 = "Update tblPayment set cPaymentRemark = replace(cPaymentRemark,'Payed with paymentnumber', 'Payed w nbr') + ' - Split Amount : '+replace(cast(" . $amount . " as varchar),'.',',') where iPaymentNbr = " . $iPaymentNbr1;
        $this->db->query($q1);
        $q2 = "Update tblPayment set cPaymentRemark = replace(cPaymentRemark,'Payed with paymentnumber', 'Payed w nbr') + ' - Split Amount : '+replace(cast(" . $amount . " as varchar),'.',',') where iPaymentNbr = " . $iPaymentNbr2;
        $this->db->query($q2);
    }
    public function getPayment($id)
    {
        $q = $this->db->query("select * from tblPayment where iPaymentNbr=?", array(
            $id
        ));
        if ($q->num_rows() > 0) {
            return $q->row();
        } else {
            return false;
        }
    }
    public function getpaymentAmount($PaymentNbr)
    {
        $q = $this->db->query("select P.mPaymentAmount from tblPayment P left join ( select iPaymentNbr, sum(mAssignedAmount) as TotalUsed from tblPaymentAssignment where iPaymentNbr = ? group by iPaymentNbr ) A ON A.iPaymentNbr = P.iPaymentNbr where P.iPaymentNbr = ?", array(
            $PaymentNbr,
            $PaymentNbr
        ));
        return $q->row()->mPaymentAmount;
    }
    public function getPaymentUsed($iPaymentNbr)
    {
        $q = $this->db->query("select ISNULL(A.TotalUsed,0) as TotalUsed
from tblPayment P
left join ( select iPaymentNbr, sum(mAssignedAmount) as TotalUsed from tblPaymentAssignment
       where iPaymentNbr = ?
       group by iPaymentNbr ) A ON A.iPaymentNbr = P.iPaymentNbr
where P.iPaymentNbr = ?", array(
            $iPaymentNbr,
            $iPaymentNbr
        ));
        return $q->row()->TotalUsed;
    }
    public function getAmountLeft($iPaymentNbr)
    {
        $q = $this->db->query("select P.mPaymentAmount-ISNULL(A.TotalUsed,0) as Amount
from tblPayment P
left join ( select iPaymentNbr, sum(mAssignedAmount) as TotalUsed from tblPaymentAssignment
       where iPaymentNbr = ?
       group by iPaymentNbr ) A ON A.iPaymentNbr = P.iPaymentNbr
where P.iPaymentNbr = ?", array(
            $iPaymentNbr,
            $iPaymentNbr
        ));
        return $q->row()->Amount;
    }
    public function getPaymentForm($iPaymentNbr)
    {
        $q = $this->db->query("select T.cTypeDescription as cPaymentForm

       from tblPayment P left join tblType T ON T.iTypeNbr = P.iPaymentForm

       where iPaymentNbr = ?", array(
            $iPaymentNbr
        ));
        return $q->row()->cPaymentForm;
    }

    public function refundAction($iPaymentNbr, $PaymentDate)
    {
        //iPaymentNbr, iAddressNbr, dPaymentDate, mPaymentAmount, iPaymentForm, cPaymentFormDescription, cPaymentRemark, bTotalyAssigned, dLastUpdate, iLastUserNbr
        $payment = $this->getPayment($iPaymentNbr);
        if ($payment) {
            $PaymentAmount          = $this->getpaymentAmount($iPaymentNbr);
            //$PaymentDate =  $payment->dPaymentDate;
            $PaymentFormDescription = $payment->cPaymentFormDescription;
            $iAddressNbr            = $payment->iAddressNbr;
            $Totalused              = $this->getPaymentUsed($iPaymentNbr);
            $AmountLeft             = $this->getAmountLeft($iPaymentNbr);
            $PaymentForm            = $this->getPaymentForm($iPaymentNbr);
            $PaymentRemark          = 'Split Amount : ' . str_replace('.', ',', $PaymentAmount);
            if ($Totalused == '.0000') {
                $Totalused = "0.00";
            }
            // return array( $PaymentAmount, $PaymentDate,$PaymentFormDescription, $iAddressNbr ,$Totalused  , $AmountLeft, $PaymentForm, $PaymentRemark);
            /*
            '!!! CHECK : if @PaymentAmount > @Totalused !!!

            --> THEN CONTINUE :'
            */
            if ($PaymentAmount > $Totalused) {
                //Query 1:
                $this->db->query("update tblPayment set mPaymentAmount = ?, bTotalAssigned = 1, cPaymentRemark = ? where iPaymentNbr = ?", array(
                    $Totalused,
                    $PaymentRemark,
                    $iPaymentNbr
                ));
                //Query 2:
                /*$this->db->query("update tblPayment set mPaymentAmount = ?, bTotalAssigned = 1, cPaymentRemark = ? where iPaymentNbr = ?", array(
                    $Totalused,
                    $PaymentRemark,
                    $iPaymentNbr
                ));
                */
                $res2         = $this->apply_payment(array(
                    'cPaymentForm' => $PaymentForm,
                    'mPaymentAmount' => $AmountLeft * -1,
                    'cPaymentFormDescription' => $PaymentFormDescription,
                    'dPaymentDate' => $PaymentDate,
                    'step' => 1,
                    'iAddressNbr' => $iAddressNbr,
                    'cPaymentRemark' => $PaymentRemark
                ));
                $iPaymentNbr1 = $iPaymentNbr;
                if ($res2->result) {
                    $iPaymentNbr2 = $res2->id;
                }
                $this->assignPaymentPayment($iPaymentNbr1, $iPaymentNbr2);
                // $this->db->query("Update tblPayment set cPaymentRemark = ?  where iPaymentNbr = ?", array('Payed w nbr - '.$PaymentRemark, $iPaymentNbr1));
                //  $this->db->query("Update tblPayment set cPaymentRemark = ?  where iPaymentNbr = ?", array('Payed w nbr - '.$PaymentRemark, $iPaymentNbr2));
                return array(
                    'result' => true
                );
            } else {
                return array(
                    'result' => false,
                    'message' => 'PaymentAmount must be higher than TotalUsed'
                );
            }
        } else {
            return array(
                'result' => false,
                'message' => 'Payment Number not found'
            );
        }
    }
    public function GetFinancialCondition($iAddressNbr)
    {
        $sql = "select dbo.fnGetFinancialCondition ( " . $iAddressNbr . " ) as Condition";
        $q   = $this->db->query($sql);
        $res = $q->row_array();
        return number_format($res['Condition'], 2);
    }
    public function getClientId($id)
    {
        $q = $this->db->query("select * from tblAddress where iAddressNbr=? and iCompanyNbr=?", array(
            $id,
            $this->companyid
        ));
        $s = $this->db->query("select b.iTypeGroupNbr,b.cTypeDescription,a.cAddressData,bPreferredOrActive from tblAddressData a  left join tblType b on b.iTypeNbr=a.iTypeNbr  where a.iAddressNbr=? and b.iTypeGroupNbr in (4,5,6)", array(
            $id
        ));
        if ($s->num_rows() > 0) {
            foreach ($s->result() as $row) {
                if ($row->bPreferredOrActive == "1") {
                    $data[str_replace(' ', '', $row->cTypeDescription)] = $row->cAddressData;
                }
            }
        } else {
            $data = array();
        }
        if ($q->num_rows() > 0) {
            return $this->set_response(array_merge($q->row_array(), $data), true);
        } else {
            return $this->set_response('No client found for id ' . $id, false);
        }
    }
    public function hasInvoice($mageboid, $vat)
    {
        if ($vat) {
            return true;
        } else {
            $q = $this->db->query("select * from tblInvoice where iAddressNbr=?", array(
                $mageboid
            ));
            if ($q->num_rows() > 0) {
                return "yes";
            } else {
                return "no";
            }
        }
    }
    public function GetPricingIndexPrice($index)
    {
        $q = $this->db->query("select * from tblGeneralPricing where iGeneralPricingIndex=?", array(
            $index
        ));
        return $q->row()->mUnitPrice;
    }
    public function updatePricingItem($index, $price)
    {
        if ($index) {
            $this->db->where('iGeneralPricingIndex', $index);
            $this->db->update('tblPricedItems', array(
                'mUnitPrice' => $price
            ));
            return "success\n";
        } else {
            return "error\n";
        }
    }
    public function getGeneralPricingIndexDetailUnited($domain)
    {
        $number = $number = "0" . $domain[2] . $domain[3] . $domain[4] . "/" . $domain[5] . $domain[6] . "." . $domain[7] . $domain[8] . "." . $domain[9] . $domain[10];
        $q      = $this->db->query("SELECT iGeneralPricingIndex, iAddressNbr, iInvoiceGroupNbr, rNumber, mUnitPrice, rVATPercentage, iMonthFrequency, iTotalMonths, dContractDate, iStartMonth, iStartYear, bEnabled, cRemark, iShowRemarkOnReport, cInvoiceDescription, bIncludePeriodDescription, dLastUpdate, iLastUserNbr
        FROM tblGeneralPricing where cRemark like ? and iTotalMonths = ?", array(
            'Abo%' . $number . '%',
            '600'
        ));
        if ($q->num_rows() > 0) {
            return $q->row()->iGeneralPricingIndex;
        } else {
            return false;
        }
    }
    public function getGeneralPricingIndexDetail($domain)
    {
        $number = $number = "0" . $domain[2] . "-" . $domain[3] . $domain[4] . " " . $domain[5] . "" . $domain[6] . " " . $domain[7] . "" . $domain[8] . " " . $domain[9] . $domain[10];
        $q      = $this->db->query("SELECT iGeneralPricingIndex, iAddressNbr, iInvoiceGroupNbr, rNumber, mUnitPrice, rVATPercentage, iMonthFrequency, iTotalMonths, dContractDate, iStartMonth, iStartYear, bEnabled, cRemark, iShowRemarkOnReport, cInvoiceDescription, bIncludePeriodDescription, dLastUpdate, iLastUserNbr
        FROM tblGeneralPricing where cRemark like ? and iTotalMonths = ?", array(
            '%' . $number . '%',
            '600'
        ));
        if ($q->num_rows() > 0) {
            return $q->row()->iGeneralPricingIndex;
        } else {
            return false;
        }
    }
    public function updateGeneraPricingIndex($pin, $index)
    {
        $q = $this->db->query("select iGeneralPricingIndex from tblGeneralPricing where iInvoiceGroupNbr = 369 and iTotalMonths > 100 and right(cRemark,14) in ( select cInvoiceReference from tblC_Pincode where iPincode = ? )", array(
            $pin
        ));
        if ($q->num_rows() > 0) {
            if ($q->row()->iGeneralPricingIndex != $index) {
                return $q->row();
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    public function getInvoices($iAddressNbr)
    {
        $q = $this->db->query("select * from tblInvoice where iAddressNbr=?", array(
            $iAddressNbr
        ));
        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return false;
        }
    }
    public function getDueInvoices($iAddressNbr, $iInvoiceNbr = false)
    {
        if ($iInvoiceNbr) {
            $q = $this->db->query("select * from tblInvoice where iInvoiceNbr=?", array(
                $iInvoiceNbr
            ));
        } else {
            $q = $this->db->query("select * from tblInvoice where iAddressNbr=? and iInvoiceType=? and iInvoiceStatus =? and dInvoiceDueDate < ?", array(
                $iAddressNbr,
                40,
                52,
                date('Y-m-d')
            ));
        }
        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return false;
        }
    }
    public function getDueInvoicesReminder3($iAddressNbr, $iInvoiceNbr = false)
    {
        if ($iInvoiceNbr) {
            $q = $this->db->query("select * from tblInvoice where iInvoiceNbr=?", array(
                $iInvoiceNbr
            ));
        } else {
            $q = $this->db->query("select * from tblInvoice where iAddressNbr=? and iInvoiceType=? and iInvoiceStatus  in(52,53) order by iInvoiceNbr ASC", array(
                $iAddressNbr,
                40
            ));
        }
        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return false;
        }
    }
    public function getInvoice($iInvoiceNbr)
    {
        $q = $this->db->query("select * from tblInvoice where iInvoiceNbr=?", array(
            $iInvoiceNbr
        ));
        if ($q->num_rows() > 0) {
            return $q->row();
        }
    }
    public function getCompanyName($companyid)
    {
        $q = $this->db->query("SELECT cCompany from tblCompany where iCompanyNbr=?", array(
            $companyid
        ));
        insert_query_log(array(
            'funct' => 'getCompanyName',
            'description' => "SELECT cCompany from tblCompany where iCompanyNbr=" . $companyid
        ));
        return $q->row()->cCompany;
    }
    //2498
    public function getFlag($type)
    {
        $q = $this->db->query("EXEC spGetFlag '" . $type . "'");
        return $q->row()->bFlagStatus;
    }
    public function updateFlag($type, $status)
    {
        $this->db->query("EXEC spUpdateFlag '" . $type . "', '" . $status . "'");
    }
    public function GetLastInvoice($companyname, $type)
    {
        $q      = $this->db->query("EXEC spGetLastInvoiceDate NULL, '" . $companyname . "', '" . $type . "'");
        $result = explode(' ', $q->row()->dLastInvoiceDate);
        return $result[0];
    }
    public function GetNewAddressNbr($companyid)
    {
        $q = $this->db->query("SELECT max(iAddressNbr) as id FROM tblAddress where iCompanyNbr=?", array(
            $companyid
        ));
        log_message('error', 'Companyid ' . $companyid);
        return $q->row()->id + 1;
    }
    public function ifcustomerexist($number)
    {
        $q = $this->db->query("select * from tblAddress where iAddressNbr=?", array(
            $number
        ));
        if ($q->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
    public function getNewInvoiceNbr($companyname)
    {
        $q = $this->db->query("EXEC spGetNewInvoiceNbr '" . $companyname . "'");
        return $q->row()->iNewInvoiceNbr;
    }
    public function getCountryIndex($code)
    {
        $q = $this->db->query("select * from tblCountry where cCountryCode=?", array(
            $code
        ));
        return $q->row()->iCountryIndex;
    }
    public function AddClient($cd)
    {
        try {
            if ($cd['country'] == $this->mvno_setting->country_base) {
                $customerType = 49;
                $lcl          = "LOCAL";
            } else {
                $eu_countries = array(
                    'AT',
                    'BE',
                    'HR',
                    'BG',
                    'CY',
                    'CZ',
                    'DK',
                    'EE',
                    'FI',
                    'FR',
                    'DE',
                    'GR',
                    'HU',
                    'IE',
                    'IT',
                    'LV',
                    'LT',
                    'LU',
                    'MT',
                    'NL',
                    'PL',
                    'PT',
                    'RO',
                    'SK',
                    'SI',
                    'ES',
                    'SE',
                    'GB'
                );
                if ($cd['companyid'] == 54) {
                    $customerType = 49;
                    $lcl          = "LOCAL";
                } else {
                    if (in_array($cd['country'], $eu_countries)) {
                        $customerType = 50;
                        $lcl          = "EUROPE";
                    } else {
                        $customerType = 51;
                        $lcl          = "NON EUROPE";
                    }
                }
            }
            $duedays     = 14;
            $iCompanyNbr = $cd['companyid'];
            // return (object) array('result' => 'success', 'iAddressNbr' => $iCompanyNbr);
            $cCompany    = $this->getCompanyName($iCompanyNbr);
            switch ($cd['language']) {
                case "dutch":
                    $cInternalLanguage = "Dutch";
                    break;
                case "english":
                    $cInternalLanguage = "English";
                    break;
                case "french":
                    $cInternalLanguage = "French";
                    break;
                case "german":
                    $cInternalLanguage = "German";
                    break;
                default:
                    $cInternalLanguage = "Dutch";
            }
            if (!empty($cd['companyname']) && !empty($cd['vat'])) {
                $cName = utf8_decode(html_entity_decode(str_replace("'", "''", ucfirst($cd['companyname']))));
            } else {
                $ccc   = (object) $cd;
                if (in_array($iCompanyNbr, array(53,56))) {
                    $cName = utf8_decode(html_entity_decode(format_name_address_no_salutation($ccc)));
                } else {
                    $cName = utf8_decode(html_entity_decode(format_name_address($ccc)));
                }

                //$cName = utf8_decode(html_entity_decode($));
                unset($ccc);
            }
            $cStreet   = utf8_decode(html_entity_decode(str_replace("'", " ", $cd['address1']))) . ' ' . $cd['housenumber'] . ' ' . $cd['alphabet'];
            $countries = getCountries();
            /* foreach ($countries as $key => $c) {
            if ($key == $cd['country']) {
            $cCountry = strtoupper($c);
            }
            }
            */
            $cCountry  = $this->getCountryName($cd['country']);
            if (!empty($cd['payment_duedays'])) {
                if ($duedays > 1) {
                    $duedays = $cd['payment_duedays'];
                }
            }
            $iZipCode   = explode(' ', $cd['postcode']);
            $cZipSuffix = "";
            $cCity      = utf8_decode(html_entity_decode(str_replace("'", "''", $cd['city'])));
            $cRemark    = "";
            $bCustomer  = 1;
            $bAgent     = 0;
            $bCarrier   = 0;
            $bIncasso   = 0;
            $bInvoice   = 0;
            $bLocation  = 1;
            $bSupplier  = 0;
            $bType      = 0;
            $cCompany   = $cCompany;
            $zc         = explode(' ', $cd['postcode']);
            if (count($zc) == 2) {
                if (is_numeric($zc[0])) {
                    $iZipCode   = $zc[0];
                    $cZipSuffix = $zc[1];
                } else {
                    $iZipCode   = "null";
                    $cZipSuffix = $cd['postcode'];
                }
            } else {
                if (is_numeric($cd['postcode'])) {
                    $iZipCode   = $cd['postcode'];
                    $cZipSuffix = "";
                } else {
                    $iZipCode   = "null";
                    $cZipSuffix = $cd['postcode'];
                }
            }
            if (file_exists('/home/mvno/public_html/application/lock/' . $iCompanyNbr)) {
                sleep(4);
                $iAddressNbr = $this->GetNewAddressNbr($iCompanyNbr);
            } else {
                $iAddressNbr = $this->GetNewAddressNbr($iCompanyNbr);
            }
            if ($iZipCode == "null") {
                $this->db->query("EXEC spInsertAddress $iAddressNbr,'$cName','$cStreet','$cCountry',null,'$cZipSuffix','$cCity','$cRemark',$bCustomer,$bAgent,$bCarrier,$bIncasso,$bInvoice,$bLocation,$bSupplier,$bType, '$cCompany','$cInternalLanguage'");
                log_message('error', "EXEC spInsertAddress $iAddressNbr,'$cName','$cStreet','$cCountry',null,'$cZipSuffix','$cCity','$cRemark',$bCustomer,$bAgent,$bCarrier,$bIncasso,$bInvoice,$bLocation,$bSupplier,$bType, '$cCompany','$cInternalLanguage'");
            } else {
                $this->db->query("EXEC spInsertAddress $iAddressNbr,'$cName','$cStreet','$cCountry',$iZipCode,'$cZipSuffix','$cCity','$cRemark',$bCustomer,$bAgent,$bCarrier,$bIncasso,$bInvoice,$bLocation,$bSupplier,$bType, '$cCompany','$cInternalLanguage'");
            }
            unlink('/home/mvno/public_html/application/lock/' . $iCompanyNbr);
            insert_query_log(array(
                'funct' => 'AddClient',
                'description' => "EXEC spInsertAddress $iAddressNbr,'$cName','$cStreet','$cCountry',$iZipCode,'$cZipSuffix','$cCity','$cRemark',$bCustomer,$bAgent,$bCarrier,$bIncasso,$bInvoice,$bLocation,$bSupplier,$bType, '$cCompany','$cInternalLanguage'"
            ));
            if (!empty($cd['mvno_id'])) {
                $this->db->query("EXEC spInsertAddressData " . $iAddressNbr . ", 6, 'MVNO CUSTOMER LOOKUP', '" . $cd['mvno_id'] . "', '',1");
            }

            if ($cd['id']) {
                $this->db->query("EXEC spInsertAddressData " . $iAddressNbr . ", 7, 'iPortalNbr', '" . $cd['id'] . "', '',1");
            }
            if (!empty($cd['vat'])) {
                $this->db->query("EXEC spInsertAddressData " . $iAddressNbr . ", 5, 'VAT NUMBER', '" . $cd['vat'] . "', '',1");
            }
            if (strlen($cd['invoice_email']) == "yes") {
                $this->db->query("EXEC spUpdateInsertInvoiceConditionEmail " . $iAddressNbr . ", '" . $cd['email'] . "'");
                $this->db->query("EXEC spUpdateInvoiceCondition " . $iAddressNbr . ", " . $duedays . ", 'NORMAL', 0, 'DIGITAL ( WHMCS )', 1, 0, 0, 1, 'FULL DETAIL', NULL, '', 'NO REPORTING', 1, 0, '" . $lcl . "', 0, NULL, 0, 'FIX COST', 10, 1, 'NO EXCHANGE', 0");
            } else {
                $this->db->query("EXEC spUpdateInvoiceCondition " . $iAddressNbr . ", " . $duedays . ", 'NORMAL', 0, 'PAPER ( WHMCS )', 1, 0, 0, 1, 'FULL DETAIL', NULL, '', 'NO REPORTING', 1, 0, '" . $lcl . "', 0, NULL, 0, 'FIX COST', 10, 1, 'NO EXCHANGE', 0");
            }
            if (strlen($cd['nationalnr']) > 0) {
                $this->db->query("EXEC spInsertAddressData " . $iAddressNbr . ", 6, 'IDENTITY CARD NUMBER', '" . $cd['nationalnr'] . "', '',1");
            }
            if (strlen($cd['companyname']) > 0) {
                $this->db->query("EXEC spInsertAddressData " . $iAddressNbr . ", 7, 'COMPANY NAME', ?, '',1", array(
                    utf8_decode(html_entity_decode($cd['companyname']))
                ));
            }
            if (strlen($cd['phonenumber']) > 0) {
                $this->db->query("EXEC spInsertAddressData " . $iAddressNbr . ", 4, 'TELEPHONENUMBER', '" . $cd['phonenumber'] . "', '',1");
            }
            if (strlen($cd['iban']) > 0) {
                $this->db->query("EXEC spInsertAddressData " . $iAddressNbr . ", 5, 'IBAN', '" . $cd['iban'] . "', '',1");
                $this->db->query("EXEC spInsertAddressData " . $iAddressNbr . ", 5, 'BIC', '" . $cd['bic'] . "', '',1");
            }
            if (!empty($cd['date_birth'])) {
                $this->db->query("EXEC spInsertAddressData " . $iAddressNbr . ", 7, 'BIRTHDATE', '" . $cd['date_birth'] . "', '',1");
            }
            return (object) array(
                'result' => 'success',
                'iAddressNbr' => $iAddressNbr
            );
        } catch (Exception $e) {
            return (object) array(
                'result' => 'error',
                'message' => $e->getMessage()
            );
        }
    }

    public function insertAddressData($data)
    {
        $this->db->query("EXEC spInsertAddressData " . $data->mageboid . ", 7, 'iPortalNbr', '" . $data->id . "', '',1");
    }
    public function getCountryName($code)
    {
        $q = $this->db->query("select * from tblCountry where cCountryCode =?", array(
            $code
        ));
        return $q->row()->cCountry;
    }
    public function change_contract_date($pin, $date)
    {
        if (substr($pin, 0, 2) == "32") {
            $pin = substr(trim($pin), 2);
        }
        $i      = 'Orderdate Updated : ' . $date;
        $podate = explode('-', $date);
        $remark = $podate[1] . '/' . $podate[0] . '/' . $podate[2];
        $this->db->query("Update tblC_Pincode set dContractDate = ?, cRemark = ? where iPincode = ?", array(
            $date,
            $remark,
            $pin
        ));
        $this->db->query("Update tblC_Pincode set dContractDate = ?, cRemark = ? where iPincode in ( select iMSISDN from tblC_SIMCard where iPincode = ? )", array(
            $date,
            $remark,
            $pin
        ));
    }
    public function updatePricingMonth($indexid, $month)
    {
        $this->db->query('update tblGeneralPricing set iStartMonth=? where iGeneralPricingIndex=?', array(
            $month,
            $indexid
        ));
    }
    public function updatePricingMonthbypin($pin, $year, $month, $day)
    {
        if (substr(trim($pin), 0, 2) == "32") {
            $iPincode = substr($pin, 2);
            $number   = "0" . substr($iPincode, 0, 3) . '/' . substr($iPincode, 3, 2) . "." . substr($iPincode, 5, 2) . "." . substr($iPincode, 7, 2);
        } else {
            $number = "0" . $pin[2] . "-" . $pin[3] . $pin[4] . " " . $pin[5] . "" . $pin[6] . " " . $pin[7] . "" . $pin[8] . " " . $pin[9] . $pin[10];
        }
        $date = $year . '-' . $month . '-' . $day;
        $this->db->like('cInvoiceDescription', '%' . $number . '%');
        $this->db->update('tblGeneralPricing', array(
            'iStartMonth' => $month,
            'dContractDate' => $date,
            'iStartYear' => $year
        ));
    }
    public function update_name($mageboid, $name)
    {
        $this->db->where('iAddressNbr', $mageboid);
        $this->db->update('tblAddress', array(
            'cName' => $name
        ));
        return $this->db->affected_rows();
    }
    public function getSubscriptions()
    {
        $q = $this->db->query("SELECT a.iGeneralPricingIndex,a.iAddressNbr,a.dContractDate,a.cRemark FROM tblGeneralPricing a left join tblAddress b on b.iAddressNbr = a.iAddressNbr where b.iCompanyNbr=53  and a.iTotalMonths=600");
        return $q->result();
    }
    public function hasUnpaidInvoice($iAddressNbr)
    {
        $today = date('Y-m-d');
        $q1    = $this->db->query("SELECT a.*
        FROM tblInvoice a left join tblAddress b on b.iAddressNbr =a.iAddressNbr
        where b.iCompanyNbr=?
        and a.iAddressNbr = ?
        and a.iInvoiceStatus IN ('52')
        and a.dInvoiceDueDate < ?
        and a.iInvoiceType = 40 order by a.iInvoiceNbr asc", array(
            $this->companyid,
            $iAddressNbr,
            $today
        ));
        log_message('error', 'add payment magebo check unpaid invoices '.$this->db->last_query());
        if ($q1->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
    public function getReminderInvoice($id, $type = false)
    {
        $q1                   = $this->db->query("SELECT a.*
        FROM tblInvoice a left join tblAddress b on b.iAddressNbr =a.iAddressNbr
        WHERE a.iInvoiceNbr = ?
        and a.iInvoiceStatus = 52
        and a.iInvoiceType = 40 order by a.iInvoiceNbr asc", array(
            $id
        ));
        $res                  = $q1->row_array();
        $res['reminder_type'] = $type;
        return (object) $res;
    }
    public function getUnpaidInvoice($iAddressNbr)
    {
        $q1 = $this->db->query("SELECT a.*
        FROM tblInvoice a left join tblAddress b on b.iAddressNbr =a.iAddressNbr
        where b.iCompanyNbr=?
        and a.iAddressNbr = ?
        and a.iInvoiceStatus = 52
        and a.iInvoiceType = 40 order by a.iInvoiceNbr asc", array(
            $this->companyid,
            $iAddressNbr
        ));
        if ($q1->num_rows() > 0) {
            return $q1->result_array();
        } else {
            return array();
        }
    }
    public function getUnpaidInvoices($date)
    {
        $count          = 0;
        $result         = array(
            'reminder_1' => array(),
            'reminder_2' => array(),
            'reminder_3' => array()
        );
        $reminder1_date = date('Y-m-d', strtotime('-' . $this->mvno_setting->reminder_1 . ' day', strtotime($date)));
        $reminder2_date = date('Y-m-d', strtotime('-' . $this->mvno_setting->reminder_2 . ' day', strtotime($date)));
        $reminder3_date = date('Y-m-d', strtotime('-' . $this->mvno_setting->reminder_3 . ' day', strtotime($date)));
        echo $reminder1_date . "\n";
        echo $reminder2_date . "\n";
        echo $reminder3_date . "\n";
        $q1 = $this->db->query("SELECT a.*,'reminder_1' as reminder_type
        FROM tblInvoice a left join tblAddress b on b.iAddressNbr =a.iAddressNbr
        where b.iCompanyNbr=?
        and a.dInvoiceDueDate = ?
        and a.iInvoiceStatus = 52
        and a.iInvoiceType = 40 order by a.iInvoiceNbr asc", array(
            $this->companyid,
            $reminder1_date
        ));
        $q2 = $this->db->query("SELECT a.*,'reminder_2' as reminder_type
        FROM tblInvoice a left join tblAddress b on b.iAddressNbr =a.iAddressNbr
        where b.iCompanyNbr=?
        and a.dInvoiceDueDate = ?
        and a.iInvoiceStatus = 52
        and a.iInvoiceType = 40 order by a.iInvoiceNbr asc", array(
            $this->companyid,
            $reminder2_date
        ));
        $q3 = $this->db->query("SELECT a.*,'reminder_3' as reminder_type
        FROM tblInvoice a left join tblAddress b on b.iAddressNbr =a.iAddressNbr
        where b.iCompanyNbr=?
        and a.dInvoiceDueDate = ?
        and a.iInvoiceStatus = 52
        and a.iInvoiceType = 40 order by a.iInvoiceNbr asc", array(
            $this->companyid,
            $reminder3_date
        ));
        if ($q1->num_rows() > 0) {
            $result['reminder_1'] = $q1->result();
            $count                = $count + 1;
        }
        if ($q2->num_rows() > 0) {
            $result['reminder_2'] = $q2->result();
            $count                = $count + 1;
        }
        if ($q3->num_rows() > 0) {
            $result['reminder_3'] = $q3->result();
            $count                = $count + 1;
        }
        if ($count > 0) {
            return $result;
        } else {
            return false;
        }
    }
    public function getUnpaidInvoicesByRejection($date)
    {
        $count          = 0;
        $result         = array(
            'reminder_1' => array(),
            'reminder_2' => array(),
            'reminder_3' => array()
        );
        $reminder1_date = date('Y-m-d', strtotime('-' . $this->mvno_setting->reminder_1 . ' day', strtotime($date)));
        $reminder2_date = date('Y-m-d', strtotime('-' . $this->mvno_setting->reminder_2 . ' day', strtotime($date)));
        $reminder3_date = date('Y-m-d', strtotime('-' . $this->mvno_setting->reminder_3 . ' day', strtotime($date)));
        echo "Reminde1 Date:    " . $reminder1_date . "\n";
        echo "Reminde2 Date:    " . $reminder2_date . "\n";
        echo "Reminde3 Date:    " . $reminder3_date . "\n";
        $r1 = $this->db1->query("SELECT message FROM `a_sepa_items` WHERE `companyid` = ? and extra_status=0 and amount < 0 and mandateid like 'SEPA%' and date_transaction like ? ORDER BY date_transaction asc", array(
            $this->companyid,
            $reminder1_date . '%'
        ));
        $r2 = $this->db1->query("SELECT message FROM `a_sepa_items` WHERE `companyid` = ? and extra_status=0 and amount < 0 and mandateid like 'SEPA%' and date_transaction like ? ORDER BY date_transaction asc", array(
            $this->companyid,
            $reminder2_date . '%'
        ));
        $r3 = $this->db1->query("SELECT message FROM `a_sepa_items` WHERE `companyid` = ? and extra_status=0 and amount < 0 and mandateid like 'SEPA%' and date_transaction like ? ORDER BY date_transaction asc", array(
            $this->companyid,
            $reminder3_date . '%'
        ));
        if ($r1->num_rows() > 0) {
            foreach ($r1->result() as $row1) {
                $invoices1[] = trim($row1->message);
            }
            $q1 = $this->getReminderList('reminder_1', $invoices1);
            if (count($q1) > 0) {
                $qr1 = array();
                foreach ($q1 as $row) {
                    if ($row->iInvoiceStatus == 52) {
                        $qr1[] = $row;
                    }
                }
                if ($qr1) {
                    $result['reminder_1'] = $qr1;
                    $count                = $count + 1;
                }
            }
        }
        if ($r2->num_rows() > 0) {
            foreach ($r2->result() as $row2) {
                $invoices2[] = trim($row2->message);
            }
            $q2 = $this->getReminderList('reminder_2', $invoices2);
            if (count($q2) > 0) {
                $qr2 = array();
                foreach ($q2 as $row) {
                    if ($row->iInvoiceStatus == 52) {
                        $qr2[] = $row;
                    }
                }
                if ($qr2) {
                    $result['reminder_2'] = $qr2;
                    $count                = $count + 1;
                }
            }
        }
        if ($r3->num_rows() > 0) {
            foreach ($r3->result() as $row3) {
                $invoices3[] = trim($row3->message);
            }
            // print_r($invoices3);
            $q3 = $this->getReminderList('reminder_3', $invoices3);
            if (count($q3) > 0) {
                $qr3 = array();
                foreach ($q3 as $row) {
                    if ($row->iInvoiceStatus == 52) {
                        $qr3[] = $row;
                    }
                }
                if ($qr3) {
                    $result['reminder_3'] = $qr3;
                    $count                = $count + 1;
                }
            }
        }
        if ($count > 0) {
            return $result;
        } else {
            return 'none';
        }
    }
    public function getReminderList($type, $invoices)
    {
        $this->db->where_in('iInvoiceNbr', $invoices);
        if ($type == "reminder_3") {
            $this->db->select("*, 'reminder_3' as reminder_type");
        } elseif ($type == "reminder_2") {
            $this->db->select("*, 'reminder_2' as reminder_type");
        } elseif ($type == "reminder_1") {
            $this->db->select("*, 'reminder_1' as reminder_type");
        } elseif ($type == "reminder_4") {
            $this->db->select("*, 'reminder_4' as reminder_type");
        }
        $res = $this->db->get('tblInvoice');
        if ($res->num_rows() > 0) {
            return $res->result();
        } else {
            return false;
        }
    }
    public function isUnpaid($iInvoiceNbr)
    {
        $q = $this->db->query("select * from tblInvoice where iInvoiceNbr=? and iInvoiceStatus=?", array(
            $iInvoiceNbr,
            '52'
        ));
        if ($q->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
    public function updateClient($clientid, $data, $cid)
    {
        $mvno_setting = globofix($cid);
        if ($clientid > 0) {
            $this->db->where('iAddressNbr', $clientid);
            $cc             = (object) $data;
            if (in_array($cid, array('53','56'))) {
                $udata['cName'] = format_name_address_no_salutation($cc);
            } else {
                $udata['cName'] = format_name_address($cc);
            }

            unset($cc);
            // mail('mail@simson.one', 'changes', print_r($mvno_setting, true) . print_r($data, true));
            if ($data['country'] == $mvno_setting->country_base) {
                $customerType      = 49;
                $lcl               = "LOCAL";
                $pcod              = explode(' ', $data['postcode']);
                $udata['iZipCode'] = $pcod[0];
                if (!empty($pcod[1])) {
                    $udata['cZIPSuffix'] = $pcod[1];
                }
            } else {
                $eu_countries = array(
                    'AT',
                    'BE',
                    'HR',
                    'BG',
                    'CY',
                    'CZ',
                    'DK',
                    'EE',
                    'FI',
                    'FR',
                    'DE',
                    'GR',
                    'HU',
                    'IE',
                    'IT',
                    'LV',
                    'LT',
                    'LU',
                    'MT',
                    'NL',
                    'PL',
                    'PT',
                    'RO',
                    'SK',
                    'SI',
                    'ES',
                    'SE',
                    'GB'
                );
                if (in_array($data['country'], $eu_countries)) {
                    $customerType = 50;
                    $lcl          = "EUROPE";
                } else {
                    $customerType = 51;
                    $lcl          = "NON EUROPE";
                }
                $pcod                = array(
                    $data['postcode'],
                    ''
                );
                $udata['cZIPSuffix'] = $data['postcode'];
            }
            $udata['cStreet']       = trim($data['address1'] . ' ' . $data['housenumber'] . ' ' . $data['alphabet']);
            $udata['cCity']         = $data['city'];
            $udata['iCountryIndex'] = $this->getCountryIndex($data['country']);
            if (!empty($data['payment_duedays'])) {
                if ($data['payment_duedays'] > 1) {
                    $duedays = $data['payment_duedays'];
                } else {
                    $duedays = 14;
                }
            } else {
                $duedays = 14;
            }
            //mail('mail@simson.one', 'ststs', print_r($udata, true));
            $this->db->update('tblAddress', $udata);
            $this->updateInvoiceZone($clientid, $customerType);
            //Update address data
            if ($data['invoice_email'] == "yes") {
                $this->UpdateInsertInvoiceConditionEmail($clientid, $data['invoice_email']);
                $this->db->query("EXEC spUpdateInvoiceCondition " . $clientid . ", " . $duedays . ", 'NORMAL', 0, 'DIGITAL ( WHMCS )', 1, 0, 0, 1, 'FULL DETAIL', NULL, '', 'NO REPORTING', 1, 0, '" . $lcl . "', 0, NULL, 0, 'FIX COST', 10, 1, 'NO EXCHANGE', 0");
            } else {
                $this->db->query("EXEC spUpdateInvoiceCondition " . $clientid . ", " . $duedays . ", 'NORMAL', 0, 'PAPER ( WHMCS )', 1, 0, 0, 1, 'FULL DETAIL', NULL, '', 'NO REPORTING', 1, 0, '" . $lcl . "', 0, NULL, 0, 'FIX COST', 10, 1, 'NO EXCHANGE', 0");
            }
            if (!empty($data['vat']) > 0) {
                $this->db->query("EXEC spInsertAddressData " . $clientid . ", 5, 'VAT NUMBER', '" . $data['vat'] . "', '',1");
            }
            if (!empty($data['companyname'])) {
                $res[] = $this->updateAddressdata($clientid, 947, $data['companyname']);
            }
            $res[] = $this->updateAddressdata($clientid, 18, $data['phonenumber']);
            $res[] = $this->updateAddressdata($clientid, 34, $data['email']);
            // $this->updateAddressdata($clientid, 936, $data['email']);
            $res[] = $this->updateAddressdata($clientid, 936, strtoupper($data['gender']));
            $res[] = $this->updateAddressdata($clientid, 896, $data['nationalnr']);
            $res[] = $this->updateAddressdata($clientid, 494, $data['date_birth']);
            //$res[] = $this->updateAddressdata($clientid, 2098, $data['iban']);
            //$res[] = $this->updateAddressdata($clientid, 2099, $data['bic']);
            $res[] = $this->updateAddressdata($clientid, 736, trim($data['mvno_id']));
            if ($data['paymentmethod'] != "directdebit") {
                foreach (array(
                    2099,
                    2098,
                    2102,
                    2108,
                    2100,
                    2101
                ) as $to) {
                    $this->disableSEPA($clientid, $to);
                }
            }
            return (object) array(
                'result' => 'success',
                $res
            );
        } else {
            return (object) array(
                'result' => 'error',
                'message' => 'Empty ' . $clientid
            );
        }
    }
    public function disableSEPA($iAddressNbr, $id)
    {
        $this->db->where('iAddressNbr', $iAddressNbr);
        $this->db->where('iTypeNbr', $id);
        $this->db->update('tblAddressData', array(
            'bPreferredOrActive' => 0
        ));
    }
    public function addSubscription($data)
    {
    }
    public function updateMvnoId($data)
    {
        $this->updateAddressdata($data['id'], 736, trim($data['mvno_id']));
    }
    public function getInvoiceCdr($table, $iInvoiceNbr)
    {
        // $this->db->select('iPincode, iCallerNbr, iDestinationNbr, convert(dCallDate, getdate(), 120) as dCallDate, iRealDuration, iInvoiceDuration, cDialedNumber, mSale, mInvoiceSale, iCallPriceNbr, iInvoiceNbr, dExportDate, dMonthExportDate');
        // $this->db->where('iInvoiceNber', $iInvoiceNbr);
        $query = "select iPincode, iCallerNbr, iDestinationNbr,dCallDate, iRealDuration, iInvoiceDuration, cDialedNumber, mSale, mInvoiceSale, iCallPriceNbr, iInvoiceNbr, dExportDate, dMonthExportDate from $table where iInvoiceNbr=$iInvoiceNbr";
        $s     = "select P.iPincode,P.cInvoiceReference, convert(varchar,C.dCallDate,120) as dCallDate, C.iInvoiceDuration, C.cDialedNumber, T.cCallTranslation,
        C.mInvoiceSale as mInvoiceSale,CP.mPrice as mPrice, C.iInvoiceNbr, IGD.cInvoiceGroupDescription,A.iAddressNbr,T.iDestinationNbr,IG.iInvoiceGroupNbr
        ,A.iCompanyNbr, CASE IC.iCustomerOrigin WHEN 49 THEN CASE IG.bDefaultNoVAT WHEN 1 THEN 0    ELSE 21 END    ELSE 0 END VATPercentage
 from " . $table . " C
 left join tblC_Pincode P on P.iPincode = C.iPincode
 left join tblLocation L on L.iLocationIndex = P.iLocationIndex
 left join tblAddress A on A.iAddressNbr = L.iAddressNbr
 left join tblInvoiceCondition IC ON IC.iAddressNbr = A.iAddressNbr
 left join tblC_CallPriceList CP on C.iCallPriceNbr = CP.iCallPriceNbr
 left join tblInvoiceGroup IG on CP.iInvoiceGroupNbr = IG.iInvoiceGroupNbr
 left join tblInvoiceGroupDescription IGD on IG.iInvoiceGroupNbr = IGD.iInvoiceGroupNbr and IGD.iLanguageIndex = A.iLanguageIndex
 left join tblC_Destination D ON ( D.iDestinationNbr = C.iDestinationNbr )
 left join dbo.tblC_CallTranslation T ON ( T.iDestinationNbr = D.iDestinationNbr ) and T.iLanguageIndex = A.iLanguageIndex
 where T.iCallerNbr = C.iCallerNbr
 and C.iInvoiceNbr = " . $iInvoiceNbr . "
 order by C.iPincode, C.dCallDate";
        // echo $s;
        $q     = $this->db->query($s);
        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return false;
        }
    }
    public function updateInvoiceDuedate($data, $companyid)
    {
        if ($data['iInvoiceNbr']) {
            $this->db->where('iInvoiceNbr', $data['iInvoiceNbr']);
            $this->db->where('iAddressNbr', $data['iAddressNbr']);
            $this->db->update('tblInvoice', array(
                'dInvoiceDueDate' => $data['dInvoiceDueDate'] . ' 00:00:00'
            ));
            return true;
        } else {
            return false;
        }
    }
    public function getInvoiceCdrSplit($table, $iInvoiceNbr)
    {
        // $this->db->select('iPincode, iCallerNbr, iDestinationNbr, convert(dCallDate, getdate(), 120) as dCallDate, iRealDuration, iInvoiceDuration, cDialedNumber, mSale, mInvoiceSale, iCallPriceNbr, iInvoiceNbr, dExportDate, dMonthExportDate');
        // $this->db->where('iInvoiceNber', $iInvoiceNbr);
        $query = "select iPincode, iCallerNbr, iDestinationNbr,dCallDate, iRealDuration, iInvoiceDuration, cDialedNumber, mSale, mInvoiceSale, iCallPriceNbr, iInvoiceNbr, dExportDate, dMonthExportDate from $table where iInvoiceNbr=$iInvoiceNbr";
        $res   = array();
        $s     = "select P.iPincode,P.cInvoiceReference, convert(varchar,C.dCallDate,120) as dCallDate, C.iInvoiceDuration, C.cDialedNumber, T.cCallTranslation,
        C.mInvoiceSale as mInvoiceSale,CP.mPrice as mPrice, C.iInvoiceNbr, IGD.cInvoiceGroupDescription,A.iAddressNbr,T.iDestinationNbr,IG.iInvoiceGroupNbr
        ,A.iCompanyNbr
 from " . $table . " C
 left join tblC_Pincode P on P.iPincode = C.iPincode
 left join tblLocation L on L.iLocationIndex = P.iLocationIndex
 left join tblAddress A on A.iAddressNbr = L.iAddressNbr
 left join tblC_CallPriceList CP on C.iCallPriceNbr = CP.iCallPriceNbr
 left join tblInvoiceGroup IG on CP.iInvoiceGroupNbr = IG.iInvoiceGroupNbr
 left join tblInvoiceGroupDescription IGD on IG.iInvoiceGroupNbr = IGD.iInvoiceGroupNbr and IGD.iLanguageIndex = A.iLanguageIndex
 left join tblC_Destination D ON ( D.iDestinationNbr = C.iDestinationNbr )
 left join dbo.tblC_CallTranslation T ON ( T.iDestinationNbr = D.iDestinationNbr ) and T.iLanguageIndex = A.iLanguageIndex
 where T.iCallerNbr = C.iCallerNbr
 and C.iInvoiceNbr = " . $iInvoiceNbr . "
 order by C.iPincode, C.dCallDate";
        // echo $s;
        $q     = $this->db->query($s);
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $row) {
                $res[$row->iPincode][] = $row;
            }
            return $res;
        } else {
            return false;
        }
    }
    public function getDataCdr($iInvoiceNbr)
    {
        $res = array();
        $cdr = $this->db->query("exec sp_DataUsageTMNL " . $iInvoiceNbr);
        if ($cdr->num_rows() > 0) {
            foreach ($cdr->result() as $row) {
                $res[$row->iPincode][] = $row;
            }
            return $res;
        } else {
            return false;
        }
    }
    public function UpdatePricing($data)
    {
        $this->db->where('iGeneralPricingIndex', $data->iGeneralPricingIndex);
        $this->db->where('iAddressNbr', $data->iAddressNbr);
        $this->db->update('tblGeneralPricing', array(
            'mUnitPrice' => $data->mUnitPrice
        ));
        return $this->db->affected_rows();
    }
    public function getInvoiceJobNbr($iInvoiceNbr)
    {
        $q = $this->db->query("SELECT iInvoicedCallsIndex, iInvoiceJobNbr, iInvoiceNbr, iNbrOfRecords, iInvoicePrintCounter, iCallsPrintCounter, iNbrOfInvoicePages, iNbrOfMarketingPages, iNbrOfCallPages, dLastUpdate, iLastUserNbr
        FROM tblC_InvoicedCalls where iInvoiceNbr=?", array(
            $iInvoiceNbr
        ));
        if ($q->num_rows() > 0) {
            return $q->row()->tblC_InvoicedCallFileReference;
        } else {
            return false;
        }
    }
    public function getInvoiceCdrFile($iInvoiceNbr)
    {
        $q = $this->db->query("SELECT iInvoicedCallFileReference, iInvoiceNbr, cCallFileName, dLastUpdate, iLastUserNbr
        FROM tblC_InvoicedCallFileReference where iInvoiceNbr=?", array(
            $iInvoiceNbr
        ));
        if ($q->num_rows() == 1) {
            $filename = $q->row()->cCallFileName;
            return $this->getInvoiceCdr($filename, $iInvoiceNbr);
        } elseif ($q->num_rows() == 2) {
            foreach ($q->result() as $row) {
                $res[] = $this->getInvoiceCdr($row->cCallFileName, $iInvoiceNbr);
            }
            foreach ($res as $p) {
                foreach ($p as $c) {
                    $rs[] = $c;
                }
            }
            return $rs;
        } elseif ($q->num_rows() == 3) {
            foreach ($q->result() as $row) {
                $res[] = $this->getInvoiceCdr($row->cCallFileName, $iInvoiceNbr);
            }
            foreach ($res as $p) {
                foreach ($p as $c) {
                    $rs[] = $c;
                }
            }
            return $rs;
        } else {
            return false;
        }
    }
    public function getInvoiceCdrFileSplit($iInvoiceNbr)
    {
        $res = array();
        $q   = $this->db->query("SELECT iInvoicedCallFileReference, iInvoiceNbr, cCallFileName, dLastUpdate, iLastUserNbr
        FROM tblC_InvoicedCallFileReference where iInvoiceNbr=?", array(
            $iInvoiceNbr
        ));
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $row) {
                $res[] = $this->getInvoiceCdrSplit($row->cCallFileName, $iInvoiceNbr);
            }
            foreach ($res as $r) {
                foreach ($r as $key => $row) {
                    foreach ($row as $pp) {
                        $rm[$key][] = $pp;
                    }
                }
            }
            return $rm;
        } else {
            return false;
        }
    }
    public function updateAddressdata($iAddressNbr, $iTypeNbr, $Value)
    {
        if ($iAddressNbr > 0) {
            $this->db->where('iAddressNbr', $iAddressNbr);
            $this->db->where('iTypeNbr', $iTypeNbr);
            $this->db->where('bPreferredOrActive', 1);
            $this->db->update('tblAddressData', array(
                'cAddressData' => $Value
            ));
            if ($this->db->affected_rows() > 0) {
                return (object) array(
                    'result' => 'success'
                );
            } else {
                if ($iTypeNbr == 736) {
                    $this->db->query("EXEC spInsertAddressData " . $iAddressNbr . ", 6, 'MVNO CUSTOMER LOOKUP', '" . $Value . "', '',1");
                    return (object) array(
                        'result' => 'success'
                    );
                } elseif ($iTypeNbr == 23) {
                    $this->db->query("EXEC spInsertAddressData " . $iAddressNbr . ", 5, 'VAT NUMBER', '" . $Value . "', '',1");
                    return (object) array(
                        'result' => 'success'
                    );
                } elseif ($iTypeNbr == 896) {
                    $this->db->query("EXEC spInsertAddressData " . $iAddressNbr . ", 6, 'IDENTITY CARD NUMBER', '" . $Value . "', '',1");
                    return (object) array(
                        'result' => 'success'
                    );
                } elseif ($iTypeNbr == 947) {
                    $this->db->query("delete from tblAddressData where iAddressNbr=? and iTypeNbr = 947", array(
                        $iAddressNbr
                    ));
                    $this->db->query("EXEC spInsertAddressData " . $iAddressNbr . ", 7, 'COMPANY NAME', '" . utf8_decode(html_entity_decode($Value)) . "', '',1");
                    return (object) array(
                        'result' => 'success'
                    );
                } elseif ($iTypeNbr == 18) {
                    $this->db->query("EXEC spInsertAddressData " . $iAddressNbr . ", 4, 'TELEPHONENUMBER', '" . $Value . "', '',1");
                    return (object) array(
                        'result' => 'success'
                    );
                } elseif ($iTypeNbr == 2098) {
                    $this->db->query("EXEC spInsertAddressData " . $iAddressNbr . ", 5, 'IBAN', '" . $Value . "', '',1");
                    return (object) array(
                        'result' => 'success'
                    );
                } elseif ($iTypeNbr == 2099) {
                    $this->db->query("EXEC spInsertAddressData " . $iAddressNbr . ", 5, 'BIC', '" . $Value . "', '',1");
                    return (object) array(
                        'result' => 'success'
                    );
                } elseif ($iTypeNbr == 494) {
                    $this->db->query("EXEC spInsertAddressData " . $iAddressNbr . ", 7, 'BIRTHDATE', '" . date("d/m/Y", strtotime($Value)) . "', '',1");
                    return (object) array(
                        'result' => 'success'
                    );
                }
                return (object) array(
                    'result' => 'error'
                );
            }
        } else {
            return (object) array(
                'result' => 'error',
                'message' => 'Empty ' . $iAddressNbr
            );
        }
    }
    public function getInvoiceDetail($iInvoiceNbr)
    {
        $q = $this->db->query("select a.iInvoiceGroupNbr,b.cInvoiceDetailDescription from tblInvoiceDetail a left join tblInvoiceDetailDescription b on b.iInvoiceDetailDescriptionNbr=a.iInvoiceDetailDescriptionNbr where a.iInvoiceNbr=?", array(
            $iInvoiceNbr
        ));
        return $q->result();
    }
    public function updateInvoiceZone($iAddressNbr, $customerType)
    {
        if ($iAddressNbr > 0) {
            $this->db->query("Update tblInvoiceCondition set iCustomerOrigin = ? where iAddressNbr = ?", array(
                $customerType,
                $iAddressNbr
            ));
            if ($this->db->affected_rows() > 0) {
                return (object) array(
                    'result' => 'success'
                );
            } else {
                return (object) array(
                    'result' => 'error'
                );
            }
        } else {
            return (object) array(
                'result' => 'error',
                'message' => 'Empty ' . $iAddressNbr
            );
        }
    }
    public function UpdateInsertInvoiceConditionEmail($iAddressNbr, $invoice_email)
    {
        if ($iAddressNbr > 0) {
            $this->db->query("exec spUpdateInsertInvoiceConditionEmail " . $iAddressNbr . ", '" . $invoice_email . "'");
            if ($this->db->affected_rows() > 0) {
                return (object) array(
                    'result' => 'success'
                );
            } else {
                return (object) array(
                    'result' => 'error'
                );
            }
        } else {
            return (object) array(
                'result' => 'error',
                'message' => 'Empty ' . $iAddressNbr
            );
        }
    }
    public function ProcessPricing($mobile, $iAddressNbr, $addonid, $createMagebo, $recurring = false, $contract = false)
    {
        $addon  = getMageboProductAddons($addonid);
        log_message('error', "Zddon for add Bundle".print_r($addon, true));
        $domain = trim($mobile->details->msisdn);
        if (!$contract) {
            $contract = $mobile->date_contract;
        }
        if (!$recurring) {
            $prorata = "0";
        } else {
            $prorata = 1;
        }
        //send this via api because php7.X does not support it
        if ($createMagebo) {
            if (substr($domain, 0, 2) == "32") {
                $this->AddBundlePricing(array(
                    'number' => substr(trim($mobile->details->msisdn), 2),
                    'date_contract' => $contract
                ), $addonid);
            } else {
                $this->AddBundlePricing(array(
                    'number' => $mobile->details->msisdn,
                    'date_contract' => $contract
                ), $addonid);
            }
        }
        if (substr($domain, 0, 2) == "32") {
            $iPincode = substr($domain, 2);
            $number   = "0" . substr($iPincode, 0, 3) . '/' . substr($iPincode, 3, 2) . "." . substr($iPincode, 5, 2) . "." . substr($iPincode, 7, 2);
        } else {
            $number = "0" . $domain[2] . "-" . $domain[3] . $domain[4] . " " . $domain[5] . "" . $domain[6] . " " . $domain[7] . "" . $domain[8] . " " . $domain[9] . $domain[10];
        }
        $prorata = "0";
        if (!$recurring) {
            $recurring = 1;
        } else {
            $datequery = "SELECT SD.InvoiceStartDate,
                                    CONCAT(SUBSTR(SD.InvoiceStartDate,6,2),'/',SUBSTR(SD.InvoiceStartDate,9,2),'/',SUBSTR(SD.InvoiceStartDate,1,4)) ContractDate,
                                    STR_TO_DATE(CONCAT(SUBSTR(SD.InvoiceStartDate,6,2),'/',SUBSTR(SD.InvoiceStartDate,9,2),'/',SUBSTR(SD.InvoiceStartDate,1,4)),'%m/%d/%Y' ) CtrDate
                                    ,LAST_DAY(STR_TO_DATE(CONCAT(SUBSTR(SD.InvoiceStartDate,6,2),'/',SUBSTR(SD.InvoiceStartDate,9,2),'/',SUBSTR(SD.InvoiceStartDate,1,4)),'%m/%d/%Y' )) Lday,
                                    DATEDIFF(LAST_DAY(STR_TO_DATE(CONCAT(SUBSTR(SD.InvoiceStartDate,6,2),'/',SUBSTR(SD.InvoiceStartDate,9,2),'/',SUBSTR(SD.InvoiceStartDate,1,4)),'%m/%d/%Y' )),
                                    STR_TO_DATE(CONCAT(SUBSTR(SD.InvoiceStartDate,6,2),'/',SUBSTR(SD.InvoiceStartDate,9,2),'/',SUBSTR(SD.InvoiceStartDate,1,4)),'%m/%d/%Y' ) )diff,
                                    DAY(LAST_DAY(STR_TO_DATE(CONCAT(SUBSTR(SD.InvoiceStartDate,6,2),'/',SUBSTR(SD.InvoiceStartDate,9,2),'/',SUBSTR(SD.InvoiceStartDate,1,4)),'%m/%d/%Y' ))) DaysInMonth,
                                    MONTH(STR_TO_DATE(CONCAT(SUBSTR(SD.InvoiceStartDate,6,2),'/',SUBSTR(SD.InvoiceStartDate,9,2),'/',SUBSTR(SD.InvoiceStartDate,1,4)),'%m/%d/%Y' )) StartMonth,
                                    YEAR(STR_TO_DATE(CONCAT(SUBSTR(SD.InvoiceStartDate,6,2),'/',SUBSTR(SD.InvoiceStartDate,9,2),'/',SUBSTR(SD.InvoiceStartDate,1,4)),'%m/%d/%Y' )) StartYear
                                    FROM
                                    (SELECT '" . $contract . "' InvoiceStartDate )SD ";
            $ct        = explode('-', $contract);
            $resultnew = $this->db1->query($datequery);
            $row       = $resultnew->row_array();
            //$PricingAmount =  $addon->recurring_subtotal;
            $prorata   = round($addon->recurring_subtotal * (($row['diff'] + 1) / $row['DaysInMonth']), 4);
            $recurring = 600;
        }
        if ($addon->bundle_type == "option") {
            $prorata = "0";
            log_message('error', "add pricing".print_r(array($iAddressNbr, $addon->GPInvoiceGroupNbr, 1, $addon->recurring_subtotal, $recurring, $addon->GPcRemark . ' ' . $number . ' - ' . date('d/m/Y'), date('m/d/Y'), date('m'), date('Y'), 1, 0, $addon->GPcRemark . ' ' . $number . ' - ' . date('d/m/Y'), $addon->GPcRemark . ' ' . $number . ' - ' . date('d/m/Y'), $prorata), 1));
            $x       = $this->x_magebosubscription_addPricing($iAddressNbr, $addon->GPInvoiceGroupNbr, 1, $addon->recurring_subtotal, $recurring, $addon->GPcRemark . ' ' . $number . ' - ' . date('d/m/Y'), date('m/d/Y'), date('m'), date('Y'), 1, 0, $addon->GPcRemark . ' ' . $number . ' - ' . date('d/m/Y'), $addon->GPcRemark . ' ' . $number . ' - ' . date('d/m/Y'), $prorata);
            log_message('error', "Adding Pricing for AddBundle ".print_r($x, true));
            return $x;
        } elseif ($addon->bundle_type == "others") {
        } else {
            insert_query_log(array(
                "funct" => "x_magebosubscription_adpricing",
                "description" => $iAddressNbr . "," . $addon->GPInvoiceGroupNbr . ", 1, " . $addon->recurring_subtotal . ", " . $recurring . ", " . $addon->GPcRemark . " " . $number . ", " . $ct[1] . '/' . $ct[2] . '/' . $ct[0] . ", " . $ct[1] . "," . $ct[0] . ", 1, 0, " . $addon->GPcRemark . " " . $number . ", " . $addon->GPcRemark . " " . $number . ", " . $prorata
            ));
            /*
            $x =  $this->x_magebosubscription_addPricing($iAddressNbr, $addon->GPInvoiceGroupNbr, 1, $addon->recurring_subtotal, $recurring, $addon->GPcRemark . ': ' . $number, $ct[1].'/'.$ct[2].'/'.$ct[0], $ct[1], $ct[0], 1, 0, $addon->GPcRemark . ': ' . $number, $addon->GPcRemark . ' ' . $number, $prorata, 1);
            */
            /* $this->x_magebosubscription_addPricing($iAddressNbr,
            $addon->GPInvoiceGroupNbr,
            1,
            $addon->recurring_subtotal,
            600,
            $addon->GPcRemark . ' ' . $number . ' ' . date('d/m/Y'),
            date('m/d/Y'),
            date('m'),
            date('Y'),
            1,
            0,
            $addon->GPcRemark . ' ' . $number . ' ' . date('d/m/Y'),
            $addon->GPcRemark . ' ' . $number . ' ' . date('d/m/Y'),
            0);
            */
        }
    }
    public function cancel_pincode($pin)
    {
        if (substr($pin, 0, 2) == "32") {
            $pin = substr(trim($pin), 2);
        }
        $remark = "Order Cancelled " . date('d/m/Y H:i:s');
        $this->db->query("Update tblC_Pincode set dStopDate = dContractDate, cRemark = ? where iPincode = ?", array(
            $remark,
            $pin
        ));
        $this->db->query("Update tblC_Pincode set dStopDate = dContractDate, cRemark = ? where iPincode in ( select iMSISDN from tblC_SIMCard where iPincode = ? )", array(
            $remark,
            $pin
        ));
    }
    public function ProcessPricingExtraBundle($mobile, $iAddressNbr, $addonid, $createMagebo, $price_incl_vat, $contract_start, $recurring, $disablepriceditems=false)
    {
        $client = getclient($mobile->userid);
        log_message('error', "ProcessPricingExtraBundle".print_r(array($mobile, $iAddressNbr, $addonid, $createMagebo, $price_incl_vat, $contract_start, $recurring), true));
        $addon = getAddonInformation($addonid);
        if (substr($domain, 0, 2) == "32") {
            $domain = trim($mobile->details->msisdn);
        } else {
            $domain = trim($mobile->details->msisdn);
        }
        //send this via api because php7.X does not support it
        if ($createMagebo) {
            if (substr($domain, 0, 2) == "32") {
                $this->AddBundlePricing(array(
                    'number' => substr(trim($mobile->details->msisdn), 2),
                    'date_contract' => $contract_start[1] . '-' . $contract_start[2] . '-' . $contract_start[0]
                ), $addonid);
            } else {
                $this->AddBundlePricing(array(
                    'number' => $mobile->details->msisdn,
                    'date_contract' => $contract_start[1] . '-' . $contract_start[2] . '-' . $contract_start[0]
                ), $addonid);
            }
        }
        if (substr($domain, 0, 2) == "32") {
            $iPincode = substr($domain, 2);
            $number   = "0" . substr($iPincode, 0, 3) . '/' . substr($iPincode, 3, 2) . "." . substr($iPincode, 5, 2) . "." . substr($iPincode, 7, 2);
        } else {
            $number = "0" . $domain[2] . "-" . $domain[3] . $domain[4] . " " . $domain[5] . "" . $domain[6] . " " . $domain[7] . "" . $domain[8] . " " . $domain[9] . $domain[10];
        }
        $datequery = "SELECT SD.InvoiceStartDate,
                                    CONCAT(SUBSTR(SD.InvoiceStartDate,6,2),'/',SUBSTR(SD.InvoiceStartDate,9,2),'/',SUBSTR(SD.InvoiceStartDate,1,4)) ContractDate,
                                    STR_TO_DATE(CONCAT(SUBSTR(SD.InvoiceStartDate,6,2),'/',SUBSTR(SD.InvoiceStartDate,9,2),'/',SUBSTR(SD.InvoiceStartDate,1,4)),'%m/%d/%Y' ) CtrDate
                                    ,LAST_DAY(STR_TO_DATE(CONCAT(SUBSTR(SD.InvoiceStartDate,6,2),'/',SUBSTR(SD.InvoiceStartDate,9,2),'/',SUBSTR(SD.InvoiceStartDate,1,4)),'%m/%d/%Y' )) Lday,
                                    DATEDIFF(LAST_DAY(STR_TO_DATE(CONCAT(SUBSTR(SD.InvoiceStartDate,6,2),'/',SUBSTR(SD.InvoiceStartDate,9,2),'/',SUBSTR(SD.InvoiceStartDate,1,4)),'%m/%d/%Y' )),
                                    STR_TO_DATE(CONCAT(SUBSTR(SD.InvoiceStartDate,6,2),'/',SUBSTR(SD.InvoiceStartDate,9,2),'/',SUBSTR(SD.InvoiceStartDate,1,4)),'%m/%d/%Y' ) )diff,
                                    DAY(LAST_DAY(STR_TO_DATE(CONCAT(SUBSTR(SD.InvoiceStartDate,6,2),'/',SUBSTR(SD.InvoiceStartDate,9,2),'/',SUBSTR(SD.InvoiceStartDate,1,4)),'%m/%d/%Y' ))) DaysInMonth,
                                    MONTH(STR_TO_DATE(CONCAT(SUBSTR(SD.InvoiceStartDate,6,2),'/',SUBSTR(SD.InvoiceStartDate,9,2),'/',SUBSTR(SD.InvoiceStartDate,1,4)),'%m/%d/%Y' )) StartMonth,
                                    YEAR(STR_TO_DATE(CONCAT(SUBSTR(SD.InvoiceStartDate,6,2),'/',SUBSTR(SD.InvoiceStartDate,9,2),'/',SUBSTR(SD.InvoiceStartDate,1,4)),'%m/%d/%Y' )) StartYear
                                    FROM
                                    (SELECT '" . $contract_start[0] . '-' . $contract_start[1] . '-' . $contract_start[2] . "' InvoiceStartDate )SD ";
        $resultnew = $this->db1->query($datequery);
        $row       = $resultnew->row_array();
        $prorata   = 0;
        $total_amount =  exvat4($client->vat_rate, $price_incl_vat);
        $prorata   = round($total_amount * (($row['diff'] + 1) / $row['DaysInMonth']), 4);

        if ($contract_start[2] == '01') {
            $disablepriceditems = true;
        } else {
            $disablepriceditems = false;
        }
        //mail("mail@simson.one", "pricing", print_r($datequery, true) . print_r($addon, true) . $prorata . print_r($row, true));
        $xx = $this->x_magebosubscription_addPricing($iAddressNbr, $addon->GPInvoiceGroupNbr, 1, exvat4('21', $price_incl_vat), $recurring, $addon->GPcRemark . ' ' . $number, $contract_start[1] . '/' . $contract_start[2] . '/' . $contract_start[0], $contract_start[1], $contract_start[0], 1, 1, $addon->GPcRemark . ' ' . $number, $addon->GPcRemark . ' ' . $number, $prorata, false, $disablepriceditems);
        log_message('error', "AddPricing ".print_r(array($iAddressNbr, $addon->GPInvoiceGroupNbr, 1, exvat4('21', $price_incl_vat), $recurring, $addon->GPcRemark . ' ' . $number, $contract_start[1] . '/' . $contract_start[2] . '/' . $contract_start[0], $contract_start[1], $contract_start[0], 1, 1, $addon->GPcRemark . ' ' . $number, $addon->GPcRemark . ' ' . $number, $prorata), true));
        log_message('error', 'PricingID'.print_r($xx, true));
        return $xx;
    }
    public function updateProductOwnerCompany($id, $typeid)
    {
        $this->db->where('iGeneralPricingIndex', $id);
        $this->db->update('tblGeneralPricing', array(
            'iInvoiceGroupNbr' => $typeid
        ));
    }
    public function addExtraPricing($client, $option, $contract, $mobile = false)
    {
        if (!empty($option->GPInvoiceGroupNbr)) {
            $groupnumber = $option->GPInvoiceGroupNbr;
        } else {
            $groupnumber = 369;
        }
        $id             = $this->x_magebosubscription_addPricing($client->mageboid, $groupnumber, 1, exvat4($client->vat_rate, $option->recurring_total), $option->terms, $option->name, $contract, substr($contract, 0, 2), substr($contract, -4), 1, 0, $option->name, $option->name, 1);
        $contract_start = explode('-', $contract);
        if (substr($mobile->details->msisdn, 0, 2) == "32") {
            $this->AddBundlePricing(array(
                'number' => substr(trim($mobile->details->msisdn), 2),
                'date_contract' => $contract_start[1] . '-' . $contract_start[2] . '-' . $contract_start[0]
            ), $addonid);
        } else {
            $this->AddBundlePricing(array(
                'number' => $mobile->details->msisdn,
                'date_contract' => $contract_start[1] . '-' . $contract_start[2] . '-' . $contract_start[0]
            ), $addonid);
        }
        return $id;
    }
    public function addSetupFee($mobile)
    {
        $setupfee = getPricing($mobile->packageid, 'setup');
        if ($setupfee > 0) {
        }
    }
    public function getClientInvoices($mageboid)
    {
    }
    public function ChangeSimLanguage($msisdn, $lang)
    {
        //$lang (1 - English, 2 - French, 3 - Dutch )
        if ($lang == "3") {
            // Dutch
            $langid = 592;
        } elseif ($lang == "2") {
            // French
            $langid = 593;
        } else {
            // English
            $langid = 594;
        }
        $this->db->where('iPincode', trim($msisdn));
        $this->db->update('tblC_SIMCard', array(
            'iVoiceMailLanguage' => $langid
        ));
        return $this->db->affected_rows();
    }
    public function getInvoiceId($id)
    {
        $result             = $this->getInvoicebyId($id);
        $result['items']    = $this->getInvoiceDetail($id);
        $result['payments'] = $this->getPaymentListInvoice($id);
        return $result;
    }
    public function getInvoicebyId($id)
    {
        $q = $this->db->query("select a.iInvoiceType,a.iInvoiceNbr,CONVERT(char(10), a.dInvoiceDate,126) as dInvoiceDate,CONVERT(char(10), a.dInvoiceDueDate,126) as dInvoiceDueDate, case when a.iInvoiceStatus  = '53' THEN 'Sepa Presented'
       when a.iInvoiceStatus = '54' then 'Paid' else 'Unpaid' end as  iInvoiceStatus  ,b.cName,a.mInvoiceAmount,a.iAddressNbr
    FROM tblInvoice a
    left join tblAddress b on b.iAddressNbr=a.iAddressNbr
    where a.iInvoiceNbr=? ", array(
            $id
        ));
        return $q->row_array();
    }
    public function TerminateRequest($service, $date)
    {
        $query = "execute spInsertSIMCardLog '" . $service->details->msisdn_sim . "', 600, NULL, NULL, NULL, NULL, '', '" . $date . "'";
        $this->db->query($query); //-- Termination request
    }
    public function CancelTerminateRequest($service)
    {
        $this->db->query("delete from tblC_SimCardLog where cSIMCardNbr=? and iSIMCardLog=?", array(
            $service->details->msisdn_sim,
            600
        ));
    }
    public function TerminateComplete($service, $date)
    {
        sleep(2);
        if (substr($service->details->msisdn, 0, 2) == "32") {
            $pin = substr(trim($service->details->msisdn), 2);
        } else {
            $pin = $service->details->msisdn;
        }
        $date1 = $date . ' ' . date('H-i-s');
        $date2 = str_replace('-', '', $date);
        $this->addSimcardLog(trim($pin), '599');

        sleep(2);
        $this->addSimcardLog(trim($pin), '612');


        // $this->db->query("execute spInsertSIMCardLog '" . $service->details->msisdn_sim . "', 613, NULL, NULL, NULL, NULL, '', '" . $date2 . "'"); //-- Terminated")
        $this->db->query("update tblC_Pincode set dStopDate = NULL where iPincode = ?", array(
            $pin
        ));
        if ($this->db->affected_rows() > 0) {
            $remark = "Msisdn Re-activation Request " . date('d/m/Y H:i:s');
            $this->db->query("Update tblC_Pincode set dStopDate = ?, cRemark = ? where iPincode in ( select iMSISDN from tblC_SIMCard where iPincode = ? )", array(
                null,
                $remark,
                $pin
            ));
            return array(
                'result' => 'success'
            );
        } else {
            return array(
                'result' => 'error'
            );
        }
    }

    public function RemoveTermination($service, $date)
    {
        sleep(2);
        if (substr($service->details->msisdn, 0, 2) == "32") {
            $pin = substr(trim($service->details->msisdn), 2);
        } else {
            $pin = $service->details->msisdn;
        }
        $date1 = $date . ' ' . date('H-i-s');
        $date2 = str_replace('-', '', $date);
        $this->db->query("execute spInsertSIMCardLog '" . $service->details->msisdn_sim . "', 613, NULL, NULL, NULL, NULL, '', '" . $date2 . "'"); //-- Terminated")
        $this->db->query("update tblC_Pincode set dStopDate = ? where iPincode = ?", array(
            $date2,
            $pin
        ));
        if ($this->db->affected_rows() > 0) {
            $remark = "Order Terminated " . date('d/m/Y H:i:s');
            $this->db->query("Update tblC_Pincode set dStopDate = ?, cRemark = ? where iPincode in ( select iMSISDN from tblC_SIMCard where iPincode = ? )", array(
                date('m-d-Y'),
                $remark,
                $pin
            ));
            return array(
                'result' => 'success'
            );
        } else {
            return array(
                'result' => 'error'
            );
        }
    }
    public function getGeneralPricing($iAddressNbr, $pin)
    {
        $q = $this->db->query("SELECT * FROM tblGeneralPricing where cInvoiceDescription like ? and iAddressNbr=?", array(
            '%' . $pin . '%',
            $iAddressNbr
        ));
        if ($q->num_rows() > 0) {
            return $q->row();
        } else {
            return false;
        }
    }
    public function updateCRemarks($id, $data, $mobile = false)
    {
        $pending = false;
        if (!empty($mobile->promo_name)) {
            $replace = true;
        } else {
            $replace = false;
        }
        if ($data->msisdn_type == "porting") {
            if ($data->msisdn_status == "Active") {
                $oldpin = $data->msisdn;
                $pin    = $data->msisdn;
            } else {
                $pending = true;
                $oldpin  = $data->msisdn;
                $pin     = $data->msisdn_sn;
            }
        } else {
            $pin = $data->msisdn_sn;
        }
        if (substr(trim($pin), 0, 2) == "32") {
            $iPincode   = substr($pin, 2);
            $pincoderef = "0" . substr($iPincode, 0, 3) . '/' . substr($iPincode, 3, 2) . "." . substr($iPincode, 5, 2) . "." . substr($iPincode, 7, 2);
        } else {
            $pincoderef = "0" . $pin[2] . "-" . $pin[3] . $pin[4] . " " . $pin[5] . "" . $pin[6] . " " . $pin[7] . "" . $pin[8] . " " . $pin[9] . $pin[10];
        }
        $cRemark             = sprintf($data->GPcRemark, $pincoderef);
        $cInvoiceDescription = sprintf($data->iInvoiceDescription, $pincoderef);
        if ($pending) {
            if ($replace) {
                $oldpincoderef           = "0" . $oldpin[2] . "-" . $oldpin[3] . $oldpin[4] . " " . $oldpin[5] . "" . $oldpin[6] . " " . $oldpin[7] . "" . $oldpin[8] . " " . $oldpin[9] . $oldpin[10];
                $oldcRemarkx             = $mobile->promo_name . " " . $oldpincoderef;
                $oldcInvoiceDescriptionx = lang($mobile->promo_name) . " " . $oldpincoderef;
                $newcRemarkx             = $mobile->promo_name . " " . $pincoderef;
                $newcInvoiceDescriptionx = lang($mobile->promo_name) . " " . $pincoderef;
                $s                       = "Update tblGeneralPricing  Set    cInvoiceDescription = replace(cInvoiceDescription, '" . $oldcInvoiceDescriptionx . "', '" . $newcInvoiceDescriptionx . "'), cRemark = replace(cRemark, '" . $oldcRemarkx . "', '" . $newcRemarkx . "') where cInvoiceDescription='" . $oldcInvoiceDescriptionx . "'";
                $r                       = "Update tblGeneralPricing  Set  cRemark = replace(cRemark, '" . $oldcRemarkx . "', '" . $newcRemarkx . "') where cRemark='" . $oldcRemarkx . "'";
                if ($data->msisdn_status != "Active") {
                    $this->db->query($r);
                    $this->db->query($s);
                }
            }
        }
        $this->db->query("update tblGeneralPricing set cInvoiceDescription = '" . $cInvoiceDescription . "', cRemark = '" . $cRemark . "'
                    where iGeneralPricingIndex = ?", array(
            $id
        ));
        return array(
            $oldcRemarkx,
            $newcRemarkx
        );
    }
    public function addPricingSubscription($mobile, $noDiscount = false, $porting = false, $activateSNOnly = false)
    {
        $setting = globofix($mobile->companyid);
        if ($mobile->iGeneralPricingIndex) {
            // mail('mail@simson.one', 'double pricing detected, request cancelled', print_r($mobile, true));
            return 'error';
        }
        $mp = array();
        if (substr(trim($mobile->details->msisdn), 0, 2) == "32") {
            $q    = $this->db->query("select * from tblC_Pincode where iPincode LIKE ?", array(
                substr(trim($mobile->details->msisdn), 2)
            ));
            $PINC = substr(trim($mobile->details->msisdn), 2);
            $pin  = $PINC;
        } else {
            if ($activateSNOnly) {
                $pin = $mobile->details->msisdn_sn;
                $q   = $this->db->query("select * from tblC_Pincode where iPincode LIKE ?", array(
                    trim($mobile->details->msisdn_sn)
                ));
            } else {
                $pin = $mobile->details->msisdn;
                $q   = $this->db->query("select * from tblC_Pincode where iPincode LIKE ?", array(
                    trim($mobile->details->msisdn)
                ));
            }
            if ($porting) {
                $PINC = trim($mobile->details->msisdn);
            } else {
                $PINC = trim($mobile->details->msisdn_sn);
            }
        }
        $lineIGD    = $q->row_array();
        $pincoderef = $lineIGD['cInvoiceReference'];
        if (substr(trim($mobile->details->msisdn), 0, 2) == "32") {
            $iPincode = substr($pin, 2);
        } else {
            $pincoderef = "0" . $pin[2] . "-" . $pin[3] . $pin[4] . " " . $pin[5] . "" . $pin[6] . " " . $pin[7] . "" . $pin[8] . " " . $pin[9] . $pin[10];
        }
        $sstartday           = substr($mobile->date_contract, 0, 2);
        $product             = getMageboProduct($mobile->packageid);
        $InvoiceGroup        = $product->iInvoiceGroupNbr;
        $d                   = explode('-', $mobile->date_contract);
        $datequery           = "SELECT SD.InvoiceStartDate,
                                    CONCAT(SUBSTR(SD.InvoiceStartDate,6,2),'/',SUBSTR(SD.InvoiceStartDate,9,2),'/',SUBSTR(SD.InvoiceStartDate,1,4)) ContractDate,
                                    STR_TO_DATE(CONCAT(SUBSTR(SD.InvoiceStartDate,6,2),'/',SUBSTR(SD.InvoiceStartDate,9,2),'/',SUBSTR(SD.InvoiceStartDate,1,4)),'%m/%d/%Y' ) CtrDate
                                    ,LAST_DAY(STR_TO_DATE(CONCAT(SUBSTR(SD.InvoiceStartDate,6,2),'/',SUBSTR(SD.InvoiceStartDate,9,2),'/',SUBSTR(SD.InvoiceStartDate,1,4)),'%m/%d/%Y' )) Lday,
                                    DATEDIFF(LAST_DAY(STR_TO_DATE(CONCAT(SUBSTR(SD.InvoiceStartDate,6,2),'/',SUBSTR(SD.InvoiceStartDate,9,2),'/',SUBSTR(SD.InvoiceStartDate,1,4)),'%m/%d/%Y' )),
                                    STR_TO_DATE(CONCAT(SUBSTR(SD.InvoiceStartDate,6,2),'/',SUBSTR(SD.InvoiceStartDate,9,2),'/',SUBSTR(SD.InvoiceStartDate,1,4)),'%m/%d/%Y' ) )diff,
                                    DAY(LAST_DAY(STR_TO_DATE(CONCAT(SUBSTR(SD.InvoiceStartDate,6,2),'/',SUBSTR(SD.InvoiceStartDate,9,2),'/',SUBSTR(SD.InvoiceStartDate,1,4)),'%m/%d/%Y' ))) DaysInMonth,
                                    MONTH(STR_TO_DATE(CONCAT(SUBSTR(SD.InvoiceStartDate,6,2),'/',SUBSTR(SD.InvoiceStartDate,9,2),'/',SUBSTR(SD.InvoiceStartDate,1,4)),'%m/%d/%Y' )) StartMonth,
                                    YEAR(STR_TO_DATE(CONCAT(SUBSTR(SD.InvoiceStartDate,6,2),'/',SUBSTR(SD.InvoiceStartDate,9,2),'/',SUBSTR(SD.InvoiceStartDate,1,4)),'%m/%d/%Y' )) StartYear
                                    FROM
                                    (SELECT '" . $d[2] . '-' . $d[0] . '-' . $d[1] . "' InvoiceStartDate )SD ";
        $resultnew           = $this->db1->query($datequery);
        $row                 = $resultnew->row_array();
        //logActivity($datequery);
        $ContractDate        = $row['ContractDate'];
        $iStartMonth         = $row['StartMonth'];
        $iStartYear          = $row['StartYear'];


        $client              = $this->CI->Admin_model->getClient($mobile->userid);
        $GPcRemark           = $product->GPcRemark;
        $InvoiceDescription  = $product->iInvoiceDescription;
        $cRemark             = sprintf("$GPcRemark", $pincoderef);
        $cInvoiceDescription = sprintf("$InvoiceDescription", $pincoderef);
        $cInvoiceReference   = $pincoderef;
        //Now we create the monthly subscription
        $bEnabled            = 1;
        $bussiness           = $this->getInvoiceBussiness($InvoiceGroup);
        $cBusiness           = $bussiness['cBusiness'];
        $cInvoiceGroup       = $bussiness['cInvoiceGroup'];
        $iAddressNbr         = $client->mageboid;
        //add new function to double check if pricing not exist 2019-05-09
        if ($this->getGeneralPricing($iAddressNbr, $PINC)) {
            //mail('mail@simson.one', 'getGeneralPricing', 'pricing existing: Magebo.php like 1362' . print_r($mobile, true));
            return 'error';
        }
        //h.promo_name,h.promo_description,h.promo_duration,h.id as promo_id,
        $client = getClientDetailidbyMagebo($iAddressNbr);
        if ($client->vat_exempt == 1) {
            $vat = 0;
        } else {
            $vat = round($client->vat_rate, 0);
        }
        if ($client->language) {
            $this->CI->config->set_item('language', $client->language);
        } else {
            $this->CI->config->set_item('language', 'dutch');
        }
        $this->CI->lang->load('admin');
        $PricingAmount = exvat4($vat, $mobile->recurring);
        if ($mobile->addons) {
            foreach ($mobile->addons as $addn) {
                if ($addn->recurring_total > 0) {
                    $others = "Hardware - " . $pincoderef;
                    if (!$noDiscount) {
                        $this->x_magebosubscription_addPricing($client->mageboid, $InvoiceGroup, 1, exvat($vat, $addn->recurring_total), $addn->terms, $addn->name, $ContractDate, $iStartMonth, $iStartYear, $bEnabled, 0, $others, $addn->name, 0);
                    }
                    unset($others);
                }
            }
        }
        //    $mp=magebosubscription_addPricing($serviceid,$iAddressNbr,$InvoiceGroup,1,$PricingAmount,600,$cInvoiceReference,$ContractDate,$iStartMonth,$iStartYear,$bEnabled,0,$cRemark);
        //If the Subscription Start is the first day of the
        //month then there is no need for prorata
        if ($d[1] == "01") {
            //logActivity('No Prorata - first of te month');
            $mp[] = $this->x_magebosubscription_addPricing($client->mageboid, $InvoiceGroup, 1, $PricingAmount, 600, $cInvoiceReference, $ContractDate, $iStartMonth, $iStartYear, $bEnabled, 0, trim($cRemark), trim($cInvoiceDescription), 1);
        } else {
            //Here we create the pricing / priced items for this
            //logActivity('!Prorata !!!');
            $prorata = round($PricingAmount * (($row['diff'] + 1) / $row['DaysInMonth']), 4);
            //logActivity('Prorata Amount : ' . $prorata);
            //First the ProRata piece
            if ($setting->Prorata != "1") {
                $prorata = "0";
            }
            $mp[] = $this->x_magebosubscription_addPricing($client->mageboid, $InvoiceGroup, 1, $PricingAmount, 600, $cInvoiceReference, $ContractDate, $iStartMonth, $iStartYear, $bEnabled, 1, trim($cRemark), trim($cInvoiceDescription), $prorata);
        }
        if ($mobile->setup > 0) {
            if (trim($mobile->notes) != 'Imported from KPN') {
                if ($this->companyid == 54) {
                    if ($mobile->regdate < '2019-03-01') {
                        $setupRemark       = "Activation - " . $pincoderef;
                        $setupRemakInvoice = "Activation - " . $pincoderef;
                        $setup             = exvat4($vat, $mobile->setup);
                        if (!$noDiscount) {
                            $this->addPricingRemote("exec spInsertgeneralPricing $iAddressNbr,'$cBusiness','Activation',1,$setup,$vat,1,1,'$ContractDate',$iStartMonth,$iStartYear,$bEnabled,'$setupRemark','NO REPORTING','$setupRemakInvoice',0");
                        }
                    } elseif ($mobile->regdate >= '2019-03-12') {
                        if ($mobile->future_activation > 0) {
                            $setupRemark       = "Activation - " . $pincoderef;
                            $setupRemakInvoice = "Activation - " . $pincoderef;
                            $setup             = exvat4($vat, $mobile->future_activation);
                            if (!$noDiscount) {
                                $this->addPricingRemote("exec spInsertgeneralPricing $iAddressNbr,'$cBusiness','Activation',1,$setup,$vat,1,1,'$ContractDate',$iStartMonth,$iStartYear,$bEnabled,'$setupRemark','NO REPORTING','$setupRemakInvoice',0");
                            }
                        }
                    }
                } else {
                    $setupRemark       = "Activation - " . $pincoderef;
                    $setupRemakInvoice = "Activation - " . $pincoderef;
                    $setup             = exvat4($vat, $mobile->setup);
                    if (!$noDiscount) {
                        $this->addPricingRemote("exec spInsertgeneralPricing $iAddressNbr,'$cBusiness','Activation',1,$setup,$vat,1,1,'$ContractDate',$iStartMonth,$iStartYear,$bEnabled,'$setupRemark','NO REPORTING','$setupRemakInvoice',0");
                    }
                }
            }
        }
        if (!empty($mobile->promocode)) {
            $promoIndex           = false;
            $iTotalMonths         = $mobile->promo_duration;
            $mUnitPrice           = exvat4($client->vat_rate, $mobile->promo_value) * -1;
            $cRemarkx             = $mobile->promo_name;
            $cInvoiceDescriptionx = lang($mobile->promo_name) . " " . $pincoderef;
            if (!$noDiscount) {
                $prorata_disc = round($mUnitPrice * (($row['diff'] + 1) / $row['DaysInMonth']), 4);
                //logActivity('Prorata Amount : ' . $prorata); mekona
                //First the ProRata piece
                log_message('error', 'Prorata Log: ');
                if ($setting->Prorata != "1") {
                    $prorata_disc = "0";
                }

                $promoIndex = $this->addPricingRemote("exec spInsertgeneralPricing $iAddressNbr,'$cBusiness','Discount Next Month',1,$mUnitPrice,$vat,1,$iTotalMonths,'$ContractDate',$iStartMonth,$iStartYear,$bEnabled,'$cRemarkx','NO REPORTING','$cInvoiceDescriptionx',1");
            }
            // $mp[]                 = $promoIndex;
            if ($promoIndex) {
                if (in_array($mobile->companyid, array("53","56"))) {
                    $toto = "exec spInsertPricedItems " . $promoIndex . ", " . $iStartMonth . ", " . $iStartYear . ", 1, " . $prorata_disc . ", " . $vat . ", 1";
                    insert_query_log(array(
                        'funct' => 'x_magebosubscription_addPricing',
                        'description' => $toto
                    ));
                    $this->db->query($toto);


                    $this->db->query("update tblGeneralPricing set cInvoiceDescription = '" . $cInvoiceDescriptionx . "', cRemark = '" . $cInvoiceDescriptionx . "'
                    where iGeneralPricingIndex = ?", array(
                        $promoIndex
                    ));
                }
            }
            //log the query
            insert_query_log(array(
                'funct' => 'x_magebosubscription_addPricing->Discount',
                'description' => "exec spInsertgeneralPricing $iAddressNbr,'$cBusiness','Discount Next Month',1,$mUnitPrice,$vat,1,$iTotalMonths,'$ContractDate',$iStartMonth,$iStartYear,$bEnabled,'$cRemarkx','NO REPORTING','$cInvoiceDescriptionx',1"
            ));
        }
        if (!empty($mp)) {
            if (!empty($mp)) {
                $this->update_generalPricing($mobile->id, implode(',', array_filter($mp)));
            }
            return implode(',', array_filter($mp));
        } else {
            return "success";
        }
    }

    public function ExecuteQuery($query1)
    {
        $promoIndex = $this->addPricingRemote($query1);

        // $mp[]                 = $promoIndex;
        return $promoIndex;
    }

    public function ExecuteQuery2($query2)
    {
        $this->db->query($query2);
    }
    public function checkInvoiceNexmonth($IndexId, $startdate)
    {
        $this->db->query("delete from tblPricedItems
where iGeneralPricingIndex = ?
AND iInvoiceNbr is null
AND cast(case when iMonth < 10 then '0'+cast(iMonth as varchar) else cast(iMonth as varchar) end + '/01/' + cast(iYear as varchar) as datetime)
>= ?", array(
            $IndexId,
            $startdate
        ));
    }
    public function addNextMonthInvoiceItems($client, $data)
    {
        $product           = getMageboProduct($mobile->packageid);
        $InvoiceGroup      = $product->iInvoiceGroupNbr;
        $bussiness         = $this->getInvoiceBussiness($InvoiceGroup);
        $cBusiness         = $bussiness['cBusiness'];
        $ContractDate      = $row['ContractDate'];
        $iStartMonth       = $row['StartMonth'];
        $iStartYear        = $row['StartYear'];
        $setupRemark       = "Activation - " . $pincoderef;
        $setupRemakInvoice = "Activation - " . $pincoderef;
        $setup             = exvat4($vat, $mobile->setup);
        $this->addPricingRemote("exec spInsertgeneralPricing $client->mageboid,'$cBusiness','Activation',1,$setup,$vat,1,1,'$ContractDate',$iStartMonth,$iStartYear,1,'$setupRemark','NO REPORTING','$setupRemakInvoice',0");
    }
    public function update_generalPricing($id, $index)
    {
        $this->db1->where('id', $id);
        $this->db1->update("a_services", array(
            'iGeneralPricingIndex' => $index
        ));
    }
    public function getInvoiceBussiness($InvoiceGroup)
    {
        $s             = $this->db->query("select * from tblInvoiceGroup where iInvoiceGroupNbr= ?", array(
            $InvoiceGroup
        ));
        $lineIG        = $s->row_array();
        $cInvoiceGroup = $lineIG['cInvoiceGroup'];
        $resultIB      = $this->db->query("select * from tblType where iTypeNbr= ?", array(
            $lineIG['iBusinessNbr']
        ));
        $lineIB        = $resultIB->row_array();
        $cBusiness     = $lineIB['cTypeDescription'];
        return array(
            'cInvoiceGroup' => $cInvoiceGroup,
            'cBusiness' => $cBusiness
        );
    }
    public function x_magebosubscription_addPricing($iAddressNbr, $InvoiceGroup, $rNumber, $mUnitPrice, $iTotalMonths, $cInvoiceReference, $ContractDate, $iStartMonth, $iStartYear, $bEnabled, $createpriceditems, $cRemark, $cInvoiceDescription, $prorata, $ci = false, $disable_priceditems=false)
    {
        //For now, we use $VAT=21
        //Get invoice Group Description
        $this->db->query("select * from tblInvoiceGroupDescription where iInvoiceGroupNbr= ? AND  iLanguageIndex=(SELECT iLanguageIndex FROM tblAddress where iAddressNbr=?)", array(
            $InvoiceGroup,
            $iAddressNbr
        ));
        $s             = $this->db->query("select * from tblInvoiceGroup where iInvoiceGroupNbr= ?", array(
            $InvoiceGroup
        ));
        $lineIG        = $s->row_array();
        $resultIB      = $this->db->query("select * from tblType where iTypeNbr= ?", array(
            $lineIG['iBusinessNbr']
        ));
        $lineIB        = $resultIB->row_array();
        // $iAddressNbr=$row['uniqueId'];
        //$cBusiness="DELTA MOBILE";
        $cBusiness     = $lineIB['cTypeDescription'];
        $cInvoiceGroup = $lineIG['cInvoiceGroup'];
        $client        = getClientDetailidbyMagebo($iAddressNbr);
        if ($client->vat_exempt == 1) {
            $vat = 0;
        } else {
            $vat = round($client->vat_rate, 0);
        }
        if ($ci) {
            $qu                          = "exec spInsertgeneralPricing " . $iAddressNbr . ",'" . $cBusiness . "','" . $cInvoiceGroup . "'," . $rNumber . "," . $mUnitPrice . "," . $vat . ",1," . $iTotalMonths . ",'" . $ContractDate . "'," . $iStartMonth . "," . $iStartYear . "," . $bEnabled . ",'" . $cRemark . "','NO REPORTING','" . $cInvoiceDescription . "'," . $prorata;
            $gpIndex                     = $this->addPricingRemote($qu);
            $QueryInsertActivationPriced = "exec spInsertPricedItems " . $gpIndex . ", " . $iStartMonth . ", " . $iStartYear . ", 1, " . $prorata . ", " . $vat . ", 1";
            insert_query_log(array(
                'funct' => 'x_magebosubscription_addPricing',
                'description' => $QueryInsertActivationPriced
            ));
            $this->db->query($QueryInsertActivationPriced);
            $this->db->query("update tblGeneralPricing set cInvoiceDescription=?, cRemark=? where iGeneralPricingIndex=?", array(
                $cInvoiceDescription,
                $cRemark,
                $gpIndex
            ));
        } else {
            $gpIndex = $this->addPricingRemote("exec spInsertgeneralPricing $iAddressNbr,'$cBusiness','$cInvoiceGroup',$rNumber,$mUnitPrice,$vat,1,$iTotalMonths,'$ContractDate',$iStartMonth,$iStartYear,$bEnabled,'$cRemark','NO REPORTING','$cInvoiceDescription',$prorata");
        }
        //logActivity("General Pricing Index : " . $gpIndex);
        if ($gpIndex) {
            if ($createpriceditems == 1) {
                //1.PricedItem
                if ($client->companyid != 55) {
                    if (!$disable_priceditems) {
                        $toto = "exec spInsertPricedItems " . $gpIndex . ", " . $iStartMonth . ", " . $iStartYear . ", 1, " . $prorata . ", " . $vat . ", 1";
                        insert_query_log(array(
                            'funct' => 'x_magebosubscription_addPricing',
                            'description' => $toto
                        ));
                        $this->db->query($toto);
                    }

                    $this->db->query("update tblGeneralPricing set cInvoiceDescription=?, cRemark=? where iGeneralPricingIndex=?", array(
                        $cInvoiceDescription,
                        $cRemark,
                        $gpIndex
                    ));
                }
                /*
                Do not create Invoice for it
                $QueryInsertActivationForInvoicing = "exec spInsertPricedItemsInvoice " . $iAddressNbr . ", " . $gpIndex . ", " . $iStartYear . ", " . $iStartMonth;
                insert_query_log(array(
                'funct' => 'x_magebosubscription_addPricing',
                'description' => $QueryInsertActivationForInvoicing));
                $this->db->query($QueryInsertActivationForInvoicing);
                */
            }
        }
        /*else {
        die("Failed to get GPIndex on exec spInsertgeneralPricing $iAddressNbr,'$cBusiness','$cInvoiceGroup',$rNumber,$mUnitPrice,$vat,1,$iTotalMonths,'$ContractDate',$iStartMonth,$iStartYear,$bEnabled,'$cRemark','NO REPORTING','$cInvoiceDescription',$prorata");
        }
        */
        return trim($gpIndex);
    }
    public function insertPromo($mobile)
    {
        $q                   = $this->db->query("select * from tblC_Pincode where iPincode= ?", array(
            $mobile->details->msisdn_sn
        ));
        $lineIGD             = $q->row_array();
        $pincoderef          = $lineIGD['cInvoiceReference'];
        $sstartday           = substr($mobile->date_contract, 0, 2);
        $product             = getMageboProduct($mobile->packageid);
        $PricingAmount       = $product->recurring_subtotal;
        $InvoiceGroup        = $product->iInvoiceGroupNbr;
        $d                   = explode('-', $mobile->date_contract);
        $datequery           = "SELECT SD.InvoiceStartDate,
                                    CONCAT(SUBSTR(SD.InvoiceStartDate,6,2),'/',SUBSTR(SD.InvoiceStartDate,9,2),'/',SUBSTR(SD.InvoiceStartDate,1,4)) ContractDate,
                                    STR_TO_DATE(CONCAT(SUBSTR(SD.InvoiceStartDate,6,2),'/',SUBSTR(SD.InvoiceStartDate,9,2),'/',SUBSTR(SD.InvoiceStartDate,1,4)),'%m/%d/%Y' ) CtrDate
                                    ,LAST_DAY(STR_TO_DATE(CONCAT(SUBSTR(SD.InvoiceStartDate,6,2),'/',SUBSTR(SD.InvoiceStartDate,9,2),'/',SUBSTR(SD.InvoiceStartDate,1,4)),'%m/%d/%Y' )) Lday,
                                    DATEDIFF(LAST_DAY(STR_TO_DATE(CONCAT(SUBSTR(SD.InvoiceStartDate,6,2),'/',SUBSTR(SD.InvoiceStartDate,9,2),'/',SUBSTR(SD.InvoiceStartDate,1,4)),'%m/%d/%Y' )),
                                    STR_TO_DATE(CONCAT(SUBSTR(SD.InvoiceStartDate,6,2),'/',SUBSTR(SD.InvoiceStartDate,9,2),'/',SUBSTR(SD.InvoiceStartDate,1,4)),'%m/%d/%Y' ) )diff,
                                    DAY(LAST_DAY(STR_TO_DATE(CONCAT(SUBSTR(SD.InvoiceStartDate,6,2),'/',SUBSTR(SD.InvoiceStartDate,9,2),'/',SUBSTR(SD.InvoiceStartDate,1,4)),'%m/%d/%Y' ))) DaysInMonth,
                                    MONTH(STR_TO_DATE(CONCAT(SUBSTR(SD.InvoiceStartDate,6,2),'/',SUBSTR(SD.InvoiceStartDate,9,2),'/',SUBSTR(SD.InvoiceStartDate,1,4)),'%m/%d/%Y' )) StartMonth,
                                    YEAR(STR_TO_DATE(CONCAT(SUBSTR(SD.InvoiceStartDate,6,2),'/',SUBSTR(SD.InvoiceStartDate,9,2),'/',SUBSTR(SD.InvoiceStartDate,1,4)),'%m/%d/%Y' )) StartYear
                                    FROM
                                    (SELECT '" . $d[2] . '-' . $d[0] . '-' . $d[1] . "' InvoiceStartDate )SD ";
        $resultnew           = $this->db1->query($datequery);
        $row                 = $resultnew->row_array();
        //logActivity($datequery);
        $ContractDate        = $row['ContractDate'];
        $iStartMonth         = $row['StartMonth'];
        $iStartYear          = $row['StartYear'];
        $client              = $this->CI->Admin_model->getClient($mobile->userid);
        $GPcRemark           = $product->GPcRemark;
        $InvoiceDescription  = $product->iInvoiceDescription;
        $cRemark             = sprintf("$GPcRemark", $pincoderef);
        $cInvoiceDescription = sprintf("$InvoiceDescription", $pincoderef);
        $cInvoiceReference   = $pincoderef;
        $bEnabled            = 1;
        if (!empty($mobile->promocode)) {
            $iAddressNbr = $client->mageboid;
            //h.promo_name,h.promo_description,h.promo_duration,h.id as promo_id,
            $client      = getClientDetailidbyMagebo($iAddressNbr);
            if ($client->vat_exempt == 1) {
                $vat = 0;
            } else {
                $vat = round($client->vat_rate, 0);
            }
            $bussiness            = $this->getInvoiceBussiness($InvoiceGroup);
            $iTotalMonths         = $mobile->promo_duration;
            $cBusiness            = $bussiness['cBusiness'];
            $cInvoiceGroup        = $bussiness['cInvoiceGroup'];
            $mUnitPrice           = exvat4($client->vat_rate, $mobile->promo_value) * -1;
            $cRemarkx             = $mobile->promo_name;
            $cInvoiceDescriptionx = $mobile->promo_name . " " . $pincoderef;
            $promoIndex           = $this->addPricingRemote("exec spInsertgeneralPricing $iAddressNbr,'$cBusiness','Discount Next Month',1,$mUnitPrice,$vat,1,$iTotalMonths,'$ContractDate',$iStartMonth,$iStartYear,$bEnabled,'$cRemarkx','NO REPORTING','$cInvoiceDescriptionx',1");
            echo "exec spInsertgeneralPricing $iAddressNbr,'$cBusiness','Discount Next Month',1,$mUnitPrice,$vat,1,$iTotalMonths,'$ContractDate',$iStartMonth,$iStartYear,$bEnabled,'$cRemarkx','NO REPORTING','$cInvoiceDescriptionx',1";
            if (!empty($promoIndex)) {
                $this->db->query("update tblGeneralPricing set cInvoiceDescription = 'Introductiekorting €5, 6mnd: '+substring(cInvoiceDescription, charindex('06-',cInvoiceDescription), 14), cRemark = 'Introductiekorting €5, 6mnd: '+substring(cInvoiceDescription, charindex('06-',cInvoiceDescription), 14)
  where iGeneralPricingIndex = ?", array(
                    $promoIndex
                ));
                echo $promoIndex;
                //$this->db1->query("update a_services set iGeneralPricingIndex=? where id=?", array(implode(',', array_filter($mp)), $mobile->id));
            }
            //log the query
            insert_query_log(array(
                'funct' => 'x_magebosubscription_addPricing->Discount',
                'description' => "exec spInsertgeneralPricing $iAddressNbr,'$cBusiness','Discount Next Month',1,$mUnitPrice,$vat,1,$iTotalMonths,'$ContractDate',$iStartMonth,$iStartYear,$bEnabled,'$cRemarkx','NO REPORTING','$cInvoiceDescriptionx',1"
            ));
        }
    }
    public function AddBundlePricing($params, $addonid)
    {
        //$bundle = $this->db->query('select * from a_products_mobile_bundles where id=?', array($addonid));
        $bundle          = getMageboProductAddons($addonid);
        //print_r($bundle);
        $bundlestartdate = $params['date_contract'];
        $iPincode        = $params['number'];
        //logActivity("Starting Magebo Bundle Creation for iPincode:" . $iPincode . " with bundle Date: " . $bundlestartdate);
        //****************CALL BUNDLE*****************************//
        $date_start      = explode('-', $bundlestartdate);
        $time            = date('H:i:s');
        $date_start1     = $date_start[2] . '-' . $date_start[0] . '-' . $date_start[1] . ' ' . $time;
        $date            = date_create($date_start[2] . '-' . $date_start[0] . '-' . $date_start[1]);
        date_add($date, date_interval_create_from_date_string("30 days"));
        $future_date       = date_format($date, "Y-m-d") . ' ' . $time;
        $unix_future       = strtotime($future_date);
        $final_unix_future = $unix_future - 1;
        $stopdate          = gmdate("m-d-Y H:i:s", $final_unix_future + (60 * 60));
        //Now we add the bundles
        $BPC1              = str_replace("|", "'", $bundle->pricing_query);
        if ($bundle->bundle_type == "option") {
            $qBPC1 = sprintf("$BPC1", $iPincode, $bundlestartdate . ' ' . $time, $stopdate);
        } elseif ($bundle->bundle_type == "others") {
        } else {
            $qBPC1 = sprintf("$BPC1", $iPincode, $bundlestartdate . ' ' . $time);
        }
        if (!empty($bundle->pricing_query_extra)) {
            $BPC2 = str_replace("|", "'", $bundle->pricing_query_extra);
            if ($bundle->bundle_type == "option") {
                $BPC2 = sprintf("$BPC2", $iPincode, $bundlestartdate . ' ' . $time, $stopdate);
            } elseif ($bundle->bundle_type == "others") {
            } else {
                $qBPC2 = sprintf("$BPC2", $iPincode, $bundlestartdate . ' ' . $time);
            }
            insert_query_log(array(
                'funct' => 'x_magebosubscription_addPricing',
                'description' => $qBPC2
            ));
        }
        insert_query_log(array(
            'funct' => 'x_magebosubscription_addPricing',
            'description' => $qBPC1
        ));
        if (substr($iPincode, 0, 2) == 31) {
            $this->db->query($qBPC1);
            $qresult = false;
        } else {
            $bundleIndex = $this->addBundleRemote($qBPC1);
            $counter     = $this->getMaxPincodeCounter($iPincode);
            if (!empty($bundle->FCA)) {
                //  exec spInsertPincodeFreeCallsAssignment @iPincode,@counter,'SMS_Pack_ATAN','FREE SMS',10000,1,'NO TRANSFER',@contractDate,@StartDate,NULL,1,'SMS BUNDLE ATAN - ID :@iPincodeBundleIndex',0
                $qT1                   = $bundle->FCA;
                $qT1                   = str_replace("|", "'", $qT1);
                $qT1                   = sprintf("$qT1", $iPincode, $counter + 1, $bundlestartdate . ' ' . $time, $date_start1, $bundleIndex);
                //mail('mail@simson.one','ResultFreeAssignment0', print_r($qT1, true));
                //mail('mail@simson.one','ResultFreeAssignment', print_r($counter, true)." ".print_r($qT1, true)."\n".$iPincode."\n".$qBPC1);
                $insertBundleFreeIndex = $this->addBundleFreecallRemote($qT1);
                insert_query_log(array(
                    'funct' => 'Pricing_query FCA',
                    'description' => $qT1
                ));
                //mail('mail@simson.one','ResultFreeAssignment1', print_r($insertBundleFreeIndex, true));
                if (!empty($bundle->FCL)) {
                    $qT2 = $bundle->FCL;
                    $qT2 = sprintf("$qT2", $bundleIndex, $insertBundleFreeIndex);
                    //mail('mail@simson.one','ResultFreeAssignment2', print_r($qT2, true));
                    $this->db->query($qT2);
                    insert_query_log(array(
                        'funct' => 'Pricing_query FCL',
                        'description' => $qT2
                    ));
                    // $qT2 = sprintf("$bundle->FCL", $bundleIndex['PincodeBundleIndex'],  $ResultFreeAssignment[]);
                }
            }
            if (!empty($bundle->pricing_query_extra)) {
                $bundleIndex2 = $this->addBundleRemote($qBPC2);
                $counter2     = $this->getMaxPincodeCounter($iPincode);
                if (!empty($bundle->FCA_extra)) {
                    //  exec spInsertPincodeFreeCallsAssignment @iPincode,@counter,'SMS_Pack_ATAN','FREE SMS',10000,1,'NO TRANSFER',@contractDate,@StartDate,NULL,1,'SMS BUNDLE ATAN - ID :@iPincodeBundleIndex',0
                    $qX1 = $bundle->FCA_extra;
                    $qX1 = str_replace("|", "'", $qX1);
                    $qX1 = sprintf("$qX1", $iPincode, $counter2 + 1, $bundlestartdate . ' ' . $time, $date_start1, $bundleIndex);
                    insert_query_log(array(
                        'funct' => 'Pricing_query FCA Extra',
                        'description' => $qX1
                    ));
                    $insertBundleFreeIndex2 = $this->addBundleFreecallRemote($qX1);
                    if (!empty($bundle->FCL_extra)) {
                        $qX2 = $bundle->FCL_extra;
                        $qX2 = sprintf("$qX2", $bundleIndex2, $insertBundleFreeIndex2);
                        insert_query_log(array(
                            'funct' => 'Pricing_query FCL Extra',
                            'description' => $qX2
                        ));
                        $this->db->query($qX2);
                    }
                }
            }
        }
        return array(
            'result' => 'success'
        );
    }
    public function getMaxPincodeCounter($iPincode)
    {
        $q = $this->db->query("Select Max(iPriority) AS LatestPriority From tblC_PincodeFreeCallsAssignment where iPincode = ?", array(
            $iPincode
        ));
        if ($q->num_rows()) {
            return $q->row()->LatestPriority;
        } else {
            return false;
        }
    }
    public function AddPortinSIMToMagebo($mobile)
    {
        //print_r($params);exit;
        $client            = $this->CI->Admin_model->getClient($mobile->userid);
        //Initialize data fields IMPORTANT !!!
        $oldpaymeth        = "";
        $oldnumpersonal    = "";
        $oldgsmnbr         = "";
        $oldsimnbr         = "";
        $oldclientnbr      = "";
        $oldclientname     = "";
        $oldvatnumber      = "";
        $oldresp           = "";
        $oldoperator       = "";
        $gprs              = "";
        $invoice_condition = "";
        $keepold           = "";
        $strucomm          = "";
        $prepost           = "";
        $dateactivation    = "";
        $tempiAdNMag       = "";
        $SimToUse          = "";
        $iAddressNbr       = "";
        $cCompany          = $client->initial . ' ' . $client->firstname . ' ' . $client->lastname;
        $cCompany          = str_replace("'", " ", $cCompany);
        /******************************************************************************************************************************************/
        /***********************************************Insert Pincode & SIMCard*******************************************************************/
        /******************************************************************************************************************************************/
        if ($mobile->details->msisdn_type == "porting") {
            $keepold = "yes";
        } else {
            $keepold = "no";
        }
        //echo "keep OLD".$keepold;exit;
        //This is the Magebo Address Number
        $iAdN = $client->mageboid;
        if ($keepold == 'no') {
            $PORTtype = "";
        }
        $ptype = $mobile->details->donor_type;
        if ($ptype == "0") {
            $PORTtype = "PrePaid";
        } else {
            $PORTtype = "PostPaid Simple";
        }
        switch ($client->language) {
            case "dutch":
                $cInternalLanguage = "Dutch";
                break;
            case "english":
                $cInternalLanguage = "English";
                break;
            case "french":
                $cInternalLanguage = "French";
                break;
            case "german":
                $cInternalLanguage = "German";
                break;
            default:
                $cInternalLanguage = "Dutch";
        }
        //if($rowOD['fieldid']==1013){$iAddressNbr=$rowOD['value'];}
        //if($rowOD['fieldid']==4){$oldnumpersonal=$res->attributevalue;}
        $oldsimnbr     = $mobile->details->donor_sim;
        $oldclientnbr  = $mobile->details->donor_accountnumber;
        $oldgsmnbr     = "0" . $mobile->details->msisdn;
        $oldclientname = "";
        $oldvatnumber  = "";
        $oldresp       = "";
        $oldoperator   = $mobile->details->donor_provider;
        //1.Get the SIMCard from the order
        $SimToUse      = $mobile->details->msisdn_sim;
        $nextFreeSim   = $SimToUse;
        $dContractDate = $mobile->date_contract;
        //2.Check if it is a porting OR new number --> insert the pincode
        //Then it is a porting
        $iPincode      = $mobile->details->msisdn;
        $ip            = $this->InsertPincode($iPincode, $client->mageboid, $dContractDate, $mobile->details->msisdn_sim);
        if ($ip) {
            /******************************************************************************************************************************************/
            /*******************************Insert PincodeCallPriceList & assign the SIMCard to the pincode********************************************/
            /******************************************************************************************************************************************/
            //$resultIC = select_query("magebo_products","",array("ResellerProductName"=>$params['customfields']['priceplan']));
            // $dataIC = mysql_fetch_array($resultIC);
            // $invoice_condition = $dataIC['iCallPriceListNbr'];
            // $PriceListTarif = $dataIC['iCallPriceListSubType'];
            $invoice_condition = $mobile->PriceList;
            $PriceListTarif    = $mobile->PriceListSubType;
            //print_r($dataIC);exit;
            //Insert PincodeCallPriceList $iAddressNbr, $invoice_condition, $iPincode, $PriceListTarif
            $this->InsertPricelist($client->mageboid, $invoice_condition, $iPincode, $PriceListTarif, $client->vat_exempt);
            //Assign SIMCard to the Pincode
            // $this->AssignSIMCard($iPincode, $mobile->details->msisdn_sim);
            /******************************************************************************************************************************************/
            /***************************************Insert SIMCard options of the pincode**************************************************************/
            /******************************************************************************************************************************************/
            $SIMCardType           = $mobile->details->ptype;
            //If Porting we have to do an updatesimCard
            //Then it is a porting
            $cSIMCardNbr           = $mobile->details->msisdn_sim;
            $cMSISDNType           = "PORTED";
            $cPaymentType          = $SIMCardType;
            $cCallSubscriptionType = "NO CALL SUBSCRIPTION";
            if ($gprs == "gprs") {
                $cGPRSSubscriptionType = "WEB.BE GPRS SUBSCRIPTION";
            } else {
                $cGPRSSubscriptionType = "WEB.BE GPRS SUBSCRIPTION";
            }
            $cVoiceMailLanguage     = strtoupper($cInternalLanguage);
            $bRoaming               = 1;
            $bBarIncomingCalls      = 0;
            $bBarInternationalCalls = 0;
            $bBarPremium            = 0;
            $bBarPremiumVoice       = 0;
            $bCLIR                  = 0;
            $bBarPartial            = 0;
            $bBarFull               = 0;
            $bIntelligentRoaming    = 0;
            $cRemark                = str_replace("'", " ", $cCompany);
            $mInitialPrepaidAmount  = 0;
            $iCarrier               = -1;
            $cTypeOfPorting         = $PORTtype;
            $cDonorMSISDN           = $oldgsmnbr;
            $cDonorSimCardNbr       = $oldsimnbr;
            $cVATNbr                = $oldvatnumber;
            $cAuthorisedRequestor   = $oldresp;
            $cDonorAccountNbr       = $oldclientnbr;
            $cDonorCustomerName     = $oldclientname;
            //$iDonorCarrier = 60008;
            //Determine the Donor Carrier Based on the the custom field PortDonorOperator
            if ($SIMCardType == "PRE PAID") {
                $bPseudoPostpaid = 1;
            } else {
                $bPseudoPostpaid = 0;
            }
            $iDonorCarrier      = 60269;
            $mInitialCostAmount = 0;
            $iValidityDays      = 365;
            $cBlackBerryType    = "NONE";
            $bMMS               = 1;
            $queryPORT          = "EXEC spUpdateSimCard '$cSIMCardNbr', '$cMSISDNType','SINGLE', '$cPaymentType', '$cCallSubscriptionType', '$cGPRSSubscriptionType', '$cVoiceMailLanguage', $bRoaming, $bBarIncomingCalls, $bBarInternationalCalls, $bBarPremium, $bBarPremiumVoice, $bCLIR, $bBarPartial, $bBarFull, $bIntelligentRoaming, $bPseudoPostpaid, '$cRemark', $mInitialPrepaidAmount, $iCarrier, '$cTypeOfPorting', '$cDonorMSISDN', '$cDonorSimCardNbr', '$cVATNbr', '$cAuthorisedRequestor', '$cDonorAccountNbr', '$cDonorCustomerName', $iDonorCarrier, $mInitialCostAmount, $iValidityDays, '$cBlackBerryType', $bMMS";
            insert_query_log(array(
                'funct' => 'AddSIMToMagebo->spUpdateSimCard',
                'description' => $queryPORT
            ));
            $resultPORT = $this->db->query($queryPORT);
            return (object) array(
                "result" => "success"
            );
        } else {
            return (object) array(
                "result" => "error",
                "message" => "Adding Sim to Magebo"
            );
        }
    }
    public function AddSIMToMagebo($mobile, $simOnly = false)
    {
        //print_r($params);exit;
        $client            = $this->CI->Admin_model->getClient($mobile->userid);
        //Initialize data fields IMPORTANT !!!
        $oldpaymeth        = "";
        $oldnumpersonal    = "";
        $oldgsmnbr         = "";
        $oldsimnbr         = "";
        $oldclientnbr      = "";
        $oldclientname     = "";
        $oldvatnumber      = "";
        $oldresp           = "";
        $oldoperator       = "";
        $gprs              = "";
        $invoice_condition = "";
        $keepold           = "";
        $strucomm          = "";
        $prepost           = "";
        $dateactivation    = "";
        $tempiAdNMag       = "";
        $SimToUse          = "";
        $iAddressNbr       = "";
        $cCompany          = $client->initial . ' ' . $client->firstname . ' ' . $client->lastname;
        $cCompany          = str_replace("'", " ", $cCompany);
        /******************************************************************************************************************************************/
        /***********************************************Insert Pincode & SIMCard*******************************************************************/
        /******************************************************************************************************************************************/
        if ($mobile->details->msisdn_type == "porting") {
            $keepold = "yes";
        } else {
            $keepold = "no";
        }
        //echo "keep OLD".$keepold;exit;
        //This is the Magebo Address Number
        $iAdN = $client->mageboid;
        if ($keepold == 'no') {
            $PORTtype = "";
        }
        $ptype = $mobile->details->donor_type;
        if ($ptype == "0") {
            $PORTtype = "PrePaid";
        } else {
            $PORTtype = "PostPaid Simple";
        }
        switch ($client->language) {
            case "dutch":
                $cInternalLanguage = "Dutch";
                break;
            case "english":
                $cInternalLanguage = "English";
                break;
            case "french":
                $cInternalLanguage = "French";
                break;
            case "german":
                $cInternalLanguage = "German";
                break;
            default:
                $cInternalLanguage = "Dutch";
        }
        //if($rowOD['fieldid']==1013){$iAddressNbr=$rowOD['value'];}
        //if($rowOD['fieldid']==4){$oldnumpersonal=$res->attributevalue;}
        $oldsimnbr     = $mobile->details->donor_sim;
        $oldclientnbr  = $mobile->details->donor_accountnumber;
        $oldgsmnbr     = "0" . $mobile->details->donor_msisdn;
        $oldclientname = "";
        $oldvatnumber  = "";
        $oldresp       = "";
        $oldoperator   = $mobile->details->donor_provider;
        //1.Get the SIMCard from the order
        $SimToUse      = $mobile->details->msisdn_sim;
        $nextFreeSim   = $SimToUse;
        $dContractDate = $mobile->date_contract;
        if (substr(trim($mobile->details->msisdn), 0, 2) == "32") {
            $iPincode = substr($mobile->details->msisdn, 2);
        } else {
            $iPincode = $mobile->details->msisdn_sn;
        }
        echo "PIn: " . $iPincode;
        //2.Check if it is a porting OR new number --> insert the pincode
        $ip = $this->InsertPincode($iPincode, $client->mageboid, $dContractDate, $mobile->details->msisdn_sim);
        //print_r($ip);
        if ($ip) {
            /******************************************************************************************************************************************/
            /*******************************Insert PincodeCallPriceList & assign the SIMCard to the pincode********************************************/
            /******************************************************************************************************************************************/
            //$resultIC = select_query("magebo_products","",array("ResellerProductName"=>$params['customfields']['priceplan']));
            // $dataIC = mysql_fetch_array($resultIC);
            // $invoice_condition = $dataIC['iCallPriceListNbr'];
            // $PriceListTarif = $dataIC['iCallPriceListSubType'];
            $invoice_condition = $mobile->PriceList;
            $PriceListTarif    = $mobile->PriceListSubType;
            //print_r($dataIC);exit;
            //Insert PincodeCallPriceList $iAddressNbr, $invoice_condition, $iPincode, $PriceListTarif
            $this->InsertPricelist($client->mageboid, $invoice_condition, $iPincode, $PriceListTarif, $client->vat_exempt);
            //Assign SIMCard to the Pincode
            $this->AssignSIMCard($iPincode, $mobile->details->msisdn_sim);
            /******************************************************************************************************************************************/
            /***************************************Insert SIMCard options of the pincode**************************************************************/
            /******************************************************************************************************************************************/
            $SIMCardType = $mobile->details->ptype;
            $cSIMCardNbr = $mobile->details->msisdn_sim;
            if (substr(trim($mobile->details->msisdn), 0, 2) == "32") {
                if ($mobile->details->msisdn_type == "porting") {
                    $cMSISDNType = "PORTED";
                } else {
                    $cMSISDNType = "NEW";
                }
            } else {
                $cMSISDNType = "NEW";
            }
            $cPaymentType          = $SIMCardType;
            $cCallSubscriptionType = "NO CALL SUBSCRIPTION";
            if ($gprs == "gprs") {
                $cGPRSSubscriptionType = "WEB.BE GPRS SUBSCRIPTION";
            } else {
                $cGPRSSubscriptionType = "WEB.BE GPRS SUBSCRIPTION";
            }
            $cVoiceMailLanguage     = strtoupper($cInternalLanguage);
            $bRoaming               = 1;
            $bBarIncomingCalls      = 0;
            $bBarInternationalCalls = 0;
            $bBarPremium            = 0;
            $bBarPremiumVoice       = 0;
            $bCLIR                  = 0;
            $bBarPartial            = 0;
            $bBarFull               = 0;
            $bIntelligentRoaming    = 0;
            $bPseudoPostpaid        = 0;
            $cRemark                = $cCompany;
            $mInitialPrepaidAmount  = 0;
            $iCarrier               = -1;
            $cTypeOfPorting         = '';
            $cDonorMSISDN           = $mobile->details->donor_msisdn;
            $cDonorSimCardNbr       = '';
            $cVATNbr                = '';
            $cAuthorisedRequestor   = '';
            $cDonorAccountNbr       = '';
            $cDonorCustomerName     = '';
            $iDonorCarrier          = -1;
            $cBlackBerryType        = "NONE";
            $bMMS                   = 1;
            $queryNOPORT            = "EXEC spUpdateSimCard '$cSIMCardNbr', '$cMSISDNType','SINGLE', '$cPaymentType', '$cCallSubscriptionType', '$cGPRSSubscriptionType', '$cVoiceMailLanguage', $bRoaming, $bBarIncomingCalls, $bBarInternationalCalls, $bBarPremium, $bBarPremiumVoice, $bCLIR, $bBarPartial, $bBarFull, $bIntelligentRoaming, $bPseudoPostpaid, '$cRemark', NULL, NULL, '$cTypeOfPorting', '$cDonorMSISDN', '$cDonorSimCardNbr', '$cVATNbr', '$cAuthorisedRequestor', '$cDonorAccountNbr', '$cDonorCustomerName', $iDonorCarrier, NULL, NULL, '$cBlackBerryType', $bMMS";
            $resultNOPORT           = $this->db->query($queryNOPORT);
            insert_query_log(array(
                'funct' => 'AddSIMToMagebo->spUpdateSimCard',
                'description' => $queryNOPORT
            ));
            return (object) array(
                "result" => "success"
            );
        } else {
            return (object) array(
                "result" => "error",
                "message" => "Adding Sim to Magebo"
            );
        }
    }
    public function Mod11($iInvoiceNbr)
    {
        $q   = $this->db->query("select dbo.fnGetStructuredMessageTrendCall(" . $iInvoiceNbr . ") as res");
        $res = $q->row_array();
        return $res['res'];
    }
    public function SetRefusedSepa($id)
    {
        $this->db->query("Update tblBankFileDetail set bRefused = 1 where iBankFileDetailIndex = ?", array(
            $id
        ));
    }
    public function hasPricingList($iAddressNbr, $iPincode)
    {
        $cInvoiceDescription = $this->getRef($iPincode);
        if (!$cInvoiceDescription) {
            return false;
        } else {
            $q = $this->db->query("select * from tblGeneralPricing where cRemark like ? and iAddressNbr=?", array(
                '%' . $cInvoiceDescription . '%',
                $iAddressNbr
            ));
            if ($q->num_rows() > 0) {
                return true;
            } else {
                return false;
            }
        }
    }
    public function InsertPricelist($iAddressNbr, $invoice_condition, $iPincode, $PriceListTarif, $vat, $counter = 1, $package_changes = false)
    {
        //First We have to get the description of the pricelist
        $resultDESC  = $this->db->query("SELECT cTypeDescription FROM tblType WHERE iTypeNbr = ?", array(
            $invoice_condition
        ));
        $lineDESC    = $resultDESC->row_array();
        $PPDESC      = $lineDESC['cTypeDescription'];
        //First We have to get the description of the subtype
        $DESCquery2  = "SELECT cTypeDescription FROM tblType WHERE iTypeNbr=" . $PriceListTarif;
        $resultDESC2 = $this->db->query("SELECT cTypeDescription FROM tblType WHERE iTypeNbr=?", array(
            $PriceListTarif
        ));
        $lineDESC2   = $resultDESC2->row_array();
        $PPDESC2     = $lineDESC2['cTypeDescription'];
        if ($vat == "1") {
            $this->db->query("execute spUpdateInvoiceCondition " . $iAddressNbr . ", 14, 'NORMAL', 0, 'PAPER', 3, 10, 10, 1, 'FULL DETAIL', NULL, '', 'NO REPORTING', 1, 0, 'EUROPE', 0, NULL, 0, 'FIX COST', 10, 1, 'NO EXCHANGE', 0");
            insert_query_log(array(
                'funct' => 'InsertPricelist',
                'description' => "execute spUpdateInvoiceCondition " . $iAddressNbr . ", 14, 'NORMAL', 0, 'PAPER', 3, 10, 10, 1, 'FULL DETAIL', NULL, '', 'NO REPORTING', 1, 0, 'EUROPE', 0, NULL, 0, 'FIX COST', 10, 1, 'NO EXCHANGE', 0"
            ));
            $rVATPercentage = 0;
        } else {
            $rVATPercentage = 21;
        }
        $test = $this->db->query("SELECT iPincodeCallPriceListIndex, iPincode, iPriority, iCallPriceListNbr, iCallPriceListSubType, iDefaultTarifNbr, mVATPercentage, dStopDate, iMVNOPriceListNbr, dLastUpdate, iLastUserNbr FROM tblC_PincodeCallPriceList where iPincode=? and iPriority = ?", array(
            $iPincode,
            1
        ));
        if ($test->num_rows() > 0) {
            // echo "Pricecall exists aborting\n";
            if ($package_changes) {
                $this->db->query("EXEC spInsertPincodeCallPriceList " . $iPincode . ", " . $counter . ", '" . $PPDESC . "', '" . $PPDESC2 . "', 'DEFAULT TARIF', " . $rVATPercentage . ", NULL");
            }
        } else {
            $this->db->query("EXEC spInsertPincodeCallPriceList " . $iPincode . ", " . $counter . ", '" . $PPDESC . "', '" . $PPDESC2 . "', 'DEFAULT TARIF', " . $rVATPercentage . ", NULL");
            insert_query_log(array(
                'funct' => 'InsertPricelist',
                'description' => "EXEC spInsertPincodeCallPriceList " . $iPincode . "," . $counter . ", '" . $PPDESC . "', '" . $PPDESC2 . "', 'DEFAULT TARIF', " . $rVATPercentage . ", NULL"
            ));
        }
    }
    /****************************************************************************************************************************/
    /******************************Funtion to assign simcard to the pincode******************************************************/
    /****************************************************************************************************************************/
    public function AssignSIMCard($iPincode, $nextFreeSim)
    {
        $test = $this->db->query("SELECT * FROM tblC_SimCard where cSIMCardNbr=? and iPincode=?", array(
            $nextFreeSim,
            $iPincode
        ));
        if ($test->num_rows() > 0) {
        } else {
            $this->db->query("EXEC spAssignSIMtoPIN " . $iPincode . ", '" . $nextFreeSim . "', 0, 0");
            insert_query_log(array(
                'funct' => 'AssignSIMCard',
                'description' => "EXEC spAssignSIMtoPIN " . $iPincode . ", '" . $nextFreeSim . "', 0, 0"
            ));
        }
    }
    public function addSimcardLog($simcard, $id)
    {
        $date = date('Ymd') . ' ' . date('H:i:s');
        $this->db->query("exec spInsertSIMCardLog '$simcard', $id,NULL,NULL,NULL,NULL,NULL,'$date'");
        insert_query_log(array(
            'funct' => 'addSimcardLog',
            'description' => "exec spInsertSIMCardLog '$simcard', $id,NULL,NULL,NULL,NULL,NULL,'$date'"
        ));
    }
    public function InsertPincode($iPincode, $iAdN, $dContractDate, $nextFreeSim)
    {
        $linePinExistCheck = $this->getPincodeRemote(trim($iPincode));
        //print_r(array("resulxt" => $linePinExistCheck));
        $PinCheck          = $linePinExistCheck['iPincode'];
        $SimCheck          = $linePinExistCheck['SimCardNbr'];
        $dStopDate         = $linePinExistCheck['dStopDate'];
        if ($iPincode == $PinCheck) {
            //Pincode already exists in magebo --> we have to do an internal porting
            //$message = "Internal Porting\n" . "PinCode:" . $iPincode;
            //logActivity($message);
            //WHMCSTicket(1002014, $serviceid, $message, 13, "Delta - InternalPorting ?");
            // mail('mail@simson.one','iAddressNbr: ' .$iAdN. ' Internal Portin Detected for Number: 0'.$iPincode,'Please check in Magebo as Internal Porting required');
            return false;
        } elseif ($iPincode == "31657608920") {
            return false;
        } else {
            //just insert the pincode
            //$cInvoiceReference="0".substr($iPincode,0,3)."/".substr($iPincode,3,2).".".substr($iPincode,5,2).".".substr($iPincode,7,2);
            //31648924972 - 06-48 92 49 72
            if (substr($iPincode, 0, 2) == "31") {
                $cInvoiceReference = "0" . substr($iPincode, 2, 1) . "-" . substr($iPincode, 3, 2) . " " . substr($iPincode, 5, 2) . " " . substr($iPincode, 7, 2) . " " . substr($iPincode, 9, 2);
            } else {
                $cInvoiceReference = "0" . substr($iPincode, 0, 3) . '/' . substr($iPincode, 3, 2) . "." . substr($iPincode, 5, 2) . "." . substr($iPincode, 7, 2);
            }
            if (substr(trim($iPincode), 0, 2) == 31) {
                //Tmobile Nl
                $carrier = 60269;
            } else {
                //Base Belgium
                $carrier = 60007;
            }
            if (!$this->isPinCreated($iPincode)) {
                $queryIPC = "EXEC spInsertPincode $iPincode,$iAdN,$iAdN,'$cInvoiceReference','', 'UNITED CALLING MOBILE', $carrier, NULL, 'CLI', 'GSM', 'NONE', '$dContractDate', NULL, '', NULL";
                insert_query_log(array(
                    'funct' => 'InsertPincode',
                    'description' => $queryIPC
                ));
                $resultIPC = $this->db->query($queryIPC);
            }
            return true;
        }
    }
    public function isPinCreated($pin)
    {
        $q = $this->db->query("select * from tblC_Pincode where iPincode = ?", array(
            $pin
        ));
        if ($q->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
    public function updateBundleStopDate($pincode, $date)
    {
        if (substr(trim($pincode), 0, 2) == 32) {
            $pincode = substr($pincode, 2);
        }
        $this->db->where('iPincode', $pincode);
        $this->db->update('tblC_BundlePerPincode', array(
            'dBundleStopDate' => $date,
            'bEnabled' => '0',
            'bInSubscription' => '0'
        ));
    }
    public function getPremier()
    {
        $q = $this->db->query('select iPincode from BlockPremierTelecom');
        return $q->result();
    }
    public function getSimcardbyMsisdn($msisdn)
    {
        if (substr($msisdn, 0, 2) == "32") {
            $msisdn = substr($msisdn, -9);
        }
        $q = $this->db->query("SELECT cSIMCardNbr, iSimCardType, iCompanyRangeNbr, iPincode, iMSISDN, cPUK1, cPUK2, cIMSI, iMSISDNType, iPaymentType, iCallSubscriptionType, iGPRSSubscriptionType, bRoaming, iVoiceMailLanguage, bBarIncomingCalls, bBarInternationalCalls, bBarPremium, bBarPremiumVoice, bCLIR, bBarPartial, bBarFull, bIntelligentRoaming, bPseudoPostpaid, bGoldenNbr, iAgentNbr, iLastImportFeedbackState, cRemark, iBoxNbr, bActive, dLastUpdate, iLastUserNbr
FROM GDC_ERP.dbo.tblC_SimCard where iMSISDN like ? and iPincode is NULL and bActive=?", array($msisdn, 1));

        if ($q->num_rows()>0) {
            log_message('error', $this->db->last_query());
            return $q->row();
        } else {
            log_message('error', "Simcard no found in magebo for ".$msisdn);
            return false;
        }
    }
    public function getSim($pincode)
    {
        $q = $this->db->query("select A.iAddressNbr, A.cName, A.cStreet,A.iCompanyNbr,Y.cTypeDescription as PaymentType,
    CASE when A.iCountryIndex = 22
    then (C.cCountryCode + '-' + Cast(A.iZipCode AS varchar(10)) + ' ' + A.cZipSuffix)
    else (C.cCountryCode + '-' +
    CASE when Cast(A.iZipCode AS varchar(10)) is null
    then ''+ A.cZipSuffix
    else Cast(A.iZipCode AS varchar(10)) + ' ' + A.cZipSuffix end  ) end as ZipCode,
    A.cCity, P.cInvoiceReference, S.cSIMCardNbr as SIMCardNbr, S.iMSISDN as MSISDN, S.*, X.cTypeDescription as Type,Z.cTypeDescription as SimCardType, B.cInternalLanguage as Language
    from tblC_SIMCard S
    left join tblC_Pincode P ON P.iPincode = S.iPincode
    left join tblLocation L ON L.iLocationIndex = P.iLocationIndex
    left join tblAddress A ON A.iAddressNbr = L.iAddressNbr
    left join tblType Y ON Y.iTypeNbr=S.iPaymentType
    left join tblType X ON X.iTypeNbr=S.iMSISDNType
    left join tblType Z ON Z.iTypeNbr=S.iSimCardType
    left join tblLanguages B on B.iBaseVoiceMailLanguage=S.iVoiceMailLanguage
    , tblCountry C
    where S.iPincode = ?
    AND S.bActive = 1
    AND (C.iCountryIndex = A.iCountryIndex)", array(
            trim($pincode)
        ));
        return $q->row();
    }
    public function CreateWelcomeLetter($serviceid)
    {
        $mobile  = $this->CI->Admin_model->getService($serviceid);
        $client  = $this->CI->Admin_model->getClient($mobile->userid);
        $product = $this->CI->Admin_model->getProduct($mobile->packageid);
        if ($client->language) {
            $this->CI->config->set_item('language', $client->language);
        } else {
            $this->CI->config->set_item('language', 'dutch');
        }
        $this->CI->lang->load('admin');
        $iAddressNbr    = $client->mageboid;
        $cName          = $client->lastname . ", " . $client->firstname;
        $iPincode       = $mobile->details->msisdn;
        $cStreet        = $client->address1;
        $iZipCode       = $client->postcode;
        $cCity          = $client->city;
        //Determine Language based on the customer's language in whmcs
        $whmcslang      = $client->language;
        $iLanguageIndex = 1; //Start by setting as languageindex to 1 dutch by default
        switch ($whmcslang) {
            case "english":
                $iLanguageIndex = 2;
                break;
            case "french":
                $iLanguageIndex = 2;
                break;
            case "dutch":
                $iLanguageIndex = 1;
                break;
            case "german":
                $iLanguageIndex = 4;
                break;
            default:
                $iLanguageIndex = 1;
        }
        //echo "WHMCSLANG:".$whmcslang."| LanguageIndex:".$iLanguageIndex;exit;
        //$iLanguageIndex=1;
        $cInvoiceReference = $mobile->details->msisdn;
        $cPUK1             = $mobile->details->msisdn_puk1;
        $cPUK2             = $mobile->details->msisdn_puk2;
        $iMSISDNType       = 589; //589 - New //590 - Porintg //591 Swap
        $iPaymentType      = 603; //Prepaid -- postpaid is 604
        $Klantnr           = lang("Cust No : ");
        $GSMnr             = lang("GSMnr : ");
        $PUK1              = "PUK1 : ";
        $PUK2              = "PUK2 : ";
        $Datum             = "DATE : ";
        $title             = lang("Mobile Welcome Letter") . "(" . $iPincode . ")";
        $fname             = $client->id . "_" . $serviceid . "_mobilewelcome.pdf";
        /*Properties to connect to the jasperserver*/
        $format            = "PDF"; // Could be HTML, RTF, etc (but remember to update the Content-Type header above)
        $report            = $product->welcome_template;
        ///reports/UnitedReports/MobileWelcomeLetter/Mobile_WelcomeLetter_1
        $repfile           = DOC_PATH . 'documents/' . $client->companyid . '/attachments/' . $fname;
        $frep              = fopen($repfile, "wb");
        $sp                = new SoapClient($product->jasper_server, array(
            "login" => "jasperadmin",
            "password" => "un!t3d",
            "trace" => 1,
            "exceptions" => 0
        ));
        $request           = "<request operationName=\"runReport\" locale=\"da\">
      <argument name=\"RUN_OUTPUT_FORMAT\">$format</argument>
      <resourceDescriptor name=\"\" wsType=\"\"
      uriString=\"$report\"
      isNew=\"false\">
      <label>null</label>

        <parameter name='iAddressNbr' isListItem='false'>$iAddressNbr</parameter>
        <parameter name='cName' isListItem='false'>$cName</parameter>
        <parameter name='iPincode' isListItem='false'>$iPincode</parameter>
        <parameter name='cStreet' isListItem='false'>$client->address1</parameter>
        <parameter name='iZipCode' isListItem='false'>$client->postcode</parameter>
        <parameter name='cCity' isListItem='false'>$client->city</parameter>
        <parameter name='iLanguageIndex' isListItem='false'>$iLanguageIndex</parameter>
        <parameter name='cInvoiceReference' isListItem='false'>$cInvoiceReference</parameter>
        <parameter name='cPUK1' isListItem='false'>$cPUK1</parameter>
        <parameter name='cPUK2' isListItem='false'>$cPUK2</parameter>
        <parameter name='iMSISDNType' isListItem='false'>$iMSISDNType</parameter>
        <parameter name='iPaymentype' isListItem='false'>$iPaymentType</parameter>
        <parameter name='Klantnr' isListItem='false'>$Klantnr</parameter>
        <parameter name='GSMnr' isListItem='false'>$GSMnr</parameter>
        <parameter name='PUK1' isListItem='false'>$PUK1</parameter>
        <parameter name='PUK2' isListItem='false'>$PUK2</parameter>
        <parameter name='Datum' isListItem='false'>$Datum</parameter>
      </resourceDescriptor>
    </request>";
        // logActivity("Welcome Letter Jasper XML Request: " . $request);
        $sp->runReport($request);
        $filecontents = $sp->__getLastResponse();
        // logActivity($filecontents);
        //echo $filecontents;exit;
        fwrite($frep, $filecontents);
        fclose($frep);
        $query = "INSERT INTO `a_clients_files`
    (`id`,
    `userid`,
    `title`,
    `filename`,
    `adminonly`,
    `dateadded`)
    VALUES
    (
    NULL,
    '$client->id',
    '$title',
    '$fname',
    0,
    NOW()
    )";
        $this->db1->query($query);
        return $repfile;
    }
    public function addPricingRemote($query)
    {
        //this is temporary solution because issue with php7.2 version
        $whmcsUrl            = 'https://probile.united-telecom.be/api.php?step=addPricing';
        $postfields['query'] = $query;
        $ch                  = curl_init();
        curl_setopt($ch, CURLOPT_URL, $whmcsUrl);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postfields));
        $response = curl_exec($ch);
        if (curl_error($ch)) {
            die('Unable to connect: WHMCS ' . curl_errno($ch) . ' - ' . curl_error($ch));
        }
        curl_close($ch);
        $jsondata = json_decode($response);
        insert_query_log(array(
            'funct' => 'addPricingRemote Result: ' . $jsondata->id,
            'description' => $query
        ));
        return $jsondata->id;
    }
    public function addBundleFreecallRemote($query)
    {
        //this is temporary solution because issue with php7.2 version
        $whmcsUrl            = 'https://probile.united-telecom.be/api.php?step=addbundlefreecall';
        $postfields['query'] = $query;
        $ch                  = curl_init();
        curl_setopt($ch, CURLOPT_URL, $whmcsUrl);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postfields));
        $response = curl_exec($ch);
        if (curl_error($ch)) {
            die('Unable to connect: WHMCS ' . curl_errno($ch) . ' - ' . curl_error($ch));
        }
        curl_close($ch);
        $jsondata = json_decode($response);
        insert_query_log(array(
            'funct' => 'addPricingRemote Result: ' . $jsondata->id,
            'description' => $query
        ));
        return $jsondata->id;
    }
    public function addBundleRemote($query)
    {
        //this is temporary solution because issue with php7.2 version
        $whmcsUrl            = 'https://probile.united-telecom.be/api.php?step=addbundle';
        $postfields['query'] = $query;
        $ch                  = curl_init();
        curl_setopt($ch, CURLOPT_URL, $whmcsUrl);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postfields));
        $response = curl_exec($ch);
        if (curl_error($ch)) {
            die('Unable to connect: WHMCS ' . curl_errno($ch) . ' - ' . curl_error($ch));
        }
        curl_close($ch);
        $jsondata = json_decode($response);
        insert_query_log(array(
            'funct' => 'addPricingRemote Result: ' . $jsondata->id,
            'description' => $query
        ));
        return $jsondata->id;
    }
    public function getPincodeRemote($query)
    {
        //this is temporary solution because issue with php7.2 version
        $whmcsUrl            = 'https://probile.united-telecom.be/api.php?step=getPincode';
        $postfields['query'] = $query;
        $ch                  = curl_init();
        curl_setopt($ch, CURLOPT_URL, $whmcsUrl);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postfields));
        $response = curl_exec($ch);
        if (curl_error($ch)) {
            die('Unable to connect: WHMCS ' . curl_errno($ch) . ' - ' . curl_error($ch));
        }
        curl_close($ch);
        $jsondata = json_decode($response);
        return (array) $jsondata;
    }
    public function PortIngAction1($servicedetails)
    {
        $iPincodePortIn = $servicedetails->details->donor_msisdn;
        $iMSISDN        = ltrim($servicedetails->details->msisdn_sn);
        $cSIMCardNbr    = ltrim($servicedetails->details->msisdn_sim);
        $qryfind        = "update tblC_SIMCard set iPincode = " . $iPincodePortIn . " where cSIMCardNbr = '" . $cSIMCardNbr . "' AND iPincode = " . $iMSISDN;
        insert_query_log(array(
            'funct' => 'PortIngAction1: ',
            'description' => $qryfind
        ));
        $this->db->query($qryfind);
    }
    public function PortIngAction2($servicedetails)
    {
        $iPincodePortIn = $servicedetails->details->donor_msisdn;
        $iMSISDN        = trim($servicedetails->details->msisdn_sn);
        $cSIMCardNbr    = trim($servicedetails->details->msisdn_sim);
        $qryfind        = "update tblC_Pincode set cRemark = 'Ported to " . $iPincodePortIn . "', iPincodeType = 16, iPincodeInterfaceType = 2446, iPincodeRedirectionType = 2447 where iPincode = " . $iMSISDN;
        $this->db->query($qryfind);
        insert_query_log(array(
            'funct' => 'PortIngAction2: ',
            'description' => $qryfind
        ));
    }
    public function PortIngAction3($servicedetails)
    {
        $iPincodePortIn = $servicedetails->details->donor_msisdn;
        $iMSISDN        = trim($servicedetails->details->msisdn_sn);
        $qryfind        = "update tblC_BundlePerPincode set iPincode = " . $iPincodePortIn . " where iPincode = " . $iMSISDN . " AND ( dBundleStopDate is null OR dBundleStopDate > getdate() )";
        $this->db->query($qryfind);
        insert_query_log(array(
            'funct' => 'PortIngAction3: ',
            'description' => $qryfind
        ));
    }
    public function DeleteMageboSubs($domain)
    {
        $pin = $this->getRef($domain);
        if ($pin) {
            $q = $this->db->query("select * from tblGeneralPricing where cRemark like ?", array(
                '%' . $pin
            ));
            if ($q->num_rows() > 0) {
                foreach ($q->result_array() as $row) {
                    echo "Deleting Pricing Index: " . $this->deletePricedItems($row['iGeneralPricingIndex']) . "\n";
                    echo "Deleting Pricing Items : " . $this->deletePricingIndex($row['iGeneralPricingIndex']) . "\n";
                }
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    public function deletePricingIndex($iGeneralPricingIndex)
    {
        if ($iGeneralPricingIndex) {
            $this->db->query("delete from tblGeneralPricing where iGeneralPricingIndex =?", array(
                $iGeneralPricingIndex
            ));
            return $this->db->affected_rows();
        }
    }
    public function deletePricedItems($iGeneralPricingIndex)
    {
        if ($iGeneralPricingIndex) {
            $q = $this->db->query("select * from tblPricedItems where iGeneralPricingIndex = ?", array(
                $iGeneralPricingIndex
            ));
            if ($q->num_rows() > 0) {
                foreach ($q->result() as $row) {
                    $this->db->query("delete from tblPricedItems where iGeneralPricingIndex =?", array(
                        $iGeneralPricingIndex
                    ));
                    return $row->iPricedItemsIndex . " " . $this->db->affected_rows();
                }
            } else {
                return false;
            }
        }
    }
    public function InsertBundlePincode($iPincode, $BPC1, $date)
    {
        $d               = explode('-', $date);
        $bundlestartdate = $d[1] . '-' . $d[2] . '-' . $d[0];
        $BPC1            = str_replace("|", "'", $BPC1);
        $qBPC1           = sprintf("$BPC1", $iPincode, $bundlestartdate . ' 00:00:00');
        $this->db->query($qBPC1);
    }
    public function getRef($iPincode)
    {
        $qryfind = "select cInvoiceReference from tblC_Pincode where iPincode = " . $iPincode;
        $q       = $this->db->query($qryfind);
        if ($q->num_rows() > 0) {
            return $q->row()->cInvoiceReference;
        } else {
            return false;
        }
    }
    public function PortIngAction4($servicedetails)
    {
        $iPincodePortIn = $servicedetails->details->donor_msisdn;
        $iMSISDN        = trim($servicedetails->details->msisdn_sn);
        $cSIMCardNbr    = trim($servicedetails->details->msisdn_sim);
        $serviceid      = $servicedetails->id;
        $P_MSISDN       = $this->getRef($iMSISDN);
        $P_PORTINGPIN   = $this->getRef($iPincodePortIn);
        $gpIndexes      = $servicedetails->iGeneralPricingIndex;
        $gpIndexes      = rtrim($gpIndexes, ',');
        $qryfind        = "update tblGeneralPricing set cRemark = replace(cRemark, '" . $P_MSISDN . "', '" . $P_PORTINGPIN . "'), cInvoiceDescription = replace(cInvoiceDescription, '" . $P_MSISDN . "', '" . $P_PORTINGPIN . "')  where iGeneralPricingIndex in (" . $gpIndexes . ")";
        $this->db->query($qryfind);
        insert_query_log(array(
            'funct' => 'PortIngAction4: ',
            'description' => $qryfind
        ));
    }
    public function PortIngAction5($iAddressNbr)
    {
        $this->db = $this->load->database('magebo', true);
        $this->db->query("update tblGeneralPricing set cInvoiceDescription = replace(cRemark, 'Abo - ', '')
  where iAddressNbr = ?
  AND iInvoiceGroupNbr = 346
  AND cInvoiceDescription like 'Abonnement -%'", array(
            $iAddressNbr
        ));
    }
    public function MovePin($donor_msisdn, $msisdn_sn, $msisdn)
    {
        $this->db->query("update tblC_Pincode
            set cRemark = ?,
            iPincodeType = 16,
            iPincodeInterfaceType = 2446,
            iPincodeRedirectionType = 2447
            where iPincode = ?", array(
            $donor_msisdn,
            $msisdn_sn
        ));
        $this->db->query("update tblC_BundlePerPincode
set iPincode =?
where iPincode = ?
  AND ( dBundleStopDate is null OR dBundleStopDate > getdate() )
 ", array(
            $msisdn,
            $msisdn_sn
        ));
        return 'success';
    }
    public function getGeneralPricingIndex($iAddressNbr, $Reference)
    {
        $this->db = $this->load->database('magebo', true);
        $this->db->query("SELECT iGeneralPricingIndex, iAddressNbr, iInvoiceGroupNbr from tblGeneralPricing where iAddressNbr=?", array(
            $iAddressNbr
        ));
    }
    public function getPaymentList($iAddressNbr)
    {
        $q = $this->db->query("SELECT * FROM tblPayment a LEFT JOIN tblAddress b on a.iAddressNbr=b.iAddressNbr WHERE a.iAddressNbr=? and  b.iCompanyNbr=?", array(
            $iAddressNbr,
            $this->companyid
        ));
        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return array();
        }
    }
    public function getPaymentListInvoice($iInvoiceNbr)
    {
        $q = $this->db->query("SELECT a.*,CONVERT(char(10), b.dPaymentDate,126) as dPaymentDate,b.cPaymentFormDescription
        FROM tblPaymentAssignment a left join tblPayment b on b.iPaymentNbr=a.iPaymentNbr WHERE a.iInvoiceNbr=?", array(
            $iInvoiceNbr
        ));
        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return array();
        }
    }
    public function update_Mandate_Date($iAddressNbr, $date)
    {
        $this->db->where('iAddressNbr', $iAddressNbr);
        $this->db->where('iTypeNbr', 2101);
        $this->db->update('tblAddressData', array(
            'cAddressData' => $date
        ));
    }
    public function insertIban($iAddressNbr, $cd)
    {
        if ($iAddressNbr) {
            $this->db->query("delete from tblAddressData where iAddressNbr=? and iTypeNbr IN (2098,2099,2100,2101,2102,2108)", array(
                $iAddressNbr
            ));
        }
        $this->db->query("EXEC spInsertAddressData " . $iAddressNbr . ", 5, 'IBAN', '" . $cd['iban'] . "', '',1");
        $this->db->query("EXEC spInsertAddressData " . $iAddressNbr . ", 5, 'BIC', '" . $cd['bic'] . "', '',1");
    }
    public function deleteIban($iAddressNbr)
    {
        $this->db->query("delete from tblAddressData where iAddressNbr=? and iTypeNbr=2098", array(
            $iAddressNbr
        ));
        $this->db->query("delete from tblAddressData where iAddressNbr=? and iTypeNbr=2099", array(
            $iAddressNbr
        ));
    }
    public function addMageboSEPA($iban, $bic, $iAddressNbr, $mandateid = false, $date = false)
    {
        if (!empty($iAddressNbr) && !empty($bic) && !empty($iban)) {
            //echo "OKI";exit;
            //First we check to see if there is already a SEPA Mandate for this customer
            //IF so, then we return an error.
            $qryAA = $this->db->query("SELECT * FROM tblAddressData where iTypeNbr=2100 AND iAddressNbr=?", array(
                $iAddressNbr
            ));
            if ($qryAA->num_rows() > 0) {
                if (strlen($qryAA->row()->cAddressData) > 0) {
                    //A Mandate already exists
                    //So we stop
                    return "Error - Mandate " . $qryAA->row()->cAddressData . "already exists for customer:" . $iAddressNbr;
                }
            }
            if (!empty($mandateid)) {
                $SepaMandate = $mandateid;
            } else {
                $SepaMandate = "SEPA" . str_pad($iAddressNbr, 10, "0", STR_PAD_LEFT);
            }
            if ($this->companyid == 54) {
                $SepaMandate = getSepaMandateIdTrendcall($iAddressNbr) . 'T';
            }
            //Use current Date as signature date
            if ($date) {
                $SepaMandateSignatureDate = $date;
            } else {
                $SepaMandateSignatureDate = gmdate('Y-m-d', time());
            }
            $SepaStatus = "SEPA FIRST";
            $SepaScheme = "CORE";
            //IBAN
            /*
            $queryAD3 = "EXEC spInsertAddressData " . $iAddressNbr . ", 5, 'IBAN', '" . $iban . "', '',1";
            $resultAD3 = mssql_query($queryAD3, $linkSQL) or die('Query failed: ' . $queryAD3);
            logActivity("SEPA ADD:" . $queryAD3);

            //BIC
            $queryAD4 = "EXEC spInsertAddressData " . $iAddressNbr . ", 5, 'BIC', '" . $bic . "', '',1";
            $resultAD4 = mssql_query($queryAD4, $linkSQL) or die('Query failed: ' . $queryAD4);
            logActivity("SEPA ADD:" . $queryAD4);
            */
            //SEPA MANDATE
            $this->db->query("EXEC spInsertAddressData " . $iAddressNbr . ", 5, 'SEPA MANDATE', '" . $SepaMandate . "', '',1");
            //$resultAD5 = mssql_query($queryAD5, $linkSQL) or die('Query failed: ' . $queryAD5);
            //logActivity("SEPA ADD:" . $queryAD5);
            //SEPA MANDATE
            $this->db->query("EXEC spInsertAddressData " . $iAddressNbr . ", 5, 'SEPA MANDATE SIGNATURE DATE', '" . $SepaMandateSignatureDate . "', '',1");
            //$resultAD6 = mssql_query($queryAD6, $linkSQL) or die('Query failed: ' . $queryAD6);
            //logActivity("SEPA ADD:" . $queryAD6);
            //SEPA STATUS
            $this->db->query("EXEC spInsertAddressData " . $iAddressNbr . ", 5, 'SEPA STATUS', '" . $SepaStatus . "', '',1");
            //$resultAD7 = mssql_query($queryAD7, $linkSQL) or die('Query failed: ' . $queryAD7);
            //logActivity("SEPA ADD:" . $queryAD7);
            //SEPA SCHEME
            $this->db->query("EXEC spInsertAddressData " . $iAddressNbr . ", 5, 'SEPA SCHEME', '" . $SepaScheme . "', '',1");
            //$resultAD8 = mssql_query($queryAD8, $linkSQL) or die('Query failed: ' . $queryAD8);
            //logActivity("SEPA ADD:" . $queryAD8);
            return (object) array(
                'SEPA_MANDATE' => $SepaMandate,
                'SEPA_MANDATE_SIGNATURE_DATE' => $SepaMandateSignatureDate,
                'SEPA_STATUS' => $SepaStatus
            );
        }
    }
    public function CheckSubscriptionPricing($InvoiceGroupNbr, $iGeneralPricingIndex)
    {
        $q = $this->db->query("SELECT * FROM GDC_ERP.dbo.tblGeneralPricing where iInvoiceGroupNbr = ? and iGeneralPricingIndex = ?", array(
            $InvoiceGroupNbr,
            $iGeneralPricingIndex
        ));
        if ($q->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
    public function UpgradeDowngrade($iGeneralPricingIndex, $date)
    {
        $q               = $this->db->query("select CASE when day('" . $date . "') = 1 THEN datediff(m, max(cast(case when iMonth < 10 then '0'+cast(iMonth as varchar) else cast(iMonth as varchar) end+'/' +'01'+'/' + cast(iYear as varchar) as datetime)), '" . $date . "')-1 ELSE datediff(m, max(cast(case when iMonth < 10 then '0'+cast(iMonth as varchar) else cast(iMonth as varchar) end+'/' +'01'+'/' + cast(iYear as varchar) as datetime)), '" . $date . "') END as Counter1 from tblPricedItems  where iGeneralPricingIndex = ? ", array(
            $iGeneralPricingIndex
        ));
        $c               = $q->row_array();
        $counter1        = $c['Counter1'];
        $r               = $this->db->query(" select count(*) as Counter2 from tblPricedItems where iGeneralPricingIndex = ? ", array(
            $iGeneralPricingIndex
        ));
        $c1              = $r->row_array();
        $counter2        = $c1['Counter2'];
        //echo "Counter:\n".$counter1." ".$counter2;
        $remaining_month = $counter1 + $counter2;
        $this->db->query("update tblGeneralPricing set iTotalMonths = ? where iGeneralPricingIndex =?", array(
            $remaining_month,
            $iGeneralPricingIndex
        ));
        $this->checkInvoiceNexmonth($iGeneralPricingIndex, $date);
        return $remaining_month;
    }
    public function migrateCallPricing($iGeneralPricingIndex, $date)
    {
        $this->db->query("update tblC_PincodeCallPriceList set dStopDate = ? where iPincode in ( select iPincode from tblC_Pincode where cInvoiceReference in ( select right(cRemark,14) from tblGeneralPricing where iGeneralPricingIndex = ? )) AND dStopDate is null", array(
            $date,
            $iGeneralPricingIndex
        ));
    }
    public function getLatestPriority($pincode)
    {
        $q   = $this->db->query("select max(iPriority)+ 1 as LatestPriority From tblC_PincodeCallPriceList where iPincode = ?", array(
            $pincode
        ));
        $res = $q->row_array();
        return $res['LatestPriority'];
        //
    }
    public function amendMageboSEPA($iban, $bic, $iAddressNbr)
    {
        //IBAN itypeNbr=2098
        $q                     = $this->db->query("SELECT * FROM tblAddressData where iTypeNbr=2143 AND iAddressNbr=?", array(
            $iAddressNbr
        ));
        //logActivity('SEPA Amendment Search:'.$qry2143);
        //$result2143= mssql_query($qry2143,$linkSQL);
        //$Row2143=mssql_fetch_assoc($result2143);
        $iAddressDataIndex2143 = $q->row()->iAddressDataIndex;
        if ($iAddressDataIndex2143 > 0) {
            //Amendment already exists
            //So we update
            $this->db->query("UPDATE tblAddressData SET cAddressData='" . $iban . "|" . $bic . "',bPreferredOrActive=1 WHERE iAddressDataIndex=?", array(
                $iAddressDataIndex2143
            ));
        } else {
            //There is no amendment
            //So we add one
            $this->db->query("EXEC spInsertAddressData " . $iAddressNbr . ", 5, 'SEPA AMENDMENT', '" . $iban . "|" . $bic . "', '',1");
        }
    }
    public function getBankDetails($iBankFileIndex)
    {
        $q = $this->db->query("SELECT a.*,b.*,d.cTypeDescription
    FROM tblBankFileDetail a
    LEFT JOIN  tblInvoice b ON b.iInvoiceNbr=a.iInvoiceNbr
    LEFT JOIN tblBankFile c ON c.iBankFileIndex=a.iBankFileIndex
    LEFT JOIN tblType d on d.iTypeNbr=c.iBankFileType
    WHERE a.iBankFileIndex=?", array(
            $iBankFileIndex
        ));
        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return false;
        }
    }
    public function disableGeneralPricing($index)
    {
        $this->db->where('iGeneralPricingIndex', $index);
        $this->db->update('tblGeneralPricing', array(
            'bEnabled' => 0
        ));
        return $this->db->affected_rows();
    }
    public function checkPayment($iInvoiceNbr)
    {
        // $invqry  = $this->db->query("SELECT iAddressNbr FROM tblPayment WHERE iAddressNbr=? AND dPaymentDate=? AND mPaymentAmount=? AND iPaymentForm=?", array());
    }
    /*
    function assignPayment(){

    $QueryInsert0Payment = "execute spInsertPayment " . $iAddressNbr2Invoice . ", '" . $PaymentDate . "', $Amount, '$paymentform', '$SeqNumberPaperStat', '$remark', " . $InvoiceNbr;
    echo "\n" . $QueryInsert0Payment;
    echo "\nPayment insert query : " . $QueryInsert0Payment . "\n";
    $resultInsert0Payment = mssql_query($QueryInsert0Payment, $linkmssql);
    $lineresultInsert0Payment = mssql_fetch_array($resultInsert0Payment);
    $PaymentNbr = $lineresultInsert0Payment['iNewPaymentNbr'];
    echo "PaymentNbr --> " . $PaymentNbr . "\n";

    //Assign Payment
    $QueryAssignPayment = "execute spAssignInvoicePayment " . $iAddressNbr2Invoice . ", " . $PaymentNbr . ", " . $InvoiceNbr . ", '$remark'";
    echo "Assignment query : " . $QueryAssignPayment . "\n";
    $resultAssignPayment = mssql_query($QueryAssignPayment, $linkmssql);

    }
    */
    public function getUnassignPayments($companyid, $iAddressNbr = false)
    {
        $q = $this->db->query("select P.iAddressNbr, P.iPaymentNbr, convert(varchar,dPaymentDate,103) as PaymentDate, P.mPaymentAmount, A.UsedAmount, P.mPaymentAmount - A.UsedAmount as AmountLeft,P.cPaymentFormDescription
from tblPayment P
 left join ( select iPaymentNbr, sum(mAssignedAmount) as UsedAmount
    from tblPaymentAssignment
    where iPaymentNbr in ( select iPaymentNbr from tblPayment
        where iAddressNbr in ( select iAddressNbr from tblAddress
            where iCompanyNbr = ? ))
    group by iPaymentNbr ) A ON A.iPaymentNbr = P.iPaymentNbr
where iAddressNbr in ( select iAddressNbr from tblAddress
    where iCompanyNbr = ? )
  AND P.mPaymentAmount <> A.UsedAmount", array(
            $companyid,
            $companyid
        ));
        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return false;
        }
    }
    public function apply_payment($postfields)
    {
        $invoice = $this->GetInvoice($postfields['iInvoiceNbr']);
        if ($invoice) {
            if ($invoice->mInvoiceAmount < $postfields['mPaymentAmount']) {
                $assign                       = 1;
                $postfields['mInvoiceAmount'] = $invoice->mInvoiceAmount;
            } else {
                $assign = 2;
            }
        }
        $ch = curl_init();
        if ($postfields['step'] == "1") {
            curl_setopt($ch, CURLOPT_URL, 'https://delta.united-telecom.nl:8443/localapi.php?step=1&assign=' . $assign);
        } elseif ($postfields["step"] == "2") {
            curl_setopt($ch, CURLOPT_URL, 'https://delta.united-telecom.nl:8443/localapi.php?step=2');
        }
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postfields));
        $response = curl_exec($ch);
        if (curl_error($ch)) {
            die('Unable to connect: ' . curl_errno($ch) . ' - ' . curl_error($ch));
        }
        curl_close($ch);
        $s = json_decode($response);
        // mail("mail@simson.one", "payments ID", print_r($postfields, true).print_r($s, true));
        return $s;
    }
    public function deleteClient($iAddressNbr)
    {
        if ($iAddressNbr) {
            $this->db->query("delete from tblInvoiceCondition where iAddressNbr = ?", array(
                $iAddressNbr
            ));
            $this->db->query("delete from tblAddressData where iAddressNbr= ?", array(
                $iAddressNbr
            ));
            $this->db->query("delete from tblAgent where iAddressNbr= ?", array(
                $iAddressNbr
            ));
            $this->db->query("delete from tblAddress where iAddressNbr= ?", array(
                $iAddressNbr
            ));
        }
    }
    public function deletePaymentAssignment($iPaymentNbr)
    {
        $this->db->query("DELETE FROM tblPaymentAssignment WHERE iPaymentNbr = ?", array(
            $iPaymentNbr
        ));
        return $this->db->affected_rows();
    }
    public function updateInvoiceStatus($InvoiceNbr)
    {
        $this->db->query("UPDATE tblBankFileDetail SET bRefused = 1 WHERE iInvoiceNbr = ?", array(
            $InvoiceNbr
        ));
        return $this->db->affected_rows();
    }
    public function getPaymentInformation($data)
    {
        if (strlen(trim($data->message)) == 12) {
            $iInvoiceNbr = substr(trim($data->message), 1);
            $iInvoiceNbr = substr($iInvoiceNbr, 0, -2);
        } elseif (strlen(trim($data->message)) == 9) {
            $iInvoiceNbr = $data->message;
        } else {
            $iInvoiceNbr = trim($data->message);
        }
        if (is_numeric($iInvoiceNbr)) {
            $q = $this->db->query("SELECT a.*,b.*,d.cTypeDescription
FROM GDC_ERP.dbo.tblPaymentAssignment a
LEFT JOIN tblPayment b ON b.iPaymentNbr=a.iPaymentNbr
LEFT JOIN tblBankFileDetail ff ON ff.iInvoiceNbr=a.iInvoiceNbr
LEFT JOIN tblBankFile c ON c.iBankFileIndex=ff.iBankFileIndex
    LEFT JOIN tblType d on d.iTypeNbr=c.iBankFileType

WHERE a.iInvoiceNbr=?
AND b.cPaymentRemark = ?", array(
                $iInvoiceNbr,
                $data->fileid
            ));
            if ($q->num_rows() > 0) {
                return $q->result();
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    public function getPaymentNumber($iInvoiceNbr, $amount)
    {
        $amount = $amount * -1;
        $q      = $this->db->query("select iPaymentNbr
from tblPaymentAssignment
where iInvoiceNbr = ? and mAssignedAmount = ?", array(
            $iInvoiceNbr,
            $amount
        ));
        if ($q->num_rows() > 0) {
            return $q->row()->iPaymentNbr;
        } else {
            return false;
        }
    }
    public function getPaymentDetail($iPaymentNbr)
    {
        $amount = $amount * -1;
        $q      = $this->db->query("select convert(varchar,dPaymentDate,120) as dPaymentDate,mPaymentAmount,cPaymentFormDescription
from tblPayment
where iPaymentNbr = ?", array(
            $iPaymentNbr
        ));
        if ($q->num_rows() > 0) {
            return $q->row();
        } else {
            return false;
        }
    }

    public function getPaymentInformationbyEnd2endId($data)
    {
        $q = $this->db->query("select iAddressNbr, cTypeDescription,I.iInvoiceNbr
        from tblBankFile B
         left join tblType T ON T.iTypeNbr = B.iBankFileType
         left join tblBankFileDetail D ON D.iBankFileIndex = B.iBankFileIndex
         left join tblInvoice I ON I.iInvoiceNbr = D.iInvoiceNbr
        where iBankFileDetailIndex = ?", array(
            $data->end2endid
        ));
        if ($q->num_rows() > 0) {
            return $q->row();
        } else {
            return false;
        }
    }
    public function getSinglePaymentInformation($data)
    {
        if (strlen(trim($data->message)) == 12) {
            $iInvoiceNbr = substr(trim($data->message), 1);
            $iInvoiceNbr = substr($iInvoiceNbr, 0, -2);
        } elseif (strlen(trim($data->message)) == 9) {
            $iInvoiceNbr = $data->message;
        } else {
            $iInvoiceNbr = trim($data->message);
        }
        $q = $this->db->query("SELECT a.*,b.*,d.cTypeDescription
FROM GDC_ERP.dbo.tblPaymentAssignment a
LEFT JOIN tblPayment b ON b.iPaymentNbr=a.iPaymentNbr
LEFT JOIN tblBankFileDetail ff ON ff.iInvoiceNbr=a.iInvoiceNbr
LEFT JOIN tblBankFile c ON c.iBankFileIndex=ff.iBankFileIndex
    LEFT JOIN tblType d on d.iTypeNbr=c.iBankFileType
WHERE a.iInvoiceNbr=?", array(
            $iInvoiceNbr
        ));
        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return false;
        }
    }
    public function updateSepaStatus($iAddressNbr)
    {
        $this->db->query("Update tblAddressData set cAddressData = 'SEPA FIRST' where iAddressNbr = ? and iTypeNbr = 2102", array(
            $iAddressNbr
        ));
        return $this->db->affected_rows();
    }
    public function assignPaymentPayment($NewPayment, $OldPayment)
    {
        $queryInsertPayment2 = "execute spAssignPaymentPayment " . $NewPayment . ", " . $OldPayment;
        log_message("error", "Payment new :" . $NewPayment . ", OLD:" . $OldPayment);
        $this->db->query($queryInsertPayment2);
    }
    public function getPincodeRef($number)
    {
        $q = $this->db->query("select * from tblC_Pincode where iPincode= ?", array(
            $number
        ));
        if ($q->num_rows() > 0) {
            return $q->row()->cInvoiceReference;
        } else {
            //06-48 92 49 92
            $n = substr($number, 2);
            return "0" . $n[0] . "-" . $n[1] . $n[2] . " " . $n[3] . $n[4] . " " . $n[5] . $n[6] . " " . $n[7] . $n[8];
        }
    }
    public function getiAddress($id)
    {
        $s = $this->db->query("select  a.dCreationDate as date_created,a.iAddressNbr as mageboid,  a.iCompanyNbr as companyid,a.cName as lastname, a.cStreet as address1, a.iZipCode as postcode, a.cCity as city, b.cCountryCode as country, c.cInternalLanguage as language from tblAddress a left join tblCountry b on b.iCountryIndex=a.iCountryIndex left join tblLanguages c on c.iLanguageIndex=a.iLanguageIndex where a.iAddressNbr=?", array(
            $id
        ));
        $f = $s->row_array();
        $a = $this->getAddressData($id);
        if ($a) {
            $res = array_merge($f, $a);
            if (!empty($res['vat'])) {
                $f['companyname'] = $f['lastname'];
            }
        } else {
            $res = $f;
        }
        $res['mvno_id']  = $f['mageboid'];
        $res['language'] = strtolower($res['language']);
        $res['uuid']     = guidv4();
        $res['password'] = password_hash(random_str('alphanum', 12), PASSWORD_DEFAULT);
        if (!isset($res['email'])) {
            $res['email']         = 'client_' . $f['mageboid'] . '@united-telecom.be';
            $res['invoice_email'] = "no";
        } else {
            $res['invoice_email'] = "yes";
        }
        if (empty($res['postcode'])) {
            $res['postcode'] = "0000";
        }
        // print_r($res);
        return $res;
    }
    public function getAddressData($iAddressNbr)
    {
        $a = array();
        $q = $this->db->query("select b.cTypeDescription, a.cAddressData from tblAddressData a left join tblType b on b.iTypeNbr=a.iTypeNbr where a.iAddressNbr=? and bPreferredOrActive=1", array(
            $iAddressNbr
        ));
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $r) {
                if (trim($r->cTypeDescription) == "TELEPHONENUMBER") {
                    $a['phonenumber'] = $r->cAddressData;
                }
                if (trim($r->cTypeDescription) == "VAT NUMBER") {
                    $a['vat'] = $r->cAddressData;
                }
                if (trim($r->cTypeDescription) == "E-MAIL") {
                    $a['email'] = $r->cAddressData;
                }
                if (trim($r->cTypeDescription) == "BIRTHDATE") {
                    $a['date_birth'] = $r->cAddressData;
                }
                if (trim($r->cTypeDescription) == "IDENTITY CARD NUMBER") {
                    $a['nationalnr'] = $r->cAddressData;
                }
                if (trim($r->cTypeDescription) == "IBAN") {
                    $a['iban'] = $r->cAddressData;
                }
                if (trim($r->cTypeDescription) == "BIC") {
                    $a['bic'] = $r->cAddressData;
                }
                if (trim($r->cTypeDescription) == "GENDER") {
                    $a['gender'] = $r->cAddressData;
                }
            }
        }
        return $a;
    }
    public function getCompanyPrintType($companyid)
    {
        $q = $this->db->query("select cCompany, iDefaultInvoicePrintType from tblCompany where iCompanyNbr=?", array(
            $companyid
        ));
        if ($q->num_rows() > 0) {
            return $q->row();
        } else {
            return false;
        }
    }
    public function getCompanyPrintLayout($iInvoicePrintNbr)
    {
        $q = $this->db->query("select cPrintTypeDescription from tblInvoicePrintType where iInvoicePrintType =?", array(
            $iInvoicePrintNbr
        ));
        if ($q->num_rows() > 0) {
            return $q->row()->cPrintTypeDescription;
        } else {
            return false;
        }
    }
    public function createCn($data)
    {
        $mvno_setting = globofix($data['companyid']);
        /*
        invoicenum
        companyid
        payment_duedays
        date
        duedate
        amount
        vat_rate
        message
        mageboid
        */
        //$this->updateFlag('479', 0);
        if ($this->companyid != $data['companyid']) {
            return array(
                'result' => 'error',
                'message' => 'Access Denied'
            );
        }
        $company = $this->getCompanyPrintType($data['companyid']);
        if (!$company) {
            return array(
                'result' => 'error',
                'message' => 'Company Not Found'
            );
        }
        $check_flag = $this->getFlag($mvno_setting->magebo_flag);
        if ($check_flag) {
            return array(
                'result' => 'error',
                'message' => 'Billing Temporary Blocked for maintenance, Please try again later'
            );
        }
        $this->updateFlag($mvno_setting->magebo_flag, 1);
        $iInvoicePrintNbr = $company->iDefaultInvoicePrintType; //TypeNumber
        $CompanyName      = $company->cCompany; //companyname
        $layout           = $this->getCompanyPrintLayout($iInvoicePrintNbr); //get Layout Name
        $iTermOfPayment   = $data['payment_duedays'];
        $iInvoiceNbr      = $this->getNewInvoiceNbr($CompanyName);
        $lastDate         = $this->GetLastInvoice($CompanyName, 41);
        if ($lastDate > $data['date']) {
            $this->updateFlag($mvno_setting->magebo_flag, 0);
            return array(
                'result' => 'error',
                'message' => 'Your Invoice date can not be lower than ' . $lastDate . ' you send ' . $data['date']
            );
        }
        $invoice_date    = convert_invoice_date($data['date']);
        $invoice_duedate = convert_invoice_date($data['duedate']);
        /* $date = new DateTime();
        $date->add(new DateInterval('P10D'));
        echo $date->format('Y-m-d') . "\n";
        */
        // Insert the Invoice
        $amount          = str_replace(',', '.', $data['amount']) * -1;
        $this->db->query("EXEC spInsertInvoice " . $iInvoiceNbr . ", " . $data['mageboid'] . ", 'CREDITNOTE', '" . $invoice_date . "', '" . $invoice_duedate . "', " . $amount . ", '', 'OPEN', '" . $layout . "'");
        //Insert the Invoice in the table for PDF printing
        $this->db->query("EXEC spInsertInvoiceEmailPDF " . $data['mageboid'] . ", " . $iInvoiceNbr . ",'" . $layout . "'");
        //Insert InvoiceDetail line :
        $this->db->query("EXEC spInsertInvoiceDetail " . $iInvoiceNbr . ", 348, '" . $data['message'] . "', 1, " . exvat4($data['vat_rate'], str_replace(',', '.', $data['amount'])) . ", " . $data['vat_rate'] . ", NULL, 1");
        $this->updateFlag($mvno_setting->magebo_flag, 0);
        if ($this->getNewInvoiceNbr($CompanyName) > $iInvoiceNbr) {
            $Invoice = $this->getInvoice($data['invoicenum']);
            if ($Invoice->iInvoiceStatus == 54) {
                $this->apply_payment(array(
                    'cPaymentForm' => 'INTERN',
                    'mPaymentAmount' => $amount,
                    'cPaymentFormDescription' => "Credit Nota - " . $iInvoiceNbr,
                    'dPaymentDate' => date("m/d/Y", strtotime($data['date'])),
                    'iInvoiceNbr' => $iInvoiceNbr,
                    'step' => 1,
                    'iAddressNbr' => $data['mageboid'],
                    'cPaymentRemark' => 'Creditnote'
                ));
                $this->apply_payment(array(
                    'cPaymentForm' => 'INTERN',
                    'mPaymentAmount' => $amount * -1,
                    'cPaymentFormDescription' => "Credit Nota - " . $iInvoiceNbr,
                    'dPaymentDate' => date("m/d/Y", strtotime($data['date'])),
                    'iInvoiceNbr' => $iInvoiceNbr,
                    'step' => 1,
                    'iAddressNbr' => $data['mageboid'],
                    'cPaymentRemark' => 'Creditnote'
                ));
            } else {
                // if (empty($_POST['iAddressNbr']) && empty($_POST['dPaymentDate']) && empty($_POST['mPaymentAmount']) && empty($_POST['cPaymentForm']) && empty($_POST['cPaymentFormDescription']) && empty($_POST['cPaymentRemark']) && empty($_POST['iInvoiceNbr'])) {
                $this->apply_payment(array(
                    'cPaymentForm' => 'INTERN',
                    'mPaymentAmount' => $amount,
                    'cPaymentFormDescription' => "Credit Nota - " . $iInvoiceNbr,
                    'dPaymentDate' => date("m/d/Y", strtotime($data['date'])),
                    'iInvoiceNbr' => $iInvoiceNbr,
                    'step' => 1,
                    'iAddressNbr' => $data['mageboid'],
                    'cPaymentRemark' => 'Creditnote'
                ));
                $this->apply_payment(array(
                    'cPaymentForm' => 'INTERN',
                    'mPaymentAmount' => str_replace(',', '.', $data['amount']),
                    'cPaymentFormDescription' => "Credit Nota - " . $data['invoicenum'],
                    'dPaymentDate' => date("m/d/Y", strtotime($data['date'])),
                    'iInvoiceNbr' => $data['invoicenum'],
                    'step' => 1,
                    'iAddressNbr' => $data['mageboid'],
                    'cPaymentRemark' => 'Creditnote'
                ));
            }
            //applay payment to Creditnote
            /*
            $this->apply_payment(array(
            'cPaymentForm' => $paymentform,
            'mPaymentAmount' => $data['amount']*-1,
            'cPaymentFormDescription' => "Credit Nota - " . $iInvoiceNbr,
            'dPaymentDate' => date("m/d/Y", strtotime($data['date'])),
            'iInvoiceNbr' => $iInvoiceNbr,
            'step' => 1,
            'iAddressNbr' => $data['mageboid'],
            'cPaymentRemark' => 'Creditnote'
            ));
            */
            return array(
                'result' => 'success',
                'iInvoiceNbr' => $iInvoiceNbr
            );
        } else {
            return array(
                'result' => 'error',
                'message' => 'Invoice was not created'
            );
        }
    }
    public function createInvoice($data)
    {
        $mvno_setting = globofix($data['companyid']);
        $trxid        = $data['transactionid'];
        /*
        invoicenum
        companyid
        payment_duedays
        date
        duedate
        amount
        vat_rate
        message
        mageboid
        */
        //$this->updateFlag('479', 0);
        if ($this->companyid != $data['companyid']) {
            return array(
                'result' => 'error',
                'message' => 'Access Denied Hack attempt'
            );
        }
        $company = $this->getCompanyPrintType($data['companyid']);
        if (!$company) {
            return array(
                'result' => 'error',
                'message' => 'Company Not Found'
            );
        }
        $check_flag = $this->getFlag($mvno_setting->magebo_flag);
        if ($check_flag) {
            return array(
                'result' => 'error',
                'message' => 'Billing Temporary Blocked for maintenance, Please try again later'
            );
        }
        $this->updateFlag($mvno_setting->magebo_flag, 1);
        $iInvoicePrintNbr = $company->iDefaultInvoicePrintType; //TypeNumber
        $CompanyName      = $company->cCompany; //companyname
        $layout           = $this->getCompanyPrintLayout($iInvoicePrintNbr); //get Layout Name
        $iTermOfPayment   = $data['payment_duedays'];
        $iInvoiceNbr      = $this->getNewInvoiceNbr($CompanyName);
        $lastDate         = $this->GetLastInvoice($CompanyName, 41);
        if ($lastDate > $data['date']) {
            $this->updateFlag($mvno_setting->magebo_flag, 0);
            return array(
                'result' => 'error',
                'message' => 'Your Invoice date can not be lower than ' . $lastDate . ' you send ' . $data['date']
            );
        }
        $invoice_date    = convert_invoice_date($data['date']);
        $invoice_duedate = convert_invoice_date($data['duedate']);
        /* $date = new DateTime();
        $date->add(new DateInterval('P10D'));
        echo $date->format('Y-m-d') . "\n";
        */
        // Insert the Invoice
        $amount          = $data['amount'];
        $this->db->query("EXEC spInsertInvoice " . $iInvoiceNbr . ", " . $data['mageboid'] . ", 'INVOICE', '" . $invoice_date . "', '" . $invoice_duedate . "', " . $amount . ", '', 'OPEN', '" . $layout . "'");
        //Insert the Invoice in the table for PDF printing
        $this->db->query("EXEC spInsertInvoiceEmailPDF " . $data['mageboid'] . ", " . $iInvoiceNbr . ",'" . $layout . "'");
        //Insert InvoiceDetail line :
        $this->db->query("EXEC spInsertInvoiceDetail " . $iInvoiceNbr . ", 348, '" . $data['message'] . "', 1, " . exvat4($data['vat_rate'], $data['amount']) . ", " . $data['vat_rate'] . ", NULL, 1");
        $this->updateFlag($mvno_setting->magebo_flag, 0);
        if ($this->getNewInvoiceNbr($CompanyName) > $iInvoiceNbr) {
            // $Invoice = $this->getInvoice($data['invoicenum']);
            return array(
                'result' => 'success',
                'iInvoiceNbr' => $iInvoiceNbr
            );
        } else {
            return array(
                'result' => 'error',
                'message' => 'Invoice was not created'
            );
        }
    }
    public function spInsertPayment($postfields, $assign = 1)
    {
        /*
        if (empty($postfields['iAddressNbr']) && empty($postfields['dPaymentDate']) && empty($postfields['mPaymentAmount']) && empty($postfields['cPaymentForm']) && empty($postfields['cPaymentFormDescription']) && empty($postfields['cPaymentRemark']) && empty($postfields['iInvoiceNbr'])) {
        return (object)array('result' => false, 'message' => 'Not complete');

        }

        $queryInsertPayment = "execute spInsertPayment '" . $postfields['iAddressNbr'] . "', '" . $postfields['dPaymentDate'] . "', '" . $postfields['mPaymentAmount'] . "', '" . $postfields['cPaymentForm'] . "', '" . $postfields['cPaymentFormDescription'] . "', '" . $postfields['cPaymentRemark'] . "', '0'";

        $result = $this->dbv5->query($query);
        $lineInsertPayment =  $result->row_array();
        $NewPayment = $lineInsertPayment['iNewPaymentNbr'];

        if ($NewPayment > 0) {
        $queryInsertPayment2 = "execute spAssignInvoicePayment '" . $postfields['iAddressNbr'] . "', '" . $NewPayment . "', '" . $postfields['iInvoiceNbr'] . "', ''";
        $res = $this->db->query($queryInsertPayment2);

        return (object)array('result' => true, 'id' => $NewPayment, 'overpayment' => $assign);
        } else {
        return (object)array('result' => false, 'message' => 'Not complete');
        }
        */
    }
}
