<?php

class Mssp {

/**
* @var resource The connection resource
* @access protected
*/
protected $db = null;

/**
* @var resource The statement resource identifier
* @access protected
*/
protected $stmt = null;

/**
* Constructor opens a connection to the database
*
*/
public function __construct() {

	try {
		//"dblib:host=$this->hostname:$this->port;dbname=$this->dbname", "$this->username", "$this->pwd"
		$this->db = new PDO("dblib:host=10.17.2.50:1433t;dbname=GDC_ERP", "Spongebobke", "spongebobke");

	}   catch (PDOException $e) {      

		print "Error Bow!: " . $e->getMessage() . "<br/>";
		die();
	}   
}

/**
* Destructor closes the statement and connection
*
*/
public function __destruct() {
	
	if ($this->stmt)
		unset($this->stmt);
	if ($this->db)
		unset($this->db);
	
}

/**
* Get Datatable function
*
*/
public function getDataTable($sTable, $sIndexColumn, $aColumns ) {

	/* Ordering */
	$sOrder = "";
	if ( isset( $_GET['iSortCol_0'] ) ) {
		$sOrder = "ORDER BY  ";
		for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ ) {
			if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" ) {
				$sOrder .= $aColumns[ intval( $_GET['iSortCol_'.$i] ) ]."
				".addslashes( $_GET['sSortDir_'.$i] ) .", ";
			}
		}
		$sOrder = substr_replace( $sOrder, "", -2 );
		if ( $sOrder == "ORDER BY" ) {
			$sOrder = "";
		}
	}

	/* Filtering */
	$sWhere = "";
	if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" ) {
		$sWhere = "WHERE (";
			for ( $i=0 ; $i<count($aColumns) ; $i++ ) {
				$sWhere .= $aColumns[$i]." LIKE '%".addslashes( $_GET['sSearch'] )."%' OR ";
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
	}

	/* Individual column filtering */
	for ( $i=0 ; $i<count($aColumns) ; $i++ ) {
		if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )  {
			if ( $sWhere == "" ) {
				$sWhere = "WHERE ";
			} else {
				$sWhere .= " AND ";
			}
			$sWhere .= $aColumns[$i]." LIKE '%".addslashes($_GET['sSearch_'.$i])."%' ";
		}
	}

	/* Paging */
	$top = (isset($_GET['iDisplayStart']))?((int)$_GET['iDisplayStart']):0 ;
	$limit = (isset($_GET['iDisplayLength']))?((int)$_GET['iDisplayLength'] ):10;
	$sQuery = "SELECT TOP $limit ".implode(",",$aColumns)."
				FROM $sTable
				$sWhere ".(($sWhere=="")?" WHERE ":" AND ")." $sIndexColumn NOT IN
				(
					SELECT $sIndexColumn FROM
					(
						SELECT TOP $top ".implode(",",$aColumns)."
						FROM $sTable
						$sWhere
						$sOrder
						)
				as [virtTable]
				)
				$sOrder";

	// SQL queries get data to display
	$this->stmt = $this->db->prepare($sQuery);

	// Bind parameters
	if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" ) {
		$this->stmt->bindValue(':search', '%'.$_GET['sSearch'].'%', PDO::PARAM_STR);
	}
	for ( $i=0 ; $i<count($aColumns) ; $i++ ) {
		if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' ) {
			$this->stmt->bindValue(':search'.$i, '%'.$_GET['sSearch_'.$i].'%', PDO::PARAM_STR);
		}
	}

	// Get data from db
	$this->stmt->execute();
	$rResult = $this->stmt->fetchAll();

	//Get total number of filtered rows
	$sQueryCnt = "SELECT COUNT(*) FROM $sTable $sWhere";
	$iFilteredTotal = current($this->db->query($sQueryCnt)->fetch());

	// Get total number of rows in table
	$sQuery = "SELECT COUNT(*) FROM $sTable";
	$iTotal = current($this->db->query($sQuery)->fetch());

	// Add datatable vars
	$output = array(
		"sEcho" => intval($_GET['sEcho']),
		"iTotalRecords" => $iTotal,
		"iTotalDisplayRecords" => $iFilteredTotal,
		"aaData" => array()
		);

	// Return array of values
	foreach($rResult as $aRow) {
		$row = array();         
		for ( $i = 0; $i < count($aColumns ); $i++ ) {
			if ( $aColumns [$i] != ' ' ) {
				$v = $aRow[ $aColumns[$i] ];
				$v = mb_check_encoding($v, 'UTF-8') ? $v : utf8_encode($v);
				$row[]=$v;
			}
		}
		$output['aaData'][] = $row;
	}

	// Return JSON data
	echo json_encode( $output );
}
}
