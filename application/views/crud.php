<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php 
foreach($css_files as $file): ?>
	<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
</head>
<body>
	<div>
	
		
		<a href='<?php echo site_url('admin/masterdb/configuration')?>'>Configuration</a> |
		<a href='<?php echo site_url('admin/masterdb/clients')?>'>Clients</a> | 
		<a href='<?php echo site_url('admin/masterdb/products')?>'>Products</a> |
		<a href='<?php echo site_url('admin/masterdb/services')?>'>Services</a> |	
		<a href='<?php echo site_url('admin/masterdb/service_mobile')?>'>Service Mobile</a> |	 
		<a href='<?php echo site_url('admin/masterdb/a_event_status')?>'>Events Status</a> |
		<a href='<?php echo site_url('admin/masterdb/api_logs')?>'>Api log</a> |
		<a href='<?php echo site_url('admin/masterdb/a_email_templates')?>'>Email Templates</a> |
		<a href='<?php echo site_url('admin/masterdb/logs')?>'>Crm Log</a>
		
	</div>
	<div style='height:20px;'></div>  
    <div style="padding: 10px">
		<?php echo $output; ?>
    </div>
    <?php foreach($js_files as $file): ?>
        <script src="<?php echo $file; ?>"></script>
    <?php endforeach; ?>
</body>
</html>
