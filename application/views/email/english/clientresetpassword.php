<p></p>
<p>Beste <?php echo $name; ?>,</p>
<p>U heeft ons verzocht uw wachtwoord te wijzigen. Klik alstublieft op de knop “Wachtwoord wijzigen” of vul de code in op de Wijzigingspagina.</p>
<p></p>
<p>Code: <?php echo trim($code); ?></p>
<p></p>
<table style="BACKGROUND: #e47321; border-radius: 6px" cellspacing="0" cellpadding="0">
	<tbody>
		<tr>
			<td height="40" width="20"><a href="<?php echo base_url(); ?>client/auth/resetwithcode/<?php echo $code; ?>" target="_blank"></a></td>
			<td style="FONT-SIZE: 16px; FONT-FAMILY: Arial, Helvetica, sans-serif" height="40"><a style="TEXT-DECORATION: none; COLOR: #ffffff" href="<?php echo base_url(); ?>client/auth/resetwithcode/<?php echo $code; ?>" target="_blank"><strong><?php echo lang('Reset password'); ?>
			</strong></a></td>
			<td height="40" width="20"><a href="<?php echo base_url(); ?>client/auth/resetwithcode/<?php echo $code; ?>" target="_blank"></a></td>
		</tr>
	</tbody>
</table>
<p></p>
<p><?php echo lang('Do you have any question? look up our'); ?> <a href="http://www.delta.nl/mobile" target="_blank"><?php echo lang('website'); ?></a> <?php echo lang('get in touch with us'); ?></p>
<p><?php echo lang('We are glad to help you'); ?>.</p>
<p><?php echo lang('Kind Regards'); ?>,</p>
<p></p>
<p style="FONT-SIZE: 14px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; LINE-HEIGHT: 16px" align="left">
Ludolf Rasterhoff <br><br>Directeur Telecom</p>