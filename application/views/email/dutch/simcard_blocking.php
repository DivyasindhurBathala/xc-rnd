<p></p>
<p style="color: #111;">Beste <?php echo $client->lastname; ?>,
<p></p>
<p style="color: #111;">
U heeft een blokkade van uw simkaart aangevraagd. Uw simkaart is vanaf nu geblokkeerd.
</p>
<p></p>
<?php if ($send_new_sim) {?>
<p style="color: #111;">
Wij sturen u zo spoedig mogelijk een nieuwe simkaart toe. Reken op een verwerkingstijd van ongeveer 3 werkdagen.
</p>
<?php } else {?>
<p style="color: #111;">Uw abonnement loopt nog door. Wilt u uw simkaart weer activeren? Neem dan contact op met onze klantenservice.
</p>
<?php }?>
<p></p>
<p style="color: #111;">Vragen? We helpen u graag. De meeste antwoorden vindt u hier.</p>
<p></p>
<p style="color: #111;">Vriendelijke groet,<br />
DELTA-klantenservice</p>
