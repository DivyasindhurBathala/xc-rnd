<p></p>
<p style="color: #111;">Beste <?php echo $client->lastname; ?>,
<p></p>
<p style="color: #111;">Onlangs hebben wij u een betalingsherinnering verzonden betreffende uw openstaande factuur voor DELTA Mobiel. Wij hebben helaas nog geen betaling ontvangen. Misschien is de factuur aan uw aandacht ontsnapt. Op dit moment staat er een bedrag open van € <?php echo $invoice->mInvoiceAmount; ?>,–, betreffende factuurnummer <?php echo $invoice->iInvoiceNbr; ?>.</p>
<p></p>
<p style="color: #111;">Blijf bereikbaar door te betalen</p>
<p style="color: #111;">Wij verzoeken u het openstaande bedrag binnen 14 dagen aan ons over te maken. Dit kan op rekeningnummer  <b>NL76 INGB 0008 5195 20</b>, onder vermelding van <?php echo $invoice->iInvoiceNbr; ?>. Betaalt u niet op tijd? Dan blokkeren wij uw abonnement. U bent dan niet meer bereikbaar en betaalt mogelijk extra administratiekosten.</p>
<p></p>
<!-- in case the customer ordered also a handset over €250,- subsidy … use this text section -->
<?php if($subsidy){ ?>
<p style="color: #111;">Registratie BKR</p>
<p style="color: #111;">U heeft ook een kredietovereenkomst afgesloten voor een telefoon met een restbedrag van €250,– of meer. Daardoor zijn wij verplicht u te registreren bij het BKR als u twee betaaltermijnen mist.</p>
<p></p>
<p style="color: #111;">Heeft uw betaling dit bericht gekruist? Dan kunt u deze betalingsherinnering als niet verzonden beschouwen.</p>
<p></p>
<p style="color: #111;">Vriendelijke groet,<br />
DELTA-klantenservice</p>
