<p></p>
<p>Beste <?php echo $name; ?>,</p>
<p>U wachtwoord op MijnDelta Mobiel is gewijzigd.</p>
<p></p>
<p><?php echo lang('Do you have any question? look up our'); ?> <a href="http://www.delta.nl/mobile" target="_blank"><?php echo lang('website'); ?></a> <?php echo lang('get in touch with us'); ?></p>
<p><?php echo lang('We are glad to help you'); ?>.</p>
<p><?php echo lang('Kind Regards'); ?>,</p>
<p></p>
<p style="FONT-SIZE: 14px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; LINE-HEIGHT: 16px" align="left">
Ludolf Rasterhoff <br><br>Directeur Telecom</p>