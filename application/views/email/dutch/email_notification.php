<p>Beste <?php echo ucfirst($client->lastname); ?>,</p>

<p>Uw factuur over de maand<?php echo ucfirst($month); ?> staat voor u klaar op MijnDELTA Mobiel. Deze factuur is helaas later beschikbaar dan u waarschijnlijk had verwacht. De abonnementskosten van <b>€ <?php echo strtoupper($invoice->amount); ?> </b>worden binnen ongeveer vier werkdagen afgeschreven.</p>

<p>De factuur van de maand oktober verwachten wij deze maand ook beschikbaar te kunnen stellen. Hier ontvangt u vooraf nog een aankondiging van.</p>

<p>Op de factuur staan (nu nog) voornamelijk onze ex BTW prijzen vermeld. Hierbij een aantal voorbeeld bedragen, zodat u toch eenvoudig de bedragen kunt herkennen:</p>

<table border="1" cellspacing="0" cellpadding="0" bgcolor="#2f5da8">
<tbody>
<tr>
<td valign="top" ><p>Omschrijving</p></td>
<td valign="top" ><p>exclusief BTW</p></td>
<td valign="top" ><p>inclusief BTW</p></td>
</tr>
<?php foreach ($invoice->lines as $lines) {?>
<tr>
<td valign="top" ><p>DELTA Mobiel 1,25GB of 2,5GB-combi</p></td>
<td valign="top" ><p>€ 16,53</p></td>
<td valign="top" ><p>€ 20,- </p></td>
</tr>

<?php }?>

</tbody>
</table>

<p>Op MijnDELTA Mobiel kunt u ook terecht voor eerdere facturen, instellingen en verbruikskosten.</p>

<table border="1" cellspacing="0" cellpadding="0" >
<tbody>
<tr>
<td> </td>
<td><p><b><a href="https://mijnmobiel.delta.nl/client/auth">naar MijnDelta Mobiel</a></b></p></td>
<td> </td>
</tr>

</tbody>
</table>

<p>Vragen? We helpen u graag. De meeste antwoorden vindt u <a href="https://www.delta.nl/mobiel">hier</a>.</p>

<p>Vriendelijke groet,</p>

<p>DELTA-klantenservice</p>
