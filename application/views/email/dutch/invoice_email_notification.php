<p style="color: #111;">Beste <?php echo ucfirst($client->lastname); ?>,</p>

<p style="color: #111;">Uw factuur over de maand september staat voor u klaar op MijnDELTA Mobiel. Deze factuur is helaas later beschikbaar dan u waarschijnlijk had verwacht. De abonnementskosten van <b>€ <?php echo number_format($invoice->mInvoiceAmount, 2); ?> </b>worden binnen ongeveer vier werkdagen afgeschreven.</p>

<p style="color: #111;">De factuur van de maand oktober verwachten wij deze maand ook beschikbaar te kunnen stellen. Hier ontvangt u vooraf nog een aankondiging van.</p>

<p style="color: #111;">Op de factuur staan (nu nog) voornamelijk onze ex BTW prijzen vermeld. Hierbij een aantal voorbeeld bedragen, zodat u toch eenvoudig de bedragen kunt herkennen:</p>

<table border="1" cellspacing="2" cellpadding="1" width="650">
<tbody>
<tr bgcolor="#2f5da8" style="color: #FFF;">
<td align="left" width="70%"><p>Omschrijving</p></td>
<td align="right" width="15%"><p>exclusief BTW</p></td>
<td align="right" width="15%"><p>inclusief BTW</p></td>
</tr>
<?php foreach ($invoice->items as $lines) {?>
<tr>
<td valign="left" ><p><?php echo $lines['cInvoiceDetailDescription']; ?></p></td>
<td align="right" ><p>€ <?php echo number_format($lines['mUnitPrice'], 2); ?></p></td>
<td align="right" ><p>€ <?php echo number_format(includevat($lines['rVATPercentage'], $lines['mUnitPrice']), 2); ?>,- </p></td>
</tr>

<?php }?>

</tbody>
</table>

<p style="color: #111;">Op MijnDELTA Mobiel kunt u ook terecht voor eerdere facturen, instellingen en verbruikskosten.</p>

<table border="0.5" cellspacing="1" cellpadding="2" >
<tbody>
<tr bgcolor="#eaa309">
<td> </td>
<td><p><b><a style="text-decoration: none; color: #fff;" href="https://mijnmobiel.delta.nl/client/auth">naar MijnDelta Mobiel</a></b></p></td>
<td> </td>
</tr>

</tbody>
</table>

<p style="color: #111;">Vragen? We helpen u graag. De meeste antwoorden vindt u <a href="https://www.delta.nl/mobiel">hier</a>.</p>

<p style="color: #111;">Vriendelijke groet,</p>

<p style="color: #111;">DELTA-klantenservice</p>
