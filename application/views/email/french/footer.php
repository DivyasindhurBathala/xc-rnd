  </P>


            </TD>
            <TD class=margin_small bgColor=#ffffff
                width=1></TD>
          </TR>
          </TBODY>
        </TABLE>
      </TD>
    </TR>
    </TBODY>
  </TABLE>
</DIV>
<DIV align=center>

<DIV align=center>
   <TABLE cellSpacing=0 cellPadding=0 width="100%" align=center border=0>
   <TBODY>
      <TR>
         <TD bgColor=#ffffff height=39></TD>
         <TD class=main style="MAX-WIDTH: 678px; WIDTH: 678px" bgColor=#ffffff></TD>
         <TD bgColor=#ffffff></TD>
      </TR>
      <TR>
         <TD style="FONT-SIZE: 1px; LINE-HEIGHT: 1px" bgColor=#1d609a height=1></TD>
         <TD style="FONT-SIZE: 1px; LINE-HEIGHT: 1px" bgColor=#1d609a></TD>
         <TD style="FONT-SIZE: 1px; LINE-HEIGHT: 1px" bgColor=#1d609a></TD>
      </TR>
      <TR>
         <TD bgColor=#1d609a height=40></TD>
         <TD bgColor=#1d609a></TD>
         <TD bgColor=#1d609a></TD>
      </TR>
      <TR>
         <TD bgColor=#1d609a>&nbsp;</TD>
         <TD bgColor=#1d609a>
            <TABLE class=main cellSpacing=0 cellPadding=0 width=678px align=center bgColor=#1d609a border=0>
               <TBODY>
                  <TR>
                     <TABLE class=main cellSpacing=0 cellPadding=0 width=678px align=center bgColor=#1d609a border=0>
                        <TD><a style="FONT-SIZE: 14px; FONT-FAMILY: Arial, Helvetica, sans-serif; FONT-WEIGHT: bold; COLOR: #ffffff; LINE-HEIGHT: 12px; TEXT-DECORATION: none;" vAlign=middle title=Mijn DELTA href="https://www.delta.nl/MijnDELTA" target="_blank">Mijn DELTA</a></TD>
                        <TD width=80>&nbsp;</TD>
                        <TD><a style="FONT-SIZE: 14px; FONT-FAMILY: Arial, Helvetica, sans-serif; FONT-WEIGHT: bold; COLOR: #ffffff; LINE-HEIGHT: 12px; TEXT-DECORATION: none;" vAlign=middle title=Privacy href="https://www.delta.nl/privacyverklaring" target="_blank">Privacy</a></TD>
                        <TD width=70>&nbsp;</TD>
                        <TD class=margin_small bgColor=#1d609a width=1></TD>
                        <TD bgColor=#1d609a align=left width=200><a title="Facebook" href="https://www.facebook.com/DeltaNL/" target="_blank"><IMG border=0 alt=Fb src="<?php echo base_url(); ?>assets/img/256d5dc20e17136259025708d848659d.gif" width=20 height=20></A>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a title="LinkedIn" href="https://www.linkedin.com/company/deltaverbindt" target="_blank"><IMG border=0 alt=Li src="<?php echo base_url(); ?>assets/img/51b8f5bd6809d179636da4f848cc2d9e.gif" width=20 height=20></A>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a title="YouTube" href="https://www.youtube.com/deltaverbindt" target="_blank"><IMG border=0 alt=Yt src="<?php echo base_url(); ?>assets/img/4c866e761184014facae812e6b251acd.gif" width=20 height=20></A>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a title="Twitter" href="https://twitter.com/deltaverbindt" target="_blank"><IMG border=0 alt=Tw src="<?php echo base_url(); ?>assets/img/beb89e9efb7f2b48bf896d00e13ee6a5.gif" width=20 height=20></A> </TD>
                        <TD class=margin_small bgColor=#1d609a width=1></TD>
                        </TR>
                        <TR>
                           <TD style="FONT-SIZE: 8px; LINE-HEIGHT: 8px" bgColor=#1d609a height=8></TD>
                        </TR>
                        <TR>
                           <TD><a style="FONT-SIZE: 14px; FONT-FAMILY: Arial, Helvetica, sans-serif; FONT-WEIGHT: bold; COLOR: #ffffff; LINE-HEIGHT: 12px; TEXT-DECORATION: none;" vAlign=middle title=Klantenservice href="https://www.delta.nl/klantenservice" target="_blank">Klantenservice</a></TD>
                           <TD width=80>&nbsp;</TD>
                           <TD><a style="FONT-SIZE: 14px; FONT-FAMILY: Arial, Helvetica, sans-serif; FONT-WEIGHT: bold; COLOR: #ffffff; LINE-HEIGHT: 12px; TEXT-DECORATION: none;" vAlign=middle title=DELTA.nl href="https://www.delta.nl/" target="_blank">DELTA.nl</a></TD>
                        </TR>
                        <TR>
                           <TD style="FONT-SIZE: 8px; LINE-HEIGHT: 8px" bgColor=#1d609a height=8></TD>
                        </TR>
                        <TR>
                           <TD><a style="FONT-SIZE: 14px; FONT-FAMILY: Arial, Helvetica, sans-serif; FONT-WEIGHT: bold; COLOR: #ffffff; LINE-HEIGHT: 12px; TEXT-DECORATION: none;" vAlign=middle title=Voorwaarden href="https://www.delta.nl/downloads?categorie=292" target="_blank">Voorwaarden</a></TD>
                           <TD width=80>&nbsp;</TD>
                           <TD></TD>
                           <TD width=70>&nbsp;</TD>
                           <TD class=margin_small bgColor=#1d609a width=1></TD>
                           <TD bgColor=#1d609a align=left width=200 style="FONT-SIZE: 9px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #ffffff; LINE-HEIGHT: 12px; TEXT-DECORATION: none;">U ontvangt deze mail als servicemail van DELTA. Heeft u vragen of opmerkingen neem dan contact op met onze <a title="Klantenservice" href="https://www.delta.nl/klantenservice" target="_blank" style="FONT-SIZE: 9px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #ffffff; LINE-HEIGHT: 12px TEXT-DECORATION: underline">Klantenservice</A></TD>
                           <TD class=margin_small bgColor=#1d609a width=1></TD>
                        </TR>
                        <TR>
                           <TD height=10 bgColor=#1d609a></TD>
                        </TR>
                        </TBODY>
                     </TABLE>
                     </TD>
                     <TD bgColor=#1d609a>&nbsp;</TD>
                  </TR>
                  <TR>
                     <TD bgColor=#1d609a vAlign=middle align=center>&nbsp;</TD>
                     <TD bgColor=#1d609a vAlign=middle align=center>
                        <TABLE class=main cellSpacing=0 cellPadding=0 width=678 align=center border=0>
                           <TBODY>
                              <TR>
                                 <TD style="FONT-SIZE: 1px; LINE-HEIGHT: 1px" bgColor=#1d609a height=1></TD>
                                 <TD style="FONT-SIZE: 1px" bgColor=#ffffff></TD>
                                 <TD style="FONT-SIZE: 1px; LINE-HEIGHT: 1px" bgColor=#1d609a></TD>
                              </TR>
                              <TR>
                                 <TD class=margin_small bgColor=#1d609a width=1></TD>
                                 <TD class=main style="MAX-WIDTH: 676px; WIDTH: 678px" align=left>
                                    <TABLE class=main cellSpacing=0 cellPadding=0 width=678 align=center border=0>
                                       <TBODY>
                                          <TR>
                                             <TD>
                                                <!--<![endif]--><a href="https://www.delta.nl/" target="_blank"><IMG class=mob_logo style="FLOAT: left; DISPLAY: block" alt=DELTA src="<?php echo base_url(); ?>assets/img/87967128bbec9355eefd12494e5d3ea1.gif" width=63 height=26></A>
                                             </TD>
                                             <TD>
                                                <!--<![endif]-->
                                                <TABLE style="DISPLAY: table" cellSpacing=0 cellPadding=0 border=0>
                                                   <TBODY>
                                                      <TR>
                                                         <TD style="MAX-WIDTH: 576px; WIDTH: 576px">
                                                            <TABLE class=floatleft cellSpacing=0 cellPadding=0 width=413 align=left border=0>
                                                               <TBODY>
                                                                  <TR>
                                                                     <TD height=20>&nbsp;</TD>
                                                                  </TR>
                                                                  <TR>
                                                                     <TD class=buzzupdate style="FONT-SIZE: 10px; FONT-FAMILY: Arial, Helvetica, sans-serif; FONT-WEIGHT: bold; COLOR: #ffffff; LINE-HEIGHT: 12px" vAlign=middle width=400 align=right>DELTA Fiber Nederland B.V. - Postbus 158 - 4330AD Middelburg - KvKnr. 22051676</TD>
                                                                  </TR>
                                                                  <TR>
                                                                     <TD height=20>&nbsp;</TD>
                                                                  </TR>
                                                               </TBODY>
                                                            </TABLE>
                                                         </TD>
                                                      </TR>
                                                   </TBODY>
                                                </TABLE>
                                             </TD>
                                          </TR>
                                       </TBODY>
                                    </TABLE>
                                    <!--<![endif]-->
                                 </TD>
                              </TR>
                           </TBODY>
                        </TABLE>
                     </TD>
                     <TD bgColor=#1d609a vAlign=middle align=center>&nbsp;</TD>
                  </TR>
               </TBODY>
            </TABLE>
</DIV>

</DIV>
</BODY>
</HTML>

