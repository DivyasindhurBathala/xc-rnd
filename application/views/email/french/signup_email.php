<p></p>
<p>Beste <?php echo $info->name; ?>,</p>
<p>Uw login gegevens voor MijnDelta Mobiel zijn opnieuw ingesteld.</p>
<p><?php echo lang('Email'); ?>: <?php echo strtolower($info->email); ?></p>
<p><?php echo lang('Password'); ?>: <?php echo $info->password; ?></p>
<p></p>
<table style="BACKGROUND: #e47321; border-radius: 6px" cellspacing="0" cellpadding="0">
    <tbody>
        <tr>
            <td height="40" width="20"><a href="<?php echo base_url(); ?>client/auth" target="_blank"></a></td>
            <td style="FONT-SIZE: 16px; FONT-FAMILY: Arial, Helvetica, sans-serif" height="40"><a style="TEXT-DECORATION: none; COLOR: #ffffff" href="<?php echo base_url(); ?>client/auth" target="_blank"><strong><?php echo lang('Login'); ?>
            </strong></a></td>
            <td height="40" width="20"><a href="<?php echo base_url(); ?>client/auth" target="_blank"></a></td>
        </tr>
    </tbody>
</table>
<p></p>
<p><?php echo lang('Do you have any question? look up our'); ?> <a href="http://www.delta.nl/mobile" target="_blank"><?php echo lang('website'); ?></a> <?php echo lang('get in touch with us'); ?></p>
<p><?php echo lang('We are glad to help you'); ?>.</p>
<p><?php echo lang('Kind Regards'); ?>,</p>
<p></p>
<p style="FONT-SIZE: 14px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; LINE-HEIGHT: 16px" align="left">
Ludolf Rasterhoff <br><br>Directeur Telecom</p>