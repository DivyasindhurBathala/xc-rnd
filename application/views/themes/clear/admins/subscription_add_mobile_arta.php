<div class="content-i">
  <div class="content-box"><div class="element-wrapper">
    <div class="element-box">
      <form method="post"  id="ordering">
        <div class="steps-w">
          <div class="step-triggers">
            <a class="step-trigger active" href="#stepContent1"><?php echo lang('Brand and Customer'); ?></a>
            <a class="step-trigger" href="#stepContent2"><?php echo lang('Configure Product'); ?></a>
            <a class="step-trigger" href="#stepContent3"><?php echo lang('Billing Information'); ?></a>
            <a class="step-trigger" id="finale"><?php echo lang('Final Step'); ?></a>
          </div>
          <div> <p class="text-center text-danger"><small><?php echo lang('Please do not click top navigation above, follow the wizard instead'); ?></small></p></div>



          <div class="step-contents">
            <div id="stepContent1_loader" style="display:none;">
              <center>
              <img src="<?php echo base_url(); ?>assets/img/f553e05cd57fc28d91e3a4b26870f24f.gif">
              </center>
            </div>
            <div class="step-content active" id="stepContent1">
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label for=""><?php echo lang('Company Brand'); ?></label>
                    <?php if ($setting->mobile_platform == "ARTA") {
    ?>
                      <select class="form-control" name="packageid" id="paket">
                      <?php foreach (getInternetProducts('MOBILE') as $row) {
        ?>
                      <option value="<?php echo $row['id']; ?>"<?php if ($row['id'] == 2) {
            ?> selected<?php
        } ?>><?php echo $row['name']; ?></option>

                      <?php
    } ?>
                    </select>
                    <?php
} else {
        ?>
                          <select class="form-control" name="packageid" id="paket">
                      <?php foreach (getInternetProducts('MOBILE') as $row) {
            ?>
                      <option value="<?php echo $row['id']; ?>"<?php if ($row['id'] == 2) {
                ?> selected<?php
            } ?>><?php echo $row['name']; ?></option>

                      <?php
        } ?>
                    </select>
                    <?php
    } ?>

                  </div>
                </div>


                <div class="col-md-12">
                  <div class="form-group">
                    <label for=""> <?php echo lang('Agent/Reseller'); ?>:</label>
                    <select class="form-control" id="agentid" name="agentid">
                    <?php foreach (get_agents($this->session->cid) as $agent) {
        ?>
                      <option value="<?php echo $agent->id; ?>"<?php if ($agent->isdefault == 1) {
            ?> selected<?php
        } ?>><?php echo $agent->agent; ?></option>
                    <?php
    } ?>


                    </select>
                  </div>
                </div>


                <div class="col-md-12">
                  <div class="form-group">
                    <label for=""> <?php echo lang('Customer Type'); ?>:</label>
                    <select class="form-control" id="customertype" name="customer_type" <?php if ($setting->mobile_platform == "TEUM") {
        ?> readonly<?php
    } ?>>
                      <option value="old"><?php echo lang('Existing Customer'); ?></option>
                      <option value="new" selected><?php echo lang('New Customer'); ?></option>
                    </select>
                  </div>
                </div>
                <div class="col-md-12"  style="display:none;" id="toli">
                  <div class="form-group" id="existing">
                    <label for=""><?php echo lang('Customer ID'); ?>:</label>
                    <input class="form-control ui-autocomplete-input customerid" id="customerid" autocomplete="new-username" placeholder="search..." type="text" name="userid">
                  </div>
                </div>
                <div class="col-md-12" style="display:none;" id="pelanggan">
                  <div class="form-group">
                    <label for=""><?php echo lang('Customer Name'); ?>:</label>
                    <input class="form-control" id="customername" autocomplete="new-username" placeholder="search..." type="text" id="namapelanggan" readonly>
                  </div>
                </div>
              </div>
              <div class="row">
                <div  id="errornya">
                </div>
              </div>
              <div class="form-buttons-w text-right">
                <button class="btn btn-primary step-trigger-btn" type="button" id="step1"> <?php echo lang('Continue'); ?></button>
              </div>
            </div>
            <div class="step-content" id="stepContent2">
              <div class="row">
                <div class="col-md-12">
                  <div id="showsuccess"></div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <h4><div id="package"></div></h4>
                  <div class="row">
                    <div class="col-md-4">
                      <div class="form-group">
                        <label for=""><?php echo lang('Product'); ?>:</label>
                        <select class="form-control" id="addonproduct" name="addonid">
                            <?php foreach (getProductSell($this->session->cid) as $row) {
        ?>
                          <option value="<?php echo $row->id; ?>"><?php echo $row->name; ?> <?php if ($row->setup > 0) {
            echo 'Setup fee: '.$setting->currency . number_format($row->setup, 2);
        } ?></option>
                            <?php
    }?>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label for=""><?php echo lang('Contract Duration'); ?> (<?php echo lang('Month'); ?>)</label>
                        <select class="form-control" id="contractduration" name="ContractDuration">
                          <option value="1" selected><?php echo lang('Monthly'); ?> (1 Month)</option>
                          <option value="3"><?php echo lang('Quarterly'); ?> (3 Months)</option>
                          <option value="6"><?php echo lang('Semi Annually'); ?> (6 Months)</option>
                          <option value="12" selected><?php echo lang('Annually'); ?> (12 Months)</option>
                          <option value="24"><?php echo lang('Bienially'); ?> (24 Months)</option>
                          <option value="36"><?php echo lang('Trienially'); ?> (36 Months)</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label for=""><?php echo lang('Price Recurring'); ?></label>
                        <input  type="text" class="form-control" id="harga" value="" disabled>
                      </div>
                    </div>
                  </div>
                  <div class="form-group" id="addonx">
                    <label for=""><?php echo lang('Options'); ?>:</label>
                    <?php foreach (getAddonSell('mobile', $this->session->cid) as $row) {
        ?>
                    <div class="form-check">
                      <label class="form-check-label">
                        <input name="addon[<?php echo $row->id; ?>]" type="checkbox" class="form-check-input optionaddon" value="<?php echo $row->id; ?>"<?php if ($row->id == "18") {
            ?> id="handset_id"<?php
        } ?>>
                        <?php echo str_replace('Option ', '', $row->name) . ' '.$setting->currency . str_replace(',', '.', round($row->recurring_total, 2)) . lang('/month'); ?>         </label>
                    </div>
                    <?php
    }?>
                  </div>
                  <div class="form-group">
                    <label for=""><?php echo lang('Diversen'); ?>:</label>
                    <?php foreach (getAddonSell('others', $this->session->cid) as $row) {
        ?>
                    <div class="form-check">
                      <label class="form-check-label">
                        <input name="addon[<?php echo $row->id; ?>]" type="checkbox" class="form-check-input optionaddon" value="<?php echo $row->id; ?>"<?php if ($row->id == "18") {
            ?> id="handset_id"<?php
        } ?>>
                        <?php echo str_replace('Option ', '', $row->name); ?>         </label>
                    </div>
                    <?php
    }?>
                  </div>
                  <div id="handset"></div>
                  <div class="form-group">
                    <label for=""><?php echo lang('SIM Order'); ?></label>
                    <select class="form-control" id="migration" name="msisdn_type">
                      <option value="porting"><?php echo lang('Yes, I wish to keep my Number'); ?></option>
                      <option value="new" selected><?php echo lang('No, Order me New Number'); ?></option>
                    </select>
                  </div>
                  <div id="migrationoption" style="display:none;">
                    <div class="form-group">
                      <label for=""><?php echo lang('Mobile Number'); ?> :<?php echo getcountrycode($setting->country_base); ?>XXXXXXXXXX</label>
                      <input class="form-control" autocomplete="new-username" placeholder="<?php echo lang('REQUIRED'); ?>" type="number" name="MsisdnNumber" id="msisdn" value="<?php echo getcountrycode($setting->country_base); ?>">
                    </div>
                    <div class="form-group">

                      <label for="">Type</label>
                      <select class="form-control" id="type_number" name="PortingType">
                        <option value="0" selected><?php echo lang('Prepaid'); ?></option>
                        <option value="1"><?php echo lang('Postpaid'); ?></option>
                      </select>
                    </div>
                    <div class="form-group">
                      <label for=""><?php echo lang('Current Provider'); ?></label>
                      <select class="form-control" id="provider" name="provider">
                        <?php foreach (getDonors($setting->country_base) as $row) {
        ?>
                        <option value="<?php echo $row->operator_code; ?>" selected><?php echo $row->operator_name; ?></option>
                        <?php
    }?>
                      </select>
                    </div>

                    <?php if ($setting->country_base != "NL") {
        ?>
                      <div class="form-group">
                      <label for=""><?php echo lang('PORTING CUSTUMER TYPE'); ?></label>
                      <select class="form-control" id="ptype_belgium" name="ptype_belgium">
                        <?php foreach (array('PREPAY','POSTPAY_SIMPLE','POSTPAY_COMPLEX','NA') as $row) {
            ?>
                        <option value="<?php echo $row; ?>" selected><?php echo $row; ?></option>
                        <?php
        } ?>
                      </select>
                    </div>

                    <?php
    } ?>
                    <div class="form-group">
                      <label for=""><?php echo lang('SIMcard Number'); ?></label>
                      <input class="form-control" autocomplete="new-username" placeholder="" type="text" name="simnumber" id="simnumber">
                    </div>
                    <div class="form-group">
                      <label for=""><?php echo lang('Customer Type'); ?></label>
                      <select class="form-control" id="customertype1" name="customertype1">
                        <option value="0" selected><?php echo lang('Residential'); ?></option>
                        <option value="1"><?php echo lang('Bussiness'); ?></option>
                      </select>
                    </div>
                    <div class="form-group" style="display:none;" id="acct">
                      <label for=""><?php echo lang('ClientNumber Current provider'); ?></label>
                      <input class="form-control" autocomplete="new-username" placeholder="" type="text" name="AccountNumber" id="AccountNumber">
                    </div>
                    <?php if ($setting->country_base == "NL") {
        ?>
                    <div class="form-group">
                      <label for=""><?php echo lang('PortIN Date Wish'); ?>:</label>
                        <?php if ($setting->country_base == "NL") {
            ?>
                        <input class="form-control" autocomplete="new-username"  type="text" name="PortInWishDate" id="pickdate8" readonly>
                        <?php
        } else {
            ?>
                      <input class="form-control" autocomplete="new-username"  type="text" name="PortInWishDate" id="pickdate9" readonly>
                        <?php
        } ?>
                    </div>
                    <?php
    } ?>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label for=""><?php echo lang('Contract Start Date'); ?> MM-DD-YYYY</label>
                    <input class="form-control" autocomplete="new-username"  value="<?php echo date('m-d-Y'); ?>" type="text" name="ContractDate" id="pickdate20" readonly>
                  </div>
                </div>
              </div>
              <div class="form-buttons-w text-right">
                <button class="btn btn-primary step-trigger-btn" type="button" id="step3"> <?php echo lang('Continue'); ?></button>
              </div>
            </div>
            <div class="step-content" id="stepContent3">
                <?php if ($setting->mobile_platform == "TEUM") {
        ?>
                 <div class="row">
                   <div class="col-sm-3 mr-auto ml-auto">
                    <button type="button" onclick="GenAnn()" class="btn btn-sm btn-primary">Generate Annonymous Customer</button>
                   </div>
               </div>
                <?php
    } ?>

              <div class="row">
                <div class="col-sm-3">
                  <div class="form-group">
                    <label for=""><?php echo lang('Relation ID'); ?></label>
                    <input class="form-control" placeholder="<?php echo lang('Enter Delta ID'); ?>" name="mvno_id" type="text" id="deltaid" <?php if ($setting->mobile_platform == "TEUM") {
        ?> readonly<?php
    } ?>>
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="form-group">
                    <label for=""><?php echo lang('Username'); ?></label><input class="form-control" placeholder="<?php echo lang('Enter Delta Username for SSO'); ?>" name="delta_username" type="text" id="delta_username"<?php if ($setting->mobile_platform == "TEUM") {
        ?> readonly<?php
    } ?>>
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="form-group">
                    <label for=""><?php echo lang('Email'); ?><span class="text-danger">*</span></label><input class="form-control" placeholder="<?php echo lang('Enter email'); ?>" name="email" type="email" id="email" required>
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="form-group">
                    <label for=""><?php echo lang('Phonenumber'); ?><span class="text-danger">*</span></label><input class="form-control" placeholder="<?php echo lang('Enter phonenumber'); ?>" name="phonenumber" type="number" id="phonenumber" required>
                  </div>
                </div>
              </div>
              <div class="row">
              <div class="col-md-3">
                  <div class="form-group">
                    <label for=""><?php echo lang('Agent/Reseller'); ?></label>
                    <select class="form-control" name="agentid" id="agentid">

                    <?php foreach (get_agents($this->session->cid) as $a) {
        ?>
                      <option value="<?php echo $a->id; ?>"><?php echo $a->agent; ?></option>
                    <?php
    }?>
                    </select>
                  </div>
                </div>


                <div class="col-sm-3">
                  <div class="form-group">
                    <label for=""> <?php echo lang('VAT'); ?>.</label><input class="form-control" placeholder="<?php echo lang('leave empty if not a company entity'); ?>" type="text" name="vat" id="vat">
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="form-group">
                    <label for=""><?php echo lang('Companyname'); ?></label><input class="form-control" placeholder="<?php echo lang('Leave empty if not a company entity'); ?>" type="text" name="companyname" id="companyname">
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="form-group">
                    <label for=""><?php echo lang('NationalNr'); ?></label><input class="form-control" placeholder="" type="text" name="nationalnr" id="nationalnr">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for=""><?php echo lang('Initial'); ?></label><input class="form-control" placeholder="<?php echo lang('Initial'); ?>" name="initial" type="text" id="Initial">
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for=""><?php echo lang('Salutation'); ?></label><input class="form-control" placeholder="<?php echo lang('Salutation'); ?>" name="salutation" type="text" id="Salutation">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for=""><?php echo lang('Firstname'); ?><span class="text-danger">*</span></label><input class="form-control" placeholder="<?php echo lang('Firstname'); ?>" type="text" name="firstname" id="firstname" required>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for=""><?php echo lang('Lastname'); ?><span class="text-danger">*</span></label><input class="form-control" placeholder="<?php echo lang('Lastname'); ?>" type="text" name="lastname" id="lastname" required>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-3">
                  <div class="form-group">
                    <label for=""><?php echo lang('Address1'); ?><span class="text-danger">*</span></label><input class="form-control" placeholder="<?php echo lang('Address'); ?>" type="text" name="address1" id="address1" required>
                  </div>
                </div>
                  <div class="col-sm-3">
                  <div class="form-group">
                    <label for=""><?php echo lang('Housenumber'); ?><span class="text-danger">*</span></label><input class="form-control" placeholder="<?php echo lang('Housenumber'); ?>" type="text" name="housenumber" id="housenumber" required>
                  </div>
                </div>
                  <div class="col-sm-3">
                  <div class="form-group">
                    <label for=""><?php echo lang('Alphabet'); ?><span class="text-danger"></span></label><input class="form-control" placeholder="<?php echo lang('Alphabet'); ?>" type="text" name="alphabet" id="alphabet" required>
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="form-group">
                    <label for=""><?php echo lang('Postcode'); ?><span class="text-danger">*</span></label><input class="form-control" placeholder="<?php echo lang('Postcode'); ?>" type="text" name="postcode" id="postcode" required>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-3">
                  <div class="form-group">
                    <label for=""><?php echo lang('City'); ?><span class="text-danger">*</span></label><input class="form-control" placeholder="<?php echo lang('City'); ?>" type="text" id="city" name="city" required>
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="form-group">
                    <label for=""><?php echo lang('Country'); ?><span class="text-danger">*</span></label>
                    <select class="form-control" name="country" id="country">
                        <?php foreach (getCountries() as $key => $c) {
        ?>
                      <option value="<?php echo $key; ?>"<?php if ($key == $setting->country_base) {
            ?> selected<?php
        } ?>>  <?php echo $c; ?> </option>
                        <?php
    }?>
                    </select>
                  </div>
                </div>

                <?php if ($setting->mobile_platform == "TEUM") {
        ?>
                    <div class="col-sm-3">
                  <div class="form-group">
                    <label for=""> Language<span class="text-danger">*</span></label>
                    <select class="form-control" name="language">
                      <?php foreach (getLanguages() as $key => $c) {
            ?>
                      <option value="<?php echo $key; ?>"<?php if ($key == "english") {
                ?> selected<?php
            } ?>>  <?php echo lang($c); ?> </option>
                      <?php
        } ?>
                    </select>
                  </div>
                </div>
                <?php
    } else {
        ?>
                      <div class="col-sm-3">
                  <div class="form-group">
                    <label for=""> Language<span class="text-danger">*</span></label>
                    <select class="form-control" name="language">
                      <?php foreach (getLanguages() as $key => $c) {
            ?>
                      <option value="<?php echo $key; ?>"<?php if ($key == "dutch") {
                ?> selected<?php
            } ?>>  <?php echo lang($c); ?> </option>
                      <?php
        } ?>
                    </select>
                  </div>
                </div>
                <?php
    } ?>

                <div class="col-sm-3">
                  <div class="form-group">

                  <label for="exampleSelect1"> InvoiceConditionEmail</label>
                  <select class="form-control" id="invoice_email" name="invoice_email">
                    <option value="yes" selected="">Yes</option>
                    <option value="yes">No</option>
                  </select>
                  <small> <?php echo lang('choose yes if wish to recieved email invoices'); ?></small>

              </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-3">
                  <div class="form-group">
                    <label for=""><?php echo lang('Gender'); ?></label>
                    <select class="form-control" name="gender">
                        <?php foreach (array('male', 'female') as $c) {
        ?>
                      <option value="<?php echo $c; ?>"> <?php echo lang(ucfirst($c)); ?> </option>
                        <?php
    }?>
                    </select>
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="form-group">
                    <label for=""><?php echo lang('Date Birth'); ?></label><input class="form-control"  value="<?php echo date('1980-m-d'); ?>" type="text" name="date_birth" id="pickdate">
                  </div>
                </div>
                    <?php if ($setting->mobile_platform != "TEUM") {
        ?>
                <div class="col-sm-3">
                  <div class="form-group">
                    <label for=""><?php echo lang('IBAN'); ?></label>
                    <input class="form-control" placeholder="NLXXXXXXXXXXXXXXXX" type="text" name="iban" id="IBAN">
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="form-group">
                    <label for=""><?php echo lang('BIC'); ?></label><input class="form-control" placeholder="ASPNLXX" type="text" name="bic" id="BIC">
                  </div>
                </div>
                    <?php
    } else {
        ?>
                <div class="col-sm-3">
              <div class="form-group">

                  <label for="exampleSelect1"><?php echo lang('Identification Type'); ?></label>
                  <select class="form-control" id="id_type" name="id_type">
                    <?php foreach (array('Passport', 'DrivingLicence', 'DNI', 'NIF', 'NIE', 'CIF', 'Visa') as $type) {
            ?>
                    <option value="<?php echo $type; ?>"><?php echo lang($type); ?></option>
                    <?php
        } ?>
                  </select>

              </div>
            </div>
            <div class="col-sm-3">

                <label class="control-label" for="Phonenumber"><?php echo lang('Identification Number'); ?><span class="text-danger"></span></label>
                <input name="idcard_number" class="form-control" id="idcard_number" type="text" placeholder="<?php echo lang('ID Number'); ?>"<?php if (!empty($_SESSION['registration']['idcard_number'])) {
            ?> value="<?php echo $_SESSION['registration']['idcard_number']; ?>" <?php
        } ?>>

            </div>
                    <?php
    } ?>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <div class="form-group">
                    <fieldset class="form-group"><div class="form-desc">
                        <?php echo lang('VAT exemption (BTW verlegd)'); ?>
                    </div>
                    <div class="form-check">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="vat_exempt" id="vatexempt" value="" checked="">
                        <?php echo lang('No, Apply tax on every invoices'); ?>
                      </label>
                    </div>
                    <div class="form-check">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="vat_exempt" id="vatexempt" value="on">
                        <?php echo lang('Yes, do not apply tax on invoices'); ?>
                      </label>
                    </div>
                  </fieldset>
                </div>
              </div>
            </div>
            <div class="form-buttons-w float-right">
              <a class="btn btn-primary step-trigger-btn" id="step4" style="color:#fff;"><?php echo lang('Continue'); ?></a>
            </div>
          </div>
          <div class="step-content" id="stepContent4">
            <table class="table table-striped">
              <tr>
                <td><?php echo lang('Customer ID'); ?></td>
                <td align="right"><div id="f_userid"></div></td>
              </tr>
              <tr>
                <td><?php echo lang('Product'); ?></td>
                <td align="right"><div id="f_productid"></div></td>
              </tr>
              <tr>
                <td><?php echo lang('Order Type'); ?></td>
                <td align="right"><div id="f_ordertype"></div></td>
              </tr>
              <tr>
                <td><?php echo lang('Customer Name'); ?></td>
                <td align="right"><div id="f_customername"></div></td>
              </tr>
              <tr>
                <td><?php echo lang('Address'); ?></td>
                <td align="right"><div id="f_address"></div></td>
              </tr>
              <tr>
                <td><?php echo lang('SIM Order'); ?></td>
                <td align="right"><div id="f_simorder"></div></td>
              </tr>
              <div id="migrationfinal" style="display:none;">
                <tr class="migrationfinal">
                  <td><?php echo lang('Mobile Number'); ?></td>
                  <td align="right"><div id="f_msisdn"></div></td>
                </tr>
                <tr class="migrationfinal">
                  <td><?php echo lang('SIM Card Number'); ?></td>
                  <td align="right"><div id="f_simcard"></div></td>
                </tr>
                <tr class="migrationfinal">
                  <td><?php echo lang('Date Request Efective'); ?></td>
                  <td align="right"><div id="f_datewish"></div></td>
                </tr>
                <tr class="migrationfinal">
                  <td><?php echo lang('Customer Type'); ?></td>
                  <td align="right"><div id="f_customertype1"></div></td>
                </tr>
              </div>
              <tr>
                <td><?php echo lang('Promotion Code'); ?></td>
                <td align="right"> <select class="form-control" id="promotion" name="Promotion">
                  <option value="None"><?php echo lang('None'); ?></option>
                    <?php foreach (getPromotions() as $p) {
        ?>
                  <option value="<?php echo $p['promo_code']; ?>"><?php echo $p['promo_code']; ?> - <?php echo $p['promo_name']; ?></option>
                    <?php
    }?>
                </select></td>
              </tr>
                <?php if ($setting->country_base == "BE") {
        ?>
                <tr>
                <td><?php echo lang('Simcard Delivery'); ?></td>
                <td align="right">
                <select class="form-control" id="sim_delivery" name="sim_delivery">
                  <option value="United"><?php echo lang('United'); ?></option>
                  <option value="<?php echo $setting->companyname; ?>" selected><?php echo $setting->companyname; ?></option>

                </select>
                </td>
              </tr>
                <?php
    } ?>
            </table>
            <br />
            <br />
            <center>
            <div class="form-check">
              <label class="form-check-label">
                <input name="skip_accept" type="checkbox" class="form-check-input" value="yes" id="skip_accept">
                <?php echo lang('Automaticly Accept This Order'); ?>
              </label>
            </div>
            <div class="form-group" id="showsim" style="display:none;">
              <label for=""><?php echo lang('SIM Number'); ?><span class="text-danger">*</span></label><input class="form-control  ui-autocomplete-input pengkor" placeholder="89311621118000XXXXX" autocomplete="new-username" type="text" name="simnumberx" id="simnumberx">
            </div>


            </center>
            <div class="form-buttons-w text-right">
              <button class="btn btn-primary" type="button" id="finalize"><?php echo lang('Submit Order'); ?></button>
            </div>
            <div id="loader" class="form-buttons-w text-center" style="display:none;">
              <img src="<?php echo base_url(); ?>assets/img/ajax-loader1.gif">
            </div>
            <div id="errorme" class="form-buttons-w text-center text-danger" style="display:none;font-weight: bold;">
            </div>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>
<script>
    function GenAnn(){
$.ajax({
url: window.location.protocol + '//' + window.location.host + '/admin/client/gen_anonymous',
type: 'post',
dataType: "json",
data: {form_data:1},
success: function(data) {
Object.keys(data).forEach(function(k){
    console.log(k + ' - ' + data[k]);
     $('#'+k).val(data[k]);
});


}
});

  }
</script>
<script>
$("#addonproduct").change(function() {
var product = $('#addonproduct').find(":selected").val();
var contractduration = $('#contractduration').find(":selected").val();
$.ajax({
url: '<?php echo base_url(); ?>admin/complete/getPriceProduct/'+product+'/'+contractduration,
dataType: 'json',
success: function(data) {
$('#harga').val(data.price);
if(data.addons =="yes"){
   $('#addonx').html('');
  console.log(data);
$('#addonx').html(data.addons_html);
}
},
error: function(errorThrown) {
console.log(errorThrown);
}
});
});
$("#contractduration").change(function() {
var product = $('#addonproduct').find(":selected").val();
var contractduration = $('#contractduration').find(":selected").val();
$.ajax({
url: '<?php echo base_url(); ?>admin/complete/getPriceProduct/'+product+'/'+contractduration,
dataType: 'json',
success: function(data) {
$('#harga').val(data.price);

if(data.addons =="yes"){
   $('#addonx').html('');
  console.log(data);
$('#addonx').html(data.addons_html);
}
},
error: function(errorThrown) {
console.log(errorThrown);
}
});
});
$( "#skip_accept" ).on( "click", function() {
var skip_accept = $('#skip_accept').is(':checked');
if(skip_accept){
$("#showsim").show('slow');
$('#simnumberx').prop('required', true);
}else{
$("#showsim").hide('slow');
$('#simnumberx').prop('required', false);
}
//
});
$("#step1").click(function() {
var userid = $('#customerid').val();
var package = $('#paket').find(":selected").text();
var sel = $('#customertype').find(":selected").val();
if(userid.length <= 0 &&  sel == "old"){
alert('You need to find customer first');
$('#customerid').focus();
return;
}
$("#package").html("TYPE :" + package);
if (userid > 0 && sel == "old") {
$.ajax({
url: '<?php echo base_url(); ?>admin/complete/getcustomerid',
dataType: 'json',
type: 'post',
data: {
'userid': userid
},
success: function(data) {
$("#deltaid").val(data.deltaid);
$("#deltaid").prop('readonly', true);
$("#delta_username").val(data.DeltaUsername);
$("#delta_username").prop('readonly', true);
$("#companyname").val(data.companyname);
$("#firstname").val(data.firstname);
$("#lastname").val(data.lastname);
$("#email").val(data.email);
$("#address1").val(data.address1);
$("#housenumber").val(data.housenumber);
$("#alphabet").val(data.alphabet);
$("#city").val(data.city);
$("#postcode").val(data.postcode);
$("#IBAN").val(data.iban);
$("#IBAN").prop('readonly', true);
$("#BIC").prop('readonly', true);
$("#BIC").val(data.bic);
$("#phonenumber").val(data.phonenumber);
$("#Initial").val(data.initial);
$("#Salutation").val(data.salutation);
var btn_href = '#stepContent2';
$('.step-trigger[href="' + btn_href + '"]').click();
},
error: function(errorThrown) {}
});
} else if (userid <= 0 && sel == "1") {
alert('Please search customer..');
} else {
var btn_href = '#stepContent2';
$('.step-trigger[href="' + btn_href + '"]').click();
}
$('#step3').prop('disabled', false);
});
$("#step3").click(function() {
var handset = $('#handset_id').is(':checked');
var btn_href = '#stepContent3';
var msisdn = $('#msisdn').val();
var sim = $('#simnumber').val();
var sel = $('#migration').find(":selected").val();
    <?php if ($setting->country_base == "NL") {
        ?>
var datewish = $('#pickdate8').val();
    <?php
    } else {
        ?>
    var datewish = '<?php echo date('Y-m-d'); ?>';

    <?php
    } ?>
var contractduration = $('#contractduration').val();
if (handset) {
var handset_name =$('#handset_name').val();
var handset_price = $('#handset_price').val();
var handset_price = $('#handset_month').val();
if(handset_name.length < 1 && handset_price.length < 1){
alert('<?php echo lang('The Handset Information is required'); ?>');
$("#handset_name").focus();
return false;
}
}
if (sel == "porting") {
var ct = $('#customertype1').find(":selected").val();
if (msisdn.length < 5) {
alert('<?php echo lang('The Mobile number is invalid or empty'); ?>');
$('#msisdn').focus();
} else if (sim.length < 5) {
alert('<?php echo lang('The SIMcard number is invalid or empty'); ?>');
$('#simnumber').focus();
} else if (datewish.length < 1 ) {
alert('<?php echo lang('The Date Wish is invalid or empty'); ?>');
$('#pickdate8').focus();
} else if (contractduration.length < 1) {
alert('<?php echo lang('The contract Duration is invalid or empty'); ?>');
$('#contractduration').focus();
return false;
} else {
if (ct == "1") {
var acctn = $('#AccountNumber').val();
if (acctn.length < 1) {
alert('<?php echo lang('The Account number is invalid or empty'); ?>');
$('#AccountNumber').focus();
} else {
$('.step-trigger[href="' + btn_href + '"]').click();
}
} else {
$('.step-trigger[href="' + btn_href + '"]').click();
}
}
} else {
if (contractduration.length < 1) {
alert('<?php echo lang('The contract Duration is invalid or empty'); ?>');
$('#contractduration').focus();
return false;
}
$('.step-trigger[href="' + btn_href + '"]').click();
}
});
$("#step4").click(function() {
var btn_href = '#stepContent4';
var firstname = $('#firstname').val();
var lastname = $('#lastname').val();
var email = $('#email').val();
var phonenumber = $('#phonenumber').val();
var address1 = $('#address1').val();
var postcode = $('#postcode').val();
var city = $('#city').val();
var country = $('#country').find(":selected").val();
var language = $('#language').find(":selected").val();
var ibanx = $('#iban').val();
console.log(firstname);
if (email == '') {
alert('<?php echo lang('The Email'); ?>  <?php echo lang('is invalid or empty'); ?>');
$('#email').focus();
} else if (firstname == '') {
alert('<?php echo lang('The Firstname'); ?>   <?php echo lang('is invalid or empty'); ?>');
$('#firstname').focus();
} else if (lastname == '') {
$('#lastname').focus();
alert('<?php echo lang('The Lastname'); ?>  <?php echo lang('is invalid or empty'); ?>');
} else if (phonenumber == '') {
$('#phonenumber').focus();
alert('<?php echo lang('The Phonenumber'); ?>   <?php echo lang('is invalid or empty'); ?>');
} else if (address1 == '') {
$('#address1').focus();
alert('<?php echo lang('The Address '); ?>  <?php echo lang('is invalid or empty'); ?>');
} else if (postcode == '') {
$('#postcode').focus();
alert('<?php echo lang('The Postcode'); ?>   <?php echo lang('is invalid or empty'); ?>');
} else if (city == '') {
$('#city').focus();
alert('<?php echo lang('The City'); ?>  <?php echo lang('is invalid or empty'); ?>');
} else if (country == '') {
alert('<?php echo lang('Please choose Country'); ?>');
} else if (language == '') {
alert('<?php echo lang('Please choose language'); ?>');
} else if (ibanx == '') {
alert('<?php echo lang('Please enter IBAN'); ?>');
} else if (!email.match(email)) {
alert('<?php echo lang('The Email'); ?> <?php echo lang('is invalid or empty'); ?>');
$('#email').focus();
return false;
} else {
var userid = $('#customerid').val();
var deltaid = $('#deltaid').val();
var package = $('#paket').find(":selected").text();
var customertype = $('#customertype').find(":selected").val();
var customertypereport = $('#customertype').find(":selected").text();
var sel = $('#migration').find(":selected").val();
var f_customername = $('#firstname').val() + ' ' + $('#lastname').val();
var f_address = $('#address1').val() + ' ' + $('#postcode').val() + ' ' + $('#city').val() + ' ' + $('#country').find(":selected").text();
console.log(sel);
if (sel == "porting") {
var simorder = "Porting";
$('#migrationfinal').show();
$('.migrationfinal').show();
var f_msisdn = $('#msisdn').val();
var f_simcard = $('#simnumber').val();

var f_datewish = $('#pickdate7').val();
var f_customertype1 = $('#customertype1').find(":selected").text();
}else{
var simorder = "<?php echo lang('New Number'); ?>";
var f_msisdn = "NA";
var f_simcard = "NA";
var f_datewish = "NA";
var f_customertype1 = "NA";
$('.migrationfinal').hide();
}
$('#f_userid').html('<strong>' + deltaid + '</strong>');
$('#f_productid').html('<strong>' + package + '</strong>');
$('#f_ordertype').html('<strong>' + customertypereport + '</strong>');
$('#f_customername').html('<strong>' + f_customername + '</strong>');
$('#f_address').html('<strong>' + f_address + '</strong>');
$('#f_msisdn').html('<strong>' + f_msisdn + '</strong>');
$('#f_simcard').html('<strong>' + f_simcard + '</strong>');
$('#f_datewish').html('<strong>' + f_datewish + '</strong>');
$('#f_simorder').html('<strong>' + simorder + '</strong>');
$('#f_customertype1').html('<strong>' + f_customertype1 + '</strong>');
$("#finale").attr("href", "#stepContent4");
$('.step-trigger[href="' + btn_href + '"]').click();
}
});
$( ".optionaddon" ).on( "click", function() {
var handset = $('#handset_id').is(':checked');
if(handset){
$( "#handset" ).html(' <div class="form-group"> <label for=""><?php echo lang('Name Of the Handset for Invoice'); ?><span class="text-danger">*</span></label><input class="form-control" placeholder="Iphone 7s" type="text" name="handset_name" id="handset_name"></div> <div class="form-group"><label for=""><?php echo lang('Handset Price To be Paid'); ?><span class="text-danger">*</span></label> <input class="form-control" placeholder="100.00" type="number" step="any" name="handset_price" id="handset_price"> </div><div class="form-group"><label for=""><?php echo lang('Terms (Month)'); ?><span class="text-danger">*</span></label> <input class="form-control" placeholder="12" type="number"  name="handset_month" id="handset_month"> </div>');
$('#handset_name').prop('required', true);
$('#handset_price').prop('required', true);
}else{
$( "#handset" ).html('');
}
//
});
$("#customertype1").change(function() {
var sel = $('#customertype1').find(":selected").val();
console.log(sel);
if (sel == "1") {
$('#acct').show('slow');
$('#AccountNumber').prop('required', true);
} else {
$("#acct").hide('slow');
$('#AccountNumber').prop('required', false);
}
});
$("#customertype").change(function() {
var sel = $('#customertype').find(":selected").val();
console.log(sel);
if (sel == "old") {
$('#toli').show('slow');
$('#existing').show('slow');
$('#customerid').val('');
} else {
$('#pelanggan').hide();
$('#toli').show('hide');
$('#existing').hide('slow');
$('#customerid').val('0');
$("#companyname").val('');
$("#firstname").val('');
$("#lastname").val('');
$("#email").val('');
$("#address1").val('');
$("#housenumber").val('');
$("#alphabet").val('');
$("#city").val('');
$("#postcode").val('');
//$("#country").val(data.companyname);
$("#phonenumber").val('');
//$("#companyname").val(data.companyname);
}
});
$("#migration").change(function() {
var sel = $('#migration').find(":selected").val();
console.log(sel);
if (sel == "porting") {
$('#migrationoption').show('slow');
} else {
$('#migrationoption').hide('slow');
$('#step3').prop('disabled', false);
}
});
$("#finalize").click(function() {
var simnumberx  = $("#simnumberx").val();
var skip_accept = $('#skip_accept').is(':checked');
/*
if(skip_accept){
if(simnumberx.length != "19"){
$("#simnumberx").focus();
alert('<?php echo lang('If you wish to auto accept this order you need to add the DeltaSim Number'); ?>');
return;
}
}
*/
//$("#finalize").hide();
$("#loader").show();
var datastring = $("#ordering").serialize();
$.ajax({
url: '<?php echo base_url(); ?>admin/complete/addorder_mobile/<?php echo $this->session->cid; ?>',
dataType: 'json',
type: 'post',
data: datastring,
success: function(data) {
if (data.result == "success") {
$("#loader").hide();
window.location.href = window.location.protocol + '//' + window.location.host + '/admin/complete/success/'+data.serviceid;
} else {
$("#finalize").show();
$("#errorme").html(data.message);
$("#errorme").show('slow');
$("#loader").hide();
}
},
error: function(errorThrown) {
console.log(errorThrown);
}
});
});
</script>
<script type="text/javascript">
$( document ).ready(function() {
var product = $('#addonproduct').find(":selected").val();
var contractduration = $('#contractduration').find(":selected").val();
console.log(product+' '+contractduration);
$.ajax({
url: '<?php echo base_url(); ?>admin/complete/getPriceProduct/'+product+'/'+contractduration,
dataType: 'json',
success: function(data) {
$('#harga').val(data.price);
if(data.addons =="yes"){
  console.log(data);
$('#addonx').html('');
$('#addonx').html(data.addons_html);
}
},
error: function(errorThrown) {
console.log(errorThrown);
}
});
});

<?php if (!empty($this->uri->segment(4))) {
        ?>
$( document ).ready(function() {
$('#customertype').val('old');
$('#customerid').val('<?php echo $this->uri->segment(4); ?>');
$('#toli').show();
var userid = $('#customerid').val();
var package = $('#paket').find(":selected").text();
var sel = $('#customertype').find(":selected").val();
if(userid.length <= 0 &&  sel == "old"){
alert('You need to find customer first');
$('#customerid').focus();
return;
}
$("#package").html("TYPE :" + package);
if (userid > 0 && sel == "old") {
$.ajax({
url: '<?php echo base_url(); ?>admin/complete/getcustomerid',
dataType: 'json',
type: 'post',
data: {
'userid': '<?php echo $this->uri->segment(4); ?>'
},
success: function(data) {
$("#deltaid").val(data.deltaid);
$("#deltaid").prop('readonly', true);
$("#delta_username").val(data.DeltaUsername);
$("#delta_username").prop('readonly', true);
$("#companyname").val(data.companyname);
$("#firstname").val(data.firstname);
$("#lastname").val(data.lastname);
$("#email").val(data.email);
$("#address1").val(data.address1);
$("#housenumber").val(data.housenumber);
$("#alphabet").val(data.alphabet);
$("#city").val(data.city);
$("#postcode").val(data.postcode);
$("#IBAN").val(data.iban);
$("#IBAN").prop('readonly', true);
$("#BIC").prop('readonly', true);
$("#BIC").val(data.bic);
$("#phonenumber").val(data.phonenumber);
$("#Initial").val(data.initial);
$("#Salutation").val(data.salutation);
var btn_href = '#stepContent2';
$('.step-trigger[href="' + btn_href + '"]').click();
},
error: function(errorThrown) {}
});
} else if (userid <= 0 && sel == "1") {
alert('Please search customer..');
} else {
var btn_href = '#stepContent2';
$('.step-trigger[href="' + btn_href + '"]').click();
}
$('#step3').prop('disabled', false);
});

<?php
    }?>
</script>