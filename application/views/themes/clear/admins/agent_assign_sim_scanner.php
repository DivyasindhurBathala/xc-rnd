<div class="content-i">
  <div class="content-box">
    <div class="element-wrapper">
      <h6 class="element-header">
      <?php echo $title; ?>
      </h6>
      <div class="element-box">
        <div class="table-responsive">
        <div class="row">
    <div class="col-sm-4">
    <h3>Agent Information</h3>
    <hr>
    <table class="table table-striped table-hover">
    <tbody>
    <tr>
    <td>Agent ID</td>
    <td><?php echo $agent->id; ?></td>
    </tr>
    <tr>
    <td>Agent Name</td>
    <td><?php echo $agent->agent; ?></td>
    </tr>
    <tr>
    <td>Agent Address</td>
    <td><?php echo $agent->address1.' '.$agent->postcode.' '.$agent->city; ?></td>
    </tr>
    <tr>
    <td>Agent Phonenumber</td>
    <td><?php echo $agent->phonenumber; ?></td>
    </tr>
    <tr>
    <td>Agent Email</td>
    <td><?php echo $agent->email; ?></td>
    </tr>

     <tr>
    <td>Agent Commission</td>
    <td><?php echo $agent->comission_type.' '.$agent->comission_value; ?></td>
    </tr>
    </tbody>
    </table>
    <button  data-toggle="modal" data-target="#editAgent" class="btn btn-block btn-md btn-primary"><i class="fa fa-edit"></i> Update Agent</button>
    <a id="export_subscription" href="<?php echo base_url(); ?>admin/agent/export_agent_subscription/<?php echo $agent->id; ?>" class="btn btn-block btn-md btn-primary"><i class="fa fa-tasks"></i> Export Subscription</a>
    <button id="send_password_agent" class="btn btn-block btn-md btn-primary"><i class="fa fa-key"></i> Send Credential</button>
    <button id="ususpend_agent" class="btn btn-block btn-md btn-primary"><i class="fa fa-pause"></i> Suspend Agent</button>
    <a id="login" class="btn btn-block btn-md btn-primary" href="<?php echo base_url(); ?>admin/agent/login_as_reseller/<?php echo $agent->id; ?>"><i class="fa fa-eye"></i> Login as Reseller</a>
    <button  data-toggle="modal" data-target="#deleteAgent" class="btn btn-block btn-md btn-danger"><i class="fa fa-trash"></i> Delete Agent</button>
    <a href="<?php echo base_url(); ?>admin/agent/assign_simcard_by_scanner/<?php echo $agent->id; ?>"  class="btn btn-block btn-md btn-primary"><i class="fa fa-credit-card"></i> Assign Simcards with Scanner</a>
    </div>
    <div class="col-sm-8">

<div class="row">
<div class="col-md-6">
<div class="form-group">
<label>Simcard</label>
<input type="number"  id="number" class="form-control" id="sim">
<div id="result" class="text-success" style="font-wight: bold;"></div>
<small id="hide">Once the barcode scanned it will automaticly assigned to Agent for use!</small>
</div>
</div>
</div>
<div class="row">
 <div class="col-sm-12">
              <h3>List Assigned Simcard</h3>
              <hr>
              <table class="table table-striped table-lightfont" id="simcardtable">
                <thead>
                  <tr>
                    <th>
                      <?php echo lang('Simcard'); ?>
                    </th>
                    <th>
                      <?php echo lang('Status'); ?>
                    </th>
                     <th>
                      <?php echo lang('Imported by'); ?>
                    </th>

                     <th>
                      <?php echo lang('Method Import'); ?>
                    </th>
              
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>


            </div>

</div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>

    <script>
    $( document ).ready(function() {
    console.log( "form ready!" );
    $('#number').focus();
    $('#number').keypress(function(event){
        var search = $("#number");
	var keycode = (event.keyCode ? event.keyCode : event.which);
	if(keycode == '13'){
        $.post("<?php echo base_url(); ?>admin/agent/assign_simcard_by_scanner/", { msisdn_sim : search.val(), agentid:'<?php echo $agent->id; ?>' }, function(data) {
                    console.log(data);
                    $("#result").html('Simcard: '+search.val()+' has been assigned');
                    $("#hide").hide();
                     $('#number').val('');
                    $('#number').focus();
                });

	}

});
    /*var search = $("#number");

        search.keyup(function() {
            if (search.val() != '') {
                $.post("<?php echo base_url(); ?>admin/agent/assign_simcard_by_scanner/", { msisdn_sim : search.val(), agentid:'<?php echo $agent->if; ?>' }, function(data) {
                    console.log(data);
                    $("#result")(data);
                });
            }
        });


        */

});
    </script>

<div class="modal fade" id="editAgent" tabindex="-1" role="dialog" aria-labelledby="editAgent" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <form method="post" action="<?php echo base_url(); ?>admin/agent/update">
        <input type="hidden" name="id" value="<?php echo $agent->id; ?>">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">
            <?php echo lang('Register Agent'); ?>
          </h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="form-group col-sm-6">
              <label for="agent">Company Name</label>
              <input name="agent" type="text" class="form-control" id="companyname" value="<?php echo $agent->agent; ?>">
            </div>
            <div class="form-group col-sm-6">
              <label for="contact_name">Contact Name</label>
              <input name="contact_name" type="text" class="form-control" id="agent" value="<?php echo $agent->contact_name; ?>">
            </div>
          </div>

          <div class="row">
            <div class="form-group col-sm-5">
              <label for="address1">Address1</label>
              <input name="address1" type="text" class="form-control" id="address1" value="<?php echo $agent->address1; ?>">
            </div>
            <div class="form-group col-sm-3">
              <label for="postcode">Postcode</label>
              <input name="postcode" type="text" class="form-control" id="postcode" value="<?php echo $agent->postcode; ?>">
            </div>
            <div class="form-group col-sm-4">
              <label for="city">City</label>
              <input name="city" type="text" class="form-control" id="city" value="<?php echo $agent->city; ?>">
            </div>
          </div>

          <div class="row">
            <div class="form-group col-sm-6">
              <label for="phonenumber">Phonenumber</label>
              <input name="phonenumber" type="text" class="form-control" id="text" value="<?php echo $agent->phonenumber; ?>">
            </div>
            <div class="form-group col-sm-6">
              <label for="country">C</label>
              <select name="country" class="form-control" name="country">
                <?php foreach(getCountries() as $key => $row){ ?>
                <option value="<?php echo $key; ?>" <?php if($agent->country == $key) { ?> selected
                  <?php
             } ?>>
                  <?php echo $row; ?>
                </option>
                <?php } ?>
              </select>
            </div>
          </div>


          <div class="row">
            <div class="form-group col-sm-6">
              <label for="comission_type">Comission Type</label>
              <select class="form-control" name="comission_type">
                <?php foreach(array('Percentage','FixedAmount') as $type){ ?>
                <option value="<?php echo $type; ?>" <?php if($agent->comission_type == $type){ ?> selected<?php } ?>>
                  <?php echo $type; ?>
                </option>
                <?php } ?>
              </select>
            </div>
            <div class="form-group col-sm-6">
              <label for="comission_value">Comission Value</label>
              <input name="comission_value" type="number" step="any" class="form-control" id="comission_value"  value="<?php echo $agent->comission_value; ?>">
            </div>

          </div>

          <div class="row">
            <div class="form-group col-sm-6">
              <label for="email">Email</label>
              <input name="email" type="email" class="form-control" id="email" value="<?php echo $agent->email; ?>">
            </div>
            <div class="form-group col-sm-6">
              <label for="password">Password</label>
              <input name="password" type="text" class="form-control" placeholder="Lave empty for no change" id="password">
            </div>
          </div>


        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">
            <?php echo lang('Close'); ?></button>
          <button type="submit" class="btn btn-primary">
            <?php echo lang('Submit'); ?></button>
        </div>
      </form>
    </div>
  </div>
</div>





<div class="modal fade" id="deleteAgent" tabindex="-1" role="dialog" aria-labelledby="deleteAgent" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <form method="post" action="<?php echo base_url(); ?>admin/agent/delete">
        <input type="hidden" name="id" value="<?php echo $agent->id; ?>">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">
            <?php echo lang('Delete Agent'); ?>
          </h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
         <div class="row">
            <div class="form-group col-sm-12">
              <label for="comission_type">Please choose to which agent the current customer need to be assigned</label>
              <select class="form-control" name="newagent">
              <option value="0">
                 None
                </option>
                <?php foreach(get_agents($this->session->cid) as $a){ ?>
                <?php if($a->id != $this->uri->segment(4)){ ?>
                <option value="<?php echo $a->id; ?>">
                  <?php echo $a->agent; ?>
                </option>
                <?php } ?>
                <?php } ?>
              </select>
            </div>
          </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">
            <?php echo lang('Close'); ?></button>
          <button type="submit" class="btn btn-primary">
            <?php echo lang('Submit'); ?></button>
        </div>
      </form>
    </div>
  </div>
</div>


<div class="modal fade" id="assign_sim" tabindex="-1" role="dialog" aria-labelledby="assign_sim" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
    <form method="post" action="<?php echo base_url(); ?>admin/agent/assign_simcards" enctype="multipart/form-data">
    <input type="hidden" name="agentid" value="<?php echo $agent->id; ?>">
    <input type="hidden" name="admin" value="<?php echo $this->session->firstname." ".$this->session->lastname; ?>">
    <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">
            <?php echo lang('Upload Simcards'); ?>
          </h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      <div class="modal-body">
        <p>if you wish your Reseller to be able to assign and activate simcards, you will need to assign simcard to their accounts<br/>.</p>
        <div class="form-control">

        <input type="file" name="userfile" class="form-control">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-warning" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-default btn-primary" data-dismiss="modal">Submit</button>
      </div>
      </form>
    </div>

  </div>
</div>

<script>
  $(document).ready(function()
{
$.getJSON(window.location.protocol + '//' + window.location.host + '/admin/complete/get_lang', function(data) {
var x = new Date();
var y = x.getFullYear().toString();
var m = (x.getMonth() + 1).toString();
var d = x.getDate().toString();
(d.length == 1) && (d = '0' + d);
(m.length == 1) && (m = '0' + m);
var date = y +'-'+ m +'-'+ d;
 $('#simcardtable').DataTable({
"autoWidth": false,
"processing": true,
"orderCellsTop": true,
"ordering": true,
"serverSide": true,
"colReorder": true,
"ajax": window.location.protocol + '//' + window.location.host + '/admin/table/get_agent_simcards/<?php echo $this->uri->segment(4); ?>',
"aaSorting": [[1, 'desc']],
"language": {
"url": window.location.protocol + '//' + window.location.host +"/assets/clear/js/datatables/lang/" + data.result + ".json"
},
"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
if(aData[1] >0){
$('td:eq(1)', nRow).html('<a href="'+window.location.protocol + '//' + window.location.host + '/admin/subscription/detail/' + aData[1] + '">Assigned</a>');
}else{
$('td:eq(1)', nRow).html('FREE');
}

return nRow;
},
});

});
});

</script>
