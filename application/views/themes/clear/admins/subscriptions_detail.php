<div class="content-i">
  <div class="content-box">
    <div class="element-wrapper">
      <h6 class="element-header">
      <?php echo lang('Services Summary'); ?>
      </h6>
      <div class="element-box">
        <h5 class="form-header">
       <?php echo lang('Subscription List'); ?>
        </h5>
        <div class="table-responsive">
              <div class="row">
                <div class="col-lg-4">
                  <div class="card bg-default mb-3">
                    <div class="card-header">  <h4 class="card-title"><i class="fa fa-cubes"></i> <?php echo $service->name; ?> <div class="close">ID #<?php echo $service->orderid; ?></div></h4>

                    </div>
                    <div class="card-body">
                      <table class="table table-striper">
                        <tr>
                          <td><?php echo lang('Date Reg'); ?></td>
                          <td align="right"><?php echo date('d/m/Y', strtotime($service->regdate)); ?></td>
                        </tr>
                        <tr>
                          <td><?php echo lang('Recurring'); ?></td>
                          <td align="right">&euro;<?php echo $service->recurring; ?></td>
                        </tr>
                        <tr>
                          <td><?php echo lang('Billingcycle'); ?></td>
                          <td align="right"><?php echo $service->billingcycle; ?></td>
                        </tr>
                        <tr>
                          <td><?php echo lang('Status'); ?></td>
                          <td align="right"><?php echo $service->domainstatus; ?></td>
                        </tr>
                        <tr>
                          <td><?php echo lang('Next Invoicedate'); ?></td>
                          <td align="right"><?php echo date('d/m/Y', strtotime($service->nextinvoicedate)); ?></td>
                        </tr>
                        <?php print_r($this->data['promotion']);?>
                        <?php if ($promotion) {?>
                           <tr>
                          <td><?php echo lang('Promocode'); ?></td>
                          <td align="right"><?php echo $promotion->promocode; ?></td>
                        </tr>

                        <?php }?>
                      </table>
                    </div>
                  </div>
                </div>
                <div class="col-lg-8">
                  <div class="card mb-3">
                    <div class="card-header><h4 class="card-title"><i class="fa fa-user"></i> <?php if (!empty($client->companyname)) {?><?php echo $client->companyname; ?><?php } else {?><?php echo $client->firstname . ' ' . $client->lastname; ?><?php }?></h4></div>
                    <div class="card-body">
                      <table class="table table-striper table-bordered">
                        <thead>
                          <tr class="bg-success  text-white"">
                            <th><?php echo lang('Client ID'); ?></th>
                            <th><?php echo lang('Contact'); ?></th>
                            <th><?php echo lang('Address'); ?></th>
                            <th><?php echo lang('Email'); ?></th>
                            <th><?php echo lang('Phonenumber'); ?></th>
                          </tr>
                        </thead>
                        <tr>
                          <td><?php echo $client->id; ?></td>
                          <td><?php echo $client->firstname . ' ' . $client->lastname; ?></td>
                          <td><?php echo $client->address1 . ' ' . $client->postcode . ' ' . $client->city; ?></td>
                          <td><?php echo $client->email; ?></td>
                          <td><?php echo $client->phonenumber; ?></td>
                        </tr>
                      </table>
                      <hr />
                      <div class="text-center">
                        <button id="suspend" class="btn btn-primary btn-md"><i class="fa fa-recycle"></i> <?php echo lang('Suspend'); ?></button>
                        <button id="suspend" class="btn btn-primary btn-md"><i class="fa fa-fire"></i> <?php echo lang('Terminate'); ?></button>
                        <button id="suspend" class="btn btn-primary btn-md"><i class="fa fa-list"></i> <?php echo lang('Logs'); ?></button>
                        <button id="suspend" class="btn btn-primary btn-md"><i class="fa fa-arrow-circle-up"></i> <?php echo lang('Move Service'); ?></button>
                      </div>
                    </div>
                  </div>
                  <!-- module import -->
                  <div class="card mb-3">
                    <div class="card-header"><h4 class="card-title"><i class="fa fa-user"></i> <?php if (!empty($client->companyname)) {?><?php echo $client->companyname; ?><?php } else {?><?php echo $client->firstname . ' ' . $client->lastname; ?><?php }?></h4></div>
                    <div class="card-body">
                      <?php if (!empty($service->module_name)) {?>
                      <?php $this->load->view('modules/' . $service->module_name . '/' . $service->module_name);?>
                      <?php }?>
                      <hr />
                      <div class="text-center">
                        <button id="suspend" class="btn btn-primary btn-md"><i class="fa fa-recycle"></i> <?php echo lang('Suspend'); ?></button>
                        <button id="suspend" class="btn btn-primary btn-md"><i class="fa fa-fire"></i> <?php echo lang('Terminate'); ?></button>
                        <button id="suspend" class="btn btn-primary btn-md"><i class="fa fa-list"></i> <?php echo lang('Logs'); ?></button>
                        <button id="suspend" class="btn btn-primary btn-md"><i class="fa fa-arrow-circle-up"></i> <?php echo lang('Move Service'); ?></button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <?php //print_r($service);?>
            </div>
          </div>
        </div>
      </div>
    </div>

