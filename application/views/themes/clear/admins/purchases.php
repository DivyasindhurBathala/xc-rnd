<div class="content-i">
  <div class="content-box">
    <div class="element-wrapper">
      <h6 class="element-header">
      Purchases
      </h6>
      <div class="element-box">
        <h5 class="form-header">
        List Puschases <div class="close">
          <a class="btn btn-primary btn-sm" href="<?php echo base_url(); ?>admin/purchases/import"><i class="fa fa-plus-circle"></i> <?php echo lang('New Purchases'); ?></a>
        </div>
        </h5>
        <div class="table-responsive">
          <table class="table table-striped table-lightfont" id="clients">
            <thead>
              <tr>
                <th>#<?php echo lang('Date'); ?></th>
                <th><?php echo lang('Suppliername'); ?></th>
                <th><?php echo lang('Invoicenum'); ?></th>
                <th><?php echo lang('Total'); ?></th>
                <th></th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>