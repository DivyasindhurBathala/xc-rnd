<div class="content-i">
	<div class="content-box">
		<div class="element-wrapper">
			<h6 class="element-header">
			<?php echo lang('Planned Packages Changes'); ?>
			</h6>
			<div class="element-box">
				<h5 class="form-header">

				<?php if (empty($this->uri->segment(4))) {?>
					<?php echo lang('List Pending'); ?>
				<div class="float-right"><a href="<?php echo base_url(); ?>admin/super/change_service_list/completed" class="btn btn-danger btn-primary">Show Completed</a></div>
			<?php } else {?>
				<?php echo lang('List Completed'); ?>
				<div class="float-right"><a href="<?php echo base_url(); ?>admin/super/change_service_list" class="btn btn-danger btn-primary">Show Pending</a></div>
			<?php }?>
				</h5>
				<div class="table-responsive">
					<table class="table table-striped table-lightfont" id="packages">
						<thead>
							<tr>
								<th><?php echo lang('#ID'); ?></th>
								<th><?php echo lang('Number'); ?></th>
								<th><?php echo lang('Customer'); ?></th>
								<th><?php echo lang('Old Bundle'); ?></th>
								<th><?php echo lang('new Bundle'); ?></th>
								<th><?php echo lang('Requestor'); ?></th>
								<th><?php echo lang('Date Request'); ?></th>
								<th></th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>



<script>
<?php if (empty($this->uri->segment(4))) {?>
$(document).ready(function()
{
$.getJSON(window.location.protocol + '//' + window.location.host + '/admin/complete/get_lang', function(data) {
var x = new Date();
var y = x.getFullYear().toString();
var m = (x.getMonth() + 1).toString();
var d = x.getDate().toString();
(d.length == 1) && (d = '0' + d);
(m.length == 1) && (m = '0' + m);
var date = y +'-'+ m +'-'+ d;
console.log(date);
$('#packages').DataTable({
"autoWidth": false,
"processing": true,
"orderCellsTop": true,
"ordering": true,
"serverSide": true,
"ajax": window.location.protocol + '//' + window.location.host + '/admin/table/getservices_changes',
"aaSorting": [[1, 'desc']],
"language": {
"url": window.location.protocol + '//' + window.location.host +"/assets/clear/js/datatables/lang/" + data.result + ".json"
},
"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
$('td:eq(1)', nRow).html('<a href="'+window.location.protocol + '//' + window.location.host + '/admin/subscription/detail/'+aData[9]+'">'+aData[1]+'</a>');
$('td:eq(2)', nRow).html('<a href="'+window.location.protocol + '//' + window.location.host + '/admin/client/detail/'+aData[7]+'">'+aData[2]+'</a>');
$('td:eq(7)', nRow).html('<a class="btn btn-primary" href="javascript:void(0)" onclick="SetComplete(\''+aData[0]+'\');">Set Completed</a>');
return nRow;
},
});
});
});
<?php } else {?>
	$(document).ready(function()
{
$.getJSON(window.location.protocol + '//' + window.location.host + '/admin/complete/get_lang', function(data) {
var x = new Date();
var y = x.getFullYear().toString();
var m = (x.getMonth() + 1).toString();
var d = x.getDate().toString();
(d.length == 1) && (d = '0' + d);
(m.length == 1) && (m = '0' + m);
var date = y +'-'+ m +'-'+ d;
console.log(date);
$('#packages').DataTable({
"autoWidth": false,
"processing": true,
"orderCellsTop": true,
"ordering": true,
"serverSide": true,
"ajax": window.location.protocol + '//' + window.location.host + '/admin/table/getservices_changes/completed',
"aaSorting": [[1, 'desc']],
"language": {
"url": window.location.protocol + '//' + window.location.host +"/assets/clear/js/datatables/lang/" + data.result + ".json"
},
"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
$('td:eq(1)', nRow).html('<a href="'+window.location.protocol + '//' + window.location.host + '/admin/subscription/detail/'+aData[9]+'">'+aData[1]+'</a>');
$('td:eq(2)', nRow).html('<a href="'+window.location.protocol + '//' + window.location.host + '/admin/client/detail/'+aData[7]+'">'+aData[2]+'</a>');
$('td:eq(7)', nRow).html('Completed By: '+aData[10]);
return nRow;
},
});
});
});
	<?php }?>
</script>
<script>
	function SetComplete(id){
		$('#procid').val(id);
		$('#confirm').modal('toggle');
	}
	function confirm_do(){

		var id = $('#procid').val();
		console.log(id);
		$.ajax({
    url: window.location.protocol + '//' + window.location.host + '/admin/super/change_packages_do',
    dataType: 'json',
    type: 'post',
    contentType: 'application/json',
    data: JSON.stringify( { "id": id } ),
    processData: false,
    success: function( data, textStatus, jQxhr ){
       if(data.result){
       	window.location.href = window.location.protocol + '//' + window.location.host + '/admin/super/change_service_list';
       }else{

				 alert(data.message);
			 }
    },
    error: function( jqXhr, textStatus, errorThrown ){
        alert( errorThrown );
    }
});
	}

	</script>


<div class="modal fade" id="confirm">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <form id="cp">

        <input type="hidden" name="id" id="procid" value="">
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title"><div id="number_cancel"></div></h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <!-- Modal body -->
        <div class="modal-body">
          <div class="table-responsive">
            <?php echo lang('Are you sure that you have done required action in Magebo and Platform?, please dont do this action if you are not sure'); ?>
          </div>
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button"  onclick="confirm_do();" class="btn btn-primary"><?php echo lang('Set request Complete'); ?></button>
        </div>
      </form>
    </div>
  </div>
</div>


