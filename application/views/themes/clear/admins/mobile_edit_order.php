
<div class="content-i">
  <div class="content-box">
    <div class="element-wrapper">
      <h6 class="element-header">
      <?php echo lang('Modify Orders'); ?> #<?php echo $this->uri->segment(4); ?>
      </h6>
      <div class="element-box">
        <h5 class="form-header">
          <div id="alr"></div>
        </h5>
        <div class="table-responsive">
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <fieldset>
                  <label class="control-label" for="vat"><a href="<?php echo base_url(); ?>admin/client/edit/<?php echo $client->id; ?>"><i class="fa fa-user"></i> <?php echo lang('Customer'); ?></a></label>
                  <input name="deltaid" class="form-control" id="deltaid" type="text" placeholder="Delta Customerid" value="<?php echo $client->firstname . ' ' . $client->lastname . ' #' . getDeltaId($client->id); ?>" readonly>
                </fieldset>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <fieldset>
                  <label for="exampleSelect1"><i class="fa fa-cube"></i> <?php echo lang('Product'); ?></label>
                  <select class="form-control" id="pid" name="pid">
                    <?php foreach (getProductSell($this->session->cid) as $p) {?>
                    <option value="<?php echo $p->id; ?>"<?php if ($p->id == $service->packageid) {?> selected<?php }?>><?php echo $p->name; ?> - €<?php echo number_format($p->recurring_total, 2); ?></option>
                    <?php }?>
                  </select>
                </fieldset>
              </div>
            </div>
            <div class="col-sm-6">

            </div>
          </div>

           <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <fieldset>
                  <label for="exampleSelect1"><i class="fa fa-cube"></i> <?php echo lang('Contract Terms'); ?> (months)</label>
                    <input type="number" class="form-control" id="contract_terms" value="<?php echo $service->contract_terms; ?>">
                </fieldset>
              </div>
            </div>
            <div class="col-sm-6">

            </div>
          </div>


          <div class="row">
            <div class="col-md-6">
              <label class="control-label" for="vat"><i class="fa fa-cubes"></i> <?php echo lang('Extra'); ?> <span class="pull-right"><a href="javascript:void(0);" onclick="addAddon('<?php echo $this->uri->segment(4); ?>');"><i class="fa fa-plus-circle"></i>[<?php echo lang('Add New'); ?>]</a></span></label>
              <?php if ($addons) {?>
              <?php foreach ($addons as $addon) {?>
              <?php if ($addon->addon_type == 'option') {?>
              <table class="table table-striped">
                <tr id="list-<?php echo $addon->id; ?>">
                  <td width="50%"><?php echo $addon->name; ?></td>
                  <td width="40%" align="right">€<?php echo number_format($addon->recurring_total, 2); ?></td>
                  <td width="10%" align="right"><a href="javascript:void(0);" onclick="deleteAddon('<?php echo $addon->id; ?>');"><i class="fa fa-trash"></i></a></td>
                </table>
                <?php }?>
                <?php }?>
                 <?php }?>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-6">
                <div class="form-group">
                  <fieldset>
                    <label for="exampleSelect1"><i class="fa fa-cube"></i> <?php echo lang('Handset'); ?>
                      <a href="javascript:void(0);" onclick="addHandset('<?php echo $this->uri->segment(4); ?>');"><i class="fa fa-plus-circle"></i>[<?php echo lang('Add New'); ?>]</a>
                    </label>
                     <?php if ($addons) {?>
                    <?php foreach ($addons as $addon) {?>
                    <?php if ($addon->addon_type == 'others') {?>
                    <table class="table table-striped">
                      <tr id="list-<?php echo $addon->id; ?>">
                        <td width="50%"><?php echo $addon->name; ?> Rest: €<?php echo number_format($addon->recurring_total * $addon->terms, 2); ?> @<?php echo $addon->terms; ?> <?php echo lang('Month'); ?>  </td>
                        <td width="40%" align="right">€<?php echo number_format($addon->recurring_total, 2); ?></td>
                        <td width="10%" align="right"><a href="javascript:void(0);" onclick="deleteAddon('<?php echo $addon->id; ?>');"><i class="fa fa-trash"></i></a></td>
                      </table>
                      <?php }?>
                      <?php }?>
                      <?php }?>
                    </fieldset>
                  </div>
                </div>
                <div class="col-sm-6">
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6">
                   <div class="form-group">

                  <label for="exampleSelect1"><i class="fa fa-cube"></i> <?php echo lang('Promotion'); ?></label>

                  <select class="form-control" id="promotion" name="promotion">
                      <option value="None"><?php echo lang('None'); ?></option>

                    <?php foreach (getPromotions($this->session->cid) as $p) {?>
                    <option value="<?php echo $p['id']; ?>"<?php if ($service->promocode == $p['promo_code']) {?> selected<?php }?>><?php echo $p['promo_code']; ?> - <?php echo $p['promo_name']; ?></option>
                    <?php }?>
                  </select>
                </div>
                </div>
              </div>

               <div class="row">
                <div class="col-sm-6">
                <div class="form-group">
                    <label for=""><?php echo lang('Contract Start Date'); ?> MM-DD-YYYY</label>
                    <input class="form-control" autocomplete="new-username" value="<?php echo $contractdate; ?>" type="text" name="ContractDate" id="editorderdate" readonly>
                  </div>
                </div>
              </div>


              <div class="row">
                <div class="col-sm-6">
                     <fieldset>
                  <a class="btn btn-primary btn-md btn-block" href="<?php echo base_url(); ?>admin/subscription/pendingorders"><i class="fa fa-arrow-left"></i> <?php echo lang('Back to Orders'); ?></a>
                 </fieldset> </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <form id="memka">
      <input type="hidden" id="simid" value="<?php echo $service->id; ?>">
    </form>

    <div class="modal fade" id="addonform">
      <div class="modal-dialog">
        <div class="modal-content">
          <form id="addon2order" action="<?php echo base_url(); ?>admin/subscription/addon2order" method="post">
            <input type="hidden" name="userid" value="<?php echo $service->userid; ?>">
            <input type="hidden" name="serviceid" value="<?php echo $service->id; ?>">
            <input type="hidden" name="orderid" value="<?php echo $service->id; ?>">
             <input type="hidden" name="type" value="mobile">
            <!-- Modal Header -->
            <div class="modal-header">
              <h4 class="modal-title"><?php echo lang('Add New Addon'); ?></h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body" id="detail">
              <div class="form-group">
                <label for=""><?php echo lang('Options'); ?>:</label>
                <?php foreach (getAddonSell('mobile', $this->session->cid) as $row) {?>
                <?php if ($row->id != "18") {?>
                <div class="form-check">
                  <label class="form-check-label">
                    <input name="addon[]" type="checkbox" class="form-check-input optionaddon" value="<?php echo $row->id; ?>"<?php if ($row->id == "18") {?> id="handset_id"<?php }?>>
                  <?php echo $row->name . ' &euro;' . str_replace('.', ',', $row->recurring_total) . '/month'; ?>         </label>
                </div>
                <?php }?>
                <?php }?>
              </div>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
              <button type="submit" class="btn btn-primary"><?php echo lang('Add Addon'); ?></button>
            </div>
          </form>
        </div>
      </div>
    </div>
    <div class="modal fade" id="handsetform">
      <div class="modal-dialog">
        <div class="modal-content">
          <form id="handser2order" action="<?php echo base_url(); ?>admin/subscription/handset2order" method="post">
             <input type="hidden" name="type" value="mobile">

            <input type="hidden" name="userid" value="<?php echo $service->userid; ?>">
            <input type="hidden" name="serviceid" value="<?php echo $service->id; ?>">
            <input type="hidden" name="orderid" value="<?php echo $service->id; ?>">
             <input type="hidden" name="addonid" value="18">
            <!-- Modal Header -->
            <div class="modal-header">
              <h4 class="modal-title"><?php echo lang('Add New Handset'); ?></h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body" id="detail">
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label for=""><?php echo lang('Name Of the Handset for Invoice'); ?><span class="text-danger">*</span></label>
                    <input class="form-control" placeholder="Iphone 7s" type="text" name="handset_name" id="handset_name">
                  </div>
                  <div class="form-group">
                    <label for=""><?php echo lang('Handset Price To be Paid'); ?>
                      <span class="text-danger">*</span>
                    </label>
                    <input class="form-control" placeholder="100.00" type="number" step="any" name="handset_price" id="handset_price">
                  </div>
                  <div class="form-group">
                    <label for=""><?php echo lang('Terms (Month)'); ?><span class="text-danger">*</span></label>
                    <input class="form-control" placeholder="12" type="number"  name="handset_month" id="handset_month">
                  </div>
                </div>
              </div>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
              <button type="submit" class="btn btn-primary"><?php echo lang('Add Handset'); ?></button>
            </div>
          </form>
        </div>
      </div>
    </div>
    <script>
    function deleteAddon(id){
    var answer = confirm("<?php echo lang('Do you wish to delete this Option?'); ?>?")
    if (answer){
    $('.form-header').html('<div class="alert alert-success" role="alert"> <strong><?php echo lang('Addon Deleted'); ?></strong></div>');
    window.setTimeout(function() {
    $(".alert").fadeTo(500, 0).slideUp(500, function(){
    $(this).remove();
    });
    }, 3000);
    $.ajax({
    url: '<?php echo base_url(); ?>admin/subscription/delete_addon',
    dataType: 'json',
    type: 'post',
    data: {
    orderid: '<?php echo $this->uri->segment(4); ?>',
    id: id
    },
    success: function(data) {
    console.log(data);
    if(data.result){
    $('#list-'+id).html('');
    }
    },
    error: function(errorThrown) {
    //console.log(errorThrown);
    }
    });
    }
    }
    function addAddon(id){
    $('#addonform').modal('toggle');

    }
    function addHandset(id){
    $('#handsetform').modal('toggle');
    /*
    $('.form-header').html('<div class="alert alert-success" role="alert"> <strong>Addon Deleted</strong></div>');
    window.setTimeout(function() {
    $(".alert").fadeTo(500, 0).slideUp(500, function(){
    $(this).remove();
    });
    }, 3000);
    */
    }


    $('#pid').on('change', function() {
    var id = this.value;
    console.log(id);
    $.ajax({
    url: '<?php echo base_url(); ?>admin/subscription/changeProduct_addon/<?php echo $service->userid; ?>',
    dataType: 'json',
    type: 'post',
    data: {
    orderid: '<?php echo $this->uri->segment(4); ?>',
    serviceid: '<?php echo $service->id; ?>',
    newid: id,
    contract_terms: $('#contract_terms').val(),
    oldid: '<?php echo $service->packageid; ?>'
    },
    success: function(data) {
    console.log(data);
     alert('<?php echo lang('Product change has been applied'); ?>');

    },
    error: function(errorThrown) {
    //console.log(errorThrown);
    }
    });
    });

 $('#contract_terms').on('keyup', function() {
    var id = this.value;
    console.log(id);
    $.ajax({
    url: '<?php echo base_url(); ?>admin/subscription/changeProduct_addon/<?php echo $service->userid; ?>',
    dataType: 'json',
    type: 'post',
    data: {
    orderid: '<?php echo $this->uri->segment(4); ?>',
    serviceid: '<?php echo $service->id; ?>',
    newid: id,
    contract_terms: $('#contract_terms').val(),
    oldid: '<?php echo $service->packageid; ?>'
    },
    success: function(data) {
    console.log(data);
     alert('<?php echo lang('Product change has been applied'); ?>');

    },
    error: function(errorThrown) {
    //console.log(errorThrown);
    }
    });
    });


    $('#promotion').on('change', function() {
    var id = this.value;
    console.log(id);
    $.ajax({
    url: '<?php echo base_url(); ?>admin/subscription/changePromotion/<?php echo $service->userid; ?>',
    dataType: 'json',
    type: 'post',
    data: {
    serviceid: '<?php echo $service->id; ?>',
    id: id,
    oldid: '<?php echo $service->packageid; ?>'
    },
    success: function(data) {
      alert('<?php echo lang('Promotion code has been applied'); ?>');
     },
    error: function(errorThrown) {
    //console.log(errorThrown);
    }
    });
    });


    </script>