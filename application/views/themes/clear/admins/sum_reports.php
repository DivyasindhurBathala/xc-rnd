<div class="content-i">
  <div class="content-box">
    <div class="element-wrapper">
      <h6 class="element-header">
      Services Mobile
      </h6>
      <div class="element-box">
        <h5 class="form-header">
        <?php echo lang('SUM Reports'); ?>
        </h5>
        <div class="table-responsive">
            <table class="table table-striped table-lightfont" id="services">
              <thead>
                <tr>
                  <th><?php echo lang('Clientid#'); ?></th>
                  <th><?php echo lang('Customername'); ?></th>
                  <th><?php echo lang('MSISDN'); ?></th>
                  <th><?php echo lang('Date'); ?></th>
                  <th><?php echo lang('Amount'); ?></th>
                  <th><?php echo lang('Target'); ?></th>
                  <th><?php echo lang('Usage'); ?></th>
                  <th><?php echo lang('Usage %'); ?></th>
                  <th><?php echo lang('Usage €'); ?></th>
                  <th><?php echo lang('Estimate %'); ?></th>
                  <th><?php echo lang('Estimate €'); ?></th>
                </tr>
              </thead>
              <tbody>
              </tbody>
              <tfoot>
            <tr>
                <th>ID</th>
                <th>Package</th>
                <th>Customer</th>
                <th>Status</th>
                <th>Recurring</th>
                 <th>Identity</th>
                  <th>Date Contract</th>
                    <th>Status</th>
                <th>Recurring</th>
                 <th>Identity</th>
                  <th>Date Contract</th>
            </tr>
        </tfoot>
            </table>
        </div>




      </div>
    </div>
  </div>
</div>



<script>
  $(document).ready(function()
{
$.getJSON(window.location.protocol + '//' + window.location.host + '/admin/complete/get_lang', function(data) {
$('#services').DataTable({
    "autoWidth": false,
    "processing": true,
    "orderCellsTop": true,
    "ordering": true,
    "serverSide": false,
    "pageLength": 50,
    "ajax": window.location.protocol + '//' + window.location.host + '/admin/table/get_sum_reports/'+status,
    "aaSorting": [[7, 'asc']],
    "language": {
    "url": window.location.protocol + '//' + window.location.host +"/assets/clear/js/datatables/lang/" + data.result + ".json"
        },
  "fnRowCallback": function( nRow, aData, iDisplayIndex ) {

      $('td:eq(1)', nRow).html('<a  class="showprogress" href="'+window.location.protocol + '//' + window.location.host + '/admin/client/detail/' + aData[13] + '">' + aData[1] + '</a>');
      $('td:eq(0)', nRow).html('<a  class="showprogress" href="'+window.location.protocol + '//' + window.location.host + '/admin/client/detail/' + aData[13] + '">' + aData[0] + '</a>');
        
            $('td:eq(2)', nRow).html('<a  class="showprogress" href="'+window.location.protocol + '//' + window.location.host + '/admin/subscription/detail/' + aData[14] + '">' + aData[2] + '</a>');

             $('td:eq(4)', nRow).html('€' + aData[4]);
              $('td:eq(5)', nRow).html('€' + aData[5]);
               $('td:eq(6)', nRow).html('€' + aData[6]);
                $('td:eq(8)', nRow).html('€' + aData[8]);
                 $('td:eq(10)', nRow).html('€' + aData[10]);
                 var kontol = Math.ceil(100-(aData[7]*100));
                 if(kontol >= 70){
 $('td:eq(7)', nRow).html('<strong class="text-danger">'+kontol+'%<strong>');
                 }else if(kontol >= 50){
 $('td:eq(7)', nRow).html('<strong class="text-primary">'+kontol+'%<strong>');
                 }else{
 $('td:eq(7)', nRow).html('<strong class="text-success">'+kontol+'%<strong>');
                 }
               
                 $('td:eq(9)', nRow).html(Math.ceil(100-(aData[9]*100))+'%');

              return nRow;
        },
       
  });
});
});



</script>
<script>
  function importxDSL(serviceid){
    $('#import_title').html('Importing #'+serviceid);
    $('#mvnoserviceid').val(serviceid);
    $('#ImportWhmcs').modal('toggle');
  }
function Processorder() {
$.ajax({
url: '<?php echo base_url(); ?>admin/super/processImport',
type: 'post',
dataType: 'json',
data: $('#importxdsltable').serialize(),
success: function (data) {
if(data.result == "success"){
window.location.href = window.location.protocol + '//' + window.location.host +'/admin/subscription/';

}else{

  window.location.href = window.location.protocol + '//' + window.location.host +'/admin/subscription/';
}
}

});


}

  $('#importnow').click(function(){
    $('#importnow').prop('disabled', true);
    $('#bodyid').hide();
    $('#loader').show('slow');
var whmcsid = $('#serviceidx').val();
var serviceid = $('#mvnoserviceid').val();
var edpnetcid = $('#circuitid').val();
$.ajax({
url: '<?php echo base_url(); ?>admin/super/getWhmcsOrder',
type: 'post',
dataType: 'json',
data: {'serviceid':whmcsid,'localid':serviceid,'circuitid':edpnetcid},
success: function (data) {
  console.log(data);
  $('#userid').val(data.userid);
  $('#circuitid').val(data.xdslCircuitID);
if(data.result == "success"){
    $('#loader').hide('slow');
    $('#importnow').prop('disabled', true);
    $('#resultid').html('<span class="text-success"><strong>Congratulation we found these information: #'+whmcsid+'</strong></span><table class="table table-border table-hover"><tr><td>Address</td><td>'+data.oaStreet+' '+data.oaHouseNumber+' '+data.oaHouseNumberAlfa+' '+data.oaZipCode+' '+data.oaCity+'</td></tr> <tr><td>Circuitid</td><td>'+data.xdslCircuitID+'</td></tr><tr><td>PPPOE USER</td><td>'+data.dsllogin+'</td></tr><tr><td>PPPOE PASS</td><td>'+data.dslpass+'</td></tr> <tr><td>REALM</td><td>'+data.realm+'</td></tr><tr><td>CircuitID EDPNET</td><td>'+edpnetcid+'</td></tr> </table><br /><br /><button class="btn btn-primary btn-sm" type="button" onclick="Processorder();">Confirm Import</button> <input type="hidden" name="street"  id="street" value="'+data.oaStreet+'"> <input type="hidden" name="number" id="number" value="'+data.oaHouseNumber+'"> <input type="hidden" name="alpha" id="alpha" value="'+data.oaHouseNumberAlfa+'"> <input type="hidden" name="city" id="city" value="'+data.oaCity+'"> <input type="hidden" name="postcode" id="postcode" value="'+data.oaZipCode+'">  <input type="hidden" name="username" id="username" value="'+data.dsllogin+'"> <input type="hidden" name="password" id="password" value="'+data.dslpass+'"><input type="hidden" name="realm" id="realm" value="'+data.realm+'">');
}else{
    $('#importnow').prop('disabled', false);
    $('#bodyid').show();
    $('#loader').hide('slow');
    $('#resultid').html('<span class="text-danger">Sorry we could not found order id: #'+whmcsid+'</span>');


}

}

});
  });
</script>
