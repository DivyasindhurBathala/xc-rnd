<?php //print_r($ticket);?>
<?php //echo $this->session->cid; ?>
<?php //print_r(getHelpdeskCategory($this->session->cid));?>
<div aria-hidden="true" aria-labelledby="replyticket" id="replyticket" class="modal fade bd-assign-modal-lg" role="dialog" tabindex="-1">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="assigntoother">
        Reply Ticket
        </h5>
        <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> ×</span></button>
      </div>
      <div class="modal-body">
        <form id="replyForm" enctype="multipart/form-data" method="post" action="<?php echo base_url(); ?>reseller/helpdesk/reply/<?php echo $this->uri->segment(4); ?>">
          <input type="hidden" name="relid" value="<?php echo $this->uri->segment(4); ?>">
          <input type="hidden" name="old_deptid" value="<?php echo $ticket['deptid']; ?>">
          <input type="hidden" name="old_assigne" value="<?php echo $ticket['admin']; ?>">
           <input type="hidden" name="old_priority" value="<?php echo $ticket['priority']; ?>">
            <input type="hidden" name="old_categoryid" value="<?php echo $ticket['categoryid']; ?>">
          <fieldset>
            <div class="row">
            <div class="form-group col-md-4">
              <label for="deptid">Department</label>
              <select class="form-control" id="deptid" name="deptid">
                <?php foreach (getDepartments($this->session->cid) as $dep) {?>
                <option value="<?php echo $dep['id']; ?>"<?php if ($dep["id"] == $ticket['deptid']) {?> selected<?php }?>><?php echo $dep['name']; ?></option>
                <?php }?>
              </select>
            </div>
            <div class="form-group col-md-4">
              <label for="status">Change Status</label>
              <select class="form-control" id="status" name="status">
                <?php foreach (getStatuses() as $status) {?>
                <option value="<?php echo $status; ?>"<?php if ($status == "Answered") {?> selected<?php }?>><?php echo $status; ?></option>
                <?php }?>
              </select>
            </div>
             <div class="form-group col-md-4">
              <label for="priority">Change Priority</label>
              <select class="form-control" id="priority" name="priority">
                <?php foreach (array('4', '3', '2', '1') as $prio) {?>
                <option value="<?php echo $prio; ?>"<?php if ($prio == $ticket['priority']) {?> selected<?php }?>><?php echo $prio; ?></option>
                <?php }?>
              </select>
            </div>
          </div>
           <div class="row">
            <div class="form-group col-md-6">
              <label for="assigne">Assign to</label>
              <select class="form-control" id="assigne" name="assigne">
                <?php foreach (getStaf($this->session->cid) as $staf) {?>
                <option value="<?php echo $staf['id']; ?>"<?php if ($staf['id'] == $ticket['admin']) {?> selected<?php }?>><?php echo $staf['firstname'] . ' ' . $staf['lastname']; ?></option>
                <?php }?>
              </select>
            </div>
               <div class="form-group col-md-6">
              <label for="categoryid">Change Category</label>
              <select class="form-control" id="categoryid" name="categoryid">
              <?php foreach (getHelpdeskCategory($this->session->cid) as $row) {?>
                    <option value="<?php echo $row->id; ?>"<?php if ($ticket['categoryid'] == $row->id) {?> selected<?php }?>><?php echo $row->name; ?></option>
                    <?php }?>
              </select>
            </div>
          </div>
            <div class="form-group">
              <label for="message">Message</label>
              <textarea class="form-control" id="message" rows="10" name="message" required></textarea>
            </div>
            <div class="form-group">
              <label for="multiple">Attachment</label>
              <input type="file" name="attachments[]" class="form-control" multiple="multiple">
            </div>
            <button type="submit" id="replybutton" class="btn btn-primary btn-block btn-success"><i class="fa fa-reply"></i> Submit</button>
          </fieldset>
        </form>
      </div>
    </div>
  </div>
</div>
<div aria-hidden="true" aria-labelledby="replyticket" id="closeticket" class="modal fade bd-assign-modal-lg" role="dialog" tabindex="-1">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="assigntoother">
        Close ticket
        </h5>
        <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> ×</span></button>
      </div>
      <div class="modal-body">
        <center>Do you wish to cloe the ticket?</center>
        <br />
        <br />
        <center><h2> <?php echo $ticket['tid']; ?></h2></center>
        <br />
        <form enctype="multipart/form-data" id="myForm" method="post" action="<?php echo base_url(); ?>reseller/helpdesk/close/<?php echo $this->uri->segment(4); ?>">
          <div class="form-group">
            <label for="exampleTextarea">Resolution</label>
            <textarea class="form-control" id="exampleTextarea" rows="10" name="message" required></textarea>
          </div>
          <input type="hidden" name="id" value="<?php echo $this->uri->segment(4); ?>">
          <input type="hidden" name="status" value="Closed">
          <button type="submit" id="closeticketing" onclick="close_ticket()"class="btn btn-primary btn-block btn-success"><i class="fa fa-reply"></i> Submit</button>
        </fieldset>
      </form>
    </div>
  </div>
</div>
</div>

<div class="content-i">
<div class="content-box">
  <div class="support-index">
    <div class="support-tickets">
      <div class="support-tickets-header">
        <div class="tickets-control">
          <h5>
          <?php echo $ticket['tid']; ?>
          </h5>
          <div class="element-search">
            <div class="pull-right"><a href="<?php echo base_url(); ?>reseller/helpdesk" class="btn btn-primary btn-md">Back to All Tickets</a></div>
          </div>
        </div>
      </div>
      <div id="ticketblock">
        <div class="element-box">
          <?php //print_r($ticket);?>
          <table class="table table-striped">
            <tr>
              <td><?php echo lang('Client ID'); ?></td>
              <td><a href="<?php echo base_url(); ?>admin/client/detail/<?php echo $client->id; ?>"><?php echo $client->mvno_id; ?></a></td>
            </tr>
            <tr>
              <td><?php echo lang('Customer Name'); ?></td>
              <td><a href="<?php echo base_url(); ?>admin/client/detail/<?php echo $client->id; ?>"><?php echo $client->initial . ' ' . $client->firstname . ' ' . $client->lastname; ?></a></td>
            </tr>
            <tr>
              <td><?php echo lang('Ticket Department'); ?></td>
              <td><?php echo $ticket['deptname']; ?></td>
            </tr>
            <tr>
              <td><?php echo lang('Assigne'); ?></td>
              <td><?php echo $ticket['assigned']; ?></td>
            </tr>
            <tr>
              <td><?php echo lang('Ticket Category'); ?></td>
              <td><?php echo $ticket['categoryname']; ?></td>
            </tr>
            <tr>
              <td><?php echo lang('Date Created'); ?></td>
              <td><?php echo $ticket['date']; ?></td>
            </tr>
             <tr>
              <td><?php echo lang('Status'); ?></td>
              <td><strong><?php echo $ticket['status']; ?></strong></td>
            </tr>

             <tr>
              <td><?php echo lang('Priority'); ?></td>
              <td><strong<?php if ($ticket['priority'] < 3) {?> class="text-danger"<?php }?>><?php echo $ticket['priority']; ?></strong></td>
            </tr>
          </table>
          <hr />
          <button id="close_ticket" class="btn btn-success btn-block"><i class="fa fa-window-close"></i> Close Ticket</button>
          <button id="assign_ticket" class="btn btn-success btn-block"><i class="fa fa-users"></i> Assign to others</button>
          <button id="reply_ticket" class="btn btn-success btn-block"><i class="fa fa-comment"></i> Respond to ticket</button>
           <button id="reply_ticket" class="btn btn-success btn-block" onclick="confirm_visible('<?php echo $ticket['id']; ?>');"><i class="fa fa-eye"></i> Set Visible to Client</button>
        </div>
      </div>
      <script>
        function confirm_visible(id){

          var t = confirm('Are you sure?, this will allow customer to see all history conversations of this ticket');
          if(t){

             $.ajax({
      url: window.location.protocol + '//' + window.location.host + '/admin/complete/set_ticket_visible/<?php echo $this->session->cid; ?>',
      type: 'POST',
      data: {
        id: id
      },
      dataType: 'json',
      success: function(response){
      console.log(response);

      }
      });

          }


        }
      function close_ticket(){
      $('#closeticketing').prop('disabled', true);
      $('#closeticket').modal('toggle');
      document.getElementById("myForm").submit();
      }
      $('#assign_ticket').click(function(){
      $('#assigntosomeone').modal('toggle');
      });
      $('#replybutton').click(function(){
      $('#replyticket').modal('toggle');
      $('#replybutton').prop('disabled', true);
      document.getElementById("replyForm").submit();
      });
      $('#reply_ticket').click(function(){
      $('#replyticket').modal('toggle');
      });
      $('#close_ticket').click(function(){
      $('#closeticket').modal('toggle');
      $('#closeticket').modal('toggle');
      });
      </script>
      <!--
      <div class="load-more-tickets" id="loading">
        <a href="#" onclick="load_more('7');"><span>Load More Tickets...</span></a>
      </div>
      -->
    </div>
    <?php if (!empty($ticket)) {?>
    <div class="support-ticket-content-w folded-info" style="padding-bottom: 60px;">
      <div class="support-ticket-content">
        <div class="support-ticket-content-header">
          <h3 class="ticket-header">
          <?php echo $ticket['subject']; ?>
          </h3>
          <a class="back-to-index" href="#"><i class="os-icon os-icon-arrow-left5"></i><span>Back</span></a><a class="show-ticket-info" href="#"><span>Show Details</span><i class="os-icon os-icon-documents-03"></i></a>
        </div>
        <div class="ticket-thread">
          <?php if (!empty($ticket['replies'])) {?>
          <?php foreach ($ticket['replies'] as $key => $row) {?>
          <?php if ($row['isadmin']) {?>
          <div class="ticket-reply highlight">
            <div class="ticket-reply-info">
              <a class="author with-avatar" href="#"><img alt="" src="<?php echo base_url(); ?>assets/img/staf/<?php echo $this->session->userdata('picture'); ?>"><span><?php echo $row['name']; ?></span></a><span class="info-data"><span class="label"><?php if ($key == "0") {?>posted on<?php } else {?>replied on<?php }?></span><span class="value"><?php echo date('d/m/Y H:i:s', strtotime($row['date'])); ?></span></span><span class="badge badge-success">support agent</span>
              <div class="actions" href="#">
              </div>
            </div>
            <div class="ticket-reply-content">
              <?php echo $row['message']; ?>
            </div>
            <?php if (!empty($row['attachments'])) {?>
            <div class="ticket-attachments">
              <?php foreach (explode('|', $row['attachments']) as $r) {?>
              <a class="attachment" href="<?php echo base_url(); ?>reseller/helpdesk/download_file/<?php echo $r; ?>"> <i class="os-icon os-icon-ui-51"></i><span><?php echo $r; ?></span></a>
              <?php }?>
            </div>
            <?php }?>
          </div>
          <?php } else {?>
          <div class="ticket-reply">
            <div class="ticket-reply-info">
              <a class="author with-avatar" href="#"><img alt="" src="<?php echo base_url(); ?>assets/img/client.png"><span><?php echo $row['fullname']; ?></span></a><span class="info-data"><span class="label">replied on</span><span class="value"><?php echo date('d/m/Y H:i:s', strtotime($row['date'])); ?></span></span>
              <div class="actions" href="#">
                <i class="os-icon os-icon-ui-46"></i>
              </div>
            </div>
            <div class="ticket-reply-content">
              <?php echo $row['message']; ?>
            </div>
            <?php if (!empty($row['attachments'])) {?>
            <div class="ticket-attachments">
              <?php foreach (explode('|', $row['attachments']) as $r) {?>
              <a class="attachment" href="<?php echo base_url(); ?>reseller/helpdesk/download_file/<?php echo $r; ?>"> <i class="os-icon os-icon-ui-51"></i><span><?php echo $r; ?></span></a>
              <?php }?>
            </div>
            <?php }?>
          </div>
          <?php }?>
          <?php }?>
          <?php }?>
          <div class="ticket-reply highlight">
            <div class="ticket-reply-info">
              <button class="btn btn-sm btn-block btn-secondary"><i class="os-icon os-icon-ui-47"></i></button>
            </div>
          </div>
          <!-- end replies -->
          <div id="replybox" style="display:none; padding-bottom: 60px;">
            <form enctype="multipart/form-data" method="post" action="<?php echo base_url(); ?>reseller/helpdesk/reply/<?php echo $this->uri->segment(4); ?>">
              <input type="hidden" name="relid" value="<?php echo $this->uri->segment(4); ?>">
              <input type="hidden" name="old_deptid" value="<?php echo $ticket['deptid']; ?>">
              <input type="hidden" name="old_assigne" value="<?php echo $ticket['admin']; ?>">
              <fieldset>
                <div class="form-group">
                  <label for="exampleSelect1">Department</label>
                  <select class="form-control" id="exampleSelect1" name="deptid">
                    <?php foreach (getDepartments($this->session->cid) as $dep) {?>
                    <option value="<?php echo $dep['id']; ?>"<?php if ($dep["id"] == $ticket['deptid']) {?> selected<?php }?>><?php echo $dep['name']; ?></option>
                    <?php }?>
                  </select>
                </div>
                <div class="form-group">
                  <label for="exampleSelect1">Change Status</label>
                  <select class="form-control" id="exampleSelect1" name="status">
                    <?php foreach (getStatuses() as $status) {?>
                    <option value="<?php echo $status; ?>"<?php if ($status == "Answered") {?> selected<?php }?>><?php echo $status; ?></option>
                    <?php }?>
                  </select>
                </div>
                <div class="form-group">
                  <label for="exampleSelect1">Assign to</label>
                  <select class="form-control" id="exampleSelect1" name="assigne">
                    <?php foreach (getStaf($this->session->cid) as $staf) {?>
                    <option value="<?php echo $staf['id']; ?>"<?php if ($staf['id'] == $ticket['admin']) {?> selected<?php }?>><?php echo $staf['firstname'] . ' ' . $staf['lastname']; ?></option>
                    <?php }?>
                  </select>
                </div>
                <div class="form-group">
                  <label for="exampleTextarea">Message</label>
                  <textarea class="form-control" id="exampleTextarea" rows="10" name="message" required></textarea>
                </div>
                <div class="form-group">
                  <label for="exampleSelect1">Attachment</label>
                  <input type="file" name="attachments[]" class="form-control" multiple="multiple">
                </div>
                <button type="submit" class="btn btn-primary btn-block btn-success"><i class="fa fa-reply"></i> Submit</button>
              </fieldset>
            </form>
          </div>
        </div>
      </div>
      <div class="support-ticket-info" style="padding-bottom: 60px;">
        <a class="close-ticket-info" href="#"><i class="os-icon os-icon-ui-23"></i></a>
        <div class="customer">
          <div class="avatar">
            <img alt="" src="img/avatar1.jpg">
          </div>
          <h4 class="customer-name">
          <?php echo ucfirst($ticket['fullname']); ?>
          </h4>
          <div class="customer-tickets">
            <?php echo getTotalTickets($ticket['userid']); ?>
          </div>
        </div>
        <h5 class="info-header">
        Ticket Details
        </h5>
        <div class="info-section text-center">
          <div class="label">
            Created:
          </div>
          <div class="value">
            <?php echo $ticket['date']; ?>
          </div>
          <div class="label">
            Department
          </div>
          <div class="value">
            <div class="badge badge-primary">
              <?php echo $ticket['deptname']; ?>
            </div>
          </div>
        </div>
        <h5 class="info-header">
        Agents Assigned
        </h5>
        <div class="info-section">
          <ul class="users-list as-tiles">
            <?php if ($ticket['admin'] != '') {?>
            <li>
              <a class="author with-avatar" href="#">
                <div class="avatar" style="background-image: url(<?php echo base_url() . 'assets/img/staf/' . $_SESSION['picture']; ?>)"></div>
                <span><?php echo $ticket['staf'] ?></span></a>
              </li>
              <?php if ($ticket['admin'] != $_SESSION['firstname'] . ' ' . $_SESSION['lastname']) {?>
              <li>
                <a class="add-agent-btn" href="<?php echo base_url(); ?>reseller/helpdesk/assigntome/<?php echo $ticket['id']; ?>">
                  <div class="avatar" style="background-image: url(<?php echo base_url() . 'assets/clear/client/staf.png'; ?>)"></div>
                  <span>Assign to me</span></a>
                </li>
                <?php } else {?>
                <li>
                  <a class="add-agent-btn" href="#" data-target=".bd-assign-modal-lg" data-toggle="modal">
                    <div class="avatar" style="background-image: url(<?php echo base_url() . 'assets/clear/client/staf.png'; ?>)"></div>
                    <span>Assign to other</span></a>
                  </li>
                  <?php }?>
                  <?php } else {?>
                  <li>
                    <a class="add-agent-btn" href="<?php echo base_url(); ?>reseller/helpdesk/assigntome/<?php echo $ticket['id']; ?>">
                      <div class="avatar" style="background-image: url(<?php echo base_url() . 'assets/clear/client/staf.png'; ?>)"></div>
                      <span>Assign to me</span></a>
                    </li>
                    <?php }?>
                  </ul>
                </div>
              </div>
            </div>
            <?php } else {?>
            <div class="support-ticket-content-w folded-info">
              <div class="support-ticket-content">
                <div class="support-ticket-content-header">
                  <h3 class="ticket-header">
                  Tickets thread
                  </h3>
                  <a class="back-to-index" href="#"><i class="os-icon os-icon-arrow-left5"></i><span>Back</span></a><a class="show-ticket-info" href="#"><span>Show Details</span><i class="os-icon os-icon-documents-03"></i></a>
                </div>
                <div class="ticket-thread">
                  No tickets
                </div>
              </div>
            </div>
            <?php }?>
          </div>
        </div>
      </div>
      <script>
      $('document').ready(function() {
      $("#replybutton").click(function(event) {
      $('#replybox').show();
      $('#replybutton').hide();
      });
      $(".support-ticket").click(function(event) {
      var id =  $(this).attr('id');
      window.location.replace(window.location.protocol + '//' + window.location.host + '/reseller/helpdesk/detail/'+id);
      });
      });

      </script>

        <div aria-hidden="true" aria-labelledby="assign" id="assigntosomeone" class="modal fade bd-assign-modal-lg" role="dialog" tabindex="-1">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="assigntoother">
             Ticket assignment
              </h5>
              <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> ×</span></button>
            </div>
            <form method="post" action="<?php echo base_url(); ?>reseller/helpdesk/assigntother">
              <input type="hidden" name="ticketid" value="<?php echo $ticket['id']; ?>">
              <div class="modal-body" id="modalonly">
                <div class="form-group">
                  <label for="departmen">Agent</label>
                  <select class="form-control" id="departmen" name="adminid">
                    <?php foreach (getStaf($this->session->cid) as $staf) {?>
                      <?php if ($this->session->id != $staf['id']) {?>
                    <option value="<?php echo $staf['id']; ?>"><?php echo $staf['firstname'] . ' ' . $staf['lastname']; ?></option>
                  <?php }?>
                    <?php }?>
                  </select>
                </div>
                <div class="form-group">
                  <label for="departmen">Department</label>
                  <select class="form-control" id="departmen" name="deptid">
                    <?php foreach (getDepartments($this->session->cid) as $row) {?>
                    <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
                    <?php }?>
                  </select>
                </div>
                <div class="modal-footer">
                  <button class="btn btn-primary btn-block" type="submit"><i class="fa fa-reply"></i> Save changes</button>
                </div>
              </form>
            </div>
          </div>
        </div>