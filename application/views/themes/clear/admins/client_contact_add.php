<div class="content-i">
  <div class="content-box">
    <div class="element-wrapper">
      <h6 class="element-header">
       <?php echo lang('Client'); ?>
      </h6>
      <div class="element-box">
        <h5 class="form-header">
        <?php echo lang('Add Contact'); ?>
        </h5>
         <div class="form-desc">
           <?php echo lang('All Fields supose to be filled correctly'); ?>
        </div>
        <div class="table-responsive">
            <form method="post" action="<?php echo base_url(); ?>admin/client/contact_add">
            	<input name="userid" value="<?php echo $this->uri->segment(4); ?>" type="hidden">
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <fieldset>
                      <label class="control-label" for="role"><?php echo lang('Role'); ?></label>
                      <input  name="role" class="form-control" id="role" type="text" placeholder="<?php echo lang('Role/Function'); ?>"<?php if (!empty($_SESSION['registration'])) {?> value="<?php echo $_SESSION['registration']['role']; ?>" <?php }?>>
                    </fieldset>
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <fieldset>
                      <label class="control-label" for="role"><?php echo lang('Email Invoice'); ?></label>
                     <select class="form-control" id="invoiceemails"  name="invoiceemails" >
                        <?php foreach (array('0' => 'No', '1' => 'Yes') as $key => $em) {?>
                        <option value="<?php echo $key; ?>"<?php if ($key == $_SESSION['registration']['invoiceemails']) {?> selected<?php }?>><?php echo $em; ?></option>
                        <?php }?>
                      </select>
                    </fieldset>
                  </div>
                </div>


              </div>
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <fieldset>
                      <label class="control-label" for="firstname"><?php echo lang('Firstname'); ?> <span class="text-danger">*</span></label>
                      <input  name="firstname" class="form-control" id="firstname" type="text" placeholder="<?php echo lang('Firstname'); ?>"<?php if (!empty($_SESSION['registration'])) {?> value="<?php echo $_SESSION['registration']['firstname']; ?>" <?php }?> required>
                    </fieldset>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <fieldset>
                      <label class="control-label" for="lastname"><?php echo lang('Lastname'); ?> <span class="text-danger">*</span></label>
                      <input  name="lastname" class="form-control" id="lastname" type="text" placeholder="<?php echo lang('Lastname'); ?>"<?php if (!empty($_SESSION['registration'])) {?> value="<?php echo $_SESSION['registration']['lastname']; ?>" <?php }?> required>
                    </fieldset>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-4">
                  <div class="form-group">
                    <fieldset>
                      <label class="control-label" for="address1"><?php echo lang('Address'); ?> <span class="text-danger">*</span></label>
                      <input  name="address1" class="form-control" id="address1" type="text" placeholder="<?php echo lang('Address'); ?>"<?php if (!empty($_SESSION['registration'])) {?> value="<?php echo $_SESSION['registration']['address1']; ?>" <?php }?> required>
                    </fieldset>
                  </div>
                </div>
                <div class="col-sm-2">
                  <div class="form-group">
                    <fieldset>
                      <label class="control-label" for="Postcode"><?php echo lang('Postcode'); ?> <span class="text-danger">*</span></label>
                      <input  name="postcode" class="form-control" id="postcode" type="text" placeholder="<?php echo lang('Postcode'); ?>"<?php if (!empty($_SESSION['registration'])) {?> value="<?php echo $_SESSION['registration']['postcode']; ?>" <?php }?> required>
                    </fieldset>
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="form-group">
                    <fieldset>
                      <label class="control-label" for="City"><?php echo lang('City'); ?> <span class="text-danger">*</span></label>
                      <input  name="city" class="form-control" id="city" type="text"<?php if (!empty($_SESSION['registration'])) {?> value="<?php echo $_SESSION['registration']['city']; ?>" <?php }?>  placeholder="<?php echo lang('City'); ?>" required>
                    </fieldset>
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="form-group">
                    <fieldset>
                      <label for="exampleSelect1"><?php echo lang('Country'); ?> <span class="text-danger">*</span></label>
                      <select class="form-control" id="country"  name="country" >
                        <?php foreach (getCountries() as $key => $country) {?>
                        <option value="<?php echo $key; ?>"<?php if ($key == "NL") {?> selected<?php }?>><?php echo $country; ?></option>
                        <?php }?>
                      </select>
                    </fieldset>
                  </div>
                </div>
              </div>
              <div class="row">


                <div class="col-sm-4">
                  <fieldset>
                    <label class="control-label" for="Phonenumber"><?php echo lang('Phonenumber'); ?> <span class="text-danger">*</span></label>
                    <input name="phonenumber" class="form-control" id="phonenumber" type="number" placeholder="<?php echo lang('Phonenumber'); ?>"<?php if (!empty($_SESSION['registration'])) {?> value="<?php echo $_SESSION['registration']['phonenumber']; ?>" <?php }?> required>
                  </fieldset>
                </div>
                <div class="col-sm-4">
                  <div class="form-group">
                    <fieldset>
                      <label class="control-label" for="Email"><?php echo lang('Email'); ?> <span class="text-danger">*</span></label>
                      <input name="email"  class="form-control" id="email" type="email" placeholder="<?php echo lang('Email address'); ?>"<?php if (!empty($_SESSION['registration'])) {?> value="<?php echo $_SESSION['registration']['email']; ?>" <?php }?> required>
                    </fieldset>
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="form-group">
                    <fieldset>
                      <label class="control-label" for="Gsm"><?php echo lang('Gsm'); ?></label>
                      <input name="gsm"  class="form-control" id="Gsm" type="number"<?php if (!empty($_SESSION['registration'])) {?> value="<?php echo $_SESSION['registration']['gsm']; ?>" <?php }?> placeholder="<?php echo lang('Gsm'); ?>">
                    </fieldset>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-sm-12">
                  <div class="form-group">
                    <button type="submit" class="btn btn-md btn-primary btn-block"><i class="fa fa-save"></i> <?php echo lang('Save'); ?></button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
