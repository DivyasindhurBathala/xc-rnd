<div class="content-i">
  <div class="content-box">
    <div class="element-wrapper">
      <h6 class="element-header">
      Purchases
      </h6>
      <div class="element-box">
        <h5 class="form-header">
        Import Invoice <div class="close">
          <h3 class="h4">Create/Modify Invoice Customer : <?php echo $client->firstname . ' ' . $client->lastname . ' - ' . $client->companyname; ?></h3>
        </div>
        </h5>
        <div class="table-responsive">
          <form method="post" action="<?php echo base_url(); ?>admin/warehouse/uploadinvoice"  enctype="multipart/form-data">
            <fieldset>
              <legend>Upload Invoices</legend>
              <div class="form-group">
                <label for="exampleSelect1">Example select</label>
                <select class="form-control" id="exampleSelect1" name="supplierid">
                  <?php foreach (getSuppliers() as $row) {?>
                  <option value="<?php echo $row->id; ?>"><?php echo $row->name; ?></option>
                  <?php }?>
                </select>
              </div>
              <div class="form-group">
                <label for="exampleInputFile">Upload Pdf Invoice</label>
                <input type="file" class="form-control-file" id="exampleInputFile" aria-describedby="fileHelp" name="invoice">

              </div>
              <button type="submit" class="btn btn-primary">Submit</button>
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>