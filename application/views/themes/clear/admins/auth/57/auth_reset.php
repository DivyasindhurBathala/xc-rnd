<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo lang('Admin Reset Password'); ?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/<?php echo $setting->default_theme; ?>/client/lumen.css?version=1.0">
    <!-- Font Awesome CSS-->
          <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css">
     <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/<?php echo $setting->default_theme; ?>/client/style.default.css?version=1.0" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/<?php echo $setting->default_theme; ?>/client/login.css?version1.0">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
  </head>
  <body>

          <?php if (!empty($this->session->flashdata('error'))) {?>
          <div class="row">
            <div class="col-lg-12">
              <div class="alert alert-danger" role="alert">
                <strong class="text-center"><?php echo $this->session->flashdata('error'); ?></strong>
              </div>
            </div>
          </div>
          <?php }?>
          <?php if (!empty($this->session->flashdata('success'))) {?>
          <div class="row">
            <div class="col-lg-12">
              <div class="alert alert-warning" role="alert">
                <strong class="text-center"><?php echo $this->session->flashdata('success'); ?></strong>
              </div>
            </div>
          </div>
          <?php }?>

<div class="login-page">
  <img src="<?php echo $setting->logo_site; ?>" width="360">
  <br />
  <br />
  <div class="form">
     <form id="login-form" method="post" action="<?php echo base_url(); ?>admin/auth/reset">
      <input type="text" placeholder="email" name="email" required=""/>
      <button><i class="fa fa-key"></i> <?php echo lang('reset password'); ?></button>
      <p class="message"><?php echo lang('Know your password'); ?>? <a href="<?php echo base_url(); ?>admin/auth"><?php echo lang('Login'); ?></a></p>
    </form>
  </div>
</div>

<!--
          <div class="row">

            <div class="col-lg-6">
              <div class="info d-flex align-items-center">
                <div class="content">
                  <div class="logo">
                    <img src="<?php print_r($setting->logo_site);?>" width="400">
                  </div>
                  <p><i class="fa fa-lock text-warning" aria-hidden="true"></i> Unauthorized access will be logged and reported!</p>
                </div>
              </div>
            </div>

            <div class="col-lg-6 bg-white">
              <div class="form d-flex align-items-center">
                <div class="content">
                  <form id="login-form" method="post" action="<?php echo base_url(); ?>client/auth/reset">
                    <div class="form-group">
                      <input id="login-username" type="email" name="email" required="" class="input-material">
                      <label for="login-username" class="label-material">Email Address</label>
                    </div>
                    <button id="login" class="btn btn-primary" type="submit">Reset Password</button>

                  </form>
                  <a href="<?php echo base_url(); ?>client/auth" class="forgot-pass">Login</a>
                </div>
              </div>
            </div>
          </div>


  <!-- Javascript files-->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

</body>
</html>









