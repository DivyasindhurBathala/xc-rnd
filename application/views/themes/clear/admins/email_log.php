<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo lang('Admin Login'); ?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/<?php echo $setting->default_theme; ?>/client/lumen.css?version=1.0">
    <!-- Font Awesome CSS-->
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
  </head>
  <body>
    <br />
    <br />
    <div class="row container">
      <div class="col-12">

          <div class="form-group row">
      <label for="staticEmail" class="col-sm-2 col-form-label">Date:</label>
      <div class="col-sm-10">
        <input type="text" readonly="" class="form-control" id="staticEmail" value="<?php echo $email->date; ?>">
      </div>
    </div>


    

     <div class="form-group row">
      <label for="staticEmail" class="col-sm-2 col-form-label">To:</label>
      <div class="col-sm-10">
        <input type="text" readonly="" class="form-control" id="staticEmail" value="<?php echo $email->to; ?>">
      </div>
    </div>
     <div class="form-group row">
      <label for="staticEmail" class="col-sm-2 col-form-label">Subject:</label>
      <div class="col-sm-10">
        <input type="text" readonly="" class="form-control" id="staticEmail" value="<?php echo $email->subject; ?>">
      </div>
    </div>

     <div class="form-group">
      <label for="exampleTextarea">Message</label>
      <?php echo $email->message; ?>
    </div>
      </div>
    </div>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
  </body>
</html>