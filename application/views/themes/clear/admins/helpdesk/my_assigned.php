
<div class="content-i">
		<div class="content-box">
				<div class="row">
						<div class="col-sm-3">
				<a class="element-box el-tablo" href="<?php echo base_url(); ?>admin/helpdesk/my_assigned/Open">
					<div class="label">
						<?php echo lang('Open Ticket Assigned to you'); ?>
					</div>
					<div class="value">
					<?php echo $stats['myassign']; ?>
					</div>
					<div class="trending trending-up-basic">
						<span></span><i class="os-icon os-icon-arrow-up2"></i>
					</div>
				</a>
			</div>
			<div class="col-sm-3">
				<a class="element-box el-tablo" href="<?php echo base_url(); ?>admin/helpdesk/status/Open">
					<div class="label">
						<?php echo lang('All Open tickets'); ?>
					</div>
					<div class="value">
					<?php echo $stats['Open']; ?>
					</div>
					<div class="trending trending-up-basic">
						<span></span><i class="os-icon os-icon-arrow-up2"></i>
					</div>
				</a>
			</div>
			<div class="col-sm-3">
				<a class="element-box el-tablo" href="<?php echo base_url(); ?>admin/helpdesk/priority/high">
					<div class="label">
						<?php echo lang('Ticket with High Priority'); ?>
					</div>
					<div class="value">
					<?php echo $stats['high']; ?>
					</div>
					<div class="trending trending-down-basic">
						<i class="os-icon os-icon-arrow-down"></i>
					</div>
				</a>
			</div>
			<div class="col-sm-3">
				<a class="element-box el-tablo" href="<?php echo base_url(); ?>admin/helpdesk/status/Closed">
					<div class="label">
						<?php echo lang('Closed Tickets'); ?>
					</div>
					<div class="value">
					<?php echo $stats['Closed']; ?>
					</div>
					<div class="trending trending-down-basic">
						<i class="os-icon os-icon-arrow-down"></i>
					</div>
				</a>
			</div>
		</div>
	</div>
</div>

<div class="content-i">
	<div class="content-box">
		<div class="element-wrapper">
			<h6 class="element-header">
			Helpdesk Support <?php echo $status; ?><div class="close">
				<div class="bd-example">
					<!-- <div class="btn-group">
						<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Category</button>
						<div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">
							<?php foreach (getTicketCategories($this->session->cid) as $row) {?>
							<a class="dropdown-item" href="#" onclick="filter('<?php echo $row->id; ?>')"><?php echo $row->name; ?></a>
							<?php }?>
							</div>
						</div> -->
						<div class="btn-group">
							<button class="btn btn-danger" data-target=".bd-newticket-modal-lg" data-toggle="modal"><i class="fa fa-life-ring" aria-hidden="true"></i> New ticket</button>
							</div><!-- /btn-group -->
							<!--<div class="btn-group">
								<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Department</button>
								<div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">
									<?php foreach (getDepartments($this->session->cid) as $row) {?>
									<a class="dropdown-item" href="#" onclick="filter('<?php echo $row['id']; ?>')"><?php echo $row['name']; ?></a>
									<?php }?>
									</div>
								</div>-->
							</div>
						</div>
						</h6>
						<div class="element-box">
							<h5 class="form-header">
							<?php echo lang('Ticket List'); ?>
                            <div class="float-right text-white">
                            <a href="<?php echo base_url(); ?>admin/helpdesk/my_assigned/Open" class="btn btn-warning btn-sm">Open</a>
                            <a href="<?php echo base_url(); ?>admin/helpdesk/my_assigned/Answered" class="btn btn-warning btn-sm">Answered</a>
                           <a href="<?php echo base_url(); ?>admin/helpdesk/my_assigned/Closed" class="btn btn-warning btn-sm">Closed</a>
                           <a href="<?php echo base_url(); ?>admin/helpdesk/my_assigned/On-Hold" class="btn btn-warning btn-sm">On-Hold</a>
                           <a href="<?php echo base_url(); ?>admin/helpdesk/my_assigned/Proefbilling" class="btn btn-warning btn-sm">Proefbilling</a>
                           <a href="<?php echo base_url(); ?>admin/helpdesk/my_assigned/Resolved" class="btn btn-warning btn-sm">Resolved</a></div>
							</h5>

							<div class="table-responsive">
								<table class="table table-striped table-lightfont" id="helpdesk">
									<thead>
										<tr>
											<th><?php echo lang('Ticketid'); ?></th>
											<th><?php echo lang('Customer'); ?></th>
											<th><?php echo lang('Subject'); ?></th>
											<th><?php echo lang('Department'); ?></th>
											<th><?php echo lang('Date'); ?></th>
											<th><?php echo lang('Status'); ?></th>
											<th><?php echo lang('Assigned'); ?></th>
											<th><?php echo lang('Priority'); ?></th>
											<th><?php echo lang('Category'); ?></th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div aria-hidden="true" aria-labelledby="newticket" class="modal fade bd-newticket-modal-lg" role="dialog" tabindex="-1">
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">
							New Support Ticket
							</h5>
							<button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> ×</span></button>
						</div>
						<form method="post" action="<?php echo base_url(); ?>admin/helpdesk/newticket" enctype="multipart/form-data">
							<input type="hidden" name="admin" value="<?php echo $this->session->firstname . ' ' . $this->session->lastname; ?>">
							<div class="modal-body" id="modalonly">
								<div class="form-group">
									<label for="subject">Client</label>
									<input id="customerid" name="userid" class="form-control  ui-autocomplete-input ui-autocomplete-loading modalonly" placeholder="Search Customer.." type="text" required>
								</div>
								<div id="pelanggan" style="display:none;">
									<div class="form-group">
										<label for="subject">Client</label>
										<input id="namapelanggan" name="name" class="form-control" placeholder="" type="text" disabled>
									</div>
								</div>
								<div class="form-group">
									<label for="subject">Subject</label>
									<input name="subject" class="form-control" placeholder="Title" type="text" required>
								</div>
								<div class="form-group">
									<label for="departmen">Category</label>
									<select class="form-control" id="departmen" name="categoryid">
										<?php foreach (getHelpdeskCategory($this->session->cid) as $row) {?>
										<option value="<?php echo $row->id; ?>"><?php echo $row->name; ?></option>
										<?php }?>
									</select>
								</div>
								<div class="form-group">
									<label for="departmen">Department</label>
									<select class="form-control" id="departmen" name="deptid">
										<?php foreach (getDepartments($this->session->cid) as $row) {?>
										<option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
										<?php }?>
									</select>
								</div>
								<div class="form-group">
									<label for="exampleSelect1">Assign to</label>
									<select class="form-control" id="exampleSelect1" name="assigne">
										<?php foreach (getStaf($this->session->cid) as $staf) {?>
										<option value="<?php echo $staf['id']; ?>"<?php if ($staf['id'] == $this->session->id) {?> selected<?php }?>><?php echo $staf['firstname'] . ' ' . $staf['lastname']; ?></option>
										<?php }?>
									</select>
								</div>
								<div class="form-group">
									<label for="file">Attachments</label>
									<input name="attachments[]" type="file" class="form-control-file" id="attachments" aria-describedby="fileHelp" multiple="">
									<small id="fileHelp" class="form-text text-muted">Allowed type docx, doc, xls, xlsx, pdf, gif, png, jpg.</small>
								</div>
								<div class="form-group">
									<label for="message">Message</label>
									<textarea name="message" class="form-control" id="message" rows="6"></textarea>
								</div>
							</div>
							<div class="modal-footer">
								<input type="hidden" name="admin" value="<?php echo $this->session->firstname . ' ' . $this->session->lastname; ?>">
								<button class="btn btn-primary" type="submit" id="bos" disabled> Save changes</button>
							</div>
						</form>
					</div>
				</div>
			</div>
            <script>
            $(document).ready(function()
{
$.getJSON(window.location.protocol + '//' + window.location.host + '/admin/complete/get_lang', function(data) {
$('#helpdesk').DataTable({
"autoWidth": false,
"processing": true,
"orderCellsTop": true,
"ordering": true,
"serverSide": true,
"ajax":window.location.protocol + '//' + window.location.host + '/admin/table/get_mytickets_assign/<?php echo $this->uri->segment(4); ?>',
"aaSorting": [[4, 'desc']],
"language": {
 "url": window.location.protocol + '//' + window.location.host +"/assets/clear/js/datatables/lang/" + data.result + ".json"
},
"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
$('td:eq(2)', nRow).html('<a href="'+window.location.protocol + '//' + window.location.host + '/admin/helpdesk/detail/' + aData[8] + '"><strong>' + aData[2] + '</strong></a>');
$('td:eq(0)', nRow).html('<a href="'+window.location.protocol + '//' + window.location.host + '/admin/helpdesk/detail/' + aData[8] + '"><strong>' + aData[0] + '</strong></a>');
if(aData[9] > 0){
$('td:eq(1)', nRow).html('<a href="'+window.location.protocol + '//' + window.location.host + '/admin/client/detail/' + aData[9] + '"><strong>' + aData[1] + '</strong></a>');
}
$('td:eq(8)', nRow).html(aData[10]);
//$('td:eq(1)', nRow).html('<a href="https://invoice.xmusix.eu/admin/clientssummary.php?userid=' + aData[5] + '">' + aData[1] + '</a>');
return nRow;
},

});
});
});
</script>