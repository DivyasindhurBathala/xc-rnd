
<div class="content-i">
		<div class="content-box">
				<div class="row">
						<div class="col-sm-3">
				<a class="element-box el-tablo" href="<?php echo base_url(); ?>admin/helpdesk/my_assigned/Open">
					<div class="label">
						<?php echo lang('Open Ticket Assigned to you'); ?>
					</div>
					<div class="value">
					<?php echo $stats['myassign']; ?>
					</div>
					<div class="trending trending-up-basic">
						<span></span><i class="os-icon os-icon-arrow-up2"></i>
					</div>
				</a>
			</div>
			<div class="col-sm-3">
				<a class="element-box el-tablo" href="<?php echo base_url(); ?>admin/helpdesk/status/Open">
					<div class="label">
						<?php echo lang('All Open tickets'); ?>
					</div>
					<div class="value">
					<?php echo $stats['Open']; ?>
					</div>
					<div class="trending trending-up-basic">
						<span></span><i class="os-icon os-icon-arrow-up2"></i>
					</div>
				</a>
			</div>
			<div class="col-sm-3">
				<a class="element-box el-tablo" href="<?php echo base_url(); ?>admin/helpdesk/priority/high">
					<div class="label">
						<?php echo lang('Ticket with High Priority'); ?>
					</div>
					<div class="value">
					<?php echo $stats['high']; ?>
					</div>
					<div class="trending trending-down-basic">
						<i class="os-icon os-icon-arrow-down"></i>
					</div>
				</a>
			</div>
			<div class="col-sm-3">
				<a class="element-box el-tablo" href="<?php echo base_url(); ?>admin/helpdesk/status/Closed">
					<div class="label">
						<?php echo lang('Closed Tickets'); ?>
					</div>
					<div class="value">
					<?php echo $stats['Closed']; ?>
					</div>
					<div class="trending trending-down-basic">
						<i class="os-icon os-icon-arrow-down"></i>
					</div>
				</a>
			</div>
		</div>
	</div>
</div>

<div class="content-i">
	<div class="content-box">
		<div class="element-wrapper">
			<h6 class="element-header">
			Helpdesk Support <div class="float-right">
				<div class="bd-example">
					<!-- <div class="btn-group">
						<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Category</button>
						<div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">
							<?php foreach (getTicketCategories($this->session->cid) as $row) {?>
							<a class="dropdown-item" href="#" onclick="filter('<?php echo $row->id; ?>')"><?php echo $row->name; ?></a>
							<?php }?>
							</div>
						</div> -->
						<div class="btn-group">
							<button class="btn btn-danger" data-target=".bd-newticket-modal-lg" data-toggle="modal"><i class="fa fa-life-ring" aria-hidden="true"></i> New ticket</button>
							</div><!-- /btn-group -->
							<!--<div class="btn-group">
								<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Department</button>
								<div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">
									<?php foreach (getDepartments($this->session->cid) as $row) {?>
									<a class="dropdown-item" href="#" onclick="filter('<?php echo $row['id']; ?>')"><?php echo $row['name']; ?></a>
									<?php }?>
									</div>
								</div>-->
							</div>
						</div>
						</h6>
						<div class="element-box">
							<h5 class="form-header">
							<?php echo lang('Ticket List'); ?>
							</h5>
							<div class="table-responsive">
								<table class="table table-striped table-lightfont" id="helpdesk">
									<thead>
										<tr>
											<th><?php echo lang('Ticketid'); ?></th>
											<th><?php echo lang('Customer'); ?></th>
											<th><?php echo lang('Subject'); ?></th>
											<th><?php echo lang('Department'); ?></th>
											<th><?php echo lang('Date'); ?></th>
											<th><?php echo lang('Status'); ?></th>
											<th><?php echo lang('Assigned'); ?></th>
											<th><?php echo lang('Priority'); ?></th>
											<th><?php echo lang('Category'); ?></th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div aria-hidden="true" aria-labelledby="newticket" class="modal fade bd-newticket-modal-lg" role="dialog" tabindex="-1">
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">
							New Support Ticket
							</h5>
							<button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> ×</span></button>
						</div>
						<form method="post" action="<?php echo base_url(); ?>admin/helpdesk/newticket" enctype="multipart/form-data">
							<input type="hidden" name="admin" value="<?php echo $this->session->firstname . ' ' . $this->session->lastname; ?>">
							<div class="modal-body" id="modalonly">
								<div class="form-group">
									<label for="subject">Client</label>
									<input id="customerid" name="userid" class="form-control  ui-autocomplete-input ui-autocomplete-loading modalonly" placeholder="Search Customer.." type="text" required>
								</div>
								<div id="pelanggan" style="display:none;">
									<div class="form-group">
										<label for="subject">Client</label>
										<input id="namapelanggan" name="name" class="form-control" placeholder="" type="text" disabled>
									</div>
								</div>
								<div class="form-group">
									<label for="subject">Subject</label>
									<input name="subject" class="form-control" placeholder="Title" type="text" required>
								</div>
								<div class="form-group">
									<label for="departmen">Category</label>
									<select class="form-control" id="departmen" name="categoryid">
										<?php foreach (getHelpdeskCategory($this->session->cid) as $row) {?>
										<option value="<?php echo $row->id; ?>"><?php echo $row->name; ?></option>
										<?php }?>
									</select>
								</div>
								<div class="form-group">
									<label for="departmen">Department</label>
									<select class="form-control" id="departmen" name="deptid">
										<?php foreach (getDepartments($this->session->cid) as $row) {?>
										<option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
										<?php }?>
									</select>
								</div>
								<div class="form-group">
									<label for="exampleSelect1">Assign to</label>
									<select class="form-control" id="exampleSelect1" name="assigne">
										<?php foreach (getStaf($this->session->cid) as $staf) {?>
										<option value="<?php echo $staf['id']; ?>"<?php if ($staf['id'] == $this->session->id) {?> selected<?php }?>><?php echo $staf['firstname'] . ' ' . $staf['lastname']; ?></option>
										<?php }?>
									</select>
								</div>
								<div class="form-group">
									<label for="file">Attachments</label>
									<input name="attachments[]" type="file" class="form-control-file" id="attachments" aria-describedby="fileHelp" multiple="">
									<small id="fileHelp" class="form-text text-muted">Allowed type docx, doc, xls, xlsx, pdf, gif, png, jpg.</small>
								</div>
								<div class="form-group">
									<label for="message">Message</label>
									<textarea name="message" class="form-control" id="message" rows="6"></textarea>
								</div>
							</div>
							<div class="modal-footer">
								<input type="hidden" name="admin" value="<?php echo $this->session->firstname . ' ' . $this->session->lastname; ?>">
								<button class="btn btn-primary" type="submit" id="bos" disabled> Save changes</button>
							</div>
						</form>
					</div>
				</div>
			</div>
			<!--
			<section class="dashboard-counts no-padding-bottom">
												<div class="container-fluid">
																					<div class="row bg-white has-shadow">
																														<div class="col-xl-4 col-sm-6">
																																							<div class="item d-flex align-items-center">
																																																<div class="icon bg-red"><i class="icon-padnote"></i></div>
								<div class="title"><span><?php echo lang('Open'); ?><br><?php echo lang('Tickets'); ?></span>
								<div class="progress">
									<div role="progressbar" style="width: 70%; height: 4px;" aria-valuenow="{#val.value}" aria-valuemin="0" aria-valuemax="100" class="progress-bar bg-red"></div>
								</div>
							</div>
							<div class="number"><strong><?php echo $stats['Open']; ?></strong></div>
						</div>
					</div>
					<div class="col-xl-4 col-sm-6">
						<div class="item d-flex align-items-center">
							<div class="icon bg-green"><i class="icon-bill"></i></div>
							<div class="title"><span><?php echo lang('In-Progress'); ?><br><?php echo lang('Ticket'); ?></span>
							<div class="progress">
								<div role="progressbar" style="width: 40%; height: 4px;" aria-valuenow="{#val.value}" aria-valuemin="0" aria-valuemax="100" class="progress-bar bg-green"></div>
							</div>
						</div>
						<div class="number"><strong><?php echo $stats['In-Progress']; ?></strong></div>
					</div>
				</div>
				<div class="col-xl-4 col-sm-6">
					<div class="item d-flex align-items-center">
						<div class="icon bg-orange"><i class="icon-check"></i></div>
						<div class="title"><span><?php echo lang('Awaiting-Reply'); ?><br><?php echo lang('Ticket'); ?></span>
						<div class="progress">
							<div role="progressbar" style="width: 50%; height: 4px;" aria-valuenow="{#val.value}" aria-valuemin="0" aria-valuemax="100" class="progress-bar bg-orange"></div>
						</div>
					</div>
					<div class="number"><strong><?php echo $stats['Awaiting-Reply']; ?></strong></div>
				</div>
			</div>
		</div>
	</div>
</section>
-->