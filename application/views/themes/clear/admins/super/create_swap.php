<div class="content-i">
  <div class="content-box">
    <div class="element-wrapper">
      <h6 class="element-header">
       <?php echo lang('Superadmin Create Swap SIM'); ?> 
       <div class="close">
       
       </div>
      </h6>
      <div class="element-box">
         <div class="table-responsive">
         <form id="swapsim">
         <div class="row">
            <div class="col-sm-3">
              <div class="form-group">
                <fieldset>
                  <label class="control-label" for="vat"><?php echo lang('Simcard Number'); ?><span class="text-danger">*</span></label>
                  <input  name="simcardnbr" class="form-control" id="deltaid" type="text" placeholder="<?php echo lang('Simcard Number'); ?>"<?php if (!empty($_SESSION['registration']['mvno_id'])) {?> value="<?php echo $_SESSION['registration']['mvno_id']; ?>" <?php }?> required>
                </fieldset>
              </div>
            </div>
            <div class="col-sm-3">
              <div class="form-group">
                <fieldset>
                  <label class="control-label" for="vat"><?php echo lang('CompanyName'); ?><span class="text-danger"></span></label>
                <select name="company" class="form-control">
                <option value="95">ResellerLMobi</option>
                <option value="26">Ello Mobile</option>
                <option value="1">UNited Prepaid</option>
                <option value="67">UNited Postpaid</option>
                </select>
                </fieldset>
              </div>
            </div>
            <div class="col-sm-3">
              <div class="form-group">
              <label class="control-label" for="vat"></label>
              <button class="btn btn-md btn-primary">Convert Sim</button>
              </div>
              </div>
           
          </div>
          </form>
        
        </div>
      </div>
    </div>
  </div>
</div>