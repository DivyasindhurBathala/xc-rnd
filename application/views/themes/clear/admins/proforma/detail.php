
<?php //print_r($invoice);?>
<div class="content-i">
  <div class="content-box">
    <div class="element-wrapper">
      <h6 class="element-header">
      Client Proforma:  <a href="<?php echo base_url(); ?>admin/client/detail/<?php echo $client->id; ?>"><?php echo $client->firstname . ' ' . $client->lastname . ' - ' . $client->companyname; ?></a>
      </h6>
      <div class="element-box">
        <h5 class="form-header">
        </h5>
        <div class="table-responsive">
              <?php if ($invoice['status'] == 'Paid') {?>

            <div class="row">
                  <div class="col-lg-12 card">

              <div class="alert alert-success" role="alert">
                <strong>Please click Open subscription button below to process the Order</strong>

        </div>
                  </div>
            </div>
        <?php }?>
        <div class="row" style="display:none;" id="addpaymentblock">
            <div class="col-lg-12 card">
                <div class="card bg-light bg-secondary">
                    <div class="close">
                        <button type="button" id="addpaymenthide" class="btn btn-sm btn-primary"><?php echo lang('Hide Panel'); ?></button>
                    </div>
                    <div class="card-header d-flex align-items-center">
                        <h3 class="h4"><?php echo lang('Add Payment to'); ?> #<?php echo $invoice['invoicenum']; ?></h3>
                    </div>
                    <div class="card-body">
                        <form method="post" action="<?php echo base_url(); ?>admin/proforma/addpayment">
                            <input type="hidden" name="invoiceid" id="invoiceid" value="<?php echo $invoice['id']; ?>">
                             <input type="hidden" name="invoicenum" id="invoicenum" value="<?php echo $invoice['invoicenum']; ?>">
                            <input type="hidden" name="adminid" value="<?php echo $_SESSION['id']; ?>">
                            <input type="hidden" name="userid" value="<?php echo $invoice['userid']; ?>">
                            <div class="form-group">
                                <label for="exampleSelect1"><?php echo lang('Payment Method'); ?></label>
                                <select class="form-control" id="exampleSelect1" name="paymentmethod">
                                    <option value="banktransfer"><?php echo lang('Bank transfer'); ?></option>
                                    <option value="cash">Cash</option>
                                    <option value="paypal">Paypal</option>
                                    <option value="mollie">Mollie</option>
                                    <option value="creditcard">Credit Card</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="amount"><?php echo lang('Amount (<?php echo $setting->currency; ?>)') ?></label>
                                <input name="amount" type="number" step="any" class="form-control" id="total" aria-describedby="emailHelp" placeholder="10.0" autocomplete="off" value="<?php echo $invoice['total']; ?>" required>
                                <small id="emailHelp" class="form-text text-muted"><?php echo lang('Specify the amount you recieved'); ?>.</small>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1"><?php echo lang('Transaction Date'); ?></label>
                                <input name="date" type="text" class="form-control" value="<?php echo date('Y-m-d'); ?>" id="pickdate">
                            </div>
                            <div class="form-group">
                                <label for="transactionid"><?php echo lang('Transaction ID'); ?></label>
                                <input name="transid" type="text" class="form-control" id="transactionid" placeholder="Transactionid" value="<?php echo random_str(); ?>-000<?php echo $_SESSION['id']; ?>" required>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary btn-sm"><i class="fa fa-credit-card"></i> <?php echo lang('Add Payment'); ?></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="close">
                        <a class="btn btn-sm btn-secondary" href="<?php echo base_url(); ?>admin/proforma/download/<?php echo $invoice['id']; ?>" target="_blank"><i class="fa fa-download"></i> <?php echo lang('Download Proforma Invoice'); ?></a>
                        <button class="btn btn-primary btn-sm" id="sendinvoice"  onclick="confirmation_send();"><i class="fa fa-envelope"></i> <?php echo lang('Send Invoice'); ?></button>
                        <?php if ($invoice['status'] != 'Paid' || $invoice['status'] != 'Draft') {?>
                      <!--  <button class="btn btn-primary btn-sm" id="addpayment"><i class="fa fa-bank"></i> <?php echo lang('Add Payment'); ?></button> -->
                        <?php }?>

                           <?php if ($invoice['status'] == 'Paid') {?>

                               <a class="btn btn-sm btn-secondary" href="<?php echo base_url(); ?>admin/subscription/detail/<?php echo $invoice['items'][0]['serviceid']; ?>" target="_blank"><i class="fa fa-cubes"></i> <?php echo lang('Open Subscription'); ?></a>

                           <?php }?>
                    </div>
                    <div class="card-header d-flex align-items-center">
                        <h3 class="h4"><?php echo lang('Proforma Invoice Details'); ?> #<?php echo $invoice['invoicenum']; ?></h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                            <table class="table table-bordered table-striped">
                                <thead>
                                    <tr class="text-white bg-primary">
                                    <th><?php echo lang('Client ID'); ?></th>
                                    <th><?php echo lang('InvoiceDate'); ?></th>
                                    <th><?php echo lang('Invoice DueDate'); ?></th>
                                    <th><?php echo lang('Payment Reference'); ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><?php echo $invoice['userid']; ?></td>
                                        <td><?php echo $invoice['date']; ?></td>
                                        <td><?php echo $invoice['duedate']; ?></td>
                                        <td><?php echo $invoice['notes']; ?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <p class="lead"><?php echo lang('Payment Methods'); ?>:</p>
                                <div class="row">
                                    <div class="col-md-12">
                                        <table class="table table-borderless table-sm">
                                            <tbody>
                                                <tr>
                                                    <td><?php echo lang('Bank name'); ?>:</td>
                                                    <td class="text-right"></td>
                                                </tr>
                                                <tr>
                                                    <td><?php echo lang('Acc name'); ?>:</td>
                                                    <td class="text-right"></td>
                                                </tr>
                                                <tr>
                                                    <td><?php echo lang('IBAN'); ?>:</td>
                                                    <td class="text-right"></td>
                                                </tr>
                                                <tr>
                                                    <td><?php echo lang('BIC code'); ?>:</td>
                                                    <td class="text-right"></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                            </div>
                            <div class="col-md-4">
                                <p class="lead"><?php echo lang('Customer Information'); ?></p>
                                <div class="row">
                                    <div class="col-md-12">
                                        <table class="table table-borderless table-sm">
                                            <tbody>
                                                <tr>
                                                    <td><i class="fa fa-bank"></i> <?php echo lang('Companyname'); ?>:</td>
                                                    <td class="text-right"><a href="<?php echo base_url(); ?>admin/client/detail/<?php echo $client->id; ?>"><?php echo $client->companyname; ?></a></td>
                                                </tr>
                                                <tr>
                                                    <td><i class="fa fa-user"></i> <?php echo lang('Customer name'); ?>:</td>
                                                    <td class="text-right"><a href="<?php echo base_url(); ?>admin/client/detail/<?php echo $client->id; ?>"><?php echo $client->firstname . ' ' . $client->lastname; ?></a></td>
                                                </tr>
                                                <tr>
                                                    <td><i class="fa fa-address-card"></i> Adress:</td>
                                                    <td class="text-right"><a href="<?php echo base_url(); ?>admin/client/detail/<?php echo $client->id; ?>"><?php echo $client->address1 . ' <br />' . $client->postcode . ' ' . $client->city; ?></a></td>
                                                </tr>
                                                <tr>
                                                    <td><i class="fa fa-certificate"></i> <?php echo lang('Btw'); ?></td>
                                                    <td class="text-right"><a href="<?php echo base_url(); ?>admin/client/detail/<?php echo $client->id; ?>"><?php echo $client->vat; ?></a></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="table-responsive col-sm-12">
                                <table class="table table-v2">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th><?php echo lang('Item & Description'); ?></th>
                                            <th class="text-right"><?php echo lang('Subtotal'); ?></th>
                                            <th class="text-right"><?php echo lang('TaxRate(Amount@%)'); ?></th>
                                            <th class="text-right"><?php echo lang('Total'); ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($invoice['items'] as $key => $item) {?>
                                        <tr>
                                            <th scope="row"><?php echo $key + 1; ?></th>
                                            <td>
                                                <?php echo nl2br($item['description']); ?>
                                            </td>
                                            <td class="text-right"><?php echo $setting->currency; ?><?php echo number_format(exvat($item['taxrate'], $item['amount']),2); ?></td>
                                            <td class="text-right"><?php echo $setting->currency; ?><?php echo number_format(vat($item['taxrate'], $item['amount']),2); ?></td>
                                            <td class="text-right"><?php echo $setting->currency; ?><?php echo number_format($item['amount'],2); ?></td>
                                        </tr>
                                        <?php }?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-9">
                                <?php if ($invoice['status'] == "Unpaid") {?>
                                    <?php if ($invoice['duedate'] > date('Y-m-d')) {?>
                                <img src="<?php echo base_url(); ?>assets/clear/img/unpaid.jpeg" height="100">
                                    <?php } elseif ($invoice['duedate'] <= date('Y-m-d')) {?>
                                    <img width="200" src="<?php echo base_url(); ?>assets/clear/img/overdue.jpg">
                                    <?php }?>

                                <?php } elseif ($invoice['status'] == "Paid") {?>
                                    <img src="<?php echo base_url(); ?>assets/clear/img/paid.png">
                                <?php } else {?>
                                <?php }?>
                            </div>
                            <div class="col-md-3">
                                <p class="lead"><?php echo lang('Total due'); ?></p>
                                <div class="table-responsive">
                                    <table class="table">
                                        <tbody>
                                            <tr>
                                                <td><?php echo lang('Subtotal'); ?></td>
                                                <td class="text-right"><?php echo $setting->currency; ?><?php echo number_format($invoice['subtotal'],2); ?></td>
                                            </tr>
                                            <tr>
                                                <td><?php echo lang('Btw'); ?> <?php echo $invoice['items'][0]['taxrate']; ?>%</td>
                                                <td class="text-right"><?php echo $setting->currency; ?><?php echo number_format($invoice['tax'],2); ?></td>
                                            </tr>
                                            <tr>
                                                <td class="text-bold-800"><?php echo lang('Total'); ?></td>
                                                <td class="text-bold-800 text-right"> <?php echo $setting->currency; ?><?php echo number_format($invoice['total'],2); ?></td>
                                            </tr>
                                            <tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                          </div>
                    </div>
                </div>


                <div class="container-fluid">
                <?php if (!empty($transactions)) {?>
                <div class="card bg-light bg-secondary">
                    <div class="card-header d-flex align-items-center">
                        <h3 class="h4"><?php echo lang('Transactions'); ?> </h3>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive col-sm-12">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#TRANSID</th>
                                        <th class="text-right"><?php echo lang('Date'); ?></th>
                                        <th class="text-right"><?php echo lang('Method'); ?></th>
                                        <th class="text-right"><?php echo lang('Total'); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($transactions as $item) {?>
                                    <tr>
                                        <td>
                                            <?php echo $item['transid']; ?>
                                        </td>
                                        <td class="text-right"><?php echo date('d/m/Y', strtotime($item['date'])); ?></td>
                                        <td class="text-right"><?php echo ucfirst($item['paymentmethod']) ?></td>
                                        <td class="text-right"><?php echo $setting->currency; ?><?php echo $item['amount']; ?></td>
                                    </tr>
                                    <?php }?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <?php }?>
            </div>


        </div>
    </div>
</div>
</div>
</div>


        <div id="fade"></div>
        <div id="modal"  class="modal">
            <img id="loader" src="<?php echo base_url(); ?>clear/assets/img/lg.rotating-balls-spinner.gif" />
        </div>
        <script type="text/javascript">
        $( "#addpayment" ).click(function() {
        $('#addcreditblock').hide("slow");
        $('#addpaymentblock').show("slow");
        });
        $( "#addpaymenthide" ).click(function() {
        $('#addpaymentblock').hide("slow");
        });
        $( "#addcredit" ).click(function() {
        $('#addpaymentblock').hide("slow");
        $('#addcreditblock').show("slow");
        });
        $( "#addcredithide" ).click(function() {
        $('#addcreditblock').hide("slow");
        });
        </script>
        <script type="text/javascript">
        function confirmation_send() {
        $("#sendinvoice").prop('disabled', true);
        var answer = confirm("<?php echo lang('Do you wish to send this invoice to customer Email'); ?>?")
        if (answer){
        var invoiceid = $("#invoiceid").val();
        var invoicenum = $("#invoicenum").val();
        openModal();
        $.ajax({
        url: window.location.protocol + '//' + window.location.host + '/admin/proforma/send',
        type: 'post',
        dataType: 'json',
        data: {
        invoiceid:invoiceid,
        invoicenum:invoicenum
        },
        success: function (data) {
        console.log(data);
        if(data.result){
        window.location.replace(window.location.protocol + "//" + window.location.host + "/admin/proforma/detail/"+invoiceid+"/success_send");
        }
        closeModal();
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
        alert("<?php echo lang('Error  accour while sending your email'); ?>");
        closeModal();
        }
        });
        }
        else{
        console.log("hello ");
        $("#sendinvoice").prop('disabled', false);
        }
        }


        function confirmation_send_reminder() {
        $("#sendinvoice").prop('disabled', true);
        var answer = confirm("<?php echo lang('Do you wish to send a reminder invoice to customer Email'); ?>?")
        if (answer){
        var invoiceid = $("#invoiceid").val();
        openModal();
        $.ajax({
        url: window.location.protocol + '//' + window.location.host + '/admin/proforma/sendreminder',
        type: 'post',
        dataType: 'json',
        data: {
        invoiceid:invoiceid,
        days:5
        },
        success: function (data) {
        console.log(data);
        if(data.result){
        window.location.replace(window.location.protocol + "//" + window.location.host + "/admin/proforma/detail/"+invoiceid+"/success_send");
        }
        closeModal();
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
        alert("<?php echo lang('Error  accour while sending your email'); ?>");
        closeModal();
        }
        });
        }
        else{
        console.log("hello ");
        $("#sendinvoice").prop('disabled', false);
        }
        }


        function openModal() {
        document.getElementById('modal').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        }
        function closeModal() {
        document.getElementById('modal').style.display = 'none';
        document.getElementById('fade').style.display = 'none';
        }
        </script>