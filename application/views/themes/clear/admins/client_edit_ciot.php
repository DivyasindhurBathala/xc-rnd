<?php //echo "has :".print_r($hasinvoice);?>
<div class="content-i">
  <div class="content-box">
    <div class="element-wrapper">
      <h6 class="element-header">
        <?php echo lang('Client Detail'); ?>
      </h6>
      <div class="element-box">
        <h5 class="form-header">
          <?php echo lang('Update CIOT Information'); ?>
        </h5>
        <div class="table-responsive">
          <form method="post" id="formdata" action="<?php echo base_url(); ?>admin/client/edit/<?php echo $client->id; ?>">
            <input type="hidden" name="id" value="<?php echo $client->id; ?>">
            <div class="row">
              <div class="col-sm-4">
                <div class="form-group">
                  <fieldset>
                    <label class="control-label" for="vat">
                      <?php echo lang('Relation ID'); ?><span class="text-danger">*</span></label>
                    <input name="mvno_id" class="form-control" id="deltaid" type="text" placeholder="<?php echo lang('Delta Customerid'); ?>"
                      value="<?php echo $client->mvno_id; ?>" required<?php if ($client->companyid == 54) {?> readonly<?php }?>>
                  </fieldset>
                </div>
              </div>
              <div class="col-sm-4">
                <div class="form-group">
                  <fieldset>
                    <label class="control-label" for="vat">
                      <?php echo lang('VAT'); ?></label>
                    <input name="vat" class="form-control ui-autocomplete-input ui-autocomplete-loading" id="VAT" type="text" placeholder="<?php echo lang('Vat Number'); ?>" value="<?php echo $client->vat; ?>" <?php if ($hasinvoice == "yes") {?> disabled<?php }?>>
                    <span id="loading_data_icon"></span>
                  </fieldset>
                </div>
              </div>
              <div class="col-sm-4">
                <div class="form-group">
                  <fieldset>
                    <label class="control-label" for="companyname">
                      <?php echo lang('Companyname'); ?></label>
                    <input name="companyname" class="form-control ui-autocomplete-input ui-autocomplete-loading" id="companyname"
                      type="text" placeholder="<?php echo lang('Companyname'); ?>" value="<?php echo $client->companyname; ?>">
                    <span id="loading_data_icon"></span>
                  </fieldset>
                </div>
              </div>
            </div>
            <div class="row">
            <div class="col-md-4">
                  <div class="form-group">
                    <label for=""><?php echo lang('Agent/Reseller'); ?> <a href="<?php echo base_url(); ?>admin/agent">(Show/Add agents)</a></label>
                    <select class="form-control" name="agentid" id="agentid">
                    <option value="0">None</option>
                    <?php foreach (get_agents($this->session->cid) as $a) {?>
                      <option value="<?php echo $a->id; ?>"<?php if ($a->id == $client->agentid) {?> selected<?php }?>><?php echo $a->agent; ?></option>
                      <?php }?>
                    </select>
                  </div>
                </div>
              <div class="col-sm-4">
                <div class="form-group">
                  <label for="">
                    <?php echo lang('Initial'); ?></label><input class="form-control" placeholder="<?php echo lang('Initial'); ?>"
                    name="initial" type="text" id="Initial" value="<?php echo $client->initial; ?>">
                </div>
              </div>
              <div class="col-sm-4">
                <div class="form-group">
                  <label for="">
                    <?php echo lang('Salutation'); ?></label><input class="form-control" placeholder="<?php echo lang('Salutation'); ?>"
                    name="salutation" type="text" id="Salutation" value="<?php echo $client->salutation; ?>">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-4">
                <div class="form-group">
                  <fieldset>
                    <label class="control-label" for="firstname">
                      <?php echo lang('Firstname'); ?><span class="text-danger">*</span></label>
                    <input name="firstname" class="form-control" id="firstname" type="text" placeholder="<?php echo lang('Firstname'); ?>"
                      value="<?php echo $client->firstname; ?>" required>
                  </fieldset>
                </div>
              </div>
              <div class="col-sm-4">
                <div class="form-group">
                  <fieldset>
                    <label class="control-label" for="lastname">
                      <?php echo lang('Lastname'); ?><span class="text-danger">*</span></label>
                    <input name="lastname" class="form-control" id="lastname" type="text" placeholder="<?php echo lang('Lastname'); ?>"
                      value="<?php echo $client->lastname; ?>" required>
                  </fieldset>
                </div>
              </div>
              <div class="col-sm-4">
                <div class="form-group">
                  <fieldset>
                    <label class="control-label" for="gender">
                      <?php echo lang('Gender'); ?><span class="text-danger">*</span></label>
                    <select class="form-control" id="gender" name="gender">
                      <option value="male" <?php if ($client->gender == "male") {?> selected
                        <?php }?>>Male</option>
                      <option value="female" <?php if ($client->gender == "female") {?> selected
                        <?php }?>>Female</option>
                    </select>
                  </fieldset>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-4">
                <div class="form-group">
                  <fieldset>
                    <label class="control-label" for="address1">
                      <?php echo lang('Address'); ?><span class="text-danger">*</span></label>
                    <input name="address1" class="form-control" id="address1" type="text" placeholder="<?php echo lang('Address'); ?>"
                      value="<?php echo $client->address1; ?>" required>
                  </fieldset>
                </div>
              </div>

                <div class="col-sm-4">
                <div class="form-group">
                  <fieldset>
                    <label class="control-label" for="housenumber">
                      <?php echo lang('Housenumber'); ?><span class="text-danger">*</span></label>
                    <input name="housenumber" class="form-control" id="housenumber" type="text" placeholder="<?php echo lang('Address'); ?>"
                      value="<?php echo $client->housenumber; ?>" required>
                  </fieldset>
                </div>
              </div>
                  <div class="col-sm-4">
                <div class="form-group">
                  <fieldset>
                    <label class="control-label" for="alphabet">
                      <?php echo lang('Alphabet'); ?><span class="text-danger"></span></label>
                    <input name="alphabet" class="form-control" id="alphabet" type="text" placeholder="<?php echo lang('Address'); ?>"
                      value="<?php echo $client->alphabet; ?>">
                  </fieldset>
                </div>
              </div>
            </div>
             <div class="row">
              <div class="col-sm-4">
                <div class="form-group">
                  <fieldset>
                    <label class="control-label" for="Postcode">
                      <?php echo lang('Postcode'); ?><span class="text-danger">*</span></label>
                    <input name="postcode" class="form-control" id="postcode" type="text" placeholder="<?php echo lang('Postcode'); ?>"
                      value="<?php echo $client->postcode; ?>" required>
                  </fieldset>
                </div>
              </div>
              <div class="col-sm-4">
                <div class="form-group">
                  <fieldset>
                    <label class="control-label" for="City">
                      <?php echo lang('City'); ?><span class="text-danger">*</span></label>
                    <input name="city" class="form-control" id="city" type="text" placeholder="<?php echo lang('City'); ?>"
                      value="<?php echo $client->city; ?>" required>
                  </fieldset>
                </div>
              </div>
              <div class="col-sm-4">
                <div class="form-group">
                  <fieldset>
                    <label for="exampleSelect1">
                      <?php echo lang('Country'); ?><span class="text-danger">*</span></label>
                    <select class="form-control" id="country" name="country">
                      <?php foreach (getCountries() as $key => $country) {?>
                      <option value="<?php echo $key; ?>" <?php if ($key == trim($client->country)) {?> selected
                        <?php }?>>
                        <?php echo $country; ?>
                      </option>
                      <?php }?>
                    </select>
                  </fieldset>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-3">
                <div class="form-group">
                  <fieldset>
                    <label for="exampleSelect1">
                      <?php echo lang('Languages'); ?> (
                      <?php echo lang('Portal'); ?>)</label>
                    <select class="form-control" id="language" name="language">
                      <?php foreach (getLanguages() as $key => $lang) {?>
                      <option value="<?php echo $key; ?>" <?php if ($key == trim($client->language)) {?> selected
                        <?php }?>>
                        <?php echo lang($lang); ?>
                      </option>
                      <?php }?>
                    </select>
                  </fieldset>
                </div>
              </div>
              <div class="col-sm-3">
                <div class="form-group">
                  <fieldset>
                    <label class="control-label" for="Phonenumber">
                      <?php echo lang('Contact Number'); ?><span class="text-danger">*</span></label>
                    <input name="phonenumber" class="form-control" id="phonenumber" type="number" placeholder="<?php echo lang('Phonenumber'); ?>"
                      value="<?php echo preg_replace('/\D/', '', $client->phonenumber); ?>" required>
                  </fieldset>
                </div>
              </div>
              <div class="col-sm-3">
                <div class="form-group">
                  <fieldset>
                    <label class="control-label" for="Email">
                      <?php echo lang('Email'); ?><span class="text-danger">*</span></label>
                    <input name="email" class="form-control" id="email" type="email" placeholder="<?php echo lang('Email address'); ?>"
                      value="<?php echo $client->email; ?>" required>
                  </fieldset>
                </div>
              </div>
              <div class="col-sm-3">
                <div class="form-group">
                  <fieldset>
                    <label class="control-label" for="NationalNr">
                      <?php echo lang('NationalNr'); ?></label>
                    <input name="nationalnr" class="form-control" id="NationalNr" type="text" placeholder="<?php echo lang('NationalNr'); ?>"
                      value="<?php echo $client->nationalnr; ?>">
                  </fieldset>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-2">
                <div class="form-group">
                  <fieldset>
                    <label for="exampleSelect1">
                      <?php echo lang('Paymentmethod'); ?></label>
                    <select class="form-control" id="paymentmethod" name="paymentmethod">
                      <option value="banktransfer" <?php if ($client->paymentmethod == "banktransfer") {?> selected
                        <?php }?>>
                        <?php echo lang('Bank transfer'); ?>
                      </option>
                      <option value="creditcard" <?php if ($client->paymentmethod == "creditcard") {?> selected
                        <?php }?>>
                        <?php echo lang('Credit Card'); ?>
                      </option>
                      <option value="directdebit" <?php if ($client->paymentmethod == "directdebit") {?> selected
                        <?php }?>>
                        <?php echo lang('Directdebit'); ?>
                      </option>
                      <option value="others" <?php if ($client->paymentmethod == "others") {?> selected
                        <?php }?>>
                        <?php echo lang('Others'); ?>
                      </option>
                    </select>

                  </fieldset>
                </div>
              </div>

              <div class="col-sm-2">
                <div class="form-group">
                  <fieldset>
                    <label>Payment Terms</label>
                    <input type="text" class="form-control" name="payment_duedays" id="payment_duedays" value="<?php echo $client->payment_duedays; ?>">

                  </fieldset>
                </div>
              </div>
              <div class="col-sm-2">
                <div class="form-group">
                  <fieldset>
                    <label for="exampleSelect1">
                      <?php echo lang('VAT Rates'); ?></label>
                    <select class="form-control" id="vat_rate" name="vat_rate">
                      <?php foreach (array('21', '6', '0') as $rate) {?>
                      <option value="<?php echo $rate; ?>" <?php if ($client->vat_rate == $rate) {?> selected
                        <?php }?>>
                        <?php echo $rate; ?>
                      </option>
                      <?php }?>
                    </select>

                  </fieldset>
                </div>
              </div>
              <div class="col-sm-2">
                <div class="form-group">
                  <fieldset>
                    <label for="EInvoice">
                      <?php echo lang('E-Invoice'); ?></label>
                    <select class="form-control" id="invoice_email" name="invoice_email">
                      <option value="yes" <?php if ($client->invoice_email == "yes") {?> selected
                        <?php }?>>
                        <?php echo lang('Yes'); ?>
                      </option>
                      <option value="no" <?php if ($client->invoice_email == "no") {?> selected
                        <?php }?>>
                        <?php echo lang('No'); ?>
                      </option>
                    </select>
                    <small>
                      <?php echo lang('choose yes if wish to recieved email invoices'); ?></small>
                  </fieldset>
                </div>
              </div>
              <div class="col-sm-2">
                <div class="form-group">
                  <fieldset>
                    <label for="exampleSelect1">
                      <?php echo lang('Password'); ?></label>
                    <input name="password2" class="form-control" id="NewPassword" type="password" placeholder="<?php echo lang('Leave empty for no change'); ?>"
                      value="" autocomplete="new-password">
                  </fieldset>
                </div>
              </div>
              <div class="col-sm-2">
                <div class="form-group">
                  <fieldset>
                    <label for="">
                      <?php echo lang('Date of Birth'); ?></label>
                    <input class="form-control" type="text" name="date_birth" id="pickdate" value="<?php echo $client->date_birth; ?>">
                  </fieldset>
                </div>
              </div>
            </div>
     
            <div class="row">
              <div class="col-sm-12">
                <div class="form-group">
                  <button id="disable" type="submit" class="mr-2 mb-2 btn btn-primary btn-lg btn-block" onclick="disable_button();"><i
                      id="spin" class="fa fa-save"></i>
                    <?php echo lang('Save Client Information'); ?></button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  function disable_button() {
    $('#disable').prop('disabled', true);
    $("#spin").removeClass("fa fa-save").addClass("fa fa-cog fa-spin");
    var form_data = $("#formdata").serializeArray();
    $.ajax({
      url: window.location.protocol + '//' + window.location.host + '/admin/client/edit_ciot',
      type: 'post',
      dataType: "json",
      data: form_data,
      success: function (data) {
        console.log(data);
        if (data.result) {
          window.location.replace(window.location.protocol + '//' + window.location.host + '/admin/client/detail/<?php echo $client->id; ?>');
        } else {
          alert("There was an error: " + data.message);
          $('#disable').prop('disabled', false);
          $("#spin").removeClass("fa fa-cog fa-spin").addClass("fas fa-save");

        }
      }
    });

  }
</script>