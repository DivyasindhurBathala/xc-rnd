

<div class="content-i">
    <div class="content-box">
        <div class="element-wrapper">
            <h6 class="element-header">
            <?php echo lang('Setting Raw'); ?><div class="close">
                <button class="btn btn-primary btn-sm"  type="button" data-toggle="modal" data-target="#KeyModal"><i class="fa fa-plus-circle"></i> <?php echo lang('New Token'); ?></button>
            </div>
            </h6>
            <div class="element-box">
                <div class="table-responsive">
             
                <?php foreach ($st as $key => $value) {
    ?>
<?php if (!in_array($key, array('footer_link','last_reminder_run_data'))) {
        ?>
<div class="form-group">
<label class="label-form"><?php echo $key; ?></label>

<input type="text" class="form-control" name="<?php echo $key; ?>" value="<?php echo $value; ?>">
</div>
<?php
    } ?>
<?php
} ?>

</div>
</div>
</div>
</div>
</div>

<div class="modal fade" id="KeyModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form method="post"
                action="<?php echo base_url(); ?>master/setting/create_configuration">
                <div class="modal-header">
                    <h4 class="modal-title"><?php echo lang('Add Setting Company'); ?>
                    </h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for=""><?php echo lang('Name'); ?></label>
                                <input class="form-control" autocomplete="new-username" type="text" name="name"
                                    required>
                            </div>

                            <div class="form-group">
                                <label for=""><?php echo lang('Default Value'); ?></label>
                                <input class="form-control" autocomplete="new-username" type="text" name="value"
                                    >
                            </div>
                       
                        </div>
                    </div>
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary"><?php echo lang('Submit'); ?></button>
                </div>
            </form>
        </div>
    </div>
</div>



