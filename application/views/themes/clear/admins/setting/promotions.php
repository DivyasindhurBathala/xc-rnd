<div class="content-i">
  <div class="content-box">
    <div class="element-wrapper">
      <h6 class="element-header">
      Promotions / Discounts
      </h6>
      <div class="element-box">
        <h5 class="form-header">
        <?php echo lang('Promotion lists'); ?> <div class="close"> <a class="btn btn-primary btn-sm" href="javascript::void(0)" onclick="add_promotion()"><i class="fa fa-plus-circle"></i> <?php echo lang('Add New Promotions'); ?></a></div>
        </h5>
        <div class="table-responsive">
          <table class="table table-striped table-lightfont" id="promo">
            <thead>
              <tr>
                <th><?php echo lang('Code'); ?></th>
                <th><?php echo lang('Description'); ?></th>
                <th><?php echo lang('Duration'); ?> <?php echo lang('months'); ?> </th>
                <th><?php echo lang('Value'); ?></th>
                <th><?php echo lang('Available From'); ?></th>
                <th><?php echo lang('Available Until'); ?></th>
                <th><?php echo lang('date Created'); ?></th>
               <th><?php echo lang('Updatded by'); ?></th>
               <th><?php echo lang('Delete'); ?></th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  $( document ).ready(function() {
  $.getJSON(window.location.protocol + '//' + window.location.host + '/admin/complete/get_lang', function(data) {
$('#promo').DataTable({
"autoWidth": false,
"processing": true,
"orderCellsTop": true,
"ordering": true,
"serverSide": true,
"colReorder": true,
"pageLength": 10,
"ajax": window.location.protocol + '//' + window.location.host + '/admin/table/getpromotionscode',
"aaSorting": [[1, 'desc']],
"language": {
"url": window.location.protocol + '//' + window.location.host +"/assets/clear/js/datatables/lang/" + data.result + ".json"
},
"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
$('td:eq(0)', nRow).html('<a href="#" onclick="EditPromotion('+aData[8]+')">'+aData[0]+'</a>');
$('td:eq(8)', nRow).html(' <button type="button" class="btn btn-xs btn-danger" onclick="DeletePromotion('+aData[8]+')"><i class="fa fa-trash"></i></button>');
return nRow;
}
});
});
});
</script>

<div class="modal fade" id="add_promotion">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

        <form method="post" action="<?php echo base_url(); ?>admin/setting/add_promotion">
        <div class="modal-header">
          <h4 class="modal-title">
            <?php echo lang('Create New Promotions/  Discounts'); ?>
          </h4>

        </div>

        <!-- Modal body -->
        <div class="modal-body" id="promox">
          <div class="form-group">
          <label class="label-control"><?php echo lang('PROMO NAME'); ?> (<?php echo lang('Appear in the Invoice'); ?>)</label>
            <input type="text" name="promo_name" class="form-control" placeholder="Promotion for student" required>
          </div>
           <div class="form-group">
          <label class="label-control"><?php echo lang('PROMO CODE'); ?></label>
            <input type="text" name="promo_code" class="form-control" placeholder="CODE123532" required>
          </div>

           <div class="form-group">
          <label class="label-control"><?php echo lang('PROMO Description'); ?></label>
            <input type="text" name="promo_description" class="form-control" placeholder="Little explaination of this promotion" required>
          </div>

            <div class="form-group">
          <label class="label-control"><?php echo lang('PROMO Duration'); ?> (months)</label>
            <input type="number" name="promo_duration" class="form-control" required>
            <span class="text-help"><?php echo lang('How many Months the Discount reoccurring if unlimited then insert 600'); ?></span>
          </div>

             <div class="form-group">
          <label class="label-control"><?php echo lang('PROMO Value'); ?> (€)</label>
            <input type="number" step="any" name="promo_value" class="form-control" placeholder="10.00" required>
          </div>

             <div class="form-group">
          <label class="label-control"><?php echo lang('Available From'); ?></label>
            <input type="text" name="date_valid" class="form-control" id="pickdate1" required>
          </div>

           <div class="form-group">
          <label class="label-control"><?php echo lang('Available Until'); ?></label>
            <input type="text" name="date_expired" class="form-control" id="pickdate2" required>
          </div>


        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary"><?php echo lang('Insert'); ?></button>

        </div>
      </form>

    </div>
  </div>
</div>


<div class="modal fade" id="edit_promotion">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

        <form method="post" action="<?php echo base_url(); ?>admin/setting/edit_promotion">
        <div class="modal-header">
          <h4 class="modal-title">
            <?php echo lang('Update Promotions/  Discounts'); ?>
          </h4>

        </div>

        <!-- Modal body -->
        <div class="modal-body" id="promoxin">
          <div class="form-group">
          <label class="label-control"><?php echo lang('PROMO NAME'); ?> (<?php echo lang('Appear in the Invoice'); ?>)</label>
            <input type="text"  id="promo_name1"  name="promo_name" class="form-control" placeholder="Promotion for student" required>
          </div>
           <div class="form-group">
          <label class="label-control"><?php echo lang('PROMO CODE'); ?></label>
            <input type="text"  id="promo_code1" name="promo_code" class="form-control" placeholder="CODE123532" required>
          </div>

           <div class="form-group">
          <label class="label-control"><?php echo lang('PROMO Description'); ?></label>
            <input type="text"  id="promo_description1" name="promo_description" class="form-control" placeholder="Little explaination of this promotion" required>
          </div>

            <div class="form-group">
          <label class="label-control"><?php echo lang('PROMO Duration'); ?> (months)</label>
            <input type="number"  id="promo_duration1" name="promo_duration" class="form-control" required>
            <span class="text-help"><?php echo lang('How many Months the Discount reoccurring if unlimited then insert 600'); ?></span>
          </div>

             <div class="form-group">
          <label class="label-control"><?php echo lang('PROMO Value'); ?> (€)</label>
            <input type="number"  id="promo_value1"  step="any" name="promo_value" class="form-control" placeholder="10.00" required>
          </div>

             <div class="form-group">
          <label class="label-control"><?php echo lang('Available From'); ?></label>
            <input type="text"  id="date_valid1"  name="date_valid" class="form-control" id="pickdate1" required>
          </div>

           <div class="form-group">
          <label class="label-control"><?php echo lang('Available Until'); ?></label>
            <input type="text" id="date_expired1"  name="date_expired" class="form-control" id="pickdate2" required>
          </div>


        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary"><?php echo lang('Update'); ?></button>

        </div>
        <input type="hidden" name="id" value="" id="idx1">
      </form>

    </div>
  </div>
</div>

<div class="modal fade" id="delete_promotion">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

        <form method="post" action="<?php echo base_url(); ?>admin/setting/delete_promotion">
        <div class="modal-header">
          <h4 class="modal-title">
            <?php echo lang('Are you sure to delete this promotion?'); ?>
          </h4>

        </div>

        <!-- Modal body -->
        <div class="modal-body" id="promoxon">
          <div class="form-group">
          <label class="label-control"><?php echo lang('PROMO NAME'); ?></label>
            <input type="text"  id="promo_name" name="promo_name" class="form-control" placeholder="Promotion for student" disabled>
          </div>
           <div class="form-group">
          <label class="label-control"><?php echo lang('PROMO CODE'); ?></label>
            <input type="text"  id="promo_code" name="promo_code" class="form-control" placeholder="CODE123532" disabled>
          </div>

           <div class="form-group">
          <label class="label-control"><?php echo lang('PROMO Description'); ?></label>
            <input type="text"  id="promo_description" name="promo_description" class="form-control" placeholder="Little explaination of this promotion" disabled>
          </div>

            <div class="form-group">
          <label class="label-control"><?php echo lang('PROMO Duration'); ?> (months)</label>
            <input type="number"  id="promo_duration" name="promo_duration" class="form-control" disabled>
            <span class="text-help"><?php echo lang('How many Months the Discount reoccurring if unlimited then insert 600'); ?></span>
          </div>

             <div class="form-group">
          <label class="label-control"><?php echo lang('PROMO Value'); ?> (€)</label>
            <input type="number"  id="promo_value"  step="any" name="promo_value" class="form-control" placeholder="10.00" disabled>
          </div>

             <div class="form-group">
          <label class="label-control"><?php echo lang('Available From'); ?></label>
            <input type="text"  id="date_valid"  name="date_valid" class="form-control" id="pickdate1" disabled>
          </div>

           <div class="form-group">
          <label class="label-control"><?php echo lang('Available Until'); ?></label>
            <input type="text"  id="date_expired"  name="date_expired" class="form-control" id="pickdate2" disabled>
          </div>


        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary"><?php echo lang('Remove'); ?></button>

        </div>
        <input type="hidden" name="id" value="" id="idx">
      </form>

    </div>
  </div>
</div>
<script>
  function add_promotion(){
    $('#add_promotion').modal('toggle');
  }
  function EditPromotion(id){
     $('#idx1').val(id);

     $.ajax({
        url:window.location.protocol + '//' + window.location.host + '/admin/setting/get_promotion/'+id,
        type: 'get',
        dataType: 'json',
        success: function (data)
             {
              console.log(data.promo_name);
              $('#promo_name1').val(data.promo_name);
              $('#promo_code1').val(data.promo_code);
              $('#promo_description1').val(data.promo_description);
              $('#promo_duration1').val(data.promo_duration);
              $('#promo_value1').val(data.promo_value);
              $('#date_valid1').val(data.date_valid);
              $('#date_expired1').val(data.date_expired);
              $('#edit_promotion').modal('toggle');
            }
     }); 


     
  }
   function DeletePromotion(id){
     $('#idx').val(id);
      $.ajax({
        url:window.location.protocol + '//' + window.location.host + '/admin/setting/get_promotion/'+id,
        type: 'get',
        dataType: 'json',
        success: function (data)
             {
               console.log(data);
              $('#promo_name').val(data.promo_name);
              $('#promo_code').val(data.promo_code);
              $('#promo_description').val(data.promo_description);
              $('#promo_duration').val(data.promo_duration);
              $('#promo_value').val(data.promo_value);
              $('#date_valid').val(data.date_valid);
              $('#date_expired').val(data.date_expired);
              $('#delete_promotion').modal('toggle');
            }
     }); 

  }
  </script>