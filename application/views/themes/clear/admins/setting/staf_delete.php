<div class="content-i">
  <div class="content-box">
    <div class="element-wrapper">
      <h6 class="element-header">
      <?php echo lang('Setting'); ?>
      </h6>
      <div class="element-box">
        <h5 class="form-header">
        <?php echo lang('Delete Staf'); ?> <div class="close"> </div>
        </h5>
        <div class="table-responsive">
          <form method="post" action="<?php echo base_url(); ?>admin/setting/staf_delete">
            <input type="hidden" name="id" value="<?php echo $staf->id; ?>">
         <center><h2><?php echo lang('Are you sure to delete Staf'); ?>: <?php echo $staf->firstname . ' ' . $staf->lastname; ?></h2>
          <button class="btn btn-primary btn-md" type="submit"><?php echo lang('Yes I am Sure'); ?></button></center>
        </form>
        </div>
      </div>
    </div>
  </div>
</div>