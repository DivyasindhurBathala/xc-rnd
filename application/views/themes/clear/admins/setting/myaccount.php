<div class="content-i">
	<div class="content-box">
		<div class="element-wrapper">
			<h6 class="element-header">
			<?php echo lang('Setting'); ?>
			</h6>
			<div class="element-box">
				<h5 class="form-header">
				<?php echo lang('My Account'); ?>
				<?php if (!has2f_authentication($this->session->id)) {
    ?>
					<div class="float-right"><button class="btn btn-primary" id="modal2fa"><img src="<?php echo base_url(); ?>assets/img/Google-Authenticator-icon.png" height="20"> <?php echo lang("Activate 2 Factor Authentication"); ?></button></div>
				
				<?php
} else {
        ?>
<div class="float-right"><button class="btn btn-primary" id="modal2fa_disable"><img src="<?php echo base_url(); ?>assets/img/Google-Authenticator-icon.png" height="20"> <?php echo lang("Disable 2 Factor Authentication"); ?></button></div>
				

<?php
    } ?>
				</h5>
				<div class="table-responsive">
					<form method="post" action="<?php echo base_url(); ?>admin/dashboard/myaccount" enctype="multipart/form-data">
						<input type="hidden" name="id" value="<?php echo $staf->id; ?>">
						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									<fieldset>
										<label class="control-label" for="firstname"><?php echo lang('Firstname'); ?></label>
										<input  name="firstname" class="form-control" id="firstname" type="text" placeholder="Firstname"  value="<?php echo $staf->firstname; ?>" required>
									</fieldset>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<fieldset>
										<label class="control-label" for="lastname"><?php echo lang('Lastname'); ?></label>
										<input  name="lastname" class="form-control" id="lastname" type="text" placeholder="Lastname"  value="<?php echo $staf->lastname; ?>" required>
									</fieldset>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-4">
								<div class="form-group">
									<fieldset>
										<label class="control-label" for="address1"><?php echo lang('Address'); ?></label>
										<input  name="address1" class="form-control" id="address1" type="text" placeholder="Address"  value="<?php echo $staf->address1; ?>" required>
									</fieldset>
								</div>
							</div>
							<div class="col-sm-2">
								<div class="form-group">
									<fieldset>
										<label class="control-label" for="Postcode"><?php echo lang('Postcode'); ?></label>
										<input  name="postcode" class="form-control" id="postcode" type="text" placeholder="Postcode"  value="<?php echo $staf->postcode; ?>" required>
									</fieldset>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="form-group">
									<fieldset>
										<label class="control-label" for="City"><?php echo lang('City'); ?></label>
										<input  name="city" class="form-control" id="city" type="text" placeholder="City"  value="<?php echo $staf->city; ?>" required>
									</fieldset>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="form-group">
									<fieldset>
										<label for="exampleSelect1"><?php echo lang('Country'); ?></label>
										<select class="form-control" id="country"  name="country" >
											<?php foreach (getCountries() as $key => $country) {
        ?>
											<option value="<?php echo $key; ?>"<?php if ($key == $this->session->country) {
            ?> selected<?php
        } ?>><?php echo $country; ?></option>
											<?php
    }?>
										</select>
									</fieldset>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-3">
								<div class="form-group">
									<fieldset>
										<label for="exampleSelect1"><?php echo lang('Languages'); ?></label>
										<select class="form-control" id="language" name="language" >
											<?php foreach (getLanguages() as $key => $lang) {
        ?>
											<option value="<?php echo $key; ?>"<?php if ($key == $this->session->language) {
            ?> selected<?php
        } ?>><?php echo $lang; ?></option>
											<?php
    }?>
										</select>
									</fieldset>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="form-group">
									<fieldset>
										<label class="control-label" for="Phonenumber"><?php echo lang('Phonenumber'); ?></label>
										<input name="phonenumber" class="form-control" id="phonenumber" type="number" placeholder="Phonenumber"  value="<?php echo $staf->phonenumber; ?>" required>
									</fieldset>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="form-group">
									<fieldset>
										<label class="control-label" for="Email"><?php echo lang('Email'); ?></label>
										<input name="email"  class="form-control" id="email" type="email" placeholder="Email address"  value="<?php echo $staf->email; ?>" required>
									</fieldset>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="form-group">
									<fieldset>
										<label class="control-label" for="NationalNr"><?php echo lang('NationalNr'); ?>.</label>
										<input name="nationalnr"  class="form-control" id="NationalNr" type="number" placeholder="NationalNr" value="<?php echo $staf->nationalnr; ?>">
									</fieldset>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-3">
								<div class="form-group">
									<fieldset>
										<label class="control-label" for="file">Picture</label>
										<input name="picture"  class="form-control" type="file">
									</fieldset>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="avatar"><img src="<?php echo base_url(); ?>assets/img/staf/<?php echo $staf->picture; ?>" class="img-circle" height="70" style="padding-bottom: 10px;"></div>
							</div>
							<div class="col-sm-3">
								<div class="form-group">
									<fieldset>
										<label class="control-label" for="Password"><?php echo lang('Password'); ?>.</label>
										<input name="password"  class="form-control" id="password" type="password" autocomplete="new-password" placeholder="<?php echo lang('Leave empty for no change'); ?>">
									</fieldset>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									<label class="control-label" for="EmailNotifications"><strong><?php echo lang('Email Subscription'); ?></strong></label>
									<div class="form-check">
										<label class="form-check-label">
										<input type="checkbox" class="form-check-input" name="email_notification" id="customCheck1" value="1"<?php if ($staf->email_notification == 1) {
        ?> checked=""<?php
    }?>>
										<?php echo lang('Received email notification on successfull authentication'); ?>
									</label>
									</div>
									<div class="form-check">
										<label class="form-check-label">
										<input type="checkbox" class="form-check-input" name="order_notification" id="customCheck2" value="1" <?php if ($staf->order_notification == 1) {
        ?> checked=""<?php
    }?> >
										<?php echo lang('Received email notification on new orders'); ?>
									</label>
									</div>
								</div>
							</div>
						</div>
						<?php if ($setting->whmcs_ticket) {
        ?>
						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									<label class="control-label" for="SupportHelpdesk"><strong><?php echo lang('Subscribe to Helpdesk Departments'); ?></strong></label>

									<?php foreach (getDepartments($this->session->cid) as $row) {
            ?>

									<div class="form-check">
										<label class="form-check-label">
											<input class="form-check-input" name="ticket_notification[]" type="checkbox" value="<?php echo $row['id']; ?>" <?php if (!empty($staf->ticket_notification)) {
                if (in_array($row['id'], json_decode($staf->ticket_notification))) {
                    ?> checked=""<?php
                }
            } ?>>
											<?php echo $row['name']; ?>
										</label>
									</div>
									<?php
        } ?>
								</div>
							</div>
						</div>
						<?php
    }?>
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									<button type="submit" class="btn btn-lg btn-primary btn-block"><i class="fa fa-save"></i>  <?php echo lang('Save Changes'); ?></button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
$( document ).ready(function() {
$('#modal2fa').click(function(){
	$('#2FA').modal('toggle');
});
$('#modal2fa_disable').click(function(){

	$('#2FADISABLE').modal('toggle');
});

$('#submit_2fa').click(function(){
	var code = $('#code').val();
	$.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>admin/dashboard/google_2fa_auth",
            dataType: "json",
            data: {code:code},
            success : function(data){
				if (data.result){
					$('#modal2fa').hide();
					$('#form_hide').show();
					$('#form_show').hide();
					$('#submit_2fa').hide();
					$('#secret').html(data.secret);
                } else{
					alert("Error: " +data.message);
				}
				//window.location.replace("<?php echo base_url(); ?>admin/dashboard/myaccount");
            }
        });

});
$('#disable_2fa').click(function(){
	var code = $('#code_disable').val();
	$.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>admin/dashboard/disable_google_2fa_auth",
            dataType: "json",
            data: {code:code},
            success : function(data){
				$('#2FADISABLE').modal('toggle');
                if (data.result){
				  
					alert("Success: " +data.message);
                } else{

					alert("Error: " +data.message);
				}

				window.location.replace("<?php echo base_url(); ?>admin/dashboard/myaccount");
            }
        });

});
});
</script>
<div class="modal" id="2FA">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><?php echo lang('Enable 2 Factor Autentication'); ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
	<form id="auth">
    <div class="modal-body" id="form_show">
	<div class="row">
	<div class="col-md-6">
	<img class="img-thumbnail" src="<?php echo $qrCodeUrl; ?>">
	</div>
	<div class="col-md-6">
	<a href="https://apps.apple.com/fr/app/google-authenticator/id388497605" target="_blank"><img src="<?php echo base_url(); ?>assets/img/play-store.png" height="70"></a> <a href="https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2&hl=en" target="_blank"><img src="<?php echo base_url(); ?>assets/img/app-store.png" height="70"> </a>
       
	</div>
	</div>
	<div class="form-group">
	<label class="label-form"><?php echo lang('Please Enter Code:'); ?></label>
	<input type="number" class="form-control input-lg" name="code" id="code">
	</div>
    </div>
	<div class="modal-body" id="form_hide" style="display:none;">
	<center><h1 id="secret" class="text-danger"></h1></center>
	<br >
	<center>Please Keep this Backup Code somewhere safe, when you need to recover your account incase you lost your device</center>
    </div>

      <div class="modal-footer">
        <button type="button" id="submit_2fa" class="btn btn-primary"><?php echo lang('Submit'); ?></button>
      </div>
	  </form>

    </div>
  </div>
</div>



<div class="modal" id="2FADISABLE">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><?php echo lang('Disabling 2F Autentication'); ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
	  <form id="auth">
      <div class="modal-body">
	 
	<div class="form-group">
	<label class="label-form"><?php echo lang('Please Enter Code From Your Device:'); ?></label>
	<input type="number" class="form-control input-lg" name="code" id="code_disable">
	</div>
      </div>
      <div class="modal-footer">
        <button type="button" id="disable_2fa" class="btn btn-primary"><?php echo lang('Submit'); ?></button>
      </div>
	  </form>

    </div>
  </div>
</div>
