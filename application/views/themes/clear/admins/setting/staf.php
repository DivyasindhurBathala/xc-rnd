<div class="content-i">
  <div class="content-box">
    <div class="element-wrapper">
      <h6 class="element-header">
      Setting
      </h6>
      <div class="element-box">
        <h5 class="form-header">
        <?php echo lang('List Staf'); ?> <div class="close"> <a class="btn btn-primary btn-sm" href="<?php echo base_url(); ?>admin/setting/staf_add"><i class="fa fa-plus-circle"></i> <?php echo lang('Add Staf'); ?></a></div>
        </h5>
        <div class="table-responsive">
          <table class="table table-striped table-lightfont" id="clients">
            <thead>
              <tr>
                <th><?php echo lang('Avatar'); ?></th>
                <th><?php echo lang('Adminname'); ?></th>
                <th><?php echo lang('Email'); ?></th>
                <th><?php echo lang('Status'); ?></th>
                <th><?php echo lang('Role'); ?></th>
                <th></th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>