<div class="content-i">
	<div class="content-box">
		<div class="element-wrapper">
			<h6 class="element-header">
			Setting
			</h6>
			<div class="element-box">
				<h5 class="form-header">
				<?php echo lang('Email Configuration'); ?>
				</h5>
				<div class="table-responsive">
					<form method="post" action="<?php echo base_url(); ?>admin/setting/email" class="form-horizontal">
						<fieldset>
							<div class="form-group row">
								<label for="exampleInputEmail1" class="col-sm-2"><?php echo lang('Sender Name'); ?></label>
								<div class="col-sm-10">
									<input name="smtp_name" type="text" class="form-control" value="<?php echo $setting->smtp_name; ?>" aria-describedby="emailHelp" placeholder="Enter Companyname">
									<small id="emailHelp" class="form-text text-muted"><?php echo lang('Name of sender'); ?></small>
								</div>
							</div>
							<div class="form-group row">
								<label for="exampleInputEmail1" class="col-sm-2"><?php echo lang('Sender Address'); ?></label>
								<div class="col-sm-10">
									<input name="smtp_sender" type="text" class="form-control" value="<?php echo $setting->smtp_sender; ?>" aria-describedby="emailHelp" placeholder="Enter Logo Url">
									<small  id="emailHelp" class="form-text text-muted">Url to your email address sender</small>
								</div>
							</div>
							<div class="form-group row">
								<label for="exampleSelect2"  class="col-sm-2"><?php echo lang('Email Type'); ?></label>
								<div class="col-sm-10">
									<select class="form-control" id="cred" name="smtp_type">
										<?php foreach (array('mail', 'smtp') as $t) {?>
										<option value="<?php echo $t; ?>"<?php if ($t == $setting->smtp_type) {?> selected<?php }?>><?php echo strtoupper($t); ?></option>
										<?php }?>
									</select>
								</div>
							</div>
							<div id="cre">
								<?php if ($setting->smtp_type == "smtp") {?>
								<div class="form-group row">
									<label for="exampleInputEmail1" class="col-sm-2"><?php echo lang('SMTP Username'); ?></label>
									<div class="col-sm-10">
										<input name="smtp_user" type="text" class="form-control" value="<?php echo $setting->smtp_user; ?>" aria-describedby="emailHelp" placeholder="Enter Logo Url">
										<small  id="emailHelp" class="form-text text-muted"><?php echo lang('Url to your email address sender'); ?></small>
									</div>
								</div>
								<div class="form-group row">
									<label for="exampleInputEmail1" class="col-sm-2"><?php echo lang('SMTP Password'); ?></label>
									<div class="col-sm-10">
										<input name="smtp_pass"  id="passwordnya" type="password" class="form-control" value="<?php echo $setting->smtp_pass; ?>" placeholder="Enter SMTP Password">
										<span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>
									</div>
								</div>
								<div class="form-group row">
									<label for="exampleInputEmail1" class="col-sm-2"><?php echo lang('SMTP Host'); ?></label>
									<div class="col-sm-10">
										<input name="smtp_host"  type="text" class="form-control" value="<?php echo $setting->smtp_host; ?>" aria-describedby="emailHelp" placeholder="Enter VAT number">
										<small  id="emailHelp" class="form-text text-muted"><?php echo lang('SMTP Host ex: ssl://smtp.googlemail.com'); ?></small>
									</div>
								</div>
								<div class="form-group row">
									<label for="exampleInputEmail1" class="col-sm-2"><?php echo lang('SMTP Port'); ?></label>
									<div class="col-sm-10">
										<input name="smtp_port"  type="text" class="form-control" value="<?php echo $setting->smtp_port; ?>" aria-describedby="emailHelp" placeholder="Enter VAT number">
										<small  id="emailHelp" class="form-text text-muted"><?php echo lang('SMTP Port ssl: 465 tls: 587'); ?></small>
									</div>
								</div>
								<?php }?>
							</div>
							<div class="form-group row">
								<label for="exampleInputEmail1" class="col-sm-2"></label>
								<div class="col-sm-10">
									<button type="submit" class="btn btn-primary"><?php echo lang('Save'); ?></button>
								</div>
							</fieldset>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script>
		$(".toggle-password").click(function() {
	//$("#passwordnya").prop('type', 'text');
	});
	</script>

	<script>
$(document).on('click', '[data-toggle="lightbox"]', function(event) {
event.preventDefault();
$(this).ekkoLightbox();
});
$('#cred').on('change', function (e) {
var optionSelected = $("option:selected", this);
var valueSelected = this.value;
if(valueSelected == "mail"){
$('#cre').html('');
}else{
$('#cre').html(' <div class="form-group row"> <label for="exampleInputEmail1" class="col-sm-2"><?php echo lang('SMTP Username'); ?></label> <div class="col-sm-10"> <input name="smtp_user" type="text" class="form-control" value="<?php echo $setting->smtp_user; ?>" aria-describedby="emailHelp"> <small id="emailHelp" class="form-text text-muted">Url to your email address sender</small> </div> </div> <div class="form-group row"> <label for="exampleInputEmail1" class="col-sm-2"><?php echo lang('SMTP Password'); ?></label> <div class="col-sm-10"> <input name="smtp_pass" id="passwordnya" type="password" class="form-control" value="<?php echo $setting->smtp_pass; ?>" placeholder="Enter SMTP Password"> <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span> </div> </div> <div class="form-group row"> <label for="exampleInputEmail1" class="col-sm-2"><?php echo lang('SMTP Host'); ?></label> <div class="col-sm-10"> <input name="smtp_host" type="text" class="form-control" value="<?php echo $setting->smtp_host; ?>" aria-describedby="emailHelp" placeholder="Enter HOST Name"> <small id="emailHelp" class="form-text text-muted"><?php echo lang('SMTP Host ex:'); ?> ssl://smtp.googlemail.com</small> </div> </div> <div class="form-group row"> <label for="exampleInputEmail1" class="col-sm-2">SMTP Port</label> <div class="col-sm-10"><input name="smtp_port" type="text" class="form-control" value="<?php echo $setting->smtp_port; ?>" aria-describedby="emailHelp" placeholder="<?php echo lang('Enter PORT number'); ?>"> <small id="emailHelp" class="form-text text-muted"><?php echo lang('SMTP Port ssl: 465 tls: 587'); ?></small> </div> </div>');
}
});
</script>