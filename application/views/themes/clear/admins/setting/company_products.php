<?php //print_r($company);?>
<div class="content-i">
    <div class="content-box">
        <div class="element-wrapper">
            <h6 class="element-header">
                <?php echo lang('Products'); ?>
            </h6>
            <div class="element-box">
                <h5 class="form-header">
                    <?php echo lang('Product list'); ?> <span class="text-danger"><strong><?php echo $company->companyname; ?></strong></span>
                    <div class="float-right"> <button class="btn btn-primary btn-sm"
                            onclick="add_product()" type="button"><i class="fa fa-plus-circle"></i>
                            <?php echo lang('Add New Product'); ?></button>
                    </div>
                </h5>
                <div class="table-responsive">
                    <table class="table table-striped table-lightfont" id="product">
                        <thead>
                            <tr>
                                <th><?php echo lang('Product ID'); ?>
                                </th>
                                <th><?php echo lang('Type'); ?>
                                </th>
                                <th><?php echo lang('Magebo Pricing ID'); ?>
                                </th>
                                <th><?php echo lang('PriceList'); ?>
                                </th>
                                <th><?php echo lang('PriceListSubType'); ?>
                                </th>
                                <th><?php echo lang('Bundles List'); ?>
                                </th>
                                <th><?php echo lang('iInvoiceGroupNbr'); ?>
                                </th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="content-box">
        <div class="element-wrapper">
            <h6 class="element-header">
                <?php echo lang('Addons'); ?>
            </h6>
            <div class="element-box">
                <h5 class="form-header">
                    <?php echo lang('Addons list'); ?>  <span class="text-danger"><strong><?php echo $company->companyname; ?></strong></span>
                    <div class="float-right"> <button class="btn btn-primary btn-sm" onclick="add_addon()"><i
                                class="fa fa-plus-circle"></i>
                            <?php echo lang('Add New Addons'); ?></button>
                    </div>
                </h5>
                <div class="table-responsive">
                    <table class="table table-striped table-lightfont" id="addons">
                        <thead>
                            <tr>
                                <th><?php echo lang('AddonName'); ?>
                                </th>
                                <th><?php echo lang('Description'); ?>
                                </th>
                                <th><?php echo lang('Type'); ?>
                                </th>
                                <th><?php echo lang('Price'); ?>
                                </th>
                                <th><?php echo lang('ShowCustomer'); ?>
                                </th>
                                <th><?php echo lang('Status'); ?>
                                </th>
                                <th><?php echo lang('BundleID'); ?>
                                </th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>
<script>
    $(document).ready(function() {
        $.getJSON(window.location.protocol + '//' + window.location.host + '/admin/complete/get_lang', function(
            data) {
            $('#product').DataTable({
                "autoWidth": false,
                "processing": true,
                "orderCellsTop": true,
                "ordering": true,
                "serverSide": true,
                "ajax": window.location.protocol + '//' + window.location.host +
                    '/admin/table/getcompany_products/<?php echo $this->uri->segment(4); ?>',
                "aaSorting": [
                    [0, 'asc']
                ],
                "language": {
                    "url": window.location.protocol + '//' + window.location.host +
                        "/assets/clear/js/datatables/lang/" + data.result + ".json"
                },
                "fnRowCallback": function(nRow, aData, iDisplayIndex) {
                    $('td:eq(7)', nRow).html(
                        '<button  type="button" onclick="edit_product(\'' + aData[
                            7] +
                        '\');"><i class="fa fa-cog"></i></button> <button  type="button" onclick="copy_product(\'' +
                        aData[7] + '\');"><i class="fa fa-copy"></i></button>');
                    $('td:eq(2)', nRow).html(aData[8] + aData[2]);
                    //$('td:eq(5)', nRow).html('<a  href="#" onclick="add_superadmin_company(\''+aData[5]+'\');"><i class="fa fa-user-plus"></i></a>');
                    return nRow;
                },
            });

            $('#addons').DataTable({
                "autoWidth": false,
                "processing": true,
                "orderCellsTop": true,
                "ordering": true,
                "serverSide": true,
                "ajax": window.location.protocol + '//' + window.location.host +
                    '/admin/table/getcompany_addons/<?php echo $this->uri->segment(4); ?>',
                "aaSorting": [
                    [0, 'asc']
                ],
                "language": {
                    "url": window.location.protocol + '//' + window.location.host +
                        "/assets/clear/js/datatables/lang/" + data.result + ".json"
                },
                "fnRowCallback": function(nRow, aData, iDisplayIndex) {
                    $('td:eq(3)', nRow).html(aData[8] + aData[3]);
                    $('td:eq(7)', nRow).html('<button onclick="edit_addon(' + aData[7] +
                        ');"><i class="fa fa-cog"></i></button> <button onclick="delete_addon(\'' +
                        aData[7] + '\');"><i class="fa fa-trash"></i></button>');
                    if (aData[4] == 1) {
                        $('td:eq(4)', nRow).html('Yes');
                    } else {
                        $('td:eq(4)', nRow).html('No');
                    }

                    if (aData[5] == 1) {
                        $('td:eq(5)', nRow).html('Active');
                    } else {
                        $('td:eq(5)', nRow).html('Disabled');
                    }

                    //$('td:eq(5)', nRow).html('<a  href="#" onclick="add_superadmin_company(\''+aData[5]+'\');"><i class="fa fa-user-plus"></i></a>');
                    return nRow;
                },
            });


        });
    });
</script>
<script>
    function edit_addon(id) {
         var invoicing = '<?php echo $setting->mage_invoicing; ?>';

        if (invoicing == "0") {
            console.log({invoice: invoicing});
            $('.billing').html('');
        }
        $.ajax({
            url: window.location.protocol + '//' + window.location.host + '/master/setting/get_addon/' + id,
            dataType: 'json',
            success: function(data) {
                for (const [key, value] of Object.entries(data)) {
                    console.log(key, value);

                    $('#addon_' + key).val(value);
                }
                $('#editAddon').modal('toggle');
            }
        });
    }

    function add_addon() {
        $('#addAddon').modal('toggle');
         var invoicing = '<?php echo $setting->mage_invoicing; ?>';

        if (invoicing == "0") {
            console.log({invoice: invoicing});
            $('.billing').html('');
        }

    }

    function AddAddonNow() {
        $('#form_addaddon').submit();
    }




    function UpdateAddon() {

        $('.btn').prop('disabled', true);
        // $('#form_editaddon').submit();
        var id = $('#addon_id').val();
        $.ajax({
            url: window.location.protocol + '//' + window.location.host + '/master/setting/edit_addon/' + id,
            dataType: 'json',
            type: 'POST',
            data: $("#form_editaddon").serialize(),
            success: function(data) {

                if (data.result) {
                    window.location.href = window.location.protocol + '//' + window.location.host +
                        '/master/setting/company_products/' + data.companyid;
                }

            }

        });
    }

    function delete_addon(id) {
        var t = confirm(
            'Are you sure?, any pending orders which uses this bundle/package will result error in provisoning!!');
        if (t) {
            $.ajax({
                url: window.location.protocol + '//' + window.location.host + '/master/setting/delete_addon/' +
                    id,
                dataType: 'json',
                type: 'POST',
                data: {
                    addonid: id,
                    companyid: '<?php echo $this->uri->segment(4); ?>'
                },
                success: function(data) {

                    if(data.result){
                         window.location.href = window.location.protocol + '//' + window.location.host +
                        '/master/setting/company_products/<?php echo $this->uri->segment(4); ?>';

                    }

                }

            });
        }
    }
    function addNewPlatform(modal){

        $("#"+modal).modal('toggle');
        $("#addPlatform").modal('toggle');

    }

    function addPlatformNow(){
        $('.btn').prop('disabled', true);
        $('#form_addPlatformAPI').submit();
    }
    function copy_product(id){
        var invoicing = '<?php echo $setting->mage_invoicing; ?>';

        if (invoicing == "0") {
            console.log({invoice: invoicing});
            $('.billing').hide();
        }
            $.ajax({
                url: window.location.protocol + '//' + window.location.host + '/master/setting/copy_product/<?php echo $this->uri->segment(4); ?>',
                dataType: 'json',
                type: 'POST',
                data: {
                    pid: id
                },
                success: function(data) {
                    $('#addname').val(data.name);
                    $('#addrecurring_total').val(data.recurring_total);
                    $('#addrecurring_taxrate').val(data.recurring_taxrate);
                    $('#addProduct').modal('toggle');

                }

            });


    }

    function EditProductNow(){

        $('#EditProductNowform').submit();

    }
    function edit_product(id) {
        var invoicing = '<?php echo $setting->mage_invoicing; ?>';
        //console.log({invoice: invoicing});
        if (invoicing == "0") {
            console.log({invoice: invoicing});
            $('.billing').hide();
        }
        $.ajax({
            url: window.location.protocol + '//' + window.location.host +
                '/master/setting/get_company_productid/' +
                id,
            dataType: 'json',
            success: function(data) {
                console.log(data);
                $("#editProduct").modal('toggle');
                $('#GPcRemark').val(data.GPcRemark);
                $('#name').val(data.name);
                $('#recurring_total').val(data.recurring_total);
                $('#recurring_taxrate').val(data.recurring_taxrate);
                $('#iInvoiceGroupNbr').val(data.iInvoiceGroupNbr);
                $('#iInvoiceGroupNbr').val(data.iInvoiceGroupNbr);
                $('#PriceListSubType').val(data.PriceListSubType);
                $('#welcome_template').val(data.welcome_template);
                $('#GPcRemark').val(data.GPcRemark);
                $('#iInvoiceDescription').val(data.iInvoiceDescription);
                $('#jasper_server').val(data.jasper_server);
                $('#PriceList').val(data.PriceList);
                $('#productid').val(id);
                $('#optionsx').html(data.html);
                $('#bundleidsx').show();
        var api_id1 = $( "#editProduct_api_id option:selected" ).text();
        var api_id1_val = $( "#editProduct_api_id option:selected" ).val();
        if(api_id1.indexOf("ARTA")>1){


            console.log('is ARTA');
            $.ajax({
            url: window.location.protocol + '//' + window.location.host + '/master/setting/getPlatformPackages/'+api_id1_val+'/'+data.ArtaPackageId,
            dataType: 'json',
            type: 'POST',
            data: {
                companyid: <?php echo $this->uri->segment(4); ?>
            },
            success: function(data) {
                console.log(data);
                if (data.result) {
                    $('#EditArtaPackageId').html(data.html);
                }else{
                    alert(data.message);
                }

            }

        });

        }else{
            $('#EditPackageArta').html('');
        }
            }
        });
    }

    function add_product() {
        var invoicing = '<?php echo $setting->mage_invoicing; ?>';
        console.log({invoice: invoicing});
        if (invoicing == "0") {
            console.log({invoice: invoicing});
            $('.billing').hide();
        }
        $.ajax({
            url: window.location.protocol + '//' + window.location.host +
                '/master/setting/getbundles/<?php echo $this->session->cid; ?>',
            dataType: 'json',
            success: function(data) {
                console.log(data);
                $('#bundleidsq').show();
                $('#optionsq').html(data.html);
                $("#addProduct").modal('toggle');
                var api_id2 = $( "#addProduct_api_id option:selected" ).text();
        var api_id2_val  = $( "#addProduct_api_id option:selected" ).val();
        if(api_id2.indexOf("ARTA")>1){
            console.log('is ARTA');

        $.ajax({
            url: window.location.protocol + '//' + window.location.host + '/master/setting/getPlatformPackages/'+api_id2_val,
            dataType: 'json',
            type: 'POST',
            data: {
                companyid: <?php echo $this->uri->segment(4); ?>
            },
            success: function(data) {
                console.log(data);
                if (data.result) {
                    $('#AddArtaPackageId').html(data.html);
                }

            }

        });
        }else{
            $('#AddPackageArta').html('');
        }
            }
        });
    }
</script>
<div class="modal fade" id="editProduct">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form method="post" id="EditProductNowform" action="<?php echo base_url(); ?>master/setting/edit_product">
                <input type="hidden" name="companyid"
                    value="<?php echo $this->uri->segment(4); ?>">
                <input type="hidden" name="id" value="" id="productid">
                <div class="modal-header">
                    <h4 class="modal-title"><?php echo lang('Edit Product'); ?>
                    </h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for=""><?php echo lang('Product Name'); ?></label>
                                <input class="form-control" autocomplete="new-username" type="text" name="name"
                                    id="name" required>
                            </div>
                            <div class="row">
                                <div class="form-group  col-md-6">
                                    <label for=""><?php echo lang('Price Include VAT'); ?></label>
                                    <input class="form-control" autocomplete="new-username" type="number" step="any"
                                        name="recurring_total" id="recurring_total" required>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for=""><?php echo lang('Tax rate'); ?></label>
                                    <input class="form-control" autocomplete="new-username" type="number" step="any"
                                        name="recurring_taxrate" id="recurring_taxrate" required>
                                </div>
                            </div>
                            <div class="form-group" id="bundleidsx" style="display:none;">
                                <div id="optionsx">
                                </div>
                            </div>

                            <div class="billing">
                            <div class="form-group">
                                <label for=""><?php echo lang('GPcRemark'); ?></label>
                                <input class="form-control" autocomplete="new-username" type="text" name="GPcRemark"
                                    id="GPcRemark">
                            </div>
                            <div class="form-group">
                                <label for=""><?php echo lang('cInvoiceDescription'); ?>
                                    BASE LANG</label>
                                <input class="form-control" autocomplete="new-username" type="text"
                                    name="iInvoiceDescription" id="iInvoiceDescription">
                            </div>
                            <?php //print_r($supported_languages);?>
                            <?php foreach ($supported_languages as $lang) {
    ?>
                            <div class="form-group">
                                <label for=""><?php echo lang('cInvoiceDescription'); ?>
                                    <?php echo $lang; ?></label>
                                <input class="form-control" autocomplete="new-username" type="text"
                                    name="lang[<?php echo $lang; ?>]"
                                    id="lang_<?php echo $lang; ?>">
                            </div>
                            <?php
} ?>
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label for=""><?php echo lang('iInvoiceGroupNbr'); ?></label>
                                    <input class="form-control" autocomplete="new-username" type="number"
                                        name="iInvoiceGroupNbr" id="iInvoiceGroupNbr">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for=""><?php echo lang('PriceList'); ?></label>
                                    <input class="form-control" autocomplete="new-username" type="number"
                                        name="PriceList" id="PriceList">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for=""><?php echo lang('PriceListSubType'); ?></label>
                                    <input class="form-control" autocomplete="new-username" type="number"
                                        name="PriceListSubType" id="PriceListSubType">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for=""><?php echo lang('Welcome Template name'); ?></label>
                                <input class="form-control" autocomplete="new-username" type="text"
                                    name="welcome_template" id="welcome_template">
                            </div>
                            <div class="form-group">
                                <label for=""><?php echo lang('Jaser Server (PDF)'); ?></label>
                                <input class="form-control" autocomplete="new-username" type="text" name="jasper_server"
                                    id="jasper_server">
                            </div>
                            </div>

                            <div class="form-group">
                                <fieldset>
                                    <label for="exampleSelect1"><?php echo lang('Choose Platform (Make sure you have add PLatform Credential) or'); ?><button type="button" class="btn" onclick="addNewPlatform('editProduct')">create new</button></label>
                                    <select class="form-control" id="editProduct_api_id" name="api_id">
                                        <?php foreach ($platforms as $plt) {
        ?>
                                        <option value="<?php echo $plt->id; ?>"><?php echo $plt->id; ?>:<?php echo $plt->platform; ?>:<?php echo $plt->m_provider; ?>:<?php echo $plt->m_payment_type; ?>:<?php echo $plt->teum_mvnoid; ?>:<?php echo $plt->arta_version; ?></option>
                                        <?php
    } ?>

                                    </select>
                                </fieldset>
                            </div>

                            <div class="form-group" id="EditPackageArta">
                                <fieldset>
                                    <label for="exampleSelect1"><?php echo lang('Assign Platform Package'); ?>
                                    </label>
                                    <select class="form-control" id="EditArtaPackageId" name="ArtaPackageId">

                                    </select>
                                </fieldset>
                            </div>


                            <div class="form-group">
                                <fieldset>
                                    <label for="exampleSelect1">Product Type</label>
                                    <select class="form-control" id="invoicing" name="product_type">
                                        <option value="mobile">Mobile</option>
                                        <option value="xdsl">xDSL</option>
                                        <option value="voip">Voip</option>
                                        <option value="others">Others</option>
                                    </select>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Modal footer  smtp_sender. smtp_name-->
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" onclick="EditProductNow()"><?php echo lang('Submit'); ?></button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="addProduct">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form method="post"
                action="<?php echo base_url(); ?>master/setting/add_product">
                <input type="hidden" name="companyid"
                    value="<?php echo $this->uri->segment(4); ?>">
                <div class="modal-header">
                    <h4 class="modal-title"><?php echo lang('Add Product'); ?>
                    </h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for=""><?php echo lang('Product Name'); ?></label>
                                <input class="form-control" autocomplete="new-username" type="text" name="name"
                                    id="addname" required>
                            </div>
                            <div class="form-group">
                                <label for=""><?php echo lang('Price Include VAT'); ?></label>
                                <input class="form-control" autocomplete="new-username" type="text"
                                    name="recurring_total" id="addrecurring_total" required>
                            </div>
                            <div class="form-group">
                                <label for=""><?php echo lang('Tax rate'); ?></label>
                                <input class="form-control" autocomplete="new-username" type="text"
                                    name="recurring_taxrate" id="addrecurring_taxrate" required>
                            </div>

                        </div>
                        <div class="col-sm-12">
                            <div class="form-group" id="bundleidsq" style="display:none;">
                                <div id="optionsq">
                                </div>
                            </div>
                            <div class="billing">
                            <div class="form-group">
                                <label for=""><?php echo lang('GPcRemark'); ?></label>
                                <input class="form-control" autocomplete="new-username" type="text"
                                    name="GPcRemark" id="addGPcRemark">
                            </div>
                            <div class="form-group">
                                <label for=""><?php echo lang('iInvoiceDescription'); ?></label>
                                <input class="form-control" autocomplete="new-username" type="text"
                                    name="iInvoiceDescription" id="addiInvoiceDescription">
                            </div>
                            <div class="form-group">
                                <label for=""><?php echo lang('iInvoiceGroupNbr'); ?></label>
                                <input class="form-control" autocomplete="new-username" type="text"
                                    name="iInvoiceGroupNbr" id="addiInvoiceGroupNbr">
                            </div>
                            <div class="form-group">
                                <label for=""><?php echo lang('PriceList'); ?></label>
                                <input class="form-control" autocomplete="new-username" type="text" name="PriceList"
                                    id="addPriceList">
                            </div>
                            <div class="form-group">
                                <label for=""><?php echo lang('PriceListSubType'); ?></label>
                                <input class="form-control" autocomplete="new-username" type="text"
                                    name="PriceListSubType" id="addPriceListSubType">
                            </div>
                            <div class="form-group">
                                <label for=""><?php echo lang('Welcome Template name'); ?></label>
                                <input class="form-control" autocomplete="new-username" type="text"
                                    name="welcome_template" id="addwelcome_template">
                            </div>
                            <div class="form-group">
                                <label for=""><?php echo lang('Jaser Server (PDF)'); ?></label>
                                <input class="form-control" autocomplete="new-username" type="text"
                                    name="jasper_server" id="addjasper_server">
                            </div>
                            </div>
                            <div class="form-group">
                                <fieldset>
                                    <label for="exampleSelect1">Choose Platform (Make sure you have add PLatform Credential) or<button type="button" class="btn" onclick="addNewPlatform('addProduct')">create new</button></label>
                                    <select class="form-control" id="addProduct_api_id" name="api_id">
                                        <?php foreach ($platforms as $plt) {
        ?>
                                        <option value="<?php echo $plt->id; ?>"><?php echo $plt->id; ?>:<?php echo $plt->platform; ?>:<?php echo $plt->m_provider; ?>:<?php echo $plt->m_payment_type; ?>:<?php echo $plt->teum_mvnoid; ?>:<?php echo $plt->arta_version; ?></option>
                                        <?php
    } ?>

                                    </select>
                                </fieldset>
                            </div>
                            <div class="form-group" id="AddPackageArta">
                                <fieldset>
                                    <label for="exampleSelect1"><?php echo lang('Assign Platform Package'); ?>
                                    </label>
                                    <select class="form-control" id="AddArtaPackageId" name="ArtaPackageId">

                                    </select>
                                </fieldset>
                            </div>
                            <div class="form-group">
                                <fieldset>
                                    <label for="exampleSelect1">Product Type</label>
                                    <select class="form-control" id="invoicing" name="product_type">
                                        <option value="mobile">Mobile</option>
                                        <option value="xdsl">xDSL</option>
                                        <option value="voip">Voip</option>
                                        <option value="others">Others</option>
                                    </select>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Modal footer  smtp_sender. smtp_name-->
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary"><?php echo lang('Submit'); ?></button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="editAddon">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form id="form_editaddon">
                <input type="hidden" name="companyid"
                    value="<?php echo $this->uri->segment(4); ?>">
                <input type="hidden" name="id" value="" id="productid">
                <div class="modal-header">
                    <h4 class="modal-title"><?php echo lang('Edit Addon'); ?>
                    </h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <span class="text-help text-danger"><?php echo lang('Please do not modify fields that you do not fully understand,
                        consult with Thierry van Eylen'); ?></span>
                    <br />
                    <br />
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="form-group  col-md-6">
                                    <label for=""><?php echo lang('Addon Name'); ?></label>
                                    <input class="form-control" autocomplete="new-username" type="text" name="name"
                                        id="addon_name" required>
                                </div>
                                <div class="form-group  col-md-6">
                                    <label for=""><?php echo lang('Addon Description'); ?></label>
                                    <input class="form-control" autocomplete="new-username" type="text" name="name_desc"
                                        id="addon_name_desc" required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group  col-md-6">
                                    <label for=""><?php echo lang('Price Include VAT'); ?></label>
                                    <input class="form-control" autocomplete="new-username" name="recurring_total"
                                        id="addon_recurring_total" required>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for=""><?php echo lang('Tax rate'); ?></label>
                                    <input class="form-control" autocomplete="new-username" name="recurring_taxrate"
                                        id="addon_recurring_taxrate" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for=""><?php echo lang('Platform BundleID'); ?></label>
                                <input class="form-control" autocomplete="new-username" type="text" name="bundleid"
                                    id="addon_bundleid" required>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-3">
                                    <fieldset>
                                        <label for="exampleSelect1"><?php echo lang('Addon Type');?></label>
                                        <select class="form-control" id="addon_bundle_type" name="bundle_type">
                                            <option value="tarif"><?php echo lang('Tarif (Auto renew)'); ?></option>
                                            <option value="option"><?php echo lang('Option (30 days)'); ?></option>

                                            <option value="others"><?php echo lang('Others'); ?></option>
                                        </select>
                                    </fieldset>
                                </div>
                                <div class="form-group col-md-3">
                                    <fieldset>
                                        <label for="exampleSelect1"><?php echo lang('Show Customer'); ?></label>
                                        <select class="form-control" id="addon_show_customer" name="show_customer">
                                            <option value="1"><?php echo lang('Yes'); ?></option>
                                            <option value="0"><?php echo lang('No'); ?></option>
                                        </select>
                                    </fieldset>
                                </div>
                                <div class="form-group col-md-3">
                                    <fieldset>
                                        <label for="exampleSelect1">Active</label>
                                        <select class="form-control" id="addon_status" name="status">
                                            <option value="1">Yes</option>
                                            <option value="0">No</option>
                                        </select>
                                    </fieldset>
                                </div>

                                <div class="form-group col-md-3">
                                    <fieldset>
                                        <label for="base_bundle"><?php echo lang('Base Product'); ?></label>
                                        <select class="form-control" id="addon_base_bundle" name="base_bundle">
                                            <option value="1"><?php echo lang('Yes'); ?></option>
                                            <option value="0" selected><?php echo lang('No'); ?></option>
                                        </select>
                                    </fieldset>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group  col-md-6">
                                    <label for=""><?php echo lang('Duration Cycle'); ?></label>
                                    <select class="form-control" id="addon_bundle_duration_type"
                                        name="bundle_duration_type">
                                        <option value="month"><?php echo lang('Month'); ?></option>
                                        <option value="day"><?php echo lang('Day'); ?></option>
                                    </select>
                                </div>
                                <div class="form-group  col-md-6">
                                    <label for=""><?php echo lang('Bundle duration Value'); ?></label>
                                    <input class="form-control" autocomplete="new-username" type="number"
                                        name="bundle_duration_value" id="addon_bundle_duration_value" required>
                                </div>


                            </div>

                        <div class="billing">
                            <div class="row">
                                <div class="form-group  col-md-4">
                                    <label for=""><?php echo lang('SubscriptionID'); ?></label>
                                    <input class="form-control" autocomplete="new-username" type="text"
                                        name="subscriptionid" id="addon_subscriptionid" required>
                                </div>
                                <div class="form-group  col-md-4">
                                    <label for=""><?php echo lang('GPInvoiceGroupNbr'); ?></label>
                                    <input class="form-control" autocomplete="new-username" type="text"
                                        name="GPInvoiceGroupNbr" id="addon_GPInvoiceGroupNbr" required>
                                </div>
                                <div class="form-group  col-md-4">
                                    <label for=""><?php echo lang('GPcRemark'); ?></label>
                                    <input class="form-control" autocomplete="new-username" type="text" name="GPcRemark"
                                        id="addon_GPcRemark" required>
                                </div>

                            </div>



                            <div class="row">
                                <div class="form-group  col-md-6">
                                    <label for=""><?php echo lang('FCA'); ?></label>
                                    <input class="form-control" autocomplete="new-username" type="text" name="FCA"
                                        id="addon_FCA" required>
                                </div>
                                <div class="form-group  col-md-6">
                                    <label for=""><?php echo lang('FCA Extra'); ?></label>
                                    <input class="form-control" autocomplete="new-username" type="text" name="FCA_extra"
                                        id="addon_FCA_extra" required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group  col-md-6">
                                    <label for=""><?php echo lang('FCL'); ?></label>
                                    <input class="form-control" autocomplete="new-username" type="text" name="FCL"
                                        id="addon_FCL" required>
                                </div>

                                <div class="form-group  col-md-6">
                                    <label for=""><?php echo lang('FCL_extra'); ?></label>
                                    <input class="form-control" autocomplete="new-username" type="text" name="FCL_extra"
                                        id="addon_FCL_extra" required>
                                </div>

                            </div>


                            <div class="row">
                                <div class="form-group  col-md-6">
                                    <label for=""><?php echo lang('Pricing Query'); ?></label>
                                    <input class="form-control" autocomplete="new-username" type="text"
                                        name="pricing_query" id="addon_pricing_query" required>
                                </div>
                                <div class="form-group  col-md-6">
                                    <label for=""><?php echo lang('Pricing Query extra'); ?></label>
                                    <input class="form-control" autocomplete="new-username" type="text"
                                        name="pricing_query_extra" id="addon_pricing_query_extra" required>
                                </div>


                            </div>
                        </div>

                        </div>
                    </div>
                </div>
                <!-- Modal footer  smtp_sender. smtp_name-->
                <div class="modal-footer">
                    <button type="button" onclick="UpdateAddon()" class="btn btn-lg btn-primary"><i
                            class="fa fa-save"></i> <?php echo lang('Submit'); ?></button>

                </div>
                <input class="form-control" type="hidden" name="id" id="addon_id" required>
                <input class="form-control" type="hidden" name="companyid" id="addon_companyid" required>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="addAddon">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form id="form_addaddon" method="post" action="<?php echo base_url(); ?>master/setting/add_addon">
                <input type="hidden" name="companyid"
                    value="<?php echo $this->uri->segment(4); ?>">
                <input type="hidden" name="id" value="" id="productid">
                <div class="modal-header">
                    <h4 class="modal-title"><?php echo lang('Add New Addon'); ?>
                    </h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <span class="text-help text-danger"><?php echo lang('Please do not modify fields that you do not fully understand,
                        consult with Thierry van Eylen'); ?></span>
                    <br />
                    <br />
                     <div class="row">
                                <div class="form-group col-md-6">
                                    <fieldset>
                                        <label for="exampleSelect1"><?php echo lang('Product'); ?></label>
                                        <select class="form-control" id="agid" name="agid">
                                            <?php foreach (getProductSell($this->uri->segment(4)) as $row) {
        ?>
                                            <option value="<?php echo $row->gid; ?>"><?php echo $row->name; ?></option>

                                            <?php
    } ?>
                                        </select>
                                    </fieldset>
                                </div>
                            </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="form-group  col-md-6">
                                    <label for=""><?php echo lang('Addon Name'); ?></label>
                                    <input class="form-control" autocomplete="new-username" type="text" name="name"
                                        id="addon_name" required>
                                </div>
                                <div class="form-group  col-md-6">
                                    <label for=""><?php echo lang('Addon Description'); ?></label>
                                    <input class="form-control" autocomplete="new-username" type="text" name="name_desc"
                                        id="addon_name_desc" required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group  col-md-6">
                                    <label for=""><?php echo lang('Price Include VAT'); ?></label>
                                    <input class="form-control" autocomplete="new-username" name="recurring_total"
                                        id="addon_recurring_total" required>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for=""><?php echo lang('Tax rate'); ?></label>
                                    <input class="form-control" autocomplete="new-username" name="recurring_taxrate"
                                        id="addon_recurring_taxrate" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for=""><?php echo lang('Platform BundleID'); ?></label>
                                <input class="form-control" autocomplete="new-username" type="text" name="bundleid"
                                    id="addon_bundleid" required>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-3">
                                    <fieldset>
                                        <label for="exampleSelect1"><?php echo lang('Addon Type'); ?></label>
                                        <select class="form-control" id="addon_bundle_type" name="bundle_type">
                                            <option value="tarif"><?php echo lang('Tarif (Auto renew)'); ?></option>
                                            <option value="option" selected><?php echo lang('Option (30 days)'); ?></option>

                                            <option value="others"><?php echo lang('Others'); ?></option>
                                        </select>
                                    </fieldset>
                                </div>
                                <div class="form-group col-md-3">
                                    <fieldset>
                                        <label for="exampleSelect1"><?php echo lang('Show Customer'); ?></label>
                                        <select class="form-control" id="addon_show_customer" name="show_customer">
                                            <option value="1"><?php echo lang('Yes'); ?></option>
                                            <option value="0"><?php echo lang('No'); ?></option>
                                        </select>
                                    </fieldset>
                                </div>
                                <div class="form-group col-md-3">
                                    <fieldset>
                                        <label for="exampleSelect1"><?php echo lang('Active'); ?></label>
                                        <select class="form-control" id="addon_status" name="status">
                                            <option value="1"><?php echo lang('Yes'); ?></option>
                                            <option value="0"><?php echo lang('No'); ?></option>
                                        </select>
                                    </fieldset>
                                </div>
                                <div class="form-group col-md-3">
                                    <fieldset>
                                        <label for="base_bundle"><?php echo lang('Base Product'); ?></label>
                                        <select class="form-control" id="base_bundle" name="base_bundle">
                                            <option value="1"><?php echo lang('Yes'); ?></option>
                                            <option value="0" selected><?php echo lang('No'); ?></option>
                                        </select>
                                    </fieldset>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group  col-md-6">
                                    <label for=""><?php echo lang('Duration Type'); ?></label>
                                    <select class="form-control" id="addon_bundle_duration_type"
                                        name="bundle_duration_type">
                                        <option value="month"><?php echo lang('Month'); ?></option>
                                        <option value="day" selected><?php echo lang('Day'); ?></option>
                                    </select>
                                </div>
                                <div class="form-group  col-md-6">
                                    <label for=""><?php echo lang('Bundle duration Value'); ?></label>
                                    <input class="form-control" autocomplete="new-username" type="number"
                                        name="bundle_duration_value" id="addon_bundle_duration_value" required value="30">
                                </div>


                            </div>

                             <div class="billing">
                            <div class="row">
                                <div class="form-group  col-md-4">
                                    <label for=""><?php echo lang('SubscriptionID'); ?></label>
                                    <input class="form-control" autocomplete="new-username" type="text"
                                        name="subscriptionid" id="addon_subscriptionid" required>
                                </div>
                                <div class="form-group  col-md-4">
                                    <label for=""><?php echo lang('GPInvoiceGroupNbr'); ?></label>
                                    <input class="form-control" autocomplete="new-username" type="text"
                                        name="GPInvoiceGroupNbr" id="addon_GPInvoiceGroupNbr" required>
                                </div>
                                <div class="form-group  col-md-4">
                                    <label for=""><?php echo lang('GPcRemark'); ?></label>
                                    <input class="form-control" autocomplete="new-username" type="text" name="GPcRemark"
                                        id="addon_GPcRemark" required>
                                </div>

                            </div>



                            <div class="row">
                                <div class="form-group  col-md-6">
                                    <label for=""><?php echo lang('FCA'); ?></label>
                                    <input class="form-control" autocomplete="new-username" type="text" name="FCA"
                                        id="addon_FCA" required>
                                </div>
                                <div class="form-group  col-md-6">
                                    <label for=""><?php echo lang('FCA Extra'); ?></label>
                                    <input class="form-control" autocomplete="new-username" type="text" name="FCA_extra"
                                        id="addon_FCA_extra" required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group  col-md-6">
                                    <label for=""><?php echo lang('FCL'); ?></label>
                                    <input class="form-control" autocomplete="new-username" type="text" name="FCL"
                                        id="addon_FCL" required>
                                </div>

                                <div class="form-group  col-md-6">
                                    <label for=""><?php echo lang('FCL_extra'); ?></label>
                                    <input class="form-control" autocomplete="new-username" type="text" name="FCL_extra"
                                        id="addon_FCL_extra" required>
                                </div>

                            </div>


                            <div class="row">
                                <div class="form-group  col-md-6">
                                    <label for=""><?php echo lang('Pricing Query'); ?></label>
                                    <input class="form-control" autocomplete="new-username" type="text"
                                        name="pricing_query" id="addon_pricing_query" required>
                                </div>
                                <div class="form-group  col-md-6">
                                    <label for=""><?php echo lang('Pricing Query extra'); ?></label>
                                    <input class="form-control" autocomplete="new-username" type="text"
                                        name="pricing_query_extra" id="addon_pricing_query_extra" required>
                                </div>


                            </div>

                        </div>
                        </div>
                    </div>
                </div>
                <!-- Modal footer  smtp_sender. smtp_name-->
                <div class="modal-footer">
                    <button type="button" onclick="AddAddonNow()" class="btn btn-lg btn-primary"><i
                            class="fa fa-save"></i>
                        <?php echo lang('Submit'); ?></button>

                </div>

            </form>
        </div>
    </div>
</div>


<div class="modal fade" id="addPlatform">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form id="form_addPlatformAPI" method="post" action="<?php echo base_url(); ?>master/setting/addPlatfom/<?php echo $this->uri->segment(4); ?>">
                <input type="hidden" name="companyid" value="<?php echo $this->uri->segment(4); ?>">

                <div class="modal-header">
                    <h4 class="modal-title"><?php echo lang('Add New Platform'); ?>
                    </h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <span class="text-help text-danger"><?php echo lang('Please enter correct values to avoid issue on provisioning level'); ?> <?php echo lang('consult to Developer team for more information'); ?></span>
                    <br />
                    <br />
                    <div class="row">
                        <div class="col-sm-12">
                        <div class="form-group">
                                    <fieldset>
                                        <label for="m_payment_type"><?php echo lang('Platform Type'); ?></label>
                                        <select class="form-control" id="m_payment_type" name="m_payment_type">
                                            <option value="PRE PAID"><?php echo lang('Prepaid'); ?></option>
                                            <option value="POST PAID"><?php echo lang('Postpaid'); ?></option>
                                        </select>
                                    </fieldset>
                                </div>
                            <div class="row">
                                <div class="form-group  col-md-6">
                                    <label for=""><?php echo lang('API Username'); ?></label>
                                    <input class="form-control" autocomplete="new-username" type="text" name="m_username"
                                        id="m_username" required>
                                </div>
                                <div class="form-group  col-md-6">
                                    <label for=""><?php echo lang('API Password'); ?></label>
                                    <input class="form-control" autocomplete="new-username" type="text" name="m_password"
                                        id="m_password" required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group  col-md-6">
                                    <label for=""><?php echo lang('Teum MVNO ID'); ?></label>
                                    <input class="form-control" autocomplete="new-username" name="teum_mvnoid"
                                        id="teum_mvnoid">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for=""><?php echo lang('Provider'); ?></label>
                                    <input class="form-control" autocomplete="new-username" name="m_provider" style="text-transform: uppercase"
                                        id="m_provider" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for=""><?php echo lang('PLatform'); ?></label>
                                <select class="form-control" id="platform" name="platform">
                                            <option value="ARTA">Arta Platform</option>
                                            <option value="TEUM" selected>Teum Core Server</option>
                                        </select>
                            </div>

                            <div class="row">
                                <div class="form-group  col-md-6">
                                    <label for=""><?php echo lang('API Url'); ?></label>
                                    <input class="form-control" autocomplete="new-username" name="arta_url" value="https://wholesale.api.pareteum.cloud/core/"
                                        id="arta_url" required>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for=""><?php echo lang('API version'); ?></label>
                                    <input class="form-control" autocomplete="new-username" name="arta_version" value="2.4.0"
                                        id="arta_version" required>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for=""><?php echo lang('API Proxy'); ?></label>
                                    <input class="form-control" autocomplete="new-username" name="arta_proxy_url"
                                        id="arta_proxy_url">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for=""><?php echo lang('API Port'); ?></label>
                                    <input class="form-control" autocomplete="new-username" name="arta_proxy_port"
                                        id="arta_proxy_port">
                                </div>
                            </div>







                        </div>
                    </div>
                </div>
                <!-- Modal footer  smtp_sender. smtp_name-->
                <div class="modal-footer">
                    <button type="submit"  class="btn btn-lg btn-primary"><i
                            class="fa fa-save"></i>
                        <?php echo lang('Submit'); ?></button>

                </div>

            </form>
        </div>
    </div>
</div>