<div class="content-i">
  <div class="content-box">
    <div class="element-wrapper">
      <h6 class="element-header">
     Warehouse

      </h6>
      <div class="element-box">
        <h5 class="form-header">
       <?php echo lang('Manufacture Add'); ?>

        </h5>
        <div class="table-responsive">
        		<form method="post" action="<?php echo base_url(); ?>admin/warehouse/manufacture_add/" enctype="multipart/form-data">
							<div class="row">
								<div class="col-sm-12">
									<div class="form-group">
										<fieldset>
											<label class="control-label" for="firstname">Manufacture Name</label>
											<input  name="name" class="form-control" id="name" type="text" placeholder="Manufacture Name"  required>
										</fieldset>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-3">
									<div class="form-group">
										<fieldset>
											<label class="control-label" for="picture">Logo</label>
											<input name="picture"  class="form-control" type="file">
										</fieldset>
									</div>
								</div>
								<div class="col-sm-3">
								</div>
							</div>
							<div class="row">
								<div class="col-sm-12">
									<div class="form-group">
										<button type="submit" class="btn btn-md btn-primary btn-block"><i class="fa fa-save"></i> Save Manufacture</button>
									</div>
								</div>
							</div>
						</form>
        </div>
    </div>
</div>
</div>
</div>

<script>
	$(document).on('click', '[data-toggle="lightbox"]', function(event) {
event.preventDefault();
$(this).ekkoLightbox();
});
</script>