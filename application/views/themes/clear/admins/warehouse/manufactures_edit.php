<section class="tables">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-body">
						<form method="post" action="<?php echo base_url(); ?>admin/warehouse/manufacture_edit/" enctype="multipart/form-data">
							<input type="hidden" name="id" value="<?php echo $manufacture->id; ?>">
							<div class="row">
								<div class="col-sm-12">
									<div class="form-group">
										<fieldset>
											<label class="control-label" for="firstname">Manufacture Name</label>
											<input  name="name" class="form-control" id="name" type="text" placeholder="Manufacture Name" value="<?php echo $manufacture->name; ?>" required>
										</fieldset>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-3">
									<div class="form-group">
										<fieldset>
											<label class="control-label" for="picture">Logo</label>
											<input name="picture"  class="form-control" type="file">
										</fieldset>
									</div>
								</div>
								<div class="col-sm-3">
									<?php if (file_exists(FCPATH . 'assets/img/manufactures/' . $manufacture->logo)) {?>
									<div class="avatar"><a href="<?php echo base_url(); ?>assets/img/manufactures/<?php echo $manufacture->logo; ?>" data-toggle="lightbox"><img src="<?php echo base_url(); ?>assets/img/manufactures/<?php echo $manufacture->logo; ?>" height="70"></a></div>
									<?php }?>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-12">
									<div class="form-group">
										<button type="submit" class="btn btn-md btn-primary btn-block"><i class="fa fa-save"></i> Save Manufacture</button>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<script>
	$(document).on('click', '[data-toggle="lightbox"]', function(event) {
event.preventDefault();
$(this).ekkoLightbox();
});
</script>