<section class="tables">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-body">
						<form method="post" action="<?php echo base_url(); ?>admin/warehouse/product_edit/<?php echo $product->id; ?>" enctype="multipart/form-data">
							<input type="hidden" name="id" value="<?php echo $product->id; ?>">
							<div class="row">
								<div class="col-sm-12">
									<div class="form-group">
										<fieldset>
											<label class="control-label" for="firstname">Product Name</label>
											<input  name="name" class="form-control" id="firstname" type="text" placeholder="Firstname"  value="<?php echo $product->name; ?>" required>
										</fieldset>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-12">
									<div class="form-group">
										<label for="exampleTextarea">Product Description</label>
										<textarea class="form-control" id="exampleTextarea" rows="5" name="description"><?php echo nl2br($product->description); ?></textarea>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-4">
									<div class="form-group">
										<fieldset>
											<label for="exampleSelect1">Supplier</label>
											<select class="form-control" id="supplierid"  name="supplierid" >
												<?php foreach (getSuppliers() as $supplier) {?>
												<option value="<?php echo $supplier->id; ?>"><?php echo $supplier->name; ?></option>
												<?php }?>
											</select>
										</fieldset>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="form-group">
										<fieldset>
											<label for="exampleSelect1">Manufacture</label>
											<select class="form-control" id="manufactureid"  name="manufactureid" >
												<?php foreach (getManufacture() as $man) {?>
												<option value="<?php echo $man->id; ?>"><?php echo $man->name; ?></option>
												<?php }?>
											</select>
										</fieldset>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="form-group">
										<fieldset>
											<label class="control-label" for="stock">Stock</label>
											<input  name="stock" class="form-control" id="firstname" type="number" value="1" required>
										</fieldset>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-3">
									<div class="form-group">
										<fieldset>
											<label class="control-label" for="picture">Picture</label>
											<input name="picture"  class="form-control" type="file">
										</fieldset>
									</div>
								</div>
								<div class="col-sm-3">
									<?php if (file_exists(FCPATH . 'assets/img/products/' . $product->picture)) {?>
									<div class="avatar"><a href="<?php echo base_url(); ?>assets/img/products/<?php echo $product->picture; ?>" data-toggle="lightbox"><img src="<?php echo base_url(); ?>assets/img/products/<?php echo $product->picture; ?>" height="70"></a></div>
									<?php }?>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-12">
									<div class="form-group">
										<button type="submit" class="btn btn-md btn-primary btn-block"><i class="fa fa-save"></i> Save Administrator</button>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<script>
	$(document).on('click', '[data-toggle="lightbox"]', function(event) {
event.preventDefault();
$(this).ekkoLightbox();
});
</script>