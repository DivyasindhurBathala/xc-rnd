<div class="content-i">
  <div class="content-box">
    <div class="element-wrapper">
      <h6 class="element-header">
       <?php echo lang('Bank File List, Downloaded Daily'); ?>
        <div class="float-right">
                <?php if ($this->session->cid == 54) { ?>
                 <button type="button" id="fileupload" class="btn btn-sm btn-primary" onclick="openUpload()">Upload CSV file Last Transaction Date was: <span class="text-danger"><?php echo getLastFileDate($this->session->companyid); ?></span></button>
                <?php } else { ?>
                  Upload CSV file Last Transaction Date was: <span class="text-danger"><?php echo getLastFileDate($this->session->companyid); ?></span>
                <?php } ?>
            </div>
      </h6>
      <div class="element-box">
        <div class="form-header">
           
        </div>

         <div class="table-responsive" id="tablex">
          <table class="table table-striped table-lightfont" id="sepa">
            <thead>
              <tr>
                <th width="30%"><?php echo lang('ID'); ?></th>
                <th width="10%"><?php echo lang('Date'); ?></th>
                <th width="5%"><?php echo lang('Code'); ?></th>
                <th width="5%"><?php echo lang('Amount'); ?></th>
                <th width="10%"><?php echo lang('IBAN'); ?></th>
                <th width="30%"><?php echo lang('Message'); ?></th>
                <th width="10%"><?php echo lang('Name'); ?></th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
        <div class="table-responsive" id="assignbox">

        </div>
      </div>
    </div>
  </div>
</div>
<?php //if(strpos($this->session->email,"united-telecom.be")){ ?>
<div class="content-i">
  <div class="content-box">
    <div class="element-wrapper">
      <h6 class="element-header">
        <?php echo lang('Unassigned Payments'); ?>
      </h6>
      <div class="element-box">
        <h5 class="form-header">
            <?php echo lang('Unassigned Payments in the Billing'); ?>

        </h5>
         <div class="table-responsive" id="tablex">
          <table class="table table-striped table-lightfont" id="unAssign">
            <thead>
              <tr>
                <th><?php echo lang('PaymentNbr'); ?></th>
                <th><?php echo lang('BillingID'); ?></th>
                <th><?php echo lang('PaymentDate'); ?></th>
                <th><?php echo lang('Payment Amount'); ?></th>
                <th><?php echo lang('Amount Used'); ?></th>
                <th><?php echo lang('Amount Left'); ?></th>
                <th><?php echo lang('Description'); ?></th>
                <th><?php echo lang('MVNO ID'); ?></th>
                <th><?php echo lang('PaymentType'); ?></th>
                <th><?php echo lang('CustomerName'); ?></th>
                <th><?php echo lang('Action'); ?></th>
              </tr>
            </thead>
            <tbody>
            </tbody>
                 <tfoot>
            <tr>
                <th>PaymentNbr</th>
                <th>BillingI</th>
                <th>PaymentDate</th>
                <th>PaymentAmount</th>
                <th>Amount</th>
                 <th>Amount</th>
                  <th>Description</th>
                 <th>MVNO ID</th>
                  <th>PaymentType</th>
                 <th>CustomerName</th>
                  <th>Action</th>
                 
            </tr>
        </tfoot>
          </table>
        </div>
        <div class="table-responsive">

        </div>
      </div>
    </div>
  </div>
</div>
<?php //} ?>



<div class="content-i">
  <div class="content-box">
    <div class="element-wrapper">
      <h6 class="element-header">
        <?php echo lang('Reject List'); ?>
      </h6>
      <div class="element-box">
        <h5 class="form-header">
            <?php echo lang('Sepa Rejection List'); ?>

        </h5>
         <div class="table-responsive" id="tablex">
          <table class="table table-striped table-lightfont" id="separeject">
            <thead>
              <tr>
                <th width="10%"><?php echo lang('Date'); ?></th>
                <th width="20%"><?php echo lang('IBAN'); ?></th>
                <th width="5%"><?php echo lang('Amount'); ?></th>
                <th width="20%"><?php echo lang('Rejection Info'); ?></th>
                <th width="15%"><?php echo lang('Customer'); ?></th>
                <th width="15%"><?php echo lang('InvoiceNumber'); ?></th>
                <th width="10%"><?php echo lang('Action'); ?></th>
                <th width="5%"><?php echo lang('Counter'); ?></th>

              </tr>
            </thead>
            <tbody>
            </tbody>
              
          </table>
        </div>
        <div class="table-responsive">

        </div>
      </div>
    </div>
  </div>
</div>

<script>
  $(document).ready(function()
{
$.getJSON(window.location.protocol + '//' + window.location.host + '/admin/complete/get_lang', function(data) {
var x = new Date();
var y = x.getFullYear().toString();
var m = (x.getMonth() + 1).toString();
var d = x.getDate().toString();
(d.length == 1) && (d = '0' + d);
(m.length == 1) && (m = '0' + m);
var date = y +'-'+ m +'-'+ d;
 $('#sepa').DataTable({
"autoWidth": false,
"processing": true,
"orderCellsTop": true,
"ordering": true,
"serverSide": true,
"colReorder": true,
"ajax": window.location.protocol + '//' + window.location.host + '/admin/table/get_sepa_files/<?php echo $this->session->cid; ?>',
"aaSorting": [[1, 'desc']],
"language": {
"url": window.location.protocol + '//' + window.location.host +"/assets/clear/js/datatables/lang/" + data.result + ".json"
},
"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
$('td:eq(0)', nRow).html('<button class="btn btn-xs btn-primary" onclick="ProcessItem('+aData[0]+');">Assign to invoice</button> <button class="btn btn-xs btn-primary" onclick="AssignItem('+aData[0]+');">Assign to customer</button>');
$('td:eq(3)', nRow).html('€'+aData[3]);
$('td:eq(5)', nRow).html(nl2br(aData[5]));
return nRow;
},
});


var domtable = $('#separeject').DataTable({
"autoWidth": false,
"processing": true,
"orderCellsTop": true,
"ordering": true,
"serverSide": true,
"colReorder": true,
"ajax": window.location.protocol + '//' + window.location.host + '/admin/table/get_sepa_rejected/<?php echo $this->session->cid; ?>',
"aaSorting": [[0, 'desc']],
"language": {
"url": window.location.protocol + '//' + window.location.host +"/assets/clear/js/datatables/lang/" + data.result + ".json"
},
"createdRow": function( nRow, aData, dataIndex){
  if(aData[7] >= 2){
  $(nRow).addClass('text-danger');
}
            },
"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
$('td:eq(4)', nRow).html('<a href="<?php echo base_url(); ?>admin/client/detail/'+aData[10]+'">'+aData[8]+'</a>');
$('td:eq(5)', nRow).html('<a href="<?php echo base_url(); ?>admin/invoice/detail/'+aData[5]+'/'+aData[4]+'">'+aData[5]+'</a>');
$('td:eq(2)', nRow).html('€'+aData[2]);
//$('td:eq(7)', nRow).html(aData[10]);



if(aData[9] == 0){
$('td:eq(6)', nRow).html('<button onclick="setAction('+aData[6]+');" class="btn btn-xs btn-primary"><i class="fa fa-fire"></i></button> <button onclick="reminderAction('+aData[6]+');" class="btn btn-xs btn-primary"><i class="fa fa-envelope"></i></button>');
}else{
$('td:eq(6)', nRow).html('Procesed');
}

return nRow;
},
});


});

function nl2br (str, is_xhtml) {
    if (typeof str === 'undefined' || str === null) {
        return '';
    }
    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
}



$.getJSON(window.location.protocol + '//' + window.location.host + '/admin/complete/get_lang', function(data) {
$('#unAssign').DataTable({
     "initComplete": function () {
        this.api().columns().eq(0).each( function (index) {
            const column = this.column(index);
            const title = $(column.header()).text();
            if(index === 2 || index === 7 || index === 8){
                var select = $(`
                    <select class="form-control">
                        <option value="">Please choose</option>
                    </select>
                `)
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex($(this).val());
                    column
                        .search( val ? '^'+val+'$' : '', true, false )
                        .draw();
                });
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                });
            }else{
                var input = $(`
                    <input class="form-control" type="text" placeholder="Search ${title}" />
                `)
                    .appendTo( $(column.footer()).empty() )
                    .on( 'keyup change', function () {
                        var val = $.fn.dataTable.util.escapeRegex($(this).val());
                    column
                        .search( val )
                        .draw();
                });
            }
        });
    },
   "autoWidth": false,
    "ajax": {
    "url": window.location.protocol + '//' + window.location.host + '/admin/table/getPaymentUnAassign',
    "dataSrc": ""
      },
    "aaSorting": [[0, 'desc']],
    "columns": [
            { "data": "iPaymentNbr" },
            { "data": "iAddressNbr" },
            { "data": "PaymentDate" },
            { "data": "mPaymentAmount" },
            { "data": "UsedAmount" },
            { "data": "AmountLeft" },
            { "data": "cPaymentFormDescription" },
            { "data": "CustomerId" },
            { "data": "Type" },
             { "data": "CustomerName" },
            { "data": "id" }
        ],
    "language": {
    "url": window.location.protocol + '//' + window.location.host +"/assets/clear/js/datatables/lang/" + data.result + ".json"
        },
    "fnRowCallback": function( nRow, aData, iDisplayIndex ) {

      if(aData.mPaymentAmount == aData.AmountLeft){
      
        $('td:eq(0)', nRow).html('<button class="btn btn-danger btn-md" onclick="MarkRefund(\''+aData.iAddressNbr +','+ aData.iPaymentNbr +','+ aData.CustomerName +','+ aData.AmountLeft +','+aData.UsedAmount+'\')">Mark Refunded</button>');
      
      }
       $('td:eq(1)', nRow).html('<a href="<?php echo base_url(); ?>admin/client/detail/'+aData.id+'">'+aData.iAddressNbr+'</button>');
       $('td:eq(9)', nRow).html('<a href="<?php echo base_url(); ?>admin/client/detail/'+aData.id+'">'+aData.CustomerName+'</button>');
        $('td:eq(10)', nRow).html('<button class="btn btn-success btn-md" onclick="assignPaymentTobeProcess(\''+aData.iAddressNbr +','+ aData.iPaymentNbr +','+ aData.CustomerName +','+ aData.AmountLeft +'\')">Assign to Invoice</button>');
       //$('td:eq(1)', nRow).html('<a  href="<?php echo base_url(); ?>client/detail/'+aData.id+'">' + aData.CustomerName + '( '+a.CustomerId+' )</a>');
              return nRow;
        },
  });
});






});

</script>


<script>
 function proformabtn(){
var id = $('#itemidx').val();
var amount = $('#AmountPaid').val();
var msg = $('#msg').val();
 $('#paymentamount').val(amount);
 $('#paymentid').val(id);
 $('#ProcessItem').modal('toggle');
 $('#proforma').modal('toggle');
 }
  function reminderAction(id){
  $.ajax({
      url: window.location.protocol + '//' + window.location.host + '/admin/complete/get_sepa_item/'+id,
      type: 'post',
      dataType: "json",
      data: {
       id: id
      },
      success: function( data ) {
        $('#reminder_iInvoiceNbr').val(data.invoicenumber);
        $('#reminder_userid').val(data.userid);
        $('#reminder_iAddressNbr').val(data.iAddressNbr);
        $('#SendReminder').modal('toggle');
      }
     });
  }

  function AssignItem(id){
  $.getJSON(window.location.protocol + '//' + window.location.host + '/admin/complete/get_sepa_item/'+id, function(data) {
    console.log(data);
  $('#xhasil').html('<tr><td>'+data.id+'</td><td>€'+data.amount+'</td><td>'+data.date_transaction+'</td> <td>'+data.iban+'</td> <td>'+data.transactioncode+'</td></tr>')
  console.log(id);
  $('#xitemid').val(id);
  $('#xAmountPaid').val(data.amount);
  $('#AssigntoCustomer').modal('toggle');
  $('#xDatePayment').val(data.date_transaction);
  $('#xpaymentmsg').val(data.message);
  $('#xdate_transaction').val(data.date_transaction);
  });


  }

  function IssueRefundNow(){

     $.ajax({
      url: window.location.protocol + '//' + window.location.host + '/admin/sepa/assign_Refund_to_Payment',
      type: 'post',
      dataType: "json",
      data: {
       iPaymentNbr: $('#refund_PaymentId').val(),
       iAddressNbr: $('#refund_iAddressNbr').val(),
       cPaymentFormDescription: $('#refund_Description').val(),
       mPaymentAmount: $('#refund_Amount').val(),
       dPaymentDate:  $('#pickdate1').val(),
       PaymentUsed:  $('#refund_Used').val()
             },
      success: function( data ) {
        console.log(data);
        if(data.result){
        $('#IssueRefund').modal('toggle');
         window.location.replace(window.location.protocol + '//' + window.location.host + '/admin/sepa');
        }else{
          alert('Error on executing: RefundPayment, please contact our support');

        }
             
      }
     });


  }


  function MarkRefund(data){
  var res = data.split(",");
$('#refund_PaymentId').val(res[1]);
$('#refund_iAddressNbr').val(res[0]);
$('#refund_Amount').val(res[3]);
$('#refund_Used').val(res[4]);
$('#IssueRefund').modal('toggle');

  }
  function assignPaymentTobeProcess(data){

   console.log(data);
   var res = data.split(",");
 $.ajax({
      url: window.location.protocol + '//' + window.location.host + '/admin/sepa/getUnPaidInvoice',
      type: 'post',
      dataType: "json",
      data: {
       iAddressNbr: res[0]
      },
      success: function( data ) {
        if(data.length >0){
            $('#PLiPaymentNbr').val(res[1]);
            $('#PLcName').val(res[2]);
            $('#PLiAddressNbr').val(res[0]);
            $('#PLAmountPaid').val(res[3]);
              data.forEach(function(element) {
                $('#PLiInvoiceNbr').append('<option value="'+element.iInvoiceNbr+'">'+element.iInvoiceNbr+' - €'+element.mInvoiceAmount+'</option>');
            console.log(element.iInvoiceNbr);
           });
           $('#AssignPaymentLeft').modal('toggle');
        }else{
          alert('No Open Invoice Found for BillingID: '+res[0]);

        }
       

       
      }
     });



   
  }
function ProcessItem(id){
  $.getJSON(window.location.protocol + '//' + window.location.host + '/admin/complete/get_sepa_item/'+id, function(data) {
    console.log(data);
  $('#hasil').html('<tr><td>'+data.id+'</td><td>€'+data.amount+'</td><td>'+data.date_transaction+'</td> <td>'+data.iban+'</td> <td>'+data.transactioncode+'</td></tr>')
  console.log(id);
  $('#itemidx').val(id);
  $('#AmountPaid').val(data.amount);
  $('#ProcessItem').modal('toggle');
  $('#DatePayment').val(data.date_transaction);
  $('#paymentmsg').val(data.message);
  $('#date_transaction').val(data.date_transaction);
  });
}
function openUpload(){
$('#UploadBankfile').modal('toggle');
}
function AssignProforma(){
 var r = confirm("Are you sure?");
 
  if(r){
      var paymentamount = $('#paymentamount').val();
         var paymentid = $('#paymentid').val();
         var proformaid = $('#proformaid').val();
         var date_transaction = $('#date_transaction').val();
    if(proformaid >0){
 $.ajax({
    url: window.location.protocol + '//' + window.location.host + '/admin/sepa/process_proforma',
    type: 'post',
    dataType: "json",
    data: {
     itemid: paymentid,
     amount: paymentamount,
     invoicenumber: proformaid,
     date_transaction: date_transaction,
     executor: '<?php echo 'Executed by ' . $this->session->firstname . ' ' . $this->session->lastname; ?>'
    },
    success: function( data ) {
       window.location.href = window.location.protocol + '//' + window.location.host + '/admin/sepa/';
      if(data.result == 'success'){
        window.location.href = window.location.protocol + '//' + window.location.host + '/admin/sepa/';
      }else{
        alert('there was an error when processing your proforma, please contact our support team');
      }
    //  
    }
   });


    }else{

      alert('You need to provide invoicenumber');
    }

  }

}
function Assign(){
  var r = confirm("Are you sure?");
  if(r){
  var id = $('#itemidx').val();
  var iAddressNbr = $('#iAddressNbr').val();
  var mInvoiceAmount = $('#mInvoiceAmount').val();
  var iInvoiceNbr = $('#invoicenum').val();
  var AmountPaid = $('#AmountPaid').val();
  var DatePayment = $('#DatePayment').val();

  $('#assign').prop('disabled', true);
  $.ajax({
    url: window.location.protocol + '//' + window.location.host + '/admin/sepa/addPayment',
    type: 'post',
    dataType: "json",
    data: {
     itemid: id,
     iAddressNbr: iAddressNbr,
     iInvoiceNbr: iInvoiceNbr,
     mPaymentAmount: AmountPaid,
     mInvoiceAmount: mInvoiceAmount,
     cPaymentForm: 'BANK TRANSACTION',
     cPaymentFormDescription: 'Payment from Sepa Item: '+id,
     dPaymentDate: DatePayment,
     step: 1,
     cPaymentRemark: '<?php echo 'Executed by ' . $this->session->firstname . ' ' . $this->session->lastname; ?>'
    },
    success: function( data ) {

      window.location.href = window.location.protocol + '//' + window.location.host + '/admin/sepa/';
    }
   });
  }

}

function AssignToCustomerProcess(){
  var r = confirm("Are you sure?");
  if(r){
  var id = $('#xitemid').val();
  var iAddressNbr = $('#xiAddressNbr').val();
  var AmountPaid = $('#xAmountPaid').val();
  var DatePayment = $('#xDatePayment').val();

  $('#assign').prop('disabled', true);
  $.ajax({
    url: window.location.protocol + '//' + window.location.host + '/admin/sepa/addPaymentOnly',
    type: 'post',
    dataType: "json",
    data: {
     itemid: id,
     iAddressNbr: iAddressNbr,
     mPaymentAmount: AmountPaid,
     cPaymentForm: 'BANK TRANSACTION',
     cPaymentFormDescription: 'Payment from Sepa Item: '+id,
     dPaymentDate: DatePayment,
     step: 1,
     cPaymentRemark: '<?php echo 'Executed by ' . $this->session->firstname . ' ' . $this->session->lastname; ?>'
    },
    success: function( data ) {

      window.location.href = window.location.protocol + '//' + window.location.host + '/admin/sepa/';
    }
   });
  }

}


function setAction(id){

  var r = confirm("Do you wish to mark it as completed?");
  if(r){
  window.location.href = window.location.protocol + '//' + window.location.host + '/admin/sepa/set_reject_complete/'+id;
  }


}
function SendReminderNow(){
$('#processreminder').prop('disabled', true);
var iInvoiceNbr  = $('#reminder_iInvoiceNbr').val();
var iAddressNbr  = $('#reminder_iAddressNbr').val();
var reminder_type = $('#reminder_typex').val();
  $.ajax({
    url: window.location.protocol + '//' + window.location.host + '/admin/invoice/sendInvoice_Reminder',
    type: 'post',
    dataType: "json",
    data: {
     iAddressNbr: iAddressNbr,
     iInvoiceNbr: iInvoiceNbr,
     reminder_type:reminder_type,
     companyid: '<?php echo $this->session->cid; ?>'
    },
    success: function( data ) {
      console.log(data);
      if(data.result){
          window.location.href = window.location.protocol + '//' + window.location.host + '/admin/sepa/';
      }else{
        alert('Error on sending reminder');
      }

    }
   });


}
function Complete(){
  var id = $('#itemidx').val();
  var r = confirm("Are you sure?");
  if(r){
    $('#loader').show();
  $('.text-center').hide();
  $('#complete').prop('disabled', true);
  $('#assign').prop('disabled', true);
  var id = $('#itemidx').val();
  window.location.href = window.location.protocol + '//' + window.location.host + '/admin/sepa/set_complete/'+id;
  }


}
</script>
<div class="modal" id="IssueRefund">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><?php echo lang('Have you refunded the amount below to customer? if yes then proceeed'); ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form>
        <input type="hidden" name="refund_iAddressNbr" id="refund_iAddressNbr">
      <div class="form-group has-success">
        <label class="form-control-label" for="inputSuccess1"><?php echo lang('Amount Refunded'); ?></label>
        <input name="refund_Amount"  type="text" value="" placeholder="" class="form-control is-valid" id="refund_Amount" readonly>
        <span class="text-help"><?php echo lang('Is the amount above ok? otherwise do not proceed'); ?></span>
        </div>

       <div class="form-group has-success">
        <label class="form-control-label" for="inputSuccess1"><?php echo lang('Date Of Refund'); ?></label>
        <input name="refund_Date"  type="text" value="<?php echo date('Y-m-d'); ?>" placeholder="" class="form-control is-valid" id="pickdate1" readonly>
        <span class="text-help"><?php echo lang('Date when you refund it'); ?></span>
        </div>

          <div class="form-group has-success">
        <label class="form-control-label" for="inputSuccess1"><?php echo lang('Description Of Refund'); ?></label>
        <input name="refund_Description"  type="text" value="" placeholder="" class="form-control is-valid" id="refund_Description" required>
        <span class="text-help"><?php echo lang('Give little description for your own needs'); ?></span>
        </div>


        <hr />
        <button type="button" id="refundButton" onclick="IssueRefundNow()" class="btn btn-block btn-primary" disabled><?php echo lang('Mark Refunded'); ?></button>
        <input type="hidden" name="refund_PaymentId" id="refund_PaymentId" value="" required>
        <input type="hidden" name="refund_Used" id="refund_Used" value="" required>
      </form>
        </div>
      </div>
    </div>
  </div>


<div class="modal" id="SendReminder">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><?php echo lang('Send Reminder'); ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form>
        <input type="hidden" name="reminder_iAddressNbr" id="reminder_iAddressNbr">
      <div class="form-group has-success">
        <label class="form-control-label" for="inputSuccess1">Invoice Number</label>
        <input name="reminder_iInvoiceNbr"  type="text" value="" placeholder="" class="form-control is-valid" id="reminder_iInvoiceNbr" readonly>
        </div>
      <div class="form-group has-success">
        <label class="form-control-label" for="inputSuccess1">Invoice Number</label>
       <select name="reminder_type" class="form-control" id="reminder_typex">
         <option value="reminder_1">Reminder 1</option>
         <option value="reminder_2">Reminder 2</option>
       </select>
        </div>
        <hr />
        <button type="button" id="processreminder" onclick="SendReminderNow()" class="btn btn-block btn-primary">Send Reminder</button>
      </form>
        </div>
      </div>
    </div>
  </div>
<div class="modal" id="AssigntoCustomer">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><?php echo lang('Process This Item'); ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <p class="text-center">Please choose what you wish to do with this transaction. if this is proforma <button type="button" class="btn btn-xs btn-prirmary" onclick="proformabtn()">click here</button></p>
          <table class="table table-bordered table-hover">
        <thead>
        <tr>
        <th>ID</th>
        <th>Amount</th>
        <th>Date</th>
        <th>IBAN</th>
        <th>Trx Code</th>
        </tr>
        </thead>
      <tbody id="hasil">
      </tbody>
        </table>
        <div class="assigntable">
        <form>
        <input type="hidden" name="id" id="xitemid" value="">
        <div class="form-group has-success">
        <label class="form-control-label" for="inputSuccess1">MVNO Number (Required)</label>
        <input name="userid" type="text" value="" placeholder="please type MVNO ID to Search" class="xmvnoid form-control is-valid" id="xmvnoid">
        </div>

        <div class="form-group has-success" id="invoicex">
        <label class="form-control-label" for="inputSuccess1">Billing ID</label>
        <input name="iAddressNbr" type="text" value="" placeholder="" class="form-control is-valid" id="xiAddressNbr" readonly>
        </div>
        <div class="form-group has-success">
        <label class="form-control-label" for="inputSuccess1">Customer Name</label>
        <input name="cName" type="text" value="" placeholder="" class="form-control is-valid" id="xcName" readonly>
        </div>
        <div class="form-group has-success">
        <label class="form-control-label" for="inputSuccess1">Amount Paid</label>
        <input name="amount" type="text" value="" placeholder="" class="form-control is-valid" id="xAmountPaid" readonly>
        </div>
        <input type="hidden" name="DatePayment" id="xDatePayment" value="">
       </form>
        </div>

        <center><img src="<?php echo base_url(); ?>assets/img/loader1.gif" id="loaderx" style="display:none;"></center>
      </div>
      <div class="modal-footer">

        <button type="button" onclick="AssignToCustomerProcess()" id="xassign" class="btn btn-primary" disabled>Assign to an Invoice</button>
       <!-- <button type="button" onclick="Complete()" id="xcomplete" class="btn btn-primary">Mark as Completed</button> -->
      </div>
    </div>
  </div>
</div>


<div class="modal" id="ProcessItem">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><?php echo lang('Process This Item'); ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <p class="text-center">Please choose what you wish to do with this transaction. if this is proforma <button type="button" class="btn btn-xs btn-prirmary" onclick="proformabtn()">click here</button></p>
          <table class="table table-bordered table-hover">
        <thead>
        <tr>
        <th>ID</th>
        <th>Amount</th>
        <th>Date</th>
        <th>IBAN</th>
        <th>Trx Code</th>
        </tr>
        </thead>
      <tbody id="hasil">
      </tbody>
        </table>
        <div class="assigntable">
        <form>
        <input type="hidden" name="id" id="itemidx" value="">
        <input type="hidden" name="iAddressNbr" id="iAddressNbr" value="">


        <div class="form-group has-success">
        <label class="form-control-label" for="inputSuccess1">MVNO Number (Optional)</label>
        <input name="userid" type="text" value="" placeholder="please type MVNO ID to Search" class="mvnoid form-control is-valid" id="mvnoid">
        </div>

        <div class="form-group has-success" id="invoicex">
        <label class="form-control-label" for="inputSuccess1">Invoice Number</label>
        <input name="invoicenum" type="text" value="" placeholder="please type invoicenumber" class="invoiceassign form-control is-valid" id="invoicenum">
        </div>
        <div class="form-group has-success">
        <label class="form-control-label" for="inputSuccess1">Customer Name</label>
        <input name="cName" type="text" value="" placeholder="" class="form-control is-valid" id="cName" readonly>
        </div>
        <div class="form-group has-success">
        <label class="form-control-label" for="inputSuccess1">Amount Paid</label>
        <input name="amount" type="text" value="" placeholder="" class="form-control is-valid" id="AmountPaid" readonly>

        </div>
        <div class="form-group has-success">
        <label class="form-control-label" for="inputSuccess1">Invoice Amount</label>
        <input name="amount" type="text" value="" placeholder="" class="form-control is-valid" id="mInvoiceAmount" readonly>

        </div>
        <input type="hidden" name="DatePayment" id="DatePayment" value="">
       </form>
        </div>

        <center><img src="<?php echo base_url(); ?>assets/img/loader1.gif" id="loader" style="display:none;"></center>
      </div>
      <div class="modal-footer">
        <button type="button" onclick="Assign()" id="assign" class="btn btn-primary" disabled>Assign to an Invoice</button>
        <button type="button" onclick="Complete()" id="complete" class="btn btn-primary">Mark as Completed</button>
      </div>
    </div>
  </div>
</div>
<script>
$( document ).ready(function() {
  $('#AssignComplete').click(function(){
    var data = $("#assigmeform").serialize();
    $('#AssignComplete').prop('disabled', true);
     $.ajax({
      url: window.location.protocol + '//' + window.location.host + '/admin/sepa/assignPaymentInvoice',
      type: 'post',
      dataType: "json",
      data:data,
      success: function( data ) {
        if(data.result){
          $('#AssignPaymentLeft').modal('toggle');
          window.location.replace(window.location.protocol + '//' + window.location.host + '/admin/sepa');
         
        }else{
          aler(data.message);
          $('#AssignComplete').prop('disabled', false);
        }
      }
     });


  });
  $('#refund_Description').keyup(function(){
    var ref = $('#refund_Description').val();
    if(ref.length > 5 ){
      $('#refundButton').prop('disabled', false);
    }else{
       $('#refundButton').prop('disabled', true);
    }

  });
  $( "#xmvnoid" ).autocomplete({
    source: function( request, response ) {
     // Fetch data

     $.ajax({
      url: window.location.protocol + '//' + window.location.host + '/admin/complete/searchmvnoidonly',
      type: 'post',
      dataType: "json",
      data: {
       keyword: request.term
      },
      success: function( data ) {
       response( data );
       $('#xassign').prop('disabled', true);
       $('#xcomplete').prop('disabled', true);
      }
     });
    },
    select: function (event, ui) {
      console.log(ui);

   $('#xmvnoid').val(ui.item.value); // display the selected tex
   $('#xcName').val(ui.item.firstname+' '+ui.item.lastname);
   $('#xiAddressNbr').val(ui.item.mageboid);
   $('#xassign').prop('disabled', false);
   $('#xcomplete').prop('disabled', true);
   return false;
    }
   });

$( "#mvnoid" ).autocomplete({
    source: function( request, response ) {
     // Fetch data

     $.ajax({
      url: window.location.protocol + '//' + window.location.host + '/admin/complete/searchmvnoid',
      type: 'post',
      dataType: "json",
      data: {
       keyword: request.term
      },
      success: function( data ) {
       response( data );
       $('#assign').prop('disabled', true);
       $('#complete').prop('disabled', true);
      }
     });
    },
    select: function (event, ui) {
      console.log(ui);
   $('#invoicenum').val(ui.item.invoiceid); // display the selected tex
   $('#cName').val(ui.item.cName);
   $('#mInvoiceAmount').val(ui.item.amount);
   $('#iAddressNbr').val(ui.item.iAddressNbr);
   $('#assign').prop('disabled', false);
   $('#complete').prop('disabled', true);
   return false;
    }
   });
  });
   </script>
<div class="modal" id="UploadBankfile">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><?php echo lang('Upload New Bank File'); ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <?php echo form_open_multipart('admin/sepa/upload_bankfile'); ?>
        <div class="form-group">
        <label class="label-control"></label>
        <input type="file" class="form-control" name="userfile" size="20" />
        </div>
        <button class="btn btn-primary btn-md" type="submit">Upload</button>
        <?php echo form_close(); ?>
      </div>
      </div>
      </div>
      </div>

<div class="modal" id="AssignPaymentLeft">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><?php echo lang('Assign Payments'); ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <form id="assigmeform">
         <div class="form-group has-success">
        <label class="form-control-label" for="inputSuccess1">Payment ID</label>
        <input name="PLiPaymentNbr" type="text" value="" class="form-control" id="PLiPaymentNbr" required readonly>
        </div>
        <div class="form-group has-success">
        <label class="form-control-label" for="inputSuccess1">BILLING ID</label>
        <input name="PLiAddressNbr" type="text" value=""  class="form-control" id="PLiAddressNbr" required readonly>
        </div>
          <div class="form-group has-success">
        <label class="form-control-label" for="inputSuccess1">Customer</label>
        <input name="PLcName" type="text" value=""  class="form-control" id="PLcName" required readonly>
        </div>

         <div class="form-group has-success">
        <label class="form-control-label" for="inputSuccess1">Amount</label>
        <input name="PLAmountPaid" type="text" value=""  class="form-control" id="PLAmountPaid" required readonly>
        </div>

         <div class="form-group has-success">
        <label class="form-control-label" for="inputSuccess1">INVOICE</label>
        <select name="PLiInvoiceNbr" class="form-control" id="PLiInvoiceNbr">

        </select>
        </div>

        <button class="btn btn-primary" type="button" id="AssignComplete">Assign Payment</button>
      </form>
      </div>
      </div>
      </div>
      </div>


<div class="modal" id="proforma">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><?php echo lang('Process Proforma'); ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <?php echo form_open_multipart('admin/sepa/process_proforma'); ?>
      <div class="modal-body">
        <div class="form-group has-success">
        <label class="form-control-label" for="inputSuccess1">Payment ID</label>
        <input name="userid" type="text" value="" class="form-control" id="paymentid" required readonly>
        </div>
        <div class="form-group has-success" id="invoicex">
        <label class="form-control-label" for="inputSuccess1">Amount</label>
        <input name="invoicenum" type="text" value=""  class="form-control" id="paymentamount" required readonly>
        </div>
         <div class="form-group has-success">
        <label class="form-control-label" for="inputSuccess1">Proforma Invoice</label>
        <input name="userid" type="text" value="" class="form-control" id="proformaid" required>
        </div>
        
         <div class="form-group has-success">
        <label class="form-control-label" for="inputSuccess1">Payment Message</label>
        <input name="userid" type="text" value="" class="form-control" id="paymentmsg" readonly>
        </div>

          <div class="form-group has-success">
        <label class="form-control-label" for="inputSuccess1">Date Transaction</label>
        <input name="userid" type="text" value="" class="form-control" id="date_transaction" readonly>
        </div>
      </div>
       <div class="modal-footer">
        <button type="button" onclick="AssignProforma()" id="assign" class="btn btn-primary">Assign to Proforma Invoice</button>
      </div>
        <?php echo form_close(); ?>
      </div>
      </div>
      </div>

