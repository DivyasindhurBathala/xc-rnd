<ul class="main-menu">
  <li class="sub-header">
    <span>
      <?php echo lang('Main Menu'); ?></span>
  </li>
  <li class="selected">
    <a class="showprogress" href="<?php echo base_url(); ?>admin">
      <div class="icon-w">
        <div class="os-icon os-icon-layout"></div>
      </div>
      <span>
        <?php echo lang('Dashboard'); ?></span>
    </a>
  </li>
  <li class="has-sub-menu">
    <a href="#">
      <div class="icon-w">
        <div class="os-icon os-icon-users"></div>
      </div>
      <span>
        <?php echo lang('Clients'); ?></span>
    </a>
    <div class="sub-menu-w">
      <div class="sub-menu-header">
        <?php echo lang('Clients'); ?>
      </div>
      <div class="sub-menu-icon">
        <i class="os-icon os-icon-users"></i>
      </div>
      <div class="sub-menu-i">
        <ul class="sub-menu">
          <li>
            <a class="showprogress"
              href="<?php echo base_url(); ?>admin/client">
              <?php echo lang('Client List'); ?></a>
          </li>
          <li>
            <a class="showprogress"
              href="<?php echo base_url(); ?>admin/client/add">
              <?php echo lang('Add New Clients'); ?></a>
          </li>
          <li>
            <a class="showprogress"
              href="<?php echo base_url(); ?>admin/client/list_all_notes">
              <?php echo lang('List Client Notes'); ?></a>
          </li>
        </ul>
        <ul class="sub-menu">


        </ul>


      </div>
    </div>
  </li>
  <li>
    <a href="<?php echo base_url(); ?>admin/agent">
      <div class="icon-w">
        <div class="os-icon os-icon-users"></div>
      </div>
      <span>
        <?php echo lang('Agents/Reseller'); ?></span>
    </a>
  </li>
  <?php if ($setting->mage_invoicing) {
    ?>
  <li class="has-sub-menu">
    <a href="#">
      <div class="icon-w">
        <div class="os-icon os-icon-credit-card"></div>
      </div>
      <span>
        <?php echo lang('Billing'); ?></span>
    </a>
    <div class="sub-menu-w">
      <div class="sub-menu-header">
        <?php echo lang('Billing'); ?>
      </div>
      <div class="sub-menu-icon">
        <i class="os-icon os-icon-credit-card"></i>
      </div>
      <div class="sub-menu-i">
        <ul class="sub-menu">

          <li>
            <a class="showprogress"
              href="<?php echo base_url(); ?>admin/invoice">
              <?php echo lang('Invoice List'); ?></a>
          </li>
          <?php if ($setting->proforma_invoice) {
        ?>
          <li>
            <a class="showprogress"
              href="<?php echo base_url(); ?>admin/proforma">
              <?php echo lang('Proforma Invoice List'); ?></a>
          </li>
          <?php
    } ?>
          <li>
            <a class="showprogress"
              href="<?php echo base_url(); ?>admin/creditnote">
              <?php echo lang('Creditnote List'); ?></a>
          </li>
        </ul>
        <ul class="sub-menu">
          <li>
            <a class="showprogress"
              href="<?php echo base_url(); ?>admin/sepa"> <?php echo lang('Bank Transactions'); ?></a>
          </li>

          <li>
            <a class="showprogress"
              href="<?php echo base_url(); ?>admin/invoice/online_transaction"><?php echo lang('Online Transactions'); ?></a>
          </li>
          <li>
            <a class="showprogress"
              href="<?php echo base_url(); ?>admin/invoice/invoice_reminders">
              <?php echo lang('Invoice Reminder Logs'); ?></a>
          </li>
          <!--
                <li>
                  <a href="<?php echo base_url(); ?>admin/quote">Quotes
          List</a>
  </li>
  <li>
    <a href="<?php echo base_url(); ?>admin/purchases">Payment
      Invoices</a>
  </li>
  -->
</ul>
</div>
</div>
</li>
<?php
}?>
<li class="has-sub-menu">
  <a href="#">
    <div class="icon-w">
      <div class="os-icon os-icon-package"></div>
    </div>
    <span>
      <?php echo lang('Services'); ?></span>
  </a>
  <div class="sub-menu-w">
    <div class="sub-menu-header">
      <?php echo lang('Services'); ?>
    </div>
    <div class="sub-menu-icon">
      <i class="os-icon os-icon-package"></i>
    </div>
    <div class="sub-menu-i">
      <ul class="sub-menu">
        <li>
          <a class="showprogress"
            href="<?php echo base_url(); ?>admin/subscription">
            <?php echo lang('Services List'); ?></a>
        </li>
        <li>
          <a class="showprogress"
            href="<?php echo base_url(); ?>admin/subscription/Suspended">
            <?php echo lang('Suspended Services'); ?></a>
        </li>
        <li>
          <a
            href="<?php echo base_url(); ?>admin/subscription/add_mobile">
            <?php echo lang('Create New Service Mobile'); ?></a>
        </li>
        <!-- 
           <?php if ($this->session->cid == 2) {
        ?>
        <li>
          <a
            href="<?php echo base_url(); ?>admin/subscription/add_internet">
            <?php echo lang('Create New Service xDSL'); ?></a>
        </li>
        <?php
    } ?>
        -->

        <li>
          <a class="showprogress"
            href="<?php echo base_url(); ?>admin/subscription/termination_service_list/completed">
            <?php echo lang('Terminated Services'); ?></a>
        </li>
        <li>
          <a class="showprogress"
            href="<?php echo base_url(); ?>admin/subscription/bundle_usage_logs">
            <?php echo lang('Bundle Usage Monitor'); ?></a>
        </li>
      </ul>
      <ul class="sub-menu">

        <?php if ($setting->mobile_platform == "ARTA") {
        ?>
        <li>
          <a class="showprogress"
            href="<?php echo base_url(); ?>admin/subscription/pendingporting">
            <?php echo lang('Pending PortIn Requests'); ?></a>
        </li>
        <li>
          <a class="showprogress"
            href="<?php echo base_url(); ?>admin/subscription/pending_porting_out">
            <?php echo lang('Pending PortOut Requests'); ?></a>
        </li>
        <li>
          <a class="showprogress"
            href="<?php echo base_url(); ?>admin/subscription/pendingorders">
            <?php echo lang('Pending Orders'); ?></a>
        </li>
        <li>
          <a class="showprogress"
            href="<?php echo base_url(); ?>admin/subscription/porting_on_demand">
            <?php echo lang('Porting On Demand'); ?></a>
        </li>
        <?php if ($setting->enable_sum) {
            ?>
        <li>
          <a class="showprogress"
            href="<?php echo base_url(); ?>admin/subscription/sum_reports">
            <?php echo lang('SUM Reports'); ?></a>
        </li>
        <?php
        } ?>
        <?php
    } else {
        ?>
  <li>
          <a class="showprogress"
            href="<?php echo base_url(); ?>admin/subscription/Teum_list_sim_pool">
            <?php echo lang('Simcard Pool'); ?></a>
        </li>

      <?php
    } ?>
      </ul>
    </div>
  </div>
</li>
<?php if ($setting->whmcs_ticket) {
        ?>
<li>
  <a href="<?php echo base_url(); ?>admin/helpdesk">
    <div class="icon-w">
      <div class="os-icon os-icon-life-buoy"></div>
    </div>
    <span>
      <?php echo lang('Helpdesk'); ?></span>
  </a>
</li>
<?php
    }?>

<!--
  <li>
    <a class="showprogress" href="<?php echo base_url(); ?>admin/sepa/">
<div class="icon-w">
  <div class="os-icon os-icon-user"></div>
</div>
<span>
  <?php echo lang('SEPA From Bank'); ?></span>
</a>
</li>


<li>
  <a
    href="<?php echo base_url(); ?>admin/subscription/sim_replacement">
    <div class="icon-w">
      <div class="os-icon os-icon-list"></div>
    </div>
    <span>
      <?php echo lang('Swap Request'); ?></span>
  </a>
</li>
-->
<li>
  <a href="<?php echo base_url(); ?>admin/dashboard/logs">
    <div class="icon-w">
      <div class="os-icon os-icon-zap"></div>
    </div>
    <span>
      <?php echo lang('Logs'); ?></span>
  </a>
</li>
<!--
                  <li>
                    <a href="<?php echo base_url(); ?>admin/calendar">
<div class="icon-w">
  <div class="os-icon os-icon-calendar"></div>
</div>
<span><?php echo lang('Calendar'); ?></span></a>
</li>
-->
<?php if ($this->session->role == "superadmin") {
        ?>
<li class="sub-header">
  <span>
    <?php echo lang('Configuration'); ?></span>
</li>

<li class=" has-sub-menu">
  <a href="#">
    <div class="icon-w">
      <div class="os-icon os-icon-life-buoy"></div>
    </div>
    <span>
      <?php echo lang('Setting'); ?></span>
  </a>
  <div class="sub-menu-w">
    <div class="sub-menu-header">
      <?php echo lang('Setting'); ?>
    </div>
    <div class="sub-menu-icon">
      <i class="os-icon os-icon-life-buoy"></i>
    </div>
    <div class="sub-menu-i">
      <ul class="sub-menu">
        <!--  <li>
                                <a href="uikit_modals.html">Modals <strong class="badge badge-danger"><?php echo lang('New'); ?></strong></a>
</li>
-->
<li>
  <a href="<?php echo base_url(); ?>admin/setting/configuration">
    <?php echo lang('General Setting'); ?></a>
</li>

<li>
  <a href="<?php echo base_url(); ?>admin/setting/welcome_template">
    <?php echo lang('PDF Documents Template'); ?></a>
</li>

<li>
  <a href="<?php echo base_url(); ?>admin/setting/email_templates">
    <?php echo lang('Email Templates'); ?></a>
</li>

<li>
  <a href="<?php echo base_url(); ?>admin/setting/api">
    <?php echo lang('API Tokens / IPs'); ?></a>
</li>
<li>
  <a href="<?php echo base_url(); ?>admin/setting/promotions">
    <?php echo lang('Promotions & Discounts'); ?></a>
</li>


</ul>
<ul class="sub-menu">
  <li>
    <a href="<?php echo base_url(); ?>admin/setting/staf">
      <?php echo lang('Staff'); ?></a>
  </li>
  <?php if ($this->session->email == "simson.parlindungan@united-telecom.be") {
            ?>

  <li>
    <a href="<?php echo base_url(); ?>admin/setting/backup_database">
      <?php echo lang('Backup'); ?></a>
  </li>

  <?php
        } ?>

  <?php if ($setting->whmcs_ticket) {
            ?>
  <li>
    <a
      href="<?php echo base_url(); ?>admin/setting/support_department">
      <?php echo lang('Support Department'); ?></a>
  </li>
  <?php
        } ?>
  <li>
    <a href="<?php echo base_url(); ?>admin/setting/roles">
      <?php echo lang('Roles/Permissions'); ?></a>
  </li>
<!--
  <li>
    <a href="<?php echo base_url(); ?>admin/setting/send_mass_sms">
      <?php echo lang('Mass SMS'); ?></a>
  </li>
      -->
  <li>
    <a href="<?php echo base_url(); ?>admin/setting/translations">
      <?php echo lang('Translations'); ?></a>
  </li>

</ul>
</div>
</div>
</li>
<?php
    } ?>
<?php if ($this->session->master) {
        ?>
<li class="sub-header">
  <span>
    <?php echo lang('Master Tools'); ?></span>
</li>

<li>
  <a class="showprogress"
    href="<?php echo base_url(); ?>master/setting/companies">
    <div class="icon-w">
      <div class="os-icon os-icon-home"></div>
    </div>
    <span><?php echo lang('Companies'); ?></span>
  </a>
</li>
<?php if (strpos($this->session->email, "@united-telecom.be")) {
            ?>
<li>
  <a class="showprogress"
    href="<?php echo base_url(); ?>admin/sepa/sepa_directdebit">
    <div class="icon-w">
      <div class="os-icon os-icon-user"></div>
    </div>
    <span>
      <?php echo lang('SEPA To Bank'); ?></span>
  </a>
</li>
<li>
  <a class="showprogress"
    href="<?php echo base_url(); ?>admin/super/change_service_list">
    <div class="icon-w">
      <div class="os-icon os-icon-user"></div>
    </div>
    <span>
      <?php echo lang('Packages Changes'); ?></span>
  </a>
</li>
<li>

  <a class="showprogress"
    href="<?php echo base_url(); ?>admin/super/termination_service_list">
    <div class="icon-w">
      <div class="os-icon os-icon-star"></div>
    </div>
    <span>
      <?php echo lang('Termination List'); ?></span>
  </a>
</li>


<li>
  <a class="showprogress"
    href="<?php echo base_url(); ?>admin/masterdb">
    <div class="icon-w">
      <div class="os-icon os-icon-user"></div>
    </div>
    <span>
      <?php echo lang('Master DB Untility'); ?></span>
  </a>
</li>

<li>
  <a class="showprogress" href="<?php echo base_url(); ?>admin/voip">
    <div class="icon-w">
      <div class="os-icon os-icon-zap"></div>
    </div>
    <span>
      <?php echo lang('VOIP'); ?></span>
  </a>
</li>
<?php
        } ?>
<?php
    }?>





<?php if ($this->session->email == "simson.parlindungan@united-telecom.be") {
        ?>
<li>
  <a class="showprogress"
    href="<?php echo base_url(); ?>admin/super/sync">
    <div class="icon-w">
      <div class="os-icon os-icon-zap"></div>
    </div>
    <span>
      <?php echo lang('Sync Source'); ?></span>
  </a>
</li>
<li>
  <a class="showprogress"
    href="<?php echo base_url(); ?>master/setting/create_configuration">
    <div class="icon-w">
      <div class="os-icon os-icon-zap"></div>
    </div>
    <span>
      <?php echo lang('Global Configuration'); ?></span>
  </a>
</li>




<?php
    } ?>

</ul>