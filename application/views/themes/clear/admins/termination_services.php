<div class="content-i">
    <div class="content-box">
        <div class="element-wrapper">
            <h6 class="element-header">
            <?php echo lang('Termination List'); ?>
            </h6>
            <div class="element-box">
                <h5 class="form-header">
    <?php if (!$c) { ?>
                <?php if (empty($this->uri->segment(4))) {?>
                    <?php echo lang('Termination List Pending'); ?>
                <div class="float-right"><a href="<?php echo base_url(); ?>admin/super/termination_service_list/completed" class="btn btn-danger">Show Completed</a></div>
                <?php } else {?>
                <?php echo lang('Termination List Completed'); ?>
                <div class="float-right"><a href="<?php echo base_url(); ?>admin/super/termination_service_list" class="btn btn-danger">Show Pending</a></div>
                <?php }?>
    <?php }?>   
                </h5>
                <div class="table-responsive">
                    <table class="table table-striped table-lightfont" id="packages">
                        <thead>
                            <tr>
                                <th><?php echo lang('#Serviceid'); ?></th>
                                <th><?php echo lang('Product'); ?></th>
                                <th><?php echo lang('Customer'); ?></th>
                                <th><?php echo lang('Identifier'); ?></th>
                                <th><?php echo lang('Recurring'); ?></th>
                                <th><?php echo lang('Date Termination'); ?></th>
                                <th>Termination Status</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>



<script>
<?php if (empty($this->uri->segment(4))) {?>
$(document).ready(function()
{
$.getJSON(window.location.protocol + '//' + window.location.host + '/admin/complete/get_lang', function(data) {
var x = new Date();
var y = x.getFullYear().toString();
var m = (x.getMonth() + 1).toString();
var d = x.getDate().toString();
(d.length == 1) && (d = '0' + d);
(m.length == 1) && (m = '0' + m);
var date = y +'-'+ m +'-'+ d;
console.log(date);
$('#packages').DataTable({
"autoWidth": false,
"processing": true,
"orderCellsTop": true,
"ordering": true,
"serverSide": true,
"ajax": window.location.protocol + '//' + window.location.host + '/admin/table/get_termination_list',
"aaSorting": [[1, 'desc']],
"language": {
"url": window.location.protocol + '//' + window.location.host +"/assets/clear/js/datatables/lang/" + data.result + ".json"
},
"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
//$('td:eq(0)', nRow).html('<a href="'+window.location.protocol + '//' + window.location.host + '/admin/subscription/detail/'+aData[0]+'">'+aData[0]+'</a>');
//$('td:eq(3)', nRow).html('<a href="'+window.location.protocol + '//' + window.location.host + '/admin/client/detail/'+aData[7]+'">'+aData[3]+'</a>');
$('td:eq(6)', nRow).html('<button class="btn btn-primary" onclick="SetCompleteT(\''+aData[0]+'\');">Set Completed</button>');
return nRow;
},
});
});
});
<?php } else {?>
    $(document).ready(function()
{
$.getJSON(window.location.protocol + '//' + window.location.host + '/admin/complete/get_lang', function(data) {
var x = new Date();
var y = x.getFullYear().toString();
var m = (x.getMonth() + 1).toString();
var d = x.getDate().toString();
(d.length == 1) && (d = '0' + d);
(m.length == 1) && (m = '0' + m);
var date = y +'-'+ m +'-'+ d;
console.log(date);
$('#packages').DataTable({
"autoWidth": false,
"processing": true,
"orderCellsTop": true,
"ordering": true,
"serverSide": true,
"ajax": window.location.protocol + '//' + window.location.host + '/admin/table/get_termination_list/completed',
"aaSorting": [[1, 'desc']],
"language": {
"url": window.location.protocol + '//' + window.location.host +"/assets/clear/js/datatables/lang/" + data.result + ".json"
},
"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
$('td:eq(0)', nRow).html('<a href="'+window.location.protocol + '//' + window.location.host + '/admin/subscription/detail/'+aData[0]+'">'+aData[0]+'</a>');
$('td:eq(3)', nRow).html('<a href="'+window.location.protocol + '//' + window.location.host + '/admin/client/detail/'+aData[7]+'">'+aData[3]+'</a>');
$('td:eq(6)', nRow).html('Completed ');
return nRow;
},
});
});
});
<?php }?>
</script>
<script>


    function SetCompleteT(id){
        console.log(id);
        $('#procidtermination').val(id);
        $('#confirm_termination_done').modal('toggle');
    }
    function confirm_doT(){

        var id = $('#procidtermination').val();
        console.log(id);
        $.ajax({
    url: window.location.protocol + '//' + window.location.host + '/admin/super/terminate_apackage_do',
    dataType: 'json',
    type: 'post',
    contentType: 'application/json',
    data: JSON.stringify( { "id": id } ),
    processData: false,
    success: function( data, textStatus, jQxhr ){
    
        window.location.href = window.location.protocol + '//' + window.location.host + '/admin/super/termination_service_list/';

    },
    error: function( jqXhr, textStatus, errorThrown ){
        alert( errorThrown );
    }
    });
    }
    </script>


<div class="modal fade" id="confirm_termination_done">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <form id="cp">

        <input type="hidden" name="id" id="procidtermination" value="">
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title"><div id="number_cancel"></div></h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <!-- Modal body -->
        <div class="modal-body">
          <div class="table-responsive">
            <?php echo lang('Are you sure that you have done required action in Magebo and Platform?, please dont do this action if you are not sure'); ?>
          </div>
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button"  onclick="confirm_doT();" class="btn btn-primary"><?php echo lang('Set request Complete'); ?></button>
        </div>
      </form>
    </div>
  </div>
</div>


