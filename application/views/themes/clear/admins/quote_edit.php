<div class="content-i">
  <div class="content-box">
    <div class="element-wrapper">
      <h6 class="element-header">
      Client List
      </h6>
      <div class="element-box">
        <h5 class="form-header">
        Modify Quote Customer : <?php echo $client->firstname . ' ' . $client->lastname . ' - ' . $client->companyname; ?>
        </h5>
        <div class="table-responsive">
          <form method="post" action="<?php echo base_url(); ?>admin/quote/edit/<?php echo $quote['id']; ?>">
            <input type="hidden" name="userid" value="<?php echo $client->id; ?>">
            <input type="hidden" id="quoteid" name="quoteid" value="<?php echo $quote['id']; ?>">
            <div class="form-group has-success">
              <div class="row">
                <div class="col-sm-12">
                  <div class="form-group">
                    <label for="tax"><?php echo lang('Subject'); ?></label>
                    <input type="text"  id="subject" name="subject" class="form-control"  value="<?php echo $quote['subject']; ?>" required>
                  </div>
                </div>
              </div>
            </div>
            <div class="form-group has-success">
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="tax">Choose VAT Percentage</label>
                    <select class="form-control" id="tax" name="taxrate">
                      <option value="0"<?php if ($quote['items'][0]['taxrate'] == "O") {?> selected<?php }?>>0%</option>
                      <option value="6"<?php if ($quote['items'][0]['taxrate'] == "6") {?> selected<?php }?>>6%</option>
                      <option value="21"<?php if ($quote['items'][0]['taxrate'] == "21") {?> selected<?php }?>>21%</option>
                    </select>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="tax">Pick Duedate</label>
                    <input type="text" value="<?php echo $quote['duedate']; ?>" name="duedate" class="form-control is-valid" id="pickdate">
                  </div>
                </div>
              </div>
            </div>
            <hr />
            <?php foreach ($quote['items'] as $key => $row) {?>
            <?php if ($key <= 0) {?>
            <div class="form-group has-success">
              <div class="row">
                <div class="col-sm-6">
                  <label class="form-control-label" for="inputSuccess1"><strong>Description</strong></label>
                  <textarea class="form-control ui-autocomplete-input ui-autocomplete-loading products" autocomplete="off" id="description<?php echo $key; ?>" style="margin-top: 0px; margin-bottom: 0px; height: 36px;" name="line[<?php echo $key; ?>][description]"><?php echo $row['description']; ?></textarea>
                </div>
                <div class="col-sm-1">
                  <label class="form-control-label" for="inputSuccess1">Qty</label>
                  <input type="text" value="<?php echo $row['qty']; ?>" name="line[<?php echo $key; ?>][qty]" class="form-control is-valid" id="qty<?php echo $key; ?>">
                </div>
                <div class="col-sm-2">
                  <label class="form-control-label" for="inputSuccess1">Price / pcs</label>
                  <input type="text" value="<?php echo $row['price']; ?>" name="line[<?php echo $key; ?>][pcs]" class="form-control is-valid" id="pcs<?php echo $key; ?>">
                </div>
                <div class="col-sm-2">
                  <label class="form-control-label" for="inputSuccess1">Total</label>
                  <input type="text" value="<?php echo $row['total']; ?>" name="line[<?php echo $key; ?>][total]" class="form-control is-valid" id="total<?php echo $key; ?>">
                </div>
                <div class="col-sm-1">
                  <label class="form-control-label" for="inputSuccess1">Delete</label>
                </div>
              </div>
            </div>
            <?php } else {?>
            <div class="form-group has-success" id="deleteme<?php echo $key; ?>">
              <div class="row">
                <div class="col-sm-6">
                  <textarea class="form-control ui-autocomplete-input ui-autocomplete-loading products" autocomplete="off" id="description<?php echo $key; ?>" style="margin-top: 0px; margin-bottom: 0px; height: 36px;" name="line[<?php echo $key; ?>][description]"><?php echo $row['description']; ?></textarea>
                </div>
                <div class="col-sm-1">
                  <input type="text" value="<?php echo $row['qty']; ?>" name="line[<?php echo $key; ?>][qty]" class="form-control is-valid" id="qty<?php echo $key; ?>">
                </div>
                <div class="col-sm-2">
                  <input type="text" value="<?php echo $row['price']; ?>" name="line[<?php echo $key; ?>][pcs]" class="form-control is-valid" id="pcs<?php echo $key; ?>">
                </div>
                <div class="col-sm-2">
                  <input type="text" value="<?php echo $row['total']; ?>" name="line[<?php echo $key; ?>][total]" class="form-control is-valid" id="total<?php echo $key; ?>">
                </div>
                <div class="col-sm-1">
                  <i class="os-icon os-icon-trash btn btn-md btn-danger remove" id="removeid<?php echo $key; ?>"></i>
                </div>
              </div>
            </div>
            <?php }?>
            <?php }?>
            <div class="form-group has-success field"  id="deleteme<?php echo $count; ?>">
              <div class="row">
                <div class="col-sm-6">
                  <textarea id="description<?php echo $count; ?>" style="margin-top: 0px; margin-bottom: 0px; height: 36px;" class="products form-control ui-autocomplete-input ui-autocomplete-loading" autocomplete="off" name="line[<?php echo $count; ?>][description]"></textarea>
                </div>
                <div class="col-sm-1">
                  <input type="text" value="1" name="line[<?php echo $count; ?>][qty]"  class="form-control is-valid" id="qty<?php echo $count; ?>">
                </div>
                <div class="col-sm-2">
                  <input type="text" value="" name="line[<?php echo $count; ?>][pcs]"  class="form-control is-valid" id="pcs<?php echo $count; ?>">
                </div>
                <div class="col-sm-2">
                  <input type="text" value=""  name="line[<?php echo $count; ?>][total]" class="form-control is-valid" id="total<?php echo $count; ?>">
                </div>
                <div class="col-sm-1">
                  <i class="os-icon os-icon-trash btn-md btn btn-danger remove" id="removeid<?php echo $count; ?>"></i>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <label class="form-control-label" for="inputSuccess1"><strong>FooterText</strong></label>
                  <textarea id="description<?php echo $count; ?>" class="form-control" name="footertext" rows="10" placeholder="Enter text here to be appear on the bottom of your quote"><?php echo $quote['footertext']; ?></textarea>
                </div>
              </div>
            </div>
            <div class="form-group has-success field">
              <div class="row">
                <div class="col-sm-6">
                  <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-save"></i> Save Changes</button>
                </div>
                <div class="col-sm-6">
                  <a target="_blank" class="btn btn-success btn-block" href="<?php echo base_url(); ?>admin/quote/download/<?php echo $quote['id']; ?>"><i class="fa fa-print"></i> Print Quotes</a>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<div id="fade"></div>
<div id="modal" class="modal">
  <img id="loader" src="<?php echo base_url(); ?>assets/img/lg.rotating-balls-spinner.gif" />
</div>
<!--
vat = 21.5;
//The price, excluding VAT.
$priceExcludingVat = 10;
//Calculate how much VAT needs to be paid.
$vatToPay = ($priceExcludingVat / 100) * $vat;
//The total price, including VAT.
$totalPrice = $priceExcludingVat + $vatToPay;
//Print out the final price, with VAT added.
//Format it to two decimal places with number_format.
echo number_format($totalPrice, 2);
-->
<script type="text/javascript">
$(document).ready(function() {
    $('.remove').on('click', function() {
        var removeto = $(this).attr('id');
        var nomor = removeto.replace(/\D/g, '');
        $("#deleteme" + nomor).remove();
    });
    $('input').on('change', function() {
        var id = $(this).attr('id');
        var nomor = id.replace(/\D/g, '');
        var qty = $('#qty' + nomor).val();
        var price = $('#pcs' + nomor).val();
        var tax = $("#tax option:selected").val();
        var total = qty * price;
        $("#total" + nomor).val(total);
        console.log(qty + ' ' + price);
    });
    $("#tax").change(function() {});
});
</script>
<script>
$(function() {
    $("#pickdate").datepicker({
        dateFormat: 'yy-mm-dd'
    });
    $("#pickdate2").datepicker({
        dateFormat: 'yy-mm-dd'
    });
    $("#pickdate3").datepicker({
        dateFormat: 'yy-mm-dd'
    });
    $("#pickdate4").datepicker({
        dateFormat: 'yy-mm-dd',
        minDate: 0
    });
    $("#pickdate5").datepicker({
        dateFormat: 'yy-mm-dd',
        minDate: 0
    });
    $("#pickdate6").datepicker({
        dateFormat: 'yy-mm-dd',
        minDate: 0
    });
});
</script>
<script type="text/javascript">
function confirmation() {
    $("#publish").prop('disabled', true);
    var answer = confirm("Make sure you have saved the current draft, are you sure?")
    if (answer) {
        var invoiceid = $("#invoiceid").val();
        openModal(function() {
            //do something special
        }, 5000);
        $.ajax({
            url: window.location.protocol + '//' + window.location.host + '/admin/invoice/publish',
            type: 'post',
            dataType: 'json',
            data: {
                invoiceid: invoiceid
            },
            success: function(data) {
                console.log(data);
                if (data.result) {
                    window.location.replace(window.location.protocol + '//' + window.location.host + '/admin/invoice/detail/' + invoiceid);
                }
                closeModal();
            },
        });
    } else {
        console.log("hello ");
        $("#publish").prop('disabled', false);
    }
}

function openModal() {
    document.getElementById('modal').style.display = 'block';
    document.getElementById('fade').style.display = 'block';
}

function closeModal() {
    document.getElementById('modal').style.display = 'none';
    document.getElementById('fade').style.display = 'none';
}
</script>