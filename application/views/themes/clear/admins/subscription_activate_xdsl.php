<div class="content-i">
  <div class="content-box">
    <div class="element-wrapper">
      <h6 class="element-header">
      <?php echo lang('Services Summary'); ?> [<a href="<?php echo base_url(); ?>admin/client/detail/<?php echo $service->userid; ?>"><?php echo lang('Back to Client Details'); ?></a>]
      </h6>
      <div class="element-box">
        <h5 class="form-header">
        </h5>
        <div class="table-responsive">
          <div class="row">
            <div class="col-sm-4">
              <table class="table table-striped table-hover" width="100%">
                 <tr>
                  <td>
                    <?php echo lang('Circuitid'); ?>
                  </td>
                  <td align="right"><strong>
                  <?php echo $service->domain; ?></strong></td>
                </tr>
                <tr>
                  <td>
                    <?php echo lang('Product Brand'); ?>
                  </td>
                  <td align="right"><strong>
                  <?php echo $service->product_brand; ?></strong></td>
                </tr>
                <tr>
                  <td>
                    <?php echo lang('Service ID'); ?>
                  </td>
                  <td align="right"><strong>
                  <?php echo $service->id; ?></strong></td>
                </tr>
                <tr>
                  <td>
                    <?php echo lang('Date Reg'); ?>
                  </td>
                  <td align="right"><strong>
                  <?php echo $service->date_created; ?></strong></td>
                </tr>
                <tr>
                  <td>
                    <?php echo lang('Product Name'); ?>
                  </td>
                  <td align="right"><strong>
                  <?php echo $service->packagename; ?></strong></td>
                </tr>
                <tr>
                  <td>
                    <?php echo lang('Recurring'); ?>
                  </td>
                  <td align="right"><strong>&euro;
                  <?php echo number_format($service->recurring, 2); ?></strong></td>
                </tr>
                <tr>
                  <td>
                    <?php echo lang('Billingcycle'); ?>
                  </td>
                  <td align="right"><strong>
                  <?php echo lang($service->billingcycle); ?></strong></td>
                </tr>
                <tr>
                  <td>
                    <?php echo lang('Proximus Orderid'); ?>
                  </td>
                  <td align="right"><strong>
                  <?php echo lang($service->proximusid); ?></strong></td>
                </tr>
                <tr>
                  <td>
                    <?php echo lang('Status'); ?>
                  </td>
                  <td align="right" class="text-success"><strong>
                  <?php echo lang($service->orderstatus); ?></strong></td>
                </tr>
                <?php if ($service->promocode) {
    ?>
                <tr>
                  <td>
                    <?php echo lang('Promocode'); ?>T<a href="#" onclick="removePromotion();"><i class="fa fa-trash text-danger"></i></a>
                  </td>
                  <td align="right"><strong>
                  <?php echo getPromoname($service->promocode); ?></strong></td>
                </tr>
                <?php
}?>
                <tr>
                  <td>
                    <?php echo lang('Contract date'); ?>
                  </td>
                  <td align="right"><strong>
                  <?php echo convert_contract($service->date_contract); ?></strong></td>
                </tr>
                <tr>
                  <td>
                    <?php echo lang('BillingID'); ?>
                  </td>
                  <td align="right"><strong>
                  <?php echo $client->mageboid; ?></strong></td>
                </tr>
                  <?php if ($service->details->bussiness_pack) {
        ?>
                    <tr>
                  <td>
                    <?php echo lang('PPPOE Username'); ?>
                  </td>
                  <td align="right" class="text-success">
                  <strong><?php echo $service->details->fixed_ip; ?></strong></td>
                </tr>

                  <?php
    } ?>
                

                 <tr>
                  <td>
                    <?php echo lang('PPPOE Username'); ?>
                  </td>
                  <td align="right" class="text-success"><strong>
                  <?php echo $service->details->pppoe_username; ?>@<?php echo $service->details->realm; ?></strong></td>
                </tr>
                 <tr>
                  <td>
                    <?php echo lang('PPPOE Password'); ?>
                  </td>
                  <td align="right"  class="text-success"><strong>
                  <?php echo $service->details->pppoe_password; ?></strong></td>
                </tr>
                <tr>
                  <td>
                    <?php echo lang('iGeneralPricing'); ?>
                  </td>
                  <td align="right"><strong>
                  <?php echo $service->iGeneralPricingIndex; ?></strong></td>
                </tr>
              </table>
            </div>
            <div class="col-sm-8">
              <h4>
                Proforma Invoices
             </h4>
               <div class="float-right text-success"><small><i>This page reloaded every 30 second to get latest update from proximus</i></small></div>
              <table class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>Invoice Number</th>
                    <th>Invoice Amount</th>
                    <th>Invoice Date</th>
                    <th>Invoice Status</th>

                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td><?php echo $invoice->invoicenum; ?></td>
                    <td>€<?php echo $invoice->invoicenum; ?></td>
                    <td><?php echo $invoice->date; ?></td>
                    <td><?php echo $invoice->status; ?></td>
                  </tr>

                </tbody>
              </table>
              <center>
                 <?php if (!empty($service->details->product_name)) {
        ?>
                <button class="btn btn-sm btn-primary" onclick="executeProximus()"<?php if ($service->orderstatus != "Pending") {
            ?> disabled<?php
        } ?>>Execute Proximus Order</button>
                <?php
    } else {
        ?>
                 <button class="btn btn-sm btn-primary" onclick="assign_proximus()">Assign Proximus Product</button>
               <?php
    } ?>
                 <button class="btn btn-sm btn-primary" onclick="generateNewProximusId()" disabled>Regenerate Order</button>
                <button class="btn btn-sm btn-primary" onclick="cancel_order();">Cancel Proximus Order</button>
              </center>
              <hr />
                <h4>Installation Data</h4>
              <table class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>Address</th>
                    <th>Address Verified</th>
                    <th>Order Stage</th>
                    <th>LEX</th>
                    <th>Proximus Offer</th>
                    <th>Profile</th>
                     <th>PairNumber</th>
                      <th>SNA</th>

                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td><?php echo ucfirst(strtolower($service->details->street))." ".ucfirst(strtolower($service->details->number)) .", ".ucfirst(strtolower($service->details->postcode))." ".ucfirst(strtolower($service->details->city)); ?></td>
                    <td><?php echo $service->details->address_verified; ?></td>
                    <td><?php echo $service->details->proximus_status; ?></td>
                    <td><?php echo $service->details->lex; ?></td>
                    <td><?php echo $service->details->product_name; ?></td>
                    <td><?php foreach ($service->xdsl as $row) {
        ?>

                      <?php if ($row->product_name ==  $service->details->product_name) {
            ?>
                        <?php echo $row->downstreamBandwidth; ?> /  <?php echo $row->upstreamBandwidth; ?> (<?php echo $row->serviceAvailabilityStatus; ?>)

                      <?php
        } ?>
                      
                      <?php
    } ?>
                    </td>
                     <td><?php echo $service->details->introductionPairUsed; ?></td>
                      <td><?php if ($service->details->SNA == 1) {
        ?>Yes<?php
    } else {
        ?>No<?php
    } ?></td>
                  </tr>

                </tbody>
              </table>
             
              <center>
                <?php foreach ($service->xdsl as $row) {
        ?>

                      <?php if ($row->product_name ==  $service->details->product_name) {
            ?>
                        Min ATTN: <?php echo $row->maximumAttenuation; ?>dB / Max ATTN: <?php echo $row->maximumAttenuation; ?>dB  |  <?php echo  $row->averageLength." ".strtolower($row->unitOfMeasure); ?> Distance | <?php echo $row->numberOfFreePairs; ?> Pairs free

                      <?php
        } ?>
                      
                      <?php
    } ?>
                        
                      </center>
                      <br />
                        <center>
                <button type="button" class="btn btn-sm btn-primary" onclick="XDSLDATA();">Update Installation Information</button>
               
              </center>
              <h4>Installation Appointment</h4>
              <table class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>Date Requested</th>
                    <th>Date Confirmed</th>
                    <th>Visit/Remote</th>
                    <th>Workorder</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td><?php echo $service->details->appointment_date_requested; ?></td>
                    <td><?php echo $service->details->appointment_date_confirmed; ?></td>
                    <td><?php echo $service->details->type_install; ?></td>
                    <td><?php echo $service->details->workorderid; ?></td>
                   
                  </tr>

                </tbody>
              </table>
                <center>
               <!-- <button class="btn btn-sm btn-primary" onclick="">Amend Appointment</button> -->
                <button class="btn btn-sm btn-primary" onclick="DetermineAvailableTimeslots();"<?php if (!in_array($service->orderstatus, array("Workorder-Needed","On-Hold"))) {
        ?> disabled<?php
    } ?>>Get Availabel Slot</button>
              </center>

            </div>
            
          </div>
          <div class="row">
            <div class="col-sm-12">
                 <hr />
                <h4>Order Status History</h4>
                <?php if ($history) {
        ?>
                    <table class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>Date</th>
                    <th>Subject</th>
                    <th>Action</th>
                    <th>Description</th>
                  </tr>
                </thead>
                <tbody>
              <?php foreach ($history as $key => $order) {
            ?>
                  <tr>
                    <td><?php echo $order['date']; ?></td>
                    <td><?php echo $order['subject']; ?></td>
                    <td><?php echo $order['status']; ?></td>
                    <td><?php echo $order['feedback_description']; ?></td>
                   
                  </tr>
              <?php
        } ?>
                 </tbody>
              </table>
              <?php
    } ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  /*
 $(document).ready(function() {
  setTimeout(function() {
  location.reload();
}, 30000);
});
*/

function generateNewProximusId(){

var r = confirm("Are your sure? this will create order to proximus!");
if (r == true) {

  $('#loading').modal({backdrop: 'static', keyboard: false});
    var status = '<?php echo $service->details->status; ?>';
     $.ajax({
        url: '<?php echo base_url(); ?>admin/complete/pxs_regenerate_pxs_orderid',
        dataType: 'json',
        type: 'post',
        data: {
        action: "preordering",
serviceid: "<?php echo $service->details->serviceid; ?>",
method: "FindGeographicLocation",
postcode: "<?php echo $service->details->postcode; ?>",
city: "<?php echo $service->details->city; ?>",
number: "<?php echo $service->details->number; ?>",
street: "<?php echo $service->details->street; ?>",
country: "BEL"
},
        success: function(data) {
        console.log(data);
         $('#loading').modal('toggle');

        },
        error: function(errorThrown) {
           $('#loading').modal('toggle');
            console.log(errorThrown);
           
        }
    });

}





}
  function BookAppointment(){
     var r = confirm("Are your sure?");
if (r == true) {
  var idnya = $('#timeslot').find(":selected").val();
    $('.buttonappointment').prop('disabled', true);
        $.ajax({
        url: '<?php echo base_url(); ?>admin/complete/pxs_book_appointment',
        dataType: 'json',
        type: 'post',
        data: {
        action: "appointment",
serviceid: "<?php echo $service->details->serviceid; ?>",
method: "BookAppointment",
orderid: "<?php echo $service->proximusid; ?>",
id: idnya,
workOrderId: "<?php echo $service->details->workorderid; ?>"
},
        success: function(s) {
          if(s.result == "success"){
             $('.dsp').hide();
             $('.appointmentform').show();
             $('.slotx').html(s.html);
              $('.submitapp').show();
              $(".headerid").html('Please choose Slot Available from Proximus');
              $('#loading').modal('hide');
              window.location.replace("https://client.united-telecom.be/admin/subscription/activate_xdsl/<?php echo $service->details->serviceid; ?>");

          }
        
        },
        error: function(errorThrown) {
          
            console.log(errorThrown);
           
        }
    });

  }

  }
  function DetermineAvailableTimeslots(){
   // $('#loading').modal('toggle');
    $('#loading').modal({backdrop: 'static', keyboard: false});

      $.ajax({
        url: '<?php echo base_url(); ?>admin/complete/pxs_get_available_slot',
        dataType: 'json',
        type: 'post',
        data: {
        action: "appointment",
serviceid: "<?php echo $service->details->serviceid; ?>",
method: "DetermineAvailableTimeslots",
orderid: "<?php echo $service->proximusid; ?>",
requestedDate: "<?php echo $service->details->appointment_date_confirmed; ?>",
workOrderId: "<?php echo $service->details->workorderid; ?>"
},
        success: function(data) {
          if(data.result == "success"){
             $('.dsp').hide();
             $('.appointmentform').show();
             $('.slotx').html(data.html);
              $('.submitapp').show();
              $(".headerid").html('Please choose Slot Available from Proximus');
          }
        
        },
        error: function(errorThrown) {
           //$('#loading').modal('toggle');
            console.log(errorThrown);
           
        }
    });



  }

   function cancel_order(){
   var r = confirm("Are your sure? to cancel this order?");
if (r == true) {
    $('#loading').modal({backdrop: 'static', keyboard: false});

      $.ajax({
        url: '<?php echo base_url(); ?>admin/complete/pxs_cancel_pxs_orderid',
        dataType: 'json',
        type: 'post',
        data: {
        action: "ordering",
serviceid: "<?php echo $service->details->serviceid; ?>",
method: "CancelCustomerOrdering",
orderid: "<?php echo $service->proximusid; ?>"
},
        success: function(data) {
        console.log(data);
         //$('#loading').modal('toggle');

        },
        error: function(errorThrown) {
           //$('#loading').modal('toggle');
            console.log(errorThrown);
           
        }
    });

    }

  }

  function assign_proximus(){


    $('#activateNow').modal('toggle');

  }

  function executeProximus(){
var r = confirm("Are your sure? this will create order to proximus!");
if (r == true) {

  $('#loading').modal({backdrop: 'static', keyboard: false});
    var status = '<?php echo $service->details->status; ?>';
     $.ajax({
        url: '<?php echo base_url(); ?>admin/complete/pxs_submitcustomerordering',
        dataType: 'json',
        type: 'post',
        data: {
action: "order",
serviceid: "<?php echo $service->details->serviceid; ?>",
ut_orderid:"<?php echo $service->details->serviceid; ?>_<?php echo rand(1000000, 9999999); ?>",
method: "SubmitCustomerOrdering",
orderid: "<?php echo $service->proximusid; ?>",
productid: "<?php echo $service->details->product_id; ?>",
pid:  "<?php echo $service->packageid; ?>",
userid:  "<?php echo $service->userid; ?>",
ProductName: "<?php echo $service->details->product_name; ?>",
<?php if ($service->details->dialnumber) {
        ?>ProductType: "WV",
dialnumber: "<?php echo $service->details->dialnumber; ?>",<?php
    } ?> 
type_order: "<?php echo $service->details->type_order; ?>",
firstname: "<?php echo $client->firstname; ?>",
lastname: "<?php echo $client->lastname; ?>",
phonenumber: "<?php echo $client->phonenumber; ?>",
language: "NL",
agent: "<?php echo $this->session->firstname." ".$this->session->lastname; ?>",
requestedDate: "<?php echo $service->details->appointment_date_requested; ?>T10:00:00Z"
},
        success: function(data) {
        console.log(data);
         $('#loading').modal('toggle');

        },
        error: function(errorThrown) {
           $('#loading').modal('toggle');
            console.log(errorThrown);
           
        }
    });

}
}

function submitChanges(){

  var pppu = $('#pppoe_username');
  var pppp = $('#pppoe_password');

  if(pppu == ''){
    alert('Username Can not be empty');
   return false;
  }

  if(pppp == ''){

    alert('Password Can not be empty');
     return false;
  }

$('.submit_btn').prop('disabled', true);
$('.submit_btn').html('<i class="fas fa-spinner fa-spin"></i> Please wait...');
//$( "#spin" ).removeClass( "fa fa-save" ).addClass( "fa fa-cog fa-spin" );
var form_data=$("#xdslform").serializeArray();
$.ajax({
url: window.location.protocol + '//' + window.location.host + '/admin/subscription/xdsl_update_information',
type: 'post',
dataType: "json",
data: form_data,
success: function(data) {
if(data.result){
window.location.href = window.location.protocol + '//' + window.location.host + '/admin/subscription/detail/<?php echo $this->uri->segment(4); ?>';
}else{
alert("There was an error: "+data.message);
$('.submit_btn').prop('disabled', false);
$('.submit_btn').html('<i class="fa fa-save"></i> <?php echo lang('Register Clients'); ?>');
}
}
});


};

 function XDSLDATA(){

    $('#XDSLDATA').modal('toggle');
  }
  </script>
<pre>
  <?php //print_r($service);?>
  </pre>

  <div class="modal fade" id="activateNow" tabindex="-1" role="dialog" aria-labelledby="activateNow"
   aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
      <form method="post" action="<?php echo base_url(); ?>admin/subscription/assign_proximus_package">
      <input type="hidden" name="serviceid" value="<?php echo $service->id; ?>">
      <input type="hidden" name="userid" value="<?php echo $service->userid; ?>">
      <input type="hidden" name="proximusid" value="<?php echo $service->proximusid; ?>">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle">
            <?php echo lang('Please choose Which Proximus Order you wish to assign'); ?>
          </h5>
        </div>
        <div class="modal-body">
          <select name="productname" class="form-control">
            <?php foreach ($service->xdsl as $row) {
        ?>
              <option value="<?php echo $row->productid; ?>"><?php echo $row->product_name; ?></option>
            <?php
    } ?>
          </select>

        </div>
        <div class="modal-footer">
          <button class="btn btn-md btn-primary" type="submit">Submit</button>
        </div>
        </form>
      </div>
    </div>
  </div>


  <div class="modal" tabindex="-1" role="dialog" id="loading">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title headerid"></h5>
      </div>
      <div class="modal-body">
      <center class="dsp">
        <img src="<?php echo base_url(); ?>assets/img/loading.gif" width="100">
        <br />
        Please wait... we are encrypting xml and push it to Proximus via MSO!
      </center>
      <div class="appointmentform" style="display:none;">
        <form id="bookappointment">

          <input type="hidden" name="orderid" value="<?php echo $service->proximusid; ?>" id="appointmentorderid">
           <div class="form-group">
            <label class="label-form">Proximus Technician Time Slot:</label>
          <select name="timeslot" class="form-control slotx" id="timeslot">
            
          </select>
        </div>
        </form>
        <div class="form-group">
        <button class="btn btn-sm btn-primary buttonappointment" type="button" onclick="BookAppointment()">Book Appointment</button>
      </div>
      </div>
      </div>
      </div>
      <div class="modal-footer">
       
      </div>
    </div>
  </div>

  <div class="modal" tabindex="-1" role="dialog" id="XDSLDATA">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form id="xdslform">
      <div class="modal-header">
        <h5 class="modal-title headerid"><?php echo lang('Update XDSL Information'); ?></h5>
      </div>
      <div class="modal-body">
      <div class="form-group">

            <label class="label-form"><?php echo lang('Realm'); ?></label>
          <select name="realm" class="form-control realm" id="realm">

            <?php foreach (array('unitedadsl' => 'UNITED',
            'digiweb_be' => 'DIGIWEB',
            'happymany' => 'HAPPYMANNY',
            'intellinet' => 'INTELLINET',
            'telenatie_be' =>'TELENATIE',
            'ldatelecom_com','LDATELECOM') as $key =>$row) {
        ?>
                <option value="<?php echo $key; ?>"><?php echo $row; ?></option>
            <?php
    } ?>
          </select>
        </div>

        <div class="form-group">
            <label class="label-form"><?php echo lang('Username'); ?></label>
          <input type="text" name="pppoe_username" class="form-control" value="<?php echo $service->details->pppoe_username; ?>">

        </div>

            <div class="form-group">
            <label class="label-form"><?php echo lang('Recurring'); ?></label>
          <input type="text" name="recurring" class="form-control" value="<?php echo $service->recurring; ?>">

        </div>


        <div class="form-group">
            <label class="label-form"><?php echo lang('Password'); ?></label>
         <input type="text" name="pppoe_password" class="form-control" value="<?php echo $service->details->pppoe_password; ?>">
        </div>
           <div class="form-group">
            <label class="label-form"><?php echo lang('Circuitid'); ?></label>
          <input type="text" name="circuitid" class="form-control" value ="<?php echo $service->details->circuitid; ?>">

        </div>

          <div class="form-group">
            <label class="label-form"><?php echo lang('PXS ID'); ?></label>
          <input type="text" name="proximus_orderid" class="form-control" value="<?php echo $service->details->proximus_orderid; ?>">

        </div>

      </div>
      <div class="modal-footer">
        <input type="hidden" name="serviceid" value="<?php echo $this->uri->segment(4); ?>">
        <button class="btn btn-md btn-primary submit_btn" type="button" onclick="submitChanges()"><?php echo lang('Save Changes'); ?></button>
      </div>
    </form>
    </div>
  </div>
</div>




