<section class="tables">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12 collapse" id="uploading">
        <div class="card">
          <div class="card-header d-flex align-items-center">
            <h3 class="h4"><?php echo lang('Upload File'); ?></h3>
          </div>
          <div class="card-body">
            <form method="post" action="<?php echo base_url(); ?>admin/transactions/coda_upload"  enctype="multipart/form-data">
              <fieldset>
                <legend>Legend</legend>
                <div class="form-group">
                  <label for="exampleInputFile">File input</label>
                  <input type="file" name="coda" class="form-control-file" id="exampleInputFile" aria-describedby="fileHelp">
                  <small id="fileHelp" class="form-text text-muted">Upload Coda file.</small>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
              </fieldset>
            </form>
          </div>
        </div>
      </div>
      <div class="col-lg-12">
        <div class="card">
          <div class="card-close">
            <button id="upload" class="btn btn-sm btn-primary" type="button" data-toggle="collapse" data-target="#uploading" aria-expanded="false" aria-controls="uploading"><span class="fa fa-upload"></span> Upload Coda </button>
          </div>
          <div class="card-header d-flex align-items-center">
            <h3 class="h4"><?php echo lang('List Coda Files'); ?></h3>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-striped table-lightfont" id="dt_coda">
                <thead>
                  <tr>
                    <th><?php echo lang('ID'); ?></th>
                    <th><?php echo lang('Filename'); ?></th>
                    <th><?php echo lang('Date'); ?></th>
                    <th><?php echo lang('Admin'); ?></th>
                    <th><?php echo lang('Status'); ?></th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
