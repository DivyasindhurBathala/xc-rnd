<section class="tables">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-close">
 <a href="<?php echo base_url(); ?>admin/transactions/coda_history/all" class="btn btn-sm btn-default"><span class="fa fa-tasks"></span> Show All </a>
 <a href="<?php echo base_url(); ?>admin/transactions/coda_history/anomalies" class="btn btn-sm btn-info"><span class="fa fa-tasks"></span> Show Anomalies </a>
 <a href="<?php echo base_url(); ?>admin/transactions/coda_history/return" class="btn btn-sm btn-danger"><span class="fa fa-tasks"></span> Show Directdebit Return </a>
 <a href="<?php echo base_url(); ?>admin/transactions/coda_history/Done" class="btn btn-sm btn-default"><span class="fa fa-tasks"></span> Show Completed</a>
 <a href="<?php echo base_url(); ?>admin/transactions/coda_history/correct" class="btn btn-sm btn-success"><span class="fa fa-tasks"></span> Show Correct</a>
 <a href="<?php echo base_url(); ?>admin/transactions/coda_history/anomaliespaid" class="btn btn-sm btn-warning"><span class="fa fa-tasks"></span> Show Anomalies Paid</a>
 <a href="<?php echo base_url(); ?>admin/transactions/coda_history/paymentout" class="btn btn-sm btn-default"><span class="fa fa-tasks"></span> Payment Out</a>
          </div>
          <div class="card-header d-flex align-items-center">
            <h3 class="h4"><?php echo lang('List Transactions'); ?></h3>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-striped table-lightfont" id="dt_coda">
                <thead>
                  <tr>
                    <th><?php echo lang('ID'); ?></th>
                    <th><?php echo lang('Customer'); ?></th>
                    <th><?php echo lang('IBAN'); ?></th>
                    <th><?php echo lang('Date'); ?></th>
                    <th><?php echo lang('Reference'); ?></th>
                     <th><?php echo lang('Amountin'); ?></th>
                    <th><?php echo lang('InvoiceAmount'); ?></th>
                    <th><?php echo lang('Invoicenum'); ?></th>
                    <th><?php echo lang('Status'); ?></th>

                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<form>
    <input type="hidden" id="type" value="<?php echo $this->uri->segment(4); ?>">
  </form>
