<?php
$m = get_string_between($transaction->msg, ' ***', '*** ');
if (!empty($m)) {
	$vara = explode('/', $m);
	$toro = str_replace('/', '', $m);
	if (substr($vara[0], 0, 3) == "888") {
		$ogm = substr($toro, 0, 10);
	} else {
		$ogm = "";
	}
} else {
	$ogm = "";
}
if (empty($transaction->invnum)) {
	// Capsule::table('x_iban')->where('iban', 'LIKE', trim($transaction->iban))->get();
	$iban = getIban($transaction->iban);
	if (!empty($iban)) {
		foreach ($iban as $i) {
			$open[$i->userid] = dt_getOpenInvoice($i->userid);
		}
	} else {
		$open = array();
	}
}
if (!empty($_GET['success'])) {
	if ($_GET['success'] == "false") {?>
<div class="alert alert-dismissible alert-danger">
	<button type="button" class="close" data-dismiss="alert">&times;</button>
	<strong>Your Payment was not successfully executed because the invoice number does not exists Please make sure the invoicenumber is correct</strong>
</div>
<?php
}
}
?>
<script src="<?php echo base_url(); ?>assets/themes/js/complete.js"></script>
<section class="tables">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-header d-flex align-items-center">
						<h3 class="h4"><?php echo lang('List Transactions'); ?></h3>
					</div>
					<div class="card-body">
						<div class="row">
							<div class="col-md-3">
								<div class="panel panel-primary">
									<div class="panel-heading">
										<h3 class="panel-title">Action Form</h3>
									</div>
									<div class="panel-body">
										<ul class="nav nav-tabs">
											<li class="nav-item">
												<a class="nav-link active" data-toggle="tab" href="#add">Add Payment</a>
											</li>
											<li class="nav-item">
												<a class="nav-link" data-toggle="tab" href="#proforma">PaymentOut</a>
											</li>
										</ul>
										<div id="myTabContent" class="tab-content">
											<div class="tab-pane fade show active" id="add">
												<form method="post" action="<?php echo base_url(); ?>admin/transactions/addpayment/<?php echo $transaction->trx_id; ?>">
													<input name="paymentmethod" value="banktransfer" type="hidden">
													<input name="iban" type="hidden" value="<?php echo $transaction->iban; ?>">
													<input name="transid" value="<?php echo $transaction->trx_id; ?>" type="hidden">
													<input name="date" value="<?php echo $transaction->date_trx; ?>" type="hidden">
													<input name="adminid" value="<?php echo $this->session->userdata('id'); ?>" type="hidden">
													<?php if (!empty($transaction->invnum)) {?>
													<div class="form-group">
														<input type="hidden" id="fakeid" value="">
														<label class="control-label" for="focusedInput">Customer ID, Name</label>
														<input  name="userid" class="form-control  ui-autocomplete-input addpayment"  id="userid" type="text" value="<?php echo $transaction->userid . ', ' . getcustomerfullname($transaction->userid); ?>">
													</div>
													<?php } elseif (!empty($iban)) {?>
													<div class="form-group">
														<label for="select">Customer ID, Name</label>
														<div id="newid"></div>
														<select class="form-control" id="customeridnya"  name="userid">';
															<?php	foreach ($iban as $ib) {?>
															<option value="<?php echo $ib->userid; ?>"><?php echo $ib->userid . ',  ' . getcustomerfullname($ib->userid); ?> </option>
															<?php	}?>
															<option value="matikan" id="matikan">Not In the List?</option>
														</select>
														<script>
														$('#customeridnya').change(function() {
														var selectid =  $('#customeridnya').val();
														if(selectid == "matikan"){
														$( "#customeridnya" ).hide();
														$( "#newid" ).html('<input type="hidden" id="fakeid" value=""><input  name="userid" class="form-control" type="text" value="" id="client_id" autocomplete="off"  onkeyup="autocomplet_client()"><ul id="client_list_id" class="list-group"></ul>');
														}
														});
														</script>
													</div>
													<?php } else {?>
													<div class="form-group">
														<label class="control-label" for="focusedInput">Customer ID, Name</label>
														<input type="hidden" id="fakeid" value="">
														<input  name="userid" class="form-control  ui-autocomplete-input addpayment" type="text" value="" id="userid" autocomplete="off" placeholder="Serach Customer">
													</div>
													<div class="form-group" style="display:none" id="buzzme">
														<label class="control-label" for="focusedInput">Customer Name</label>
														<input class="form-control" type="text" value="" id="customername">
													</div>
													<?php }?>
													<div class="form-group" id="addmecall">
														<label class="control-label" for="focusedInput">Invoice Number <!-- <a class="add_field_button"><i class="fa fa-plus-circle"></i> Add More Invoicenumber</a> --></label>
														<input name="invoicenum" class="form-control" id="invoice_id" type="text" value="<?php echo $transaction->invnum; ?>">
														<div class="invalid-feedback">Make sure this is Invoicenumber not Invoiceid</div>
													</div>
													<div class="form-group">
														<label class="control-label" for="focusedInput">Amount</label>
														<input name="amount" class="form-control" id="focusedInput" type="text" value="<?php echo $transaction->amount_paid; ?>">
													</div>
													<div class="form-group">
														<button class="btn btn-primary btn-mg" id="focusedInput" type="submit">Add Transaction</button>
													</div>
												</form></div>
												<div class="tab-pane fade" id="remove">
													<form method="post" action="<?php echo base_url(); ?>admin/transaction/addpayment">
														<div class="form-group">
															<label class="control-label" for="focusedInput">InvoiceNumber</label>
															<input  name="userid" class="form-control" id="getinvoicenum" type="text" value="' . $transaction->invnum . '">
															<span class="help-block text-danger">Make sure this is Invoicenumber not Invoiceid</span>
														</div>
														<div class="form-group">
															<button class="btn btn-primary btn-mg" id="focusedInput" type="submit">Remove Transaction & Set Unpaid</button>
														</div>
													</form>
												</div>
												<div class="tab-pane fade" id="proforma">
													<form method="post" action="<?php echo base_url(); ?>transaction/addpaymentout">
														<div class="form-group">
															<label class="control-label" for="focusedInput">Proforma Number</label>
															<input  name="invoiceid" class="form-control" id="focusedInput" type="text" value="<?php echo $ogm; ?>">
															<span class="help-block text-danger">Make sure this is Invoicenumber not Invoiceid</span>
														</div>
														<div class="form-group">
															<label class="control-label" for="focusedInput">Amount</label>
															<input  name="amount" class="form-control" id="focusedInput" type="text" value="<?php echo $transaction->amount_paid; ?>">
															<span class="help-block text-danger">Make sure this is Invoicenumber not Invoiceid</span>
														</div>
														<div class="form-group">
															<label class="control-label" for="focusedInput">Date</label>
															<input  name="date" class="form-control" id="focusedInput" type="text" value="<?php echo $transaction->date_trx; ?>">
															<span class="help-block text-danger"></span>
														</div>
														<div class="form-group">
															<button class="btn btn-primary btn-mg" id="focusedInput" type="submit">Applay Payment and Create Invoice</button>
														</div>
													</form>
												</div>
											</div></div>
										</div>
									</div>
									<div class="col-md-9">
										<div class="table-responsive">
											<table class="table table-bordered table-striped" width="100">
												<thead>
													<tr>
														<th>IBAN</th>
														<th>SENDER NAME</th>
														<th>AMOUNT</th>
														<th>DATE TRX</th>
														<th>MESSAGE</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td><?php echo $transaction->iban; ?></td>
														<td><?php echo $transaction->name; ?></td>
														<td><?php echo $transaction->amount_paid; ?></td>
														<td><?php echo $transaction->date_trx; ?></td>
														<td><?php echo shorten_string($transaction->msg, 25); ?></td>
													</tr>
												</tbody>
											</table>
											<br />
											<table class="table table-bordered" id="invoices" style="display:none;">
												<thead>
													<tr>
														<th>Customerid</th>
														<th>Invoice Number</th>
														<th>AMOUNT</th>
														<th>Duedate</th>
														<th>Status</th>
													</tr>
												</thead>
												<tbody id="items">
												</tbody>
											</table>
											<div id="scriptme"></div>
										</div>
									</div>
									<?php	if (!empty($iban)) {
	?>
									<div class="panel panel-primary">
										<div class="panel-heading">
											<h3 class="panel-title">Possibly Invoice Information</h3>
										</div>
										<div class="panel-body">
											<table class="table table-bordered">
												<thead>
													<tr>
														<th>CUSTOMER ID</th>
														<th>Invoicen Number</th>
														<th>DATE</th>
														<th>AMOUNT</th>
													</tr>
												</thead>
												<tbody>
													<?php	if (!empty($open)) {
		foreach ($open as $key => $val) {
			foreach ($val as $o) {?>
													<tr>
														<td><?php echo $key; ?></td>
														<td><?php echo $o->invoicenum; ?></td>
														<td><?php echo $o->date; ?></td>
														<td><?php echo $o->total; ?></td>
													</tr>
													<?php	}
		}
	}?>
												</tbody>
											</table>
										</div>
									</div>
									<?php }?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>