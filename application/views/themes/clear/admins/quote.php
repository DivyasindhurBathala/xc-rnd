<div class="content-i">
  <div class="content-box">
    <div class="element-wrapper">
      <h6 class="element-header">
      Quotes<div class="close">
          <a class="btn btn-primary btn-sm" href="<?php echo base_url(); ?>admin/quote/create"><i class="fa fa-plus-circle"></i> <?php echo lang('New Quote'); ?></a>
        </div>
      </h6>
      <div class="element-box">
        <h5 class="form-header">
        List Quotes
        </h5>
        <div class="table-responsive">
          <table class="table table-striped table-lightfont" id="clients">
            <thead>
              <tr>
                <th>#<?php echo lang('Number'); ?></th>
                <th><?php echo lang('Customer'); ?></th>
                <th><?php echo lang('Date'); ?></th>
                <th><?php echo lang('Duedate'); ?></th>
                <th><?php echo lang('Total'); ?></th>
                <th><?php echo lang('Status'); ?></th>
                <th></th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>