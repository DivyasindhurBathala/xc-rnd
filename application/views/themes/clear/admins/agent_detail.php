<div class="content-i">
    <div class="content-box">
        <div class="element-wrapper">
            <h6 class="element-header">
                <?php echo $title; ?>
            </h6>
            <div style="display:none;" id="stats">
                <div class="table-responsive">
                    <div class="row">
                        <div class="col-sm-3">
                            <a class="element-box el-tablo" href="#">
                                <div class="label">
                                    <?php echo lang('Subscription Activated Today'); ?>
                                </div>
                                <div class="value s_today">


                                </div>
                                <div class="trending trending-down-basic">

                                </div>
                            </a>
                        </div>
                        <div class="col-sm-3">
                            <a class="element-box el-tablo" href="#">
                                <div class="label">
                                    <?php echo lang('Subscription 7 days'); ?>
                                </div>
                                <div class="value s_week">


                                </div>
                                <div class="trending trending-down-basic">

                                </div>
                            </a>
                        </div>
                        <div class="col-sm-3">
                            <a class="element-box el-tablo" href="#">
                                <div class="label">
                                    <?php echo lang('Subscription 30 days'); ?>
                                </div>
                                <div class="value s_month float-center">


                                </div>
                                <div class="trending trending-down-basic">

                                </div>
                            </a>
                        </div>

                        <div class="col-sm-3">
                            <a class="element-box el-tablo" href="#">
                                <div class="label">
                                    <?php echo lang('Subscription This Year'); ?>
                                </div>
                                <div class="value s_year">


                                </div>
                                <div class="trending trending-down-basic">

                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-3">
                            <a class="element-box el-tablo" href="#">
                                <div class="label">
                                    <?php echo lang('Topup Ordered Today'); ?>
                                </div>
                                <div class="value t_today">


                                </div>
                                <div class="trending trending-down-basic">

                                </div>
                            </a>
                        </div>
                        <div class="col-sm-3">
                            <a class="element-box el-tablo" href="#">
                                <div class="label">
                                    <?php echo lang('Topup Ordered 7 days'); ?>
                                </div>
                                <div class="value t_week">


                                </div>
                                <div class="trending trending-down-basic">

                                </div>
                            </a>
                        </div>
                        <div class="col-sm-3">
                            <a class="element-box el-tablo" href="#">
                                <div class="label">
                                    <?php echo lang('Topup Ordered 30 days'); ?>
                                </div>
                                <div class="value t_month">


                                </div>
                                <div class="trending trending-down-basic">

                                </div>
                            </a>
                        </div>

                        <div class="col-sm-3">
                            <a class="element-box el-tablo" href="#">
                                <div class="label">
                                    <?php echo lang('Topup Ordered This Year'); ?>
                                </div>
                                <div class="value t_year">


                                </div>
                                <div class="trending trending-down-basic">

                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-3">
                            <a class="element-box el-tablo" href="#">
                                <div class="label">
                                    <?php echo lang('Bundle Ordered Today'); ?>
                                </div>
                                <div class="value b_today">


                                </div>
                                <div class="trending trending-down-basic">

                                </div>
                            </a>
                        </div>
                        <div class="col-sm-3">
                            <a class="element-box el-tablo" href="#">
                                <div class="label">
                                    <?php echo lang('Bundle Ordered 7 days'); ?>
                                </div>
                                <div class="value b_week">


                                </div>
                                <div class="trending trending-down-basic">

                                </div>
                            </a>
                        </div>
                        <div class="col-sm-3">
                            <a class="element-box el-tablo" href="#">
                                <div class="label">
                                    <?php echo lang('Bundle Ordered 30 days'); ?>
                                </div>
                                <div class="value b_month">


                                </div>
                                <div class="trending trending-down-basic">

                                </div>
                            </a>
                        </div>

                        <div class="col-sm-3">
                            <a class="element-box el-tablo" href="#">
                                <div class="label">
                                    <?php echo lang('Bundle Ordered This Year'); ?>
                                </div>
                                <div class="value b_year">


                                </div>
                                <div class="trending trending-down-basic">

                                </div>
                            </a>
                        </div>
                    </div>



                </div>
            </div>
            <div class="element-box">
                <div class="table-responsive">
                    <div class="row">
                        <div class="col-sm-4">
                            <h4><?php echo lang('Reseller Information'); ?>
                                <?php if ($setting->mobile_platform == "TEUM") {
    ?>
                                <button class="btn btn-sm btn-danger" type="button" onclick="showStats();"><i
                                        class="fa fa-chart-line float-right"></i></button>
                                <?php
} ?>
                            </h4>
                            <hr>
                            <table class="table table-striped table-hover">
                                <tbody>
                                    <tr>
                                        <td><?php echo lang('Reseller ID'); ?></td>
                                        <td>
                                            <?php echo $agent->id; ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?php echo lang('Reseller Name'); ?></td>
                                        <td>
                                            <?php echo $agent->agent; ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?php echo lang('Reseller Address'); ?></td>
                                        <td>
                                            <?php echo $agent->address1.' '.$agent->postcode.' '.$agent->city; ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?php echo lang('Reseller Phonenumber'); ?></td>
                                        <td>
                                            <?php echo $agent->phonenumber; ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?php echo lang('Reseller Email'); ?></td>
                                        <td>
                                            <?php echo $agent->email; ?>
                                        </td>
                                    </tr>
                                    <?php if ($agent->reseller_type == "Postpaid") {
        ?>
                                    <tr>
                                        <td><?php echo lang('Reseller Commission'); ?></td>
                                        <td>
                                            <?php echo $agent->comission_type.' '.$agent->comission_value; ?>
                                        </td>
                                    </tr>
                                    <?php
    } elseif ($agent->reseller_type == "Prepaid") {
        ?>
                                    <tr>
                                        <td><?php echo lang('Reseller Balance'); ?></td>
                                        <td>
                                            <?php echo $setting->currency.' '.$agent->reseller_balance; ?>
                                        </td>
                                    </tr>
                                    <?php
    } else {
        ?>
                                    <tr>
                                        <td><?php echo lang('Curent Month Earnings'); ?></td>
                                        <td>
                                            <?php echo $setting->currency; ?> <?php if (!$agent->amount) {
            ?>0.00<?php
        } else {
            ?><?php echo $agent->amount; ?><?php
        } ?>
                                        </td>
                                    </tr>
                                    <?php
    } ?>
                                    <?php //print_r($agent);?>
                                </tbody>
                            </table>
                            <button data-toggle="modal" data-target="#editAgent"
                                class="btn btn-block btn-md btn-primary"><i class="fa fa-edit"></i>
                                <?php echo lang('Modify Reseller'); ?></button>
                            <?php if ($agent->reseller_type == "Prepaid") {
        ?>

                            <button data-toggle="modal" data-target="#AgentTopup"
                                class="btn btn-block btn-md btn-primary"><i class="fa fa-upload"></i>
                                <?php echo lang('Topup Reseller Balance'); ?></button>
                            <?php
    } ?>

                            <a id="export_subscription"
                                href="<?php echo base_url(); ?>admin/agent/export_agent_subscription/<?php echo $agent->id; ?>"
                                class="btn btn-block btn-md btn-primary"><i class="fa fa-tasks"></i>
                                <?php echo lang('Export Subscription'); ?></a>
                            <button data-toggle="modal" data-target="#send_password"
                                class="btn btn-block btn-md btn-primary"><i class="fa fa-key"></i>
                                <?php echo lang('Send Credential'); ?></button>
                            <?php if ($agent->status == "Active") {
        ?>
                            <button data-toggle="modal" data-target="#suspend_reseller"
                                class="btn btn-block btn-md btn-primary"><i class="fa fa-pause"></i>
                                <?php echo lang('Suspend Reseller'); ?></button>

                            <?php
    } else {
        ?>
                            <button data-toggle="modal" data-target="#unsuspend_reseller"
                                class="btn btn-block btn-md btn-primary"><i class="fa fa-play"></i>
                                <?php echo lang('UnSuspend Reseller'); ?></button>

                            <?php
    } ?>
                            <a id="login" class="btn btn-block btn-md btn-primary"
                                href="<?php echo base_url(); ?>admin/agent/login_as_reseller/<?php echo $agent->id; ?>"><i
                                    class="fa fa-eye"></i> <?php echo lang('Login as Reseller'); ?></a>
                                    <?php if (!$agent->isdefault) {
        ?>
                                        <button data-toggle="modal" data-target="#deleteAgent"
                                class="btn btn-block btn-md btn-danger"><i class="fa fa-trash"></i>
                                <?php echo lang('Delete Reseller'); ?></button>
                                    <?php
    } ?>

                            <button data-toggle="modal" data-target="#assign_sim"
                                class="btn btn-block btn-md btn-primary"><i class="fa fa-upload"></i>
                                <?php echo lang('Assign Bulk Simcards'); ?></button>
                            <a href="<?php echo base_url(); ?>admin/agent/assign_simcard_by_scanner/<?php echo $agent->id; ?>"
                                class="btn btn-block btn-md btn-primary"><i class="fa fa-credit-card"></i>
                                <?php echo lang('Assign Simcards with Scanner'); ?></a>
                            <button data-toggle="modal" data-target="#products"
                                class="btn btn-block btn-md btn-primary"><i class="fa fa-edit"></i>
                                <?php echo lang('Assign Product'); ?></button>


                            <hr />

                            <table class="table table-striped">
                            <thead>
                            <tr>
                            <th><?php echo lang('Permission'); ?></th>
                            <th class="text-right"><?php echo lang('Switch'); ?></th>
                            </tr>
                            </thead>
                            <tbody>

                            <?php foreach ($perms as $key => $value) {
        ?>
                            <tr>
                            <td><?php echo ucfirst($key); ?></td>
                            <td class="text-right"><div class="custom-switch custom-switch-label-onoff"><input class="custom-switch-input permission" id="<?php echo $key; ?>" type="checkbox"<?php if ($value == 1) {
            ?> checked<?php
        } ?>><label class="custom-switch-btn" for="<?php echo $key; ?>"></label> </div></td>
                            </tr>
                            <?php
    } ?>
                            </tbody>
                            </table>


                        </div>

                        <div class="col-sm-8">
                            <div class="row">
                                <div class="col-sm-12">
                                    <h4> <?php echo lang('List Subscriptions'); ?></h4>
                                    <hr>
                                    <table class="table table-striped table-lightfont" id="orders">
                                        <thead>
                                            <tr>
                                                <th>
                                                    <?php echo lang('#Client ID'); ?>
                                                </th>
                                                <th>
                                                    <?php echo lang('Package'); ?>
                                                </th>
                                                <th>
                                                    <?php echo lang('Contact'); ?>
                                                </th>
                                                <th>
                                                    <?php echo lang('Status'); ?>
                                                </th>
                                                <th>
                                                    <?php echo lang('Amount'); ?>
                                                </th>
                                                <th>
                                                    <?php echo lang('Identifier'); ?>
                                                </th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <h4> <?php echo lang('List Assigned Simcard'); ?></h4>
                                    <hr>
                                    <table class="table table-striped table-lightfont" id="simcardtable">
                                        <thead>
                                            <tr>
                                                <th>
                                                    <?php echo lang('Simcard'); ?>
                                                </th>
                                                <th>
                                                    <?php echo lang('Status'); ?>
                                                </th>
                                                <th>
                                                    <?php echo lang('MSISDN'); ?>
                                                </th>

                                                <th>
                                                    <?php echo lang('Tool'); ?>
                                                </th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                            <?php if ($agent->reseller_type == "Prepaid") {
        ?>
                            <div class="row">
                                <div class="col-sm-12">
                                    <h3> <?php echo lang('Reseller Reload History'); ?></h3>
                                    <hr>
                                    <table class="table table-striped table-lightfont" id="topups">
                                        <thead>
                                            <tr>
                                                <th>
                                                    <?php echo lang('Date'); ?>
                                                </th>
                                                <th>
                                                    <?php echo lang('Amount'); ?>
                                                </th>
                                                <th>
                                                    <?php echo lang('Description'); ?>
                                                </th>
                                                <th>
                                                    <?php echo lang('Executor'); ?>
                                                </th>
                                                <th>
                                                    <?php echo lang('Channels'); ?>
                                                </th>



                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <?php
    } ?>



<div class="row">
<div class="col-sm-12">
<div class="card border-light">
  <div class="card-body">
    <h4 class="card-title"><?php echo lang('Credit History'); ?><div class="float-right"><a href="<?php echo base_url(); ?>admin/agent/export_reseller_credit_usage/<?php echo $this->uri->segment(4); ?>" target="_blank" class="btn btn-primary"><i class="fa fa-download"></i> <?php echo lang("Export Credit Usage"); ?></a></div></h4>
     <table id="history" class="table table-striped table-bordered">
        <thead>
          <tr>
                <th><?php echo lang('DateTime'); ?> (GMT+1)</th>
                <th><?php echo lang('Type'); ?></th>
                <th><?php echo lang('Amount'); ?></th>
                <th><?php echo lang('Msisdn'); ?></th>
                <th><?php echo lang('User'); ?></th>
              </tr>
        </thead>
        <tbody>
</tbody>
</table>
  </div>
</div>
</div>
</div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<script>
$(document).ready(function() {
    $.getJSON(window.location.protocol + '//' + window.location.host + '/admin/complete/get_lang', function(
        data) {
        var x = new Date();
        var y = x.getFullYear().toString();
        var m = (x.getMonth() + 1).toString();
        var d = x.getDate().toString();
        (d.length == 1) && (d = '0' + d);
        (m.length == 1) && (m = '0' + m);
        var date = y + '-' + m + '-' + d;
        $('#orders').DataTable({
            "autoWidth": false,
            "processing": true,
            "orderCellsTop": true,
            "ordering": true,
            "serverSide": true,
            "colReorder": true,
            "ajax": window.location.protocol + '//' + window.location.host +
                '/admin/table/get_agentorders/<?php echo $this->uri->segment(4); ?>',
            "aaSorting": [
                [1, 'desc']
            ],
            "language": {
                "url": window.location.protocol + '//' + window.location.host +
                    "/assets/clear/js/datatables/lang/" + data.result + ".json"
            },
            "fnRowCallback": function(nRow, aData, iDisplayIndex) {
                if (aData[8] == date) {
                    $('td:eq(0)', nRow).html(aData[0] + '<img src="' + window.location
                        .protocol + '//' + window.location.host +
                        '/assets/img/new-sticker.png" height="16">');
                }
                $('td:eq(0)', nRow).html('<a  href="' + window.location.protocol + '//' +
                    window.location.host + '/admin/subscription/detail/' + aData[8] +
                    '">' + aData[0] + '</a>');
                $('td:eq(1)', nRow).html('<a  href="' + window.location.protocol + '//' +
                    window.location.host + '/admin/subscription/detail/' + aData[8] +
                    '">' + aData[1] + '</a>');
                $('td:eq(2)', nRow).html('<a  href="' + window.location.protocol + '//' +
                    window.location.host + '/admin/subscription/detail/' + aData[8] +
                    '">' + aData[2] + '</a>');
                $('td:eq(3)', nRow).html('<a href="' + window.location.protocol + '//' +
                    window.location.host + '/admin/subscription/detail/' + aData[8] +
                    '">' + aData[3] + '</a>');
                $('td:eq(4)', nRow).html(data.currency + aData[4]);
                return nRow;
            },
        });

        $('#topups').DataTable({
            "autoWidth": false,
            "processing": true,
            "orderCellsTop": true,
            "ordering": true,
            "serverSide": true,
            "colReorder": true,
            "ajax": window.location.protocol + '//' + window.location.host +
                '/admin/table/getResellerToups/<?php echo $this->uri->segment(4); ?>',
            "aaSorting": [
                [1, 'desc']
            ],
            "language": {
                "url": window.location.protocol + '//' + window.location.host +
                    "/assets/clear/js/datatables/lang/" + data.result + ".json"
            },

        });

    });
});
</script>


<div class="modal fade" id="editAgent" tabindex="-1" role="dialog" aria-labelledby="editAgent" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="post" action="<?php echo base_url(); ?>admin/agent/update">
                <input type="hidden" name="id" value="<?php echo $agent->id; ?>">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                        <?php echo lang('Modify Agent Information'); ?>
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label for="agent"><?php echo lang('Company Name'); ?></label>
                            <input name="agent" type="text" class="form-control" id="companyname"
                                value="<?php echo $agent->agent; ?>">
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="contact_name"><?php echo lang('Contact Name'); ?></label>
                            <input name="contact_name" type="text" class="form-control" id="agent"
                                value="<?php echo $agent->contact_name; ?>">
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-sm-5">
                            <label for="address1"><?php echo lang('Address1'); ?></label>
                            <input name="address1" type="text" class="form-control" id="address1"
                                value="<?php echo $agent->address1; ?>">
                        </div>
                        <div class="form-group col-sm-3">
                            <label for="postcode"><?php echo lang('Postcode'); ?></label>
                            <input name="postcode" type="text" class="form-control" id="postcode"
                                value="<?php echo $agent->postcode; ?>">
                        </div>
                        <div class="form-group col-sm-4">
                            <label for="city"><?php echo lang('City'); ?></label>
                            <input name="city" type="text" class="form-control" id="city"
                                value="<?php echo $agent->city; ?>">
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label for="phonenumber"><?php echo lang('Phonenumber'); ?></label>
                            <input name="phonenumber" type="text" class="form-control" id="text"
                                value="<?php echo $agent->phonenumber; ?>">
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="country"><?php echo lang('Country'); ?></label>
                            <select name="country" class="form-control" name="country">
                                <?php foreach (getCountries() as $key => $row) {
        ?>
                                <option value="<?php echo $key; ?>" <?php if ($agent->country == $key) {
            ?> selected <?php
        } ?>>
                                    <?php echo $row; ?>
                                </option>
                                <?php
    } ?>
                            </select>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-sm-4">
                            <label for="reseller_type"><?php echo lang('Reseller Type'); ?></label>
                            <select class="form-control" name="reseller_type" id="reseller_type" disabled>
                                <?php foreach (array('Postpaid','Prepaid','Internal') as $type) {
        ?>
                                <option value="<?php echo $type; ?>" <?php if ($agent->reseller_type == $type) {
            ?> selected<?php
        } ?>><?php echo lang($type); ?>
                                </option>
                                <?php
    } ?>
                            </select>
                        </div>
                        <div class="form-group col-sm-8 balance" style="display:none;">
                            <label for="balance"><?php echo lang('Current Balance'); ?></label>
                            <input type="number" step="any" class="form-control" id="balance"
                                value="<?php echo $agent->reseller_balance; ?>" disabled>
                                <div class="form-group ">
              <label for="balance"><?php echo lang('Discount'); ?> % <?php echo lang('Pertcentage will be deducted when redirected to payment gateway'); ?></label>
              <input name="discount" type="number" step="any" class="form-control" id="discount" value="<?php echo $agent->comission_value; ?>">
             </div>

             <div class="form-group ">
              <label for="balance"><?php echo lang('Minimum Topup Amount'); ?></label>
              <input name="min_topup" type="number" step="any" class="form-control" id="min_topup" value="<?php echo $agent->min_topup; ?>">
             </div>

             <div class="form-group ">
              <label for="balance"><?php echo lang('Maximum Topup Amount'); ?></label>
              <input name="max_topup" type="number" step="any" class="form-control" id="max_topup" value="<?php echo $agent->max_topup; ?>">
             </div>
                        </div>
                        <div class="form-group col-sm-4 commission">
                            <label for="comission_type"><?php echo lang('Comission Type'); ?></label>
                            <select class="form-control" name="comission_type" id="comission_type" disabled>
                                <?php foreach (array('Percentage','FixedAmount','None') as $type) {
        ?>
                                <option value="<?php echo $type; ?>"><?php echo $type; ?></option>
                                <?php
    } ?>
                            </select>
                        </div>
                        <div class="form-group col-sm-4 commission">
                            <label for="comission_value"><?php echo lang('Comission Value'); ?></label>
                            <input name="comission_value" type="number" step="any" class="form-control"
                                id="comission_value" readonly>
                        </div>

                    </div>

                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label for="email"><?php echo lang('Email'); ?></label>
                            <input name="email" type="email" class="form-control" id="email"
                                value="<?php echo $agent->email; ?>">
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="password"><?php echo lang('Password'); ?></label>
                            <input name="password" type="text" class="form-control"
                                placeholder="<?php echo lang('Leave empty for no change'); ?>" id="password">
                        </div>
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        <?php echo lang('Close'); ?></button>
                    <button type="submit" class="btn btn-primary">
                        <?php echo lang('Submit'); ?></button>
                </div>
                <input type="hidden" name="reseller_type" value="<?php echo $agent->reseller_type; ?>">
            </form>
        </div>
    </div>
</div>

<script>
$(".permission").change(function() {
    var agentid = "<?php echo $this->uri->segment(4); ?>";
    if ($(this).is(":checked")) {
        var val = "1";
    } else {
        var val = "0";
    }
    var perms = $(this).attr("id");
    $.ajax({
        url: window.location.protocol + "//" + window.location.host + "/admin/agent/change_permission",
        type: "post",
        dataType: "json",
        success: function(data) {
            console.log(data);
        },
        data: {
            "agentid": agentid,
            "permission": perms,
            "val": val
        }
    });
});

$("#reseller_type").change(function() {
    var reseller_type = $("#reseller_type option:selected").val();
    if (reseller_type == "Postpaid") {
        $("#comission_type").val('Percentage');
        $('.balance').hide();
        $('.commission').show();
    } else {
        $("#comission_type").val('None');
        $('.balance').show();
        $('.commission').hide();
    }
});


<?php if (!getProductSell($this->session->cid, $_SESSION['reseller']['id'])) {
        ?>
    $(document).ready(function() {

    $('#products').modal('toggle');
    });
<?php
    } ?>
</script>

<div class="modal fade" id="products" tabindex="-1" role="dialog" aria-labelledby="products" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="post" action="<?php echo base_url(); ?>admin/agent/assign_product">
                <input type="hidden" name="agentid" value="<?php echo $agent->id; ?>">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                        <?php echo lang('Assign Product for this Agent'); ?>
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-sm-12">
                            <label for="comission_type"><?php echo lang('Please choose to which agent the current customer need to be
                                assigned'); ?></label>
                            <?php $prd = getProductAgents($this->uri->segment(4)); ?>
                            <?php if (getProductforAgents($this->session->cid)) {
        ?>
                            <?php foreach (getProductforAgents($this->session->cid) as $prod) {
            ?>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" name="productids[]" type="checkbox"
                                        value="<?php echo $prod->id; ?>" <?php if (in_array($prod->id, $prd)) {
                ?> checked<?php
            } ?>><?php echo $prod->name; ?></label>
                            </div>
                            <?php
        } ?>
                            <?php
    } ?>

                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        <?php echo lang('Close'); ?></button>
                    <button type="submit" class="btn btn-primary">
                        <?php echo lang('Submit'); ?></button>
                </div>
            </form>
        </div>
    </div>
</div>


<div class="modal fade" id="AgentTopup" tabindex="-1" role="dialog" aria-labelledby="AgentTopup" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="post" action="<?php echo base_url(); ?>admin/agent/topup">
                <input type="hidden" name="agentid" value="<?php echo $agent->id; ?>">
                <div class="modal-header">
                    <h5 class="modal-title" id="agentopupBalancd">
                        <?php echo lang('Add Credit to the Reseller'); ?>
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-sm-12">
                            <label for="Amount"><?php echo lang('Amount To be Added'); ?>
                                <small><?php echo lang('If you add minus valud it will deduct the current balance'); ?></small></label>
                            <input name="Amount" type="number" step="any" class="form-control" id="text" value="0.00">
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="Notes"><?php echo lang('Notes'); ?></label>
                            <textarea class="form-control" name="description"></textarea>
                        </div>

                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        <?php echo lang('Close'); ?></button>
                    <button type="submit" class="btn btn-primary">
                        <?php echo lang('Submit'); ?></button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="deleteAgent" tabindex="-1" role="dialog" aria-labelledby="deleteAgent" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="post" action="<?php echo base_url(); ?>admin/agent/delete">
                <input type="hidden" name="id" value="<?php echo $agent->id; ?>">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                        <?php echo lang('Delete Agent'); ?>
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-sm-12">
                            <label for="comission_type"><?php echo lang('Please choose to which agent the current customer need to be
                                assigned'); ?></label>
                            <select class="form-control" name="newagent">
                                <option value="0">
                                    <?php echo lang('None'); ?>
                                </option>
                                <?php foreach (get_agents($this->session->cid) as $a) {
        ?>
                                <?php if ($a->id != $this->uri->segment(4)) {
            ?>
                                <option value="<?php echo $a->id; ?>">
                                    <?php echo $a->agent; ?>
                                </option>
                                <?php
        } ?>
                                <?php
    } ?>
                            </select>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        <?php echo lang('Close'); ?></button>
                    <button type="submit" class="btn btn-primary">
                        <?php echo lang('Submit'); ?></button>
                </div>
            </form>
        </div>
    </div>
</div>


<div class="modal fade" id="assign_sim" tabindex="-1" role="dialog" aria-labelledby="assign_sim" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="post" action="<?php echo base_url(); ?>admin/agent/assign_simcards"
                enctype="multipart/form-data">
                <input type="hidden" name="agentid" value="<?php echo $agent->id; ?>">
                <input type="hidden" name="admin"
                    value="<?php echo $this->session->firstname." ".$this->session->lastname; ?>">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                        <?php echo lang('Upload Simcards'); ?>
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p><?php echo lang('if you wish your Reseller to be able to assign and activate simcards, you will need to assign simcard to their accounts'); ?><br />.
                    </p>
                    <div class="form-control">

                        <input type="file" name="userfile" class="form-control">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-warning"
                        data-dismiss="modal"><?php echo lang('Close'); ?></button>
                    <button type="submit" class="btn btn-default btn-primary"><?php echo lang('Submit'); ?></button>
                </div>

            </form>
        </div>

    </div>
</div>


<div class="modal fade" id="send_password" tabindex="-1" role="dialog" aria-labelledby="send_password"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="post" action="<?php echo base_url(); ?>admin/agent/send_password">
                <input type="hidden" name="resellerid" value="<?php echo $agent->id; ?>">
                <input type="hidden" name="admin"
                    value="<?php echo $this->session->firstname." ".$this->session->lastname; ?>">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                        <?php echo lang('Reset & Send reseller credential'); ?>
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p><?php echo lang('This will reset current password of reseller and send him new password'); ?><br />.
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-warning"
                        data-dismiss="modal"><?php echo lang('Cancel'); ?></button>
                    <button type="button" class="btn btn-default btn-primary" onclick="send_pass()"><?php echo lang('Send
                        Password'); ?></button>
                </div>
            </form>
        </div>

    </div>
</div>



<div class="modal fade" id="suspend_reseller" tabindex="-1" role="dialog" aria-labelledby="suspend_reseller"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="post" action="<?php echo base_url(); ?>admin/agent/suspend_reseller">
                <input type="hidden" name="resellerid" value="<?php echo $agent->id; ?>">
                <input type="hidden" name="admin"
                    value="<?php echo $this->session->firstname." ".$this->session->lastname; ?>">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                        <?php echo lang('Suspend Reseller'); ?>
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p><?php echo lang('This will disallow reseller to login until you unsuspend him/her'); ?><br />.
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-warning"
                        data-dismiss="modal"><?php echo lang('Cancel'); ?></button>
                    <button type="button" class="btn btn-default btn-primary"
                        onclick="suspend()"><?php echo lang('Suspend'); ?></button>
                </div>
            </form>
        </div>

    </div>
</div>



<div class="modal fade" id="unsuspend_reseller" tabindex="-1" role="dialog" aria-labelledby="unsuspend_reseller"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="post" action="<?php echo base_url(); ?>admin/agent/unsuspend_reseller">
                <input type="hidden" name="resellerid" value="<?php echo $agent->id; ?>">
                <input type="hidden" name="admin"
                    value="<?php echo $this->session->firstname." ".$this->session->lastname; ?>">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                        <?php echo lang('Upload Simcards'); ?>
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p><?php echo lang('This will allow reseller to login'); ?><br />.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-warning"
                        data-dismiss="modal"><?php echo lang('Cancel'); ?></button>
                    <button type="button" class="btn btn-default btn-primary"
                        onclick="unsuspend()"><?php echo lang('Unsuspend'); ?></button>
                </div>
            </form>
        </div>

    </div>
</div>


<script>
function send_pass() {
    $.ajax({
        type: "POST",
        dataType: 'json',
        url: '<?php echo base_url();?>admin/agent/send_password',
        data: {
            resellerid: '<?php echo $this->uri->segment(4); ?>'
        }, // serializes the form's elements.
        success: function(data) {
            if (data.result) {

                window.location.href =
                    '<?php echo base_url(); ?>admin/agent/detail/<?php echo $this->uri->segment(4); ?>';

            } else {

                alert(data.message);
            }
        }
    });
}

function suspend() {
    $.ajax({
        type: "POST",
        dataType: 'json',
        url: '<?php echo base_url();?>admin/agent/suspend_reseller',
        data: {
            resellerid: '<?php echo $this->uri->segment(4); ?>'
        }, // serializes the form's elements.
        success: function(data) {
            if (data.result) {

                window.location.href =
                    '<?php echo base_url(); ?>admin/agent/detail/<?php echo $this->uri->segment(4); ?>';

            } else {

                alert(data.message);
            }
        }
    });
}

function DeleteSimcard(id) {
    var t = confirm("Are you sure?");
    if (t) {
        $.ajax({
            type: "POST",
            dataType: 'json',
            url: '<?php echo base_url();?>admin/agent/delete_simcard/' + id,
            data: {
                resellerid: '<?php echo $this->uri->segment(4); ?>'
            }, // serializes the form's elements.
            success: function(data) {
                if (data.result) {
                    window.location.href =
                        '<?php echo base_url(); ?>admin/agent/detail/<?php echo $this->uri->segment(4); ?>';
                } else {

                    alert('<?php echo lang('error when deleting simcard'); ?>');
                }
            }
        });

    }

}

function showStats() {
    $('#stats').show('slow');
    $.ajax({
        type: "POST",
        dataType: 'json',
        url: '<?php echo base_url();?>admin/agent/getStatistic/<?php echo $this->uri->segment(4); ?>',
        data: {
            agentid: '<?php echo $this->uri->segment(4); ?>'
        }, // serializes the form's elements.
        success: function(data) {
            console.log(data.counter);
            $('.s_today').html(data.counter.subscription.today);
            $('.s_week').html(data.counter.subscription.week);
            $('.s_month').html(data.counter.subscription.month);
            $('.s_year').html(data.counter.subscription.year);

            $('.t_today').html(data.counter.topup.today + '/<?php echo $setting->currency; ?>' + data.amount
                .topup.today);
            $('.t_week').html(data.counter.topup.week + '/<?php echo $setting->currency; ?>' + data.amount
                .topup.week);
            $('.t_month').html(data.counter.topup.month + '/<?php echo $setting->currency; ?>' + data.amount
                .topup.month);
            $('.t_year').html(data.counter.topup.year + '/<?php echo $setting->currency; ?>' + data.amount
                .topup.year);

            $('.b_today').html(data.counter.bundle.today + '/<?php echo $setting->currency; ?>' + data
                .amount.bundle.today);
            $('.b_week').html(data.counter.bundle.week + '/<?php echo $setting->currency; ?>' + data.amount
                .bundle.week);
            $('.b_month').html(data.counter.bundle.month + '/<?php echo $setting->currency; ?>' + data
                .amount.bundle.month);
            $('.b_year').html(data.counter.bundle.year + '/<?php echo $setting->currency; ?>' + data.amount
                .bundle.year);

        }
    });


}
function InstantActivation(msisdn){

    alert('This feature will be available soon!');
}
function unsuspend() {

    $.ajax({
        type: "POST",
        dataType: 'json',
        url: '<?php echo base_url();?>admin/agent/unsuspend_reseller',
        data: {
            resellerid: '<?php echo $this->uri->segment(4); ?>'
        }, // serializes the form's elements.
        success: function(data) {
            if (data.result) {
                window.location.href =
                    '<?php echo base_url(); ?>admin/agent/detail/<?php echo $this->uri->segment(4); ?>';
            } else {

                alert(data.message);
            }
        }
    });

}
</script>

<script>
$(document).ready(function() {
    var reseller_type = $("#reseller_type option:selected").val();
    if (reseller_type == "Postpaid") {
        $("#comission_type").val('Percentage');
        $('.balance').hide();
        $('.commission').show();
    } else {
        $("#comission_type").val('None');
        $('.balance').show();
        $('.commission').hide();
    }
    $.getJSON(window.location.protocol + '//' + window.location.host + '/admin/complete/get_lang', function(
        data) {
        var x = new Date();
        var y = x.getFullYear().toString();
        var m = (x.getMonth() + 1).toString();
        var d = x.getDate().toString();
        (d.length == 1) && (d = '0' + d);
        (m.length == 1) && (m = '0' + m);
        var date = y + '-' + m + '-' + d;
        $('#simcardtable').DataTable({
            "autoWidth": false,
            "processing": true,
            "orderCellsTop": true,
            "ordering": true,
            "serverSide": true,
            "colReorder": true,
            "ajax": window.location.protocol + '//' + window.location.host +
                '/admin/table/get_agent_simcards/<?php echo $this->uri->segment(4); ?>',
            "aaSorting": [
                [1, 'desc']
            ],
            "language": {
                "url": window.location.protocol + '//' + window.location.host +
                    "/assets/clear/js/datatables/lang/" + data.result + ".json"
            },
            "fnRowCallback": function(nRow, aData, iDisplayIndex) {
                if (aData[1] > 0) {
                    $('td:eq(1)', nRow).html('<a href="' + window.location.protocol + '//' +
                        window.location.host + '/admin/subscription/detail/' + aData[
                            1] + '">Assigned</a>');
                     $('td:eq(3)', nRow).html('');
                } else {

                    $('td:eq(0)', nRow).html(aData[0] +
                        ' <a href="#" onclick="DeleteSimcard(' + aData[4] +
                        ')"><i class="fa fa-trash text-danger"></i></a>');
                    $('td:eq(1)', nRow).html('FREE');
                     $('td:eq(3)', nRow).html('<button type="button" onclick="InstantActivation('+aData[2]+');"><?php echo lang('Activate'); ?></a>');

                }



                return nRow;
            },
        });
        $('#history').DataTable({
"autoWidth": false,
"processing": true,
"orderCellsTop": true,
"ordering": true,
"serverSide": true,
"colReorder": true,

"ajax": window.location.protocol + '//' + window.location.host + '/admin/table/get_reseller_credit_usage/<?php echo $this->uri->segment(4); ?>',
"aaSorting": [[0, 'desc']],
"language": {
"url": window.location.protocol + '//' + window.location.host +"/assets/clear/js/datatables/lang/" + data.result + ".json"
},
"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
    if(aData[1] == "bundle"){
        $('td:eq(1)', nRow).html(aData[1]+': '+aData[6]);
    }
$('td:eq(2)', nRow).html('- <?php echo $setting->currency; ?>' + aData[2]);
return nRow;
},
});

    });
});
</script>

<script src="https://cdn.datatables.net/buttons/1.6.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.0/js/buttons.html5.min.js"></script>