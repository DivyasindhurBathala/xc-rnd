<script>
$( document ).ready(function() {
$('#loadingx1').show('slow');
$('#loadingx2').show('slow');
$('#loadingx3').show('slow');
$('#loadingx4').show('slow');
$('#loadingx5').show('slow');
$.ajax({
url: window.location.protocol + '//' + window.location.host + '/admin/complete/get_service_detail/<?php echo $this->uri->segment(4); ?>',
dataType: 'json',
success: function (data) {

$('#loadingx1').hide('slow');
$('#loadingx2').hide('slow');
$('#loadingx3').hide('slow');
$('#loadingx4').hide('slow');
$('#loadingx5').hide('slow');
if(data.bundles){
$('#tbody_bundlesx').show();
//$('#datax').html(data.cdr.data+' Bytes');
//$('#smsx').html(data.cdr.sms +' sms');
//$('#voicex').html(data.cdr.voice);
if(data.bundles.length > 1){
data.bundles.forEach(function (b) {
if(b.Percentage > 90){
var text = "text-danger";
var coli = "bg-danger";
}else{
var text = "text-light";
var coli = "bg-success";
}
$('#tbody_bundlesx').append(' <tr><td><i class="fa fa-'+b.icon+'"></i></td><td>'+b.szBundle+'</td><td>'+b.ValidFrom+'</td> <td>'+b.UsedValue+' / '+b.AssignedValue+'</td><td><div class="progress" style="height:30px"><div style="height:30px" class="progress-bar '+coli+'" role="progressbar" style="width: '+b.Percentage+'%;" aria-valuenow="'+b.Percentage+'" aria-valuemin="0" aria-valuemax="100"><span class="'+text+'">'+b.Percentage+'%</span></div> </div></td></tr>');
});
}else{
if(data.bundles.Percentage > 90){
var text = "text-danger";
var coli = "bg-danger";
}else{
var text = "text-light";
var coli = "bg-success";
}
$('#tbody_bundlesx').append(' <tr><td><i class="fa fa-'+data.bundles.icon+'"></i></td><td>'+data.bundles.szBundle+'</td><td>'+data.bundles.ValidFrom+'</td> <td>'+data.bundles.UsedValue+' / '+data.bundles.AssignedValue+'</td><td><div class="progress" style="height:30px"><div style="height:30px" class="progress-bar '+coli+'" role="progressbar" style="width: '+data.bundles.Percentage+'%;" aria-valuenow="'+data.bundles.Percentage+'" aria-valuemin="0" aria-valuemax="100"><span class="'+text+'">'+data.bundles.Percentage+'%</span></div> </div></td></tr>');
}
}
if(data.packages){
$('#packagesx').show();
if(data.packages.length > 1 && data.packages.length != 0){
data.packages.forEach(function (b) {
  console.log(b);

if(b.Available == "1"){
var t =' checked';
}else{
var t = '';
}
$('#packagesx').append(' <tr> <td>'+b.CallModeDescription+' <a href="#" data-toggle="tooltip" data-placement="top" title="'+b.CallModeDescriptionTooltip+'"><i class="fa fa-info-circle"></i></a></td><td><div class="custom-control custom-switch float-right"><input type="checkbox" class="custom-control-input packageid" value="'+b.PackageDefinitionId+'" id="'+b.PackageDefinitionId+'"'+t+'> <label class="custom-control-label" for="'+b.PackageDefinitionId+'"></label></div></td> </tr>');
});
}else{

  console.log(data.packages);
if(data.packages.Available == "1"){
var t =' checked';
}else{
var t = '';
}
$('#packagesx').append(' <tr> <td>'+data.packages.CallModeDescription+' <a href="#" data-toggle="tooltip" data-placement="top" title="'+data.packages.CallModeDescriptionTooltip+'"><i class="fa fa-info-circle"></i></a></td><td><div class="custom-switch custom-switch-label-onoff"><input class="custom-switch-input packageid" id="'+data.packages.PackageDefinitionId+'" type="checkbox"'+t+'><label class="custom-switch-btn" for="'+data.packages.PackageDefinitionId+'"></label> </div></td> </tr>');
}
$("#packagesx").append('<script> $(".packageid").change(function() { var userid = "<?php echo $service->userid; ?>"; var serviceid = "<?php echo $service->id; ?>"; var sn = "<?php echo $mobile->msisdn_sn; ?>"; var id = $(this).attr("id"); var msisdn = "<?php echo trim($service->domain); ?>"; if($(this).is(":checked")) { var val = "1"; }else{ var val = "0"; } $.ajax({ url: window.location.protocol + "//" + window.location.host + "/client/service/change_packagesetting", type: "post", dataType: "json", success: function (data) {  }, data: {"sn": sn, "id": id, "val":val, "msisdn":msisdn, "userid":userid, "serviceid":serviceid} }); });<\/script>');
}
/*
if(data.bundle_list){
$('#bundle_listx').show();
if(data.bundle_list.length > 1 && data.bundle_list.length != 0){
data.bundle_list.forEach(function (d) {
$('#bundle_listx').append('<tr> <td>'+d.name.Description+'</td><td>'+d.ValidFrom.slice(0, -15)+'</td> <td>'+d.ValidUntil.slice(0, -15)+'</td><td>'+d.UsedValue.slice(0, -7)+'/'+d.AssignedValue.slice(0, -7)+'  </td><td><div class="progress"> <div class="progress-bar" role="progressbar" style="width: '+d.Percentage.toFixed(2)+'%;" aria-valuenow="'+d.Percentage.toFixed(2)+'" aria-valuemin="0" aria-valuemax="100"></div> </div></td></tr>');
});
}else{
}
}
*/
}
});
});
</script>
<?php //echo print_r($service); ?>
<div class="card">
  <div class="card-header bg-primary text-light">
    <?php echo lang('Subscription'); ?>: <?php echo $service->packagename; ?>
        <div class="float-right text-white"><?php echo calculate_contractend_date($service->date_contract, $service->contract_terms); ?></div>
  </div>
  <div class="card-body">
    <div class="row">
      <div class="col-sm-6  table-responsive">
        <table class="table table-striped">
          <tr>
            <td><?php echo lang('MSISDN'); ?></td>
            <td align="right"><?php echo $mobile->msisdn; ?></td>
          </tr>
          <tr>
            <td><?php echo lang('SIMCARD NO'); ?></td>
            <td align="right"><?php echo $mobile->msisdn_sim; ?></td>
          </tr>
          <tr>
            <td><?php echo lang('VOICEMAIL LANGUAGE'); ?></td>
            <td align="right">
              <select id="language" class="form-control" dir="rtl">
                <?php foreach (array('3' => 'Dutch', '1' => 'English', '2' => 'French') as $key => $language) {?>
                <option value="<?php echo $key; ?>"<?php if ($key == $mobile->msisdn_languageid) {?> selected<?php }?>><?php echo lang($language); ?></option>
                <?php }?>
              </select>
            </td>
          </tr>
          <tr>
            <td>PIN</td>
            <td align="right"><?php echo $mobile->msisdn_pin; ?></td>
          </tr>
          <tr>
            <td>PUK1</td>
            <td align="right"><?php echo $mobile->msisdn_puk1; ?></td>
          </tr>
          <tr>
            <td>PUK2</td>
            <td align="right"><?php echo $mobile->msisdn_puk2; ?></td>
          </tr>
        </table>
        <hr />

        <?php for ($i = 1; $i < 6; $i++) { ?>
        <a class="btn btn-success btn-block" href="<?php echo base_url(); ?>client/service/export_livecdr_byMonth/<?php echo trim($service->domain); ?>/<?php echo $service->id; ?>/<?php echo trim($sn->SN); ?>/<?php echo date('Y-m', strtotime("-$i month")); ?>"><i class="fa fa-list"></i> <?php echo lang('Show Call History'); ?> <?php echo date(', Y-m', strtotime("-$i month")); ?></a>
      <?php } ?>
      
          <!-- //  <button class="btn btn-success" type="button" data-toggle="modal" data-target="#CdrModal"><i class="fa fa-list"></i> <?php echo lang('Show Call History'); ?></button> -->
      </div> 
      <div class="col-sm-6  table-responsive">
        <div class="card z-depth-1">
          <div class="card-header"><h4 class="card-title"><i class="fa fa-cog"></i> <?php echo lang('Service Setting'); ?></h4></div>
          <div class="card-body">
            <table class="table table-striped" id="packagesx">
              <div id="loadingx2" style="display:none;">
                <img src="<?php echo base_url(); ?>assets/img/lg.rotating-balls-spinner.gif" height="100">
              </div>
            </table>
            <hr />
            <h3><?php echo lang('Legend'); ?></h3>
            <div class="row">
            <div class="col-6 text-success">
                <?php echo lang('ON'); ?>
            </div>
                <div class="col-6">
                   <div class="custom-control custom-switch float-right"><input type="checkbox" class="custom-control-input" checked=""> <label class="custom-control-label"></label></div>
             </div>
            </div>
             
              <div class="row text-danger">
            <div class="col-6">
                <?php echo lang('OFF'); ?>
            </div>
                <div class="col-6">
                   <div class="custom-control custom-switch float-right"><input type="checkbox" class="custom-control-input"> <label class="custom-control-label"></label></div>
             </div>
            </div>
             
            <div class="text-center">
              <!--
              <button id="suspend" class="btn btn-primary btn-md"><i class="fa fa-recycle"></i> Suspend</button>
              <button id="suspend" class="btn btn-primary btn-md"><i class="fa fa-fire"></i> Terminate</button>
              <button id="suspend" class="btn btn-primary btn-md"><i class="fa fa-list"></i> Logs</button>
              <button id="suspend" class="btn btn-primary btn-md"><i class="fa fa-arrow-circle-up"></i> Move Service</button>
              -->
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
    </div>
    <hr />
    <div class="row">
      <div class="col-sm-12">
        <div class="card">
          <div class="card-body text-center">
            <button class="btn btn-success" type="button" data-toggle="modal" data-target="#buyBundle"><i class="fa fa-shopping-bag"></i> <?php echo lang('Purchase Bundle'); ?></button>
            <button class="btn btn-success" type="button" data-toggle="modal" data-target="#CdrModal"><i class="fa fa-list"></i> <?php echo lang('Call History This Month'); ?></button>
            <button class="btn btn-success" type="button" data-toggle="modal" data-target="#StolenModal"><i class="fa fa-mobile"></i> <?php echo lang('Report Stolen Phone'); ?></button>
          </div>
        </div>
      </div>
    </div>
    <?php if ($productchange) {?>
    <hr />
    <div class="row">
      <div class="col-sm-12">
        <div class="card">
          <div class="card-body text-center">
            <div class="alert alert-warning" role="alert">
              <center>U heeft lopende actie: productverandering op <?php echo $productchange->date_commit; ?></center>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php }?>

  </div>
</div>

<?php if ($showusage) {
    ?>
<?php if ($bundles) {
        ?>
<div class="row top-buffer">
  <div class="col-md-12">
    <div class="card-header bg-primary text-light">
      <?php echo lang('Bundels (waarbij uw vaste bundels zijn gebaseerd op kalendermaanden)'); ?>
    </div>
    <div class="table-responsive card">
      <div class="card-body" id="usageloading" style="display:none;">
        <center><img src="<?php echo base_url(); ?>assets/img/lg.rotating-balls-spinner.gif"></center>
      </div>
      <div class="card-body" id="usagebox">
        <div class="row">
          <?php foreach ($bundles as $index => $row) {?>
          <div class=" col-md-4">
            <div class="card bg-default">
              <div class="card-header text-center text-light bg-primary"><?php echo $row->szBundle; ?> </div>
              <div class="card-body text-dark text-center">
                <h5><?php echo str_replace('.', ',', $row->UsedValue) . ' / ' . str_replace('.', ',', $row->AssignedValue); ?> </h5>
              </div>
            </div>
          </div>
          <?php if ($index == 4) {?>
          <hr />
          <?php }?>
          <?php }?>
        </div>
      </div>
    </div>
  </div>
</div>
<?php }?>
<?php }?>
<!-- The Stolen Modal -->
<div class="modal fade" id="StolenModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <form method="post" action="<?php echo base_url(); ?>client/service/mobile_stolen">
        <input type="hidden" name="SN" value="<?php echo $sn->SN; ?>">
        <input type="hidden" name="SimCardNbr" value="<?php echo $mobile->msisdn_sim; ?>">
        <input type="hidden" name="userid" value="<?php echo $service->userid; ?>">
        <input type="hidden" name="serviceid" value="<?php echo $service->id; ?>">
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title"><?php echo lang('Report Stolen Phone'); ?>?</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <!-- Modal body -->
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-12">
              <div class="form-group">
                <fieldset>
                  <label class="control-label" for="type"> <?php echo lang('Request SIM Replacement'); ?></label>
                  <select class="form-control" id="ticket" name="ticket">
                    <option value="1"><?php echo lang('NO'); ?></option>
                    <option value="2"><?php echo lang('YES'); ?></option>
                  </select>
                </fieldset>
              </div>
            </div>
            <div class="col-sm-12">
              <div class="form-group">
                <fieldset>
                  <label class="control-label" for="msisdn"><?php echo lang('CLI(Number)'); ?></label>
                  <input  name="msisdn" class="form-control" type="text" value="<?php echo $service->domain; ?>" readonly>
                </fieldset>
              </div>
            </div>
          </div>
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary" data-dismiss="modal"><?php echo lang('Blokkeren'); ?></button>
        </div>
      </form>
    </div>
  </div>
</div>
<div class="modal fade" id="CdrModal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title"><?php echo lang('Call detail Record'); ?></h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!-- Modal body -->
      <div class="modal-body">
        <div class="row">
          <div class="col-sm-12">
            <table class="table table-striped table-lightfont table-responsive" width="100%"  id="cdr">
              <thead>
                <tr>
                  <th><?php echo lang('Date'); ?></th>
                  <th><?php echo lang('Number'); ?></th>
                  <th><?php echo lang('Description'); ?></th>
                  <th><?php echo lang('Type'); ?></th>
                  <th><?php echo lang('Duration'); ?></th>
                  <th><?php echo lang('Cost'); ?></th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <a class="btn btn-md btn-primary" href="<?php echo base_url(); ?>client/service/export_livecdr/<?php echo trim($service->domain); ?>/<?php echo $service->id; ?>/<?php echo trim($sn->SN); ?>"><i class="fa fa-file-excel"></i> <?php echo lang('Export Excel'); ?></a>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="buyBundle">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title"><i class="fa fa-shopping-bag"></i> <?php echo lang('Please Choose Bundle'); ?></h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!-- Modal body -->
      <div class="modal-body">
        <div class="row">
          <?php foreach (getAddonSellClient('mobile', $this->session->cid) as $row) {?>

          <?php if ($row->id != "18") {?>
          <div class="col-md-6 top-buffer">
            <div class="card bg-primary">
              <div class="card-header  text-light text-center"> <?php echo str_replace('Option ', '', $row->name); ?></div>
              <div class="card-body text-light text-center">
                <h1><?php echo ' &euro;' . str_replace('.', ',', number_format($row->recurring_total, 2)); ?></h1>
                <button type="button" onclick="buyBundle('<?php echo $row->id; ?>')" class="btn btn-info btn-sm"><?php echo lang('Order'); ?></button>
              </div>
            </div>
          </div>
          <hr />


          <?php }?>
          <?php }?>
        </div>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="buyBundle2">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title"><i class="fa fa-shopping-bag"></i> <?php echo lang('Bundle Order Confirmation'); ?></h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!-- Modal body -->
      <form method="post" action="<?php echo base_url(); ?>client/service/buybundle/<?php echo $service->id; ?>">
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12 top-buffer">
              <div class="card bg-primary">
                <div class="card-header  text-light text-center" id="productname"></div>
                <div class="card-body text-light text-center">
                  <h1 id="productprice"></h1>
                  <input type="hidden" name="bundleid" value="" id="idbundle">
                  <div class="form-group">
                    <label><?php echo lang('Date Start'); ?></label>
                    <input type="text" name="from" id="pickdate6" class="form-control col-md-2 offset-md-5 text-center" value="<?php echo date('Y-m-d'); ?>" required>
                  </div>
                 <!-- <div class="form-group">
                    <label><?php echo lang('Are you sure that you wish to order this Package'); ?><br><?php echo lang('Please type \'Yes i am sure\''); ?></label>
                    <input type="text" id="confirm" class="form-control col-md-4 offset-md-4 text-center" required>
                  </div>-->
                  
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-danger btn-md" data-dismiss="modal" type="button"><?php echo lang('Annuleren'); ?></button> <button class="btn btn-success btn-md" id="finalize" type="submit"><?php echo lang('Submit'); ?></button>
      
        </div>
        <input type="hidden" name="serviceid" value="<?php echo $service->id; ?>">
      </form>
    </div>
  </div>
</div>
<div class="lewat"></div>
<script>
$(document).ready(function() {
$( "#confirm" ).keyup(function() {
var text = $('#confirm').val();
if(text == '<?php echo lang('Yes i am sure'); ?>'){
$('#finalize').prop('disabled', false);
}else{
$('#finalize').prop('disabled', true);
}
});
$('#language').change(function() {
var lang = $('#language').val();
var sn = '<?php echo $sn->SN; ?>';
var id = '<?php echo $this->uri->segment(4); ?>'
var serviceid =  '<?php echo $service->id; ?>';
var msisdn = '<?php echo trim($service->domain); ?>';
$.ajax({
url: '<?php echo base_url(); ?>client/service/update_sim_language',
type: 'post',
dataType: 'json',
data: {'sn': sn,  'msisdn':msisdn, 'language':lang, 'serviceid': serviceid},
success: function (data) {
alert(data.message);
}
});
});
$('.packageid').click(function() {
var sn = '<?php echo $sn->SN; ?>';
var id = $(this).attr("id");
var serviceid =  '<?php echo $service->id; ?>';
var msisdn = '<?php echo trim($service->domain); ?>';
if($(this).is(":checked")) {
var val = "1";
//var returnVal = confirm("Are you sure?");
//$(this).attr("checked", returnVal);
}else{
var val = "0";
}
$.ajax({
url: '<?php echo base_url(); ?>client/service/change_packagesetting',
type: 'post',
dataType: 'json',
success: function (data) {

},
data: {'sn': sn, 'id': id, 'val':val, 'msisdn':msisdn, 'serviceid':serviceid}
});
});
});
</script>
<script>
$(document).ready(function()
{
$.getJSON(window.location.protocol + '//' + window.location.host + '/admin/complete/get_lang', function(data) {
var tableData = <?php echo json_encode($cdr['cdr']); ?>;
$('#cdr').DataTable({
"autoWidth": false,
"data": tableData,
"aaSorting": [[0, 'desc']],
"columns": [
{ "data": "Begintime" },
{ "data": "MaskDestination" },
{ "data": "DestinationCountry" },
{ "data": "TrafficTypeId" },
{ "data": "DurationConnection" },
{ "data": "CurNumUnitsUsed" },
],
"language": {
"url": window.location.protocol + '//' + window.location.host +"/assets/clear/js/datatables/lang/" + data.result + ".json"
},
"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
if(aData["TrafficTypeId"] == "0"){
$('td:eq(3)', nRow).html('-');
}
if(aData["TrafficTypeId"] == 1){
  if(aData["TypeCallId"] == 2){
              $('td:eq(3)', nRow).html('<strong>Voice Incoming</strong>');
            }else{
              $('td:eq(3)', nRow).html('<strong>Voice</strong>');
            } 
}else if(aData["TrafficTypeId"] == 2){
  $('td:eq(4)', nRow).html(aData['Megabit']);
$('td:eq(3)', nRow).html('<strong>Data</strong>');
}else if(aData["TrafficTypeId"] == 5){
$('td:eq(3)', nRow).html('<strong>SMS</strong>');
}
/*
$('td:eq(0)', nRow).html('<a href="'+window.location.protocol + '//' + window.location.host + '/admin/helpdesk/detail/' + aData[6] + '"><strong>' + aData[0] + '</strong></a>');
if(aData[7] > 0){
$('td:eq(1)', nRow).html('<a href="'+window.location.protocol + '//' + window.location.host + '/admin/client/detail/' + aData[7] + '"><strong>' + aData[1] + '</strong></a>');
}
*/
//$('td:eq(1)', nRow).html('<a href="https://invoice.xmusix.eu/admin/clientssummary.php?userid=' + aData[5] + '">' + aData[1] + '</a>');
return nRow;
},
});
});
});
</script>
<script>
function buyBundle(id){
$('#idbundle').val(id);
$.ajax({
url: '<?php echo base_url(); ?>client/service/getAddonid',
type: 'post',
dataType: 'json',
success: function (data) {
  //log(data);
$('#confirm').val('');
$("#productprice").html(data.name_desc+'<br />[&euro;'+data.recurring_total+']');
},
data: {
'id': id
}
});
$('#buyBundle').modal('hide');
$('#buyBundle2').modal('show');
}
</script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js" type="text/javascript"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css">
<pre>
</pre>
<div class="top-buffer"></div>
<?php // print_r($cdr);?>

<!-- <a href="#" data-toggle="tooltip" title="Long text lorem ipsum here!"><i class="fa fa-info-circle"></i></a> -->