<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>
        <?php echo lang('Porting On demand'); ?>
    </title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <!-- Font Awesome CSS-->
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/<?php echo $setting->default_theme; ?>/client/pod.css?version1.0">
    <!-- Tweaks for older IEs-->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
</head>

<body>

    <?php if (!empty($this->session->flashdata('error'))) {?>
    <div class="row">
        <div class="col-lg-12">
            <div class="alert alert-danger" role="alert">
                <strong class="text-center">
                    <?php echo $this->session->flashdata('error'); ?></strong>
            </div>
        </div>
    </div>
    <?php }?>
    <?php if (!empty($this->session->flashdata('success'))) {?>
    <div class="row">
        <div class="col-lg-12">
            <div class="alert alert-warning" role="alert">
                <strong class="text-center">
                    <?php echo $this->session->flashdata('success'); ?></strong>
            </div>
        </div>
    </div>
    <?php }?>

    <!-- Logo & Information Panel-->


<!--
    <div class="login-page">
    <img src="<?php echo $setting->logo_site; ?>" width="360">
    <br />
    <br />
    <div class="form">
      <form id="login-form" method="post" action="<?php echo base_url(); ?>client/portinondemand/confirm_code">
        <input type="number" value="0"  name="msisdn" required="" />
        <input type="text" value="" placeholder="SMS Code"  name="code" required="" />
        <button type="submit"><i class="fa fa-check"></i>
            <?php echo lang('Confirm Port In'); ?></button>
        <br />
        <br />
      </form>

      <p class="message">
        <?php echo lang('Please enter your phonenumber and your SMS Code that we have sent you!'); ?>
        <br />
        <?php if ($pod) { ?>
            <?php echo lang('You still have '); ?> <span class="text-danger"><?php echo 3-$pod->pod_counter; ?></span> <?php echo lang('time(s) left to request The code'); ?>

        <?php } ?>
        </p>
    </div>
  </div>


-->

    <!------ Include the above in your HEAD tag ---------->
    <?php
//print_r($setting);
    ?>
    <div class="container contact-form">
    <center>
            <img src="https://www.delta.nl/themes/delta/images/logo_delta.png" alt="Delta " />
        </center>
        <form id="reg-form" method="post">
        <h5>
         <center>
        <?php echo lang('Please enter your phonenumber and your SMS Code that we have sent you!'); ?>
        <br />
        <br />
        De Code is uit veiligheidsoverwegingen 15 minuten geldig.
<br />
Klik  <a href="<?php echo base_url(); ?>client/portinondemand/request_new"><?php echo lang('HIER'); ?> </a> om een nieuwe Code aan te vragen.
<br />
<br />
</center>
</h5>
            <div class="row">
                <div class="col-sm-3">
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
        <input type="text" name="msisdn" class="form-control" placeholder="Number" value="" id="msisdn" />
                    </div>


<br />
                    <div class="form-group">
                        <input maxlength="5" type="number" name="code" class="form-control" placeholder="Code" value="" id="code" />
                    </div>


                </div>
                <div class="col-sm-3">
                </div>

            </div>
            <br />
            <br />
<br />
            <div class="row">
                <div class="col-sm-3">
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <input type="button" id="submiti" class="btn btn-warning btn-md btn-block" value="  <?php echo lang('Confirm Port In'); ?>" />
                       <center> <img height="100" src="<?php echo base_url(); ?>assets/img/lg.recycle-spinner.gif" id="loader" style="display:none;"></center>
                    </div>
                </div>
                <div class="col-sm-3">
                </div>
            </div>




        </form>
    </div>

    <script>
        $('#submiti').click(function () {
            $('#submiti').hide();
            $('#loader').show();
         var number = $('#msisdn').val();
          var code =  $('#code').val();
            if(code == ''){
                alert('Code Must can not be empty');
                $('#submiti').show();
                $('#loader').hide();
                $('#code').focus();
                return;
            }else if(number == ''){
                alert('Number Must can not be empty');
                $('#submiti').show();
                $('#loader').hide();
                $('#msisdn').focus();
                return;
            }
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>client/portinondemand/confirm_code",
                dataType: 'json',
                data: {
                    'msisdn': number,
                    'code':  code,
                },
                success: function (response) {
                    console.log(response);
                    if (response.result == "success") {
                        window.location.href = "<?php echo base_url(); ?>client/portinondemand";
                    } else {
                        $('#loader').hide();
                        $('#submiti').show();
                        alert(response.message);
                    }
                    $('#loader').hide();
                }
            });

        });
    </script>
</body>

</html>