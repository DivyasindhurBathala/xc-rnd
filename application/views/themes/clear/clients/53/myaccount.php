<section class="tables">
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <form method="post" action="<?php echo base_url(); ?>client/dashboard/myaccount">
              <input type="hidden" name="id" value="<?php echo $client->id; ?>">
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <fieldset>
                      <label class="control-label" for="vat"><?php echo lang('VAT'); ?>.</label>
                      <input  name="vat" class="form-control ui-autocomplete-input ui-autocomplete-loading" id="vat" type="text" placeholder="<?php echo lang('Vat Number'); ?>" value="<?php echo $client->vat; ?>" readonly data-toggle="tooltip" data-placement="left" title="" data-original-title="Tooltip on left">

                      <span id="loading_data_icon"></span>
                    </fieldset>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <fieldset>
                      <label class="control-label" for="companyname"><?php echo lang('Companyname'); ?></label>
                      <input  name="companyname" class="form-control ui-autocomplete-input ui-autocomplete-loading" id="companyname" type="text" placeholder="<?php echo lang('Companyname'); ?>" value="<?php echo $client->companyname; ?>" readonly>
                      <span id="loading_data_icon"></span>
                    </fieldset>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <fieldset>
                      <label class="control-label" for="firstname"><?php echo lang('Firstname'); ?></label>
                      <input  name="firstname" class="form-control" id="firstname" type="text" placeholder="<?php echo lang('Firstname'); ?>"  value="<?php echo $client->firstname; ?>" readonly>
                    </fieldset>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <fieldset>
                      <label class="control-label" for="lastname"><?php echo lang('Lastname'); ?></label>
                      <input  name="lastname" class="form-control" id="lastname" type="text" placeholder="<?php echo lang('Lastname'); ?>"  value="<?php echo $client->lastname; ?>" readonly>
                    </fieldset>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-4">
                  <div class="form-group">
                    <fieldset>
                      <label class="control-label" for="address1"><?php echo lang('Address'); ?></label>
                      <input  name="address1" class="form-control" id="address1" type="text" placeholder="<?php echo lang('Address'); ?>"  value="<?php echo $client->address1; ?>" readonly>
                    </fieldset>
                  </div>
                </div>
                <div class="col-sm-2">
                  <div class="form-group">
                    <fieldset>
                      <label class="control-label" for="Postcode"><?php echo lang('Postcode'); ?></label>
                      <input  name="postcode" class="form-control" id="postcode" type="text" placeholder="<?php echo lang('Postcode'); ?>"  value="<?php echo $client->postcode; ?>" readonly>
                    </fieldset>
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="form-group">
                    <fieldset>
                      <label class="control-label" for="City"><?php echo lang('City'); ?></label>
                      <input  name="city" class="form-control" id="city" type="text" placeholder="<?php echo lang('City'); ?>"  value="<?php echo $client->city; ?>" readonly>
                    </fieldset>
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="form-group">
                    <fieldset>
                      <label for="exampleSelect1"><?php echo lang('Country'); ?></label>
                      <select class="form-control" id="country"  name="country" disabled>
                        <?php foreach (getCountries() as $key => $country) {?>
                        <option value="<?php echo $key; ?>"<?php if ($key == $client->country) {?> selected<?php }?>><?php echo getCountryNameLang($key, 'dutch'); ?></option>
                        <?php }?>
                      </select>
                    </fieldset>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-3">
                  <div class="form-group">
                    <fieldset>
                      <label for="exampleSelect1"><?php echo lang('Languages'); ?></label>
                      <select class="form-control" id="language" name="language" disabled>
                        <?php foreach (getLanguages() as $key => $lang) {?>
                        <option value="<?php echo $key; ?>"<?php if ($key == "dutch") {?> selected<?php }?>><?php echo lang($lang); ?></option>
                        <?php }?>
                      </select>
                    </fieldset>
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="form-group">
                    <fieldset>
                      <label class="control-label" for="Phonenumber"><?php echo lang('Phonenumber'); ?></label>
                      <input name="phonenumber" class="form-control" id="phonenumber" type="number" placeholder="Phonenumber"  value="<?php echo $client->phonenumber; ?>" readonly>
                    </fieldset>
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="form-group">
                    <fieldset>
                      <label class="control-label" for="Email"><?php echo lang('Email'); ?></label>
                      <input name="email"  class="form-control" id="email" type="email" placeholder="Email address"  value="<?php echo $client->email; ?>" readonly>
                    </fieldset>
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="form-group">
                    <fieldset>
                      <label class="control-label" for="NationalNr"><?php echo lang('Account Number'); ?></label>
                      <input name="nationalnr"  class="form-control" id="NationalNr" type="text" value="<?php echo $client->iban; ?>" readonly>
                    </fieldset>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-sm-12">
                  <div class="form-group">
                    <!-- Voor het aanpassen van Mijn gegevens, neem contact op met onze klantenservice op 0118 – 22 55 05 of mobile@delta.nl -- >
                    <?php echo lang('ContactUsTochange'); ?>
                    <!-- <button type="submit" class="btn btn-md btn-primary btn-block"><i class="fa fa-save"></i> <?php echo lang('Save Client Information'); ?></button> -->
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

</section>
<section class="tables" style="padding-top: 20px; padding-bottom: 30px;">
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">

            <center>  <?php echo lang('ContactUsTochange'); ?></center>
            </div>
          </div>
        </div>
      </div>
    </section>
