</div>
</div>
</div>
<div class="top-buffer"></div>
<?php if ($this->uri->segment(2) == "service" && $this->uri->segment(3) == "detail") {?>
  <div class="tsl-copybar-wrapper fixed-bottom">
<?php } else {?>
  <div class="tsl-copybar-wrapper fixed-bottom">
<?php }?>

<div class="container">
<div class="row">
<div class="tsl-logo col-md-2">
  <img src="<?php echo $setting->logo_site; ?>" alt="Home">
</div>

<div class="col-md-10">
  <div style="margin-top: 27px;">
    <div class="row">
    <div class="col-md-4">
      <a href="javascript:void(0);"><?php echo $setting->trademark_text; ?></a></div>
  <div class="col-md-8" align='right'>
<?php echo $setting->footer_link; ?>
</div>

</div>
</div>
</div>

</div>
</div>
</div>
<script type="text/javascript">
function confirmation_logout() {
var answer = confirm("<?php echo lang('Do you wish logout?'); ?>")
if (answer){
window.location.replace(window.location.protocol + '//' + window.location.host + '/client/auth/logout');
}
else{
console.log("logout cancelled ");
}
}
</script>
<script src="<?php echo base_url(); ?>assets/clear/js/bootstrap.min.js"></script>
 <?php if ($_SESSION['cid'] == "53") {?>
  <?php if (!empty($_SESSION['logged'])) {?>
  <?php if (!has_sso($_SESSION['client']['id'])) {?>

<?php if (!empty($_SESSION['client']['sso'])) {?>

<?php if ($_SESSION['client']['sso'] != 1) {?>

 <script>
  $( document ).ready(function() {
  $('#deltapd').modal({backdrop: 'static', keyboard: false});
});
</script>
<?php }?>
<?php }?>
  <?php }?>
<?php }?>

     <?php }?>
<?php //print_r($_SESSION);?>
<!-- Modal -->
<div class="modal fade" id="deltapd" tabindex="-1" role="dialog" aria-labelledby="deltapdTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header bg-primary text-light">
        <h5 class="modal-title" id="exampleModalLongTitle"><?php echo lang('Link Your Account to Delta Account'); ?></h5>

      </div>
      <form id="link2delta" method="post" action="<?php echo base_url(); ?>client/dashboard/link2delta">
        <input type="hidden" name="userid" value="<?php echo $_SESSION['client']['id']; ?>">
      <div class="modal-body">
           <?php if (!empty($this->session->flashdata('error'))) {?>
      <div class="row">
        <div class="col-sm-12">
          <div class="alert alert-danger" role="alert">
            <strong class="text-center"><center><?php echo $this->session->flashdata('error'); ?></center></strong>
          </div>
        </div>
      </div>
      <?php }?>
<div class="form-group">
  <label><?php echo lang('Username'); ?></label>
  <input type="text" name="username" class="form-control" autocomplete="new-username" value="" required>
</div>
  <small><?php //echo lang('If you click Do not link my account, you would not be asked anymore'); ?></small>
      </div>
      <div class="modal-footer">
        <button type="button" id="nolink" class="btn btn-secondary" data-dismiss="modal"><?php echo lang('Do not link my account'); ?></button>
        <button type="submit" id="yeslink" class="btn btn-primary"><?php echo lang('Link my account'); ?></button>
      </div>
    </form>
    </div>
  </div>
</div>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script>

  $('#nolink').click(function (){
  $.ajax({
  url: '<?php echo base_url(); ?>client/dashboard/stopasking',
  dataType: 'json',
  type: 'post',
  data: {
  userid: '<?php echo $_SESSION['client']['id']; ?>'
  },
  success: function(data) {

  },
  error: function(errorThrown) {}
  });
});

</script>
<script>
  var unloadHandler = function(e){
  $.ajax({
  url: '<?php echo base_url(); ?>client/dashboard/remove_session',
  dataType: 'json',
  type: 'post',
  data: {
  userid: '<?php echo $_SESSION['client']['id']; ?>'
  },
  success: function(data) {

  },
  error: function(errorThrown) {}
  });
  };
window.unload = unloadHandler;
</script>

</html>
