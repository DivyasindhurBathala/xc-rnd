<div class="row">
  <div class="col-md-6" width="100">
    <div class="card bg-primary">
      <div class="card-header  text-light text-center"><?php echo lang('Total This Month Usage'); ?></div>
      <div class="card-body text-light text-center">
        <h1>&euro;
            <?php //echo $consumption; ?>
            <?php if (!empty($consumption)) {?>
                <?php echo str_replace('.', ',', number_format($consumption, 2)); ?>
            <?php } else {?>
              <?php echo '0.00'; ?>
            <?php }?>
            </h1>
      </div>
    </div>
  </div>
  
  <div class="col-md-6">
    <div class="card bg-warning">
      <div class="card-header text-light text-center"><?php echo lang('Total Services'); ?></div>
      <div class="card-body text-light text-center">
        <h1><?php echo $stats->services; ?></h1>
      </div>
    </div>
  </div>


<div class="col-md-12" style="padding-top: 13px;">
    <div class="card bg-primary">
     
      <div class="card-body text-light text-center">
       <button class="btn btn-lg btn-light" data-toggle="modal" data-target="#buyBundle" type="button"><?php echo lang('Buy Bundle'); ?></button>
       <button class="btn btn-lg btn-light" data-toggle="modal" data-target="#reloadCredit" type="button"><?php echo lang('Topup Credit'); ?></button>
      </div>
    </div>
  </div>
</div>

<div class="row" style="padding-top: 13px;">
  <?php if($setting->mage_invoicing){ ?>
  <div class="col-md-12">
    <div class="card-header bg-primary text-light"><?php echo lang('List Invoices'); ?></div>
    <div class="card">
      <div class="card-body">
        <table class="table table-hover dt-responsive nowrap" id="dt_invoice">
          <thead>
            <tr>
              <th>#<?php echo lang('Invoicenum'); ?></th>
              <th><?php echo lang('Date'); ?></th>
              <th><?php echo lang('Duedate'); ?></th>
              <th><?php echo lang('Status'); ?></th>
              <th><?php echo lang('Total'); ?></th>
              <th><?php echo lang('Download'); ?></th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
        <hr />
        <center><h6><?php echo lang('invoice_text_line'); ?></h6></center>

      </div>
    </div>
  </div>
<?php } ?>
</div>
<?php if ($showusage) {
    ?>

<div class="row" style="padding-top: 13px;">
  <div class="col-md-12">
    <div class="card-header bg-primary">
      <select class="form-control  col-sm-6 input-sm" id="activemobile" onchange="getUsage()">
        <?php foreach ($activemobiles as $mob) {?>
        <?php if((strpos( $mob['packagename'],'Medium')) || (strpos( $mob['packagename'], 'Large'))){ ?>

        <?php  $trendcall_issue = true; ?>
        <?php }else{ ?>
          <?php  $trendcall_issue = false; ?>
        <?php } ?>
        <option value="<?php echo $mob['id']; ?>"><?php echo $mob['number']; ?> [ <?php echo $mob['packagename']; ?> ]</option>
        <?php }?>
      </select>
    </div>
    <?php if ($bundles) {
        ?>
<div class="row"  style="padding-bottom: 13px; padding-top: 13px;">
  <div class="col-md-12">
    <div class="card-header bg-primary text-light">
        <?php echo lang('text-bundles'); ?>
    </div>
    <div class="table-responsive card">
      <div class="card-body" id="usageloading" style="display:none;">
        <center><img src="<?php echo base_url(); ?>assets/img/lg.rotating-balls-spinner.gif"></center>
      </div>
      <div class="card-body" id="usagebox">
        <div class="row">
        <?php $usaged = array(); ?>
          <?php foreach ($bundles as $index => $row) {?>
          <?php if($trendcall_issue) { ?>
          <?php if(!in_array($row->BundleId, array(9,10,48))) { ?>
          <div class=" col-md-4" <?php if ($index >= 3) {
                ?>style="padding-top: 20px;"<?php
                                 }?>>
            <div class="card bg-default">
               <div class="card-header text-center text-light bg-primary"><?php echo $row->szBundle; ?> </div>
              <div class="card-body text-dark text-center">
                <?php if ($this->session->master) {?>
                <?php }?>
                <?php if (isset($row->UsedValue)) {?>
                     <h5><?php echo str_replace('.', ',', $row->UsedValue) . ' / ' . $row->AssignedValue; ?> </h5>
                <?php } else {?>
                  <?php echo lang('Not In used yet'); ?>
                <?php }?>
                <?php if (strpos($row->szBundle, '30 dagen') || strpos($row->szBundle, '30 days')) {?>
                  <h6><?php echo lang('ValidUntil'); ?> <?php echo $row->ValidUntil; ?></h6>
                <?php }?>

              </div>
            </div>
          </div>
                <?php if ($index >= 4) {?>
            <br />
            <hr />
              <hr />
                <?php }?>

          <?php }else{?>
            <?php $usaged[] =  $row->UsedValue; ?>
          <?php } ?>
        <?php }else {?>
          <div class=" col-md-4" <?php if ($index >= 3) {
                ?>style="padding-top: 20px;"<?php
                                 }?>>
            <div class="card bg-default">
               <div class="card-header text-center text-light bg-primary"><?php echo $row->szBundle; ?> </div>
              <div class="card-body text-dark text-center">
                <?php if ($this->session->master) {?>
                <?php }?>
                <?php if (isset($row->UsedValue)) {?>
                     <h5><?php echo str_replace('.', ',', $row->UsedValue) . ' / ' . $row->AssignedValue; ?> </h5>
                <?php } else {?>
                  <?php echo lang('Not In used yet'); ?>
                <?php }?>
                <?php if (strpos($row->szBundle, '30 dagen') || strpos($row->szBundle, '30 days')) {?>
                  <h6><?php echo lang('ValidUntil'); ?> <?php echo $row->ValidUntil; ?></h6>
                <?php }?>

              </div>
            </div>
          </div>
                <?php if ($index >= 4) {?>
            <br />
            <hr />
              <hr />
                <?php }?>
          <?php }?>

        <?php }?>

        <?php if($trendcall_issue){ ?>

          <div class="col-md-4">
            <div class="card bg-default">
               <div class="card-header text-center text-light bg-primary">Domestic Unlimited</div>
              <div class="card-body text-dark text-center">

                <?php if (isset($usaged)) {?>
                     <h5><?php echo str_replace('.', ',', array_sum($usaged)) . ' / Unlimited'; ?> </h5>
                <?php } else {?>
                  <?php echo lang('Not In used yet'); ?>
                <?php }?>


              </div>
            </div>
          </div>
                <?php if ($index >= 4) {?>
            <br />
            <hr />
              <hr />
                <?php }?>




        <?php } ?>

        </div>

      </div>
    </div>
  </div>
</div>

    <?php }?>
  </div>
</div>
  <div class="col-md-12">
         <?php if (hasChangeProductPlanned($activemobiles[0]['id'])) {?>
      <hr />
        <div class="row">
      <div class="col-sm-12">
        <div class="card">
          <div class="card-body text-center">
            <div class="alert alert-warning" role="alert">

              <center><?php echo lang('you_have_product_change'); ?> <?php echo hasChangeProductPlanned($activemobiles[0]['id'])->date_commit; ?></center>
            </div>
          </div>
        </div>
      </div>
    </div>
         <?php }?>
  </div>
<?php }?>

<!--
<div class="row top-buffer">
  <div class="col-md-12">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Services List </h4>
        <div class="card-text"></div>
      </div>
    </div>
  </div>
</div>
-->
<script>
function getUsage(){
var id = $( "#activemobile option:selected" ).val();
$("#usageloading").show();
$("#usagebox").html(' <div class="row" id="taro"></div>');
console.log('calling');
$.ajax({
url: '<?php echo base_url(); ?>client/dashboard/getusagev2',
type: 'post',
dataType: 'json',
success: function (i) {
$("#taro").html(i.html);
$("#usageloading").hide();
$("#usagebox").show();
},
data: {'id': id}
});
}
</script>
<script>
$(document).ready(function()
{
$.getJSON(window.location.protocol + '//' + window.location.host + '/client/dashboard/get_lang', function(data) {

  $.ajax({
    type : 'POST',
    url  : window.location.protocol + '//' + window.location.host + '/client/table/getclient_invoices',
    dataType: 'json',
    cache: false,
    success :  function(result)
        {
            $('#dt_invoice').DataTable({
                "searching": true, //this is disabled because I have a custom search.
                "aaData": result, //here we get the array data from the ajax call.
                 "language": {
           "url": window.location.protocol + '//' + window.location.host +"/assets/clear/js/datatables/lang/" + data.result + ".json"
        },
                "aoColumns": [
            { "data": "iInvoiceNbr" },
            { "data": "dInvoiceDate" },
            { "data": "dInvoiceDueDate" },
            { "data": "iInvoiceStatus" },
            { "data": "mInvoiceAmount" },
            { "data": "iAddressNbr" }
                ], //this isn't necessary unless you want modify the header
                  //names without changing it in your html code.
                  //I find it useful tho' to setup the headers this way.
                  "fnRowCallback": function( nRow, aData, iDisplayIndex ) {
                    console.log(aData);

if(aData.iInvoiceStatus == "54"){
$('td:eq(3)', nRow).html('<font color="green"><b><?php echo lang('Paid'); ?></b></font>');
}else{
$('td:eq(3)', nRow).html('<font color="red"><b><?php echo lang('UnPaid'); ?></b></font>');
}
$('td:eq(4)', nRow).html(accounting.formatMoney(aData.mInvoiceAmount, "€", 2, ".", ","));
$('td:eq(0)', nRow).html('<b>' + aData.iInvoiceNbr + '</b>');
<?php if ($this->session->cid != 53) { ?>
  $('td:eq(5)', nRow).html('<a  target="_blank" class="btn btn-sm btn-primary"  href="'+window.location.protocol + '//' + window.location.host + '/client/invoice/download/'+aData.iInvoiceNbr+'"><i class="fa fa-file-pdf"></i> Download</a> <?php if ($setting->online_payment == "yes") {?><a href="#"  onclick="openPayModal('+aData.iInvoiceNbr+')" class="btn btn-sm btn-success"><i class="fa fa-credit-card"></i> Pay</a><?php }?>');
<?php } else { ?>
  $('td:eq(5)', nRow).html('<a  target="_blank" class="btn btn-sm btn-primary"  href="'+window.location.protocol + '//' + window.location.host + '/client/invoice/download/'+aData.iInvoiceNbr+'"><i class="fa fa-file-pdf"></i> Download</a> <a  target="_blank" class="btn btn-sm btn-primary"  href="'+window.location.protocol + '//' + window.location.host + '/client/invoice/downloadcdr_id/'+aData.iInvoiceNbr+'"><i class="fa fa-file-pdf"></i> CDR</a> ');

<?php  } ?>
$('td:eq(1)', nRow).html(aData.dInvoiceDate.slice(0,-13));
$('td:eq(2)', nRow).html(aData.dInvoiceDueDate.slice(0,-13));

return nRow;
},
            });
        }
    });



});
});
</script>

<script>
function openPayModal(id){
  $('#myModal').modal('toggle');
  console.log(id);
  $(".visa").attr("href", "<?php echo base_url(); ?>client/pay/index/"+id+"/VISA");
  $(".mastercard").attr("href", "<?php echo base_url(); ?>client/pay/index/"+id+"/MASTERCARD");
  $(".sofort").attr("href", "<?php echo base_url(); ?>client/pay/index/"+id+"/SOFORT");
  $(".ideal").attr("href", "<?php echo base_url(); ?>client/pay/index/"+id+"/IDEAL");
}
</script>
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title"><?php echo lang('Please Choose your Payment method'); ?></h4>
      </div>
      <div class="modal-body">
        <p>
        <a href=""  class="visa">
        <img  src="<?php echo base_url(); ?>assets/img/visa.png" alt='<?php echo lang('Pay with VISA'); ?>' height="60">
        </a>
        <a href=""  class="mastercard">
        <img  src="<?php echo base_url(); ?>assets/img/mastercard.png" alt='<?php echo lang('Pay with Mastercard'); ?>' height="60">
        </a>
        <a href="" class="sofort">
        <img   src="<?php echo base_url(); ?>assets/img/sofort-1.png" alt='<?php echo lang('Pay with Sofort'); ?>' height="60">
        </a>
        <a href="#" class="ideal">
        <img src="<?php echo base_url(); ?>assets/img/ideal.png" alt='<?php echo lang('Pay with Ideal'); ?>' height="60">
        </a>
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang('Close'); ?></button>
      </div>
    </div>

  </div>
</div>

<!-- Modal -->


<!-- Modal -->
<?php if($activemobiles ){ ?>
<div id="reloadCredit" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title"><?php echo lang('Reload Credit'); ?></h4>
      </div>
      <div class="modal-body">
      <div class="row"> 
      <label class="col-md-2"><?php echo lang('CLI Number'); ?></label>
      <select class="form-control  col-md-10" id="reload_serviceid">
        <?php foreach ($activemobiles as $mob) {?>
        <option value="<?php echo $mob['id']; ?>"><?php echo $mob['number']; ?></option>
        <?php }?>
      </select>
      </div>
       
  <div class="row" style="padding-top:13px;"> 
    <div class="col-md-3">
    <div class="card bg-primary">
    <div class="card-header text-light text-center"><?php echo $setting->currency; ?>5.00</div>
      <div class="card-body text-light text-center">
       <button class="btn btn-lg btn-light btn-reload" id="500" type="button"><?php echo lang('Buy'); ?></button>
      </div>
    </div>
</div>
<div class="col-md-3">
    <div class="card bg-primary">
    <div class="card-header text-light text-center"><?php echo $setting->currency; ?>10.00</div>
      <div class="card-body text-light text-center">
       
       <button class="btn btn-lg btn-light btn-reload" id="1000" type="button"><?php echo lang('Buy'); ?></button>
      </div>
    </div>
  </div>
<div class="col-md-3">
    <div class="card bg-primary">
    <div class="card-header text-light text-center"><?php echo $setting->currency; ?>15.00</div>
      <div class="card-body text-light text-center">
       
       <button class="btn btn-lg btn-light btn-reload" id="1500"  type="button"><?php echo lang('Buy'); ?></button>
      </div>
    </div>
  </div>
<div class="col-md-3">
    <div class="card bg-primary">
    <div class="card-header text-light text-center"><?php echo $setting->currency; ?>25.00</div>
      <div class="card-body text-light text-center">
      
       <button class="btn btn-lg btn-light btn-reload" id="2500" type="button"><?php echo lang('Buy'); ?></button>
      </div>
    </div>
  </div>
</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang('Close'); ?></button>
      </div>
    </div>
  </div>
</div>

        <?php } else{ ?>
          <div id="buyBundle" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title"><?php echo lang('Buy Bundle'); ?></h4>
      </div>
      <div class="modal-body">
      <h4 class="text-center"><?php echo lang('You need at least 1 active subscription'); ?></h4>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang('Close'); ?></button>
      </div>
    </div>

  </div>
</div>



          <div id="reloadCredit" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title"><?php echo lang('Reload Credit'); ?></h4>
      </div>
      <div class="modal-body">
   
          <h4 class="text-center"><?php echo lang('You need at least 1 active subscription'); ?></h4>
  

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang('Close'); ?></button>
      </div>
    </div>

  </div>
</div>

        <?php } ?>
<script>

$('.btn-reload').click(function(){
  console.log(this.id);
  var serviceid = $('#reload_serviceid').val();
  var userid = '<?php echo $_SESSION['client']['id']; ?>';
  var type = 'reload';
  var amount = this.id;
  $.ajax({
url: '<?php echo base_url(); ?>client/pay/payu/'+serviceid,
type: 'post',
dataType: 'json',
success: function (i) {

  window.location.replace(i.url);

},
data: {serviceid: serviceid, userid:userid, amount:amount, type:type, description:'Reload Credit',name:'reload '+this.id, qty:'1'},
});
});
</script>