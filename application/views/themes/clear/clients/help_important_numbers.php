

		 <div class="card-header bg-primary text-light"><?php echo lang('Important Numbers/Code'); ?>:</div>
                <div class="table-responsive card">
                <div class="card-body mb-0">
                	<div class="row">
                		<div class="col-md-6 mb-0"><h5><?php echo lang('Delta klantenservice'); ?> :</h5>
                		</div>
                		<div class="col-md-6 text-right"><h5>+31 118  22 55 05</h5>
                		</div>
                	</div>

                	<div class="row">
                		<div class="col-md-6"><h5><?php echo lang('Delta Voicemail'); ?></h5>
                		</div>
                		<div class="col-md-6 text-right"><h5>+31 637 00 12 33</h5>
                		</div>
                	</div>

                	<div class="row">
                		<div class="col-md-6"><h5><?php echo lang('Al uw gesprekken doorschakelen'); ?> :</h5>
                		</div>
                		<div class="col-md-6 text-right"><h5>**21*+31(telefoonnummer naar waar u wilt doorschakelen)# (Verzenden/groene bel knop)</h5>
                		</div>
                	</div>
<!--
                	<div class="row">
                		<div class="col-md-6"><h5><?php echo lang('Deactiveren alle doorschakelingen'); ?> +</h5>
                		</div>
                		<div class="col-md-6 text-right"><h5>##002# (<?php echo lang('send'); ?>)</h5>
                		</div>
                	</div>

                	<div class="row">
                		<div class="col-md-6"><h5><?php echo lang('Voicemail deactiveren als GSM uitstaat'); ?> :</h5>
                		</div>
                		<div class="col-md-6 text-right"><h5>##62# (<?php echo lang('send'); ?>)</h5>
                		</div>
                	</div>

                	<div class="row">
                		<div class="col-md-6"><h5><?php echo lang('Voicemail deactiveren indien GSM'); ?></h5>
                		</div>
                		<div class="col-md-6 text-right"><h5>##61# (<?php echo lang('send'); ?>)</h5>
                		</div>
                	</div>

                	<div class="row">
                		<div class="col-md-6"><h5><?php echo lang('Voicemail deactiveren als GSM bezet is'); ?> :</h5>
                		</div>
                		<div class="col-md-6 text-right"><h5>##67# (<?php echo lang('send'); ?>)</h5>
                		</div>
                	</div>

                	<div class="row">
                		<div class="col-md-6"><h5><?php echo lang('Voicemail tijd aanpassen (duur beltonen)'); ?> :</h5>
                		</div>
                		<div class="col-md-6 text-right"><h5>**61*+32486191933**5,10,15,20,25,30# (<?php echo lang('send'); ?>)</h5>
                		</div>
                	</div>

                	<div class="row">
                		<div class="col-md-6"><h5><?php echo lang('Doorschakelen als GSM uitstaat'); ?> :</h5>
                		</div>
                		<div class="col-md-6 text-right"><h5>**62*+32 (tel.nummer)# (<?php echo lang('send'); ?>)</h5>
                		</div>
                	</div>

                	<div class="row">
                		<div class="col-md-6"><h5><?php echo lang('Doorschakelen indien GSM onbeantwoord'); ?></h5>
                		</div>
                		<div class="col-md-6 text-right"><h5>**61*+32 (tel.nummer)# (<?php echo lang('send'); ?>)</h5>
                		</div>
                	</div>

                	<div class="row">
                		<div class="col-md-6"><h5><?php echo lang('Doorschakelen als GSM bezet is'); ?> :</h5>
                		</div>
                		<div class="col-md-6 text-right"><h5>**67*+32 (tel.nummer)# (<?php echo lang('send'); ?>)</h5>
                		</div>
                	</div>

                	<div class="row">
                		<div class="col-md-6"><h5><?php echo lang('Doorschakelen Alles'); ?> :</h5>
                		</div>
                		<div class="col-md-6 text-right"><h5>**21*+32 (tel.nummer)# (<?php echo lang('send'); ?>)</h5>
                		</div>
                	</div>

                	<div class="row">
                		<div class="col-md-6"><h5><?php echo lang('Tweede oproep activeren'); ?> :</h5>
                		</div>
                		<div class="col-md-6 text-right"><h5>*43# (<?php echo lang('send'); ?></h5>
                		</div>
                	</div>

                	<div class="row">
                		<div class="col-md-6"><h5><?php echo lang('Tweede oproep deactiveren'); ?> :</h5>
                		</div>
                		<div class="col-md-6 text-right"><h5>#43# (<?php echo lang('send'); ?>)</h5>
                		</div>
                	</div>

                	<div class="row">
                		<div class="col-md-6"><h5><?php echo lang('Tweede oproep status'); ?> :</h5>
                		</div>
                		<div class="col-md-6 text-right"><h5>*#43# (<?php echo lang('send'); ?>)</h5>
                		</div>
                	</div>


-->


                	</div>
                </div>



