<div class="row">
	<div class="col-md-12">
		 <div class="card-header bg-primary text-light"><?php echo lang('List Services'); ?></div>
                <div class="card">
                <div class="card-body">
                  <div class="table-responsive">
                <table class="table table-hover" id="dt_services">
                  <thead>
                    <tr>
                      <th>#<?php echo lang('ID'); ?></th>
                      <th><?php echo lang('Service Name'); ?></th>
                      <th><?php echo lang('Identifier'); ?></th>
                      <th><?php echo lang('BillingCycle'); ?></th>
                      <th><?php echo lang('Status'); ?></th>
                      <th><?php echo lang('Prijs'); ?></th>


                    </tr>
                  </thead>
                  <tbody>
                  </tbody>

                </table>
              </div>
              </div>
              </div>
	</div>
	</div>


<script>
    $(document).ready(function()
{
$.getJSON(window.location.protocol + '//' + window.location.host + '/client/dashboard/get_lang', function(data) {


$('#dt_services').DataTable({
    "autoWidth": false,
    "processing": true,
    "orderCellsTop": true,
    "ordering": true,
    "serverSide": true,
    "ajax": window.location.protocol + '//' + window.location.host + '/client/table/getclient_services/',
    "aaSorting": [[0, 'desc']],
    "columnDefs": [
    { "searchable": false, "targets": 1 }
     ],
    "language": {
    "url": window.location.protocol + '//' + window.location.host +"/assets/clear/js/datatables/lang/" + data.result + ".json"
        },
    "fnRowCallback": function( nRow, aData, iDisplayIndex ) {
      if(aData[3] == "Free Account"){
         $('td:eq(3)', nRow).html('<b><?php echo lang('Monthly'); ?></b>');
      }else if(aData[3] == "Monthly"){
          $('td:eq(3)', nRow).html('<b><?php echo lang('Monthly'); ?></b>');
        }

   if(aData[5] == "Pending"){
         $('td:eq(5)', nRow).html('<b><?php echo lang('Pending'); ?></b>');
      }else  if(aData[5] == "Active"){
          $('td:eq(5)', nRow).html('<b><?php echo lang('Active'); ?></b>');
        }


       $('td:eq(0)', nRow).html('<b>' + aData[0] + '</b>');
       //$('td:eq(3)', nRow).html('<b>' + aData[3] + '</b>');
        $('td:eq(5)', nRow).html('<b><?php echo $setting->currency; ?>' + aData[5].replace('.',',') + '</b>');
       $('td:eq(1)', nRow).html('<a href="'+window.location.protocol + '//' + window.location.host + '/client/service/detail/'+aData[0]+'">'+aData[1]+'</a>');
       $('td:eq(0)', nRow).html('<a href="'+window.location.protocol + '//' + window.location.host + '/client/service/detail/'+aData[0]+'">'+aData[0]+'</a>');
              return nRow;
        },
  });


});
});
</script>
