<div class="row">
  <div class="col-md-12">
    <div class="card-header bg-primary text-light"><?php echo lang('List Invoices'); ?></div>
    <div class="table-responsive card">
      <div class="card-body">
        <table class="table table-hover" id="dt_invoice" width="100%">
          <thead>
            <tr>
              <th>#<?php echo lang('Invoicenum'); ?></th>
              <th><?php echo lang('Date'); ?></th>
              <th><?php echo lang('Duedate'); ?></th>
              <th><?php echo lang('Status'); ?></th>
              <th><?php echo lang('Total'); ?></th>
              <th><?php echo lang('Download'); ?></th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
        <hr />
        <center><h6><?php echo lang('invoice_text_line'); ?></h6></center>
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function()
{
$.getJSON(window.location.protocol + '//' + window.location.host + '/client/dashboard/get_lang', function(data) {
$.ajax({
type : 'POST',
url  : window.location.protocol + '//' + window.location.host + '/client/table/getclient_invoices',
dataType: 'json',
cache: false,
success :  function(result)
{
$('#dt_invoice').DataTable({
"searching": true, //this is disabled because I have a custom search.
"aaData": result, //here we get the array data from the ajax call.
"language": {
"url": window.location.protocol + '//' + window.location.host +"/assets/clear/js/datatables/lang/" + data.result + ".json"
},
"language": {
"url": window.location.protocol + '//' + window.location.host +"/assets/clear/js/datatables/lang/" + data.result + ".json"
},
"aoColumns": [
{ "data": "iInvoiceNbr" },
{ "data": "dInvoiceDate" },
{ "data": "dInvoiceDueDate" },
{ "data": "iInvoiceStatus" },
{ "data": "mInvoiceAmount" },
{ "data": "iAddressNbr" }
], //this isn't necessary unless you want modify the header
//names without changing it in your html code.
//I find it useful tho' to setup the headers this way.
"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
if(aData.iInvoiceStatus == "54"){
$('td:eq(3)', nRow).html('<font color="green"><b><?php echo lang('Paid'); ?></b></font>');
}else{
$('td:eq(3)', nRow).html('<font color="red"><b><?php echo lang('UnPaid'); ?></b></font>');
}
$('td:eq(4)', nRow).html(accounting.formatMoney(aData.mInvoiceAmount, "€", 2, ".", ","));
$('td:eq(0)', nRow).html('<b>' + aData.iInvoiceNbr + '</b>');
 if(aData.iInvoiceType != "41"){
  $('td:eq(5)', nRow).html('<a  target="_blank" class="btn btn-sm btn-primary"  href="'+window.location.protocol + '//' + window.location.host + '/client/invoice/download/'+aData.iInvoiceNbr+'"><i class="fa fa-file-pdf"></i> Factuur</a> <a  target="_blank" class="btn btn-sm btn-primary"  href="'+window.location.protocol + '//' + window.location.host + '/client/invoice/downloadcdr_id/'+aData.iInvoiceNbr+'"><i class="fa fa-file-pdf"></i> Specificatie</a> ');
}else{
  $('td:eq(5)', nRow).html('<a  target="_blank" class="btn btn-sm btn-primary"  href="'+window.location.protocol + '//' + window.location.host + '/client/invoice/downloadcn/'+aData.iInvoiceNbr+'"><i class="fa fa-file-pdf"></i> Creditnote</a>');
}
$('td:eq(1)', nRow).html(aData.dInvoiceDate.slice(0,-13));
$('td:eq(2)', nRow).html(aData.dInvoiceDueDate.slice(0,-13));
return nRow;
},
});
}
});
});
});
</script>
<script>
function pay_online(id){
$('#invoiceid').val(id);
$('#payModal').modal('toggle');
}

function open_ideal(){
$('#payModal').modal('hide');
var invoiceid =  $('#invoiceid').val();
 $( "#body_form" ).submit();
}
</script>
<div class="modal fade" id="payModal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title"><?php echo lang('Please choose Payment Method below'); ?></h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!-- Modal body -->
      <div class="modal-body">
        <div class="row">
          <div class="col-sm-4" text-center>
            <input type="hidden" id="invoiceid" value="">
            <form name="body_form" method="post" xaction="<?hpp echo base_url(); ?>client/pay">
  <td><input id="purchaseid" type="text" name="purchaseid" value="Kenmerk" maxlength="16" style="color: #008ed0; width: 200px;" /></td>
  <td><input id="description" type="text" name="description" value="Omschrijving" maxlength="32" style="color: #008ed0; width: 200px;"  readonly=""></td>
  <input id="amount" type="text" name="amount" value="2.34" maxlength="10" style="color: #008ed0; width: 200px;" readonly="" /></td>
  <input type="submit"  value="Ga verder" title="Betaal" />
</form>
            <a href="#" onclick="open_ideal();"><img src="<?php echo base_url(); ?>assets/ideal.png" border="0" height="50"></a>
          </div>
          <div class="col-sm-4" text-center>
            <img src="<?php echo base_url(); ?>assets/cc.png" border="0" height="50">
          </div>
          <div class="col-sm-4" text-center>
            <img src="<?php echo base_url(); ?>assets/paypal.png" border="0" height="50">
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
