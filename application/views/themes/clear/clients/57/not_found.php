<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Exocom Client Login</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes/vendor/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes/vendor/font-awesome/css/font-awesome.min.css">
    <!-- Fontastic Custom icon font-->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes/css/fontastic.css">
    <!-- Google fonts - Poppins -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes/css/style.default.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes/css/custom.css">
    <!-- Favicon-->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/themes/img/favicon.ico">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
  </head>
  <body>
    <div class="page login-page">
      <div class="container d-flex align-items-center">
        <div class="form-holder has-shadow">
          <?php if (!empty($this->session->flashdata('error'))) {?>
          <div class="row">
            <div class="col-lg-12">
              <div class="alert alert-danger" role="alert">
                <strong><?php echo $this->session->flashdata('error'); ?></strong>
              </div>
            </div>
          </div>
          <?php }?>
          <?php if (!empty($this->session->flashdata('success'))) {?>
          <div class="row">
            <div class="col-lg-12">
              <div class="alert alert-success" role="alert">
                <strong><?php echo $this->session->flashdata('success'); ?></strong>
              </div>
            </div>
          </div>
          <?php }?>
          <div class="row">
            <!-- Logo & Information Panel-->
            <div class="col-lg-6">
              <div class="info d-flex align-items-center">
                <div class="content">
                  <div class="logo">
                    <img src="<?php echo $setting->logo_site; ?>" width="400">
                  </div>
                  <p><i class="fa fa-lock text-warning" aria-hidden="true"></i> <?php echo lang('unauthorized'); ?></p>
                  <div class="row">
                    <div class="col-md-4">
                       <a href="<?php echo base_url(); ?>client/auth/switchlang/english"> <img src="<?php echo base_url(); ?>assets/img/flag/uk.png" width="100"></a>
                    </div>
                        <div class="col-md-4">
                        <a href="<?php echo base_url(); ?>client/auth/switchlang/dutch">     <img src="<?php echo base_url(); ?>assets/img/flag/nl.png" width="100"></a>
                    </div>
                        <div class="col-md-4">
                       <a href="<?php echo base_url(); ?>client/auth/switchlang/french">     <img  src="<?php echo base_url(); ?>assets/img/flag/fr.png" width="100"> </a>
                    </div>
                  </div>

                </div>
              </div>
            </div>
            <!-- Form Panel    -->
            <div class="col-lg-6 bg-white">
             <h1 class="center">Page Not Found</h1>
            </div>
          </div>
        </div>
      </div>
      <div class="copyrights text-center">
        <p>Develop by <a href="https://www.exocom.be" class="external">Simson Lai</a>
        <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
      </p>
    </div>
  </div>
  <!-- Javascript files-->
  <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/themes/vendor/popper.js/umd/popper.min.js"> </script>
  <script src="<?php echo base_url(); ?>assets/themes/vendor/bootstrap/js/bootstrap.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/themes/vendor/jquery.cookie/jquery.cookie.js"> </script>
  <script src="<?php echo base_url(); ?>assets/themes/vendor/chart.js/Chart.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/themes/vendor/jquery-validation/jquery.validate.min.js"></script>
  <!-- Main File-->
  <script src="<?php echo base_url(); ?>assets/themes/js/front.js"></script>
</body>
</html>