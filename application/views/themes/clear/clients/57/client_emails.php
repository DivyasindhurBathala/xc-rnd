<div class="row">
	<div class="col-md-12">
		 <div class="card-header bg-primary text-light"><?php echo lang('List Email Account'); ?></div>
                <div class="table-responsive card">
                <div class="card-body">
                <table class="table table-hover" id="dt_emails">
                  <thead>
                    <tr>
                      <th>#<?php echo lang('Email'); ?></th>
                      <th><?php echo lang('Usage'); ?></th>

                      <th>Quota</th>
                       <th></th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>

                </table>
              </div>
              </div>
	</div>
	</div>
