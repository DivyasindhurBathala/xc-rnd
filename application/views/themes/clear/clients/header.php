<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $setting->companyname; ?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/<?php echo $setting->default_theme; ?>/client/delta.css?version=1.7">

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/<?php echo $setting->default_theme; ?>/client/custom.css?version2.5">
    <!-- Font Awesome CSS-->

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <!-- Fontastic Custom icon font-->
   
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/clear/jquery-ui/jquery-ui.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/clear/jquery-ui/jquery-ui.theme.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/clear/css/delta.css">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
    <script
    src="https://code.jquery.com/jquery-2.2.4.js"
    integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI="
    crossorigin="anonymous"></script>
    <script
    src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"
    integrity="sha256-T0Vest3yCU7pafRw9r+settMBX6JkKN06dqBnpQ8d30="
    crossorigin="anonymous"></script>
    <?php if ($this->uri->segment(3) == "product_edit") {?>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.css">
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.js"></script>
    <?php }?>
    <?php if (!empty($dtt)) {?>
    <?php echo dth(); ?>
    <?php }?>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/<?php echo $setting->default_theme; ?>/js/autocomplete.js"></script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
     <script type="text/javascript" src="<?php echo base_url(); ?>assets/<?php echo $setting->default_theme; ?>/client/bootstrap.min.js"></script>
    <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.4.0/css/bootstrap4-toggle.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.4.0/js/bootstrap4-toggle.min.js"></script>
    <script>
    $( function() {
    $( "#pickdate" ).datepicker( { dateFormat: 'yy-mm-dd'} );
    $( "#pickdate2" ).datepicker( { dateFormat: 'yy-mm-dd' } );
    $( "#pickdate3" ).datepicker( { dateFormat: 'yy-mm-dd' } );
    $( "#pickdate4" ).datepicker( { dateFormat: 'yy-mm-dd',minDate:0 } );
    $( "#pickdate5" ).datepicker( { dateFormat: 'yy-mm-dd',minDate:0 } );
    $( "#pickdate6" ).datepicker( { dateFormat: 'yy-mm-dd',minDate:0 } );
    } );
    </script>
    <script src="<?php echo base_url(); ?>assets/clear/js/accounting.js"></script>
  </head>
  <body>
    <!--
    <header>
      <div class="container">
        <div class="left-menu grid grid-65">
          <div class="grid grid-25 logo">
            <img src="<?php echo base_url(); ?>assets/clear/img/logo_delta.png">
          </div>
        </div>
      </div>
    </header>
    -->

  <nav class="<?php echo $setting->navbar_top_color; ?>">


      <div class="container">
  <span class="navbar-brand"> <img src="<?php echo $setting->logo_site; ?>" height="65"></span>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarColor01">
    <ul class="navbar-nav mr-auto">

    </ul>
    <form class="form-inline my-2 my-lg-0">
<?php if ($setting->multilang == "1") {?>
  <a href="<?php echo base_url(); ?>client/dashboard/switchlang/english" border="0"><img src="<?php echo base_url(); ?>assets/clear/img/flags/shiny/64/United-Kingdom.png" style="padding-right: 5px"></a>
  <a href="<?php echo base_url(); ?>client/dashboard/switchlang/dutch" border="0"><img src="<?php echo base_url(); ?>assets/clear/img/flags/shiny/64/Netherlands.png" style="padding-right: 5px"></a>
<a href="<?php echo base_url(); ?>client/dashboard/switchlang/french" border="0"><img src="<?php echo base_url(); ?>assets/clear/img/flags/shiny/64/France.png" style="padding-right: 5px"></a>
<a href="<?php echo base_url(); ?>client/dashboard/switchlang/japanese" border="0"><img src="<?php echo base_url(); ?>assets/clear/img/flags/shiny/64/Japan.png"></a>
<?php }?>
    </form>
  </div>
</div>
</nav>


      <hr />
      <hr />
      <div class="lewat"></div>
      <div class="container" style="padding-top: 30px;">
        <div class="breadcrumb-holder"  style="padding-top: 13px;">
          <ul class="breadcrumb  bg-primary">
            <?php foreach (extend_uri() as $uri) {?>
            <li class="breadcrumb-item"><?php  if(!is_numeric($uri)) { echo lang(ucfirst(str_replace('_', ' ', $uri))); } ?></li>
            <?php }?>
          </ul>
        </div>
        <?php if (!empty($this->session->flashdata('success'))) {?>
        <div class="row">
          <div class="col-sm-12">
            <div class="alert alert-success" role="alert">
              <strong class="text-center"><?php echo $this->session->flashdata('success'); ?></strong>
            </div>
          </div>
        </div>
        <?php }?>
        <?php if (!empty($this->session->flashdata('error'))) {?>
        <div class="row">
          <div class="col-sm-12">
            <div class="alert alert-danger" role="alert">
              <strong class="text-center"><center><?php echo $this->session->flashdata('error'); ?></center></strong>
            </div>
          </div>
        </div>
        <?php }?>
        <div class="row"  style="padding-bottom: 30px;">
          <?php if (!empty($_SESSION['client']['islogged'])) {?>
          <div class="col-md-4">
            <div class="card">
              <div class="card-header bg-primary text-light"><i class="fa fa-user"></i> <?php echo $this->session->userdata('client')['firstname'] . ' ' . $this->session->userdata('client')['lastname']; ?></div>
              <div class="card-body">

                <table class="table table-striped" width="100%">
                  <?php if (!empty(getDeltaId($_SESSION['client']['id']))) {?>
                  <tr>
                    <td><?php echo lang('Customerid'); ?></td>
                    <td><?php echo getDeltaId($_SESSION['client']['id']); ?></td>
                  </tr>
                  <?php }?>
                  <?php if (!empty($this->session->userdata('cient')['companyname'])) {?>
                  <tr>
                    <td><?php echo lang('Companyname'); ?></td>
                    <td><?php echo $this->session->userdata('client')['companyname']; ?></td>
                  </tr>
                  <?php }?>
                  <?php if (!empty($this->session->userdata('client')['vat'])) {?>
                  <tr>
                    <td><?php echo lang('BTW'); ?></td>
                    <td><?php echo $this->session->userdata('client')['vat']; ?>
                    </td>
                  </tr>
                  <?php }?>
                  <tr>
                    <td><?php echo lang('Address'); ?></td>
                    <td><?php echo $this->session->userdata('client')['address1']; ?><br />
                    <?php echo $this->session->userdata('client')['postcode']; ?>, <?php echo $this->session->userdata('client')['city']; ?><br /></td>
                  </tr>
                  <tr>
                    <td><?php echo lang('Email'); ?></td>
                    <td><?php echo $this->session->userdata('client')['email']; ?></td>
                  </tr>
                </table>
                <div class="row">
                   <div class="col-md-12">
 <?php if ($_SESSION['client']['sso'] != 1) {?>
                    <a class="btn btn-block btn-sm btn-primary btn-block" href="<?php echo base_url(); ?>client/dashboard/changepassword" class="card-link"><i class="fa fa-key"></i> <?php echo lang('Change Password'); ?></a>
<?php }?>

                    <a class="btn btn-block btn-sm btn-primary btn-block" href="<?php echo base_url(); ?>client/dashboard/myaccount" class="card-link"><i class="fa fa-edit"></i> <?php echo lang('Edit My Details'); ?></a>
                  </div>
                </div>
              </div>
            </div>
            <?php if (!empty($_SESSION['logged'])) {?>
            <a href="<?php echo base_url(); ?>admin/client/detail/<?php echo $_SESSION['client']['id']; ?>" class="btn btn-danger btn-md btn-block"><i class="fa fa-user"></i> <?php echo lang('Back to Admin'); ?></a>
            <?php }?>
            <div class="list-group"  style="padding-top: 13px;">
              <a href="<?php echo base_url(); ?>client/" class="list-group-item list-group-item-action <?php if (empty($this->uri->segment(2))) {?> active<?php }?>">
                <i class="fa fa-home"></i> <?php echo lang('Home'); ?>
              </a>
              <a href="<?php echo base_url(); ?>client/service" class="list-group-item list-group-item-action <?php if ($this->uri->segment(2) == "service") {?> active<?php }?>">
                <i class="fa fa-cog"></i> <?php echo lang('My Services'); ?>
              </a>
              <?php if ($setting->mage_invoicing) {?>
              <a href="<?php echo base_url(); ?>client/invoice" class="list-group-item list-group-item-action <?php if ($this->uri->segment(2) == "invoice") {?> active<?php }?>"><i class="fa fa-credit-card"></i> <?php echo lang('My Invoices'); ?>
              </a>
              <!--
                <a href="<?php echo base_url(); ?>client/invoice/creditnotes" class="list-group-item list-group-item-action <?php if ($this->uri->segment(3) == "creditnotes") {?> active<?php }?>"><i class="fa fa-credit-card"></i> <?php echo lang('My Creditnotes'); ?>

              -->
              <?php }?>
              <?php if ($setting->whmcs_ticket) {?>
              <a href="<?php echo base_url(); ?>client/helpdesk" class="list-group-item list-group-item-action <?php if ($this->uri->segment(2) == "helpdesk") {?> active<?php }?> "><i class="fa fa-info-circle"></i> <?php echo lang('My Support'); ?>
              </a>
              <?php }?>
              <a href="<?php echo base_url(); ?>client/help/important_numbers" class="list-group-item list-group-item-action <?php if ($this->uri->segment(2) == "help") {?> active<?php }?> "><i class="fa fa-info-circle"></i> <?php echo lang('Important Numbers'); ?>
              </a>
              <?php if ($_SESSION['cid'] == '53') {?>
              <?php if (!has_sso($_SESSION['client']['id'])) {?>
              <a href="javascript:void(0)" data-toggle="modal" data-target="#deltapd" class="list-group-item list-group-item-action <?php if ($this->uri->segment(2) == "dashboard" && $this->uri->segment(3) == "link2delta") {?> active<?php }?> "><i class="fa fa-link"></i> <?php echo lang('Link to My Delta'); ?>
              </a>
              <?php }?>
              <?php }?>
              <a href="#" onclick="confirmation_logout();" class="list-group-item list-group-item-action <?php if ($this->uri->segment(2) == "auth") {?> active<?php }?>"><i class="fa fa-sign-out-alt"></i> <?php echo lang('Logout'); ?>
              </a>
            </div>
          </div>
          <div class="col-md-8">
            <?php } else {?>
            <div class="modal fade" id="deltapod" tabindex="-1" role="dialog" aria-labelledby="deltapdTitle" aria-hidden="true">
              <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                  <div class="modal-header bg-primary text-light">
                    <h5 class="modal-title" id="exampleModalLongTitle"><?php echo lang('Link Your Delta Account to Delta Mobiel account'); ?></h5>
                  </div>
                  <form id="link2delta" method="post" action="<?php echo base_url(); ?>client/auth/link2artilium">
                    <input type="hidden" name="deltausername" value="<?php echo $account['account']; ?>">
                    <div class="modal-body">
                      <?php if (!empty($this->session->flashdata('error'))) {?>
                      <div class="row">
                        <div class="col-sm-12">
                          <div class="alert alert-danger" role="alert">
                            <strong class="text-center"><center><?php echo $this->session->flashdata('error'); ?></center></strong>
                          </div>
                        </div>
                      </div>
                      <?php }?>
                      <div class="form-group">
                        <label><?php echo lang('Delta Username'); ?></label>
                        <input type="text" name="username" class="form-control" autocomplete="new-username" value="<?php echo $account["account"]; ?>" readonly>
                      </div>
                      <div class="form-group">
                        <label><?php echo lang('Delta Mobiel Username'); ?></label>
                        <input type="text" name="username" class="form-control" autocomplete="new-username" value="" placeholder="email@domain.com" required>
                      </div>
                      <div class="form-group">
                        <label><?php echo lang('Delta Mobiel Password'); ?></label>
                        <input type="password" name="password" class="form-control" autocomplete="new-username" value="" required>
                      </div>
                      <small><?php echo lang('You receieved this login information when you order Delta Mobiel product from us'); ?></small>
                    </div>
                    <div class="modal-footer">
                      <button type="submit" id="yeslink" class="btn btn-primary"><?php echo lang('Link my account'); ?></button>
                    </div>
                    <input type="hidden" name="key" value="<?php echo $_GET["key"]; ?>">
                  </form>
                </div>
              </div>
            </div>
            <?php if ($_SESSION['cid'] == "53") {?>
            <script>
            $( document ).ready(function() {
            $('#deltapod').modal({backdrop: 'static', keyboard: false});
            });
            </script>
            <?php }?>
            <div class="col-12">
              <?php }?>