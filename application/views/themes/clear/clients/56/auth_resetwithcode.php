<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo lang('Client Login'); ?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/<?php echo $setting->default_theme; ?>/client/lumen.css?version=1.0">
    <!-- Font Awesome CSS-->
          <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css">
     <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/<?php echo $setting->default_theme; ?>/client/style.default.css?version=1.0" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/<?php echo $setting->default_theme; ?>/client/login.css?version1.0">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
  </head>
  <body>

          <?php if (!empty($this->session->flashdata('error'))) {?>
          <div class="row">
            <div class="col-lg-12">
              <div class="alert alert-danger" role="alert">
                <strong class="text-center"><?php echo $this->session->flashdata('error'); ?></strong>
              </div>
            </div>
          </div>
          <?php }?>
          <?php if (!empty($this->session->flashdata('success'))) {?>
          <div class="row">
            <div class="col-lg-12">
              <div class="alert alert-warning" role="alert">
                <strong class="text-center"><?php echo $this->session->flashdata('success'); ?></strong>
              </div>
            </div>
          </div>
          <?php }?>

   <div class="login-page">
  <img src="<?php echo $setting->logo_site; ?>" width="360">
  <br />
  <br />
  <div class="form">
    <form id="login-form" method="post" action="<?php echo base_url(); ?>client/auth/resetwithcode">
        <input type="hidden" name="code" value="<?php echo $_SESSION['code']; ?>">
      <input type="password" placeholder="<?php echo lang('New Password'); ?>" name="password1" required=""/>
        <input type="password" placeholder="<?php echo lang('Confirm Password'); ?>" name="password2" required=""/>
      <button><i class="fa fa-key"></i> <?php echo lang('Change Password'); ?></button>
      <p class="message"><a href="<?php echo base_url(); ?>client/auth"><?php echo lang('Please Enter New Password'); ?></a></p>
    </form>
  </div>
</div>



  <!-- Javascript files-->
  <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/themes/vendor/popper.js/umd/popper.min.js"> </script>
  <script src="<?php echo base_url(); ?>assets/themes/vendor/bootstrap/js/bootstrap.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/themes/vendor/jquery.cookie/jquery.cookie.js"> </script>
  <script src="<?php echo base_url(); ?>assets/themes/vendor/chart.js/Chart.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/themes/vendor/jquery-validation/jquery.validate.min.js"></script>
  <!-- Main File-->
  <script src="<?php echo base_url(); ?>assets/themes/js/front.js"></script>
</body>
</html>