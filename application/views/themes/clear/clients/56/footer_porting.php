</div>
</div>
</div>
<div class="top-buffer"></div>
<?php if ($this->uri->segment(2) == "service" && $this->uri->segment(3) == "detail") {?>
  <div class="tsl-copybar-wrapper fixed-bottom">
<?php } else {?>
  <div class="tsl-copybar-wrapper fixed-bottom">
<?php }?>

<div class="container">
<div class="row">
<div class="tsl-logo col-md-2">
  <img src="<?php echo base_url(); ?>assets/img/logo_delta.png" alt="Home">
</div>

<div class="col-md-10">
  <div style="margin-top: 27px;">
    <div class="row">
    <div class="col-md-4">
      <a href="javascript:void(0);">©2018&nbsp;DELTA. Alle rechten voorbehouden.</a></div>
  <div class="col-md-8" align='right'>
  <a href="https://www.delta.nl/klantenservice" style="padding-right: 15px;">Contact DELTA klantservice</a>
  <a href="https://delta.nl/cookies" style="padding-right: 15px;">Cookies</a>
  <a href="https://delta.nl/disclaimer/" style="padding-right: 15px;">Disclaimer</a>
  <a href="https://delta.nl/privacyverklaring" style="padding-right: 15px;">Privacy</a></p>
</div>
</div>
</div>
</div>
</div>
</div>
</div>

<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>

</html>
