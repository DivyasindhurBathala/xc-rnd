<?php if(showInvoice($this->session->cid)){ ?>
<div class="row">
  <div class="col-md-12">
    <div class="card-header bg-primary text-light"><?php echo lang('List Invoices'); ?></div>
    <div class="table-responsive card">
      <div class="card-body">
        <table class="table table-hover" id="dt_invoice" width="100%">
          <thead>
            <tr>
              <th>#<?php echo lang('Invoicenum'); ?></th>
              <th><?php echo lang('Date'); ?></th>
              <th><?php echo lang('Duedate'); ?></th>
              <th><?php echo lang('Status'); ?></th>
              <th><?php echo lang('Total'); ?></th>
              <th><?php echo lang('Download'); ?></th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
        <hr />
        <center><h6><?php echo lang('invoice_text_line'); ?></h6></center>
      </div>
    </div>
  </div>
</div>
<?php } ?>
<script>
$(document).ready(function()
{
$.getJSON(window.location.protocol + '//' + window.location.host + '/client/dashboard/get_lang', function(data) {
$.ajax({
type : 'POST',
url  : window.location.protocol + '//' + window.location.host + '/client/table/getclient_invoices',
dataType: 'json',
cache: false,
success :  function(result)
{
$('#dt_invoice').DataTable({
"searching": true, //this is disabled because I have a custom search.
"aaData": result, //here we get the array data from the ajax call.
"language": {
"url": window.location.protocol + '//' + window.location.host +"/assets/clear/js/datatables/lang/" + data.result + ".json"
},
"language": {
"url": window.location.protocol + '//' + window.location.host +"/assets/clear/js/datatables/lang/" + data.result + ".json"
},
"aoColumns": [
{ "data": "iInvoiceNbr" },
{ "data": "dInvoiceDate" },
{ "data": "dInvoiceDueDate" },
{ "data": "iInvoiceStatus" },
{ "data": "mInvoiceAmount" },
{ "data": "iAddressNbr" }
], //this isn't necessary unless you want modify the header
//names without changing it in your html code.
//I find it useful tho' to setup the headers this way.
"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
if(aData.iInvoiceStatus == "54"){
$('td:eq(3)', nRow).html('<font color="green"><b><?php echo lang('Paid'); ?></b></font>');
}else{
$('td:eq(3)', nRow).html('<font color="red"><b><?php echo lang('UnPaid'); ?></b></font>');
}
$('td:eq(4)', nRow).html(accounting.formatMoney(aData.mInvoiceAmount, "€", 2, ".", ","));
$('td:eq(0)', nRow).html('<b>' + aData.iInvoiceNbr + '</b>');
<?php if($this->session->cid != 53){ ?>
  $('td:eq(5)', nRow).html('<a  target="_blank" class="btn btn-sm btn-primary"  href="'+window.location.protocol + '//' + window.location.host + '/client/invoice/download/'+aData.iInvoiceNbr+'"><i class="fa fa-file-pdf"></i> Download</a>  <?php if ($setting->online_payment == "yes") {?><a href="#"  onclick="openPayModal('+aData.iInvoiceNbr+')" class="btn btn-sm btn-success"><i class="fa fa-credit-card"></i> Pay</a><?php }?>');

<?php } else { ?>
  $('td:eq(5)', nRow).html('<a  target="_blank" class="btn btn-sm btn-primary"  href="'+window.location.protocol + '//' + window.location.host + '/client/invoice/download/'+aData.iInvoiceNbr+'"><i class="fa fa-file-pdf"></i> Download</a> <a  target="_blank" class="btn btn-sm btn-primary"  href="'+window.location.protocol + '//' + window.location.host + '/client/invoice/downloadcdr_id/'+aData.iInvoiceNbr+'"><i class="fa fa-file-pdf"></i> CDR</a> ');

<?php  } ?>
$('td:eq(1)', nRow).html(aData.dInvoiceDate.slice(0,-13));
$('td:eq(2)', nRow).html(aData.dInvoiceDueDate.slice(0,-13));
return nRow;
},
});
}
});
});
});
</script>
<script>
function pay_online(id){
$('#invoiceid').val(id);
$('#payModal').modal('toggle');
}

function open_ideal(){
$('#payModal').modal('hide');
var invoiceid =  $('#invoiceid').val();
 $( "#body_form" ).submit();
}
</script>


<script>
function openPayModal(id){
  $('#myModal').modal('toggle');
  console.log(id);
  $(".visa").attr("href", "<?php echo base_url(); ?>client/pay/index/"+id+"/VISA");
  $(".mastercard").attr("href", "<?php echo base_url(); ?>client/pay/index/"+id+"/MASTERCARD");
  $(".sofort").attr("href", "<?php echo base_url(); ?>client/pay/index/"+id+"/SOFORT");
  $(".ideal").attr("href", "<?php echo base_url(); ?>client/pay/index/"+id+"/IDEAL");
  $(".bc").attr("href", "<?php echo base_url(); ?>client/pay/index/"+id+"/BC");
  $(".paypal").attr("href", "<?php echo base_url(); ?>client/pay/paypal/"+id);

}
</script>
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Please Choose your Payment method</h4>
      </div>
      <div class="modal-body">
        <p>
        <a href=""  class="visa">
        <img  src="<?php echo base_url(); ?>assets/img/visa.png" alt='Pay with VISA' height="40">
        </a>
        <a href=""  class="mastercard">
        <img  src="<?php echo base_url(); ?>assets/img/mastercard.png" alt='Pay with Mastercard' height="40">
        </a>

        <a href="#" class="ideal">
        <img src="<?php echo base_url(); ?>assets/img/ideal.png" alt='Pay with Ideal' height="40">
        </a>
       <!--
        <a href="" class="sofort">
        <img   src="<?php echo base_url(); ?>assets/img/sofort-1.png" alt='Pay with Sofort' height="40">
        </a>
        <a href="#" class="bc">
        <img src="<?php echo base_url(); ?>assets/img/mister_cash_logo.png" alt='Pay with BanContact' height="40">
        </a>
-->
        <a href="#" class="paypal">
        <img src="<?php echo base_url(); ?>assets/img/paypal.jpg" alt='Pay with Paypal' height="40">
        </a>
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>