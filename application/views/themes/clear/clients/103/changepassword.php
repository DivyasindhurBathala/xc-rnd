<section class="tables">
  <div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-body">
          <form method="post" action="<?php echo base_url(); ?>client/dashboard/changepassword">
            <input type="hidden" name="id" value="<?php echo $client->id; ?>">
            <div class="row">
              <div class="col-sm-6">
                <div class="form-group">
                  <fieldset>
                    <label class="control-label" for="vat"><?php echo lang('Current Password'); ?></label>
                    <input  name="currentpassword" class="form-control" id="currentpassword" type="password" autocomplete="off" value="" required>
                    <span id="loading_data_icon"></span>
                  </fieldset>
                </div>


                <div class="form-group">
                  <fieldset>
                    <label class="control-label" for="companyname"><?php echo lang('New Password'); ?> <span id="msg"></span></label>
                    <input  name="password1" class="form-control" id="password1" onkeyup="validatePassword(this.value);" type="password" placeholder="" value="" autocomplete="new-password" required>
                    <span id="loading_data_icon"></span>

                  </fieldset>
                </div>

                  <div class="form-group">
                  <fieldset>
                    <label class="control-label" for="companyname"><?php echo lang('Confirm Password'); ?> <span id="msg2"></span></label>
                    <input  name="password2" class="form-control" id="password2" onkeyup="validatePassword2(this.value);" type="password" placeholder="" value="" autocomplete="new-password"  required disabled>
                    <span id="loading_data_icon"></span>
                    <span id="msg"></span>
                  </fieldset>
                </div>
              </div>
              <div class="col-sm-6">
              	<center><img src="<?php echo base_url(); ?>assets/img/shield-ok-icon.png" height="250">
              </div>
            </div>


            <div class="row">
              <div class="col-sm-6">
                <div class="form-group">
                  <button type="submit" id="tombol" class="btn btn-md btn-primary btn-block" disabled><i class="fa fa-save"></i> <?php echo lang('Save'); ?></button>
                </div>
              </div>
            </div>
            <?php if (!empty($this->uri->segment(4))) {?>

              <input type="hidden" value="1" name="first">

            <?php }?>
          </form>
        </div>
      </div>
    </div>
  </div>
  <div class="row" style="padding-top: 10px; padding-bottom: 20px;">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-body">
     <center> <?php echo lang('Om uw wachtwoord te kunnen wijzigen moet het sterk zijn.
Een sterk wachtwoord voldoet aan de volgende eisen:<br />
- minimaal 8 karakters lang<br />
- ten minste vier soorten tekens: hoofdletters, kleine letters,<br />
cijfers en speciale tekens.<br />
Bijvoorbeeld: W@chtw00rd'); ?></center>
    </div>
  </div>
</div>
  </div>
</section>
<script>
  function validatePassword2(cpassword){

    var npassword = $('#password1').val();
    console.log(npassword+' '+cpassword);
    if(npassword == cpassword){

 $('#tombol').prop('disabled', false);
    }else{
 $('#tombol').prop('disabled', true);

    }
  }
 function validatePassword(password) {

                // Do not show anything when the length of password is zero.
                if (password.length === 0) {
                    document.getElementById("msg").innerHTML = "";
                    return;
                }
                // Create an array and push all possible values that you want in password
                var matchedCase = new Array();
                matchedCase.push("[$@$!%*#?&]"); // Special Charector
                matchedCase.push("[A-Z]");      // Uppercase Alpabates
                matchedCase.push("[0-9]");      // Numbers
                matchedCase.push("[a-z]");     // Lowercase Alphabates

                // Check the conditions
                var ctr = 0;
                for (var i = 0; i < matchedCase.length; i++) {
                    if (new RegExp(matchedCase[i]).test(password)) {
                        ctr++;
                    }
                }
                // Display it
                var color = "";
                var strength = "";
                switch (ctr) {
                    case 0:
                    case 1:
                    case 2:
                        strength = "<?php echo lang('Weak'); ?>";
                        color = "red";
                        break;
                    case 3:
                        strength = "<?php echo lang('Medium'); ?>";
                        color = "orange";

                        break;
                    case 4:
                        strength = "<?php echo lang('Strong'); ?>";
                        color = "green";

                        break;
                }
                if(ctr >= 4){

                  $('#password2').prop('disabled', false);
                }else{
                   $('#password2').prop('disabled', true);
                }
                document.getElementById("msg").innerHTML = strength;
                document.getElementById("msg").style.color = color;
            }
        </script>