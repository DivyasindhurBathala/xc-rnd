<div class="main-panel" style="padding-top:40px; padding-left:10px;padding-right:10px;">
  <div class="content-wrapper">
    <div class="row">
      <div class="col-sm-3">
        <div class="card border-light">
          <div class="card-body">
            <h4 class="card-title"><?php echo lang('Client Detail'); ?></h4>
            <table class="table table-hover table-striped" width="100%">
              <tr>
                <td><?php echo lang('Client Name'); ?></td>
                <td>
                  <?php echo format_name($client); ?>
                </td>
              </tr>
              <tr>
                <td><?php echo lang('Client ID'); ?></td>
                <td>
                    <?php echo $client->mvno_id; ?>
                </td>
              </tr>
              <tr>
                <td><?php echo lang('Address'); ?></td>
                <td>
                    <?php echo $client->address1; ?>
                    <?php echo $client->postcode; ?><br />
                    <?php echo $client->city; ?>
                    <?php echo $client->country; ?>
                </td>
              </tr>
              <tr>
                <td><?php echo lang('Email'); ?></td>
                <td>
                    <?php echo $client->email; ?>
                </td>
              </tr>
              <tr>
                <td><?php echo lang('Phonenumber'); ?></td>
                <td>
                    <?php echo $client->phonenumber; ?>
                </td>
              </tr>
            </table>
            <hr />
            <?php if (!empty($_SESSION['id'])) { ?>
            <a class="btn btn-md btn-danger btn-block" href="<?php echo base_url(); ?>admin/agent/detail/<?php echo $this->session->reseller['id']; ?>"><?php echo lang('Back
              To Admin Dashboard'); ?></a>
            <?php } ?>
            <a class="btn btn-md btn-primary btn-block" href="<?php echo base_url(); ?>reseller/dashboard/viewclient/<?php echo $client->id; ?>"><?php echo lang('Back To Client Summary'); ?></a>
              <!--
            <?php if ($service->details->msisdn_type== "new") { ?>
            <button type="button" data-toggle="modal" data-target="#newModal" class="btn btn-md btn-primary btn-block">Download Welcome Letter</button>
            <?php } ?>
            <?php if ($service->details->msisdn_swap) { ?>
            <button type="button" data-toggle="modal" data-target="#swapModal" class="btn btn-md btn-primary btn-block">Download Swap Letter</button>
            <?php } ?>
            <?php if ($service->details->msisdn_type== "porting") { ?>
            <button type="button" onclick="pdfshow('porting', '<?php echo $service->id; ?>');" class="btn btn-md btn-primary btn-block">Download Porting Letter</button>
            <?php } ?>
-->
              <!-- <button type="button" id="cdr" class="btn btn-md btn-primary btn-block">CDR details</button> -->
              <?php //if($service->companyid == 33){ ?>
                <?php if ($service->orderstatus == "Suspended") { ?>
                <button type="button" id="unsuspend" class="btn btn-md btn-primary btn-block"><?php echo lang('UnSuspend'); ?></button>
                <?php } else { ?>
                <button type="button" id="suspend" class="btn btn-md btn-primary btn-block"><?php echo lang('Suspend'); ?></button>
                <?php } ?>
                 <button type="button"   data-toggle="modal" data-target="#SwapModal"  class="btn btn-md btn-primary btn-block"><?php echo lang('Swap SIMCARD'); ?></button>
              <?php //} ?>
               <?php if($service->product_sub_type == "Prepaid"){ ?>
                <button type="button" data-toggle="modal" data-target="#ReloadModal" class="btn btn-md btn-primary btn-block"><?php echo lang('Reload Credit'); ?></button> 
                <?php } ?>
               <!--  <button type="button" id="terminate" class="btn btn-md btn-primary btn-block">Terminate Service</button> 
                <button type="button" id="upgrade" class="btn btn-md btn-primary btn-block">Upgrade/Downgrade</button>

              -->


          </div>
        </div>
      </div>

      <div class="col-sm-9">
        <div class="card border-light">
          <div class="card-body">
            <h4 class="card-title"><?php echo lang('Subscription Detail'); ?>
                <?php echo $service->domain; ?>
            </h4>
            <table class="table table-striped table-bordered">
              <thead>
                <tr style="background-color: #D4D4D4;">
                  <th><?php echo lang('ProductName'); ?></th>
                  <th><?php echo lang('Date Contract'); ?></th>
                  <th><?php echo lang('Recurring'); ?></th>
                  <th><?php echo lang('Status'); ?></th>
                </tr>
                <tr>
                  <td>
                    <?php echo $service->packagename; ?>
                  </td>
                  <td>
                    <?php echo convert_contract($service->date_contract); ?>
                  </td>
                  <td>€
                    <?php echo number_format($service->recurring, 2); ?>
                  </td>
                  <td>
                    <?php echo $service->orderstatus; ?>
                  </td>
                </tr>
              </thead>
            </table>
            
            <hr />
            <table class="table" id="bundlesx_table">
              <thead>
                <tr class="bg-primary  text-white">
                  <th width="5%">
                    <?php echo lang('Type'); ?>
                  </th>
                  <th width="40%">
                    <?php echo lang('Package'); ?>
                  </th>
                  <th width="20%">
                    <?php echo lang('ValidFrom'); ?>
                  </th>
                  <th width="20%">
                    <?php echo lang('Usage'); ?>
                  </th>
                  <th width="15%">
                    <?php echo lang('Percentage'); ?>
                  </th>
                </tr>
              </thead>
              <tbody id="tbody_bundlesx">
              </tbody>
            </table>
            <div id="loadingx1" style="display:none;">
              <center>
                <img src="<?php echo base_url(); ?>assets/img/loader1.gif" height="100" class="text-center">
              </center>
            </div>
            <hr />
            <div class="row">
              <div class="col-sm-6">
                <table class="table table-striped table-bordered" id="sum_assign" style="display:none;">
                  <thead>
                    <tr class="bg-primary  text-white">
                      <th width="33%">
                        <?php echo lang('Call ceiling'); ?>
                      </th>
                      <th width="33%">
                        <?php echo lang('ValidFrom'); ?>
                      </th>
                      <th width="33%">
                        <?php echo lang('UsagePlan'); ?>
                      </th>
                    </tr>
                  </thead>
                  <tbody id="sumx">
                  </tbody>
                  <div id="scp"></div>

                </table>


              </div>
              <div class="col-sm-6">
                 <table class="table table-striped table-bordered">
              <thead>
                <thead>
                  <tr class="bg-primary  text-white">
                    <th><?php echo lang('Description'); ?></th>
                    <th><?php echo lang('Value'); ?></th>
                  </tr>
                </thead>
                <tr>
                  <td>
                  <?php echo lang('Simcard'); ?>
                  </td>
                  <td>
                    <?php echo $service->details->msisdn_sim; ?>
                  </td>
                </tr>
                <tr>
                  <td>
                  <?php echo lang('PUK1'); ?>
                  </td>
                  <td>
                    <?php echo $service->details->msisdn_puk1; ?>
                  </td>
                </tr>
                <tr>
                <td>
                <?php echo lang('PUK2'); ?>
                  </td>
                  <td>
                    <?php echo $service->details->msisdn_puk2; ?>
                  </td>
                 
                </tr>
              </thead>
            </table>
                <hr />
              </div>
            </div>

            <hr />

          </div>
        </div>
      </div>


    </div>
  </div>
</div>

<form>
  <input type="hidden" id="msisdn" value="<?php echo trim($service->domain); ?>">
</form>
<script>
  $(document).ready(function () {
    var product = $('#addonproduct').find(":selected").val();
    var contractduration = $('#contractduration').find(":selected").val();
    console.log(product + ' ' + contractduration);
    $.ajax({
      url: '<?php echo base_url(); ?>admin/complete/getPriceProduct/' + product + '/' + contractduration,
      dataType: 'json',
      success: function (data) {
        $('#harga').val(data.price);
      },
      error: function (errorThrown) {
        console.log(errorThrown);
      }
    });
    $("#contractduration").change(function () {
      var product = $('#addonproduct').find(":selected").val();
      var contractduration = $('#contractduration').find(":selected").val();
      $.ajax({
        url: '<?php echo base_url(); ?>admin/complete/getPriceProduct/' + product + '/' + contractduration,
        dataType: 'json',
        success: function (data) {
          $('#harga').val(data.price);
        },
        error: function (errorThrown) {
          console.log(errorThrown);
        }
      });
    });
     $("#suspend").click(function () {
      var serviceid = '<?php echo $service->id; ?>';
      var r = confirm("Are you sure to block outbound  traffic of this customer");
    if (r == true) {
    $.ajax({
        url: '<?php echo base_url(); ?>reseller/subscription/block_mobile_originating/' + serviceid,
        dataType: 'json',
        success: function (data) {
          window.location.replace(window.location.protocol + '//' + window.location.host + '/reseller/subscription/detail/'+serviceid);
        },
        error: function (errorThrown) {
          console.log(errorThrown);
        }
      });
    } 
     });

       $("#unsuspend").click(function () {
        var serviceid = '<?php echo $service->id; ?>';
      var r = confirm("Are you sure to unblock the outbound traffic for this customer");
      if (r == true) {
        $.ajax({
        url: '<?php echo base_url(); ?>reseller/subscription/unblock_mobile_originating/' + serviceid,
        dataType: 'json',
        success: function (data) {
          
          window.location.replace(window.location.protocol + '//' + window.location.host + '/reseller/subscription/detail/'+serviceid);
        },
        error: function (errorThrown) {
          console.log(errorThrown);
        }
      });
      } 
     });
    $("#addonproduct").change(function () {
      var product = $('#addonproduct').find(":selected").val();
      var contractduration = $('#contractduration').find(":selected").val();
      $.ajax({
        url: '<?php echo base_url(); ?>admin/complete/getPriceProduct/' + product + '/' + contractduration,
        dataType: 'json',
        success: function (data) {
          $('#harga').val(data.price);
        },
        error: function (errorThrown) {
          console.log(errorThrown);
        }
      });
    });

    $('#loadingx1').show('slow');
    $('#loadingx2').show('slow');
    $('#loadingx3').show('slow');
    $('#loadingx4').show('slow');
    $('#loadingx5').show('slow');
    $.ajax({
      url: window.location.protocol + '//' + window.location.host +
        '/admin/complete/get_service_detail/<?php echo $this->uri->segment(4); ?>',
      dataType: 'json',
      success: function (data) {

        if (data.sum.length > 1) {
          $('#sum_assign').show();
          data.sum.forEach(function (b) {

            $('#sumx').append('<tr><td>' + b.Name + '</td><td>' + b.ValidFrom +
              '</td><td><select class="form-control ceilling" id="opt' + b.SumAssignmentId +
              '" disabled></select></td></tr>');
            if (b.ActionSet.GetSumPlanActionSetsResult.Data.Code) {
              $('#opt' + b.SumAssignmentId).append('<option value="' + b.ActionSet.GetSumPlanActionSetsResult
                .Data.Id + '" disabled>' + b.ActionSet.GetSumPlanActionSetsResult.Data.Description + '</option>'
              );

            } else {
              b.ActionSet.GetSumPlanActionSetsResult.Data.forEach(function (c) {
                if (b.SumSetId == c.Id) {
                  $('#opt' + b.SumAssignmentId).append('<option value="' + c.Id + '" selected disabled>' + c.Description +
                    '</option>');
                } else {
                  $('#opt' + b.SumAssignmentId).append('<option value="' + c.Id + '" disabled>' + c.Description +
                    '</option>');
                }

              });
            }

          });
          $('#scp').html(
            '<script>$(\'.ceilling\').change(function(){var PlanId = this.id;var SN="<?php echo $service->details->msisdn_sn; ?>"; var SetId = $( "#"+PlanId+" option:selected" ).val();$.ajax({ url: window.location.protocol + "//" + window.location.host + "/admin/subscription/change_call_ceilling", type: "post", dataType: "json", data: {"sn": SN, "PlanId": PlanId, "SetId":SetId, "serviceid":<?php echo $this->uri->segment(4); ?>},success: function (data) {}}); });'
          );
        } else {
          $('#addSum').show();
        }
        $('#loadingx1').hide('slow');
        $('#loadingx2').hide('slow');
        $('#loadingx3').hide('slow');
        $('#loadingx4').hide('slow');
        $('#loadingx5').hide('slow');
        if (data.bundles) {
          ////console.log(data.bundles);
          $('#tbody_bundlesx').show();
          $('#datax').html(data.cdr.data + ' Bytes');
          $('#smsx').html(data.cdr.sms + ' sms');
          $('#voicex').html(data.cdr.voice);
          if (data.bundles.length >= 1) {
            data.bundles.forEach(function (b) {
              if (b.Percentage > 90) {
                var text = "text-danger";
                var coli = "bg-danger";
              } else {
                var text = "text-dark";
                var coli = "bg-success";
              }

              $('#tbody_bundlesx').append(' <tr><td><i class="fa fa-' + b.icon + '"></i></td><td>' + b.szBundle +
                '</td><td>' + b.ValidFrom + '</td> <td>' + b.UsedValue + ' / ' + b.AssignedValue +
                '</td><td>    <div id="progress" class="graph"><div id="bar" style="width:'+b.Percentage+'%"><p>'+b.Percentage+'% used</p></div></div></td></tr>');
            });
          } else {
            if (data.bundles.Percentage) {
              if (data.bundles.Percentage > 90) {
                var text = "text-danger";
                var coli = "bg-danger";
              } else {
                var text = "text-dark";
                var coli = "bg-success";
              }
              $('#tbody_bundlesx').append(' <tr><td><i class="fa fa-' + data.bundles.icon + '"></i></td><td>' +
                data.bundles.szBundle + '</td><td>' + data.bundles.ValidFrom + '</td> <td>' + data.bundles.UsedValue +
                ' / ' + data.bundles.AssignedValue +
                '</td><td><div id="progress" class="graph"><div id="bar" style="width:'+b.bundles.Percentage+'%"><p>'+b.bundles.Percentage+'% used</p></div></div></td></tr>');
            } else {
              $('#tbody_bundlesx').append(
                ' <tr><td colspan="5" class="text-center">No Bundle assigned</td></tr>');
            }


          }
        } else {
          $('#tbody_bundlesx').hide();

        }
        if (data.packages) {
          $('#packagesx').show();
          if (data.packages.length > 1 && data.packages.length != 0) {
            data.packages.forEach(function (b) {
              //console.log(b);
              if (b.Available == "1") {
                var t = ' checked';
              } else {
                var t = '';
              }
              $('#packagesx').append(' <tr> <td>' + b.CallModeDescription +
                '</td><td><div class="custom-switch custom-switch-label-onoff"><input class="custom-switch-input packageid" id="' +
                b.PackageDefinitionId + '" type="checkbox"' + t +
                '><label class="custom-switch-btn" for="' + b.PackageDefinitionId +
                '"></label> </div></td> </tr>');
            });
          } else {
            if (data.packages.Available == "1") {
              var t = ' checked';
            } else {
              var t = '';
            }
            $('#packagesx').append(' <tr> <td>' + data.packages.CallModeDescription +
              '</td><td><div class="custom-switch custom-switch-label-onoff"><input class="custom-switch-input packageid" id="' +
              data.packages.PackageDefinitionId + '" type="checkbox"' + t +
              '><label class="custom-switch-btn" for="' + data.packages.PackageDefinitionId +
              '"></label> </div></td> </tr>');
          }
          $("#packagesx").append(
            '<script> $(".packageid").change(function() { var userid = "<?php echo $service->userid; ?>"; var serviceid = "<?php echo $service->id; ?>"; var sn = "<?php echo $service->details->msisdn_sn; ?>"; console.log(sn); var id = $(this).attr("id"); var msisdn = "<?php echo trim($service->domain); ?>"; if($(this).is(":checked")) { var val = "1"; }else{ var val = "0"; } $.ajax({ url: window.location.protocol + "//" + window.location.host + "/reseller/subscription/change_packagesetting", type: "post", dataType: "json", success: function (data) { console.log(data); }, data: {"sn": sn, "id": id, "val":val, "msisdn":msisdn, "userid":userid, "serviceid":serviceid} }); });<\/script>'
          );
        }

      }
    });

  });
</script>

<div class="modal fade" id="portingModal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content" style="height: 700px;">
      <input type="hidden" name="SN" value="<?php echo $service->details->msisdn_sn; ?>">
      <input type="hidden" name="userid" value="<?php echo $service->userid; ?>">
      <input type="hidden" name="msisdn" value="<?php echo $service->domain; ?>">
      <input type="hidden" name="serviceid" value="<?php echo $service->id; ?>">
      <input type="hidden" name="typebar" value="2">
      <!-- Modal Header SimCardNbr -->
      <div class="modal-header">
        <h4 class="modal-title"><?php echo lang('Porting Letter'); ?></h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!-- Modal body -->
      <div class="modal-body">
        <div class="row">
          <div class="col-sm-12">
          <div id="portingPdf"></div>
          <table class="table table-striped">
              <div id="loadingx20"  style="display:none;">
                <img src="<?php echo base_url(); ?>assets/img/loader1.gif" height="100">
              </div>
            </table>
          </div>
        </div>
      </div>
      <!-- Modal footer -->
    </div>
  </div>
</div>


<div class="modal fade" id="newModal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content" style="height: 700px;">
      <input type="hidden" name="SN" value="<?php echo $service->details->msisdn_sn; ?>">
      <input type="hidden" name="userid" value="<?php echo $service->userid; ?>">
      <input type="hidden" name="msisdn" value="<?php echo $service->domain; ?>">
      <input type="hidden" name="serviceid" value="<?php echo $service->id; ?>">
      <input type="hidden" name="typebar" value="2">
      <!-- Modal Header SimCardNbr -->
      <div class="modal-header">
        <h4 class="modal-title"><?php echo lang('Welcome Letter'); ?></h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!-- Modal body -->
      <div class="modal-body">
        <div class="row">
          <div class="col-sm-12">
          <div id="newPdf"></div>
          <table class="table table-striped">
              <div id="loadingx30"  style="display:none;">
                <img src="<?php echo base_url(); ?>assets/img/loader1.gif" height="100">
              </div>
            </table>
          </div>
        </div>
      </div>
      <!-- Modal footer -->
    </div>
  </div>
</div>


<div class="modal fade" id="Suspend">
  <div class="modal-dialog modal-lg">
    <div class="modal-content" style="height: 700px;">
      <input type="hidden" name="SN" value="<?php echo $service->details->msisdn_sn; ?>">
      <input type="hidden" name="userid" value="<?php echo $service->userid; ?>">
      <input type="hidden" name="msisdn" value="<?php echo $service->domain; ?>">
      <input type="hidden" name="serviceid" value="<?php echo $service->id; ?>">
      <input type="hidden" name="typebar" value="2">
      <!-- Modal Header SimCardNbr -->
      <div class="modal-header">
        <h4 class="modal-title"><?php echo lang('Swap Letter'); ?></h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!-- Modal body -->
      <div class="modal-body">
        <div class="row">
          <div class="col-sm-12">
          <div id="swapPdf"></div>
            <table class="table table-striped">
              <div id="loadingx40"  style="display:none;">
                <img src="<?php echo base_url(); ?>assets/img/loader1.gif" height="100">
              </div>
            </table>
          </div>
        </div>
      </div>
      <!-- Modal footer -->
    </div>
  </div>
</div>



<div class="modal fade" id="SwapModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <form method="post" action="<?php echo base_url(); ?>reseller/subscription/swap_simcard">
        <input type="hidden" name="msisdn" value="<?php echo trim($service->domain); ?>">
        <input type="hidden" name="SN" value="<?php echo $service->details->msisdn_sn; ?>">
        <input type="hidden" name="userid" value="<?php echo $service->userid; ?>">
        <input type="hidden" name="serviceid" value="<?php echo $service->id; ?>">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">
            <?php echo lang('Do you wish to swap SIMcard?'); ?>
          </h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-12">
              <div class="form-group">
                <fieldset>
                  <label class="control-label" for="msisdn">
                    <?php echo lang('CLI(Number)'); ?></label>
                  <input name="new_simnumber" class="form-control" type="text" value="<?php echo trim($service->details->msisdn); ?>"
                    readonly>
                </fieldset>
              </div>
            </div>
            <div class="col-sm-12">
              <div class="form-group">
                <fieldset>
                  <label class="control-label" for="msisdn">
                    <?php echo lang('Current SIMCARD'); ?></label>
                  <input name="new_simnumber" class="form-control" type="text" value="<?php echo $service->details->msisdn_sim; ?>"
                    readonly>
                </fieldset>
              </div>
            </div>
            <div class="col-sm-12">
              <div class="form-group">
                <fieldset>
                  <label class="control-label" for="msisdn">
                    <?php echo lang('New SIMCARD'); ?></label>
                  <input name="new_simnumber" id="simcard" class="form-control" type="text" value="" required>
                  <span class="text-help">
                    <?php echo lang('This will revoke old simcard, please make sure the simcard has been received by customer before executing this'); ?></span>
                </fieldset>
              </div>
            </div>
          
          


          </div>


        </div>
        <div class="modal-footer">
          <button type="submit" id="continue" class="btn btn-md btn-primary" disabled><i class="fa fa-thumbs-up"></i>
            <?php echo lang('Yes Swap Simcard'); ?></button>
        </div>
      </form>
    </div>
  </div>
</div>
<script src="<?php echo base_url(); ?>assets/clear/js/pdfobject.js"></script>
<script>
function pdfshow(type, serviceid){

  $('#'+type+'Modal').modal('toggle');
  if(type == "porting"){
    $('#loadingx20').show();
  }
  if(type == "new"){
    $('#loadingx30').show();
  }
  if(type == "swap"){
    $('#loadingx40').show();
  }

  PDFObject.embed(window.location.protocol + '//' + window.location.host + '/reseller/subscription/download_letter/'+serviceid+'/'+type, "#"+type+'Pdf', {
    height: "600px"});
    $('#loadingx20').hide();
    $('#loadingx30').hide();
    $('#loadingx40').hide();

}

</script>
<script>
  $( "#simcard" ).autocomplete({
  source: function( request, response ) {
   $.ajax({
    url: window.location.protocol + '//' + window.location.host + '/reseller/table/search_simcard',
    type: 'post',
    dataType: "json",
    data: {
     keyword: request.term
    },
    success: function( data ) {
    $('#continue').prop('disabled', true); // display the selected tex
     response( data );
    }
   });
  },
  select: function (event, ui) {
   $('#simcard').val(ui.item.label); // display the selected tex
   $('#continue').prop('disabled', false); // display the selected tex
  
   return false;
  }
 });
</script>