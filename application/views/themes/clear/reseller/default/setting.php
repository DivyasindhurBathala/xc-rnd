<div class="main-panel" style="padding-top:30px; padding-left:10px;padding-right:10px;">
	<div class="content-wrapper">
		<div class="row">
			<div class="col-sm-8 mr-auto ml-auto">
				<div class="card border-light">
					<div class="card-body">
						<form method="post" action="<?php echo base_url(); ?>reseller/dashboard/setting">
							<h4 class="card-title">My Account</h4>
							<?php if (!empty($this->session->flashdata('error'))) {?>
							<div class="row">
								<div class="col-lg-12">
									<div class="alert alert-danger" role="alert">
										<strong class="text-center">
											<?php echo $this->session->flashdata('error'); ?></strong>
									</div>
								</div>
							</div>
							<?php }?>
							<?php if (!empty($this->session->flashdata('success'))) {?>
							<div class="row">
								<div class="col-lg-12">
									<div class="alert alert-warning" role="alert">
										<strong class="text-center">
											<?php echo $this->session->flashdata('success'); ?></strong>
									</div>
								</div>
							</div>
							<?php }?>
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">

										<label class="control-label" for="agent">
											<?php echo lang('Agent Name'); ?><span class="text-danger">*</span></label>
										<input name="agent" class="form-control" id="agent" type="text" placeholder="Company Name" value="<?php echo $agent->agent; ?>"
										 required readonly>

									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">

										<label class="control-label" for="contact_name">
											<?php echo lang('Contact Name'); ?></label>
										<input name="contact_name" class="form-control" id="contact_name" type="text" placeholder="<?php echo lang('Contact Name'); ?>"
										 value="<?php echo $agent->contact_name; ?>" required>

									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label class="control-label" for="email">
											<?php echo lang('Email'); ?></label>
										<input name="email" class="form-control" id="email" type="text" placeholder="<?php echo lang('Email'); ?>"
										 value="<?php echo $agent->email; ?>" required>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label class="control-label" for="phonenumber">
											<?php echo lang('Phonenumber'); ?></label>
										<input name="phonenumber" class="form-control" id="phonenumber" type="text" placeholder="<?php echo lang('Phonenumber'); ?>"
										 value="<?php echo $agent->phonenumber; ?>" required>
									</div>
								</div>


								<div class="col-sm-6">
									<div class="form-group">
										<label class="control-label" for="email">
											<?php echo lang('Address'); ?></label>
										<input name="address1" class="form-control" id="address1" type="text" placeholder="<?php echo lang('Address1'); ?>"
										 value="<?php echo $agent->address1; ?>" required>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="form-group">
										<label class="control-label" for="postcode">
											<?php echo lang('Postcode'); ?></label>
										<input name="postcode" class="form-control" id="postcode" type="text" placeholder="<?php echo lang('Postcode'); ?>"
										 value="<?php echo $agent->postcode; ?>" required>
									</div>
								</div>

								<div class="col-sm-3">
									<div class="form-group">
										<label class="control-label" for="city">
											<?php echo lang('City'); ?></label>
										<input name="city" class="form-control" id="city" type="text" placeholder="<?php echo lang('city'); ?>" value="<?php echo $agent->city; ?>"
										 required>
									</div>
								</div>


								<div class="col-sm-6">
									<div class="form-group">
										<label class="control-label" for="Language">
											<?php echo lang('Language'); ?></label>


										<select class="form-control" id="language" name="language">
											<?php foreach (getLanguages() as $key => $lang) {?>
											<option value="<?php echo $key; ?>" <?php if ($key==$agent->language) {?> selected
												<?php }?>>
												<?php echo lang($lang); ?>
											</option>
											<?php }?>
										</select>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label class="control-label" for="Language">
											<?php echo lang('Country'); ?></label>
										<select class="form-control" id="country" name="country">
											<?php foreach (getCountries() as $key => $country) {?>
											<option value="<?php echo $key; ?>" <?php if ($key==$agent->country) {?> selected
												<?php }?>>
												<?php echo $country; ?>
											</option>
											<?php }?>
										</select>
									</div>
								</div>


							</div>
							<div class="row">
								<div class="col-sm-4">
									<button class="btn btn-block btn-primary btn-md" type="submit">Save</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>