<div class="main-panel" style="padding-top:40px; padding-left:10px;padding-right:10px;padding-bottom:50px;">
<div class="content-wrapper">
    <div class="col-sm-12">
<div class="card border-light">
  <div class="card-body">
    <h4 class="card-title"><?php echo lang('Credit History'); ?></h4>
     <table id="orders" class="table table-striped table-bordered">
        <thead>
          <tr>
                <th><?php echo lang('Date'); ?></th>
                <th><?php echo lang('Type'); ?></th>
                <th><?php echo lang('Amount'); ?></th>
                <th><?php echo lang('Msisdn'); ?></th>
              </tr>
        </thead>
        <tbody>
</tbody>
</table>
  </div>
</div>
</div>
</div>
</div>

<script>
  $(document).ready(function()
{
 $.getJSON(window.location.protocol + '//' + window.location.host + '/reseller/table/get_lang', function(data) {
 $('#orders').DataTable({
"autoWidth": false,
"processing": true,
"orderCellsTop": true,
"ordering": true,
"serverSide": true,
"colReorder": true,
"ajax": window.location.protocol + '//' + window.location.host + '/reseller/table/get_reseller_credit_usage',
"aaSorting": [[1, 'desc']],
"language": {
"url": window.location.protocol + '//' + window.location.host +"/assets/clear/js/datatables/lang/" + data.result + ".json"
},
"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
$('td:eq(2)', nRow).html('- <?php echo $this->data['setting']->currency; ?>' + aData[2]);
return nRow;
},
});

});
});
</script>
