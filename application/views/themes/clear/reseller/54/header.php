<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Reseller: <?php echo $setting->companyname; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/clear/reseller/bootstrap.min.css" media="screen">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/clear/reseller/custom.min.css?version=2.5">
 <!--   https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.css -->

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js" type="text/javascript"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css">

  <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
  <link rel="stylesheet" href="https://mijnmobiel.delta.nl/assets/clear/jquery-ui/jquery-ui.min.css">
    <link rel="stylesheet" href="https://mijnmobiel.delta.nl/assets/clear/jquery-ui/jquery-ui.theme.min.css">
  </head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-primary fixed-top">
  <a class="navbar-brand" href="#"><img src="<?php echo $setting->logo_site; ?>" height="40"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarColor01">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="<?php echo base_url(); ?>reseller"><i class="fa fa-home"></i> <strong>Home</strong> </a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
         <i class="fa fa-users"></i> <strong>Clients</strong>
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="<?php echo base_url();?>reseller/dashboard/client"><i class="fa fa-tasks"></i> List Clients</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="<?php echo base_url();?>reseller/client/add"><i class="fa fa-user-plus"></i> Add Client</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="<?php echo base_url();?>reseller/client/export_clients"><i class="fa fa-tasks"></i> Export Clients</a>

        </div>
      </li>

      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
         <i class="fa fa-cube"></i> <strong>Subscriptions</strong>
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="<?php echo base_url();?>reseller/dashboard"><i class="fa fa-cubes"></i> List Subscriptions</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="<?php echo base_url();?>reseller/subscription/export_subscriptions"><i class="fa fa-tasks"></i> Export Subscription</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="<?php echo base_url();?>reseller/dashboard/order"><i class="fa fa-shopping-cart"></i> New Order</a>
        </div>
      </li>
      <?php if($setting->whmcs_ticket){ ?>
      <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url();?>reseller/helpdesk"><i class="fa fa-cog"></i> <strong>Helpdesk</strong></a>
      </li>
    <?php } ?>
      <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url();?>reseller/dashboard/setting"><i class="fa fa-cog"></i> <strong>My Account</strong></a>
      </li>
        <?php if (!empty($this->session->id)) { ?>
        <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url();?>admin/agent/detail/<?php echo $this->session->reseller['id']; ?>"><i class="fa fa-user-md"></i> <strong>Back to Admin Pane</strong>l</a>
      </li>
        <?php } ?>

    </ul>

      <a class="btn btn-secondary my-2 my-sm-0" href="<?php echo base_url(); ?>reseller/auth/logout"><i class="fa fa-sign-out"></i> Logout</a>

  </div>
</nav>
<div style="padding-bottom: 40px;padding-top: 60px;">



