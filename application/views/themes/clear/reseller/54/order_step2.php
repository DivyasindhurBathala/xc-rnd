<?php //print_r($_SESSION); ?>
<div class="main-panel" style="padding-top:60px;padding-bottom:30px; padding-left:10px;padding-right:10px;">
  <div class="content-wrapper">
    <div class="row">
      <div class="col-sm-8 mr-auto ml-auto">
        <div class="card border-light">
          <div class="card-body">
            <h4 class="card-title">Add Orders - Step 2</h4>
            <form method="post" action="<?php echo base_url(); ?>reseller/dashboard/order_step2">
              <div class="form-group">
                <label for="">
                  <?php echo lang('SIM Order'); ?></label>
                <select class="form-control" id="migration" name="msisdn_type">
                  <option value="porting">
                    <?php echo lang('Yes, I wish to keep my Number'); ?>
                  </option>
                  <option value="new" selected>
                    <?php echo lang('No, Order me New Number'); ?>
                  </option>
                </select>
              </div>
              <div id="migrationoption" style="display:none;">
                <div class="form-group">
                  <label for="">
                    <?php echo lang('Mobile Number'); ?> :
                    <?php echo getcountrycode($setting->country_base); ?>XXXXXXXXXX</label>
                  <input class="form-control" autocomplete="new-username" placeholder="<?php echo lang('REQUIRED'); ?>"
                    type="number" name="msisdn" id="msisdn" value="<?php echo getcountrycode($setting->country_base); ?>">
                </div>
                <div class="form-group">
                  <label for="">Type</label>
                  <select class="form-control" id="type_number" name="donor_type">
                    <option value="0" selected>
                      <?php echo lang('Prepaid'); ?>
                    </option>
                    <option value="1">
                      <?php echo lang('Postpaid'); ?>
                    </option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="">
                    <?php echo lang('Donor Provider'); ?></label>
                  <select class="form-control" id="provider" name="donor_provider">
                    <?php foreach (getDonors($setting->country_base) as $row) {?>
                    <option value="<?php echo $row->operator_code; ?>" selected>
                      <?php echo $row->operator_name; ?>
                    </option>
                    <?php }?>
                  </select>
                </div>
                <?php if ($setting->country_base != "NL") { ?>
                <div class="form-group">
                  <label for="">
                    <?php echo lang('PORTING CUSTUMER TYPE'); ?></label>
                  <select class="form-control" id="ptype_belgium" name="ptype_belgium">
                    <?php foreach (array('PREPAY','POSTPAY_SIMPLE','POSTPAY_COMPLEX','NA') as $row) {?>
                    <option value="<?php echo $row; ?>" selected>
                      <?php echo $row; ?>
                    </option>
                    <?php }?>
                  </select>
                </div>
                <?php } ?>

                <div class="form-group">
                  <label for="">
                    <?php echo lang('Donor SIMcard Number'); ?></label>
                  <input class="form-control" autocomplete="new-username" placeholder="" type="text" name="donor_sim"
                    id="simnumber">
                </div>

                <div class="form-group">
                  <label for="">
                    <?php echo lang('Donor Customer Type'); ?></label>
                  <select class="form-control" id="customertype1" name="customertype1">
                    <option value="0" selected>
                      <?php echo lang('Residential'); ?>
                    </option>
                    <option value="1">
                      <?php echo lang('Bussiness'); ?>
                    </option>
                  </select>
                </div>
                <div class="form-group" style="display:none;" id="acct">
                  <label for="">
                    <?php echo lang('Donor ClientNumber provider'); ?></label>
                  <input class="form-control" autocomplete="new-username" placeholder="" type="text" name="donor_accountnumber"
                    id="AccountNumber">
                </div>
                <?php if ($setting->country_base == "NL") { ?>
                <div class="form-group">
                  <label for="">
                    <?php echo lang('PortIN Date Wish'); ?>:</label>
                  <?php if ($setting->country_base == "NL") { ?>
                  <input class="form-control" autocomplete="new-username" type="text" value="<?php echo date('Y-m-d'); ?>" name="date_wish" id="pickdate8"
                    readonly>
                  <?php } else { ?>
                  <input class="form-control" autocomplete="new-username" type="text" value="<?php echo date('Y-m-d'); ?>" name="date_wish" id="pickdate9"
                    readonly>
                  <?php } ?>
                </div>
                <?php } ?>
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="">
                        <?php echo lang('Contract Start Date'); ?> MM-DD-YYYY</label>
                      <input class="form-control" autocomplete="new-username" value="<?php echo date('m-d-Y'); ?>" type="text"
                        name="date_contract" id="pickdate20" readonly>
                    </div>
                  </div>
                </div>

              </div>
              <div class="row">
                <div class="col-md-4">
                  <a class="btn btn-block btn-md btn-primary" href="<?php echo base_url(); ?>reseller/dashboard/order_step1"><i
                      class="fa fa-arrow-left"></i> Back</a>
                </div>
                <div class="col-md-4">
                  <button type="submit" class="btn btn-block btn-md btn-primary"><i class="fa fa-arrow-right"></i> Next</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card border-light">
          <div class="card-body">
            <h4 class="card-title">Order Summary - Step 3</h4>
            <table class="table table-striped table-hover">
              <tr>
                <td>Product Order</td>
                <td>
                  <?php echo getProductName($_SESSION['order']['pid']); ?>
                </td>
              </tr>
              <tr>
                <td>Recurring Amount</td>
                <td>
                  <?php echo $_SESSION['order']['recurring']; ?>
                </td>
              </tr>

              <tr>
                <td>Client ID</td>
                <td>
                  <?php echo $_SESSION['order']['clientid']; ?>
                </td>
              </tr>

              <tr>
                <td>Client Name</td>
                <td>
                  <?php echo $client->firstname.' '.$client->lastname; ?>
                </td>
              </tr>

              <tr>
                <td>Type Order</td>
                <td>
                  <?php if($_SESSION['order']['msisdn_type'] == "new") { ?>New Number
                  <?php }else{ ?>Porting Number
                  <?php } ?>
                </td>
              </tr>

              <?php if($_SESSION['order']['msisdn_type'] != "new"){ ?>
              <tr>
                <td>Number to Port</td>
                <td>
                  <?php echo $_SESSION['order']['msisdn']; ?>
                </td>
              </tr>
              <?php } ?>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script>
    $(document).ready(function () {
    var mig = $('#migration').find(":selected").val();
    console.log(mig);
    if(mig == "porting"){
      $('#migrationoption').show('slow');
    }else{
      $('#migrationoption').hide('slow');
    }
    $("#customertype1").change(function () {
      var sel = $('#customertype1').find(":selected").val();
      console.log(sel);
      if (sel == "1") {
        $('#acct').show('slow');
        $('#AccountNumber').prop('required', true);
      } else {
        $("#acct").hide('slow');
        $('#AccountNumber').prop('required', false);
      }
    });
    $("#migration").change(function () {
      var sel = $('#migration').find(":selected").val();
      $('#simnumber').prop('required', true);
      $('#pickdate20').prop('required', true);
      <?php if ($setting->country_base == "NL") { ?>
      $('#pickdate8').prop('required', true);
      <?php } ?>
      if (sel == "porting") {

        $('#migrationoption').show('slow');
      } else {
        $('#migrationoption').hide('slow');
        $('#step3').prop('disabled', false);
      }
    });
  });
  </script>
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/clear/js/datepickers.js?version=1.11"></script>