<script type="text/javascript" src="https://portal.central-services.nl/assets/clear/js/datatables/reseller/helpdesk.js?version=1.8"></script>
<div class="main-panel" style="padding-top:40px; padding-left:10px;padding-right:10px;">
<div class="content-wrapper">
<div class="row">
<div class="col-sm-3">
<div class="card border-light">
  <div class="card-body">
 <form method="post" action="<?php echo base_url(); ?>reseller/helpdesk/newticket" enctype="multipart/form-data">
              <input type="hidden" name="admin" value="<?php echo $this->session->firstname . ' ' . $this->session->lastname; ?>">
              <div class="modal-body" id="modalonly">
                <div class="form-group">
                  <label for="subject">Client</label>
                  <input id="customerid" name="userid" class="form-control  ui-autocomplete-input ui-autocomplete-loading modalonly" placeholder="Search Customer.." type="text" required>
                </div>
                <div id="pelanggan" style="display:none;">
                  <div class="form-group">
                    <label for="subject">Client</label>
                    <input id="namapelanggan" name="name" class="form-control" placeholder="" type="text" disabled>
                  </div>
                </div>
                <div class="form-group">
                  <label for="subject">Subject</label>
                  <input name="subject" class="form-control" placeholder="Title" type="text" required>
                </div>
                <div class="form-group">
                  <label for="departmen">Category</label>
                  <select class="form-control" id="departmen" name="categoryid">
                    <?php foreach (getHelpdeskCategory($this->session->cid) as $row) {?>
                    <option value="<?php echo $row->id; ?>"><?php echo $row->name; ?></option>
                    <?php }?>
                  </select>
                </div>
                <div class="form-group">
                  <label for="departmen">Department</label>
                  <select class="form-control" id="departmen" name="deptid">
                    <?php foreach (getDepartments($this->session->cid) as $row) {?>
                    <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
                    <?php }?>
                  </select>
                </div>
                <div class="form-group">
                  <label for="exampleSelect1">Assign to</label>
                  <select class="form-control" id="exampleSelect1" name="assigne">
                    <?php foreach (getStaf($this->session->cid) as $staf) {?>
                    <option value="<?php echo $staf['id']; ?>"<?php if ($staf['id'] == $this->session->id) {?> selected<?php }?>><?php echo $staf['firstname'] . ' ' . $staf['lastname']; ?></option>
                    <?php }?>
                  </select>
                </div>
                <div class="form-group">
                  <label for="file">Attachments</label>
                  <input name="attachments[]" type="file" class="form-control-file" id="attachments" aria-describedby="fileHelp" multiple="">
                  <small id="fileHelp" class="form-text text-muted">Allowed type docx, doc, xls, xlsx, pdf, gif, png, jpg.</small>
                </div>
                <div class="form-group">
                  <label for="message">Message</label>
                  <textarea name="message" class="form-control" id="message" rows="6"></textarea>
                </div>
              </div>
              <div class="modal-footer">
                <input type="hidden" name="admin" value="<?php echo $this->session->firstname . ' ' . $this->session->lastname; ?>">
                <button class="btn btn-primary" type="submit" id="bos" disabled> Save changes</button>
              </div>
            </form>

  	</div>
  </div>
</div>
<div class="col-sm-9">
<div class="card border-light">
  <div class="card-body">
  	<table class="table table-striped table-bordered" id="helpdesk">
  		<thead>
  			<th><?php echo lang('ID'); ?></th>
  			<th><?php echo lang('Customername'); ?></th>
  			<th><?php echo lang('Subject'); ?></th>
  			<th><?php echo lang('Department'); ?></th>
  			<th><?php echo lang('Date'); ?></th>
  			<th><?php echo lang('Status'); ?></th>
  			<th><?php echo lang('Assigne'); ?></th>
  			<th><?php echo lang('Priority'); ?></th>
  			<th><?php echo lang('Category'); ?></th>
  		</thead>
  		<tbody>
  		</tbody>
  	</table>
  	</div>
  </div>
</div>
</div>
</div>
</div>

<script>
   $( "#customerid" ).autocomplete({
  source: function( request, response ) {
   // Fetch data
   $('#pelanggan').hide('slow');
   $.ajax({
    url: window.location.protocol + '//' + window.location.host + '/admin/complete/searchclient',
    type: 'post',
    dataType: "json",
    data: {
     keyword: request.term
    },
    success: function( data ) {
     response( data );
    }
   });
  },
  select: function (event, ui) {
     $('#step3').prop('disabled', false);
   // Set selection
   $('#customerid').val(ui.item.value); // display the selected tex
   $('#customername').val(ui.item.label);
   $('#namapelanggan').val(ui.item.label);
   $('#bos').prop('disabled', false);
   $('#pelanggan').show('slow');
   $('#namapelanggan').focus();

   return false;
  }
 });
</script>