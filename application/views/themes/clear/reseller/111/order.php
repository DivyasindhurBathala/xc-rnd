<div class="main-panel" style="padding-top:60px; padding-left:10px;padding-right:10px;">
<div class="content-wrapper">
<div class="row">
<div class="col-sm-8 mr-auto ml-auto">
<div class="card border-light">
  <div class="card-body">
    <h4 class="card-title">Add Orders</h4>
    <form method="post" action="<?php echo base_url(); ?>reseller/dashboard/order">
   
                  <div class="form-group">
                    <label for=""> <?php echo lang('Customer Type'); ?>:</label>
                    <select class="form-control" id="customertype" name="customer_type">
                      <option value="old"><?php echo lang('Existing Customer'); ?></option>
                      <option value="new" selected><?php echo lang('New Customer'); ?></option>
                    </select>
                  </div>

                 <div id="toli">
                  <div class="form-group" id="existing" style="display:none;">
                    <label for=""><?php echo lang('Customer ID'); ?>:</label>
                    <input class="form-control ui-autocomplete-input customerid" id="customerid" autocomplete="new-username" placeholder="search..." type="text" name="clientid">
                  </div>
               </div>
                <div  style="display:none;" id="pelanggan">
                  <div class="form-group">
                    <label for=""><?php echo lang('Customer Name'); ?>:</label>
                    <input class="form-control" id="customername" autocomplete="new-username" placeholder="search..." type="text" id="namapelanggan" readonly>
                  </div>
                </div>
                <div class="row">
                <div class="col-md-4">
             <button type="submit" class="btn btn-block btn-md btn-primary">Next</button>
             </div>
             </div>
</form>
  </div>
</div>
</div>
</div>
    </div>
</div>

</div>
<script>
$("#customertype").change(function() {
var sel = $('#customertype').find(":selected").val();
if (sel == "old") {
$('#existing').show('slow');
$('#customerid').val('');
} else {
$('#pelanggan').hide();
$('#toli').show('hide');
$('#existing').hide('slow');
$('#customerid').val('0');
$("#companyname").val('');
$("#firstname").val('');
$("#lastname").val('');
$("#email").val('');
$("#address1").val('');
$("#city").val('');
$("#postcode").val('');
$("#phonenumber").val('');
}
});
$( "#customerid" ).autocomplete({
  source: function( request, response ) {
   $('#pelanggan').hide('slow');
   $.ajax({
    url: window.location.protocol + '//' + window.location.host + '/reseller/table/searchclient',
    type: 'post',
    dataType: "json",
    data: {
     keyword: request.term
    },
    success: function( data ) {
     response( data );
    }
   });
  },
  select: function (event, ui) {
   $('#customerid').val(ui.item.value); // display the selected tex
   $('#customername').val(ui.item.label);
   $('#namapelanggan').val(ui.item.label);
   $('#pelanggan').show('slow');
   return false;
  }
 });
</script>