<div class="main-panel" style="padding-top:30px; padding-left:10px;padding-right:10px;padding-bottom:40px">
  <div class="content-wrapper">
    <div class="row">
      <div class="col-sm-8 mr-auto ml-auto">
        <div class="card border-light">
          <div class="card-body">
            <h4 class="card-title">Add Orders - Step 1</h4>
            <form method="post" action="<?php echo base_url(); ?>reseller/dashboard/order_step1">
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="">
                      <?php echo lang('Product'); ?>:</label>
                    <select class="form-control" id="addonproduct" name="pid">
                      <?php foreach (getProductSell($this->session->cid, $_SESSION['reseller']['id']) as $row) {
    ?>
                      <option value="<?php echo $row->id; ?>">
                        <?php echo $row->name; ?>
                        <?php if ($row->setup > 0) {
        echo 'Setup fee: &euro;' . number_format($row->setup, 2);
    } ?>
                      </option>
                      <?php
}?>
                    </select>
                  </div>
                </div>
                <div class="col-md-4">
                <div class="form-group">
                  <label for=""><?php echo lang('Promotion'); ?></label>
                    <select class="form-control" id="promotion" name="promo_code">
                      <option value="None">
                        <?php echo lang('None'); ?>
                      </option>
                      <?php foreach (getPromotions() as $p) {
        ?>
                      <option value="<?php echo $p['promo_code']; ?>">
                        <?php echo $p['promo_code']; ?> -
                        <?php echo $p['promo_name']; ?>
                      </option>
                      <?php
    }?>
                    </select>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                  
                    <input name="recurring" type="hidden" class="form-control" id="harga" value="0.00">
                  </div>
                </div>
                <div class="col-md-12">
                 
                </div>


                  <div class="col-md-6">

                 <div class="form-group" id="addonx">
                    <label for=""><?php echo lang('Options'); ?>:</label>
                    <select name="addon" class="optionaddon form-control">
                    <option value="NONE">Select Option Addon</option>
                  
                    <?php foreach (getAddonSell('mobile', $this->session->cid) as $row) {
        ?>
                      <option value="<?php echo $row->id; ?>"><?php echo str_replace('Option ', '', $row->name) . ' '.$setting->currency.' ' . number_format($row->recurring_total, 2) . lang('/month'); ?></option>
                      <?php
    } ?>
                    </select>

                   
                  
                  </div>

                </div>
                <div class="col-md-6">
                <div id="addon_renew">
                    </div>
                    </div>

              </div>
              <div class="row">
                <div class="col-md-4">
                  <a class="btn btn-block btn-md btn-dark" href="<?php echo base_url(); ?>reseller/dashboard/order"><i
                      class="fa fa-arrow-left"></i> Back</a>
                </div>
                <div class="col-md-4">
                  <button type="submit" class="btn btn-block btn-md btn-dark"><i class="fa fa-arrow-right"></i> Next</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

</div>
<script>
  $(document).ready(function () {
 $(".optionaddon").change(function() {
    
       
       $('#addon_renew').html('<div class="form-group"> <label for=""><?php echo lang('Auto renew for '); ?> (<?php echo lang('Month'); ?> 1 month = 30 days)</label> <select class="form-control" id="contractduration" name="ContractDuration"> <option value="1" selected><?php echo lang("One time"); ?></option> <option value="2">1 <?php echo lang("Month"); ?></option> <option value="3">3 <?php echo lang("Months"); ?></option> <option value="6">6 <?php echo lang("Months"); ?></option> <option value="12">12 <?php echo lang("Months"); ?></option> <option value="24">24 <?php echo lang("Months"); ?></option> <option value="36">36 <?php echo lang("Months"); ?></option> <option value="600"> <?php echo lang("Indefinite"); ?></option> </select> </div>');
 
     
 }); 
  });
</script>