<div class="main-panel" style="padding-bottom:60px;padding-top:30px; padding-left:10px;padding-right:10px;">
  <div class="content-wrapper">
    <div class="row">
      <div class="col-sm-8">
        <div class="card border-light">
          <div class="card-body">
            <h4 class="card-title">Customer Billing Information - Complete Order
              <?php if ($setting->country_base == "UK") {
    ?>
           <div class="float-right"><button type="button" onclick="GenAnn()" class="btn btn-sm btn-primary">Generate Anonymous</button></div>
        <?php
} ?>
      </h4>
            <form id="orderfinal">
              <?php if ($_SESSION['order']['customer_type'] == "new") {
        ?>
                <?php if ($setting->mobile_platform == "ARTA") {
            ?> 
              <div class="row">
                <div class="col-sm-4">
                  <div class="form-group">
                    <fieldset>
                      <label class="control-label" for="vat">
                        <?php echo lang('Relation ID'); ?><span class="text-danger">*</span></label>
                      <input name="mvno_id" class="form-control" id="deltaid" type="text" placeholder="<?php echo lang('Leave Empty for auto generate'); ?>"
                        <?php if (!empty($_SESSION['registration']['mvno_id'])) {
                ?> value="
                      <?php echo $_SESSION['registration']['mvno_id']; ?>"
                      <?php
            } ?> readonly>
                    </fieldset>
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="form-group">
                    <fieldset>
                      <label class="control-label" for="vat">
                        <?php echo lang('Btw'); ?></label>
                      <input name="vat" class="form-control" id="vat" type="text" placeholder="<?php echo lang('Vat Number'); ?>"
                        <?php if (!empty($_SESSION['registration']['vat'])) {
                ?> value="
                      <?php echo $_SESSION['registration']['vat']; ?>"
                      <?php
            } ?>>
                    </fieldset>
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="form-group">
                    <fieldset>
                      <label class="control-label" for="companyname">
                        <?php echo lang('Companyname'); ?></label>
                      <input name="companyname" class="form-control" id="companyname" type="text" placeholder="<?php echo lang('Comapnyname'); ?>"
                        <?php if (!empty($_SESSION['registration']['companyname'])) {
                ?> value="
                      <?php echo $_SESSION['registration']['companyname']; ?>"
                      <?php
            } ?>>
                    </fieldset>
                  </div>
                </div>
              </div>
            <?php
        } ?>
              <div class="row">
                <div class="col-sm-4">
                  <div class="form-group">
                    <label class="control-label" for="gender">
                      <?php echo lang('Gender'); ?><span class="text-danger">*</span></label>
                    <select class="form-control" id="gender" name="gender">
                      <option value="male">Male</option>
                      <option value="female">Female</option>
                    </select>
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="form-group">
                    <label for="">
                      <?php echo lang('Initial'); ?></label><input class="form-control" placeholder="<?php echo lang('Initial'); ?>"
                      name="initial" type="text" id="Initial">
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="form-group">
                    <label for="">
                      <?php echo lang('Salutation'); ?></label><input class="form-control" placeholder="<?php echo lang('Salutation'); ?>"
                      name="salutation" type="text" id="salutation">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <fieldset>
                      <label class="control-label" for="firstname">
                        <?php echo lang('Firstname'); ?><span class="text-danger">*</span></label>
                      <input name="firstname" class="form-control" id="firstname" type="text" placeholder="<?php echo lang('Firstname'); ?>"
                        <?php if (!empty($_SESSION['registration']['firstname'])) {
            ?> value="
                      <?php echo $_SESSION['registration']['firstname']; ?>"
                      <?php
        } ?> required>
                    </fieldset>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <fieldset>
                      <label class="control-label" for="lastname">
                        <?php echo lang('Lastname'); ?><span class="text-danger">*</span></label>
                      <input name="lastname" class="form-control" id="lastname" type="text" placeholder="<?php echo lang('Lastname'); ?>"
                        <?php if (!empty($_SESSION['registration']['lastname'])) {
            ?> value="
                      <?php echo $_SESSION['registration']['lastname']; ?>"
                      <?php
        } ?> required>
                    </fieldset>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-4">
                  <div class="form-group">
                    <fieldset>
                      <label class="control-label" for="address1">
                        <?php echo lang('Address'); ?><span class="text-danger">*</span></label>
                      <input name="address1" class="form-control" id="address1" type="text" placeholder="<?php echo lang('Address'); ?>"
                        <?php if (!empty($_SESSION['registration']['address1'])) {
            ?> value="
                      <?php echo $_SESSION['registration']['address1']; ?>"
                      <?php
        } ?> required>
                    </fieldset>
                  </div>
                </div>
                  <div class="col-sm-1">
                  <div class="form-group">
                    <fieldset>
                      <label class="control-label" for="Housenumber">
                        <?php echo lang('Housenumber'); ?><span class="text-danger">*</span></label>
                      <input name="housenumber" class="form-control" id="housenumber" type="text" placeholder="XX" <?php
                        if (!empty($_SESSION['registration']['housenumber'])) {
                            ?> value="
                      <?php echo $_SESSION['registration']['housenumber']; ?>"
                      <?php
                        } ?> required>
                    </fieldset>
                  </div>
                </div>

                  <div class="col-sm-1">
                  <div class="form-group">
                    <fieldset>
                      <label class="control-label" for="Alphabet">
                        <?php echo lang('Alphabet'); ?></label>
                      <input name="alphabet" class="form-control" id="alphabet" type="text" placeholder="XX" <?php
                        if (!empty($_SESSION['registration']['alphabet'])) {
                            ?> value="
                      <?php echo $_SESSION['registration']['alphabet']; ?>"
                      <?php
                        } ?> required>
                    </fieldset>
                  </div>
                </div>

                <div class="col-sm-2">
                  <div class="form-group">
                    <fieldset>
                      <label class="control-label" for="Postcode">
                        <?php echo lang('Postcode'); ?><span class="text-danger">*</span></label>
                      <input name="postcode" class="form-control" id="postcode" type="text" placeholder="NNNN XX" <?php
                        if (!empty($_SESSION['registration']['postcode'])) {
                            ?> value="
                      <?php echo $_SESSION['registration']['postcode']; ?>"
                      <?php
                        } ?> required>
                    </fieldset>
                  </div>
                </div>
                <div class="col-sm-2">
                  <div class="form-group">
                    <fieldset>
                      <label class="control-label" for="City">
                        <?php echo lang('City'); ?><span class="text-danger">*</span></label>
                      <input name="city" class="form-control" id="city" type="text" <?php if (!empty($_SESSION['registration'])) {
                            ?> value="
                      <?php echo $_SESSION['registration']['city']; ?>"
                      <?php
                        } ?> placeholder="
                      <?php echo lang('City'); ?>" required>
                    </fieldset>
                  </div>
                </div>
                <div class="col-sm-2">
                   <?php if ($setting->mobile_platform == "ARTA") {
                            ?>
                  <div class="form-group">
                    <fieldset>
                      <label for="exampleSelect1">
                        <?php echo lang('Country'); ?><span class="text-danger">*</span></label>
                      <select class="form-control" id="country" name="country">
                        <?php foreach (getCountries() as $key => $country) {
                                ?>
                        <option value="<?php echo $key; ?>" <?php if ($key=="NL") {
                                    ?> selected
                          <?php
                                } ?>>
                          <?php echo $country; ?>
                        </option>
                        <?php
                            } ?>
                      </select>
                    </fieldset>
                  </div>

                  <?php
                        } else {
                            ?>
                   <div class="form-group">
                    <fieldset>
                      <label for="exampleSelect1">
                        <?php echo lang('Country'); ?><span class="text-danger">*</span></label>
                      <select class="form-control" id="country" name="country">
                        <?php foreach (getCountries() as $key => $country) {
                                ?>
                        <option value="<?php echo $key; ?>" <?php if ($key=="UK") {
                                    ?> selected
                          <?php
                                } ?>>
                          <?php echo $country; ?>
                        </option>
                        <?php
                            } ?>
                      </select>
                    </fieldset>
                  </div>
                  <?php
                        } ?>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-3">
                   <?php if ($setting->mobile_platform == "ARTA") {
                            ?>
                  <div class="form-group">
                    <fieldset>
                      <label for="exampleSelect1">
                        <?php echo lang('Languages'); ?></label>
                      <select class="form-control" id="language" name="language">
                        <?php foreach (getLanguages() as $key => $lang) {
                                ?>
                        <option value="<?php echo $key; ?>" <?php if ($key=="dutch") {
                                    ?> selected
                          <?php
                                } ?>>
                          <?php echo lang($lang); ?>
                        </option>
                        <?php
                            } ?>
                      </select>
                    </fieldset>
                  </div>
                <?php
                        } else {
                            ?>
                     <div class="form-group">
                    <fieldset>
                      <label for="exampleSelect1">
                        <?php echo lang('Languages'); ?></label>
                      <select class="form-control" id="language" name="language">
                        <?php foreach (getLanguages() as $key => $lang) {
                                ?>
                        <option value="<?php echo $key; ?>" <?php if ($key=="english") {
                                    ?> selected
                          <?php
                                } ?>>
                          <?php echo lang($lang); ?>
                        </option>
                        <?php
                            } ?>
                      </select>
                    </fieldset>
                  </div>
                <?php
                        } ?>
                </div>
                <div class="col-sm-3">
                  <fieldset>
                    <label class="control-label" for="Phonenumber">
                      <?php echo lang('Contact Number'); ?><span class="text-danger">*</span></label>
                    <input name="phonenumber" class="form-control" id="phonenumber" type="number" placeholder="<?php echo lang('Contact Number'); ?>"
                      <?php if (!empty($_SESSION['registration']['phonenumber'])) {
                            ?> value="
                    <?php echo $_SESSION['registration']['phonenumber']; ?>"
                    <?php
                        } ?> required>
                  </fieldset>
                </div>
                <div class="col-sm-3">
                  <div class="form-group">
                    <fieldset>
                      <label class="control-label" for="Email">
                        <?php echo lang('Email'); ?><span class="text-danger">*</span></label>
                      <input name="email" class="form-control" id="email" type="email" placeholder="<?php echo lang('Email address'); ?>"
                        <?php if (!empty($_SESSION['registration']['email'])) {
                            ?> value="
                      <?php echo $_SESSION['registration']['email']; ?>"
                      <?php
                        } ?> required>
                    </fieldset>
                  </div>
                </div>
                 <?php if ($setting->mobile_platform == "ARTA") {
                            ?>
                <div class="col-sm-3">
                  <div class="form-group">
                    <fieldset>
                      <label class="control-label" for="NationalNr">
                        <?php echo lang('NationalNr'); ?></label>
                      <input name="nationalnr" class="form-control" id="NationalNr" type="number" <?php if (!empty($_SESSION['registration']['nationalnr'])) {
                                ?> value="
                      <?php echo $_SESSION['registration']['nationalnr']; ?>"
                      <?php
                            } ?> placeholder="
                      <?php echo lang('NationalNr'); ?>">
                    </fieldset>
                  </div>
                </div>
              <?php
                        } ?>
              </div>
               <?php if ($setting->mobile_platform == "ARTA") {
                            ?>
              <div class="row">
                <div class="col-sm-2">
                  <div class="form-group">
                    <fieldset>
                      <label for="Paymentmethod">
                        <?php echo lang('Paymentmethod'); ?></label>
                      <select class="form-control" id="default_paymentmethod">
                        <?php foreach (array('directdebit','banktransfer') as $pmt) {
                                ?>
                        <option value="<?php echo $pmt; ?>">
                          <?php echo ucfirst($pmt); ?>
                        </option>
                        <?php
                            } ?>
                      </select>
                    </fieldset>
                  </div>
                </div>
                <div class="col-sm-2">
                  <div class="form-group">
                    <fieldset>
                      <label for="exampleSelect1">E-Invoice</label>
                      <select class="form-control" id="invoice_email" name="invoice_email">
                        <option value="yes" selected="">Yes</option>
                        <option value="yes">No</option>
                      </select>

                    </fieldset>
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="form-group">
                    <fieldset>
                      <label for="IBAN">
                        <?php echo lang('IBAN'); ?></label>
                      <input name="iban" class="form-control" id="iban" type="text" value="" placeholder="<?php echo lang('IBAN'); ?>">
                    </fieldset>
                  </div>
                </div>
                <div class="col-sm-2">
                  <div class="form-group">
                    <fieldset>
                      <label for="">
                        <?php echo lang('BIC'); ?></label>
                      <input class="form-control" placeholder="ASPNLXX" type="text" name="bic" id="bic" <?php if (!empty($_SESSION['registration']['iban'])) {
                                ?> value="
                      <?php echo $_SESSION['registration']['iban']; ?>"
                      <?php
                            } ?>>
                    </fieldset>
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="form-group">
                    <fieldset>
                      <label for="">
                        <?php echo lang('Date of Birth'); ?></label>
                      <input class="form-control" placeholder="1997-01-01" type="text" name="date_birth" id="pickdate"
                        <?php if (!empty($_SESSION['registration']['date_birth'])) {
                                ?> value="
                      <?php echo $_SESSION['registration']['date_birth']; ?>"
                      <?php
                            } ?>>
                    </fieldset>
                  </div>
                </div>
              </div>

            <?php
                        } ?>
              <div class="row">
                <div class="col-sm-12">
                  <div class="form-group">
                    <fieldset class="form-group">
                      <div class="form-desc">
                        <?php echo lang('VAT exemption (BTW verlegd)'); ?>
                      </div>
                      <div class="form-check">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input" name="vat_exempt" id="vat_exempt" value="0"
                            checked="">
                          <?php echo lang('No, Apply tax on every invoices'); ?>
                        </label>
                      </div>
                      <div class="form-check">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input" name="vat_exempt" id="vat_exempt" value="1">
                          <?php echo lang('Yes, do not apply tax on invoices'); ?>
                        </label>
                      </div>
                    </fieldset>
                  </div>
                </div>
              </div>

              <?php
    } else {
        ?>

              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <fieldset>
                      <label class="control-label" for="firstname">
                        <?php echo lang('Firstname'); ?><span class="text-danger">*</span></label>
                      <input class="form-control" id="firstname" type="text" placeholder="<?php echo lang('Firstname'); ?>"
                        value="<?php echo $client->firstname; ?>" readonly>
                    </fieldset>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <fieldset>
                      <label class="control-label" for="lastname">
                        <?php echo lang('Lastname'); ?><span class="text-danger">*</span></label>
                      <input class="form-control" id="lastname" type="text" placeholder="<?php echo lang('Lastname'); ?>"
                        value="<?php echo $client->lastname; ?>" readonly>
                    </fieldset>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <fieldset>
                      <label class="control-label" for="Email">
                        <?php echo lang('Email'); ?><span class="text-danger">*</span></label>
                      <input class="form-control" id="firstname" type="text" placeholder="<?php echo lang('Email'); ?>"
                        value="<?php echo $client->email; ?>" readonly>
                    </fieldset>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <fieldset>
                      <label class="control-label" for="Phonenumber">
                        <?php echo lang('Phonenumber'); ?><span class="text-danger">*</span></label>
                      <input class="form-control" id="phonenumber" type="text" placeholder="<?php echo lang('Phonenumber'); ?>"
                        value="<?php echo $client->phonenumber; ?>" readonly>
                    </fieldset>
                  </div>
                </div>
              </div>


              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <fieldset>
                      <label class="control-label" for="Address1">
                        <?php echo lang('Address1'); ?><span class="text-danger">*</span></label>
                      <input class="form-control" id="address1" type="text" placeholder="<?php echo lang('Address1'); ?>"
                        value="<?php echo $client->address1; ?>" readonly>
                    </fieldset>
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="form-group">
                    <fieldset>
                      <label class="control-label" for="Postcode">
                        <?php echo lang('Postcode'); ?><span class="text-danger">*</span></label>
                      <input class="form-control" id="postcode" type="text" placeholder="<?php echo lang('Postcode'); ?>"
                        value="<?php echo $client->postcode; ?>" readonly>
                    </fieldset>
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="form-group">
                    <fieldset>
                      <label class="control-label" for="City">
                        <?php echo lang('City'); ?><span class="text-danger">*</span></label>
                      <input class="form-control" id="city" type="text" placeholder="<?php echo lang('City'); ?>"
                        value="<?php echo $client->city; ?>" readonly>
                    </fieldset>
                  </div>
                </div>
              </div>
              <input type="hidden" name="existingcustomer" value="1">
              <?php
    } ?>
                  <?php if ($setting->mobile_platform == "TEUM") {
        ?>

          <div class="row">
            <div class="col-sm-3">
              <div class="form-group">
                <fieldset>
                  <label for="exampleSelect1"><?php echo lang('Identification Type'); ?></label>
                  <select class="form-control" id="id_type" name="id_type">
                    <?php foreach (array('Passport', 'DrivingLicence', 'DNI', 'NIF', 'NIE', 'CIF', 'Visa') as $type) {
            ?>
                    <option value="<?php echo $type; ?>"><?php echo lang($type); ?></option>
                    <?php
        } ?>
                  </select>
                </fieldset>
              </div>
            </div>
            <div class="col-sm-3">
              <fieldset>
                <label class="control-label" for="Phonenumber"><?php echo lang('Identification Number'); ?><span class="text-danger"></span></label>
                <input name="idcard_number" class="form-control" id="idcard_number" type="text" placeholder="<?php echo lang('ID Number'); ?>"<?php if (!empty($_SESSION['registration']['idcard_number'])) {
            ?> value="<?php echo $_SESSION['registration']['idcard_number']; ?>" <?php
        } ?>>
              </fieldset>
            </div>
          </div>
            <?php
    } ?>
              <div class="row">
                <div class="col-md-4">
                  <a class="btn btn-block btn-md btn-dark" href="<?php echo base_url(); ?>reseller/dashboard/order_step2"><i
                      class="fa fa-arrow-left"></i> Back</a>
                </div>
                <div class="col-md-4">
                  <button type="button" class="btn btn-block btn-md btn-dark" id="finishorder"><i class="fa fa-arrow-right"></i>
                    Finalize</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card border-light">
          <div class="card-body">
            <h4 class="card-title">Order Summary - Step 3</h4>
            <table class="table table-striped table-hover">
              <tr>
                <td>Product Order</td>
                <td>
                  <?php echo getProductName($_SESSION['order']['pid']); ?>
                </td>
              </tr>
              <tr>
                <td>Recurring Amount</td>
                <td>
                  <?php echo $_SESSION['order']['recurring']; ?>
                </td>
              </tr>

              <tr>
                <td>Client ID</td>
                <td>
                  <?php echo $_SESSION['order']['clientid']; ?>
                </td>
              </tr>

              <tr>
                <td>Client Name</td>
                <td>
                  <?php echo $client->firstname.' '.$client->lastname; ?>
                </td>
              </tr>

              <tr>
                <td>Type Order</td>
                <td>
                  <?php if ($_SESSION['order']['msisdn_type'] == "new") {
        ?>New Number
                  <?php
    } else {
        ?>Porting Number
                  <?php
    } ?>
                </td>
              </tr>

              <?php if ($_SESSION['order']['msisdn_type'] != "new") {
        ?>
              <tr>
                <td>Number to Port</td>
                <td>
                  <?php echo $_SESSION['order']['msisdn']; ?>
                </td>
              </tr>
              <?php
    } ?>
            </table>
          </div>
        </div>
      </div>


    </div>
  </div>
</div>

<script>
  $(document).ready(function () {

   checkme = $('#firstname').val();
    console.log('firstname:'+checkme);
   if(!checkme){
    $.ajax({
url: window.location.protocol + '//' + window.location.host + '/reseller/client/gen_anonymous',
type: 'post',
dataType: "json",
data: {form_data:1},
success: function(data) {
Object.keys(data).forEach(function(k){
    console.log(k + ' - ' + data[k]);
     $('#'+k).val(data[k]);
});
  

}
});
   }
    
  
        var payment = $('#default_paymentmethod').find(":selected").val();
        if (payment == "directdebit") {
          $('#iban').prop('required', true);
        } else {
          $('#iban').prop('required', false);
        }
        $("#default_paymentmethod").change(function () {
          var payment = $('#default_paymentmethod').find(":selected").val();
          if (payment == "directdebit") {
            $('#iban').prop('required', true);
          } else {
            $('#iban').prop('required', false);
          }
        });
 
        $('#finishorder').click(function () {
          $("#finishorder").prop('disabled', true);
          var client = $('#orderfinal').serialize();
          console.log(client)
          $.ajax({
            url: window.location.protocol + '//' + window.location.host + '/reseller/dashboard/finish_order',
            type: 'post',
            dataType: 'json',
            data: client,
            success: function (data) {
              if(data.result){
                $("#finishorder").prop('disabled', true);
                  window.location.href = window.location.protocol + '//' + window.location.host + '/reseller/subscription/activate_mobile/'+data.serviceid;

              }else{
                $("#finishorder").prop('disabled', false);
                alert(data.message);
              }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                $("#finishorder").prop('disabled', false);
              alert("Error  accour while finalizing order");
            }
          });


        });
      });
</script>

<script>

  function GenAnn(){
$.ajax({
url: window.location.protocol + '//' + window.location.host + '/reseller/client/gen_anonymous',
type: 'post',
dataType: "json",
data: {form_data:1},
success: function(data) {
Object.keys(data).forEach(function(k){
    console.log(k + ' - ' + data[k]);
     $('#'+k).val(data[k]);
});
  

}
});

  }
</script>