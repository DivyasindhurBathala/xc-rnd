<div class="main-panel" style="padding-top:40px; padding-left:10px;padding-right:10px;padding-bottom:40px;">
<div class="content-wrapper">
<div class="row">
<div class="col-sm-4">
<div class="card border-light">
  <div class="card-body">
    <h4 class="card-title">Client Details</h4>
    <table class="table table-bordered table-striped">
    <tr>
    <td>Client Name</td>
    <td><?php echo format_name($client); ?></td>
    </tr>
    <tr>
    <td>Client ID</td>
    <td><?php echo $client->mvno_id; ?></td>
    </tr>
    <tr>
    <td>Address</td>
    <td><?php echo $client->address1; ?> <?php echo $client->postcode; ?><br /><?php echo $client->city; ?> <?php echo $client->country; ?></td>
    </tr>
    <tr>
    <td>Email</td>
    <td><?php echo $client->email; ?></td>
    </tr>
    <tr>
    <td>Phonenumber</td>
    <td><?php echo $client->phonenumber; ?></td>
    </tr>
    </table>
 
    <br />
     <a  href="<?php echo base_url(); ?>reseller/dashboard/editclient/<?php echo $client->id; ?>" class="btn btn-md btn-dark btn-block"><i class="fa fa-edit"></i> <?php echo lang('Edit Client'); ?></a>
        <?php if ($setting->whmcs_ticket) { ?>
      <button type="button" onclick="openticket()" class="btn btn-md btn-dark btn-block">Create Ticket</button>
        <?php } ?>
        <?php if ($setting->mage_invoicing) { ?>
            <?php if ($client->paymentmethod == "directdebit") {?>
         <button type="button" data-toggle="modal" data-target="#SepaAmend" class="btn btn-md btn-dark btn-block">Change Directdebit IBAN</button>
            <?php } ?>
        <?php } ?>
    </div>
    </div>
    </div>

    <div class="col-sm-8">
<div class="card border-light">
  <div class="card-body">
    <h4 class="card-title">Summary</h4>
    <ul class="nav nav-tabs">
  <li class="nav-item">
    <a class="nav-link active" data-toggle="tab" href="#subscriptions"><i class="fa fa-cubes"></i> Subscriptions</a>
  </li>
    <?php if ($setting->mage_invoicing) {?>
  <li class="nav-item">
    <a class="nav-link" data-toggle="tab" href="#invoices"><i class="fa fa-credit-card"></i> Invoices</a>
  </li>
   <li class="nav-item">
    <a class="nav-link" data-toggle="tab" href="#creditnotes"><i class="fa fa-credit-card"></i> Creditnotes</a>
  </li>
    <?php } ?>
    <?php if ($setting->whmcs_ticket) {?>
  <li class="nav-item">
    <a class="nav-link" data-toggle="tab" href="#tickets"><i class="fa fa-credit-card"></i> Ticket</a>
  </li>
    <?php } ?>
  <li class="nav-item">
    <a class="nav-link" data-toggle="tab" href="#notes"><i class="fa fa-sticky-note"></i> Notes</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" data-toggle="tab" href="#documents"><i class="fa fa-file"></i> Documents</a>
  </li>
</ul>
<div id="myTabContent" class="tab-content" style="padding-top:10px;">
  <div class="tab-pane fade show active" id="subscriptions">
   <table id="services" class="table table-bordered table-striped">
   <thead>
   <tr>
   <th><?php echo lang('Product Name'); ?></th>
   <th><?php echo lang('DateReg'); ?></th>
   <th><?php echo lang('Status'); ?></th>
   <th><?php echo lang('Recurring'); ?></th>
   <th><?php echo lang('Identifier'); ?></th>
   <th><?php echo lang('Cycle'); ?></th>
   </tr>
   </thead>
<tbody>
</tbody>
   </table>
  </div>
  <div class="tab-pane fade" id="invoices">
  <table id="dt_invoice" class="table table-bordered table-striped">
   <thead>
   <tr>
   <th><?php echo lang('InvoiceNbr'); ?></th>
   <th><?php echo lang('Date'); ?></th>
   <th><?php echo lang('Duedate'); ?></th>
   <th><?php echo lang('Status'); ?></th>
   <th><?php echo lang('Total'); ?></th>
   </tr>
   </thead>
<tbody>
</tbody>
   </table>
     </div>

       <div class="tab-pane fade" id="creditnotes">
  <table id="dt_cn" class="table table-bordered table-striped">
   <thead>
   <tr>
   <th><?php echo lang('Creditnote Number'); ?></th>
   <th><?php echo lang('Date'); ?></th>
   <th><?php echo lang('Duedate'); ?></th>
   <th><?php echo lang('Status'); ?></th>
   <th><?php echo lang('Total'); ?></th>
   </tr>
   </thead>
<tbody>
</tbody>
   </table>
     </div>
  <div class="tab-pane fade" id="tickets">
     
  <table id="dt_tickets" class="table table-bordered table-striped">
   <thead>
   <tr>
   <th><?php echo lang('Ticket'); ?></th>
   <th><?php echo lang('Date'); ?></th>
   <th><?php echo lang('Title'); ?></th>
   <th><?php echo lang('Dept'); ?></th>
   <th><?php echo lang('Status'); ?></th>
   </tr>
   </thead>
<tbody>
</tbody>
   </table>
    </div>
  <div class="tab-pane fade" id="notes">
   
  <table id="dt_notes" class="table table-bordered table-striped">
   <thead>
   <tr>
   <th><?php echo lang('Date'); ?></th>
   <th><?php echo lang('Description'); ?></th>
   <th><?php echo lang('Admin'); ?></th>
   <th></th>

   </tr>
   </thead>
<tbody>
</tbody>
   </table> </div>
  <div class="tab-pane fade" id="documents">
   
  <table id="dt_documents" class="table table-bordered table-striped">
   <thead>
   <tr>
   <th><?php echo lang('Description'); ?></th>
   <th><?php echo lang('File'); ?></th>
  
   </tr>
   </thead>
<tbody>
</tbody>
   </table>
    </div>
</div>

    </div>
    </div>
    </div>


    </div>
    </div>
    </div>
    <form>
    <input type="hidden" id="mageboid" value="<?php echo $client->mageboid; ?>">
    <input type="hidden" id="userid" value="<?php echo $client->id; ?>">
    </form>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js" type="text/javascript"></script>
     <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css">
    <script type="text/javascript">
    $(document).ready(function() {
    $.getJSON(window.location.protocol + '//' + window.location.host + '/admin/complete/get_lang', function(data) {
        var userid = $('#userid').val();
        var mageboid = $('#mageboid').val();
            $('#services').DataTable({
            "autoWidth": false,
            "processing": true,
            "orderCellsTop": true,
            "ordering": true,
            "serverSide": true,
            "ajax": window.location.protocol + '//' + window.location.host + '/admin/table/getclient_services/' + userid,
            "aaSorting": [
                [0, 'desc']
            ],
            "columnDefs": [
                { "searchable": false, "targets": 1 }
            ],
            "language": {
                "url": window.location.protocol + '//' + window.location.host + "/assets/clear/js/datatables/lang/" + data.result + ".json"
            },
            "fnRowCallback": function(nRow, aData, iDisplayIndex) {
                if (aData[2] != "Terminated" || aData[2] != "Cancelled") {
                    $('td:eq(4)', nRow).html('<a href="' + window.location.protocol + '//' + window.location.host + '/reseller/subscription/detail/' + aData[6] + '">' + aData[4] + '</a>');
                    $('td:eq(0)', nRow).html('<a href="' + window.location.protocol + '//' + window.location.host + '/reseller/subscription/detail/' + aData[6] + '">' + aData[0] + '</a>');
                }
                $('td:eq(3)', nRow).html('€' + aData[3]);
                $('td:eq(5)', nRow).html('Maandelijks');
                return nRow;
            },
        });
        $('#dt_invoice').DataTable({
            "autoWidth": false,
            "ajax": {
                "url": window.location.protocol + '//' + window.location.host + '/admin/table/getclient_invoices/' + mageboid,
                "dataSrc": ""
            },
            "aaSorting": [
                [0, 'desc']
            ],
            "columns": [
                { "data": "iInvoiceNbr" },
                { "data": "dInvoiceDate" },
                { "data": "dInvoiceDueDate" },
                { "data": "iInvoiceStatus" },
                { "data": "mInvoiceAmount" }
            ],
            "language": {
                "url": window.location.protocol + '//' + window.location.host + "/assets/clear/js/datatables/lang/" + data.result + ".json"
            },
            "fnRowCallback": function(nRow, aData, iDisplayIndex) {
                $('td:eq(4)', nRow).html('€' + aData.mInvoiceAmount);
                $('td:eq(0)', nRow).html('<a href="' + window.location.protocol + '//' + window.location.host + '/reseller/invoice/detail/' + aData.iInvoiceNbr + '/' + aData.iAddressNbr + '">' + aData.iInvoiceNbr + '</a>');
                return nRow;
            },
        });
         $('#dt_cn').DataTable({
            "autoWidth": false,
            "ajax": {
                "url": window.location.protocol + '//' + window.location.host + '/admin/table/getclient_creditnotes/' + mageboid,
                "dataSrc": ""
            },
            "aaSorting": [
                [0, 'desc']
            ],
            "columns": [
                { "data": "iInvoiceNbr" },
                { "data": "dInvoiceDate" },
                { "data": "dInvoiceDueDate" },
                { "data": "iInvoiceStatus" },
                { "data": "mInvoiceAmount" }
            ],
            "language": {
                "url": window.location.protocol + '//' + window.location.host + "/assets/clear/js/datatables/lang/" + data.result + ".json"
            },
            "fnRowCallback": function(nRow, aData, iDisplayIndex) {
                $('td:eq(4)', nRow).html('€' + aData.mInvoiceAmount);
                $('td:eq(0)', nRow).html('<a href="' + window.location.protocol + '//' + window.location.host + '/reseller/invoice/cndetail/' + aData.iInvoiceNbr + '/' + aData.iAddressNbr + '">' + aData.iInvoiceNbr + '</a>');
                return nRow;
            },
        });

        $('#dt_tickets').DataTable({
            "autoWidth": false,
            "processing": true,
            "orderCellsTop": true,
            "ordering": true,
            "serverSide": true,
            "ajax": window.location.protocol + '//' + window.location.host + '/admin/table/getclient_tickets/' + userid,
            "aaSorting": [
                [0, 'desc']
            ],
            "columnDefs": [
                { "searchable": false, "targets": 1 }
            ],
            "language": {
                "url": window.location.protocol + '//' + window.location.host + "/assets/clear/js/datatables/lang/" + data.result + ".json"
            },
            "fnRowCallback": function(nRow, aData, iDisplayIndex) {
                //$('td:eq(5)', nRow).html('â‚¬'+ aData[5]);
                $('td:eq(0)', nRow).html('<a href="' + window.location.protocol + '//' + window.location.host + '/reseller/helpdesk/detail/' + aData[6] + '"><i class="fa fa-edit"></i> ' + aData[0] + '</a>');
                $('td:eq(2)', nRow).html('<a href="' + window.location.protocol + '//' + window.location.host + '/reseller/helpdesk/detail/' + aData[6] + '"><i class="fa fa-edit"></i> ' + aData[2] + '</a>');
                $('td:eq(4)', nRow).html(aData[5]);
                return nRow;
            },
        });
        
        $('#dt_notes').DataTable({
            "autoWidth": false,
            "processing": true,
            "orderCellsTop": true,
            "ordering": true,
            "serverSide": true,
            "ajax": window.location.protocol + '//' + window.location.host + '/admin/table/getnotes_client/' + userid,
            "aaSorting": [
                [0, 'desc']
            ],
            "columnDefs": [
                { "searchable": false, "targets": 1 }
            ],
            "language": {
                "url": window.location.protocol + '//' + window.location.host + "/assets/clear/js/datatables/lang/" + data.result + ".json"
            },
            "fnRowCallback": function(nRow, aData, iDisplayIndex) {
                $('td:eq(3)', nRow).html('<button class="btn btn-md btn-warning" onclick="edit_note(\'' + aData[3] + '\')"><i class="fa fa-edit"></i></button> <button class="btn btn-md btn-danger" onclick="delete_note(\'' + aData[3] + '\')"><i class="fa fa-trash"></i></button>');
                return nRow;
            },
        });
        $('#dt_documents').DataTable({
            "autoWidth": false,
            "processing": true,
            "orderCellsTop": true,
            "ordering": true,
            "serverSide": true,
            "ajax": window.location.protocol + '//' + window.location.host + '/admin/table/getdocuments_clients/' + userid,
            "aaSorting": [
                [0, 'desc']
            ],
            "columnDefs": [
                { "searchable": false, "targets": 1 }
            ],
            "language": {
                "url": window.location.protocol + '//' + window.location.host + "/assets/clear/js/datatables/lang/" + data.result + ".json"
            },
            "fnRowCallback": function(nRow, aData, iDisplayIndex) {

                $('td:eq(5)', nRow).html('€' + aData[5]);
                $('td:eq(2)', nRow).html('<a href="' + window.location.protocol + '//' + window.location.host + '/reseller/subscription/download_welcome_letter/' + aData[1] + '"><i class="fa fa-download"></i> Download</a>');
                return nRow;
            },
        });



        $.ajax({
            url: window.location.protocol + '//' + window.location.host + '/admin/client/getLastNotes/' + userid,
            type: 'post',
            dataType: 'json',
            data: {
                userid: userid
            },
            success: function(data) {
              $('#consumption').html('â‚¬'+data.usage);
                if (data.notes.length > 0) {
                    data.notes.forEach(function(b) {
                        $('#noteitems').append('<li>' + b.note + '</li>');
                    });
                }
                if (data.tickets.length > 0) {
                    data.tickets.forEach(function(b) {
                        $('#ticketitems').append('<li><a href="' + window.location.protocol + '//' + window.location.host + '/reseller/helpdesk/detail/' + b.id + '">' + b.subject + '</a></li>');
                    });

                }
                $('#loadingController').modal('hide');
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                //alert("Error  accour while sending your email");
                
            }
        });
      
    });  
});
    </script>
    <script>
      function openticket(){
        $('#ticketNew').modal('toggle');

      }
    </script>

    <!-- Modal -->
<div id="ticketNew" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Submit Ticket for <?php echo $client->mvno_id; ?></h4>
      </div>
      <div class="modal-body">
        <pre>
            <?php //print_r($_SESSION); ?>
      </pre>
      <form method="post" action="<?php echo base_url(); ?>reseller/helpdesk/newticket" enctype="multipart/form-data">
          <input type="hidden" name="admin" value="<?php echo $_SESSION['reseller']['contact_name']; ?>">
          <input type="hidden" name="userid" value="<?php echo $client->id; ?>">
          <div class="modal-body" id="modalonly">
            <div class="form-group">
              <label for="subject">Subject</label>
              <input name="subject" class="form-control" placeholder="Title" type="text" required>
            </div>
            <div class="row">
              <div class="form-group col-md-4">
                <label for="category">Category</label>
                <select class="form-control" id="category" name="categoryid">
                    <?php foreach (getHelpdeskCategory($this->session->cid) as $row) {?>
                  <option value="<?php echo $row->id; ?>"><?php echo $row->name; ?></option>
                    <?php }?>
                </select>
              </div>
              <div class="form-group col-md-4">
                <label for="department">Department</label>
                <select class="form-control" id="department" name="deptid">
                    <?php foreach (getDepartments($this->session->cid) as $row) {?>
                  <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
                    <?php }?>
                </select>
              </div>
                  <div class="form-group col-md-4">
              <label for="priority">Change Priority</label>
              <select class="form-control" id="priority" name="priority">
                <?php foreach (array('4', '3', '2', '1') as $prio) {?>
                <option value="<?php echo $prio; ?>"<?php if ($prio == 3) {
                    ?> selected<?php
                               }?>><?php echo $prio; ?></option>
                <?php }?>
              </select>
            </div>
            </div>
            <div class="form-group">
              <label for="exampleSelect1">Assign to</label>
              <select class="form-control" id="exampleSelect1" name="assigne">
                <?php foreach (getStaf($this->session->cid) as $staf) {?>
                <option value="<?php echo $staf['id']; ?>"<?php if ($staf['id'] == $this->session->id) {
                    ?> selected<?php
                               }?>><?php echo $staf['firstname'] . ' ' . $staf['lastname']; ?></option>
                <?php }?>
              </select>
            </div>
            <div class="form-group">
              <label for="file">Attachments</label>
              <input name="attachments[]" type="file" class="form-control-file" id="attachments" aria-describedby="fileHelp" multiple="">
              <small id="fileHelp" class="form-text text-muted">Allowed type docx, doc, xls, xlsx, pdf, gif, png, jpg.</small>
            </div>
            <div class="form-group">
              <label for="message">Message</label>
              <textarea name="message" class="form-control" id="message" rows="6"></textarea>
            </div>
          </div>
          <div class="modal-footer">
           
            <button class="btn btn-dark" type="submit" id="bos"> Save changes</button>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-dark" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


 <div class="modal fade" id="SepaAmend" tabindex="-1" role="dialog" aria-labelledby="SepaAmend" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <form method="post" action="<?php echo base_url(); ?>reseller/client/sepa_amend">
            <?php //print_r($_SESSION['reseller']); ?>
          <input type="hidden" name="admin" value="<?php echo $_SESSION['reseller']['contact_name']; ?>">
          <input type="hidden" name="userid" value="<?php echo $client->id; ?>">
          <input type="hidden" name="companyid" value="<?php echo $client->companyid; ?>">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel"> <?php echo lang('Sepa Amendment'); ?></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
          <div class="form-group">
              <label for="exampleTextarea"> <?php echo lang('TYPE SEPA'); ?>:</label>
             <select name="type" class="form-control">
             <option value="">SELECT TYPE..</option>
                <?php if ($financial->SEPA_MANDATE) {?>
               <option value="AMEND">AMEND</option>
                <?php }?>
               <option value="NEW">NEW</option>
              </select>
            </div>
          <div class="form-group">
              <label for="exampleTextarea"> <?php echo lang('IBAN'); ?>:</label>
              <input class="form-control" name="iban" value="" required>
            </div>
             <div class="form-group">
              <label for="exampleTextarea"> <?php echo lang('Mandateid'); ?>:(Optional)</label>
              <input class="form-control" name="mandateid" value="">
            </div>

              <div class="form-group">
              <label for="exampleTextarea"> <?php echo lang('BIC'); ?>(Optional, Works Guarantee on BE,NL,FR,DE,UK,LU):</label>
              <input class="form-control" name="bic" value="">
            </div>


          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-dark" data-dismiss="modal"> <?php echo lang('Close'); ?></button>
            <button type="submit" class="btn btn-dark"> <?php echo lang('Save changes'); ?></button>
          </div>
        </form>
      </div>
    </div>
  </div>