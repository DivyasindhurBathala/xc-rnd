<div class="main-panel" style="padding-bottom:60px;padding-top:30px; padding-left:10px;padding-right:10px;padding-bottom:40px;">
  <div class="content-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <div class="card border-light">
          <div class="card-body">
            <h4 class="card-title"><?php echo lang('Edit Customer Information'); ?></h4>
            <form id="orderfinal">
             
              <div class="row">
                
                <div class="col-sm-6">
                  <div class="form-group">
                    <fieldset>
                      <label class="control-label" for="vat">
                        <?php echo lang('Btw'); ?></label>
                      <input name="vat" class="form-control" id="vat" type="text" placeholder="<?php echo lang('Vat Number'); ?>"
                        <?php if (!empty($client['vat'])) {
                            ?> value="<?php echo $client['vat']; ?>"<?php
                        }?>>
                    </fieldset>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <fieldset>
                      <label class="control-label" for="companyname">
                        <?php echo lang('Companyname'); ?></label>
                      <input name="companyname" class="form-control" id="companyname" type="text" placeholder="<?php echo lang('Companyname'); ?>"
                        <?php if (!empty($client['companyname'])) {
                            ?> value="<?php echo $client['companyname']; ?>"<?php
                        }?>>
                    </fieldset>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-4">
                  <div class="form-group">
                    <label class="control-label" for="gender">
                        <?php echo lang('Gender'); ?><span class="text-danger">*</span></label>
                    <select class="form-control" id="gender" name="gender">
                      <option value="male">Male</option>
                      <option value="female">Female</option>
                    </select>
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="form-group">
                    <label for="">
                        <?php echo lang('Initial'); ?></label><input class="form-control" value="<?php echo $client['initial']; ?>"
                      name="initial" type="text" id="Initial">
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="form-group">
                    <label for="">
                        <?php echo lang('Salutation'); ?></label><input class="form-control" placeholder="<?php echo $client['salutation']; ?>"
                      name="salutation" type="text" id="salutation">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <fieldset>
                      <label class="control-label" for="firstname">
                        <?php echo lang('Firstname'); ?><span class="text-danger">*</span></label>
                      <input name="firstname" class="form-control" id="firstname" type="text" placeholder="<?php echo lang('Firstname'); ?>"
                        <?php if (!empty($client['firstname'])) {
                            ?> value="<?php echo $client['firstname']; ?>"<?php
                        }?> required>
                    </fieldset>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <fieldset>
                      <label class="control-label" for="lastname">
                        <?php echo lang('Lastname'); ?><span class="text-danger">*</span></label>
                      <input name="lastname" class="form-control" id="lastname" type="text" placeholder="<?php echo lang('Lastname'); ?>" <?php if (!empty($client['lastname'])) {
                            ?> value="<?php echo $client['lastname']; ?>"<?php
                                                                                                         }?> required>
                    </fieldset>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-4">
                  <div class="form-group">
                    <fieldset>
                      <label class="control-label" for="address1">
                        <?php echo lang('Address'); ?><span class="text-danger">*</span></label>
                      <input name="address1" class="form-control" id="address1" type="text" placeholder="<?php echo lang('Address'); ?>"
                        <?php if (!empty($client['address1'])) {
                            ?> value="<?php echo $client['address1']; ?>" <?php
                        }?> required>
                    </fieldset>
                  </div>
                </div>
                <div class="col-sm-2">
                  <div class="form-group">
                    <fieldset>
                      <label class="control-label" for="Postcode">
                        <?php echo lang('Postcode'); ?><span class="text-danger">*</span></label>
                      <input name="postcode" class="form-control" id="postcode" type="text" placeholder="NNNN XX" <?php
                        if (!empty($client['postcode'])) {
                            ?> value="<?php echo $client['postcode']; ?>"<?php
                        }?> required>
                    </fieldset>
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="form-group">
                    <fieldset>
                      <label class="control-label" for="City">
                        <?php echo lang('City'); ?><span class="text-danger">*</span></label>
                      <input name="city" class="form-control" id="city" type="text" <?php if (!empty($client)) {
                            ?> value="<?php echo $client['city']; ?>"<?php
                                                                                    }?> placeholder="<?php echo lang('City'); ?>" required>
                    </fieldset>
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="form-group">
                    <fieldset>
                      <label for="exampleSelect1">
                        <?php echo lang('Country'); ?><span class="text-danger">*</span></label>
                      <select class="form-control" id="country" name="country">
                        <?php foreach (getCountries() as $key => $country) {?>
                        <option value="<?php echo $key; ?>" <?php if ($key=="NL") {
                            ?> selected<?php
                                       }?>><?php echo $country; ?></option>
                        <?php }?>
                      </select>
                    </fieldset>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-3">
                  <div class="form-group">
                    <fieldset>
                      <label for="exampleSelect1">
                        <?php echo lang('Languages'); ?></label>
                      <select class="form-control" id="language" name="language">
                        <?php foreach (getLanguages() as $key => $lang) {?>
                        <option value="<?php echo $key; ?>" <?php if ($key=="dutch") {
                            ?> selected<?php
                                       }?>><?php echo lang($lang); ?></option>
                        <?php }?>
                      </select>
                    </fieldset>
                  </div>
                </div>
                <div class="col-sm-3">
                  <fieldset>
                    <label class="control-label" for="Phonenumber">
                        <?php echo lang('Contact Number'); ?><span class="text-danger">*</span></label>
                    <input name="phonenumber" class="form-control" id="phonenumber" type="number" placeholder="<?php echo lang('Contact Number'); ?>"
                        <?php if (!empty($client['phonenumber'])) {
                            ?> value="<?php echo $client['phonenumber']; ?>"<?php
                        }?> required>
                  </fieldset>
                </div>
                <div class="col-sm-3">
                  <div class="form-group">
                    <fieldset>
                      <label class="control-label" for="Email">
                        <?php echo lang('Email'); ?><span class="text-danger">*</span></label>
                      <input name="email" class="form-control" id="email" type="email" placeholder="<?php echo lang('Email address'); ?>"<?php if (!empty($client['email'])) {
                            ?> value="<?php echo $client['email']; ?>"<?php
                                                                                                    }?> required>
                    </fieldset>
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="form-group">
                    <fieldset>
                      <label class="control-label" for="NationalNr">
                        <?php echo lang('NationalNr'); ?></label>
                      <input name="nationalnr" class="form-control" id="NationalNr" type="number" <?php if (!empty($client['nationalnr'])) {
                            ?> value="<?php echo $client['nationalnr']; ?>"<?php
                                                                                                  }?> placeholder="<?php echo lang('NationalNr'); ?>">
                    </fieldset>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-4">
                  <div class="form-group">
                    <fieldset>
                      <label for="Paymentmethod">
                        <?php echo lang('Paymentmethod'); ?></label>
                      <select class="form-control" id="default_paymentmethod">
                        <?php foreach (array('directdebit','banktransfer') as $pmt) {
                            ?><option value="<?php echo $pmt; ?>"><?php echo ucfirst($pmt); ?></option>
                        <?php }?>
                      </select>
                    </fieldset>
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="form-group">
                    <fieldset>
                      <label for="exampleSelect1">E-Invoice</label>
                      <select class="form-control" id="invoice_email" name="invoice_email">
                        <option value="yes" selected="">Yes</option>
                        <option value="yes">No</option>
                      </select>

                    </fieldset>
                  </div>
                </div>
                
                <div class="col-sm-4">
                  <div class="form-group">
                    <fieldset>
                      <label for="">
                        <?php echo lang('Date of Birth'); ?></label>
                      <input class="form-control" placeholder="1997-01-01" type="text" name="date_birth" id="pickdate"
                        <?php if (!empty($client['date_birth'])) {
                            ?> value="<?php echo $client['date_birth']; ?>"<?php
                        }?>>
                    </fieldset>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <div class="form-group">
                    <fieldset class="form-group">
                      <div class="form-desc">
                        <?php echo lang('VAT exemption (BTW verlegd)'); ?>
                      </div>
                      <div class="form-check">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input" name="vat_exempt" id="vat_exempt" value="0" checked="">
                            <?php echo lang('No, Apply tax on every invoices'); ?>
                        </label>
                      </div>
                      <div class="form-check">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input" name="vat_exempt" id="vat_exempt" value="1">
                            <?php echo lang('Yes, do not apply tax on invoices'); ?>
                        </label>
                      </div>
                    </fieldset>
                  </div>
                </div>
              </div>

             
              <div class="row">
                
                <div class="col-md-4 ml-auto mr-auto">
                  <button type="button" class="btn btn-block btn-lg btn-dark" id="finishorder">
                    <i class="fa fa-save"></i> <?php echo lang('Save'); ?></button>
                </div>
              </div>
              <input type="hidden" name="id" value="<?php echo $client['id']; ?>" id="clientid">
            </form>
          </div>
        </div>
      </div>
      


    </div>
  </div>
</div>

<script>
  $(document).ready(function () {
        var payment = $('#default_paymentmethod').find(":selected").val();
        if (payment == "directdebit") {
          $('#iban').prop('required', true);
        } else {
          $('#iban').prop('required', false);
        }
       /* $("#default_paymentmethod").change(function () {
          var payment = $('#default_paymentmethod').find(":selected").val();
          if (payment == "directdebit") {
            $('#iban').prop('required', true);
          } else {
            $('#iban').prop('required', false);
          }
        });
        */

        $('#finishorder').click(function () {
           var id = $('#clientid').val();
          var client = $('#orderfinal').serialize();
          if ($('#firstname').val()  === '') {
                alert('Firstname is required');
                $('#firstname').focus();
                return false;
             }else  if ($('#lastname').val()  === '') {
                alert('Lastname is required');
                $('#lastname').focus();
                return false;
             }else  if ($('#email').val()  === '') {
                
                alert('Email is required');
                $('#email').focus();
                return false;
             }else  if ($('#address1').val()  === '') {
                alert('Address1 is required');
                $('#address1').focus();
                return false;
             }else  if ($('#postcode').val()  === '') {
                alert('Postcode is required');
                $('#postcode').focus();
                return false;
             }else  if ($('#city').val()  === '') {
                alert('City is required');
                $('#city').focus();
                return false;
             }else  if ($('#phonenumber').val()  === '') {
                alert('Contact Number is required');
                $('#phonenumber').focus();
                return false;
             }
            var payment = $( "#default_paymentmethod option:selected" ).val();
            if(payment == "directdebit"){
                if ($('#iban').val()  === '') {
                alert('IBAN Number is required when paymentmethod is directdebit');
                $('#iban').focus();
                return false;
             }

            }
        $("#finishorder").prop('disabled', true);
          $.ajax({
            url: window.location.protocol + '//' + window.location.host + '/reseller/client/edit',
            type: 'post',
            dataType: 'json',
            data: client,
            success: function (data) {
                console.log(data);
              if(data.result){
                $("#finishorder").prop('disabled', true);
                  window.location.href = window.location.protocol + '//' + window.location.host + '/reseller/dashboard/viewclient/'+id;
              }else{
                $("#finishorder").prop('disabled', false);
                alert(data.message);
              }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                
              alert("Error  while adding customer");
            }
          });


        });
      });
</script>