</div>
<nav class="navbar fixed-bottom navbar-expand-sm navbar-dark bg-dark">
	<a class="navbar-brand" href="#"><?php echo $setting->companyname; ?></a>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
		aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="navbarCollapse">
		<ul class="navbar-nav mr-auto">

			<li class="nav-item">
				<a class="nav-link disabled" href="#">ArtiPanel 1.5c</a>
			</li>

		</ul>
	</div>
</nav>
<script src="<?php echo base_url(); ?>assets/clear/reseller/bootstrap.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/clear/css/progress.css">
<?php if ($_SESSION['reseller']['reseller_type'] == "Prepaid") {
    ?>
<div class="modal" id="reloadx">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title"><?php echo lang('Reload My Account'); ?></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
			<form id="reload_data">
				<p><?php echo lang('Please enter amount you wish to add'); ?></p>
				<div class="row">
					<div class="col-md-6">
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">

									<label class="control-label" for="amount">
										<?php echo lang('Amount'); ?><span class="text-danger">*</span></label>
									<input name="amount" class="form-control" id="amountreload" placeholder="0" value=""
										required>
									<small class="text-danger"><?php echo lang('Minimum'); ?>:
										<?php echo $setting->currency; ?><?php echo $_SESSION['reseller']['min_topup']; ?>,
										<?php echo lang('Maximum'); ?>:
										<?php echo $setting->currency; ?><?php echo $_SESSION['reseller']['max_topup']; ?>.</small>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-12">
								<fieldset class="form-group">
									<legend><?php echo lang('Payment Method'); ?></legend>
									<div class="form-check">
										<label class="form-check-label">
											<input type="radio" class="form-check-input pm" name="paymentmethod" id="cop"
												value="creditcard">
											<?php echo lang('Credit Card'); ?>
										</label>
									</div>
									<div class="form-check">
										<label class="form-check-label">
											<input type="radio" class="form-check-input pm" name="paymentmethod" id="banktransfer"
												value="banktransfer">
											<?php echo lang('Bank Transfer'); ?>
										</label>
									</div>
									<div class="form-check disabled">
										<label class="form-check-label">
											<input type="radio" class="form-check-input pm" name="paymentmethod" id="cash"
												value="cash">
											<?php echo lang('Cash'); ?>
										</label>
									</div>
								</fieldset>

								<fieldset id="cc_detail">
								<div class="form-group">
										<label class="form-check-label">
										<?php echo lang('Card Holder Name'); ?></label>
										<input class="form-control" type="text" class="form-control" name="cc_name" id="cc_name">
										</div>

										<?php if(empty($_SESSION['reseller']['agent_dni'])){ ?>
											<div class="form-group">
											<label class="control-label" for="Bank"><?php echo lang('Identification Type'); ?><span class="text-danger">*</span></label>
<select name="cc_dni_type" class="form-control">
<option value="CC"><?php echo lang('Certificate Citizenship'); ?></option>
<option value="CE"><?php echo lang('Immigrant Certificate.'); ?></option>
<option value="NIT"><?php echo lang('Company TAX ID Number'); ?></option>
<option value="TI"><?php echo lang('Identity Card.'); ?></option>
<option value="PP"><?php echo lang('Passport'); ?></option>
<option value="IDC"><?php echo lang('Customer Unique ID'); ?></option>
<option value="CEL"><?php echo lang('Cell Number'); ?></option>
<option value="RC"><?php echo lang('Civil Registration.'); ?></option>
<option value="DE"><?php echo lang('Foreign ID'); ?></option>
</select>
</div>
										<div class="form-group">
										<label class="form-check-label">
										<?php echo lang('Identification Number'); ?></label>
										<input class="form-control" type="text" class="form-control" name="cc_dni" id="cc_dni">
										</div>
										<?php } ?>

										<div class="form-group">
										<label class="form-check-label">
										<?php echo lang('Credit Card Type'); ?></label>
										<select name="cc_type" id="cc_type" class="form-control">
										<?php foreach(array('VISA' => 'Visa','MASTERCARD' => 'Mastercard','AMEX' => 'American Express','DINERS' => 'Dinners Club') as $key => $cc){ ?>
											<option value="<?php echo $key; ?>"><?php echo $cc; ?></option>
										<?php } ?>
										</select>
										</div>

										<div class="form-group">
										<label class="form-check-label">
										<?php echo lang('Credit Card Number'); ?></label>
										<input class="form-control" type="text" class="form-control" name="cc_number" id="cc_number">
										</div>
										<div class="row">
										<div class="form-group col-sm-6">
										<label class="form-check-label">
										<?php echo lang('Expiry'); ?> YYYY/MM</label>
										<input class="form-control" type="text" class="form-control" name="cc_expire" id="cc_expire">
										</div>
										<div class="form-group col-sm-6">
										<label class="form-check-label">
										<?php echo lang('CCV'); ?></label>
										<input class="form-control" type="number" class="form-control" name="cc_ccv" id="cc_ccv">
										</div>
										</div>
</fieldset>
								<fieldset class="form-group" id="paymentdetail">
								</fieldset>

							</div>
						</div>
					</div>
					<div class="col-md-6">
						<img src="<?php echo base_url(); ?>assets/img/PayU_Corporate_Logo.png">
											<br />
											<img height="230" src="<?php echo base_url(); ?>assets/img/SecureSSL-800.jpg">
					</div>
				</div>
											</form>
			</div>


			<div class="modal-footer">
				<button type="button" id="btn_payu" class="btn btn-danger btn_payu"
					onclick="reload();">
					<?php echo lang('Submit'); ?></button>
					<img height="100" id="img_loader" style="display:none;" src="<?php echo base_url(); ?>assets/img/loader1.gif">

			</div>
		</div>
	</div>
</div>
<script>
function reload() {
	$('#btn_payu').hide();
	$('#img_loader').show();
	var str = $("#reload_data").serializeArray();
	console.log(str);

	var type = $("input[name='paymentmethod']:checked").val();
	var agentid = '<?php echo $_SESSION['reseller']['id ']; ?>';
	var amount = $('#amountreload').val();
	if(amount <= 0){
		alert('Please anter Amount');
		$('#img_loader').hide();
		$('#btn_payu').show();
		$('#amountreload').focus();
		return;
	}
	$.ajax({
		url: '<?php echo base_url(); ?>reseller/pay/reload/' + agentid,
		type: 'post',
		dataType: 'json',
		success: function(i) {
			$('#img_loader').hide();
			$('#btn_payu').show();
			if (i.redirect) {
				window.location.replace(i.url);
			}else{

				alert(i.message);
			}

		},
		data: {
			agentid: agentid,
			amount: amount,
			type: type,
			description: 'Reload Reseller Credit',
			name: 'reload ' + agentid,
			qty: '1',
			info: str
		},
	});

}
$(document).ready(function() {
	$('#cc_detail').hide();
  $('#cc_expire').mask('0000/00');
	//pm = $("input[name='paymentmethod']:checked").val();

	$('#cc_number').keyup(function(){
		var number = $('#cc_number').val();
		$.ajax({
				url: '<?php echo base_url(); ?>reseller/pay/payu_getcc/',
				type: 'post',
				dataType: 'json',
				data:{
					cc_number: number
				},
				success: function(i) {
					$('.cc_img').html(i.result);

				}
			});


	})
	$('.pm').change(function() {
		$('#paymentdetail').html('<img height="50" src="<?php echo base_url(); ?>assets/img/loader1.gif">');
		pm = $("input[name='paymentmethod']:checked").val();
		if (pm == 'cash') {
			$('#cc_detail').hide();
			$.ajax({
				url: '<?php echo base_url(); ?>reseller/pay/payu_getcash/',
				type: 'get',
				dataType: 'json',
				success: function(i) {
					$('#paymentdetail').html(i.html);
				}
			});

		} else if (pm == 'banktransfer') {
			$('#cc_detail').hide();
			$.ajax({
				url: '<?php echo base_url(); ?>reseller/pay/payu_getbanks/',
				type: 'get',
				dataType: 'json',
				success: function(i) {
					$('#paymentdetail').html(i.html);

				}
			});

		} else if (pm == "creditcard") {
			$('#cc_detail').show();
			$('#paymentdetail').html('');
		}

	});
});
</script>
<?php
} ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>
</body>

</html>