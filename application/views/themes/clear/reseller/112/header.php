<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title><?php echo lang('Reseller'); ?>: <?php echo $setting->companyname; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/clear/reseller/cl.css" media="screen">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/clear/reseller/custom.min.css?version=2.5">
 <!--   https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.css -->
 <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
  <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
  <link rel="stylesheet" href="https://mijnmobiel.delta.nl/assets/clear/jquery-ui/jquery-ui.min.css">
    <link rel="stylesheet" href="https://mijnmobiel.delta.nl/assets/clear/jquery-ui/jquery-ui.theme.min.css">
  </head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
  <a class="navbar-brand" href="#"><img src="<?php echo $setting->logo_site; ?>" height="40"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarColor01">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="<?php echo base_url(); ?>reseller"><i class="fa fa-home"></i> <strong><?php echo lang('Home'); ?></strong> </a>
      </li>
        <?php if (reseller_allow_perm('addclient', $_SESSION['reseller']['id'])) { ?>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
         <i class="fa fa-users"></i> <strong><?php echo lang('Clients'); ?></strong>
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="<?php echo base_url();?>reseller/dashboard/client"><i class="fa fa-tasks"></i> <?php echo lang('List Clients'); ?></a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="<?php echo base_url();?>reseller/client/add"><i class="fa fa-user-plus"></i> <?php echo lang('Add Client'); ?></a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="<?php echo base_url();?>reseller/client/export_clients"><i class="fa fa-tasks"></i> <?php echo lang('Export Clients'); ?></a>

        </div>
      </li>
        <?php } ?>
    <?php if (reseller_allow_perm('service', $_SESSION['reseller']['id'])) { ?>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
         <i class="fa fa-cube"></i> <strong><?php echo lang('Subscriptions'); ?></strong>
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="<?php echo base_url();?>reseller/dashboard"><i class="fa fa-cubes"></i> <?php echo lang('List Subscriptions'); ?></a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="<?php echo base_url();?>reseller/subscription/export_subscriptions"><i class="fa fa-tasks"></i> <?php echo lang('Export Subscription'); ?></a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="<?php echo base_url();?>reseller/dashboard/order"><i class="fa fa-shopping-cart"></i> <?php echo lang('New Order'); ?></a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="<?php echo base_url();?>reseller/dashboard/renewal_report"><i class="fa fa-list"></i> <?php echo lang('Renewal Report'); ?></a>
        </div>
      </li>
    <?php } ?>
        <?php if ($setting->whmcs_ticket) { ?>
            <?php if (reseller_allow_perm('helpdesk', $_SESSION['reseller']['id'])) { ?>
      <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url();?>reseller/helpdesk"><i class="fa fa-cog"></i> <strong><?php echo lang('Helpdesk'); ?></strong></a>
      </li>
            <?php } ?>
        <?php } ?>
      <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url();?>reseller/dashboard/setting"><i class="fa fa-cog"></i> <strong><?php echo lang('My Account'); ?> <?php if ($_SESSION['reseller']['reseller_type'] == "Prepaid") { ?>
          <span class="text-success">
          (<?php echo lang('Balance'); ?>: <?php echo $setting->currency; ?> <?php echo getResellerBalance($_SESSION['reseller']['id']); ?>)
        </span>
                                  <?php } ?></strong></a>
      </li>
        <?php if (!empty($this->session->id)) { ?>
        <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url();?>admin/agent/detail/<?php echo $this->session->reseller['id']; ?>"><i class="fa fa-user-md"></i> <strong><?php echo lang('Back to Admin Panel'); ?></strong></a>
      </li>
        <?php } ?>

    </ul>


      <a class="btn btn-secondary my-2 my-sm-0" href="<?php echo base_url(); ?>reseller/auth/logout"><i class="fa fa-sign-out"></i> <?php echo lang('Logout'); ?></a>

  </div>
</nav>
<div style="padding-bottom: 40px;padding-top: 30px;">

<?php if (!empty($this->session->flashdata('success'))) { ?>
    <div class="alert alert-dismissible alert-success">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  <h4 class="alert-heading">Info</h4>
    <?php echo $this->session->flashdata('success'); ?>
</div>

<?php } ?>
<?php if (!empty($this->session->flashdata('error'))) { ?>
    <div class="alert alert-dismissible alert-danger">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  <h4 class="alert-heading">Error</h4>
    <?php echo $this->session->flashdata('error'); ?>
</div>
<?php } ?>

