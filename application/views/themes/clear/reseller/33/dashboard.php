<div class="main-panel">
    <div class="content-wrapper">
        <div class="row">
<div class="col-sm-12">

<div class="card border-light">
  <div class="card-body">
    <h4 class="card-title">List Orders</h4>
     <table id="orders" class="table table-striped table-bordered">
        <thead>
          <tr>
          <th><?php echo lang('#Client ID'); ?></th>
                <th><?php echo lang('Package'); ?></th>
                <th><?php echo lang('Contact'); ?></th>
                <th><?php echo lang('Status'); ?></th>
                <th><?php echo lang('Amount'); ?></th>
                <th><?php echo lang('Identifier'); ?></th>
              </tr>
        </thead>
        <tbody>
</tbody>
</table>
  </div>
</div>
</div>
</div>
    </div>
</div>

 <script>
  $(document).ready(function()
{
$.getJSON(window.location.protocol + '//' + window.location.host + '/reseller/table/get_lang', function(data) {
var x = new Date();
var y = x.getFullYear().toString();
var m = (x.getMonth() + 1).toString();
var d = x.getDate().toString();
(d.length == 1) && (d = '0' + d);
(m.length == 1) && (m = '0' + m);
var date = y +'-'+ m +'-'+ d;
 $('#orders').DataTable({
"autoWidth": false,
"processing": true,
"orderCellsTop": true,
"ordering": true,
"serverSide": true,
"colReorder": true,
"ajax": window.location.protocol + '//' + window.location.host + '/reseller/table/getservices',
"aaSorting": [[1, 'desc']],
"language": {
"url": window.location.protocol + '//' + window.location.host +"/assets/clear/js/datatables/lang/" + data.result + ".json"
},
"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
if(aData[8] == date ){
$('td:eq(0)', nRow).html(aData[0] + '<img src="'+window.location.protocol + '//' + window.location.host + '/assets/img/new-sticker.png" height="16">');
}
$('td:eq(0)', nRow).html('<a  href="'+window.location.protocol + '//' + window.location.host + '/reseller/subscription/detail/' + aData[0] + '">'+aData[10]+'</a>');
$('td:eq(1)', nRow).html('<a  href="'+window.location.protocol + '//' + window.location.host + '/reseller/subscription/detail/' + aData[0] + '">'+aData[1]+'</a>');
$('td:eq(2)', nRow).html('<a  href="'+window.location.protocol + '//' + window.location.host + '/reseller/dashboard/viewclient/' + aData[9] + '">'+aData[2]+'</a>');
$('td:eq(3)', nRow).html('<a href="'+window.location.protocol + '//' + window.location.host + '/reseller/subscription/detail/' + aData[0] + '">'+aData[3]+'</a>');

$('td:eq(4)', nRow).html('€' + aData[4]);
return nRow;
},
});

});
});

</script>
