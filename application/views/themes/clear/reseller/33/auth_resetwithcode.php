<!DOCTYPE html>
<html>
<head>
	<title>Reseller Reset Password</title>
   <!--Made with love by Mutiullah Samim -->

	<!--Bootsrap 4 CDN-->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <!--Fontawesome CDN-->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

	<!--Custom styles-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/clear/reseller/style.css">
</head>
<body>
<div class="container">
	<div class="d-flex justify-content-center h-100">
		<div class="cardi">
			<div class="card-header">
				<h3>Change Password:</h3>
				<div class="d-flex justify-content-end social_icon">
				<!--	<span><i class="fab fa-facebook-square"></i></span>
					<span><i class="fab fa-google-plus-square"></i></span>
					<span><i class="fab fa-twitter-square"></i></span>
					-->
				</div>
			</div>
			<div class="card-body">
				<form action="<?php echo base_url(); ?>reseller/auth/resetwithcode" method="post">
				<input type="hidden" name="code" value="<?php echo $_SESSION['code']; ?>">
					<div class="input-group form-group">
						<div class="input-group-prepend">
							<span class="input-group-text"><i class="fas fa-key"></i></span>
						</div>
						<input type="password" class="form-control" name="password1" required>
					</div>
					<div class="input-group form-group">
						<div class="input-group-prepend">
							<span class="input-group-text"><i class="fas fa-key"></i></span>
						</div>
						<input type="password" class="form-control" name="password2" required>
					</div>
					<div class="form-group">
						<input type="submit" value="Login" class="btn float-right login_btn">
					</div>
				</form>
			</div>
			<div class="card-footer">
				<div class="d-flex justify-content-center links">
				<?php if (!empty($this->session->flashdata('error'))) {?>
    <div class="row">
      <div class="col-lg-12">
        <div class="alert alert-danger" role="alert">
          <strong class="text-center"><?php echo $this->session->flashdata('error'); ?></strong>
        </div>
      </div>
    </div>
    <?php }?>
    <?php if (!empty($this->session->flashdata('success'))) {?>
    <div class="row">
      <div class="col-lg-12">
        <div class="alert alert-warning" role="alert">
          <strong class="text-center"><?php echo $this->session->flashdata('success'); ?></strong>
        </div>
      </div>
    </div>
    <?php }?>
				</div>
				<div class="d-flex justify-content-center">
					<a href="<?php echo base_url(); ?>reseller/auth">I know my password?</a>
				</div>
			</div>
		</div>
	</div>
</div>
</body>