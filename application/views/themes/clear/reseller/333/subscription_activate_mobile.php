<div class="main-panel" style="padding-bottom:60px;padding-top:30px; padding-left:10px;padding-right:10px;">
  <div class="content-wrapper">
    <div class="row">
    	 <div class="col-sm-4">
    	 </div>
      <div class="col-sm-4">
        <div class="card border-light">
          <div class="card-body">
            <h4 class="card-title">Assign Simcard (<?php echo ucfirst($service->details->msisdn_type); ?>) Number</h4>
               <div class="row">
                
                <div class="col-sm-12">

                	<?php if($service->details->msisdn_type == "porting"){ ?>

                	  <div class="form-group">
                  <label for="">
                    <?php echo lang('Mobile Number'); ?></label>
                  <input class="form-control" autocomplete="new-username" type="text" name="msisdn" id="msisdn" value="<?php echo $service->details->msisdn; ?>">
                	</div>
                
               

                <div class="form-group">
                    <fieldset>
                      <label class="control-label" for="simcard">
                        <?php echo lang('Simcard Number'); ?></label>
                      <input name="vat" class="form-control  ui-autocomplete-input" id="simcard" autocomplete="new-username" type="text" placeholder="89XXXXXXXXXXXXXXXXXXXX" value="">
                    </fieldset>
                  </div>
                  <div class="row">
                  <div class="col-sm-6">
                  <div class="form-group has-success">
                  	<fieldset>
								<label class="form-control-label" for="inputSuccess1">
									<?php echo lang('DONOR Provider'); ?></label>
								<select class="form-control" id="donor_provider" name="donor_provider">
									<?php foreach (getDonors($setting->country_base) as $row) { ?>
									<option value="<?php echo $row->operator_code; ?>" <?php if ($service->details->donor_provider == $row->operator_code) {?> selected<?php }?>>
										<?php echo $row->operator_name; ?>
									</option>
									<?php }?>
								</select>
								<div class="valid-feedback">
									<?php echo lang('Choose the correct Provider'); ?>
								</div>
							</fieldset>
							</div>
						</div>
						  <div class="col-sm-6">
					 <div class="form-group has-success">
                  	<fieldset>
								<label class="form-control-label" for="inputSuccess1">
									<?php echo lang('DONOR Type'); ?></label>
								<select class="form-control" id="donor_type" name="donor_type">
									<?php foreach (array('0' => 'Prepaid','1' => 'Postpaid') as $key => $row) { ?>
									<option value="<?php echo $row; ?>"<?php if ($service->details->donor_type == $key) {?> selected<?php }?>>
										<?php echo $row; ?>
									</option>
									<?php }?>
								</select>
								<div class="valid-feedback">
									<?php echo lang('Choose the correct Provider'); ?>
								</div>
							</fieldset>
							</div>
						</div>
					</div>
					<div class="row">
				<div class="form-group col-sm-6">
                    <fieldset>
                      <label class="control-label" for="account_number">
                        <?php echo lang('Account Number'); ?></label>
                      <input name="vat" class="form-control" id="donor_accountnumber" autocomplete="new-username" type="text" value="<?php echo $service->details->donor_accountnumber; ?>">
                    </fieldset>
                  </div>



                  <div class="form-group col-sm-6">
                    <fieldset>
                      <label class="control-label" for="simcard">
                        <?php echo lang('Date Wish'); ?></label>
                      <input name="vat" class="form-control" id="pickdate4" autocomplete="new-username" type="text" value="<?php echo $service->details->date_wish; ?>">
                    </fieldset>
                  </div>
              </div>
                	<?php }else{ ?>
                  <div class="form-group">
                    <fieldset>
                      <label class="control-label" for="simcard">
                        <?php echo lang('Simcard Number'); ?></label>
                      <input name="vat" class="form-control  ui-autocomplete-input" id="simcard" autocomplete="new-username" type="text" placeholder="89XXXXXXXXXXXXXXXXXXXX" value="">
                    </fieldset>
                  </div>
                  <?php } ?>

                  <button class="pull-right btn btn-md btn-primary" id="continue" disabled>Activate</button> 
                </div>
            </div>

        </div>
    </div>
</div>
 <div class="col-sm-3">
    	 </div>
</div>
</div>
</div>

<script>
$('#continue').click(function(){
	$('#continue').prop('disabled', true); // display the selected tex
  $.ajax({
    url: window.location.protocol + '//' + window.location.host + '/reseller/subscription/activate_mobile/<?php echo $this->uri->segment(4); ?>',
    type: 'post',
    dataType: "json",
    data: {
     serviceid: '<?php echo $this->uri->segment(4); ?>',
     <?php if($service->details->msisdn_type == "porting"){ ?>
     donor_provider:  $('#donor_provider').val(),
     donor_msisdn: $('#msisdn').val(),
     donor_accountnumber: $('#donor_accountnumber').val(),
     donor_type: $('#donor_type').val(),
     date_wish: $('#date_wish').val(),
     <?php } ?>
     msisdn_sim:  $('#simcard').val()
    },
    success: function( data ) {
    	
    if(data.result){

    	window.location.replace(window.location.protocol + '//' + window.location.host + '/reseller/subscription/detail/<?php echo $this->uri->segment(4); ?>');

    }else{

    	alert(data.message);
    }

    
     

    }
   });

});
$( "#donor_type" ).change(function() {
var sel = $( "#donor_type option:selected" ).val();
if(sel == "0"){
$('#donor_accountnumber').prop('required', false);
}else{
$('#donor_accountnumber').prop('required', true);
}

});
$( "#simcard" ).autocomplete({
  source: function( request, response ) {
   $('#pelanggan').hide('slow');
   $.ajax({
    url: window.location.protocol + '//' + window.location.host + '/reseller/table/search_simcard',
    type: 'post',
    dataType: "json",
    data: {
     keyword: request.term
    },
    success: function( data ) {
    $('#continue').prop('disabled', true); // display the selected tex
     response( data );
    }
   });
  },
  select: function (event, ui) {
   $('#simcard').val(ui.item.label); // display the selected tex
   $('#continue').prop('disabled', false); // display the selected tex
  
   return false;
  }
 });

	</script>
	<script>
$( document ).ready(function() {
 var sel = $( "#donor_type option:selected" ).val();
if(sel == "0"){
$('#donor_accountnumber').prop('required', false);
}else{
$('#donor_accountnumber').prop('required', true);
}
});
</script>