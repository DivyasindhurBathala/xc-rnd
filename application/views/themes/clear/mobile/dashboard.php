  
  <!-- Begin page content -->
<div class="container"   style="padding-bottom:30px;">
<div class="row">
<?php if ($bundles) { ?>
    <div class="col-xs-12">
  <div class="panel panel-primary">
  <div class="panel-body">
     <h2 class="text-center" style="color:  #3361aa;"><strong><?php echo $activemobiles[0]['number']; ?></strong></h2>
</div>
</div>
</div>


<div class="col-xs-6">
  <div class="panel panel-warning">
  <div class="panel-heading">
    <h3 class="panel-title text-center"><?php echo lang('Total This Month Usage'); ?></h3>
  </div>
  <div class="panel-body">      
                  <h6 class="text-center"> 
                  &euro;
          <?php if (!empty($consumption)) {?>
                <?php echo str_replace('.', ',', number_format($consumption, 2)); ?>
            <?php } else {?>
              <?php echo '0.00'; ?>
          <?php }?>
              </h6>
                
  </div>
</div>
</div>


<div class="col-xs-6">
  <div class="panel panel-info">
  <div class="panel-heading">
    <h3 class="panel-title text-center"><?php echo lang('Amount unPaid Invoices'); ?></h3>
  </div>
  <div class="panel-body">      
                  <h6 class="text-center"> 
                <?php if ($stats->invoice_amountunpaid > 0) {
                    ?><a class="text-light" href="<?php echo base_url(); ?>client/invoice"><?php
}?>&euro;<?php echo str_replace('.', ',', number_format($stats->invoice_amountunpaid, 2)); ?><?php if ($stats->invoice_amountunpaid > 0) {
    ?></a><?php
                }?>
              </h6>
                
  </div>
</div>
</div>




    <?php foreach ($bundles as $index => $row) {?>
<div class="col-xs-12">
  <div class="panel panel-primary">
  <div class="panel-heading">
    <h3 class="panel-title text-center"><?php echo $row->szBundle; ?></h3>
  </div>
  <div class="panel-body">
        <?php if (isset($row->UsedValue)) {?>
                     <h5 class="text-center"><?php echo str_replace('.', ',', $row->UsedValue) . ' / ' . $row->AssignedValue; ?> </h5>
        <?php } else {?>
                  <?php echo lang('Not In used yet'); ?>
        <?php }?>
                <?php if (strpos($row->szBundle, '30 dagen') || strpos($row->szBundle, '30 days')) {?>
                  <h6 class="text-center"><?php echo lang('ValidUntil'); ?> <?php echo $row->ValidUntil; ?></h6>
                <?php }?>
  </div>
</div>
</div>
    <?php } ?>
<?php } ?>
</div>
</div>

