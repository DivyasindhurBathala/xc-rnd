<?php
defined('BASEPATH') or exit('No direct script access allowed');
use Genkgo\Camt\Config;
use Genkgo\Camt\Reader;

class SepaController extends CI_Controller
{
    protected $data = [];
    public function __construct()
    {
        parent::__construct();
        if (!empty($this->session->language)) {
            $this->config->set_item('language', $this->session->language);
        } else {
            $this->config->set_item('language', 'dutch');
        }
        $this->lang->load('admin');
        $this->companyid       = get_companyidby_url(base_url());
        $this->data['setting'] = globofix($this->companyid);
        $this->load->library('magebo', array(
        'companyid' => $this->companyid
        ));
    }
}
class Sepa extends SepaController
{
    public function __construct()
    {
        parent::__construct();
        /*
        $classes = get_class_methods($this);
        //if($this->session->email == "simson.parlindungan@united-telecom.be"){
        foreach ($classes as $class) {
            log_message('error', $class);
            if ($class != 'setProperty' && $class != 'get_instance' && $class != '__construct') {
                $this->db->insert('a_admin_permissions', array('folder' => 'admin', 'controller' => 'sepa', 'method' => $class));
            }
        }
        // }
        */

        if (!empty($this->session->profiler)) {
            if ($this->session->profiler) {
                $this->output->enable_profiler(true);
                error_reporting(E_ALL);
                ini_set('display_errors', 1);
            }
        }
        $this->companyid = get_companyidby_url(base_url());
        $this->load->model('Admin_model');
    }
    public function process_proforma()
    {
        $this->load->model('Proforma_model');
        $this->Proforma_model->UpdateInvoice($_POST['invoicenumber'], array(
        'status' => 'Paid',
        'datepaid' => date('Y-m-d H:i:s')
        ));
        // mail('mail@simson.one', 'Please create Invoice manually for ' . $_POST['invoicenumber'], print_r($_POST, true));
        $invoice = $this->Proforma_model->getInvoiceidbyInvoicenum(trim($_POST['invoicenumber']));
        $client  = $this->Admin_model->getClient($invoice['userid']);
        $this->db->insert('a_payments', array(
        'date' => $_POST['date_transaction'] . ' ' . date('H:i:s'),
        'invoiceid' => trim($_POST['invoicenumber']),
        'userid' => $invoice['userid'],
        'companyid' => $invoice['companyid'],
        'paymentmethod' => 'BT',
        'transid' => $_POST['itemid'] . ': ' . $_POST['executor'],
        'amount' => str_replace(',', '.', trim($_POST['amount'])),
        'raw' => json_encode($_POST)
        ));

        logAdmin(array(
            'companyid' => $this->companyid,
            'userid' => $client->id,
            'serviceid' => 0,
            'user' => $this->session->firstname . ' ' . $this->session->lastname,
            'ip' => $_SERVER['REMOTE_ADDR'],
            'description' => lang('Process the Proforma').' #'.$_POST['invoicenumber']
            ));


        if ($this->db->insert_id() > 0) {
            $this->db->query("update a_sepa_items set status='Completed' where id=?", array(
            $_POST['itemid']
            ));
            echo json_encode(array(
            'result' => 'success'
            ));
        } else {
            echo json_encode(array(
            'result' => 'error'
            ));
        }
    }
    public function assign_Refund_to_Payment()
    {
        $this->load->library('magebo', array(
        'companyid' => $this->companyid
        ));

        $res = $this->magebo->refundAction($_POST['iPaymentNbr'], $_POST['dPaymentDate']);
        $payment =  $this->magebo->getPayment($_POST['iPaymentNbr']);
        $client = getClientByMageboid($payment->iAddressNbr);
        logAdmin(array(
            'companyid' => $this->companyid,
            'userid' => $client->id,
            'serviceid' => 0,
            'user' => $this->session->firstname . ' ' . $this->session->lastname,
            'ip' => '127.0.0.1',
            'description' => lang('Refund Invoice').': '.$payment->iAddressNbr
            ));


        echo json_encode($res);
    }
    public function assignPaymentInvoice()
    {
        $this->load->library('magebo', array(
        'companyid' => $this->companyid
        ));
        $payment = $this->magebo->getPaymentDetail($_POST['PLiPaymentNbr']);
        $invoice = $this->magebo->getInvoice($_POST['PLiInvoiceNbr']);
        if ($payment) {
            $p              = explode(' ', $payment->dPaymentDate);
            $invoice_amount = $invoice->mInvoiceAmount;
            $payment_amount = $_POST['PLAmountPaid'];
            if ($payment_amount >= $invoice_amount) {
                $amount = $invoice_amount;
            } else {
                $amount = $payment_amount;
            }
            $postfields['iAddressNbr'] = $_POST['PLiAddressNbr'];
            $postfields['iInvoiceNbr'] = $_POST['PLiInvoiceNbr'];
            $postfields['iPaymentNbr'] = $_POST['PLiPaymentNbr'];
            $ch                        = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://delta.united-telecom.nl:8443/localapi.php?step=3');
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_TIMEOUT, 30);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postfields));
            $response = curl_exec($ch);
            if (curl_error($ch)) {
                die('Unable to connect: ' . curl_errno($ch) . ' - ' . curl_error($ch));
            }
            curl_close($ch);
            $s = json_decode($response);
            if ($s->result) {
                $client = getClientDetailidbyMagebo($_POST['PLiAddressNbr']);
                $this->db->insert('a_payments', array(
                'date' => $p[0],
                'invoiceid' => trim($_POST['PLiInvoiceNbr']),
                'userid' => $client->id,
                'companyid' => $client->companyid,
                'paymentmethod' => 'BT',
                'transid' => $payment->cPaymentFormDescription,
                'amount' => $amount,
                'adminid' => $this->session->id,
                'raw' => 'Executed by: ' . $this->session->firstname . ' ' . $this->session->lastname
                ));

                logAdmin(array(
                    'companyid' => $this->companyid,
                    'userid' => $client->id,
                    'serviceid' => 0,
                    'user' => $this->session->firstname . ' ' . $this->session->lastname,
                    'ip' => $_SERVER['REMOTE_ADDR'],
                    'description' => lang('Assign Payment To Invoice').' #'.$postfields['iInvoiceNbr']
                    ));
            }
            if ($this->db->insert_id() > 0) {
                $this->session->set_flashdata('success', lang('Item has been processed'));
                echo json_encode(array(
                'result' => true
                ));
            } else {
                $this->session->set_flashdata('success', lang('Item has been processed, but payments log was not added'));
                echo json_encode(array(
                'result' => true
                ));
            }


            if (!$this->magebo->hasUnpaidInvoice($postfields['iAddressNbr'])) {
                $client = getClientDetailidbyMagebo($postfields['PLiAddressNbr']);
                $this->load->library('artilium', array(
                'companyid' => $this->companyid
                ));
                $services = $this->Admin_model->getServices($client->id);
                if ($services) {
                    foreach ($services as $serv) {
                        if ($serv->status == "Suspended") {
                            if ($serv->type == "mobile") {
                                $ii   = $this->Admin_model->getPackageState($serv->id);
                                $pack = (array) json_decode($ii);
                                $this->artilium->UnBlockOriginating(trim($serv->sn), $pack, '1');
                                $this->Admin_model->update_services_data('mobile', $serv->id, array(
                                'bar' => 0
                                ));
                            }

                            $this->Admin_model->update_services($serv->id, array(
                            'status' => 'Active'
                            ));
                            logAdmin(array(
                            'companyid' => $this->companyid,
                            'userid' => $client->id,
                            'serviceid' => $serv->id,
                            'user' => $this->session->firstname . ' ' . $this->session->lastname,
                            'ip' => '127.0.0.1',
                            'description' => lang('Number has been unblocked, because invoice').' ' . $postfields['iInvoiceNbr'] . ' '.lang('has been paid function assignPaymentInvoice')
                            ));
                        }
                    }
                }
                $this->Admin_model->ChangeClientDunnigProfile($client->id, 'No');
            }
        } else {
            echo json_encode(array(
            'result' => false,
            'message' => lang('Payment Number not found')
            ));
        }
    }
    public function getUnPaidInvoice()
    {
        $this->load->library('magebo', array(
        'companyid' => $this->companyid
        ));
        $invoices = $this->magebo->getUnPaidInvoice($_POST['iAddressNbr']);
        if ($invoices) {
            foreach ($invoices as $inv) {
                $inv['mPaymentAmount'] = number_format($inv['mPaymentAmount'], 2);
                $res[]                 = $inv;
            }
        } else {
            $res = array();
        }
        echo json_encode($res);
    }
    public function set_complete()
    {
        $this->db->where('id', $this->uri->segment(4));
        $this->db->update('a_sepa_items', array(
        'status' => 'Completed'
        ));
        $this->session->set_flashdata('success', lang('Item') .' ' . $this->uri->segment(4) .' '. lang('has been set completed'));
        redirect('admin/sepa');
    }
    public function addPayment()
    {
        $assignpayment = false;
        $this->load->library('magebo', array(
        'companyid' => $this->session->cid
        ));
        $this->load->model('Admin_model');
        $postfields                 = $_POST;
        $toto                       = $_POST['mPaymentAmount'];
        $postfields['dPaymentDate'] = date("m/d/Y", strtotime($postfields['dPaymentDate']));
        $invoice                    = $this->magebo->getInvoice($_POST['iInvoiceNbr']);
        if ($_POST['mPaymentAmount'] > $_POST['mInvoiceAmount']) {
            $p                            = $_POST['mPaymentAmount'];
            $assign                       = 1;
            $postfields['mPaymentAmount'] = $_POST['mPaymentAmount'];
            $postfields['mInvoiceAmount'] = $invoice->mInvoiceAmount;
        } else {
            $assign                       = 2;
            $p                            = $_POST['mPaymentAmount'];
            $postfields['mPaymentAmount'] = $_POST['mPaymentAmount'];
        }
        if ($invoice->mInvoiceAmount > $_POST['mPaymentAmount']) {
            $p = $_POST['mPaymentAmount'];
        }
        $ch        = curl_init();
        $paymentID = $this->Admin_model->hasPaymentNbr($_POST['itemid']);
        if (!$paymentID) {
            if ($_POST['step'] == "1") {
                curl_setopt($ch, CURLOPT_URL, 'https://delta.united-telecom.nl:8443/localapi.php?step=1&assign=' . $assign);
            } elseif ($_POST["step"] == "2") {
                curl_setopt($ch, CURLOPT_URL, 'https://delta.united-telecom.nl:8443/localapi.php?step=2');
            }
        } else {
            $assignpayment             = true;
            $postfields['iPaymentNbr'] = $paymentID;
            curl_setopt($ch, CURLOPT_URL, 'https://delta.united-telecom.nl:8443/localapi.php?step=3');
        }
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postfields));
        $response = curl_exec($ch);
        if (curl_error($ch)) {
            die('Unable to connect: ' . curl_errno($ch) . ' - ' . curl_error($ch));
        }
        curl_close($ch);
        $s = json_decode($response);
        $x = $this->Admin_model->getSepaItem($_POST['itemid']);
        if ($x->iPaymentNbr <= 0) {
            if ($s->result) {
                $this->Admin_model->SepaItemsComplete($_POST['itemid']);
                $client = getClientDetailidbyMagebo($_POST['iAddressNbr']);
                $this->db->insert('a_payments', array(
                'date' => $x->date_transaction,
                'invoiceid' => trim($_POST['iInvoiceNbr']),
                'userid' => $client->id,
                'companyid' => $client->companyid,
                'paymentmethod' => 'BT',
                'transid' => $_POST['itemid'],
                'amount' => $p,
                'adminid' => $this->session->id,
                'raw' => 'Executed by: ' . $this->session->firstname . ' ' . $this->session->lastname
                ));

              
                if ($client->dunning_profile == "4") {
                }

        

                send_growl(array(
                'message' => 'Invoice '.trim($_POST['iInvoiceNbr']).' has been paid, executed by '.$this->session->firstname . ' ' . $this->session->lastname,
                'companyid' => $client->companyid));
            }
            if ($toto > $invoice->mInvoiceAmount) {
                $amount = $_POST['mPaymentAmount'] - $invoice->mInvoiceAmount;
                $this->Admin_model->UpdateSepaItems($_POST['itemid'], 'Completed', $amount, $x->message, $invoice->mInvoiceAmount, trim($_POST['iInvoiceNbr']), $s->id);
            } elseif ($toto < $invoice->mInvoiceAmount) {
                $amount = 0;
                $this->Admin_model->UpdateSepaItems($_POST['itemid'], 'Completed', $amount, $x->message, $invoice->mInvoiceAmount, trim($_POST['iInvoiceNbr']));
            } else {
                $amount = 0;
                $this->Admin_model->UpdateSepaItems($_POST['itemid'], 'Completed', $amount, $x->message, $invoice->mInvoiceAmount, trim($_POST['iInvoiceNbr']));
            }
        } else {
            if ($s->result) {
                $this->Admin_model->SepaItemsComplete($_POST['itemid']);
                $client = getClientDetailidbyMagebo($_POST['iAddressNbr']);
               
                if ($client->dunning_profile == "4") {
                }

                $this->db->insert('a_payments', array(
                'date' => $x->date_transaction,
                'invoiceid' => trim($_POST['iInvoiceNbr']),
                'userid' => $client->id,
                'companyid' => $client->companyid,
                'paymentmethod' => 'BT',
                'transid' => $_POST['itemid'],
                'amount' => $p,
                'adminid' => $this->session->id,
                'raw' => 'Executed by: ' . $this->session->firstname . ' ' . $this->session->lastname
                ));

                logAdmin(array(
                    'companyid' => $this->companyid,
                    'userid' => $client->id,
                    'serviceid' => 0,
                    'user' => $this->session->firstname . ' ' . $this->session->lastname,
                    'ip' => $_SERVER['REMOTE_ADDR'],
                    'description' => lang('Applay Payment to Invoice').' #'.$_POST['iInvoiceNbr']
                    ));

                    

                send_growl(array(
                'message' => 'Invoice '.trim($_POST['iInvoiceNbr']).' has been paid, executed by '.$this->session->firstname . ' ' . $this->session->lastname,
                'companyid' => $client->companyid));
            }
            if ($x->balance > $invoice->mInvoiceAmount) {
                $amount = $x->balance - $invoice->mInvoiceAmount;
                $this->Admin_model->UpdateSepaItems($_POST['itemid'], 'Completed', $amount, $x->message, $invoice->mInvoiceAmount, trim($_POST['iInvoiceNbr']));
            } elseif ($x->balance < $invoice->mInvoiceAmount) {
                $amount = 0;
                $this->Admin_model->UpdateSepaItems($_POST['itemid'], 'Completed', $amount, $x->message, $invoice->mInvoiceAmount, trim($_POST['iInvoiceNbr']));
            } else {
                $amount = 0;
                $this->Admin_model->UpdateSepaItems($_POST['itemid'], 'Completed', $amount, $x->message, $invoice->mInvoiceAmount, trim($_POST['iInvoiceNbr']));
            }
        }

        
        $client = getClientDetailidbyMagebo($_POST['iAddressNbr']);
        if (!$this->magebo->hasUnpaidInvoice($_POST['iAddressNbr'])) {
            $this->load->library('artilium', array(
            'companyid' => $this->session->cid
            ));
            $services = $this->Admin_model->getServices($client->id);
            if ($services) {
                foreach ($services as $serv) {
                    if ($serv->status == "Suspended") {
                        if ($serv->type == "mobile") {
                            $ii   = $this->Admin_model->getPackageState($serv->id);
                            $pack = (array) json_decode($ii);
                            $this->artilium->UnBlockOriginating(trim($serv->sn), $pack, '1');
                            $this->Admin_model->update_services_data('mobile', $serv->id, array(
                            'bar' => 0
                            ));
                        }

                        $this->Admin_model->update_services($serv->id, array(
                        'status' => 'Active'
                        ));
                        logAdmin(array(
                        'companyid' => $this->session->cid,
                        'userid' => $serv->userid,
                        'serviceid' => $serv->id,
                        'user' => $this->session->firstname . ' ' . $this->session->lastname,
                        'ip' => '127.0.0.1',
                        'description' => lang('Number has been unblocked, because invoice').' ' . $_POST['iInvoiceNbr'] . ' '.lang('has been paid Function AddPayment')
                        ));
                    }
                }
            }
            $this->Admin_model->ChangeClientDunnigProfile($client->id, 'No');
        }
        echo $response;
    }
    public function addPaymentOnly()
    {
        $assignpayment = false;
        $this->load->library('magebo', array(
        'companyid' => $this->session->cid
        ));
        $this->load->model('Admin_model');
        $postfields                 = $_POST;
        $toto                       = $_POST['mPaymentAmount'];
        $postfields['dPaymentDate'] = date("m/d/Y", strtotime($postfields['dPaymentDate']));
        $invoice                    = $this->magebo->getInvoice($_POST['iInvoiceNbr']);
        $p                          = $_POST['mPaymentAmount'];
        $ch                         = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://delta.united-telecom.nl:8443/localapi.php?step=99');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postfields));
        $response = curl_exec($ch);
        if (curl_error($ch)) {
            die('Unable to connect: ' . curl_errno($ch) . ' - ' . curl_error($ch));
        }
        curl_close($ch);
        $s = json_decode($response);
        $x = $this->Admin_model->getSepaItem($_POST['itemid']);
        if ($s->result) {
            $this->Admin_model->SepaItemsComplete($_POST['itemid']);
            $client = getClientDetailidbyMagebo($_POST['iAddressNbr']);
            $this->db->insert('a_payments', array(
            'date' => $x->date_transaction,
            'invoiceid' => '9999999999',
            'userid' => $client->id,
            'companyid' => $client->companyid,
            'paymentmethod' => 'BT',
            'transid' => $_POST['itemid'],
            'amount' => $p,
            'adminid' => $this->session->id,
            'raw' => 'Executed by: ' . $this->session->firstname . ' ' . $this->session->lastname
            ));
            $amount = $invoice->mInvoiceAmount;
            $this->Admin_model->UpdateSepaItems($_POST['itemid'], 'Completed', $amount, $x->message, $invoice->mInvoiceAmount, 9999999999);
        }
        echo $response;
    }
    public function getfile()
    {
        $q = $this->db->query("select * from a_sepa_files where id=?", array(
        $this->uri->segment(4)
        ));
        if ($this->companyid == 54) {
            $row = 1;
            if (($handle = fopen('/home/mvno/documents/' . $this->companyid . '/csv/' . $q->row()->filename, "r")) !== false) {
                echo '<table border="1">';
                while (($data = fgetcsv($handle, 1000, ",")) !== false) {
                    $num = count($data);
                    if ($row == 1) {
                        echo '<thead><tr>';
                    } else {
                        echo '<tr>';
                    }
                    for ($c = 0; $c < $num; $c++) {
                        if (empty($data[$c])) {
                            $value = "&nbsp;";
                        } else {
                            $value = $data[$c];
                        }
                        if ($row == 1) {
                            echo '<th>' . $value . '</th>';
                        } else {
                            echo '<td>' . $value . '</td>';
                        }
                    }
                    if ($row == 1) {
                        echo '</tr></thead><tbody>';
                    } else {
                        echo '</tr>';
                    }
                    $row++;
                }
                echo '</tbody></table>';
                fclose($handle);
            }
        } else {
            echo "\nProcessing " . $file . "\n";
            $reader     = new Reader(Config::getDefault());
            $message    = $reader->readFile('/home/mvno/documents/' . $this->companyid . '/camt/done/' . $q->row()->filename);
            $statements = $message->getRecords();
            echo '
   <table border="1">
    <thead>
        <tr>
            <th>Fileid</th>
            <th>Date</th>
            <th>Companyid</th>
            <th>amount</th>
            <th>iban</th>
            <th>end2endid</th>
            <th>mandateid</th>
            <th>remit</th>
            <th>status</th>
            <th>batchid</th>
            <th>C/D</th>
            <th>Issuer</th>
            <th>Txcode</th>
            <th>Retcode</th>
            <th>Extra</th>
        </tr>
    </thead>';
            foreach ($statements as $statement) {
                foreach ($statement->getEntries() as $entry) {
                    $dddd              = $entry->getBookingDate();
                    $date              = $dddd->format('Y-m-d');
                    $BatchPaymentId    = null;
                    $mandateid         = "";
                    $endtoend          = "";
                    $name              = "";
                    $transactioncode   = $entry->getBankTransactionCode()->getProprietary()->getCode();
                    $transactionissuer = $entry->getBankTransactionCode()->getProprietary()->getIssuer();
                    $amount            = $entry->getAmount();
                    foreach ($entry->getTransactionDetails() as $r) {
                        $retinfo = $r->getReturnInformation();
                        if ($retinfo != null) {
                            $ret_code  = $retinfo->getCode();
                            $ret_extra = $retinfo->getAdditionalInformation();
                        } else {
                            $ret_code  = null;
                            $ret_extra = null;
                        }
                        $reference = $r->getreferences();
                        foreach ($reference as $o) {
                            $mandateid = $o->getMandateId();
                            $endtoend  = $o->GetendToEndId();
                        }
                        if ($mandateid) {
                            $remit = $r->getRemittanceInformation()->getMessage();
                        } else {
                            $remit = "";
                        }
                        $bank = $r->getRelatedParties();
                        foreach ($bank as $party) {
                            $name = $party->getAccount()->getIban();
                        }
                        $am = $amount->getAmount() * 0.01;
                        echo $am . "\n";
                        if ($am < 0) {
                            $creddeb = 'D';
                        } else {
                            $creddeb = 'C';
                        }
                        unset($am);
                    }
                    $BatchPaymentId = $entry->getBatchPaymentId();
                    echo '<tr>
          <td>' . $this->uri->segment(4) . '</td>
          <td>' . $date . '</td>
          <td>' . $this->companyid . '</td>
          <td>' . $amount->getAmount() * 0.01 . '</td>
          <td>' . $name . '</td>
          <td>' . $endtoend . '</td>
          <td>' . $mandateid . '</td>
          <td>' . $remit . '</td>
          <td>FILED</td>
          <td>' . $BatchPaymentId . '</td>
          <td>' . $creddeb . '</td>
          <td>' . $transactionissuer . '</td>
          <td>' . $transactioncode . '</td>
          <td>' . $ret_code . '</td>
          <td>' . $ret_extra . '</td></tr>';
                }
            }
            echo '</table>';
        }
    }
    public function index()
    {
        $this->data['title']        = lang("Sepa File Items and Unassign Payments");
        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'sepa_file';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }
    public function upload_bankfile()
    {
        set_time_limit(0);
        $this->load->model('Admin_model');
        $this->load->library('magebo', array(
        'companyid' => $this->companyid
        ));
        $config['upload_path']   = $this->data['setting']->DOC_PATH . $this->companyid . '/csv/';
        $config['allowed_types'] = 'csv';
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('userfile')) {
            $error = array(
            'error' => $this->upload->display_errors()
            );
            $this->session->set_flashdata('error', $this->upload->display_errors());
        } else {
            $this->db = $this->load->database('default', true);
            $data     = $this->upload->data();
            $this->db->insert('a_sepa_files', array(
            'filename' => $data['file_name'],
            'status' => 'Processing',
            'companyid' => $this->companyid,
            'uploader' => $this->session->firstname . ' ' . $this->session->lastname
            ));
            $fileid = $this->db->insert_id();
            $array  = array_map('str_getcsv', file($this->data['setting']->DOC_PATH . $this->companyid . '/csv/' . $data['file_name']));
            log_message('error', $array);

            foreach ($array as $key => $row) {
                log_message('error', print_r($row, true));
                if ($key > 0) {
                    if (str_replace('+', '', str_replace(',', '.', $row[6])) > 0) {
                        $art = 'C';
                    } else {
                        $art = 'D';
                    }
                    $trxid = $this->Admin_model->SepaItems(array(
                    'fileid' => $fileid,
                    'date_transaction' => $row['4'],
                    'debitcredit' => $art,
                    'companyid' => $this->companyid,
                    'transactioncode' => $row['13'],
                    'transactionissuer' => $row['2'],
                    'amount' => str_replace('+', '', str_replace(',', '.', $row['6'])),
                    'iban' => $row['8'],
                    'end2endid' => $row['15'],
                    'mandateid' => $row['18'],
                    'message' => $row[19],
                    'name_owner' => $row[9],
                    'return_code' => $row[22],
                    'return_extra' => 'not_sent',
                    'status' => 'Pending',
                    'BatchPaymentId' => $row[14],
                    'pmt' => $row[16],
                    'cd' => $row[17],
                    'oxt' => $row[25]
                    ));
                    log_message('error', print_r(array(
                    'fileid' => $fileid,
                    'date_transaction' => $row['4'],
                    'debitcredit' => $art,
                    'companyid' => $this->companyid,
                    'transactioncode' => $row['13'],
                    'transactionissuer' => $row['2'],
                    'amount' => str_replace('+', '', str_replace(',', '.', $row['6'])),
                    'iban' => $row['8'],
                    'end2endid' => $row['15'],
                    'mandateid' => $row['18'],
                    'message' => $row[19],
                    'name_owner' => $row[9],
                    'return_code' => $row[22],
                    'return_extra' => 'not_sent',
                    'status' => 'Pending',
                    'BatchPaymentId' => $row[14],
                    'pmt' => $row[16],
                    'cd' => $row[17],
                    'oxt' => $row[25]
                    ), true));
                    if ($art == "C") {
                        if (!empty($row[18])) {
                            if (strlen(trim($row[18])) == 16) {
                                $iInvoiceNbr = substr(substr(trim($row[18]), 1, 15), 0, 9);
                                if (is_numeric($iInvoiceNbr) && strlen($iInvoiceNbr) == 9) {
                                    $invoice = $this->magebo->getInvoice($iInvoiceNbr);
                                    if ($invoice) {
                                        $userid = getWhmcsid($invoice->iAddressNbr);

                                        $client = $this->Admin_model->getClient($userid);
                                        if ($client->dunning_profile == "4") {
                                        }

                                        

                                        $this->db->insert('a_payments', array(
                                        'date' => $row[4],
                                        'invoiceid' => trim($iInvoiceNbr),
                                        'userid' => $userid,
                                        'companyid' => $this->session->cid,
                                        'paymentmethod' => 'BT',
                                        'transid' => $trxid,
                                        'adminid' => $this->session->id,
                                        'iban' => $row['8'],
                                        'amount' => str_replace('+', '', str_replace(',', '.', $row['6'])),
                                        'raw' => 'Executed by: ' . $this->session->firstname . ' ' . $this->session->lastname
                                        ));

                                        $payment  = array(
                                        'mPaymentAmount' => str_replace('+', '', str_replace(',', '.', $row['6'])),
                                        'cPaymentForm' => 'BANK TRANSACTION',
                                        'cPaymentFormDescription' => "BANK TRANSFER :" . $row[18],
                                        'dPaymentDate' => convert_invoice_date($row[4]),
                                        'iInvoiceNbr' => trim($iInvoiceNbr),
                                        'step' => 1,
                                        'iAddressNbr' => $invoice->iAddressNbr,
                                        'cPaymentRemark' => 'Executed Upload FileID: ' . $fileid
                                        );
                                        send_growl(array(
                                        'message' => lang('Invoice').' '.trim($iInvoiceNbr).' '.lang(' has been paid, executed by').' '.$this->session->firstname . ' ' . $this->session->lastname,
                                        'companyid' => $this->session->cid));
                                        $response = $this->magebo->apply_payment($payment);
                                        $this->Admin_model->SepaItemsComplete($trxid, array('status' => 'Completed', 'invoicenumber' => trim($iInvoiceNbr), 'iAddressNbr' => $invoice->iAddressNbr));
                                        if (!$this->magebo->hasUnpaidInvoice($invoice->iAddressNbr)) {
                                            $this->load->library('artilium', array(
                                            'companyid' => $this->companyid
                                            ));
                                            $services = $this->Admin_model->getServices($userid);
                                            if ($services) {
                                                foreach ($services as $serv) {
                                                    if ($serv->status == "Suspended") {
                                                        $ii   = $this->Admin_model->getPackageState($serv->id);
                                                        $pack = (array) json_decode($ii);
                                                        $this->artilium->UnBlockOriginating(trim($serv->sn), $pack, '1');
                                                        $this->Admin_model->update_services_data('mobile', $serv->id, array(
                                                        'bar' => 0
                                                        ));
                                                        $this->Admin_model->update_services($serv->id, array(
                                                        'status' => 'Active'
                                                        ));
                                                        logAdmin(array(
                                                        'companyid' => $this->companyid,
                                                        'userid' => $userid,
                                                        'serviceid' => $serv->id,
                                                        'user' => $this->session->firstname . ' ' . $this->session->lastname,
                                                        'ip' => '127.0.0.1',
                                                        'description' => $serv->domain.' '.lang('has been unsuspended')
                                                        ));
                                                        send_growl(array(
                                                        'message' => $serv->domain.' '.lang('has been unsuspended'),
                                                        'companyid' => $this->session->cid));
                                                    }
                                                }
                                            }
                                            $this->Admin_model->ChangeClientDunnigProfile($userid, 'No');
                                        }
                                    }
                                }
                            }
                        } else {
                            if (strlen(str_replace(' ', '', $row[19])) == 16) {
                                $iInvoiceNbr = substr(substr(trim(str_replace(' ', '', trim($row[19]))), 1, 15), 0, 9);
                                if (is_numeric($iInvoiceNbr) && strlen($iInvoiceNbr) == 9) {
                                    $invoice = $this->magebo->getInvoice($iInvoiceNbr);
                                    if ($invoice) {
                                        $userid = getWhmcsid($invoice->iAddressNbr);
                                        $client = $this->Admin_model->getClient($userid);
                                        if ($client->dunning_profile == "4") {
                                        }
                        
                                        $this->db->insert('a_payments', array(
                                        'date' => $row[4],
                                        'invoiceid' => trim($iInvoiceNbr),
                                        'userid' => $userid,
                                        'companyid' => $this->session->cid,
                                        'paymentmethod' => 'BT',
                                        'transid' => $trxid,
                                        'adminid' => $this->session->id,
                                        'iban' => $row['8'],
                                        'amount' => str_replace('+', '', str_replace(',', '.', $row['6'])),
                                        'raw' => 'Executed by: ' . $this->session->firstname . ' ' . $this->session->lastname
                                        ));
                                        $payment  = array(
                                        'mPaymentAmount' => str_replace('+', '', str_replace(',', '.', $row['6'])),
                                        'cPaymentForm' => 'BANK TRANSACTION',
                                        'cPaymentFormDescription' => "BANK TRANSFER :" . $row[18],
                                        'dPaymentDate' => convert_invoice_date($row[4]),
                                        'iInvoiceNbr' => trim($iInvoiceNbr),
                                        'step' => 1,
                                        'iAddressNbr' => $invoice->iAddressNbr,
                                        'cPaymentRemark' => 'Executed Upload FileID: ' . $fileid
                                        );
                                        send_growl(array(
                                        'message' => 'Invoice '.trim($iInvoiceNbr).' has been paid, executed by '.$this->session->firstname . ' ' . $this->session->lastname,
                                        'companyid' => $this->session->cid));
                                        $response = $this->magebo->apply_payment($payment);
                                        $this->Admin_model->SepaItemsComplete($trxid, array('status' => 'Completed', 'invoicenumber' => trim($iInvoiceNbr), 'iAddressNbr' => $invoice->iAddressNbr));
                                        if (!$this->magebo->hasUnpaidInvoice($invoice->iAddressNbr)) {
                                            $this->load->library('artilium', array(
                                            'companyid' => $this->companyid
                                            ));
                                            $services = $this->Admin_model->getServices($userid);
                                            if ($services) {
                                                foreach ($services as $serv) {
                                                    if ($serv->status == "Suspended") {
                                                        $ii   = $this->Admin_model->getPackageState($serv->id);
                                                        $pack = (array) json_decode($ii);
                                                        $this->artilium->UnBlockOriginating(trim($serv->sn), $pack, '1');
                                                        $this->Admin_model->update_services_data('mobile', $serv->id, array(
                                                        'bar' => 0
                                                        ));
                                                        $this->Admin_model->update_services($serv->id, array(
                                                        'status' => 'Active'
                                                        ));
                                                        send_growl(array(
                                                        'message' => $serv->domain.' '.lang('has been unsuspended'),
                                                        'companyid' => $this->session->cid));
                                                        logAdmin(array(
                                                        'companyid' => $this->companyid,
                                                        'userid' => $userid,
                                                        'serviceid' => $serv->id,
                                                        'user' => $this->session->firstname . ' ' . $this->session->lastname,
                                                        'ip' => '127.0.0.1',
                                                        'description' => $serv->domain.' '.lang('has been unsuspended')
                                                        ));
                                                    }
                                                }
                                            }
                                            $this->Admin_model->ChangeClientDunnigProfile($userid, 'No');
                                        }
                                    }
                                }
                            }
                        }//end else
                    }
                    unset($userid);
                    unset($trxid);
                    unset($iInvoiceNbr);
                    unset($invoice);
                }
            }
            $this->session->set_flashdata('success', lang('FIle').': ' . $data['file_name'] . ' '.lang('has been uploaded and to be queued for processing'));
        }
        redirect('admin/sepa');
    }
    public function detail()
    {
        $this->data['title']        = "Sepa File Items";
        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'sepa_items';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }
    public function sepa_directdebit()
    {
        error_reporting(E_ALL);
        ini_set('display_errors', 1);
        $this->data['title']        = "Sepa File Bank List";
        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'sepa_file_tobank';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }
    public function upload_sepa_directdebit()
    {
        $setting = globofix($_POST['companyid']);
        $this->db->insert('a_sepa_directdebit', $_POST);
        $id                    = $this->db->insert_id();
        $config['upload_path'] = $setting->DOC_PATH . $_POST['companyid'] . '/directdebit/';
        if (!file_exists($config['upload_path'])) {
            mkdir($config['upload_path']);
        }
        $config['allowed_types'] = 'xml';
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('file')) {
            $error = array(
            'error' => $this->upload->display_errors()
            );
        } else {
            $data = $this->upload->data();
            if (file_exists($config['upload_path'] . $data['file_name'])) {
                $this->db->query("update a_sepa_directdebit set filename=? where id=?", array(
                $data['file_name'],
                $id
                ));
                $this->send_sepa_notification($setting->DOC_PATH . $_POST['companyid'] . '/directdebit/' . $data['file_name'], $_POST['companyid']);
                if ($_POST['companyid'] == 53) {
                    $connection = ssh2_connect($setting->sftp_host, $setting->sftp_port, array(
                    'hostkey' => 'ssh-rsa'
                    ));
                    if (ssh2_auth_pubkey_file($connection, $setting->sftp_user, $setting->sftp_pub, $setting->sftp_key)) {
                        echo "Public Key Authentication Successful\n";
                        ssh2_scp_send($connection, $setting->DOC_PATH . $_POST['companyid'] . '/directdebit/' . $data['file_name'], $setting->sftp_bank . $data['file_name'], 0755);
                        send_growl(array('companyid' => $_POST['companyid'], 'message'=> $this->session->firstname.' has uploaded sepa directdebit for Delta'));
                        $this->session->set_flashdata('success', 'File uploaded to remote server: ' . $setting->sftp_host);
                    } else {
                        die('Public Key Authentication Failed');
                    }
                }
                if ($_POST['companyid'] == 56) {
                    $connection = ssh2_connect($setting->sftp_host, $setting->sftp_port);
                    if (ssh2_auth_password($connection, $setting->sftp_user, $setting->sftp_password)) {
                        echo "Password  Authentication Successful\n";
                        ssh2_scp_send($connection, $setting->DOC_PATH . $_POST['companyid'] . '/directdebit/' . $data['file_name'], $setting->sftp_bank . $data['file_name'], 0755);
                        $this->session->set_flashdata('success', 'File uploaded to remote server: ' . $setting->sftp_host);
                        send_growl(array('companyid' => $_POST['companyid'], 'message'=> $this->session->firstname.' has uploaded sepa directdebit for Caiway'));
                    } else {
                        die('Public Key Authentication Failed');
                    }
                }
                if ($_POST['companyid'] == 54) {
                    $row =array(
                    'BatchPaymentId' => $_POST['iBankIndex'],
                    'date_transaction' => date('Y-m-d'),
                    'fileid' => $id,
                    'companyid' => $_POST['companyid']
                    );
                    file_put_contents(APPPATH.'queue/'.$_POST['iBankIndex'].'.payment', json_encode($row));
                    /*
                    $this->process_sepa_to_paid((object) array(
                    'BatchPaymentId' => $_POST['iBankIndex'],
                    'date_transaction' => date('Y-m-d'),
                    'fileid' => $id,
                    'companyid' => $_POST['companyid']
     ));
     */
                    //$this->db->query("update a_configuration set val=? where companyid=? and name=?", array('1', $_POST['companyid'], 'update_reseller_earnings'));
                    send_growl(array('companyid' => $_POST['companyid'], 'message'=> $this->session->firstname.' has uploaded sepa directdebit for Trendcall'));
                    $this->session->set_flashdata('success', lang('Mvno has been notified that sepa directdebit has been created The invoices will be processed for payments and you will get some notification on the portal'));
                }
            } else {
                $this->session->set_flashdata('error', lang('File was not uploaded'));
            }
        }
        echo $setting->DOC_PATH . $_POST['companyid'] . '/directdebit/' . $data['file_name'];
        echo "<br />";
        echo $setting->sftp_bank . $data['file_name'];
        redirect('admin/sepa/sepa_directdebit');
    }

    public function set_reject_complete()
    {
        $id = $this->uri->segment(4);
        $this->db->where('id', $id);
        $this->db->update('a_sepa_items', array(
        'extra_status' => 1
        ));
        $this->session->set_flashdata('success', lang('successfully set to completed'));
        redirect('admin/sepa');
    }
    public function send_sepa_notification($file, $companyid)
    {
        $this->data['setting'] = globofix($companyid);
        if (!empty($this->data['setting']->sepa_email_notification)) {
            $recipients         = explode('|', $this->data['setting']->sepa_email_notification);
            $config['protocol'] = 'sendmail';
            $config['mailpath'] = '/usr/sbin/sendmail';
            $config['mailtype'] = 'html';
            $config['charset']  = 'utf-8';
            $config['wordwrap'] = true;
            $this->email->clear(true);
            $this->email->initialize($config);
            $this->data['language'] = "dutch";
            $body                   = "Hello,<br /> Your sepa file has been uploaded on your FTP server if you opted for it otherwise here is your File<br /><br />Regards, <br>United Telecom";
            $this->email->set_newline("\r\n");
            $this->email->from('noreply@united-telecom.be', 'United Team');
            $this->email->to($recipients);
            //$this->email->cc('mail@simson.one');
            $this->email->bcc('it@united-telecom.be');
            $this->email->subject(lang("Sepa File Directdebit"));
            $this->email->attach($file);
            $this->email->message($body);
            if ($this->email->send()) {
                return true;
            } else {
                return false;
            }
        }
    }
}
