<?php
defined('BASEPATH') or exit('No direct script access allowed');
class SuperController extends CI_Controller
{
    protected $data = [];
    public function __construct()
    {
        // Ensure you run parent constructor
        parent::__construct();
        if (!empty($this->session->language)) {
            $this->config->set_item('language', $this->session->language);
        } else {
            $this->config->set_item('language', 'english');
        }

        $this->companyid       = get_companyidby_url(base_url());
        $this->data['setting'] = globofix($this->companyid);
        if (file_exists(APPPATH.'language/'.$this->config->item('language').'/'.$this->companyid.'_admin_lang.php')) {
            // log_message('error', 'Using Custom language files');
            $this->lang->load($this->companyid.'_admin');
        } else {
            // log_message('error', APPPATH.'language/'.$this->config->item('language').'/'.$this->companyid.'_admin.php');
            $this->lang->load('admin');
        }
        $this->load->library('magebo', array(
            'companyid' => $this->companyid
        ));
    }
}
class Super extends SuperController
{
    public function __construct()
    {
        parent::__construct();
        $this->companyid = get_companyidby_url(base_url());
        //$this->load->model('Sepa_model');
        $this->load->model('Admin_model');
        //$this->load->model('Invoice_model');
        if (!$this->session->master) {
            $this->session->set_flashdata('error', 'Access Denied');
            redirect('admin');
        }
    }
    public function index()
    {
        //$this->data['dtt']          = 'sepa_file.js?version=1.2';
        $this->data['title']        = "Sepa File List";
        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'super/dashboard';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }
    public function sync()
    {
        send_growl(array('companyid' => $this->session->cid, 'message' => 'Framework has been updated to version V1.5.'.time()));
        shell_exec('sync_dev');
        redirect('admin');
    }
    public function profiler()
    {
        if ($this->uri->segment(4) >0) {
            $_SESSION['profiler'] = 1;
        } else {
            $_SESSION['profiler'] = 0;
        }
        redirect('admin/dashboard');
    }
    public function internet_status()
    {
        $this->load->model('Admin_model');
        if (!empty($this->uri->segment(4))) {
            $this->data['xdsl'] = $this->Admin_model->getInternetStatus($this->uri->segment(4), $this->uri->segment(5));
            if (!$this->data['xdsl']) {
                $this->data['xdsl'] = (object)array('status' => 'Unknown', 'username' => $this->uri->segment(4), 'password' => '', 'ipaddress' => '');
            }
            //  print_r( $this->data['xdsl']);
        } else {
            $this->data['xdsl'] = (object)array('status' => 'Unknown', 'username' => $this->uri->segment(4), 'password' => '', 'ipaddress' => '');
        }

        // print_r($this->data['xdsl']);
        // exit;
        $this->data['title']        = "Internet Status Radius";
        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'super/internet_status';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }

    public function move_customer_to_company()
    {
        $userid = $this->uri->segment(4);
        $companyid = $this->uri->segment(5);

        if ($companyid && $userid) {
            $this->db->query("update a_clients set companyid=?,mageboid=? where id=?", array($companyid,'0', $userid));
            $this->db->query("update a_services set companyid=? where userid=?", array($companyid, $userid));
            $this->db->query("update a_services_mobile set companyid=? where userid=?", array($companyid, $userid));
            $this->db->query("update a_email_log set companyid=? where userid=?", array($companyid, $userid));
            $this->db->query("update a_logs set companyid=? where userid=?", array($companyid, $userid));
            redirect('admin/client/detail/'.$userid);
        } else {
            die('Not working like this');
        }
    }
    public function getinternet()
    {
        redirect("admin/super/internet_status/".$_POST['username']."/".$_POST['realm']);
    }
    public function create_swap()
    {
        //$this->data['dtt']          = 'sepa_file.js?version=1.2';
        $this->data['title']        = "Create Swap SIm";
        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'super/create_swap';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }

    public function termination_service_list()
    {
        $this->data['c'] = false;
        //xzxz$this->data['dtt'] = 'termination_services.js?version=' . date('ymdhis');
        $this->data['title']        = lang("Termination Services List");
        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'termination_services';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }
    public function change_service_list()
    {

            //$this->data['dtt']          = 'change_services.js?version=' . date('ymdhis');
        $this->data['title']        = lang("Change Services List");
        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'subscriptions_changes';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }
    public function change_packages_do()
    {
        $post     = file_get_contents("php://input");
        $obj      = (array) json_decode($post);
        $this->db = $this->load->database('default', true);



        $q = $this->db->query("select * from a_subscription_changes where id=?", array(trim($obj['id'])));

        if ($q->num_rows()>0) {

            if($q->row()->keep_start_contract_date == "No"){
                $this->complete_subscription_changes($q->row_array(), false);
            }else{
                $this->complete_subscription_changes($q->row_array(), true);
            }

        }

        $this->db->where('id', trim($obj['id']));
        $this->db->update('a_subscription_changes', array(
                    'status' => '1',
                    'executor' => $this->session->firstname . ' ' . $this->session->lastname
            ));

        echo json_encode(array(
                    'result' => $this->db->affected_rows(),
                    'row' => $q->row_array()
            ));
    }
    public function terminate_apackage_do()
    {
        $post = file_get_contents("php://input");
        $obj = (array) json_decode($post);

        $this->db = $this->load->database('default', true);
        $this->db->where('id', $obj['id']);
        $this->db->update('a_services', array('termination_status' => 1));
        echo json_encode($obj);
    }

    public function complete_subscription_changes($obj, $contract_date_change=true)
    {
        $serviceid = $obj['serviceid'];
        $newdate =  $obj['date_commit'];
        $old_pid = $obj['old_pid'];
        $companyid = $this->session->cid;

        $this->load->model('Admin_model');
        $InvoiceGroupNbr = $this->Admin_model->getiInvoiceGroupNbr($old_pid);
        $this->load->library('magebo', array(
                    'companyid' => $companyid
            ));
        $service               = $this->Admin_model->getServiceCli($serviceid);

        $date_contract         = convert_to_contract($newdate);
        $this->data['setting'] = globofix($service->companyid);

        if ($service->iGeneralPricingIndex) {
            $GP = explode(',', $service->iGeneralPricingIndex);
            if (count($GP) == 1) {
                $date_contract = convert_to_contract($newdate);
                $this->magebo->UpgradeDowngrade($service->iGeneralPricingIndex, str_replace('-', '', $newdate));
                $this->Admin_model->update_services($serviceid, array(
                            'iGeneralPricingIndex' => null,
                            'date_contract' => $date_contract
                    ));
                $this->process_pricing($service->userid, $serviceid, $service->companyid);
                $this->magebo->migrateCallPricing($service->iGeneralPricingIndex, str_replace('-', '', $newdate));
            } elseif (count($GP) > 1) {
                foreach ($GP as $G) {
                    $this->magebo->CheckSubscriptionPricing($InvoiceGroupNbr, $G) . "\n";
                    if ($this->magebo->CheckSubscriptionPricing($InvoiceGroupNbr, $G)) {
                        $this->magebo->UpgradeDowngrade($G, str_replace('-', '', $newdate));
                        $this->Admin_model->update_services($serviceid, array(
                            'iGeneralPricingIndex' => null,
                            'date_contract' => $date_contract
                        ));
                        $this->process_pricing($service->userid, $serviceid, $service->companyid);
                        $this->magebo->migrateCallPricing($G, str_replace('-', '', $newdate));
                    }
                }
            }



            //if ($service->companyid == "54") {
            $invoice_condition = $service->PriceList;
            $PriceListTarif    = $service->PriceListSubType;
            $client = $this->Admin_model->getClient($service->userid);
            $priority = $this->magebo->getLatestPriority($service->details->msisdn);
            $this->magebo->InsertPricelist($client->mageboid, $invoice_condition, $service->details->msisdn, $PriceListTarif, $client->vat_exempt, $priority, true);
            //}

            $check = $this->magebo->updateGeneraPricingIndex(trim($service->details->msisdn), $service->iGeneralPricingIndex);
            if ($check) {
                // echo $row->msisdn."    ".$row->serviceid."    ".$row->iGeneralPricingIndex. "  ".$check->iGeneralPricingIndex."\n";
                //print_r($check);
                $this->db->query("update a_services set iGeneralPricingIndex=? where id=?", array($check->iGeneralPricingIndex, $serviceid));
            }
        }

        if($contract_date_change){
            $date_contract = convert_to_contract($newdate);
            }else{
            $date_contract         =  $service->date_contract;
            }
            $this->Admin_model->update_services($serviceid, array(
                'date_contract' => $date_contract
            ));

    }


    public function process_pricing($userid, $serviceid, $companyid)
    {
        $this->load->library('magebo', array(
                    'companyid' => $companyid
            ));
        $service = $this->db->query("select * from a_services where userid=? and status=? and id=?", array(
                    $userid,
                    'Active',
                    $serviceid
            ));

        $srv = $this->db->query("select * from a_services_mobile where serviceid=?", array($serviceid));

        if ($service->num_rows() > 0) {
            foreach ($service->result() as $row) {
                $mobile = $this->Admin_model->getServiceCli($row->id);
                if (!$row->iGeneralPricingIndex) {
                    if ($srv->num_rows()>0) {
                        if ($srv->msisdn_type == "porting") {
                            $po = true;
                        } else {
                            $po = false;
                        }
                    }


                    $this->magebo->addPricingSubscription($mobile, true, $po) . "\n";
                    if (($row->msisdn_type == "porting") && (substr($mobile->details->msisdn, 0, 2) == "31")) {
                        $this->Api_model->PortIngAction1($mobile);
                        $this->Api_model->PortIngAction2($mobile);
                        $this->Api_model->PortIngAction3($mobile);
                        $this->Api_model->PortIngAction4($mobile);
                        $this->Api_model->PortIngAction5($mobile->companyid);
                    }
                    unset($mobile);
                }
            }
        }
    }
    public function processImport()
    {
        $this->load->library('encrypt');

        $username = $_POST['username'];
        $password = $_POST['password'];
        $this->Admin_model->update_services($_POST['serviceid'], array(
            'username' => $username,
            'password' => $this->encryption->encrypt($password)));
        /*
        alpha: "A"
circuitid: ""
city: "Kelmis"
number: "13"
password: "e9syah46"
postcode: "4721 "
realm: "@unitedadsl"
serviceid: "1809785"
street: "Kehrweg"
username: "ut6354203"
whmcsid: "73063"

*/

        $this->db->insert(
            'a_services_xdsl',
            array('requestid' => null,
            'companyid' => 2,
            'userid' => $_POST['userid'],
            'serviceid' => $_POST['serviceid'],
            'circuitid' =>$_POST['circuitid'],
            'status' => 'Active',
            'street' => $_POST['street'],
            'number' => $_POST['number'],
            'alpha' => $_POST['alpha'],
            'bus' => null,
            'floor' => null,
            'block' => null,
            'city' => $_POST['city'],
            'postcode' => $_POST['postcode'],
            'country' => 'BEL',
            'phonenumber' => null,
            'dialnumber' => null,
            'proximus_status' => 'Active',
            'profile' => null,
            'lex' => null,
            'proformaid' => null,
            'created' => null,
            'last_updated' => date('Y-m-d H:i:s'),
            'proximus_orderid' => null,
            'product_name' => null,
            'product_id' =>  null,
            'address_verified' => 1,
            'feedback_description' => null,
            'address_lang' => null,
            'MDU_SDU ' => null,
            'appointment_date_requested' => null,
            'appointment_date_confirmed' => null,
            'remarks' => 'Imported from Whmcs: Serviceid: '.$_POST['whmcsid'],
            'workorderid' => null,
            'InvoiceGroupMaster' => null,
            'ArticleIdVOIP' => null,
            'realm' =>  $_POST['realm'])
        );

        if ($this->db->insert_id()>0) {
            $this->session->set_flashdata('success', 'Order has been imported');
            echo json_encode(array('result' => 'success'));
        } else {
            $this->session->set_flashdata('success', 'Order fail to be imported');
            echo json_encode(array('result' => 'error'));
        }
    }
    public function getWhmcsOrder()
    {
        $serviceid = $_POST['localid'];

        $service = $this->Admin_model->getService($serviceid);
        $params['userid'] = $service->userid;
        $circuitid = $_POST['circuitid'];
        $params['result'] = 'success';
        $this->db = $this->load->database('import', true);
        $res = $this->Admin_model->united_api(array('action' => 'GetClientsProducts', 'serviceid' => $_POST['serviceid']));
        $params['res']= $res;
        if ($res->result == "success") {
            $product = $res->products->product[0];

            foreach ($product->customfields->customfield as $row) {
                $params[str_replace('.', '', $row->name)] = $row->value;
            }
        } else {
            $params['result'] = 'error';
        }



        echo json_encode($params);
    }
}
