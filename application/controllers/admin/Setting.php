<?php defined('BASEPATH') or exit('No direct script access allowed');
class SettingController extends CI_Controller
{
    protected $data = [];

    public function __construct()
    {
        // Ensure you run parent constructor
        parent::__construct();
        $this->companyid = get_companyidby_url(base_url());
        if (!empty($this->session->language)) {
            $this->config->set_item('language', $this->session->language);
        } else {
            $this->config->set_item('language', 'english');
        }
        if (file_exists(APPPATH.'language/'.$this->config->item('language').'/'.$this->companyid.'_admin_lang.php')) {
            // log_message('error', 'Using Custom language files');
            $this->lang->load($this->companyid.'_admin');
        } else {
            // log_message('error', APPPATH.'language/'.$this->config->item('language').'/'.$this->companyid.'_admin.php');
            $this->lang->load('admin');
        }
        $this->data['setting'] = globofix($this->companyid);
    }
}
class Setting extends SettingController
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *     http://example.com/index.php/welcome
     *  - or -
     *     http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->model('Admin_model');
        $this->db = $this->load->database('default', true);
    }
    public function saveTranslation()
    {
        $write = "<?php\n";
        file_put_contents(APPPATH.'language/'.$this->uri->segment(4).'/'.$this->companyid.'_'.$_POST['filename'].'_lang.php', $write);
        $this->db->query("update a_translations set value= ? where companyid=? and  id=? and filename=?", array($_POST['value'], $this->companyid, $_POST['id'], $_POST['filename']));
        $q = $this->db->query("select name,value from a_translations where companyid=? and lang = ? and filename=?", array($this->companyid, $this->uri->segment(4), $_POST['filename']));
        foreach ($q->result() as $r) {
            file_put_contents(APPPATH.'language/'.$this->uri->segment(4).'/'.$this->companyid.'_'.$_POST['filename'].'_lang.php', '$lang["'.$r->name.'"] = "'.str_replace('"', '\"', $r->value).'";'.PHP_EOL, FILE_APPEND);
        }

        echo json_encode(array('result' => true));
    }

    public function addTranslation()
    {
        foreach ($_POST['lang'] as $key => $value) {
            if ($this->hasKey($this->companyid, $key, $_POST['keyword'])) {
                $this->db->query("update a_translations set value=? where name=? and lang=? and companyid=? and filename=?", array($value, $_POST['keyword'], $key, $this->companyid, $_POST['filename']));
            } else {
                $this->db->insert('a_translations', array('filename' => $_POST['filename'], 'companyid' => $_POST['companyid'], 'lang' => $key, 'name' => trim($_POST['keyword']), 'value' => $value));
            }
        }
        $write = "<?php\n";
        file_put_contents(APPPATH.'language/'.$this->session->language.'/'.$this->companyid.'_'.$_POST['filename'].'_lang.php', $write);
        //foreach

        $q = $this->db->query("select name,value from a_translations where companyid=? and lang = ? and filename=?", array($this->companyid, $this->session->language,$_POST['filename']));
        foreach ($q->result() as $r) {
            file_put_contents(APPPATH.'language/'.$this->session->language.'/'.$this->companyid.'_'.$_POST['filename'].'_lang.php', '$lang["'.$r->name.'"] = "'.str_replace('"', '\"', $r->value).'";'.PHP_EOL, FILE_APPEND);
        }

        echo json_encode(array('result' => true));
        //redirect('admin/setting/translations/'.$this->uri->segment(4));
    }

    public function hasKey($companyid, $lang, $name)
    {
        $this->db->where('companyid', $companyid);
        $this->db->where('lang', $lang);
        $this->db->where('name', $name);

        $q = $this->db->get('a_translations');
        if ($q->num_rows()>0) {
            return true;
        } else {
            return false;
        }
    }
    public function translations()
    {
        if (!empty($this->uri->segment(4))) {
            $q = $this->db->query("select * from a_translations where companyid=? and lang=? and filename=? order by name asc", array($this->companyid, $this->uri->segment(4), $this->uri->segment(5)));
            if ($q->num_rows()<=0) {
                if (file_exists(APPPATH.'language/'.$this->session->language.'/'.$this->companyid.'_'.$this->uri->segment(5).'_lang.php')) {
                    include(APPPATH.'language/'.$this->uri->segment(4).'/'.$this->companyid.'_'.$this->uri->segment(4).'_lang.php');
                } else {
                    include(APPPATH.'language/'.$this->uri->segment(4).'/'.$this->uri->segment(5).'_lang.php');
                }
                ksort($lang);
                foreach (array_filter(array_unique($lang)) as $key => $val) {
                    $res[] =  array(
                    'filename' => $this->uri->segment(5),
                      'companyid' => $this->companyid,
                      'lang' => $this->uri->segment(4),
                      'name' => str_replace('"', '\"', $key),
                      'value'=>str_replace('"', '\"', $val));
                }
                $this->db->insert_batch('a_translations', $res);
                $q = $this->db->query("select * from a_translations where companyid=? and lang=? and filename=? order by name asc", array($this->companyid, $this->uri->segment(4),$this->uri->segment(5)));
            }
            $this->data['trans'] = $q->result_array();
        }

        $this->data['lang'] = getSupportedLanguage($this->companyid);
        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'setting/translations';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }
    public function email()
    {
        if (isPost()) {
            foreach ($_POST as $key => $row) {
                $this->db->where('name', $key);
                $this->db->where('companyid', $this->companyid);
                if ($key == "smtp_pass") {
                    $this->db->update('a_configuration', array('val' => encrypt_decrypt('encrypt', $row)));
                } else {
                    $this->db->update('a_configuration', array('val' => $row));
                }
            }
            $this->data['setting'] = globo();
            $this->session->set_flashdata('success', lang('your  email configuration has been updated'));
        }

        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'setting/email_setting';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }

    public function invoices()
    {
        if (isPost()) {
            $config['upload_path'] = FCPATH . 'assets/img/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['encrypt_name'] = true;
            $this->load->library('upload', $config);

            if ($this->upload->do_upload('footer')) {
                $_POST['invoice_footer'] = $this->upload->data('file_name');
            }

            if ($this->upload->do_upload('logo')) {
                $_POST['invoice_logo'] = $this->upload->data('file_name');
            }
            unset($_POST['logo']);
            unset($_POST['footer']);
            foreach ($_POST as $key => $row) {
                $this->db->where('name', $key);
                $this->db->where('companyid', $this->companyid);
                $this->db->update('a_configuration', array('val' => $row));
            }
            $this->data['setting'] = globo();
            $this->session->set_flashdata('success', lang('Your Invoice configuration has been updated'));
            redirect('admin/setting/invoices');
        }

        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'setting/invoice_setting';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }
    public function email_templates()
    {
        $this->data['lang'] = getSupportedLanguage($this->companyid);
        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'setting/email_templates';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }


    public function disable_email_templates()
    {
        $template = $this->db->query("select * from a_email_templates where id=?", array($this->uri->segment(4)));

        if ($template->num_rows()>0) {
            $this->db->where('name', $template->row()->name);
            $this->db->where('companyid', $this->companyid);
            $this->db->update('a_email_templates', array('status' => 0));

            if ($this->db->affected_rows()>0) {
                $this->session->set_flashdata('success', 'Template has been disabled');
            }
        }

        log_message('error', 'Template id '.$this->uri->segment(4). ' has been disabled by '.$this->session->firstname.' '.$this->session->lastname);
        redirect('admin/setting/edit_email_templates/'.$this->uri->segment(4));
    }

    public function enable_email_templates()
    {
        $d = $this->db->query("select * from a_email_templates where id=?", array($this->uri->segment(4)));

        $this->db->where('name', $d->row()->name);

        $this->db->where('companyid', $this->companyid);
        $this->db->update('a_email_templates', array('status' => 1));

        if ($this->db->affected_rows()>0) {
            $this->session->set_flashdata('success', 'Template has been enabled');
        }
        log_message('error', 'Template id '.$this->uri->segment(4). ' has been enabled by '.$this->session->firstname.' '.$this->session->lastname);
        redirect('admin/setting/edit_email_templates/'.$this->uri->segment(4));
    }

    public function promotions()
    {
        //$this->data['lang'] = getSupportedLanguage($this->companyid);
        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'setting/promotions';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }
    public function get_promotion()
    {
        $promotion = $this->db->query("select * from a_promotion where id=?", array($this->uri->segment(4)));

        echo json_encode($promotion->row_array());
    }
    public function edit_addon()
    {
        $id = $this->uri->segment(4);
        if ($this->session->master != 1) {
            if ($this->session->role == "superadmin") {
                $this->db->where('companyid', $_POST['companyid']);
            } else {
                $this->session->set_flashdata('error', 'Access Denied');
                redirect('admin/dashboard');
            }
        }
        $this->db->where('id', $id);
        $this->db->update('a_products_mobile_bundles', $_POST);
        $this->session->set_flashdata('success', 'Addon has been Updated');
        echo json_encode(array('companyid' => $_POST['companyid'], 'result' => $this->db->affected_rows()));
    }

    public function add_promotion()
    {
        if (isPost()) {
            $_POST['companyid'] = $this->session->cid;

            if ($this->Admin_model->isPromotionUnique($_POST)) {
                $_POST['modified_by'] = $this->session->firstname.' '.$this->session->lastname;
                $result = $this->Admin_model->insertPromotion($_POST);
                if ($result) {
                    $this->session->set_flashdata('success', 'Promotion has been added');
                } else {
                    $this->session->set_flashdata('error', 'Error inserting promotion');
                }
            } else {
                $this->session->set_flashdata('error', 'Promotion Code must be Unique');
            }
            redirect('admin/setting/promotions');
        } else {
            $this->session->set_flashdata('error', 'Bad request');
            redirect('admin/setting/promotions');
        }
    }

    public function edit_promotion()
    {
        if (isPost()) {
            if ($this->Admin_model->isPromotionUnique($_POST)) {
                $_POST['modified_by'] = $this->session->firstname.' '.$this->session->lastname;
                $this->db->where('id', $_POST['id']);
                $this->db->where('companyid', $this->session->cid);
                $this->db->update('a_promotion', $_POST);
                $this->session->set_flashdata('success', 'Promotion has been updated');
            } else {
                $this->session->set_flashdata('error', 'Promotion Code must be Unique');
            }
        } else {
            $this->session->set_flashdata('error', 'Bad request');
            redirect('admin/setting/promotions');
        }

        redirect('admin/setting/promotions');
    }
    public function copy_product()
    {
        $this->db->where('id', $_POST['pid']);
        $this->db->where('companyid', $this->uri->segment(4));
        $q = $this->db->get('a_products');
        if (!empty($_POST['copy'])) {
            if ($q->num_rows()>0) {
                $this->db->insert('a_products', $q->row_array());
            }
        } else {
            if ($q->num_rows() > 0) {
                echo json_encode($q->row_array());
            } else {
                echo json_encode(array());
            }
        }
    }
    public function delete_promotion()
    {
        if (isPost()) {
            $this->db->where('id', $_POST['id']);
            $this->db->where('companyid', $this->session->cid);
            $this->db->delete('a_promotion');
            $this->session->set_flashdata('success', 'Promotion has been deleted');
            redirect('admin/setting/promotions');
        } else {
            $this->session->set_flashdata('error', 'Bad request');
            redirect('admin/setting/promotions');
        }
    }

    public function edit_email_templates()
    {
        $this->data['lang'] = getSupportedLanguage($this->companyid);
        $tpl = $this->Admin_model->getTemplates($this->uri->segment(4));
        $i = $tpl[0];
        if (!$tpl) {
            $this->session->set_flashdata('error', 'access denied');
            redirect('admin/setting/email_templates');
        } else {
            if (isPost()) {
                $subject = array();
                $id = $_POST['templateid'];
                unset($_POST['templateid']);
                unset($_POST['files']);

                foreach ($_POST as $key => $ddd) {
                    $r = explode('-', $key);

                    if (count($r) == 2) {
                        $subject[$r[1]] = $ddd;
                        unset($_POST[$key]);
                    }
                }

                foreach ($_POST as $key => $body) {
                    $tplid = hasTemplateLanguage(trim($key), getTemplatename($id), $this->companyid);

                    if ($tplid) {
                        $this->db->query("update a_email_templates set body=?, date_modified=?, last_modifier=?, subject=? where id=?", array($body, date('Y-m-d H:i:s'), $this->session->firstname . ' ' . $this->session->lastname, $subject[$key], $tplid));
                    } else {
                        if (!empty($key)) {
                            // if ($key == "files") {
                            $this->db->insert(
                                "a_email_templates",
                                array('name' => getTemplatename($id),
                                    'default_body' => $body,
                                    'body' => $body,
                                    'subject' => $subject[$key],
                                    'language' => $key,
                                    'date_modified' => date('Y-m-d H:i:s'),
                                    'last_modifier' => $this->session->firstname . ' ' . $this->session->lastname,
                                'companyid' => $this->companyid)
                            );
                            // }
                        }
                    }
                    log_message('error', $this->db->last_query());
                    unset($tplid);
                }

                $tpl = $this->Admin_model->getTemplates($this->uri->segment(4));
                $i = $tpl[0];
            }

            foreach ($tpl as $row) {
                $this->data['tpl'][$row->language] = (object) array('id' => $row->id, 'body' => $row->body, 'subject' => $row->subject);
                $this->data['tpl']['default_body'] = $i->default_body;
            }
            $this->data['template'] = $tpl[0];
        }

        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'setting/email_templates_edit';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }

    public function welcome_template()
    {
        $this->data['lang'] = getSupportedLanguage($this->companyid);
        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'setting/welcome_templates';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }

    public function edit_welcome_templates()
    {
        $this->data['lang'] = getSupportedLanguage($this->companyid);
        $tpl = $this->Admin_model->getWelcomeTemplates($this->uri->segment(4));
        $i = $tpl[0];

        if (!$tpl) {
            $this->session->set_flashdata('error', 'access denied');
            redirect('admin/setting/email_template');
        } else {
            if (isPost()) {
                $subject = array();
                $id = $_POST['templateid'];
                unset($_POST['templateid']);
                unset($_POST['files']);

                //POST);
                // exit;
                $this->db = $this->load->database('default', true);
                foreach ($_POST as $lang => $body) {
                    $this->Admin_model->updateWelcomeTemplete($this->companyid, $lang, $body, trim($this->uri->segment(4)));
                }
                $tpl = $this->Admin_model->getWelcomeTemplates($this->uri->segment(4));
                /* foreach ($_POST as $key => $body) {

                $tplid = hasTemplateLanguage(trim($key), getTemplatename($id), $this->companyid);
                if ($tplid) {

                $this->db->query("update a_products_pdf set body=?, date_modified=?, last_modifier=?, subject=? where id=?", array($body, date('Y-m-d H:i:s'), $this->session->firstname . ' ' . $this->session->lastname, $subject[$key], $tplid));

                } else {
                if (!empty($key)) {
                if ($key == "files") {
                $this->db->insert("a_products_pdf",
                array('name' => getTemplatename($id),
                'default_body' => $body,
                'body' => $body,
                'subject' => $subject[$key],
                'language' => $key,
                'date_modified' => date('Y-m-d H:i:s'),
                'last_modifier' => $this->session->firstname . ' ' . $this->session->lastname,
                'companyid' => $this->companyid));
                }
                }
                }
                unset($tplid);

                }

                $tpl = $this->Admin_model->getTemplates($this->uri->segment(4));
                $i = $tpl[0];
             */
            }

            foreach ($tpl as $row) {
                $this->data['tpl'][$row->language] = (object) array('id' => $row->id, 'body' => $row->body, 'subject' => $row->subject);
                $this->data['tpl']['default_body'] = $i->default_body;
            }
            $this->data['template'] = $tpl[0];
        }

        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'setting/welcome_templates_edit';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }

    public function configuration()
    {
        /* if (isPost()) {
        foreach ($_POST as $key => $row) {
        $this->db->where('name', $key);
        $this->db->where('companyid', $this->companyid);
        $this->db->update('a_configuration', array('val' => $row));

        }

        $this->session->set_flashdata('success', lang('Your  global configuration has been updated'));

        }
         */
        $this->data['company_config'] = globofix($this->session->cid);
        $this->data['company'] = $this->Admin_model->GetCompany($this->session->cid);
        $this->data['setting'] = globo();
        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'setting/configuration';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }
    public function switch_company()
    {
        $this->load->helper('string');
        $this->load->database('default', true);
        if ($this->session->master) {
            $s = $this->db->query('select * from a_admin where id=?', array($this->session->id));
            $admin = $s->row_array();
            $random = random_string('alnum', 16);
            $adminid = $this->session->id;
            $comp = $this->db->query('select * from a_mvno where id=?', array($this->uri->segment(4)));
            $q = $this->db->query("select * from a_admin where companyid=? and email=?", array($comp->row()->companyid, $this->session->email));
            if ($q->num_rows() > 0) {
                $this->db->query("update a_admin set master_code=? where id=?", array($random, $q->row()->id));
                //$this->session->sess_destroy();
                header('Location: ' . $comp->row()->portal_url . 'admin/auth/switchcompany/' . $random);
            } else {
                unset($admin['logged']);
                $admin['master_code'] = $random;
                $admin['username'] = $admin['firstname'] . rand(1000, 9999);
                $admin['companyid'] = $comp->row()->companyid;
                unset($admin['id']);
                $this->db->insert('a_admin', $admin);
                echo $this->db->insert_id();
                header('Location: '.$comp->row()->portal_url.'admin/auth/switchcompany/'.$random);
            }
        }
    }
    public function updates()
    {
        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'setting/updates';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }
    public function backup_database()
    {
        $this->session->set_flashdata('error', lang('This utilities is disabled on demo version'));
        redirect('admin');
        /*
        $this->load->dbutil();
        $time = date('YmdHis');
        $backup = $this->dbutil->backup();
        write_file($this->data['setting']->DOC_PATH . 'backups/backup_' . $time . '.gz', $backup);
        $this->load->helper('download');
        force_download('backup_' . $time . '.gz', $backup);
     */
    }
    public function api()
    {
        $this->data['dtt'] = 'tokens.js?version=3.8';
        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'setting/api';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }
    public function staf()
    {
        $this->data['dtt'] = 'staf.js?version='.time();
        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'setting/staf';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }
    public function edit_pricing()
    {
        if (isPost()) {
            $this->load->model('Admin_model');
            $this->Admin_model->UpdatePricing($this->companyid, $_POST);
            mail('mail@simson.one', 'pricing', print_r($_POST, true));
            $this->session->set_flashdata('success', 'Price has been saved');
        }
        $products = $this->db->query("SELECT a.*,b.name  FROM `a_pricing_specific` a left join a_products b on b.id=a.packageid where a.companyid=?", array($this->session->cid));
        $this->data['products'] = $products->result();
        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'setting/edit_product';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }

    public function staf_delete()
    {
        $me = $this->Admin_model->getStaf($this->session->id);
        $this->data['id'] = $this->uri->segment(4);

        if ($this->data['id'] != 1 && $this->data['id'] != $me->id) {
            if (isPost()) {
                $this->db->where('id', $_POST['id']);
                $this->db->where('companyid', $this->companyid);
                $this->db->delete('a_admin');
                $this->session->set_flashdata('success', lang('Staf has been deleted'));
                redirect('admin/setting/staf');
            }
            $this->data['staf'] = $this->Admin_model->getStaf($this->data['id']);
            //print_r($this->data['staf']);
            $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'setting/staf_delete';
            $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
        } else {
            $this->session->set_flashdata('error', lang('You can not delete your self or SuperAdmin'));
            redirect('errors/access_denied');
        }
    }
    public function staf_add()
    {
        if (isPost()) {
            if ($this->session->role == "superadmin") {
                if (is_admin_exists(strtolower($_POST['email']))) {
                    $this->session->set_flashdata('error', lang('Email address exists please provide unique email address'));
                } else {
                    $_POST['email'] = strtolower($_POST['email']);

                    $username = preg_replace("/[^a-zA-Z0-9\s]/", "", $_POST['firstname']);
                    $_POST['username'] = str_replace(' ', '', $username);

                    if (username_taken($_POST['username'])) {
                        $_POST['username'] = str_replace(' ', '', $username) . rand(100, 999);
                    }

                    if (empty($_POST['password'])) {
                        $password = random_str('alphanum', '10');
                        $_POST['password'] = password_hash($password, PASSWORD_DEFAULT);
                    } else {
                        $password = $_POST['password'];
                        $_POST['password'] = password_hash($_POST['password'], PASSWORD_DEFAULT);
                    }
                    $_POST['companyid'] = $this->companyid;
                    $this->db = $this->load->database('default', true);
                    $this->db->insert('a_admin', $_POST);
                    $this->session->set_flashdata('success', lang('New Staf Has been Inserted'));
                    $id = $this->db->insert_id();
                    $_POST["password"] = $password;
                    $_POST['language'] = 'dutch';
                    $this->send_email_login($_POST);
                }

                redirect('admin/setting/staf');
            } else {
                $this->session->set_flashdata('error', lang('You do not have access to add new Staff'));
                redirect('admin/setting/staf_add/');
            }
        }
        if ($this->session->role != "superadmin") {
            $this->session->set_flashdata('error', lang('Your role is not allowed you to add new staf'));
            redirect('admin/setting/staf');
            exit;
        }
        $this->data['password'] = random_str('alphanum', '10');
        $this->data['staf'] = $this->Admin_model->getStaf($this->session->id);
        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'setting/staf_add';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }

    public function send_email_login($d)
    {
        if ($this->data['setting']->smtp_type == 'smtp') {
            $config = array(
            'protocol' => 'smtp',
            'smtp_host' => $this->data['setting']->smtp_host,
            'smtp_port' => $this->data['setting']->smtp_port,
            'smtp_user' => $this->data['setting']->smtp_user,
            'smtp_pass' => $this->encryption->decrypt($this->data['setting']->smtp_pass),
            'mailtype' => 'html',
            'charset' => 'utf-8',
            'starttls' => true,
            'wordwrap' => true,
            );
        } else {
            $config['protocol'] = 'sendmail';
            $config['mailpath'] = '/usr/sbin/sendmail';
            $config['mailtype'] = 'html';
            $config['charset'] = 'utf-8';
            $config['wordwrap'] = true;
        }
        $this->email->clear(true);
        $this->email->initialize($config);
        //$this->data['info'] = (object) array('email' => $d['email'], 'password' => $d['password'], 'name' => $d['firstname'] . ' ' . $d['lastname']);
        $body = getMailContent('admin_welcome_email', $d['language'], $this->companyid);
        $body = str_replace('{$name}', $d['firstname'] . ' ' . $d['lastname'], $body);
        $body = str_replace('{$email}', $d['email'], $body);
        $body = str_replace('{$password}', $d['password'], $body);
        $body = str_replace('{$base_url}', url_to_domain(base_url()), $body);

        $this->email->set_newline("\r\n");
        $this->email->from($this->data['setting']->smtp_sender, $this->data['setting']->smtp_name);
        $this->email->to(strtolower(trim($d['email'])));
        //$this->email->bcc('lainard@gmail.com');
        $this->email->subject(getSubject('admin_welcome_email', $d['language'], $this->companyid));
        $this->email->message($body);
        if ($this->email->send()) {
            //mail('simson@exocom.be', 'email sent', 'email really sent');
        } else {
            //mail('simson@exocom.be', 'email not sent', 'email really not sent.' . $this->email->print_debugger());
            //echo json_encode(array('result' => false, 'error' => $this->email->print_debugger()));
        }
    }
    public function staf_edit()
    {
        $id = $this->uri->segment(4);
        if (isPost()) {
            if (!empty($_POST['password'])) {
                $_POST['password'] = password_hash(trim($_POST['password']), PASSWORD_DEFAULT);
            } else {
                unset($_POST['password']);
            }
            $config['upload_path'] = FCPATH . 'assets/img/staf/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['encrypt_name'] = true;
            $this->load->library('upload', $config);

            if ($this->session->id == $_POST['id']) {
                $adminid = $_POST['id'];
                unset($_POST['id']);

                if ($this->upload->do_upload('picture')) {
                    $_POST['picture'] = $this->upload->data('file_name');
                }

                $this->Admin_model->updateAdminbyId($adminid, $_POST);

                $this->session->set_flashdata('success', lang('Information has been updated, do you wish to see') . ' <a href="' . base_url() . 'admin/setting/staf">' . lang('list of the Staf') . '?</a>');
            /*if (!empty($_POST['picture'])) {*/
            } else {
                if ($this->session->role == "superadmin") {
                    $adminid = $_POST['id'];
                    unset($_POST['id']);
                    if ($this->upload->do_upload('picture')) {
                        $_POST['picture'] = $this->upload->data('file_name');
                    }

                    $this->Admin_model->updateAdminbyId($adminid, $_POST);
                    $this->session->set_flashdata('success', lang('Information has been updated, do you wish to see') . ' <a href="' . base_url() . 'admin/setting/staf">' . lang('list of the Staf') . '?</a>');
                } else {
                    $this->session->set_flashdata('error', lang('You do not have access to edit someone else profile'));
                }
            }
        }
        $this->data['staf'] = $this->Admin_model->getStaf($id);
        if (empty($this->data['staf'])) {
            redirect('errors/access_denied');
        }
        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'setting/staf_edit';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }

    public function support_department()
    {
        $this->data['dtt'] = 'department.js?version=1.2';
        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'setting/helpdesk_dept';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }

    public function department_setting()
    {
        $this->data['helpdesk'] = $this->Admin_model->getDepartment($this->uri->segment(4));
        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'setting/helpdesk_dept_edit';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }
    public function department_delete()
    {
        if ($this->Admin_model->checkAssignedTicket($this->uri->segment(4))) {
            $this->session->set_flashdata('error', lang('You can not delete this department because there are tickets assigned to it'));
        } else {
            $this->db->where('id', $this->uri->segment(4));
            $this->db->where('companyid', $this->companyid);
            $this->db->delete('a_helpdesk_department');

            if ($this->db->affected_rows()>0) {
                $this->session->set_flashdata('success', lang('Department has been deleted as requested'));
            } else {
                $this->session->set_flashdata('success', lang('Access Denied'));
            }
        }
        redirect('admin/setting/support_department');
    }
    public function add_department()
    {
        if (isPost()) {
            $this->db->insert('a_helpdesk_department', $_POST);
            $this->session->set_flashdata('success', lang('Department has been created'));
        }
        //Array ( [name] => Customer [email] => lainard@gmail.com [smtp_type] => mail [username] => demo@exocom.be [password] => demo1234 [host] => mydev.united-telecom.be [port] => 112 )
        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'setting/helpdesk_dept_add';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }

    public function edit_department()
    {
        if (isPost()) {
            $this->db->where('id', $this->uri->segment(4));
            $this->db->where('companyid', $this->companyid);
            $this->db->update('a_helpdesk_department', $_POST);
            $this->session->set_flashdata('success', lang('Department has been updated'));
        }
        //Array ( [name] => Customer [email] => lainard@gmail.com [smtp_type] => mail [username] => demo@exocom.be [password] => demo1234 [host] => mydev.united-telecom.be [port] => 112 )
        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'setting/helpdesk_dept_edit';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }
    public function roles()
    {
        $this->data['dtt'] = 'roles.js?version=1.0';
        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'setting/roles';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }
    public function add_role()
    {
        if ($_POST['copy'] == "none") {
            $this->db->insert('a_role', array('companyid' => $_POST['companyid'], 'description' => $_POST['name'], 'name' => strtolower(str_replace(' ', '_', $_POST['name']))));
            if ($this->db->insert_id()) {
                $this->session->set_flashdata('success', 'Role has been added together with it permissions');
            }
        } else {
            $roles = $this->Admin_model->getAdminRoles(trim($_POST['copy']), $this->companyid, strtolower(str_replace(' ', '_', $_POST['name'])));

            $this->db->insert('a_role', array('companyid' => $_POST['companyid'], 'description' => $_POST['name'], 'name' => strtolower(str_replace(' ', '_', $_POST['name']))));
            if ($this->db->insert_id()) {
                $this->session->set_flashdata('success', 'Role has been added together with it permissions');
            }
            if ($roles) {
                $this->db->insert_batch('a_admin_roles', $roles);
            }
        }
        redirect('admin/setting/roles');
    }

    public function edit_role()
    {
        $this->data['role'] = $this->Admin_model->getRole($this->uri->segment(4));
        if (!$this->data['role']) {
            $this->session->set_flashdata('error', lang('Role not found'));
            redirect('admin/setting/roles');
        }
        if ($this->data['role']->name == "superadmin") {
            $this->session->set_flashdata('error', lang('Superadmin role can not be edited nor to be deleted'));
            redirect('admin/setting/roles');
            exit;
        }
        if (isPost()) {
            $this->db->query("delete from a_admin_roles where rolename=? and companyid=?", array($_POST['rolename'], $this->companyid));

            foreach ($_POST['permissions'] as $key => $row) {
                if ($row == "on") {
                    $this->db->insert('a_admin_roles', array('companyid' => $this->companyid, 'rolename' => $_POST['rolename'], 'perms' => $key));
                }
            }
            $this->session->set_flashdata('success', lang('Role permissions has been updated'));
        }

        $perms = $this->Admin_model->getRolePermission($this->data['role']->name);
        $count = count($perms);
        $chunk = $count / 3;
        $this->data['permissions'] = array_chunk($perms, round($chunk));
        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'setting/role_edit';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }

    public function delete_role()
    {
        $role = $this->Admin_model->getRole($this->uri->segment(4));
        $protect = array('superadmin', 'administratie', 'sales', 'finance');

        if (in_array($role->name, $protect)) {
            $this->session->set_flashdata('error', lang('Builtin Role can not be edited nor to be deleted'));
        } else {
            $this->db->query("delete from a_admin_roles where rolename=? and companyid=?", array($role->name, $this->companyid));
            $this->db->query("delete from a_role where id=? and companyid=?", array($this->uri->segment(4), $this->companyid));
            $this->session->set_flashdata('success', lang('Role ') . $role->name . lang(' has been deleted successfully'));
        }
        redirect('admin/setting/roles');
    }
    public function add_superadmin()
    {
        $this->load->database('default', true);
        if ($this->session->master) {
            $_POST['username'] = str_replace(' ', '', $_POST['firstname'] . rand(1000, 9999));
            $_POST['password'] = password_hash($_POST['password'], PASSWORD_DEFAULT);
            $_POST['uuid'] = gen_uuid();
            $this->db->insert('a_admin', $_POST);
            if ($this->db->insert_id() > 0) {
                $this->session->set_flashdata('success', lang('Superadmin user ') . $_POST['firstname'] . lang(' has been added successfully'));
            } else {
                $this->session->set_flashdata('error', lang('Superadmin user ') . $_POST['firstname'] . lang(' was not added'));
            }
        }
    }
    public function companies()
    {
        if ($this->session->master != 1) {
            $this->session->set_flashdata('error', 'You do not have access for this feature');
            redirect('admin');
            exit;
        }
        $this->load->helper('string');
        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'setting/companies';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }

    public function company_products()
    {
        if ($this->session->master != 1) {
            $this->session->set_flashdata('error', 'You do not have access for this feature');
            redirect('admin');
            exit;
        }
        $this->data['platforms'] = $this->Admin_model->getPlatformList($this->uri->segment(4));
        $this->data['company'] = $this->Admin_model->GetCompany($this->uri->segment(4));
        $this->data['supported_languages'] = explode(',', $this->data['setting']->supported_language);

        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'setting/company_products';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }
    public function save_mycompany_config()
    {
        $companyid = $this->companyid;
        $mvno['RangeNumber'] = $_POST["RangeNumber"];
        $mvno['companyid'] = $_POST["companyid"];
        $mvno['companyname'] = $_POST["companyname"];
        $mvno['email_notification'] = $_POST["email_notification"];
        $mvno['email_on_order'] = $_POST["email_on_order"];
        $mvno['email_portin_event'] = $_POST["email_portin_event"];
        //$mvno['footer_link'] = $_POST["footer_link"];
        unset($_POST["email_portin_event"]);
        unset($_POST["email_on_order"]);
        unset($_POST["RangeNumber"]);
        unset($_POST["companyid"]);
        unset($_POST["companyname"]);
        unset($_POST["email_notification"]);

        $configuration = $_POST;

        if (!empty($configuration['smtp_pass'])) {
            $configuration['smtp_pass'] = $this->encryption->encrypt($configuration['smtp_pass']);
        } else {
            unset($configuration['smtp_pass']);
        }
        $result2 = $this->Admin_model->UpdateMvno($companyid, $mvno);
        $result1 = $this->Admin_model->UpdateMvnoConfig($companyid, $configuration);
        sleep(4);
        if ($result1) {
            if ($result2) {
                echo json_encode(array('result' => true, 'data' => $configuration));
            } else {
                echo json_encode(array('result' => false));
            }
        } else {
            echo json_encode(array('result' => false));
        }
    }
    public function save_company_config()
    {
        if (!empty($this->uri->segment(4))) {
            if (!$this->session->master) {
                echo json_encode(array('result' => false, 'You have no access'));
                exit;
            }
            $companyid = $this->uri->segment(4);
        } else {
            $companyid = $this->session->cid;
        }



        $mvno['RangeNumber'] = $_POST["RangeNumber"];
        $mvno['companyid'] = $_POST["companyid"];
        $mvno['companyname'] = $_POST["companyname"];
        $mvno['email_notification'] = $_POST["email_notification"];

        unset($_POST["RangeNumber"]);
        unset($_POST["companyid"]);
        unset($_POST["companyname"]);
        unset($_POST["email_notification"]);
        $configuration = $_POST;
        if (!empty($configuration['smtp_pass'])) {
            $configuration['smtp_pass'] = $this->encryption->encrypt($configuration['smtp_pass']);
        } else {
            unset($configuration['smtp_pass']);
        }
        $result2 = $this->Admin_model->UpdateMvno($companyid, $mvno);
        $result1 = $this->Admin_model->UpdateMvnoConfig($companyid, $configuration);
        sleep(4);
        if ($result1) {
            if ($result2) {
                echo json_encode(array('result' => true, 'data' => $configuration));
            } else {
                echo json_encode(array('result' => false));
            }
        } else {
            echo json_encode(array('result' => false));
        }
    }

    public function send_mass_sms()
    {
        if (isPost()) {
            $message = $_POST['message'];
            $send_by = $this->session->firstname.' '.$this->session->lastname;
            $filter= json_encode($_POST);
            $batch = $this->db->query("select * from a_sms_log order by batchid desc limit 1");
            if ($batch->num_rows()>0) {
                $batchid = $batch->row()->batchid+1;
            } else {
                $batchid = 1000;
            }

            $this->db->select("a.companyid,a.msisdn as number,b.id as userid,'Queued' as status,'".$message."' as message,'".$send_by."' as send_by,'".$filter."' as filter_raw,'".$batchid."' as batchid");
            if ($_POST['status'] != 'All') {
                $this->db->where('c.status', $_POST['status']);
            }

            if (!empty($_POST['products'])) {
                $this->db->where_in('c.packageid', $_POST['products']);
            }

            if (!empty($_POST['postcodes'])) {
                $this->db->where_in('b.postcode', $_POST['postcode']);
            }
            $this->db->where('a.companyid', $this->companyid);
            $this->db->from('a_services_mobile a');
            $this->db->join('a_clients b', 'b.id = a.userid', 'left');
            $this->db->join('a_services c', 'c.id = a.serviceid', 'left');
            $this->db->group_by("a.userid");
            $q = $this->db->get();
            echo $this->db->last_query();
            if ($q->num_rows()) {
                $this->db->insert_batch('a_sms_log', $q->result_array());
            }
            $this->session->set_flashdata('success', 'Please check the status of the message below');
            redirect('admin/setting/send_mass_sms/status/'.$batchid);
        }
        $this->data['company_config'] = globofix($this->session->cid);
        $this->data['company'] = $this->Admin_model->GetCompany($this->session->cid);

        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'setting/bulksms.php';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }

    public function cancel_sms_batch()
    {
        $this->db->where('status !=', 'Sent');
        $this->db->where('companyid', $this->companyid);
        $this->db->where('batchid', $_POST['batchid']);
        $this->db->update('a_sms_log', array('status' => 'Cancelled'));
        echo json_encode(array('result' => $this->db->affected_rows()));
    }
    public function company_detail()
    {
        if ($this->session->master != 1) {
            $this->session->set_flashdata('error', 'You do not have access for this feature');
            redirect('admin');
            exit;
        }



        $this->load->helper('string');
        $this->load->database('default', true);

        $c = $this->db->query("select * from a_mvno where companyid=?", array($this->uri->segment(4)));

        if ($c->num_rows()>0) {
            $this->session->set_userdata('redirect_onlogin', $c->row()->portal_url.'admin/setting/configuration');
        }

        if ($this->session->master) {
            $s = $this->db->query('select * from a_admin where id=?', array($this->session->id));
            $admin = $s->row_array();
            $random = random_string('alnum', 16);
            $adminid = $this->session->id;
            $comp = $this->db->query('select * from a_mvno where id=?', array($c->row()->id));
            $q = $this->db->query("select * from a_admin where companyid=? and email=?", array($comp->row()->companyid, $this->session->email));
            if ($q->num_rows() > 0) {
                $this->db->query("update a_admin set master_code=? where id=?", array($random, $q->row()->id));
                //$this->session->sess_destroy();
                header('Location: ' . $comp->row()->portal_url . 'admin/auth/switchcompany/' . $random);
            } else {
                unset($admin['logged']);
                $admin['master_code'] = $random;
                $admin['username'] = $admin['firstname'] . rand(1000, 9999);
                $admin['companyid'] = $comp->row()->companyid;
                unset($admin['id']);
                $this->db->insert('a_admin', $admin);
                echo $this->db->insert_id();
                header('Location: '.$comp->row()->portal_url.'admin/auth/switchcompany/'.$random);
            }
        }
    }
    public function get_addon()
    {
        $q = $this->db->query("select * from a_products_mobile_bundles where id=?", array($this->uri->segment(4)));

        foreach ($q->row_array() as $key => $val) {
            $res[][$key] = $val;
        }

        echo json_encode($q->row_array());
    }

    public function delete_addon()
    {
        echo json_encode(array('result'=> false));
    }
    public function get_company_productid()
    {
        $checked = array('500');
        $q = $this->db->query("select * from a_products where id=?", array($this->uri->segment(4)));

        $lang = $this->db->query("select * from a_products_language where packageid=?", array($this->uri->segment(4)));
        if (!empty($q->row()->notes)) {
            $checked = explode(',', $q->row()->notes);
        }

        $html = "";

        $html .= '<label for="">' . lang('Platform Bundles') . '</label>';
        foreach (getTarifs($q->row()->companyid) as $row) {
            $html .= ' <div class="custom-control">';

            if (in_array($row['id'], $checked)) {
                $html .= '<input type="checkbox" name="notes[]" value="' . $row['id'] . '" checked>';
            } else {
                $html .= ' <input type="checkbox" name="notes[]" value="' . $row['id'] . '">';
            }

            $html .= '<label class="control-label" for="customCheck1"> ' . $row['name'] . '</label>
    						</div>';
        }

        $res = $q->row_array();

        if ($lang->num_rows()>0) {
            $res['languages'] = $lang->result_array();
        }
        $res['html'] = $html;
        echo json_encode($res);
    }

    public function getbundles()
    {
        $html = "";
        $companyid = $this->uri->segment(4);
        $html .= '<label for="">' . lang('Platform Bundles') . '</label>';
        foreach (getTarifs($companyid) as $row) {
            $html .= ' <div class="custom-control">';

            $html .= '<input type="checkbox" name="notes[]" value="' . $row['id'] . '">';

            $html .= '<label class="control-label"> ' . $row['name'] . '</label>
    						</div>';
        }

        $res['html'] = $html;
        echo json_encode($res);
    }
    public function add_product()
    {
        if ($this->session->master != 1) {
            $this->session->set_flashdata('error', 'You do not have access for this feature');
            redirect('admin');
            exit;
        }
        $id = $this->uri->segment(4);
        if (!empty($_POST['notes'])) {
            $_POST['notes'] = implode(',', $_POST['notes']);
        } else {
            $_POST['notes'] = '';
        }

        $_POST['recurring_subtotal'] = exvat4($_POST['recurring_taxrate'], $_POST['recurring_total']);
        $_POST['recurring_tax'] = vat4($_POST['recurring_taxrate'], $_POST['recurring_total']);

        $this->db->insert('a_products', $_POST);

        if ($this->db->affected_rows() > 0) {
            $this->session->set_flashdata('success', 'Product has been ceated');
        } else {
            $this->session->set_flashdata('error', 'Product  failed to be created');
        }
        redirect('admin/setting/company_products/' . $_POST['companyid']);
    }
    public function edit_product()
    {
        $this->data['setting'] = globofix($this->uri->segment(4));
        if ($this->session->master != 1) {
            $this->session->set_flashdata('error', 'You do not have access for this feature');
            redirect('admin');
            exit;
        }


        $id = $this->uri->segment(4);
        if (!empty($_POST['notes'])) {
            $_POST['notes'] = implode(',', $_POST['notes']);
        } else {
            $_POST['notes'] = '';
        }

        $_POST['recurring_subtotal'] = exvat4($_POST['recurring_taxrate'], $_POST['recurring_total']);
        $_POST['recurring_tax'] = vat4($_POST['recurring_taxrate'], $_POST['recurring_total']);
        $languages = $_POST['lang'];
        unset($_POST['lang']);
        foreach ($languages as $key => $lang) {
            $this->db->query("update a_products_language set name=? where language=? and packageid=?", array($lang, $key, $_POST['id']));
        }
        $this->db->where('id', $_POST['id']);
        unset($_POST['id']);
        $this->db->update('a_products', $_POST);

        if ($this->db->affected_rows() > 0) {
            $this->session->set_flashdata('success', 'Product has been updated');
        } else {
            $this->session->set_flashdata('error', 'Product  failed to be updated');
        }
        redirect('admin/setting/company_products/' . $_POST['companyid']);
    }
    public function gen_webserver()
    {
        if ($this->session->master != 1) {
            $this->session->set_flashdata('error', 'You do not have access for this feature');
            redirect('admin');
            exit;
        }
        $id = $this->uri->segment(4);
        $company = $this->db->query("select * from a_mvno where id=?", array($id));
        //print_r($company->row());
        if ($company->num_rows() > 0) {
            $this->db->query("update a_mvno set apache=? where id=?", array('2', $id));
            $domain = url_to_domain($company->row()->portal_url);


            //$ip = gethostbyname('delta.united-telecom.nl');

            $config =  '<VirtualHost *:80>
ServerAdmin webmaster@localhost
DocumentRoot /home/mvno_dev/public_html
Options -Indexes
ServerName '. $domain.'
LogFormat "%v %h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\"" cplus
ServerSignature On
LogLevel debug
</VirtualHost>';

            file_put_contents(APPPATH.'server/'.$domain.'.conf', $config);
            $this->session->set_flashdata('success', 'Domain configuration on apache has been queued, please change dns A record of ' . $domain . ' to ' . $id);
        } else {
            $this->session->set_flashdata('error', 'Company not found');
        }

        redirect('admin/setting/companies');
    }

    public function company_product_options()
    {
        if ($this->session->master != 1) {
            $this->session->set_flashdata('error', 'You do not have access for this feature');
            redirect('admin');
            exit;
        }
        $this->data['company'] = $this->Admin_model->GetCompany($this->uri->segment(4));

        $this->load->helper('string');
        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'setting/company_addons';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }
    public function add_companies()
    {
        $companyid = $_POST['companyid'];
        $invoicing = $_POST['mage_invoicing'];
        $whmcs_ticket = $_POST['whmcs_ticket'];
        $companyname = $_POST['companyname'];
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select * from a_mvno where companyid=?", array($_POST['companyid']));

        if ($q->num_rows() > 0) {
            $this->session->set_flashdata('error', 'Companyid ' . $_POST['companyid'] . 'already exist');
        } else {
            $this->db->insert('a_mvno', array('companyid' => $_POST['companyid'], 'companyname' => $_POST['companyname'], 'portal_url' => $_POST['company_url']));
            $a_configuration = array(
                array('companyid' => $companyid, 'name' => 'address', 'val' => 'Wingepark 5B
              3110
              Rotselar'),
                array('companyid' => $companyid, 'name' => 'bank_acc', 'val' => 'BE68 3631 6950 6934'),
                array('companyid' => $companyid, 'name' => 'bank_swift', 'val' => 'BBRUBEBB'),
                array('companyid' => $companyid, 'name' => 'companyid', 'val' => '53'),
                array('companyid' => $companyid, 'name' => 'companyname', 'val' => 'MVNO  Mobiel'),
                array('companyid' => $companyid, 'name' => 'create_magebo_bundle', 'val' => '1'),
                array('companyid' => $companyid, 'name' => 'creditnotenum', 'val' => '20170014'),
                array('companyid' => $companyid, 'name' => 'currency', 'val' => '€'),
                array('companyid' => $companyid, 'name' => 'creditnote_prefix', 'val' => ''),
                array('companyid' => $companyid, 'name' => 'default_paymentmethod', 'val' => 'directdebit'),
                array('companyid' => $companyid, 'name' => 'default_pin', 'val' => '0000'),
                array('companyid' => $companyid, 'name' => 'default_theme', 'val' => 'clear'),
                array('companyid' => $companyid, 'name' => 'DOC_PATH', 'val' => '/home/mvno/documents/'),
                array('companyid' => $companyid, 'name' => 'email_footer', 'val' => 'Pareteum'),
                array('companyid' => $companyid, 'name' => 'email_notification', 'val' => ''),
                array('companyid' => $companyid, 'name' => 'enable_pusher', 'val' => null),
                array('companyid' => $companyid, 'name' => 'footer_link', 'val' => '<p style="color:white">
<a href="tel:003211111111">+32 11 11 11 11</a> -
<a href="mailto:mvno-portal@pareteum.cloud">mvno-portal@pareteum.cloud</a> -
<a href="https://pareteum.cloud">www.pareteum.cloud</a>
<Br>
<small>Let op: aan de op deze pagina\'s weergegeven informatie kunt u geen rechten ontlenen</small></p>'),
                array('companyid' => $companyid, 'name' => 'helpdesk_default_deptid', 'val' => '1'),
                array('companyid' => $companyid, 'name' => 'invoicenum', 'val' => '2018000023'),
                array('companyid' => $companyid, 'name' => 'invoice_footer', 'val' => 'efa6ffa870df9762701c3806c2347000.png'),
                array('companyid' => $companyid, 'name' => 'invoice_logo', 'val' => '893195a7dabf3dd10514f921f9eff992.png'),
                array('companyid' => $companyid, 'name' => 'invoice_prefix', 'val' => ''),
                array('companyid' => $companyid, 'name' => 'kyc', 'val' => '0'),
                array('companyid' => $companyid, 'name' => 'licences', 'val' => '9ca592d908022d14cce986e40c752df5'),
                array('companyid' => $companyid, 'name' => 'logo_site', 'val' => 'https://mijnmobiel.yoursite.nl/assets/img/logo_yoursitepng'),
                array('companyid' => $companyid, 'name' => 'mage_invoicing', 'val' => $_POST['mage_invoicing']),
                array('companyid' => $companyid, 'name' => 'maintenance', 'val' => 'no'),
                array('companyid' => $companyid, 'name' => 'multilang', 'val' => '0'),
                array('companyid' => $companyid, 'name' => 'navbar_bottom_color', 'val' => 'navbar_top_color'),
                array('companyid' => $companyid, 'name' => 'navbar_top_color', 'val' => 'navbar navbar-expand-lg navbar-dark bg-primary fixed-top'),
                array('companyid' => $companyid, 'name' => 'online_payment', 'val' => 'no'),
                array('companyid' => $companyid, 'name' => 'phonenumber', 'val' => '0118 225 500'),
                array('companyid' => $companyid, 'name' => 'quotenum', 'val' => '300044'),
                array('companyid' => $companyid, 'name' => 'quote_prefix', 'val' => ''),
                array('companyid' => $companyid, 'name' => 'rabbit_event_queue', 'val' => 'CCMobile'),
                array('companyid' => $companyid, 'name' => 'rabbit_event_routingkey', 'val' => 'Arta.Events.Reseller.Subscription.#.14'),
                array('companyid' => $companyid, 'name' => 'rabbit_host', 'val' => '10.10.10.104'),
                array('companyid' => $companyid, 'name' => 'rabbit_password', 'val' => 'un!t3d'),
                array('companyid' => $companyid, 'name' => 'rabbit_port', 'val' => '5672'),
                array('companyid' => $companyid, 'name' => 'rabbit_user', 'val' => 'admin'),
                array('companyid' => $companyid, 'name' => 'rabbit_vhost', 'val' => '/'),
                array('companyid' => $companyid, 'name' => 'sftp_bank', 'val' => '/home/sftpArtilium-P/VanArtaNaarDelta/Pain008/'),
                array('companyid' => $companyid, 'name' => 'sftp_host', 'val' => 'sftp.yoursite.nl'),
                array('companyid' => $companyid, 'name' => 'sftp_key', 'val' => '/home/mvno/.ssh/delta.key'),
                array('companyid' => $companyid, 'name' => 'sftp_key_password', 'val' => ''),
                array('companyid' => $companyid, 'name' => 'sftp_password', 'val' => ''),
                array('companyid' => $companyid, 'name' => 'sftp_path', 'val' => '/home/sftpArtilium-P/VanDeltaNaarArta/CAMT053/'),
                array('companyid' => $companyid, 'name' => 'sftp_port', 'val' => '22'),
                array('companyid' => $companyid, 'name' => 'sftp_pub', 'val' => '/home/mvno/.ssh/delta.pub'),
                array('companyid' => $companyid, 'name' => 'sftp_user', 'val' => 'sftpArtilium-P'),
                array('companyid' => $companyid, 'name' => 'smtp_host', 'val' => 'ssl://smtp.googlemail.com'),
                array('companyid' => $companyid, 'name' => 'smtp_name', 'val' => 'Company Administratie'),
                array('companyid' => $companyid, 'name' => 'smtp_pass', 'val' => ''),
                array('companyid' => $companyid, 'name' => 'smtp_port', 'val' => '465'),
                array('companyid' => $companyid, 'name' => 'smtp_sender', 'val' => 'mobiel@united-telecom.nl'),
                array('companyid' => $companyid, 'name' => 'smtp_type', 'val' => 'mail'),
                array('companyid' => $companyid, 'name' => 'smtp_user', 'val' => 'easy_switch@united-telecom.be'),
                array('companyid' => $companyid, 'name' => 'specific_pricing', 'val' => '0'),
                array('companyid' => $companyid, 'name' => 'subscription_invoice_type', 'val' => 'Subscription Next Month'),
                array('companyid' => $companyid, 'name' => 'supported_language', 'val' => 'dutch,english'),
                array('companyid' => $companyid, 'name' => 'taxrate', 'val' => '21'),
                array('companyid' => $companyid, 'name' => 'trademark_text', 'val' => '©2018 CompayName. Alle rechten voorbehouden.'),
                array('companyid' => $companyid, 'name' => 'vat', 'val' => ''),
                array('companyid' => $companyid, 'name' => 'whmcs_ticket', 'val' => $_POST['whmcs_ticket']),
            );
            $this->db->insert_batch('a_configuration', $a_configuration);
            $a_helpdesk_category = array(
                array('companyid' => $companyid, 'name' => 'Agent'),
                array('companyid' => $companyid, 'name' => 'Contact'),
                array('companyid' => $companyid, 'name' => 'DataBundle'),
                array('companyid' => $companyid, 'name' => 'Finance'),
                array('companyid' => $companyid, 'name' => 'Lead'),
                array('companyid' => $companyid, 'name' => 'Number Portability'),
                array('companyid' => $companyid, 'name' => 'Order'),
                array('companyid' => $companyid, 'name' => 'Others'),
                array('companyid' => $companyid, 'name' => 'Proefbilling'),
                array('companyid' => $companyid, 'name' => 'Rating'),
                array('companyid' => $companyid, 'name' => 'Risk'),
                array('companyid' => $companyid, 'name' => 'Settlement'),
                array('companyid' => $companyid, 'name' => 'Simcard Replacement'),
                array('companyid' => $companyid, 'name' => 'Technical'),
                array('companyid' => $companyid, 'name' => 'VoiceBundle'),
                array('companyid' => $companyid, 'name' => 'Supplier'),
            );
            $a_helpdesk_department = array(
                array('companyid' => $companyid, 'name' => 'Support Helpdesk', 'email' => '', 'pop3_host' => '', 'pop3_user' => '', 'pop3_password' => '', 'pop3_port' => ''),
                array('companyid' => $companyid, 'name' => 'Finance', 'email' => '', 'pop3_host' => '', 'pop3_user' => '', 'pop3_password' => '', 'pop3_port' => ''),
                array('companyid' => $companyid, 'name' => 'Sales Department', 'email' => '', 'pop3_host' => '', 'pop3_user' => '', 'pop3_password' => '', 'pop3_port' => ''),
                array('companyid' => $companyid, 'name' => 'Administration', 'email' => '', 'pop3_host' => '', 'pop3_user' => '', 'pop3_password' => '', 'pop3_port' => ''),
            );
            $a_role = array(
                array('companyid' => $companyid, 'name' => 'superadmin', 'description' => 'SuperAdmin All Permissions'),
                array('companyid' => $companyid, 'name' => 'sales', 'description' => 'Sales Role'),
                array('companyid' => $companyid, 'name' => 'finance', 'description' => 'Finance Role'),
                array('companyid' => $companyid, 'name' => 'shop', 'description' => 'Shop Role'),
                array('companyid' => $companyid, 'name' => 'administration', 'description' => 'Administration'),
            );
            $a_products_group = array('companyid' => $companyid, 'name' => 'Default Postpaid');
            $emails = $this->db->query("select '".$companyid."' as companyid,
            name,
            subject,
            variables,
            description,
            language,
            body,
            default_body,
            date_created,
            date_modified,
            last_modifier,
            attachments,
            status from a_email_templates where companyid=54 order by name asc");

            log_message('error', $this->db->last_query());

            $pdfs =  $this->db->query("select
description,
vars,
name,
package_gid,
body,
'".$companyid."' as companyid,
language,
font,
font_size from a_products_pdf where companyid=54 order by name asc");
            $this->db->insert_batch('a_email_templates', $emails->result_array());
            $this->db->insert_batch('a_products_pdf', $pdfs->result_array());

            $this->db->insert_batch("a_helpdesk_category", $a_helpdesk_category);
            $this->db->insert_batch("a_helpdesk_department", $a_helpdesk_department);
            $this->db->insert_batch("a_role", $a_role);
            $this->db->insert("a_products_group", $a_products_group);
            $reseller = 'res'.rand(1000000, 99999999).'@pareteum.com';
            $superadmin = 'spa_'.rand(1000000, 99999999).'@pareteum.com';
            $password = 'cHaNge'.rand(1000000, 99999999);
            $hash = password_hash($password, PASSWORD_DEFAULT);
            $this->db->query("INSERT INTO `a_clients_agents` (`id`, `ContactType2Id`, `status`, `companyid`, `agent`, `contact_name`, `address1`, `postcode`, `city`, `email`, `resetcode`, `password`, `phonenumber`, `language`, `country`, `comission_type`, `comission_value`, `lastseen`, `ip`, `last_month_earning`, `isdefault`) VALUES(null, null, 'Active', '".$companyid."', 'Default Reseller', 'Foo', 'Bar', '12345', 'London', '".$reseller."', null, '".$hash."', '071234567', 'english', 'UK', 'Percentage', '0.00', null, null, '0.00', '0')");


            $this->db->query("INSERT INTO `a_admin` (`id`, `username`, `companyid`, `firstname`, `lastname`, `address1`, `postcode`, `city`, `country`, `email`, `password`, `resetcode`, `master_code`, `status`, `datecreated`, `role`, `picture`, `language`, `phonenumber`, `nationalnr`, `theme`, `lastseen`, `ip`, `master`, `email_notification`, `order_notification`, `ticket_notification`, `isagent`, `request_export_portinlist`, `disable_dashboard_notification`, `accept_tos`) VALUES (null, 'spa_".rand(100000000, 9999999999)."', '100', 'Simson', 'Lai', 'Wingepark, 5B', '3110', 'Rotselaar', 'BE', '".$superadmin."', '".$hash."', '5OoWxr5OEy', 'd2orXMAKFvkTyDuW', 'Active', '".date('Y-m-d')."', 'superadmin', 'nopic.png, 'english', '01234567890', '', 'clear', '1564768203', '127.0.0.1', '0', '0', '0', '', '0', '0', '0', '0')");
            $this->session->set_flashdata('success', 'Companyid ' . $_POST['companyid'] . ' has been setup, Super admin has been also created username: '.$rand.' password: '.$password);
            mail('mail@simson.one', 'super admin account for companyid:'.$companyid, "User: ".$superadmin."\nPassword:".$password);
        }

        redirect('admin/setting/companies');
    }

    public function edit_companies()
    {
        print_r($_POST);
    }



    public function key_add()
    {
        if (isset($_POST['sso'])) {
            $sso = $_POST['sso'];
        } else {
            $sso = "off";
        }
        if ($this->Admin_model->getKeys($this->companyid) <= 7) {
            $key = $this->Admin_model->generateNewKey($this->session->companyid, $sso, $this->session->firstname . ' ' . $this->session->lastname);
            $this->session->set_flashdata('success', 'new key has been generated :' . $key . ' ' . $this->Admin_model->getKeys($this->data['setting']->companyid));
        } else {
            $this->session->set_flashdata('error', lang('You have reach your maximum keys of 8'));
        }

        redirect('admin/setting/api');
    }

    public function ip_add()
    {
        if (isPost()) {
            if ($this->Admin_model->getIps() <= 7) {
                if ($this->Admin_model->checkIP(trim(strtolower($_POST['ip'])))) {
                    $this->session->set_flashdata('error', 'You already have this ip:' . $_POST['ip'] . ' on the list');
                } else {
                    $_POST['companyid'] = $this->companyid;
                    if ($this->Admin_model->insertIP($_POST)) {
                        $this->session->set_flashdata('success', lang('new IP has been inserted :') . trim($_POST['ip']));
                    } else {
                        $this->session->set_flashdata('error', lang('there was an error inserting your new IP'));
                    }
                }
            } else {
                $this->session->set_flashdata('error', lang('You have reach your maximum IP of 8'));
            }
        } else {
            $this->session->set_flashdata('error', lang('Wrong Method'));
        }

        redirect('admin/setting/api');
    }

    public function delete_key()
    {
        $del = $this->Admin_model->delete_key($this->uri->segment(4));

        if ($del) {
            $this->session->set_flashdata('success', lang('Token key has been deleted'));
        } else {
            $this->session->set_flashdata('error', lang('Token key can not be deleted'));
        }

        //$this->session->set_flashdata('success', lang('API key has been deleted'));
        redirect('admin/setting/api');
    }

    public function delete_ip()
    {
        $del = $this->Admin_model->delete_ip($this->uri->segment(4));
        if ($del) {
            $this->session->set_flashdata('success', lang('Ip address has been deleted'));
        } else {
            $this->session->set_flashdata('error', lang('Ip address can not be deleted'));
        }

        redirect('admin/setting/api');
    }

    public function prep_database()
    {
        $this->db->query("ALTER TABLE `whmcs`.`tblclients` ADD COLUMN `mageboid` INT NULL AFTER pwresetexpiry");
    }

    public function disable_admin_account()
    {
        $id = $this->uri->segment(4);

        if ($id == $this->session->id) {
            $this->session->set_flashdata('success', lang('You can not Disable your own account'));
        } else {
            $this->db->where('companyid', $this->companyid);
            $this->db->where('id', $id);
            $this->db->update('a_admin', array('status' => 'InActive'));
            $this->session->set_flashdata('success', lang('Account has been enabled'));
        }

        redirect('admin/setting/staf');
    }

    public function enable_admin_account()
    {
        $id = $this->uri->segment(4);

        if ($id == $this->session->id) {
            $this->session->set_flashdata('success', lang('You can not enable your own account'));
        } else {
            $this->db->where('id', $id);
            $this->db->where('companyid', $this->companyid);
            $this->db->update('a_admin', array('status' => 'Active'));
            $this->session->set_flashdata('success', lang('Account has been enabled'));
        }

        redirect('admin/setting/staf');
    }
}
