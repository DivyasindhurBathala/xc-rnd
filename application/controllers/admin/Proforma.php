<?php
defined('BASEPATH') or exit('No direct script access allowed');
class ProformaController extends CI_Controller
{
    protected $data = [];

    public function __construct()
    {
        // Ensure you run parent constructor
        error_reporting(1);
        parent::__construct();
        $this->config->set_item('language', $this->session->userdata('language'));
        $this->companyid = $this->session->cid;
        if (file_exists(APPPATH.'language/'.$this->config->item('language').'/'.$this->companyid.'_admin_lang.php')) {
            // log_message('error', 'Using Custom language files');
            $this->lang->load($this->companyid.'_admin');
        } else {
            // log_message('error', APPPATH.'language/'.$this->config->item('language').'/'.$this->companyid.'_admin.php');
            $this->lang->load('admin');
        }
        $this->setProperty();
    }

    public function setProperty()
    {
        $this->data['setting'] = globo();
    }
}
class Proforma extends ProformaController
{
    public function __construct()
    {
        parent::__construct();
        /*
        $classes = get_class_methods($this);
        if ($this->session->email == "simson.parlindungan@united-telecom.be") {
        foreach ($classes as $class) {
            log_message('error', $class);
            if ($class != 'setProperty' && $class != 'get_instance' && $class != '__construct') {
                $this->db->insert('a_admin_permissions', array('folder' => 'admin', 'controller' => 'proforma', 'method' => $class));
            }
        }
        }
        */
        $this->load->model('Proforma_model');
    }

    public function index()
    {
        if (!empty($this->uri->segment(3))) {
            if ($this->uri->segment(3) == "success_delete") {
                $this->session->set_flashdata('success', 'your invoice has been deleted from the database');
            }
        }
        $this->data['dtt'] = 'proforma.js?version=2.5';
        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'proforma/list';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }

    public function view()
    {
        print_r($this->data['setting']->bank_acc);
    }

    public function detail()
    {
        $id = $this->uri->segment(4);
        if (!empty($this->uri->segment(5))) {
            if ($this->uri->segment(5) == "success_send") {
                $this->session->set_flashdata('success', 'your invoice has been sent to your customer');
            }
        }
        $this->data['invoice'] = $this->Proforma_model->getInvoice($id);
        $this->data['transactions'] = $this->Proforma_model->getTransactions($id);
        $this->data['client'] = $this->Proforma_model->getClient($this->data['invoice']['userid']);
        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'proforma/detail';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }
    public function detailbyNumber()
    {
        $id = $this->uri->segment(4);
        $invoicenum = $this->Proforma_model->getInvoiceidbyInvoicenum($id);
        if ($invoicenum) {
            print_r($invoicenum);
            redirect('admin/proforma/detail/' . $invoicenum['id']);
        }
    }
    public function create()
    {
        if (isPost()) {
            $lines = $_POST['line'];
            $tax = $_POST['taxrate'];
            if ($tax == 0) {
                $tax = 1;
            }
            $i['userid'] = $_POST['userid'];
            $i['date'] = date('Y-m-d');
            $i['duedate'] = $_POST['duedate'];
            foreach ($lines as $line) {
                if ($tax != 1) {
                    $pricetotal[] = includevat($tax, $line['total']);
                } else {
                    $pricetotal[] = $line['total'];
                }
            }

            $i['total'] = array_sum($pricetotal);
            if ($i != 1) {
                $i['subtotal'] = exvat($tax, $i['total']);
                $data['tax'] = $data['total'] - $i['subtotal'];
            } else {
                $i['subtotal'] = $i['total'];
                $i['tax'] = "0.00";
            }

            $invoiceid = $this->Proforma_model->insertInvoice($i);
            foreach ($lines as $line) {
                if (!empty($line['description'])) {
                    $items[] = array('price' => $line['pcs'], 'taxrate' => $_POST['taxrate'], 'invoiceid' => $invoiceid, 'description' => $line['description'], 'qty' => $line['qty'], 'amount' => $line['total']);
                }
                //qty,pcs,total,description
            }
            $this->db->insert_batch('invoiceitems', $items);
            redirect('admin/proforma/edit/' . $invoiceid);
        }
        $date = new DateTime(date('Y-m-d'));
        $date->modify("+14 day");
        $this->data['date'] = $date->format("Y-m-d");
        $id = $this->uri->segment(4);
        $this->data['client'] = $this->Proforma_model->getClient($id);
        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'proforma_add';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }
    public function edit()
    {
        if (isPost()) {
            //print_r($_POST);

            $this->Proforma_model->deleteInvoiceItems($_POST['invoiceid']);
            $lines = $_POST['line'];
            $tax = $_POST['taxrate'];
            if ($tax == 0) {
                $tax = 1;
            }
            $i['userid'] = $_POST['userid'];
            $i['date'] = date('Y-m-d');
            $i['duedate'] = $_POST['duedate'];
            foreach ($lines as $line) {
                if (!empty($line['description'])) {
                    if ($tax != 1) {
                        $pricetotal[] = includevat($tax, $line['total']);
                    } else {
                        $pricetotal[] = $line['total'];
                    }
                }
            }

            $i['total'] = array_sum($pricetotal);
            if ($i != 1) {
                $i['subtotal'] = exvat($tax, $i['total']);
                $i['tax'] = $i['total'] - $i['subtotal'];
            } else {
                $i['subtotal'] = $i['total'];
                $i['tax'] = "0.00";
            }

            $invoiceid = $_POST['invoiceid'];
            foreach ($lines as $line) {
                if (!empty($line['description'])) {
                    $items[] = array('price' => $line['pcs'], 'taxrate' => $_POST['taxrate'], 'invoiceid' => $invoiceid, 'description' => $line['description'], 'qty' => $line['qty'], 'amount' => $line['total']);
                }

                //qty,pcs,total,description
            }
            $this->db->insert_batch('invoiceitems', $items);
            //redirect('admin/proforma/edit/' . $invoiceid);
        }
        $id = $this->uri->segment(4);
        $invoice = $this->Proforma_model->getInvoice($id);
        $this->data['count'] = count($invoice['items']);
        $this->data['invoice'] = $invoice;
        $this->data['client'] = $this->Proforma_model->getClient($invoice['userid']);
        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'proforma_edit';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }

    public function publish()
    {
        $invoiceid = $_POST['invoiceid'];
        $invoice = $this->Proforma_model->getInvoice($invoiceid);
        $taxrate = $invoice['items']['0']['taxrate'];
        foreach ($invoice['items'] as $item) {
            if ($taxrate != 0) {
                $totalprice[] = includevat($item['taxrate'], $item['amount']);
                $this->Proforma_model->updateInvoiceItems($item['id'], array('amount' => includevat($item['taxrate'], $item['amount'])));
            } else {
                $totalprice[] = $item['amount'];
            }
        }
        /* update invoice duedate */
        $this->Proforma_model->updateInvoicePublish($invoiceid, $taxrate, array_sum($totalprice));
        $i['invoicenum'] = $this->data['setting']->proforma_prefix . getNextInvoicenum();
        $i['notes'] = ogm($i['invoicenum']);
        $i['ogm'] = preg_replace('/\D/', '', $i['notes']);
        $i['status'] = 'Unpaid';
        $i['duedate'] = $_POST['duedate'];
        //$i['linktoken'] = rand(100000000, 9999999999);
        $this->Proforma_model->updateInvoice($invoiceid, $i);
        logClient(array('userid' => $invoice['userid'], 'description' => $_SESSION['firstname'] . ' Create invoicenumber ' . $i['invoicenum']));
        $result['result'] = true;
        sleep(5);
        echo json_encode($result);
    }
    public function addpayment()
    {
        if (isPost()) {
            $i = $_POST;
            $invoice = $this->Proforma_model->getInvoice($_POST['invoiceid']);
            $client = $this->Proforma_model->getClient($invoice['userid']);
            $payment_balance = $this->Proforma_model->getPayments($_POST['invoiceid']);
            $balance = $invoice['total'] - $payment_balance;
            $i['amount'] = str_replace(',', '.', trim($_POST['amount']));
            $this->db->insert('payments', $i);
            logClient(array('userid' => $_POST['userid'], 'description' => $_SESSION['firstname'] . ' apply payment to invoicenumber ' . $invoice['invoicenum']));
            if ($this->db->insert_id() > 0) {
                if ($i['amount'] > $invoice['total']) {
                    $credit = $i['amount'] - $invoice['total'];
                    $new_credit = $credit + $client['credit'];
                    $this->Proforma_model->ChangeInvoiceStatus($invoice['id'], 'Paid');
                    $this->Proforma_model->UpdateClientCredit($invoice['userid'], $new_credit);
                    $this->session->set_flashdata('succes', 'Payment has been added into the system');
                } elseif ($i['amount'] == $invoice['total']) {
                    $this->session->set_flashdata('succes', 'Payment has been added into the system');
                    $this->Proforma_model->ChangeInvoiceStatus($invoice['id'], 'Paid');
                } else {
                    $this->session->set_flashdata('succes', 'Payment has been added into the system however the amount was not enough to set invoice as paid');
                }
            }
            redirect('admin/proforma/detail/' . $_POST['invoiceid']);
        }
    }

    public function sendReminder()
    {
        ini_set('display_errors', 0);
        $invoice = $this->Proforma_model->getInvoice($_POST['invoiceid']);
        $client = $this->Proforma_model->getClient($invoice['userid']);
        $res = $this->download_id($invoice['id']);
        if (!file_exists($this->data['setting']->DOC_PATH . 'invoices/' . $invoice['id'] . '.pdf')) {
            echo json_encode(array('result' => false));
        } else {
            if ($this->data['setting']->smtp_type == 'smtp') {
                $config = array(
                    'protocol' => 'smtp',
                    'smtp_host' => $this->data['setting']->smtp_host,
                    'smtp_port' => $this->data['setting']->smtp_port,
                    'smtp_user' => $this->data['setting']->smtp_user,
                    'smtp_pass' => $this->data['setting']->smtp_pass,
                    'mailtype' => 'html',
                    'charset' => 'utf-8',
                    'starttls' => true,
                    'wordwrap' => true,
                );
            } else {
                $config['protocol'] = 'sendmail';
                $config['mailpath'] = '/usr/sbin/sendmail';
                $config['mailtype'] = 'html';
                $config['charset'] = 'utf-8';
                $config['wordwrap'] = true;
            }

            $array['setting'] = $this->data['setting'];
            $array['name'] = $client->firstname . ' ' . $client->lastname;

            $reminder = "email/reminder5.php";

            $fu = (object) array_merge((array) $this->data['setting'], $invoice);

            $body = $this->load->view($reminder, $fu, true);
            $this->email->clear(true);
            $this->email->initialize($config);
            $this->email->set_newline("\r\n");
            $this->email->from($this->data['setting']->smtp_sender, $this->data['setting']->smtp_name);
            $this->email->to($client->email);
            $this->email->cc('info@exocom.be');
            $this->email->subject($this->data['setting']->companyname . " #" . $invoice['invoicenum']);
            $this->email->message($body);
            $this->email->attach($this->data['setting']->DOC_PATH . 'invoices/' . $invoice['id'] . '.pdf', 'attachment', 'Factuur_' . $invoice['invoicenum'] . '.pdf');
            //$this->email->attach('filename.pdf', 'attachment', 'report.pdf');
            if ($this->email->send()) {
                echo json_encode(array('result' => true));
            } else {
                echo print_r($this->email->print_debugger());
            }
        }
    }

    public function delete()
    {
        $this->db->query("delete from invoices where id=?", array($_POST['invoiceid']));
        $this->db->query("delete from invoiceitems where invoiceid=?", array($_POST['invoiceid']));
        echo json_encode(array('result' => true));
    }
    public function send()
    {
        $this->load->model('Admin_model');
        if (isPost()) {
            //echo json_encode(array('result' => false, 'data' => $_POST)); dcoppieters@delta.nl
            if (!file_exists($this->data['setting']->DOC_PATH . '/' . $this->companyid . '/proformas/')) {
                mkdir($this->data['setting']->DOC_PATH . '/' . $this->companyid . '/proformas/', 0755, true);
            }
            $invoice_path = $this->data['setting']->DOC_PATH . '/' . $this->companyid . '/proformas/';
            $invoiceid = $_POST['invoiceid'];
            $invoice = $this->Proforma_model->getInvoice($invoiceid);
            $client = $this->Admin_model->getClient($invoice['userid']);

            $this->load->library('magebo', array('companyid' => $client->companyid));
            if (!$_POST['invoicenum']) {
                die('Invoice not found 11');
            }
            $invoice_ref = $this->magebo->Mod11($_POST['invoicenum']);
            //$invoice_ref = "11111111111";
            $res = $this->download_id($invoiceid);

            if ($res) {
                if ($this->data['setting']->smtp_type == 'smtp') {
                    $config = array(
                        'protocol' => 'smtp',
                        'smtp_host' => $this->data['setting']->smtp_host,
                        'smtp_port' => $this->data['setting']->smtp_port,
                        'smtp_user' => $this->data['setting']->smtp_user,
                        'smtp_pass' => $this->encryption->decrypt($this->data['setting']->smtp_pass),
                        'mailtype' => 'html',
                        'charset' => 'utf-8',
                        'starttls' => true,
                        'wordwrap' => true,
                    );
                } else {
                    $config['protocol'] = 'sendmail';
                    $config['mailpath'] = '/usr/sbin/sendmail';
                    $config['mailtype'] = 'html';
                    $config['charset'] = 'utf-8';
                    $config['wordwrap'] = true;
                }
                $this->email->clear(true);
                $this->email->initialize($config);
                $body = getMailContent('proforma', $client->language, $client->companyid);
                //$attachments = getAttachmentEmail('invoice', $client->language, $client->companyid);

                $amount = $invoice['total'];
                $body = str_replace('{$mInvoiceAmount}', number_format($amount, 2), $body);
                $body = str_replace('{$iInvoiceNbr}', $invoice['invoicenum'], $body);
                $body = str_replace('{$invoiceid}', $invoice['id'], $body);
                $body = str_replace('{$clientid}', $client->mvno_id, $body);
                $body = str_replace('{$dInvoiceDate}', $invoice['date'], $body);
                $body = str_replace('{$dInvoiceDueDate}', $invoice['duedate'], $body);
                $body = str_replace('{$Companyname}', $this->data['setting']->companyname, $body);
                $body = str_replace('{$name}', format_name($client), $body);
                $body = str_replace('{$cInvoiceReference}', $invoice_ref, $body);

                $this->email->set_newline("\r\n");
                $this->email->from($this->data['setting']->smtp_sender, $this->data['setting']->smtp_name);
                $this->email->to($client->email);
                $this->email->bcc(array('mail@simson.one', $_SESSION['email']));
                $subject = getSubject('proforma', $client->language, $client->companyid);
                $subject = str_replace('{$iInvoiceNbr}', $invoice['invoicenum'], $subject);
                $subject = str_replace('{$Companyname}', $this->data['setting']->companyname, $subject);
                $this->email->bcc($_SESSION['email']);
                $this->email->subject($subject);
                $this->email->attach($res);
                $this->email->message($body);
                if ($this->email->send()) {
                    logEmailOut(array('userid' => $client->id, 'companyid' => $client->companyid, 'to' => $client->email, 'subject' => $subject, 'message' => $body));

                    echo json_encode(array('result' => true));
                } else {
                    echo json_encode(array('result' => false, 'error' => $this->email->print_debugger()));
                }
            } else {
                echo json_encode(array('result' => false));
            }
        } else {
            echo json_encode(array('result' => false));
        }
    }

    public function download_id($id)
    {
        $companyid = $this->session->cid;
        set_time_limit(0);
        $this->load->model('Admin_model');

        if ($companyid == 54) {
            $brand = $this->Admin_model->getBrandPdfFooter(1);
            //print_r($brand);
            //exit;
            $this->lang->load('admin');
        }
        $this->config->set_item('language', 'english');
        $this->lang->load('admin');
        // $this->lang->load('admin');
        $invoice = $this->Proforma_model->getInvoice($id);

        $customerku = $this->Proforma_model->getClient($invoice['userid']);
        $this->load->library('magebo', array('companyid' => $companyid));
        $this->load->library('Spdf');
        $pdf = new Spdf('P', 'mm', 'A4', true, 'UTF-8', false);
        //$pdf->setData($invoice['notes']);
        //$pdf->setBank($this->data['setting']->bank_acc);
        //$pdf->settax($invoice['tax']);
        //$pdf->SetTitle($this->data['setting']->companyname . ' ' . $invoice['invoicenum']);
        $pdf->SetHeaderMargin(30);
        $pdf->SetTopMargin(20);
        //$pdf->droidsansogo($this->data['setting']->proforma_footer);
        $pdf->SetAutoPageBreak(true, 30);
        $pdf->SetAuthor('Simson Parlindungan & Tim Claesen');
        $pdf->SetDisplayMode('real', 'default');
        $pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        $pdf->setFoot($brand->brand_footer);
        $pdf->setFooterFont('droidsans');
        //$pdf->disableExtrabox(true);

        /*
        $this->getPageNumGroupAlias() to get current page number
        and
        $this->getPageGroupAlias() to get total page count
         */

        $tax = round($invoice['tax']) . '%';

        $pdf->AddPage('P', 'A4');

        $pdf->Image($brand->brand_logo, 10, 10, 60);
        $style = array('position' => '',
            'align' => 'R',
            'stretch' => false,
            'fitwidth' => true,
            'cellfitalign' => '',
            'border' => false,
            'hpadding' => 'auto',
            'vpadding' => 'auto',
            'fgcolor' => array(0, 0, 0),
            'bgcolor' => false,
            'text' => true,
            'font' => 'droidsans',
            'fontsize' => 8,
            'stretchtext' => 6,
        );

        $style1 = array('position' => '',
            'align' => 'R',
            'stretch' => false,
            'fitwidth' => true,
            'cellfitalign' => '',
            'border' => true,
            'hpadding' => 'auto',
            'vpadding' => 'auto',
            'fgcolor' => array(64, 64, 64),
            'bgcolor' => false,
            'color' => array(255, 255, 255),
            'text' => true,
            'font' => 'droidsans',
            'fontsize' => 8, 'stretchtext' => 6,
        );
        $pdf->setY(13);
        $pdf->setX(130);
        // $pdf->write1DBarcode(preg_replace("/[^0-9,.]/", "", $invoice['invoicenum']), 'I25', '', '', '', 18, 0.9, $style, 'R');

        $pdf->GetPage();

        $pdf->Image($brand->brand_logo, 10, 10, 90);
        $pdf->SetFont('droidsans', '', 10);
        if (!empty($customerku->companyname)) {
            $company = $customerku->companyname . "\n";
            $contact = '';
        } else {
            $company = '';
            $contact = $customerku->firstname . " " . $customerku->lastname . "\n";
        }
        $address1 = str_replace("&#039;", "'", $customerku->address1) . "\n";
        $pdf->setY(70);
        $city = $customerku->postcode . ", " . $customerku->city . "\n";
        $country = $customerku->country . "\n";
        $pdf->SetFont('droidsans', 'B', 12);
        $pdf->setCellPaddings(0, 0, 0, 0);
        $pdf->setCellMargins(0, 0, 0, 0);
        $pdf->SetFillColor(255, 255, 255);
        $a = '';
        $pdf->MultiCell(100, 10, $a, 0, 'J', 0, 0, '', '', true, 0, false, true, 30, 'T');
        $pdf->MultiCell(90, 30, $company . '' . $contact . '' . $address1 . '' . $city . '' . getCountryNameLang($customerku->country, $customerku->language), 0, 'L', 1, 1, '', '', true, 0, false, true, 30, 'M');

        if ($this->data['setting']->country_base == "BE") {
            $this->data['invoice_ref'] = ogm($this->uri->segment(4));
        } else {
            $this->data['invoice_ref'] = $this->magebo->Mod11($invoice['invoicenum']);
            // $invoice_ref = $this->magebo->Mod11($invoice->iInvoiceNbr);
        }

        $pdf->SetFont('droidsans', 'B', 14);
        //$pdf->SetLineStyle(array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(255, 153, 0)));
        $pdf->cell(70, 4, lang("Proforma Invoice") . ": " . $invoice['invoicenum'], 0, 1, 'L');
        $pdf->Ln(10);
        $pdf->SetFont('droidsansb', 'B', 9);
        $pdf->SetFillColor(64, 64, 64);
        $pdf->SetTextColor(255, 255, 255);
        $pdf->setCellMargins(0, 0, 0, 0);
        $pdf->setCellPaddings(2, 1, 2, 1);
        $pdf->SetLineStyle(array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(64, 64, 64)));
        $pdf->MultiCell(35, 7, lang('Client Number'), 1, 'L', 1, 0, '', '', true, 0, false, true, 40, 'T');
        $pdf->MultiCell(40, 7, lang('VAT'), 1, 'L', 1, 0, '', '', true, 0, false, true, 40, 'T');
        $pdf->MultiCell(50, 7, lang('Reference'), 1, 'L', 1, 0, '', '', true, 0, false, true, 40, 'T');
        $pdf->MultiCell(35, 7, lang('Date'), 1, 'L', 1, 0, '', '', true, 0, false, true, 40, 'T');
        $pdf->MultiCell(35, 7, lang('Duedate'), 1, 'R', 1, 1, '', '', true, 0, false, true, 40, 'T');
        $pdf->setCellPaddings(1, 1, 1, 1);
        $pdf->setCellMargins(1, 1, 1, 1);
        $pdf->SetFillColor(255, 255, 255);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('droidsans', '', 9);
        $pdf->setCellPaddings(2, 1, 2, 1);
        $pdf->setCellMargins(0, 0, 0, 0);
        $pdf->SetLineStyle(array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(64, 64, 64)));
        $pdf->MultiCell(35, 5, trim($invoice['mvno_id']), 1, 'L', 1, 0, '', '', true, 0, false, true, 10, 'T');
        $pdf->MultiCell(40, 5, $customerku->vat, 1, 'L', 1, 0, '', '', true, 0, false, true, 10, 'T');
        $pdf->MultiCell(50, 5, $this->data['invoice_ref'], 1, 'L', 1, 0, '', '', true, 0, false, true, 10, 'T');
        $pdf->MultiCell(35, 5, date("d-m-Y", strtotime($invoice['date'])), 1, 'L', 1, 0, '', '', true, 0, false, true, 10, 'T');
        $pdf->MultiCell(35, 5, date("d-m-Y", strtotime($invoice['duedate'])), 1, 'R', 1, 1, '', '', true, 0, false, true, 10, 'T');
        $pdf->Ln(15);

        # Invoice Items
        $tblhtml = '<table width="550" bgcolor="#404040" cellspacing="1" cellpadding="1" border="0">
    <tr height="40" bgcolor="#404040" style="font-weight:bold;text-align:center;">
    <td align="left" width="7%"  color="#fff" >' . lang('Qty') . '</td>
        <td align="left" width="58%"  color="#fff">' . lang('Description') . '</td>
         <td align="right" width="18%" color="#fff">' . lang('Price/pcs') . '</td>
        <td align="right" width="18%" color="#fff">' . lang('Amount') . '</td>
    </tr>';
        foreach ($invoice['items'] as $item) {
            $tblhtml .= '
    <tr bgcolor="#fff" border="1">
         <td align="left">' . $item['qty'] . '<br /></td>
        <td align="left">' . nl2br($item['description']) . '<br /></td>
        <td align="right">' . $this->data['setting']->currency . number_format($item['price'],2) . '</td>
        <td align="right">' . $this->data['setting']->currency . exvat($item['taxrate'], $item['amount']) . '</td>
    </tr>';
        }
        $tblhtml .= '

</table>';

        $pdf->writeHTML($tblhtml, true, false, false, false, '');

        $pdf->Ln(5);

        $styles = array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(64, 64, 64));

        //$pdf->Ln(10);
        $pdf->SetFont('droidsansb', 'B', 9);
        $pdf->SetFillColor(255, 255, 255);
        $pdf->setCellMargins(0, 0, 0, 0);
        $pdf->setCellPaddings(2, 1, 2, 1);
        $pdf->SetLineStyle(array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(64, 64, 64)));

        $pdf->MultiCell(135, 5, '', 0, 'L', 1, 0, '', '', true, 0, false, true, 40, 'T');
        $pdf->MultiCell(30, 5, 'Subtotal' . " :", 1, 'R', 1, 0, '', '', true, 0, false, true, 40, 'T');
        $pdf->MultiCell(30, 5, $this->data['setting']->currency. $invoice['subtotal'], 1, 'R', 1, 1, '', '', true, 0, false, true, 40, 'T');
        $pdf->SetLineStyle(array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(64, 64, 64)));
        $pdf->MultiCell(135, 5, '', 0, 'L', 1, 0, '', '', true, 0, false, true, 10, 'T');
        $pdf->MultiCell(30, 5, "VAT " . floor($invoice['items'][0]['taxrate']) . '%:', 1, 'R', 1, 0, '', '', true, 0, false, true, 10, 'T');
        $pdf->MultiCell(30, 5, $this->data['setting']->currency . number_format( $invoice['tax'],2), 1, 'R', 1, 1, '', '', true, 0, false, true, 10, 'T');
        $pdf->MultiCell(135, 5, '', 0, 'L', 1, 0, '', '', true, 0, false, true, 10, 'T');

        $pdf->SetFillColor(64, 64, 64);
        $pdf->setCellMargins(0, 0, 0, 0);
        $pdf->SetTextColor(255, 255, 255);
        $pdf->MultiCell(30, 5, "Total :", 1, 'R', 1, 0, '', '', true, 0, false, true, 10, 'T');
        $pdf->MultiCell(30, 5, $this->data['setting']->currency . number_format($invoice['total'],2), 1, 'R', 1, 1, '', '', true, 0, false, true, 10, 'T');
        $taxrate = 1;
        $lebar = '38';
        $ypos = '100';
        $xpos = '161';
        /*
        Gelieve het totale bedrag te storten voor 13/03/2019
        op rekeningnummer NL10RABO0147382548
        met vermelding van de referentie 5310 0005 5200 0000
         */
        $pdf->setY(-120);
        $pdf->SetLeftMargin(-10);
        $pdf->SetRightMargin(0);
        $pdf->SetAutoPageBreak(true, 0);
        $pdf->SetFont('droidsansb', 'B', 7);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->setImageScale(1.53);
        $pdf->SetLineStyle(array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
        $pdf->RoundedRect(5, 240, 200, 40, 3.50, '1111');
        $pdf->setXY(10, 242);
        $pdf->SetFont('droidsansb', '', 12);
        $pdf->Cell(140, 0, lang("Please pay amount of ") . $this->data['setting']->currency .  number_format($invoice['total'],2) . " " . lang("to be transfered before ") . date("d-m-Y", strtotime($invoice['duedate'])), 0, 0, 'R', 0, '0', 4);
        $pdf->setXY(10, 248);
        $pdf->Cell(100, 0, lang("On account number") . " : NL10RABO0147382548", 0, 0, 'R', 0, '0', 4);
        if ($invoice['items'][0]['taxrate'] > 0) {
            $pdf->ln(4);
            $pdf->setXY(10, 264);
            $pdf->Cell(100, 0, lang("with payment reference ") . $this->data['invoice_ref'], 0, 0, 'R', 0, '0', 4);
        } else {
            $pdf->ln(4);
            $pdf->setXY(10, 264);
            $pdf->Cell(30, 0, lang("Vat Exempt"), 0, 0, 'R', 0, '0', 4);
            $pdf->ln(4);
            $pdf->setXY(10, 270);
            $pdf->Cell(100, 0, lang("with payment reference ") . $this->data['invoice_ref'], 0, 0, 'R', 0, '0', 4);
        }
        //$this->data['setting']->DOC_PATH . '/' . $this->companyid . '/proformas/'

        $pdf->Output($this->data['setting']->DOC_PATH . '/' . $this->companyid . '/proformas/Proforma_' . $id . '.pdf', 'F');
        if (file_exists($this->data['setting']->DOC_PATH . '/' . $this->companyid . '/proformas/Proforma_' . $id . '.pdf')) {
            return $this->data['setting']->DOC_PATH . '/' . $this->companyid . '/proformas/Proforma_' . $id . '.pdf';
        } else {
            return false;
        }
    }

    public function download()
    {
        $companyid = $this->session->cid;
        set_time_limit(0);
        $this->load->model('Admin_model');

        if (!is_dir($this->data['setting']->DOC_PATH . 'proforma')) {
            mkdir($this->data['setting']->DOC_PATH . 'proforma', 0755);
        }
        if ($companyid == 54) {
            $brand = $this->Admin_model->getBrandPdfFooter(1);
            //print_r($brand);
            //exit;
        }
        $id = $this->uri->segment(4);
        $invoice = $this->Proforma_model->getInvoice($id);

        $customerku = $this->Proforma_model->getClient($invoice['userid']);
        $this->load->library('magebo', array('companyid' => $companyid));
        $this->load->library('Spdf');
        $pdf = new Spdf('P', 'mm', 'A4', true, 'UTF-8', false);
        //$pdf->setData($invoice['notes']);
        //$pdf->setBank($this->data['setting']->bank_acc);
        //$pdf->settax($invoice['tax']);
        //$pdf->SetTitle($this->data['setting']->companyname . ' ' . $invoice['invoicenum']);
        $pdf->SetHeaderMargin(30);
        $pdf->SetTopMargin(20);
        //$pdf->droidsansogo($this->data['setting']->proforma_footer);
        $pdf->SetAutoPageBreak(true, 30);
        $pdf->SetAuthor('Simson Parlindungan & Tim Claesen');
        $pdf->SetDisplayMode('real', 'default');
        $pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        $pdf->setFoot($brand->brand_footer);
        $pdf->setFooterFont('droidsans');
        //$pdf->disableExtrabox(true);

        /*
        $this->getPageNumGroupAlias() to get current page number
        and
        $this->getPageGroupAlias() to get total page count
         */

        $tax = round($invoice['tax']) . '%';

        $pdf->AddPage('P', 'A4');

        $pdf->Image($brand->brand_logo, 10, 10, 60);
        $style = array('position' => '',
            'align' => 'R',
            'stretch' => false,
            'fitwidth' => true,
            'cellfitalign' => '',
            'border' => false,
            'hpadding' => 'auto',
            'vpadding' => 'auto',
            'fgcolor' => array(0, 0, 0),
            'bgcolor' => false,
            'text' => true,
            'font' => 'droidsans',
            'fontsize' => 8,
            'stretchtext' => 6,
        );

        $style1 = array('position' => '',
            'align' => 'R',
            'stretch' => false,
            'fitwidth' => true,
            'cellfitalign' => '',
            'border' => true,
            'hpadding' => 'auto',
            'vpadding' => 'auto',
            'fgcolor' => array(64, 64, 64),
            'bgcolor' => false,
            'color' => array(255, 255, 255),
            'text' => true,
            'font' => 'droidsans',
            'fontsize' => 8, 'stretchtext' => 6,
        );
        $pdf->setY(13);
        $pdf->setX(130);
        // $pdf->write1DBarcode(preg_replace("/[^0-9,.]/", "", $invoice['invoicenum']), 'I25', '', '', '', 18, 0.9, $style, 'R');

        $pdf->GetPage();

        $pdf->Image($brand->brand_logo, 10, 10, 90);
        $pdf->SetFont('droidsans', '', 10);
        if (!empty($customerku->companyname)) {
            $company = $customerku->companyname . "\n";
            $contact = '';
        } else {
            $company = '';
            $contact = $customerku->firstname . " " . $customerku->lastname . "\n";
        }
        $address1 = str_replace("&#039;", "'", $customerku->address1. ' '.$customerku->housenumber. ' '.$customerku->alphabet) . "\n";
        $pdf->setY(70);
        $city = $customerku->postcode . ", " . $customerku->city . "\n";
        $country = $customerku->country . "\n";
        $pdf->SetFont('droidsans', 'B', 12);
        $pdf->setCellPaddings(0, 0, 0, 0);
        $pdf->setCellMargins(0, 0, 0, 0);
        $pdf->SetFillColor(255, 255, 255);
        $a = '';
        $pdf->MultiCell(100, 10, $a, 0, 'J', 0, 0, '', '', true, 0, false, true, 30, 'T');
        $pdf->MultiCell(90, 30, $company . '' . $contact . '' . $address1 . '' . $city . '' . getCountryNameLang($customerku->country, $customerku->language), 0, 'L', 1, 1, '', '', true, 0, false, true, 30, 'M');

        if ($this->data['setting']->country_base == "BE") {
            $this->data['invoice_ref'] = ogm($this->uri->segment(4));
        } else {
            if (!$invoice['invoicenum']) {
                die('Invoice not found 456');
            }
            $this->data['invoice_ref'] = $this->magebo->Mod11($invoice['invoicenum']);
            // $invoice_ref = $this->magebo->Mod11($invoice->iInvoiceNbr);
        }

        $pdf->SetFont('droidsans', 'B', 14);
        //$pdf->SetLineStyle(array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(255, 153, 0)));
        $pdf->cell(70, 4, lang("Proforma Invoice") . ": " . $invoice['invoicenum'], 0, 1, 'L');
        $pdf->Ln(10);
        $pdf->SetFont('droidsansb', 'B', 9);
        $pdf->SetFillColor(64, 64, 64);
        $pdf->SetTextColor(255, 255, 255);
        $pdf->setCellMargins(0, 0, 0, 0);
        $pdf->setCellPaddings(2, 1, 2, 1);
        $pdf->SetLineStyle(array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(64, 64, 64)));
        $pdf->MultiCell(35, 7, lang('Client Number'), 1, 'L', 1, 0, '', '', true, 0, false, true, 40, 'T');
        $pdf->MultiCell(40, 7, lang('VAT'), 1, 'L', 1, 0, '', '', true, 0, false, true, 40, 'T');
        $pdf->MultiCell(50, 7, lang('Reference'), 1, 'L', 1, 0, '', '', true, 0, false, true, 40, 'T');
        $pdf->MultiCell(35, 7, lang('Date'), 1, 'L', 1, 0, '', '', true, 0, false, true, 40, 'T');
        $pdf->MultiCell(35, 7, lang('Duedate'), 1, 'R', 1, 1, '', '', true, 0, false, true, 40, 'T');
        $pdf->setCellPaddings(1, 1, 1, 1);
        $pdf->setCellMargins(1, 1, 1, 1);
        $pdf->SetFillColor(255, 255, 255);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('droidsans', '', 9);
        $pdf->setCellPaddings(2, 1, 2, 1);
        $pdf->setCellMargins(0, 0, 0, 0);
        $pdf->SetLineStyle(array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(64, 64, 64)));
        $pdf->MultiCell(35, 5, trim($invoice['mvno_id']), 1, 'L', 1, 0, '', '', true, 0, false, true, 10, 'T');
        $pdf->MultiCell(40, 5, $customerku->vat, 1, 'L', 1, 0, '', '', true, 0, false, true, 10, 'T');
        $pdf->MultiCell(50, 5, $this->data['invoice_ref'], 1, 'L', 1, 0, '', '', true, 0, false, true, 10, 'T');
        $pdf->MultiCell(35, 5, date("d-m-Y", strtotime($invoice['date'])), 1, 'L', 1, 0, '', '', true, 0, false, true, 10, 'T');
        $pdf->MultiCell(35, 5, date("d-m-Y", strtotime($invoice['duedate'])), 1, 'R', 1, 1, '', '', true, 0, false, true, 10, 'T');
        $pdf->Ln(15);

        # Invoice Items
        $tblhtml = '<table width="550" bgcolor="#404040" cellspacing="1" cellpadding="1" border="0">
    <tr height="40" bgcolor="#404040" style="font-weight:bold;text-align:center;">
    <td align="left" width="7%"  color="#fff" >' . lang('Qty') . '</td>
        <td align="left" width="58%"  color="#fff">' . lang('Description') . '</td>
         <td align="right" width="18%" color="#fff">' . lang('Price/pcs') . '</td>
        <td align="right" width="18%" color="#fff">' . lang('Amount') . '</td>
    </tr>';
        foreach ($invoice['items'] as $item) {
            $tblhtml .= '
    <tr bgcolor="#fff" border="1">
   		 <td align="left">' . $item['qty'] . '<br /></td>
        <td align="left">' . nl2br($item['description']) . '<br /></td>
        <td align="right">' .$this->data['setting']->currency . number_format($item['price'],2) . '</td>
        <td align="right">' . $this->data['setting']->currency . exvat($item['taxrate'], $item['amount']) . '</td>
    </tr>';
        }
        $tblhtml .= '

</table>';

        $pdf->writeHTML($tblhtml, true, false, false, false, '');

        $pdf->Ln(5);

        $styles = array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(64, 64, 64));

        //$pdf->Ln(10);
        $pdf->SetFont('droidsansb', 'B', 9);
        $pdf->SetFillColor(255, 255, 255);
        $pdf->setCellMargins(0, 0, 0, 0);
        $pdf->setCellPaddings(2, 1, 2, 1);
        $pdf->SetLineStyle(array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(64, 64, 64)));

        $pdf->MultiCell(135, 5, '', 0, 'L', 1, 0, '', '', true, 0, false, true, 40, 'T');
        $pdf->MultiCell(30, 5, 'Subtotal' . " :", 1, 'R', 1, 0, '', '', true, 0, false, true, 40, 'T');
        $pdf->MultiCell(30, 5, $this->data['setting']->currency .  number_format($invoice['subtotal'],2), 1, 'R', 1, 1, '', '', true, 0, false, true, 40, 'T');
        $pdf->SetLineStyle(array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(64, 64, 64)));
        $pdf->MultiCell(135, 5, '', 0, 'L', 1, 0, '', '', true, 0, false, true, 10, 'T');
        $pdf->MultiCell(30, 5, "VAT " . floor($invoice['items'][0]['taxrate']) . '%:', 1, 'R', 1, 0, '', '', true, 0, false, true, 10, 'T');
        $pdf->MultiCell(30, 5, $this->data['setting']->currency . number_format($invoice['tax'], 2), 1, 'R', 1, 1, '', '', true, 0, false, true, 10, 'T');
        $pdf->MultiCell(135, 5, '', 0, 'L', 1, 0, '', '', true, 0, false, true, 10, 'T');

        $pdf->SetFillColor(64, 64, 64);
        $pdf->setCellMargins(0, 0, 0, 0);
        $pdf->SetTextColor(255, 255, 255);
        $pdf->MultiCell(30, 5, "Total :", 1, 'R', 1, 0, '', '', true, 0, false, true, 10, 'T');
        $pdf->MultiCell(30, 5, $this->data['setting']->currency . number_format($invoice['total'], 2), 1, 'R', 1, 1, '', '', true, 0, false, true, 10, 'T');
        $taxrate = 1;
        $lebar = '38';
        $ypos = '100';
        $xpos = '161';
        /*
        Gelieve het totale bedrag te storten voor 13/03/2019
        op rekeningnummer NL10RABO0147382548
        met vermelding van de referentie 5310 0005 5200 0000
         */
        $pdf->setY(-120);
        $pdf->SetLeftMargin(-10);
        $pdf->SetRightMargin(0);
        $pdf->SetAutoPageBreak(true, 0);
        $pdf->SetFont('droidsansb', 'B', 7);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->setImageScale(1.53);
        $pdf->SetLineStyle(array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
        $pdf->RoundedRect(5, 240, 200, 40, 3.50, '1111');
        $pdf->setXY(10, 242);
        $pdf->SetFont('droidsansb', '', 12);
        $pdf->Cell(140, 0, lang("Please pay amount of ") . $this->data['setting']->currency . number_format($invoice['total'],2) . " " . lang("to be transfered before ") . date("d-m-Y", strtotime($invoice['duedate'])), 0, 0, 'R', 0, '0', 4);
        $pdf->setXY(10, 248);
        $pdf->Cell(100, 0, lang("On account number") . " : NL10RABO0147382548", 0, 0, 'R', 0, '0', 4);
        if ($invoice['items'][0]['taxrate'] > 0) {
            $pdf->ln(4);
            $pdf->setXY(10, 264);
            $pdf->Cell(100, 0, lang("with payment reference ") . $this->data['invoice_ref'], 0, 0, 'R', 0, '0', 4);
        } else {
            $pdf->ln(4);
            $pdf->setXY(10, 264);
            $pdf->Cell(30, 0, lang("Vat Exempt"), 0, 0, 'R', 0, '0', 4);
            $pdf->ln(4);
            $pdf->setXY(10, 270);
            $pdf->Cell(100, 0, lang("with payment reference ") . $this->data['invoice_ref'], 0, 0, 'R', 0, '0', 4);
        }

        $pdf->Output('Proforma_' . $id . '.pdf', 'I');

        /*
    ob_clean();
    flush();
    $idx = $invoice['invoicenum'];
    //header('Content-Type: application/octet-stream');
    //header('Content-Length: ' . filesize($this->data['setting']->DOC_PATH . 'invoices/' . $invoice['id'] . '.pdf'));
    header('Content-Disposition: attachment; filename="' . $idx . '.pdf' . '"');
    readfile($this->data['setting']->DOC_PATH . 'proforma/' . $idx . '.pdf');
     */
    }
}
