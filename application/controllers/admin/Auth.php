<?php
defined('BASEPATH') or exit('No direct script access allowed');
class AuthController extends CI_Controller
{
    protected $data = [];
    public function __construct()
    { echo "a1";
        // Ensure you run parent constructor
        parent::__construct();
        if (!empty($this->session->language)) { echo " au1 ";
            $this->config->set_item('language', $this->session->language);
        } else {
            $this->config->set_item('language', 'english');
        }

        $this->companyid = get_companyidby_url(base_url());
        if (file_exists(APPPATH.'language/'.$this->config->item('language').'/'.$this->companyid.'_admin_lang.php')) {
            // log_message('error', 'Using Custom language files');
            $this->lang->load($this->companyid.'_admin');
        } else {
            // log_message('error', APPPATH.'language/'.$this->config->item('language').'/'.$this->companyid.'_admin.php');
            $this->lang->load('admin');
        }
        $this->headers = $this->input->request_headers();
        $this->data['setting'] = globofix($this->companyid);
    }
}
class Auth extends AuthController
{
    public function __construct()
    {echo "a2";
        parent::__construct();
        if (empty($this->session->userdata('theme'))) {
            $this->session->set_userdata('theme', 'exocom');
        }
        $this->load->model('Auth_model');
    }
    public function index()
    {echo "a3";
        if (isPost()) {echo "a6";
            // if($this['setting']->admin_captcha_enable){
            if ($_POST['captcha'] != $this->session->code) {
                if (!empty($_POST['json'])) {
                    echo json_encode(array('result' => false,'message' => 'Your Captcha is Incorrect'));echo "a5";
                    exit;
                } else {
                    $this->session->set_flashdata('error', 'Your Captcha is Incorrect');
                    redirect('admin/auth');
                }
            }
            // }
            $valid = $this->Auth_model->admin_auth($_POST);echo "a4";
            if ($valid['result'] == "success") {echo "a20";
                if (has2f_authentication($valid['admin']['id'])) {
                    $this->session->set_userdata('adminid', $valid['admin']['id']);
                    $this->session->set_userdata('need_google_fa', true);
                    if (!empty($_POST['json'])) { echo "a21";
                        echo json_encode(array('result' => true, 'redirect' => true));
                        exit;
                    }
                         echo "a21";
                    redirect('admin/auth/auth_google');
                } else { echo "a23";
                    $this->session->unset_userdata('adminid');
                    $this->session->set_userdata('need_google_fa', false);
                }
                echo "a24";
                $valid['admin']['logged'] = true; 
                unset($valid['admin']['password']);
                $this->session->set_userdata($valid['admin']);
                $this->session->set_userdata('chatusername', $valid['admin']['username']);
                $this->session->set_userdata('username', $valid['admin']['id']); echo "a27";
              /*  send_growl(array(
                'message' => $this->session->firstname . ' ' . $this->session->lastname.' successfully logged in to the system from ip: '.$_SERVER['REMOTE_ADDR'],
                'companyid' => $this->session->cid)); */ echo "a28";
                if (!empty($_POST['json'])) {echo "a25";
                    echo json_encode(array('result' => true));
                    exit;
                }echo "a26";
                redirect('admin');
            } else {echo "a21";
                if (!empty($_POST['json'])) {echo "a22";
                    echo json_encode(array('result' => false, 'message' => $valid['message']));
                    exit;
                }

                $this->session->set_flashdata('error', $valid['message']);
            }
        }

echo "a777";echo $this->session->cid;  echo $this->data; 
echo admin_theme($this->data['setting']->default_theme);
        $this->load->view(admin_theme($this->data['setting']->default_theme).'auth/'.$this->session->cid.'/auth_login', $this->data); echo "a8";
    }

    public function auth_google()
    {
        if (isPost()) {
            $q  = $this->db->query("select a.secret as google_secret, b.* from a_admin_2fa a left join a_admin b on b.id=a.adminid where a.adminid=? order by a.id desc", array($this->session->adminid));
            if ($q->num_rows()>0) {
                $ga = new PHPGangsta_GoogleAuthenticator();
                $checkResult = $ga->verifyCode($q->row()->google_secret, $_POST['code'], 2);    // 2 = 2*30sec clock tolerance
                if ($checkResult) {
                    $valid['admin'] = $q->row_array();
                    unset($valid['password']);
                    unset($valid['google_secret']);
                    $valid['admin']['logged'] = true;
                    $this->session->set_userdata($valid['admin']);
                    $this->session->unset_userdata($valid['adminid']);
                    $this->session->unset_userdata($valid['code']);
                    $this->session->unset_userdata($valid['need_google_fa']);
                    $this->session->set_userdata('chatusername', $valid['admin']['username']);
                    $this->session->set_userdata('username', $valid['admin']['id']);
                    $this->session->set_flashdata('success', 'Welcome to your Dashboard');
                    redirect('admin');
                } else {
                    $this->session->set_flashdata('error', 'Token Provided is not valid');
                    redirect('admin/auth/auth_google');
                }
            } else {
                $this->session->set_flashdata('error', 'Not possible to logged you in. Internal error: 1252. Please contact our support');
                redirect('admin/auth/auth_google');
            }
        }
        $this->load->view(admin_theme($this->data['setting']->default_theme).'auth/'.$this->session->cid.'/auth_login_google', $this->data);
    }

    public function reset_backup_code()
    {
        $this->load->view(admin_theme($this->data['setting']->default_theme).'auth/'.$this->session->cid.'/reset_backup_code', $this->data);
    }
    public function switchlang()
    {
        $this->session->set_userdata('language', $this->uri->segment(4));
        redirect('admin/auth');
    }

    public function switchcompany()
    {
        $this->db = $this->load->database('default', true);
        $code= $this->uri->segment(4);
        $q = $this->db->query("select * from a_admin where master_code=?", array($code));
        if ($q->num_rows()>0) {
            $admin = $q->row_array();
            //$this->db->query('update a_admin set master_code=? where id=?', array('', $admin['id']));
            $this->session->set_userdata($admin);
            $this->session->unset_userdata('password');
            $this->session->set_userdata('chatusername', $admin['username']);
            $this->session->set_userdata('username', $admin['id']);
            $this->session->set_userdata('logged', true);
            $this->session->set_userdata('cid', $admin['companyid']);
            $this->session->set_userdata('base_url', base_url());
            $this->session->set_flashdata('success', lang('Welcome to your Dashboard'));
        } else {
            $this->session->set_flashdata('error', lang('Authentication failed'));
        }
        if (!empty($this->uri->segment(5))) {
            redirect('admin/setting/configuration');
        } else {
            redirect('admin');
        }
    }
    public function iamonline()
    {
        if (empty($this->session->id)) {
            $res = array('ack' => false, 'new' => false);

            echo json_encode($res);
            die();
        } else {
            $id = $this->session->id;

            $ip = $_SERVER['REMOTE_ADDR'];
            $this->db->where('id', $id);
            $this->db->update('a_admin', array('lastseen' => time(), 'ip' => $ip));
            $new = $this->Auth_model->anyNewMessage();
            if ($new['new']) {
                $res = array('ack' => true, 'admin' => $id, 'new' => true, 'users' => $new['users']);
            } else {
                $res = array('ack' => true, 'admin' => $id, 'new' => false);
            }
            echo json_encode($res);
        }
    }


    public function cpt()
    {
        $captcha_num = rand(1000, 9999);
        //$this->session->set['code'] = $captcha_num;
        $this->session->set_userdata('code', $captcha_num);
        header('Content-type: image/jpeg');
        $font_size = 20;
        $img_width = 90;
        $img_height = 40;
        $image = imagecreate($img_width, $img_height); // create background image with dimensions
        imagecolorallocate($image, 255, 255, 255); // set background color
        $text_color = imagecolorallocate($image, 0, 0, 0);
        imagettftext($image, $font_size, 0, 15, 30, $text_color, 'assets/clear/client/Campton.Book.ttf', $captcha_num);
        imagejpeg($image);
    }
    public function reset()
    {
        if (isPost()) {
            $this->load->library('umail', array('companyid' => $this->session->cid));
            $admin = $this->db->query('select * from a_admin where email like ? and companyid =?', array(strtolower($_POST['email']), $this->companyid));
            if ($admin->num_rows() > 0) {
                $code = $this->Auth_model->addCode(random_str('alphanum', '10'), $admin->row()->id);
                $_POST['code'] =  $code;
              
                $this->umail->admin_reset($admin->row(), $_POST);
            } else {
                $this->session->set_flashdata('error', lang('We can not find this email in our system'));
            }
        }
        $this->session->sess_destroy();
        //$this->load->view('themes/' . $this->data['setting']->default_theme . '/admins/auth_reset', $this->data);
        $this->load->view(admin_theme($this->data['setting']->default_theme).'auth/'.$this->session->cid.'/auth_reset', $this->data);
    }

    public function entercode()
    {
        if (isPost()) {
            $code             = $this->Auth_model->getCode($_POST['code']);
            $this->session->set_userdata('code', $_POST['code']);
            if ($code['result']) {
                $this->session->set_flashdata('Success', lang('Please Change your Password'));
                $this->load->view('themes/' . $this->data['setting']->default_theme . '/admins/auth_resetwithcode', $this->data);
            } else {
                $this->session->set_flashdata('error', lang('your token is invalid'));
                $this->load->view('themes/' . $this->data['setting']->default_theme . '/admins/auth/'.$this->session->cid.'/auth_code', $this->data);
            }
        } else {
            $this->load->view('themes/' . $this->data['setting']->default_theme . '/admins/auth/'.$this->session->cid.'/auth_code', $this->data);
        }
    }

    public function resetwithcode()
    {
        $this->db = $this->load->database('default', true);
        if (isPost()) {
            $this->load->library('umail', array('companyid' => $this->session->cid));
            $admin            = $this->db->query('select * from a_admin where resetcode like ? ', array($_POST['code']));
            $code             = $_POST['code'];
            $this->session->set_userdata('code', $code);
            if (trim($_POST['password1']) == trim($_POST['password2'])) {
                $this->Auth_model->ChangePassword($_POST);
                $valid           = $admin->row_array();
                $valid['logged'] = true;
                $this->session->sess_destroy();
                $this->session->set_userdata($valid);
                $this->session->set_userdata('code');
                $this->umail->resetwithcode($admin->row());
            } else {
                $this->session->set_flashdata('error', lang('your password do not match'));
                redirect('admin');
                exit;
            }
        } else {
            $code = $this->Auth_model->getCode(trim($this->uri->segment(4)));
            if ($code['result']) {
                $this->session->set_userdata('code', trim($this->uri->segment(4)));
                $this->load->view('themes/' . $this->data['setting']->default_theme . '/admins/auth/'.$this->session->cid.'/auth_resetwithcode', $this->data);
            } else {
                $this->session->set_flashdata('error', lang('Please enter valid token'));
                $this->load->view('themes/' . $this->data['setting']->default_theme . '/admins/auth/'.$this->session->cid.'/auth_code', $this->data);
            }
        }
    }

    public function logout()
    {
        logAdmin(array('companyid' => $this->session->cid, 'userid' => 0, 'user' => $this->session->firstname . ' ' . $this->session->lastname, 'ip' => $_SERVER['REMOTE_ADDR'], 'description' => 'Logged successfully'));
        $companyid = $this->session->cid;
        $this->session->sess_destroy();
        $this->session->set_userdata('cid', $companyid);
        redirect('admin/auth');
    }



    public function logCall()
    {
        $this->db  = $this->load->database('default', true);
        $serviceid = $this->uri->segment(4);
        $status    = $this->uri->segment(5);
        $this->db->insert('a_event_status', array('serviceid' => $serviceid, 'status' => $status));
    }

    public function verifytoken()
    {
        $g = $this->input->request_headers();
        foreach ($g as $key => $header) {
            $headers[strtolower($key)] = $header;
        }
        $data = explode("#", $this->encryption->decrypt($headers['x-api-key']));
        $q = $this->db->query("select * from a_admin where id=?", array($data[1]));
        if ($data[0] == $this->session->cid) {
            if ($q->num_rows()>0) {
                $result["verified"] = true;
                $result["cid"] = $data[0];
                $result["parameters"] = $headers;
                // $result['headers'] = $this->headers;
                $result["time"] = $data[3];
                $this->peer_online(array('name' => $q->row()->firstname.' '.$q->row()->lastname, 'username' => $q->row()->username,  'picture' => $q->row()->picture));
            } else {
                $result["verified"] = false;
                $result["message"] = "Token Invalid";
                $result["parameters"] = $data;
            }
        } else {
            $result["verified"] = false;
            $result["message"] = "Token Invalid";
            $result["parameters"] = $data;
            // $result['headers'] = $this->headers;
        }
        echo json_encode($result);
    }
    public function getOnlineUser()
    {
        $now = time() - 114000;


        $this->db    = $this->load->database('default', true);
        $q         = $this->db->query("select  id,concat(firstname,' ',lastname) as name,username,picture,lastseen from  a_admin where companyid=? and lastseen >= ? and id != ? order by lastseen desc limit 10", array(
          $this->companyid,
          $now,
          $this->adminid
         ));
        if ($q->num_rows()) {
            foreach ($q->result_array() as $r) {
                if (time()- $r['lastseen'] <= 600) {
                    $r['status'] = "success";
                } else {
                    $r['status'] = "dark";
                }

                $result[] = $r;
            }
        }



        echo json_encode($result);
    }
    public function read_envelope()
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://socket.united-telecom.be/read_envelope");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt(
            $ch,
            CURLOPT_POSTFIELDS,
            "user=".$_POST['user']
        );
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        curl_close($ch);
    }

    public function peer_online($staf)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://socket.united-telecom.be/send_online");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt(
            $ch,
            CURLOPT_POSTFIELDS,
            "name=".$staf['name']."&username=".$staf['username']."&picture=".$staf['picture']
        );
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        curl_close($ch);
    }
}
