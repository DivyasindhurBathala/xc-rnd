<?php
defined('BASEPATH') or exit('No direct script access allowed');
class CreditnoteController extends CI_Controller
{
    protected $data = [];

    public function __construct()
    {
        // Ensure you run parent constructor
        parent::__construct();
        if (!empty($this->session->language)) {
            $this->config->set_item('language', $this->session->language);
        } else {
            $this->config->set_item('language', 'dutch');
        }
        $this->lang->load('admin');
        $this->companyid = get_companyidby_url(base_url());

        $this->data['setting'] = globofix($this->companyid);
    }
}
class Creditnote extends CreditnoteController
{
    public function __construct()
    {
        parent::__construct();
        $this->companyid = get_companyidby_url(base_url());
        /*
        $classes = get_class_methods($this);
        foreach ($classes as $class) {
            if ($class != 'setProperty' && $class != 'get_instance' && $class != '__construct') {
                $this->db->insert('a_admin_permissions', array('folder' => 'admin', 'controller' => 'creditnote', 'method' => $class));
                //$this->db->insert('admin_roles', array('rolename' => 'superadmin', 'perms' => $this->db->insert_id()));
            }
        }

      */
        $this->load->model('Admin_model');
    }

    public function index()
    {
        $this->data['stats'] = $this->Admin_model->getStatistic();
        $this->data['dtt'] = 'creditnotes.js?version=1.0';
        $this->data['title'] = "Creditnote List";
        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'creditnote';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }
    public function detail()
    {
        $clientid = getClientidbyMagebo($this->uri->segment(5));
        $this->data['whmcsid'] = $clientid;
        $this->data['client'] = $this->Admin_model->getClient($clientid);
        $this->data['invoice'] = $this->Admin_model->getInvoicedetail($this->uri->segment(4));
        $this->data['title'] = "Client List Dasboard";
        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'creditnote_detail';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }
    public function export_invoice_pdf_bydate()
    {
        $filename = time() . '_Invoices.pdf';
        $this->db = $this->load->database('magebo_replication', true);
        $this->db->select("iInvoiceNbr as id,iInvoiceNbr as invoicenum");
        $this->db->from("magebo_invoices");
        if (!empty($_POST['userid'])) {
            $this->db->where("iAddressNbr", $_POST['userid']);
        }
        if ($_POST['status'] != "ALL") {
            $this->db->where("iInvoiceStatus", $_POST['status']);
        }
        $this->db->where("dInvoiceDate >=", $_POST['start']);
        $this->db->where("dInvoiceDate <=", $_POST['end']);
        $this->db->where("iCompanyNbr", $this->data['setting']->companyid);
        $this->db->order_by("iInvoiceNbr", "ASC");
        $q = $this->db->get();
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $data) {
                $this->download_id($data['id']);

                $invoices[] = $this->data['setting']->DOC_PATH . $this->companyid . '/invoices/' . $data['id'] . '.pdf';
            }
            $files = implode(' ', $invoices);

            echo shell_exec('pdftk ' . $files . ' output ' . $this->data['setting']->DOC_PATH . $this->companyid . '/invoices/' . $filename);
            ob_clean();
            flush();

            //echo $this->data['setting']->DOC_PATH . 'invoices/' . $invoice['id'] . '.pdf';
            header('Content-Type: application/octet-stream');
            header('Content-Length: ' . filesize($this->data['setting']->DOC_PATH . $this->companyid . "/invoices/" . $filename));
            header('Content-Disposition: attachment; filename="' . $filename . '"');
            readfile($this->data['setting']->DOC_PATH . $this->companyid . '/invoices/' . $filename);
            unlink($this->data['setting']->DOC_PATH . $this->companyid . '/invoices/' . $filename);
        } else {
            $this->session->set_flashdata('error', lang('There is no result to be exported'));
            redirect('admin/invoice');
        }
    }

    public function export_invoice_csv_bydate()
    {
        $fileName = 'Invoice-Summary_by_Date_' . time() . '.csv';
        $this->db = $this->load->database('magebo_replication', true);

        $this->db->select("magebo_invoices.iInvoiceNbr as Invoicenumber,magebo_invoices.iAddressNbr as ClientId,tblAddress.cName as Customer,magebo_invoices.dInvoiceDate as Date,magebo_invoices.dInvoiceDueDate as Duedate,
			CASE magebo_invoices.iInvoiceStatus
  WHEN '54' THEN 'PAID'
  ELSE 'UNPAID'
  END as 'Status',magebo_invoices.mInvoiceAmount as Amount");
        $this->db->from("magebo_invoices");
        $this->db->join("tblAddress", "tblAddress.iAddressNbr=magebo_invoices.iAddressNbr", "LEFT");
        if (!empty($_POST['userid'])) {
            $this->db->where("magebo_invoices.iAddressNbr", $_POST['userid']);
        }
        if ($_POST['status'] != "ALL") {
            $this->db->where("magebo_invoices.iInvoiceStatus", $_POST['status']);
        }
        $this->db->where("magebo_invoices.dInvoiceDate >=", $_POST['start']);
        $this->db->where("magebo_invoices.dInvoiceDate <=", $_POST['end']);
        $this->db->where("magebo_invoices.iCompanyNbr", $this->data['setting']->companyid);
        $this->db->order_by("magebo_invoices.iInvoiceNbr", "ASC");
        $q = $this->db->get();
        if ($q->num_rows() > 0) {
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header('Content-Description: File Transfer');
            header("Content-type: text/csv");
            header("Content-Disposition: attachment; filename={$fileName}");
            header("Expires: 0");
            header("Pragma: public");

            $fh = @fopen('php://output', 'w');
            $headerDisplayed = false;
            foreach ($q->result_array() as $data) {
                // Add a header row if it hasn't been added yet
                if (!$headerDisplayed) {
                    // Use the keys from $data as the titles
                    fputcsv($fh, array_keys($data));
                    $headerDisplayed = true;
                }

                // Put the data into the stream
                fputcsv($fh, $data);
            }
            // Close the file
            fclose($fh);
            // Make sure nothing else is sent, our file is done
            exit;
        } else {
            $this->session->set_flashdata('error', lang('There is no result to be exported'));
            redirect('admin/invoice');
        }
    }

    public function download()
    {
        if (!file_exists($this->data['setting']->DOC_PATH . '/' . $this->companyid . '/invoices/')) {
            mkdir($this->data['setting']->DOC_PATH . '/' . $this->companyid . '/invoices/', 0755, true);
        }
        $invoice_path = $this->data['setting']->DOC_PATH . '/' . $this->companyid . '/invoices/';

        $id = $this->uri->segment(4);
        if ($this->data['setting']->companyid == 2) {
            $companyid = array(1, 2, 3, 4, 6, 8, 9, 25, 26, 28, 29, 31, 34, 35, 37, 41, 47, 50);
        } else {
            $companyid[] = $this->companyid;
        }

        if ($this->Admin_model->IsAllowedCn($id, $companyid)) {
            $invoice = $this->Admin_model->getInvoicePdf($id);

            //print_r($invoice->PDF);
            // exit;
            $file = $invoice_path . $id . '.pdf';
            file_put_contents($file, $invoice->PDF);
            if (file_exists($file)) {
                header('Content-type: application/pdf');
                header('Content-Disposition: inline; filename="' . $id . '.pdf"');
                header('Content-Transfer-Encoding: binary');
                header('Accept-Ranges: bytes');
                @readfile($file);
            } else {
                echo "file does not exists";
            }
        } else {
            $this->session->set_flashdata('error', lang('This acction has been denied'));
            redirect('errors/access_denied');
        }
    }

    public function download_id($id)
    {
        if (!file_exists($this->data['setting']->DOC_PATH . '/' . $this->companyid . '/invoices/')) {
            mkdir($this->data['setting']->DOC_PATH . '/' . $this->companyid . '/invoices/', 0755, true);
        }
        $invoice_path = $this->data['setting']->DOC_PATH . '/' . $this->companyid . '/invoices/';

        if ($this->companyid == 2) {
            $companyid = array(1, 2, 3, 4, 6, 8, 9, 25, 26, 28, 29, 31, 34, 35, 37, 41, 47, 50);
        } else {
            $companyid[] = $this->companyid;
        }
        if ($this->Admin_model->IsAllowedInvoice($id, $companyid)) {
            if (!file_exists($invoice_path . $id . '.pdf')) {
                $invoice = $this->Admin_model->getInvoicePdf($id);
                //$file = '/tmp/' . $id . '.pdf';

                file_put_contents($invoice_path . $id . '.pdf', $invoice->PDF);
                //copy($file, $this->data['setting']->DOC_PATH . '/invoices/' . $id . '.pdf');
                //unlink($file);
            }
        } else {
        }
    }
    public function add()
    {
    }

    public function publish()
    {
        $this->load->model('Admin_model');
        $clientid = getClientidbyMagebo($_POST['iAddressNbr']);
        $client = $this->Admin_model->getClient($clientid);
        $this->load->library('magebo', array("companyid" => $this->session->cid));

        $date = new DateTime(date('Y-m-d'));
        $date->modify("+" . $client->payment_duedays . " day");

        if ($_POST['amount'] > $_POST['mInvoiceAmount']) {
            echo json_encode(array('result' => 'error', lang('Your Amount of Ceditnote seem to be larger than invoice amount')));
            exit;
        }

        $result = $this->magebo->CreateCn(array(
            'invoicenum' => $_POST['invoicenum'],
            'payment_duedays' => $client->payment_duedays,
            'date' => date('Y-m-d'),
            'duedate' => $date->format('Y-m-d'),
            'amount' => $_POST['amount'],
            'vat_rate' => $client->vat_rate,
            'message' => $_POST['description'] . " - " . $_POST['invoicenum'],
            'companyid' => $this->session->cid,
            'mageboid' => $_POST['iAddressNbr']));

        echo json_encode($result);
    }
    public function send()
    {
        if (isPost()) {
            if (!file_exists($this->data['setting']->DOC_PATH . '/' . $this->companyid . '/invoices/')) {
                mkdir($this->data['setting']->DOC_PATH . '/' . $this->companyid . '/invoices/', 0755, true);
            }
            $invoice_path = $this->data['setting']->DOC_PATH . '/' . $this->companyid . '/invoices/';
            $invoiceid = $_POST['invoiceid'];

            $this->load->library('magebo', array('companyid' => $this->session->cid));
            $invoice = $this->magebo->getInvoice($invoiceid);

            $whmcsid = getWhmcsid($invoice->iAddressNbr);
            $client = $this->Admin_model->getClient($whmcsid);

            $res = $this->download_id($invoiceid);
            if (!file_exists($invoice_path . $invoiceid . '.pdf')) {
                $this->download_id($invoiceid);

                echo json_encode(array('result' => true));
            } else {
                $config['protocol'] = 'sendmail';
                $config['mailpath'] = '/usr/sbin/sendmail';
                $config['mailtype'] = 'html';
                $config['charset'] = 'utf-8';
                $config['wordwrap'] = true;

                $body = getMailContent('creditnote', $client->language, $client->companyid);
                $body = str_replace('{$mInvoiceAmount}', number_format($invoice->mInvoiceAmount, 2), $body);
                $body = str_replace('{$iInvoiceNbr}', $invoice->iInvoiceNbr, $body);
                $body = str_replace('{$clientid}', $client->mvno_id, $body);
                $body = str_replace('{$dInvoiceDate}', $invoice->dInvoiceDate, $body);
                $body = str_replace('{$dInvoiceDueDate}', $invoice->dInvoiceDueDate, $body);
                $body = str_replace('{$Companyname}', $this->data['setting']->companyname, $body);
                $body = str_replace('{$name}', format_name($client), $body);

                $subject = getSubject('creditnote', $client->language, $client->companyid);
                $subject = str_replace('{$iInvoiceNbr}', $invoice->iInvoiceNbr, $subject);
                $subject = str_replace('{$Companyname}', $this->data['setting']->companyname, $subject);
                $this->email->clear(true);
                $this->email->initialize($config);
                $this->email->set_newline("\r\n");
                $this->email->from($this->data['setting']->smtp_sender, $this->data['setting']->smtp_name);
                $this->email->to($client->email);
                //$this->email->to('mail@simson.one');
                $this->email->subject($subjet);
                $this->email->bcc($_SESSION['email']);
                $this->email->message($body);
                $this->email->attach($invoice_path . $invoiceid . '.pdf');
                if (!isTemplateActive($client->companyid, 'creditnote')) {
                    log_message('error', lang('Template creditnote is disabled sending aborted'));
                    echo json_encode(array('result' => false, 'detail' => lang('Template creditnote is disabled sending aborted')));
                    exit;
                }

                if ($this->email->send()) {
                    $this->session->set_flashdata('success', lang('Creditnote').' :' . $invoiceid . lang('has been sent to customer with you in BCC'));
                    logEmailOut(array('userid' => $client->id, 'companyid' => $client->companyid, 'to' => $client->email, 'subject' => $subject, 'message' => $body));
                    echo json_encode(array('result' => true, 'detail' => $client->id));
                } else {
                    echo json_encode(array('result' => false, 'error' => $this->email->print_debugger()));
                }
            }
        } else {
            echo json_encode(array('result' => false));
        }
    }

    public function downloadcdr()
    {
        $id = $this->uri->segment(4);
        $this->load->library('xlswriter');

        $cdrs = $this->Admin_model->mageboGet('GetCdrs/' . $id);

        $fileName = 'CDR-Summary_invoice_' . $id . '.csv';
        /*
        lang('Date') => 'string',
        lang('Type') => 'string',
        lang('Destination') => 'string',
        lang('Duration') => 'string',

        lang('Cost') => 'string',
         */
        $header = array(

            lang('Date') => 'string',
            lang('Type') => 'string',
            lang('Destination') => 'string',
            lang('Duration') => 'string',
            lang('Cost') => 'string',

        );

        if (count($cdrs) > 0) {
            foreach ($cdrs as $array) {
                $to[] = array('datum' => $array->datum, 'type' => 'NA', 'Destination' => $array->naar, 'Duration' => gmdate("H:i:s", $array->duur), 'Cost' => includevat('21', $array->kost));
            }
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            $this->xlswriter->writeSheet($to, 'Sheet1', $header);
            $this->xlswriter->setAuthor('Simson Parlindungan');
            $this->xlswriter->setTitle('Cdr Export');
            $this->xlswriter->setCompany('Delta');
            echo $this->xlswriter->writeToString();
        } else {
            $this->session->set_flashdata('error', lang('no data to be exported'));
            redirect('admin/invoice');
            exit;
        }
    }

    public function apply_payment()
    {
        $postfields = $_POST;
        $ch = curl_init();
        if ($_POST['step'] == "1") {
            curl_setopt($ch, CURLOPT_URL, 'https://probile.united-telecom.be/api.php?step=1');
        } elseif ($_POST["step"] == "2") {
            curl_setopt($ch, CURLOPT_URL, 'https://probile.united-telecom.be/api.php?step=2');
        }

        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postfields));
        $response = curl_exec($ch);
        if (curl_error($ch)) {
            die('Unable to connect: ' . curl_errno($ch) . ' - ' . curl_error($ch));
        }
        curl_close($ch);
        $s = json_decode($response);
        if ($s->result) {
            $this->session->set_userdata('success', lang('Payment has been inserted'));
        }
        echo $response;
    }
}
