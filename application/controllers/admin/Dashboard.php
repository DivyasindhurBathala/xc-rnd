<?php
defined('BASEPATH') or exit('No direct script access allowed');
class DashboardController extends CI_Controller
{
    protected $data = [];

    public function __construct()
    {
        // Ensure you run parent constructor
        parent::__construct();
        if (!empty($this->session->language)) {
            $this->config->set_item('language', $this->session->language);
        } else {
            $this->config->set_item('language', 'dutch');
        }

        $this->companyid = get_companyidby_url(base_url());
        if (file_exists(APPPATH.'language/'.$this->config->item('language').'/'.$this->companyid.'_admin_lang.php')) {
            log_message('error', APPPATH.'language/'.$this->config->item('language').'/'.$this->companyid.'_admin_lang.php');
            $this->lang->load($this->companyid.'_admin');
        } else {
            // log_message('error', APPPATH.'language/'.$this->config->item('language').'/'.$this->companyid.'_admin.php');
            $this->lang->load('admin');
        }


        $this->data['setting'] = globofix($this->companyid);
    }
}
class Dashboard extends DashboardController
{
    public function __construct()
    {
        parent::__construct();

        /*
        $classes = get_class_methods($this);
        foreach ($classes as $class) {
        if ($class != 'setProperty' && $class != 'get_instance' && $class != '__construct') {
        $this->db->insert('admin_permissions', array('folder' => 'admin', 'controller' => 'dashboard', 'method' => $class));
        $this->db->insert('admin_roles', array('rolename' => 'superadmin', 'perms' => $this->db->insert_id()));
        }

        }
         */
        if (!empty($this->session->profiler)) {
            if ($this->session->profiler) {
                $this->output->enable_profiler(true);
                error_reporting(E_ALL);
                ini_set('display_errors', 1);
            }
        }
        $this->load->model('Admin_model');
    }

    public function index()
    {echo "d1";
        if ($this->session->id == "1") {echo "d2";
            // print_r($_SESSION);
            //echo $this->companyid.'_admin';
        }
        if ($this->data['setting']->mage_invoicing) {
            $this->data['invoices'] = $this->Admin_model->getInvoicebyMonth($this->session->cid);
        } else {
            $this->data['invoices'] = false;
        }
echo "d4";
       // $this->data['gr'] = $this->Admin_model->getClientsByMonth($this->session->cid);
       // $this->data['serv'] = $this->Admin_model->getSubscriptionActiveByMonth($this->session->cid);
       // $this->data['stats'] = $this->Admin_model->getStatistic();
       // $this->data['dtt'] = 'clients.js';echo "d5";
        $this->data['title'] = "UnitedMvno Dasboard";
       // if ($this->data['setting']->mage_invoicing) {
        //   $this->load->library('magebo', array('companyid' => $this->companyid));
       //     $this->data['invoicesum'] = $this->magebo->getAvarageInvoice();
       // } 
echo "d5";
        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'dashboard'; echo admin_theme($this->data['setting']->default_theme) . 'dashboard';
echo $this->data['setting']->default_theme;
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }

    public function logs()
    {
        $this->data['title'] = "Logs Dasboard";
        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'logs';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }
    public function switchlang()
    {
        $url = $this->agent->referrer();
        $lang = $this->uri->segment(4);
        //_SESSION['language'] = $lang;
        $this->session->set_userdata('language', $lang);
        header('Location: ' . $url);
    }
    public function upload_picture()
    {
        $config['upload_path'] = FCPATH . 'assets/img/staf/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['encrypt_name'] = true;
        $this->load->library('upload', $config);
        if ($this->upload->do_upload('picture')) {
            $pic = $this->upload->data('file_name');
            //$_SESSION['picture'] = $pic;
            $this->session->set_userdata('picture', $pic);

            $result = $this->Admin_model->updateAdmin($this->session->email, array('picture' => $pic, 'email' => $this->session->email));
            $this->session->set_flashdata('success', 'Your picture has been updated');
        } else {
            $this->session->set_flashdata('error', 'Your picture was not uploaded');
        }

        //28dee33750699a52915becf03c63df4f.jpg

        redirect('admin');
    }
    public function myaccount()
    {
        $id = $this->session->id;
        if (isPost()) {
            if (empty($_POST['order_notification'])) {
                $_POST['order_notification'] = "0";
            } else {
                $_POST['order_notification'] = "1";
            }
            if (empty($_POST['email_notification'])) {
                $_POST['email_notification'] = "0";
            } else {
                $_POST['email_notification'] = "1";
            }
            if (!empty($_POST['ticket_notification'])) {
                $_POST['ticket_notification'] = json_encode($_POST['ticket_notification']);
            } else {
                $_POST['ticket_notification'] = "";
            }

            if (!empty($_POST['password'])) {
                $_POST['password'] = password_hash(trim($_POST['password']), PASSWORD_DEFAULT);
            } else {
                unset($_POST['password']);
            }
            $config['upload_path'] = FCPATH . 'assets/img/staf/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['encrypt_name'] = true;
            $this->load->library('upload', $config);

            if (validate_admin_email(strtolower(trim($_POST['email'])), $this->session->id, $this->companyid)) {
                $this->session->set_flashdata('success', lang('Your email address is not unique, please try using other email address'));
                redirect('admin/dashboard/myaccount');
                exit;
            }
            $_POST['email'] = strtolower(trim($_POST['email']));
            if ($this->session->id == $_POST['id']) {
                $adminid = $_POST['id'];
                unset($_POST['id']);

                if ($this->upload->do_upload('picture')) {
                    $_POST['picture'] = $this->upload->data('file_name');
                }

                $result = $this->Admin_model->updateAdmin($this->session->email, $_POST);

                unset($_POST['picture']);
                $client = $this->Admin_model->getAdminbyId($adminid);

                unset($client['password']);
                $client['logged'] = true;
                $client['cid'] = $this->companyid;
                //$_SESSION = $client;
                $this->session->set_userdata($client);
                $this->session->set_flashdata('success', lang('Your Account information has been updated'));
            } else {
                if ($this->session->userdata('role') == "superadmin") {
                    $adminid = $_POST['id'];
                    unset($_POST['id']);
                    if ($this->upload->do_upload('picture')) {
                        $_POST['picture'] = $this->upload->data('file_name');
                    }
                    $this->Admin_model->updateAdmin($_POST['email'], $_POST);
                    $this->session->set_flashdata('success', lang('Information has been updated, do you wish to see') . ' <a href="' . base_url() . 'admin/setting/staf">' . lang('list of the Staf') . '?</a>');
                } else {
                    $this->session->set_flashdata('error', lang('You do not have access to edit someone else profile'));
                }
            }
        }

        $this->data['staf'] = $this->Admin_model->getStaf($id);
        if (empty($this->data['staf'])) {
            redirect('errors/access_denied');
        }
        $ga = new PHPGangsta_GoogleAuthenticator();
        
        $this->data['secret'] =  $ga->createSecret();
        $this->session->set_userdata('google_secret', $this->data['secret']);
        $this->data['qrCodeUrl'] = $ga->getQRCodeGoogleUrl('EUPORTAL:'.$this->data['setting']->companyname.':'.$this->session->id.':'.$this->companyid, $this->data['secret']);
        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'setting/myaccount';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }

    public function google_2fa_auth()
    {
        $this->load->helper('string');
        $oneCode = $_POST['code'];
        $ga = new PHPGangsta_GoogleAuthenticator();
        $checkResult = $ga->verifyCode($this->session->google_secret, $oneCode, 2);    // 2 = 2*30sec clock tolerance
        if ($checkResult) {
            $backup_code =  strtoupper(random_string('alnum', 10));
            $this->db->insert('a_admin_2fa', array('adminid' =>$this->session->id, 'secret' => $this->session->google_secret, 'backup_code' => $backup_code));
            // unset($_SESSION['google_secret']);
            $this->session->unset_userdata('google_secret');
            echo json_encode(array('result' => true, 'message'  => lang('2 Factor Authentication has been activated'), 'secret' => $backup_code));
        } else {
            // unset($_SESSION['google_secret']);
            $this->session->unset_userdata('google_secret');
            echo json_encode(array('result' => false, 'message'  => lang('Failed to confirm your code maybe expired, please try again!')));
        }
    }

    public function disable_google_2fa_auth()
    {
        $oneCode = $_POST['code'];
        $ga = new PHPGangsta_GoogleAuthenticator();
        $q = $this->db->query("select * from a_admin_2fa where adminid=?", array($this->session->id));
        if ($q->num_rows()>0) {
            $checkResult = $ga->verifyCode($q->row()->secret, $oneCode, 2);    // 2 = 2*30sec clock tolerance
            if ($checkResult) {
                $this->db->where('adminid', $this->session->id);
                $this->db->delete('a_admin_2fa');
                if ($this->db->affected_rows()>0) {
                    echo json_encode(array('result' => true, 'message'  => lang('2 Factor Authentication has been deactivated, you may now delete it from your Device')));
                } else {
                    echo json_encode(array('result' => false, 'message'  => lang('Error on deleting 2factor authentication, please contact developer')));
                }
            } else {
                echo json_encode(array('result' => false, 'message'  => lang('Failed to confirm your code maybe expired, please try again!'), 'secret' =>  $q->row()->secret, 'code' => $oneCode));
            }
        } else {
            echo json_encode(array('result' => false, 'message'  => lang('Google 2 Factor authentication not found on your account')));
        }
    }
    public function disable_portout_notification()
    {
        $this->db->where('id', $this->session->id);
        $this->db->update('a_admin', array('disable_dashboard_notification' => 1));
        $this->session->set_flashdata('success', 'Portout notification will not shows on your next login');
        $this->session->sess_destroy();
        redirect('admin');
    }
    public function download_reminder()
    {
        $this->load->helper('download');
        $filename = $this->uri->segment(4);
        force_download('/home/mvno/documents/' . $this->session->cid . '/reminders/' . $filename, null);
    }
}
