<?php
defined('BASEPATH') or exit('No direct script access allowed');
class PayController extends CI_Controller
{
    protected $data = [];

    public function __construct()
    {
        // Ensure you run parent constructor
        parent::__construct();

        $this->setProperty();
    }

    public function setProperty()
    {
        $this->config->set_item('language', $_SESSION['client']['language']);
        $this->lang->load('client');
        $this->companyid = get_companyidby_url(base_url());

        $this->data['setting'] = globofix($this->companyid);
        $this->load->library('magebo', array('companyid' => $this->companyid));
        $this->load->library('sisow', array("2537491064", "503ef491ae8da9b02a9eaed101f93de602264d89"));
    }
}
class Pay extends PayController
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('Admin_model');
        $this->load->model('Client_model');
        // $this->load->library('sisow', array("2537491064", "503ef491ae8da9b02a9eaed101f93de602264d89"));
    }

    public function index()
    {
        $ideal = "";
        $paypal = "";
        $invoice = $this->magebo->getInvoice($this->uri->segment(4));
        if ($invoice->iInvoiceStatus != 52) {
            die('Your Invoice will be paid using Directdebit or has been paid, therefore online payment is disabled');
        }

        $payment = $this->magebo->getInvoiceBalance($invoice->iInvoiceNbr, $invoice->iAddressNbr);
        $amount = $invoice->mInvoiceAmount - $payment;

        $params = $this->db->query('select * from a_payment_gateway where companyid=? and name =?', array(
            $this->companyid,
            'emspay',
        ));

        $ems = json_decode($params->row()->options);
        $this->load->library('emspay', array('companyid' => $this->session->cid, 'storename' => $ems->store_name));
        $client = $this->Admin_model->getClient($_SESSION['client']['id']);
        if ($client->language == "dutch") {
            $lang = "en_GB";
        } elseif ($client->language == "french") {
            $lang = "fr_FR";
        } else {
            $lang = "en_GB";
        }
        if ($this->uri->segment(5) == "VISA") {
            $method = "V";
        } elseif ($this->uri->segment(5) == "MASTERCARD") {
            $method = "M";
        } elseif ($this->uri->segment(5) == "SOFORT") {
            $method = "sofort";
        } elseif ($this->uri->segment(5) == "PAYPAL") {
            $method = "paypal";
            $paypal = '<input type="hidden" name="sale" value="true">';
        } elseif ($this->uri->segment(5) == "IDEAL") {
            $method = "ideal";
        //$ideal .='<input type="hidden" name="idealIssuerID" value="'.$this->uri->segment(6).'">';
        } elseif ($this->uri->segment(5) == "DINNERS") {
            $method = "C";
        } elseif ($this->uri->segment(5) == "BC") {
            $method = "BCMC";
        } elseif ($this->uri->segment(5) == "MAESTRO") {
            $method = "MA";
        } else {
            $method = "M";
        }
        //id;description;quantity;item_total_price;sub_total;vat_tax;shipping
        echo '
        <html>
        <head><title>IPG Connect</title></head>
        <body>
        <p><center><h2>Please wait, or click sumbit button</h2></center>

        </p>
        <form method="post" name="myform" action="https://www.ipg-online.com/connect/gateway/processing">
        <input type="hidden" name="txntype" value="sale">
        <input type="hidden" name="timezone" value="Europe/Amsterdam"/>
        <input type="hidden" name="txndatetime" value="' . $this->emspay->getDateTime() . '"/>
        <input type="hidden" name="hash_algorithm" value="SHA256"/>
        <input type="hidden" name="hash" value="' . $this->emspay->createHash($amount, "978") . '"/>
        <input type="hidden" name="storename" value="' . $ems->store_name . '"/>
        <input type="hidden" name="mode" value="payonly"/>
        <input type="hidden" name="invoicenumber" value="' . $this->uri->segment(4) . '"/>
        <input type="hidden" name="oid" value="Invoice #' . $this->uri->segment(4) . '"/>
       <!-- <input type="hidden" name="item1" value="Proforma;' . $this->uri->segment(4) . ';1;' . $invoice->mInvoiceAmount . ';' . exvat4('21', $invoice->mInvoiceAmount) . ';' . vat('21', $invoice->mInvoiceAmount) . '"/> -->
        <input type="hidden" name="paymentMethod" value="' . $method . '"/>
        <input type="hidden" name="chargetotal" value="' . $amount . '"/>
        <input type="hidden" name="currency" value="978"/>
        ' . $ideal . ' ' . $paypal . '
        <input type="hidden" name="checkoutoption" value="combinedpage"/>
        <center> <input type="submit" value="Submit"></center>
        <input type="hidden" name="language" value="' . $lang . '">
        </form>
        <script>
        var t = setTimeout("document.myform.submit();",1000); //2 seconds measured in miliseconds
        </script>
        </body>
        </html>';
    }

    public function pay_proforma()
    {
        $this->load->model('Proforma_model');
        $ideal = "";
        $paypal = "";
        $inv = $this->Proforma_model->getInvoice($this->uri->segment(4));
        $invoice = (object) $inv;
        $amount = $invoice->total;

        $params = $this->db->query('select * from a_payment_gateway where companyid=? and name =?', array(
            $this->companyid,
            'emspay',
        ));

        $ems = json_decode($params->row()->options);
        $this->load->library('emspay', array('companyid' => $this->session->cid, 'storename' => $ems->store_name));
        $client = $this->Admin_model->getClient($_SESSION['client']['id']);
        if ($client->language == "dutch") {
            $lang = "en_GB";
        } elseif ($client->language == "french") {
            $lang = "fr_FR";
        } else {
            $lang = "en_GB";
        }
        if ($this->uri->segment(5) == "VISA") {
            $method = "V";
        } elseif ($this->uri->segment(5) == "MASTERCARD") {
            $method = "M";
        } elseif ($this->uri->segment(5) == "SOFORT") {
            $method = "sofort";
        } elseif ($this->uri->segment(5) == "PAYPAL") {
            $method = "paypal";
            $paypal = '<input type="hidden" name="sale" value="true">';
        } elseif ($this->uri->segment(5) == "IDEAL") {
            $method = "ideal";
        //$ideal .='<input type="hidden" name="idealIssuerID" value="'.$this->uri->segment(6).'">';
        } elseif ($this->uri->segment(5) == "DINNERS") {
            $method = "C";
        } elseif ($this->uri->segment(5) == "BC") {
            $method = "BCMC";
        } elseif ($this->uri->segment(5) == "MAESTRO") {
            $method = "MA";
        } else {
            $method = "M";
        }
        //id;description;quantity;item_total_price;sub_total;vat_tax;shipping
        echo '
        <html>
        <head><title>IPG Connect Sample for PHP</title></head>
        <body>
        <p><center><h2>Please wait, or click sumbit button</h2></center>

        </p>
        <form method="post" name="myform" action="https://www.ipg-online.com/connect/gateway/processing">
        <input type="hidden" name="txntype" value="sale">
        <input type="hidden" name="timezone" value="Europe/Amsterdam"/>
        <input type="hidden" name="txndatetime" value="' . $this->emspay->getDateTime() . '"/>
        <input type="hidden" name="hash_algorithm" value="SHA256"/>
        <input type="hidden" name="hash" value="' . $this->emspay->createHash($amount, "978") . '"/>
        <input type="hidden" name="storename" value="' . $ems->store_name . '"/>
        <input type="hidden" name="mode" value="payonly"/>
        <input type="hidden" name="invoicenumber" value="' . $invoice->invoicenum . '"/>
        <input type="hidden" name="oid" value="Proforma #' . $invoice->invoicenum . ' : '.rand(1000, 9999).'"/>
        <input type="hidden" name="paymentMethod" value="' . $method . '"/>
        <input type="hidden" name="chargetotal" value="' . $amount . '"/>
        <input type="hidden" name="currency" value="978"/>
        ' . $ideal . ' ' . $paypal . '
        <input type="hidden" name="checkoutoption" value="combinedpage"/>
        <center> <input type="submit" value="Submit"></center>
        <input type="hidden" name="language" value="' . $lang . '">
        </form>
        <script>
        var t = setTimeout("document.myform.submit();",1000); //2 seconds measured in miliseconds
        </script>
        </body>
        </html>';
    }
    public function sisow()
    {
        // 2537491064 and our merchant key is 503ef491ae8da9b02a9eaed101f93de602264d89

        $this->data['invoice'] = $this->magebo->getInvoice($this->uri->segment(4));
        //print_r($this->data['invoice']);
        if (isset($_POST["issuerid"])) {
            $this->sisow->purchaseId = $_POST["purchaseid"];
            $this->sisow->description = "TEST Payment";
            $this->sisow->amount = "100";
            $this->sisow->payment = $_POST['payment'];
            $this->sisow->issuerId = $_POST["issuerid"];
            $this->sisow->returnUrl = "http://" . $_SERVER["HTTP_HOST"] . $_SERVER["PHP_SELF"];
            if (($ex = $this->sisow->TransactionRequest()) < 0) {
                header("Location: payment.php?ex=" . $ex . "&ec=" . $this->sisow->errorCode . "&em=" . $this->sisow->errorMessage);
                exit;
            }
            header("Location: " . $this->sisow->issuerUrl);
        } elseif (isset($_GET["trxid"])) {
            $this->sisow->StatusRequest($_GET["trxid"]);
            // if ($sisow->status == Sisow::statusSuccess) {
            //     echo $sisow->consumerAccount;
            //     echo $sisow->consumerName; Our Merchant ID with Sisow is 2537491064 and our merchant key is 503ef491ae8da9b02a9eaed101f93de602264d89
            // }
            header("Location: payment.php?status=" . $this->sisow->status);
            exit;
        } else {
            // there are 2 methods for filling the available issuers in the select/dropdown
            // below, the REST method DirectoryRequest is used
            $this->data['select'] = $this->sisow->DirectoryRequest($select, true);
        }

        $this->load->view('themes/clear/clients/' . $this->session->cid . '/sisow_page', $this->data);
    }
    public function mollie()
    {
        $this->load->model('Proforma_model');
        if (substr($this->uri->segment(4), 0, 1) == "8" && strlen($this->uri->segment(4)) == 9) {
            $this->load->model('Admin_model');

            $invoice = $this->Proforma_model->getInvoice($this->uri->segment(4));

            $amount = $invoice['total'];
            $userid = $invoice['userid'];
            $client = $this->Admin_model->getClient($userid);
            $mageboid = $client->mageboid;
            $desc =  lang("Invoice") . ": " . $invoice['invoicenum'];
        } else {
            $invoice = $this->magebo->getInvoice($this->uri->segment(4));
            if ($invoice->iInvoiceStatus != 52) {
                die('Your Invoice will be paid using Directdebit or has been paid, therefore online payment is disabled');
            }
            $payment = $this->magebo->getInvoiceBalance($invoice->iInvoiceNbr, $invoice->iAddressNbr);
            $amount = $invoice->mInvoiceAmount - $payment;
            $userid = getWhmcsid($invoice->iAddressNbr);
            $mageboid = $invoice->iAddressNbr;
            $desc =  lang("Invoice") . ": " . $this->uri->segment(4);
        }

        $invoiceid = $this->uri->segment(4);
        $mollie = new \Mollie\Api\MollieApiClient();

        if ($this->data['setting']->molie_api_env == "live") {
            $mollie->setApiKey($this->data['setting']->molie_api_live);
        } else {
            $mollie->setApiKey($this->data['setting']->molie_api_sandbox);
        }
        //echo $this->uri->segment(5);
        //exit;
        $payment = $mollie->payments->create([
        "amount" => [
        "currency" => "EUR",
        "value" =>  number_format($amount, 2)
        ],
        "metadata" => [
            "invoiceid" => $invoiceid
        ],
        /*"customerId" => "cst_".$userid, */
        "method" =>$this->uri->segment(5),
        "mandateId"  => "mdt_".$mageboid,
        "description" => $desc,
        "redirectUrl" => base_url()."client/invoice",
        "webhookUrl"  =>  base_url()."event/mollie_ipn",
]);
        //After creation, the payment id is available in the $payment->id property. You should store this id with your order.

        //After storing the payment id you can send the customer to the checkout using the $payment->getCheckoutUrl().

        header("Location: " . $payment->getCheckoutUrl(), true, 303);
    }
    public function process()
    {
        $this->load->library('emspay', array('companyid' => $this->session->cid));
        if ($client->language == "dutch") {
            $lang = "nl_NL";
        } else {
            $lang = "en_GB";
        }
        echo '
      <html>
      <head><title>IPG Connect Sample for PHP</title></head>
      <body>
      <p><h1>Order Form</h1>
      <form method="post" action="https://www.ipg-online.com/connect/gateway/processing">
      <input type="hidden" name="txntype" value="sale">
      <input type="hidden" name="timezone" value="Europe/Amsterdam"/>
      <input type="hidden" name="txndatetime" value="' . $this->emspay->getDateTime() . '"/>
      <input type=”hidden” name="hash_algorithm" value="SHA256"/>
      <input type="hidden" name="hash" value="' . $this->emspay->createHash("13.00", "978") . '"/>
      <input type="hidden" name="storename" value="2364131445"/>
      <input type="hidden" name="mode" value="payonly"/>
      <input type="hidden" name="paymentMethod" value="M"/>
      <input type="text" name="chargetotal" value="13.00"/>
      <input type="hidden" name="currency" value="978"/>
      ' . $ideal . '
      <input type="hidden" name="checkoutoption" value="combinedpage"/>
      <input type="submit" value="Submit">
      <input type="hidden" name="language" value="' . $lang . '">
      </form>
      </body>
      </html>';
    }

    public function paypal_success()
    {
        $this->session->set_flashdata('success', 'Your payment has been accepted');
        redirect('client');
    }

    public function paypal_cancel()
    {
        $this->session->set_flashdata('error', 'Your payment has been cancelled');
        redirect('client');
    }

    public function ipn()
    {
        if ($this->uri->segment(4) == "payment_success") {
            $this->session->set_flashdata('success', 'Thank you for your payment, your payment will be processed shortly');
        } else {
            $this->session->set_flashdata('error', 'Payment has been cancelled');
        }
        redirect('client');
    }

    public function payu_cop()
    {
        $this->load->model('Admin_model');
        try {
            require_once APPPATH.'third_party/PayU.php';
            PayU::$apiKey = "4Vj8eK4rloUd272L48hsrarnUA"; //Enter your own apiKey here.
        PayU::$apiLogin = "pRRXKOl8ikMmt9u"; //Enter your own apiLogin here.
        PayU::$merchantId = "508029"; //Enter your commerce Id here.
        PayU::$language = SupportedLanguages::EN; //Select the language.
        PayU::$isTest = true; //Leave it True when testing.
       // PayU.paymentsUrl = “https://sandbox.api.payulatam.com/payments-api/”;
       // PayU.reportsUrl = “https://sandbox.api.payulatam.com/reports-api/”;

            Environment::setPaymentsCustomUrl("https://sandbox.api.payulatam.com/payments-api/4.0/service.cgi");
            Environment::setReportsCustomUrl("https://sandbox.api.payulatam.com/reports-api/4.0/service.cgi");
            Environment::setSubscriptionsCustomUrl("https://sandbox.api.payulatam.com/payments-api/rest/v4.9/");
            if (!empty($_POST['serviceid'])) {
                $service = $this->Admin_model->getServiceCLI($_POST['serviceid']);
                $client = $this->Admin_model->getClient($service->userid);
            } else {
                $q = $this->db->query("select * from a_services_mobile where msisdn=?", array($_POST['msisdn']));
                if ($q->num_rows()>0) {
                    $service =$this->Admin_model->getService($q->row()->serviceid);
                    $client = $this->Admin_model->getClient($service->userid);
                } else {
                    $this->session->set_flashdata('error', 'Your phone number is incorrect');
                    redirect('client/order/mobile');
                }
            }
            if ($_POST['type'] == "reload") {
                $reference =  $service->id."-".$service->details->msisdn."-".time()."-RELOAD"; //must be unique!
            } else {
                $reference =  $service->id."-".$service->details->msisdn."-".time()."-BUNDLE-".$_POST['bundleid']; //must be unique!
            }
            $value =  $_POST['amount'];

            $parameters = array(
    // Enter the account’s identifier here.
    PayUParameters::ACCOUNT_ID => "512321",
    // Enter the reference code here.
    PayUParameters::REFERENCE_CODE => $reference,
    // Enter the description here.
    PayUParameters::DESCRIPTION => $_POST['type'].' '.$service->details->msisdn,

    // -- Values --
        // Enter the value here.
    PayUParameters::VALUE => $value,
        // Enter the value of the VAT (Value Added Tax only valid for Colombia) of the transaction,
        // if no VAT is sent, the system will apply 19% automatically. It can contain two decimal digits.
        // Example 19000.00. In case you have no VAT you should fill out 0.
    PayUParameters::TAX_VALUE => "3193",
        // Enter the value of the base value on which VAT (only valid for Colombia) is calculated.
        // If you do not have VAT should be sent to 0.
    PayUParameters::TAX_RETURN_BASE => "16806",
    // Enter the currency here.
    PayUParameters::CURRENCY => "COP",

    // -- Buyer --
        //Enter the buyer Id here.
    PayUParameters::BUYER_NAME => $client->firstname.' '.$client->lastname,
    //Enter the buyer's email here.
    PayUParameters::BUYER_EMAIL => $client->email,
    //Enter the buyer's contact phone here.
    PayUParameters::BUYER_CONTACT_PHONE => $client->phonenumber,
    //Enter the buyer's contact document here.
    PayUParameters::BUYER_DNI => $client->idcard_number,
    //Enter the buyer's address here.
    PayUParameters::BUYER_STREET => $client->address1.' '.$client->housenumber,
    PayUParameters::BUYER_STREET_2 => "",
    PayUParameters::BUYER_CITY => $client->city,
    PayUParameters::BUYER_STATE => $client->city,
    PayUParameters::BUYER_COUNTRY => $client->country,
    PayUParameters::BUYER_POSTAL_CODE =>$client->postcode,
    PayUParameters::BUYER_PHONE =>$service->details->msisdn,

    // -- Payer --
    //Enter the payer's name here.
    PayUParameters::PAYER_NAME => $client->firstname.' '.$client->lastname,
    //Enter the payer's email here.
    PayUParameters::PAYER_EMAIL => $client->email,
    //Enter the payer's contact phone here.
    PayUParameters::PAYER_CONTACT_PHONE => $client->phonenumber,
    //Enter the payer's contact document here.
    PayUParameters::PAYER_DNI => $client->idcard_number,
    //Enter the payer's address here.
    PayUParameters::PAYER_STREET => $client->address1.' '.$client->housenumber,
    PayUParameters::PAYER_STREET_2 => "",
    PayUParameters::PAYER_CITY => $client->city,
    PayUParameters::PAYER_STATE => $client->city,
    PayUParameters::PAYER_COUNTRY => $client->country,
    PayUParameters::PAYER_POSTAL_CODE => $client->postcode,
    PayUParameters::PAYER_PHONE => $service->details->msisdn,

    // -- Credit card data --
    // Enter the number of the credit card here
    PayUParameters::CREDIT_CARD_NUMBER => $_POST['CC_NUMBER'],
    // Enter expiration date of the credit card here
    PayUParameters::CREDIT_CARD_EXPIRATION_DATE => $_POST['CC_EXP'],
    //Enter the security code of the credit card here YYYY/MM
    PayUParameters::CREDIT_CARD_SECURITY_CODE=> $_POST['CC_CCV'],
    //Enter the name of the credit card here
    //VISA||MASTERCARD||AMEX||DINERS
    PayUParameters::PAYMENT_METHOD => $_POST['CC_TYPE'],

    // Enter the number of installments here.
    PayUParameters::INSTALLMENTS_NUMBER => "1",
    // Enter the name of the country here.
    PayUParameters::COUNTRY => PayUCountries::CO,

    //Session id del device.
    PayUParameters::DEVICE_SESSION_ID => md5($_SESSION['client']['id']),
    // Payer IP
    PayUParameters::IP_ADDRESS => $_SERVER['REMOTE_ADDT'],
    // Cookie of the current session.
    PayUParameters::PAYER_COOKIE=> "pt1t38347bs6jc9ruv2ecpv7o2",
    // User agent of the current session.
    PayUParameters::USER_AGENT=>"Mozilla/5.0 (Windows NT 5.1; rv:18.0) Gecko/20100101 Firefox/18.0"
);

            $response = PayUPayments::doAuthorizationAndCapture($parameters);
            print_r($response);
            if ($response) {
                $response->transactionResponse->orderId;
                $response->transactionResponse->transactionId;
                $response->transactionResponse->state;
                if ($response->transactionResponse->state=="PENDING") {
                    $response->transactionResponse->pendingReason;
                }
                $response->transactionResponse->paymentNetworkResponseCode;
                $response->transactionResponse->paymentNetworkResponseErrorMessage;
                $response->transactionResponse->trazabilityCode;
                $response->transactionResponse->responseCode;
                $response->transactionResponse->responseMessage;
            }
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }

    public function payu()
    {
        $this->load->model('Admin_model');
        log_message('error', print_r($_POST, 1));
        $service = $this->Admin_model->getService($this->uri->segment(4));
        
        /*
        POS ID (pos_id):	363089
        Second key (MD5):	5aa67c2aca162d781aea42c9cd0c75fa
        OAuth protocol - client_id :	363089
        OAuth protocol - client_secret:	6dc4e83ee2251c2cf5383ed5e0cbbdab
        */

        /*
                POS ID (pos_id):	363090
                Second key (MD5):	2eca4b5f25507b54e411db45ca8385c3
        OAuth protocol - client_id :	363090
        OAuth protocol - client_secret:	2348faed97a12392637667f9db590cbb
        */
        OpenPayU_Configuration::setEnvironment($this->data['setting']->payu_env);

        //set POS ID and Second MD5 Key (from merchant admin panel)
        OpenPayU_Configuration::setMerchantPosId($this->data['setting']->payu_pos_id);
        OpenPayU_Configuration::setSignatureKey($this->data['setting']->payu_key);

        //set Oauth Client Id and Oauth Client Secret (from merchant admin panel)
        OpenPayU_Configuration::setOauthClientId($this->data['setting']->payu_client_id);
        OpenPayU_Configuration::setOauthClientSecret($this->data['setting']->payu_client_secret);


        $order['continueUrl'] = base_url().'client'; //customer will be redirected to this page after successfull payment
        $order['notifyUrl'] = base_url().'event/payu_ipn';
        $order['customerIp'] = $_SERVER['REMOTE_ADDR'];
        $order['merchantPosId'] = OpenPayU_Configuration::getMerchantPosId();


        $order['description'] =  $_POST['description'];

        if ($this->data['setting']->payu_env == "sandbox") {
            $order['currencyCode'] = 'PLN';
        } else {
            $order['currencyCode'] = $this->data['setting']->currency_code;
        }
        
        $order['totalAmount'] = $_POST['amount'];

        if ($_POST['type'] == "reload") {
            $order['extOrderId'] =  $service->id."-".$service->details->msisdn."-".time()."-RELOAD"; //must be unique!
        } else {
            $order['extOrderId'] =  $service->id."-".$service->details->msisdn."-".time()."-BUNDLE-".$_POST['bundleid']; //must be unique!
        }
       


        $order['products'][0]['name'] = $_POST['name'];
        $order['products'][0]['unitPrice'] = 1000;
        $order['products'][0]['quantity'] = $_POST['qty'];


        //optional section buyer
        if ($this->data['setting']->payu_env == "sandbox") {
            $order['buyer']['email'] = 'dd@ddd.pl';
            $order['buyer']['phone'] = '123123123';
            $order['buyer']['firstName'] = 'Jan';
            $order['buyer']['lastName'] = 'Kowalski';
        } else {
            $order['buyer']['email'] = $client->email;
            $order['buyer']['phone'] = $client->phonenumber;
            $order['buyer']['firstName'] = $client->firstname;
            $order['buyer']['lastName'] =  $client->lastname;
        }
 

        $response = OpenPayU_Order::create($order);

        echo json_encode(array('result' => true, 'url' => $response->getResponse()->redirectUri)); //You must redirect your client to PayU payment summary page.
    }
    public function paypal()
    {
        $this->load->model('Proforma_model');
        $this->load->library('paypal_lib', array('config' => $this->data['setting']));
        $returnURL = base_url() . 'client/pay/paypal_success';
        $cancelURL = base_url() . 'client/pay/paypal_cancel';
        $notifyURL = base_url() . 'event/paypal_ipn';

        if (substr($this->uri->segment(4), 0, 1) == "8" && strlen($this->uri->segment(4)) == 9) {
            $invoice = $this->Proforma_model->getInvoice($this->uri->segment(4));

            //print_r($invoice);
            //exit;
            // Get current user ID from the session
            //$payment = $this->magebo->getInvoiceBalance($invoice->iInvoiceNbr, $invoice->iAddressNbr);
            $amount = $invoice['total'];
            $userID = $_SESSION['client']['id'];

            // Add fields to paypal form
            $this->paypal_lib->add_field('return', $returnURL);
            $this->paypal_lib->add_field('cancel_return', $cancelURL);
            $this->paypal_lib->add_field('notify_url', $notifyURL);
            $this->paypal_lib->add_field('item_name', lang("Invoice") . ": " . $invoice['invoicenum']);
            $this->paypal_lib->add_field('custom', $this->uri->segment(4));
            $this->paypal_lib->add_field('item_number', $invoice['invoicenum']);
            $this->paypal_lib->add_field('amount', round($amount, 2));

            // Render paypal form
            $this->paypal_lib->paypal_auto_form();
        } else {
            $invoice = $this->magebo->getInvoice($this->uri->segment(4));
            //print_r($invoice);
            //exit;
            // Get current user ID from the session
            if ($invoice->iInvoiceStatus != 52) {
                die('Your Invoice will be paid using Directdebit or has been paid, therefore online payment is disabled');
            }
            $payment = $this->magebo->getInvoiceBalance($invoice->iInvoiceNbr, $invoice->iAddressNbr);
            $amount = $invoice->mInvoiceAmount - $payment;
            $userID = $_SESSION['client']['id'];

            // Add fields to paypal form
            $this->paypal_lib->add_field('return', $returnURL);
            $this->paypal_lib->add_field('cancel_return', $cancelURL);
            $this->paypal_lib->add_field('notify_url', $notifyURL);
            $this->paypal_lib->add_field('item_name', lang("Invoice") . ": " . $this->uri->segment(4));
            $this->paypal_lib->add_field('custom', $this->uri->segment(4));
            $this->paypal_lib->add_field('item_number', $this->uri->segment(4));
            $this->paypal_lib->add_field('amount', round($amount, 2));

            // Render paypal form
            $this->paypal_lib->paypal_auto_form();
        }
    }
}
