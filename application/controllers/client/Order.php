<?php
defined('BASEPATH') or exit('No direct script access allowed');
class OrderController extends CI_Controller
{

    protected $data = [];

    public function __construct()
    {
        // Ensure you run parent constructor
        parent::__construct();

        $this->setProperty();
    }

    public function setProperty()
    {
        IF(!empty($_SESSION['client'])){
 $this->config->set_item('language', $_SESSION['client']['language']);
        }else{
             $this->config->set_item('language', 'english');
        }
       
        $this->lang->load('client');
        $this->companyid = get_companyidby_url(base_url());

        $this->data['setting'] = globofix($this->companyid);
        $this->load->library('magebo', array('companyid' => $this->companyid));
        $this->load->library('sisow', array("2537491064", "503ef491ae8da9b02a9eaed101f93de602264d89"));
    }
}
class Order extends OrderController
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model('Admin_model');
        $this->load->model('Client_model');
        // $this->load->library('sisow', array("2537491064", "503ef491ae8da9b02a9eaed101f93de602264d89"));
    }

    public function index()
    {

        $this->data['title'] = $this->data['setting']->companyname;
        $this->data['dtt'] = true;
        //$this->data['main_content'] = 'themes/' . $this->data['setting']->default_theme . '/clients/' . $this->session->cid . '/order/step1';
        $this->load->view('themes/' . $this->data['setting']->default_theme . '/clients/' . $this->session->cid . '/order/step1', $this->data);

    }

     public function internet()
    {
        $this->data['logged'] = false;
        $this->load->model('Teams_model');
          $this->load->model('Admin_model');
        
        if(empty($_SESSION['client']['id'])){
  if(isset($_GET['code'])) {
    $this->load->helper('string');
        $this->data['logged'] = true;
        // Get the access token 
        $data = $this->Teams_model->GetAccessToken($this->data['setting']->google_client_id, $this->data['setting']->google_redirect_url, $this->data['setting']->google_client_secret, $_GET['code']);

        // Access Tokem
        $access_token = $data['access_token'];
         // print_r($data);
        // Get user information
        $user_info = $this->Teams_model->GetUserProfileInfo($access_token);

        if($user_info['verified_email'] == 1){

        //    print_r($user_info);

            $password=random_string('alnum', 16);
            $clientid = $this->Teams_model->getClientbyEmail($user_info, $password);
          
            if($clientid['result']){
                $c = $this->Admin_model->getClient($clientid['id']);
                if($clientid['type'] == "new"){
                    $this->send_welcome($c, $password);
                }
                $valid['client'] = (array) $c;
                $valid['client']['islogged'] = true;
                unset($valid['client']['password']);
                $this->session->set_userdata('client', $valid['client']);
            }
        }else{

           
        }
     
    
    }


        }else{

               $this->data['logged'] = true;
        }
      
       // print_r($this->data);
        $this->data['redirect']  = 'https://accounts.google.com/o/oauth2/v2/auth?scope=' . urlencode('https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email') . '&redirect_uri=' . urlencode($this->data['setting']->google_redirect_url) . '&response_type=code&client_id=' . $this->data['setting']->google_client_id . '&access_type=online';
        $this->data['title'] = $this->data['setting']->companyname;
        $this->data['dtt'] = true;
        $this->data['channel'] = rand(100000,99999999);
        //$this->data['main_content'] = 'themes/' . $this->data['setting']->default_theme . '/clients/' . $this->session->cid . '/order/step1';
        $this->load->view('themes/' . $this->data['setting']->default_theme . '/clients/' . $this->session->cid . '/order_internet', $this->data);

    }

    public function send_welcome($client, $password){
               if ($this->data['setting']->smtp_type == 'smtp') {
                    $config = array(
                        'protocol' => 'smtp',
                        'smtp_host' => $this->data['setting']->smtp_host,
                        'smtp_port' => $this->data['setting']->smtp_port,
                        'smtp_user' => $this->data['setting']->smtp_user,
                        'smtp_pass' => $this->encryption->decrypt($this->data['setting']->smtp_pass),
                        'mailtype' => 'html',
                        'charset' => 'utf-8',
                        'starttls' => true,
                        'wordwrap' => true,
                    );
                } else {
                    $config['protocol'] = 'sendmail';
                    $config['mailpath'] = '/usr/sbin/sendmail';
                    $config['mailtype'] = 'html';
                    $config['charset'] = 'utf-8';
                    $config['wordwrap'] = true;
                }
                $this->email->clear(true);
                $this->email->initialize($config);
                // $this->data['info'] = (object) array('email' => $_POST['email'], 'password' => $password, 'name' => $_POST['firstname'] . ' ' . $_POST['lastname']);
                //$this->data['language'] = "dutch";
                //$this->data['main_content'] = 'email/' . $this->data['language'] . '/signup_email.php';
                //$body = $this->load->view('email/content', $this->data, true);
               // $client = $this->Admin_model->getClient($client->id);
                $body = getMailContent('signup_email', $client->language, $client->companyid);
                // $body = $this->load->view('email/content', $this->data, true);
                $body = str_replace('{$name}', $client->firstname . ' ' . $client->lastname, $body);
                $body = str_replace('{$password}', $password, $body);
                $body = str_replace('{$email}', trim(strtolower($_POST['email'])), $body);
                $body = str_replace('{$base_url}', url_to_domain(base_url()), $body);
                $this->email->set_newline("\r\n");
                $this->email->from($this->data['setting']->smtp_sender, $this->data['setting']->smtp_name);
                $this->email->to(strtolower(trim($_POST['email'])));
                //$this->email->bcc('lainard@gmail.com');
                $this->email->subject(getSubject('signup_email', $client->language, $client->companyid));
                $this->email->message($body);
                if ($this->email->send()) {
                    logEmailOut(array('userid' => $client->id, 'companyid' => $client->companyid, 'to' => $client->email, 'subject' => getSubject('signup_email', $client->language, $client->companyid), 'message' => $body));
                } else {
                    logEmailOut(array('userid' => $client->id, 'companyid' => $client->companyid, 'to' => $client->email, 'subject' => getSubject('signup_email', $client->language, $client->companyid), 'message' => $body, 'status' => 'error', 'error_message' => $this->email->print_debugger()));
                }

                logAdmin(array('companyid' => $this->session->cid, 'userid' => $client->id, 'user' => 'System', 'ip' => $_SERVER['REMOTE_ADDR'], 'description' => 'Adding customer ' . $client->id));
    }

}