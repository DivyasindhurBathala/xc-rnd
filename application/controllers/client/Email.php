<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class ClientEmailController extends CI_Controller {

	protected $data = [];

	public function __construct() {
		// Ensure you run parent constructor
		parent::__construct();
		$this->config->set_item('language', $this->session->userdata('language'));
		$this->lang->load('client');
		$this->companyid = get_companyidby_url(base_url());

		$this->data['setting'] = globofix($this->companyid);
	}

}
class Email extends ClientEmailController {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 *     http://example.com/index.php/welcome
	 *  - or -
	 *     http://example.com/index.php/welcome/index
	 *  - or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct() {
		parent::__construct();
		/*
			$classes = get_class_methods($this);
			foreach ($classes as $class) {
				if ($class != 'setProperty' && $class != 'get_instance' && $class != '__construct') {
					$this->db->insert('admin_permissions', array('controller' => 'admin/creditnote', 'method' => $class));
				}

			}
		*/

		$this->load->model('Client_model');

	}

	public function index() {
		//print_r($_SESSION);
		$this->data['stats'] = $this->Client_model->getStats($_SESSION['client']['id']);
		$this->data['title'] = $this->data['setting']->companyname;
		$this->data['dtt'] = 'client_email.js';
		$this->data['title'] = "Exocom Dasboard";
		$this->data['main_content'] = 'themes/' . $this->data['setting']->default_theme . '/clients/'.$this->session->cid.'/client_emails';
		$this->load->view('themes/' . $this->data['setting']->default_theme . '/clients/' . 'content', $this->data);
	}

	public function add_email() {
		if (isPost()) {
			$post = $_POST;

			$this->db->where('id', $_SESSION['client']['id']);

			$this->db->update('clients', $post);
			if ($this->db->affected_rows() > 0) {
				$_SESSION = $post;
				$_SESSION['islogged'] = true;
				$this->session->set_flashdata('success', lang('Your information has been saved'));
			}
		}
		//print_r($_SESSION);
		//$this->data['stats'] = $this->Client_model->getStats($_SESSION['id']);
		$this->data['title'] = $this->data['setting']->companyname;
		$this->data['client'] = (object) $_SESSION['client'];
		$this->data['title'] = "Exocom Dasboard";
		$this->data['main_content'] = 'themes/' . $this->data['setting']->default_theme . '/clients/'.$this->session->cid.'/myaccount';
		$this->load->view('themes/' . $this->data['setting']->default_theme . '/clients/' . 'content', $this->data);
	}

	public function edit_email() {
		if (isPost()) {
			$post = $_POST;

			$this->db->where('id', $_SESSION['client']['id']);

			$this->db->update('clients', $post);
			if ($this->db->affected_rows() > 0) {
				$_SESSION['client'] = $post;
				$_SESSION['islogged'] = true;
				$this->session->set_flashdata('success', lang('Your information has been saved'));
			}
		}
		//print_r($_SESSION);
		//$this->data['stats'] = $this->Client_model->getStats($_SESSION['id']);
		$this->data['title'] = $this->data['setting']->companyname;
		$this->data['client'] = (object) $_SESSION['client'];
		$this->data['title'] = "Exocom Dasboard";
		$this->data['main_content'] = 'themes/' . $this->data['setting']->default_theme . '/clients/'.$this->session->cid.'/myaccount';
		$this->load->view('themes/' . $this->data['setting']->default_theme . '/clients/' . 'content', $this->data);
	}

	public function delete_email() {
		if (isPost()) {
			$post = $_POST;

			$this->db->where('id', $_SESSION['client']['id']);

			$this->db->update('clients', $post);
			if ($this->db->affected_rows() > 0) {
				$_SESSION = $post;
				$_SESSION['islogged'] = true;
				$this->session->set_flashdata('success', lang('Your information has been saved'));
			}
		}
		redirect('client/email');
	}
}
