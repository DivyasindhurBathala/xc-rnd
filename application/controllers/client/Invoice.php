<?php
defined('BASEPATH') or exit('No direct script access allowed');
class InvoiceController extends CI_Controller
{

    protected $data = [];

    public function __construct()
    {
        // Ensure you run parent constructor
        parent::__construct();

        $this->companyid = get_companyidby_url(base_url());

        $this->data['setting'] = globofix($this->companyid);
        $this->config->set_item('language', $_SESSION['client']['language']);
        $this->lang->load('client');
    }
}
class Invoice extends InvoiceController
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Admin_model');
        $this->load->model('Client_model');
    }

    public function index()
    {
        //print_r($_SESSION['client']['state']);
        $this->data['title'] = $this->data['setting']->companyname;
        $this->data['dtt'] = true;
        $this->data['main_content'] = 'themes/' . $this->data['setting']->default_theme . '/clients/' . $this->session->cid . '/client_invoices';
        $this->load->view('themes/' . $this->data['setting']->default_theme . '/clients/' . $this->session->cid . '/content', $this->data);
    }

     public function creditnotes()
    {
        //print_r($_SESSION['client']['state']);
        $this->data['title'] = $this->data['setting']->companyname;
        $this->data['dtt'] = true;
        $this->data['main_content'] = 'themes/' . $this->data['setting']->default_theme . '/clients/' . $this->session->cid . '/client_creditnotes';
        $this->load->view('themes/' . $this->data['setting']->default_theme . '/clients/' . $this->session->cid . '/content', $this->data);
    }

    public function pay()
    {
        $live = "live_z8wbDeH4z4ANTpcmQKpy87RvMH9tea";
        $test = "test_lj3QAVk44vstI9VJTuukZAP0siDJVN";
        $invoice = $this->Invoice_model->getInvoice($this->uri->segment(4));
        //print_r($invoice);
        require APPPATH . "third_party/mollie/src/Mollie/API/Autoloader.php";

        $mollie = new Mollie_API_Client;
        $mollie->setApiKey("live_z8wbDeH4z4ANTpcmQKpy87RvMH9tea");

        $payment = $mollie->payments->create(array(
            "amount" => $invoice['total'],
            "description" => "Factuur #" . $invoice['invoicenum'],
            "redirectUrl" => base_url() . 'client/invoice',
            "webhookUrl" => base_url() . 'payment/mollie',
            "metadata" => array('invoiceid' => $invoice['id']),
        ));

        $this->Invoice_model->insertmollie(array('paymentid' => $payment->id, 'invoiceid' => $invoice['id'], 'userid' => $invoice['userid']));
        header('Location: ' . $payment->links->paymentUrl);
    }
    public function detail()
    {
        redirect('client/invoice/download/' . $this->uri->segment(4));
    }
    public function download()
    {
        $id = $this->uri->segment(4);

        $client = $this->Admin_model->getClient($this->session->client['id']);
        if (!file_exists($this->data['setting']->DOC_PATH . '/' . $this->companyid . '/invoices/')) {
            mkdir($this->data['setting']->DOC_PATH . '/' . $this->companyid . '/invoices/', 0755, true);
        }
        if (!empty($this->uri->segment(5))) {
            unlink($this->data['setting']->DOC_PATH . '/' . $this->companyid . '/invoices/withcdr/' . $id . '.pdf');
            unlink($this->data['setting']->DOC_PATH . '/' . $this->companyid . '/invoices/' . $id . '.pdf');

        }
        if (file_exists($this->data['setting']->DOC_PATH . '/' . $this->companyid . '/invoices/withcdr/' . $id . '.pdf')) {
            header('Content-type: application/pdf');
            header('Content-Disposition: inline; filename="' . $id . '.pdf"');
            header('Content-Transfer-Encoding: binary');
            header('Accept-Ranges: bytes');
            @readfile($this->data['setting']->DOC_PATH . '/' . $this->companyid . '/invoices/withcdr/' . $id . '.pdf');
            exit;
        }
        if (file_exists($this->data['setting']->DOC_PATH . '/' . $this->companyid . '/invoices/' . $id . '.pdf')) {
            header('Content-type: application/pdf');
            header('Content-Disposition: inline; filename="' . $id . '.pdf"');
            header('Content-Transfer-Encoding: binary');
            header('Accept-Ranges: bytes');
            @readfile($this->data['setting']->DOC_PATH . '/' . $this->companyid . '/invoices/' . $id . '.pdf');
            exit;
        }
        $invoice_path = $this->data['setting']->DOC_PATH . '/' . $this->companyid . '/invoices/';

        $id = $this->uri->segment(4);
        if ($this->data['setting']->companyid == 2) {
            $companyid = array(1, 2, 3, 4, 6, 8, 9, 25, 26, 28, 29, 31, 34, 35, 37, 41, 47, 50);
        } else {
            $companyid[] = $this->companyid;
        }

        if ($this->Client_model->IsAllowedInvoice($id, $client->mageboid)) {
            $invoice = $this->Admin_model->getInvoicePdf($id);

            //print_r($invoice->PDF);
            // exit;

            $file = $invoice_path . $id . '.pdf';

            file_put_contents($file, $invoice->PDF);
            if (file_exists($file)) {
                //if ($this->session->cid != "53") {
                    $cdr = $this->downloadcdrpdf($id, $this->session->cid);
                    if ($cdr) {
                        shell_exec('pdftk ' . $file . ' ' . $cdr . ' cat output ' . $this->data['setting']->DOC_PATH . $this->session->cid . '/invoices/withcdr/' . $id . '.pdf');
                        unlink($file);
                        copy($this->data['setting']->DOC_PATH . $this->session->cid . '/invoices/withcdr/' . $id . '.pdf', $file);
                        $file = $this->data['setting']->DOC_PATH . $this->session->cid . '/invoices/withcdr/' . $id . '.pdf';
                    }
                //}

                header('Content-type: application/pdf');
                header('Content-Disposition: inline; filename="' . $id . '.pdf"');
                header('Content-Transfer-Encoding: binary');
                header('Accept-Ranges: bytes');
                @readfile($file);
            } else {
                echo "file does not exists";
            }
        } else {
            $this->session->set_flashdata('error', lang('This acction has been denied'));
            redirect('errors/access_denied');
        }
    }

    public function downloadcn()
    {
        $id = $this->uri->segment(4);

        $client = $this->Admin_model->getClient($this->session->client['id']);
        if (!file_exists($this->data['setting']->DOC_PATH . '/' . $this->companyid . '/invoices/')) {
            mkdir($this->data['setting']->DOC_PATH . '/' . $this->companyid . '/invoices/', 0755, true);
        }
        if (!empty($this->uri->segment(5))) {
            unlink($this->data['setting']->DOC_PATH . '/' . $this->companyid . '/invoices/withcdr/' . $id . '.pdf');
            unlink($this->data['setting']->DOC_PATH . '/' . $this->companyid . '/invoices/' . $id . '.pdf');

        }
        if (file_exists($this->data['setting']->DOC_PATH . '/' . $this->companyid . '/invoices/withcdr/' . $id . '.pdf')) {
            header('Content-type: application/pdf');
            header('Content-Disposition: inline; filename="' . $id . '.pdf"');
            header('Content-Transfer-Encoding: binary');
            header('Accept-Ranges: bytes');
            @readfile($this->data['setting']->DOC_PATH . '/' . $this->companyid . '/invoices/withcdr/' . $id . '.pdf');
            exit;
        }
        if (file_exists($this->data['setting']->DOC_PATH . '/' . $this->companyid . '/invoices/' . $id . '.pdf')) {
            header('Content-type: application/pdf');
            header('Content-Disposition: inline; filename="' . $id . '.pdf"');
            header('Content-Transfer-Encoding: binary');
            header('Accept-Ranges: bytes');
            @readfile($this->data['setting']->DOC_PATH . '/' . $this->companyid . '/invoices/' . $id . '.pdf');
            exit;
        }
        $invoice_path = $this->data['setting']->DOC_PATH . '/' . $this->companyid . '/invoices/';

        $id = $this->uri->segment(4);
        if ($this->data['setting']->companyid == 2) {
            $companyid = array(1, 2, 3, 4, 6, 8, 9, 25, 26, 28, 29, 31, 34, 35, 37, 41, 47, 50);
        } else {
            $companyid[] = $this->companyid;
        }

        if ($this->Client_model->IsAllowedCn($id, $client->mageboid)) {
            $invoice = $this->Admin_model->getInvoicePdf($id);

            //print_r($invoice->PDF);
            // exit;

            $file = $invoice_path . $id . '.pdf';

            file_put_contents($file, $invoice->PDF);
            if (file_exists($file)) {
                if ($this->session->cid != "53") {
                    $cdr = $this->downloadcdrpdf($id, $this->session->cid);
                    if ($cdr) {
                        shell_exec('pdftk ' . $file . ' ' . $cdr . ' cat output ' . $this->data['setting']->DOC_PATH . $this->session->cid . '/invoices/withcdr/' . $id . '.pdf');
                        unlink($file);
                        copy($this->data['setting']->DOC_PATH . $this->session->cid . '/invoices/withcdr/' . $id . '.pdf', $file);
                        $file = $this->data['setting']->DOC_PATH . $this->session->cid . '/invoices/withcdr/' . $id . '.pdf';
                    }
                }

                header('Content-type: application/pdf');
                header('Content-Disposition: inline; filename="' . $id . '.pdf"');
                header('Content-Transfer-Encoding: binary');
                header('Accept-Ranges: bytes');
                @readfile($file);
            } else {
                echo "file does not exists";
            }
        } else {
            $this->session->set_flashdata('error', lang('This acction has been denied'));
            redirect('errors/access_denied');
        }
    }
    public function downloadcdrpdf($id, $companyid)
    {
        error_reporting(1);
        $setting = globofix($companyid);
        //$mageboid = $this->uri->segment(5);
        $this->load->library('magebo', array('companyid', $companyid));
        $cdrs = $this->magebo->getInvoiceCdrFile($id);
        $gprs = $this->magebo->getDataCdr($id);
        // print_r($cdrs);
        //$cdrs = $this->Admin_model->mageboGet('GetCdrs/' . $id);
        // exit;
        if (!$cdrs) {
            if ($gprs) {
                $gp = "";
            } else {
                return false;
            }
        } else {
            //print_r($cdrs);
            // exit;
            $table = '<h2>CDR Voice & SMS: ' . $pincode . '</h2><br />
            <table><thead>
            <tr bgcolor="#111" color="#fff" cellpadding="1" cellspacing="0">
            <th width="12%">' . lang('Date') . '</th>
            <th width="10%">' . lang('Number') . '</th>
            <th width="15%">' . lang('Destination') . '</th>
            <th width="42%">' . lang('Description') . '</th>
            <th width="10%">' . lang('Duration') . '</th>
            <th width="10%">' . lang('Cost') . ' (incl. VAT)</th>
            </tr>
            </thead>
            <tbody>
            ';
           // mail('mail@simson.one','cdr',print_r($cdrs, true));
            foreach ($cdrs as $array) {
                $table .= '
                <tr>
                <td width="12%">' . substr($array->dCallDate, 0, -3) . '</td>
                <td width="10%">' . $array->iPincode . '</td>
                <td width="15%">' . $array->cDialedNumber . '</td>
                <td width="42%">' . $array->cInvoiceGroupDescription.' - ' .$array->cCallTranslation. '</td>
                <td width="10%">' . gmdate("H:i:s", $array->iInvoiceDuration) . '</td>
                <td width="10%">'.$this->data['setting']->currency . str_replace('.', ',', includevat('21', $array->mInvoiceSale)) . '</td>
                </tr>
                ';
            }

            $table .= '</tbody></table>';
        }

        if ($gprs) {
            foreach ($gprs as $pincode => $cdr) {
                $gp .= '<h2>CDR DATA: ' . $pincode . '</h2><br />
                <table><thead>
                <tr bgcolor="#111" color="#fff" cellpadding="1" cellspacing="0">
                <th width="15%">' . lang('Date') . '</th>
                <th width="13%">' . lang('Number') . '</th>
                <th width="20%">' . lang('Type') . '</th>
                <th width="35%">' . lang('Zone') . ' </th>
                <th width="10%">' . lang('Bytes') . '</th>


                </tr>
                </thead>
                <tbody>
                ';
                foreach ($cdr as $array) {
                    $bytes[] = $array->Bytes;
                    $gp .= '
                    <tr>
                    <td width="15%">' . substr($array->SessionDate, 0, -3) . '</td>
                    <td width="13%">' . $array->iPincode . '</td>
                    <td width="20%">' . $array->Type . '</td>
                    <td width="35%">' . $array->Zone . ' '.$array->RoamingCountry . '</td>
                    <td width="10%">' . convertToReadableSize($array->Bytes) . '</td>

                    </tr>
                    ';
                }
                $gp .= '</tbody></table><br /><br /><strong>Total Data usage  ' . $pincode . ' : ' . convertToReadableSize(array_sum($bytes)) . '</strong>';
                unset($bytes);
            }
        }

        $this->load->library('spdf');
        $pdf = new Spdf('P', 'mm', 'A4', true, 'UTF-8', false);
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Simson Asuni');
        $pdf->SetTitle('CDR');
        $pdf->setInvoicenumber($id);
        $pdf->SetSubject('CDR Invoice ' . $id);
        $pdf->SetKeywords('TCPDF, Invoice, Cdr, ' . $id);
        $pdf->AddPage('L', 'A4');
        //$pdf->SetFont('droidsans', '', '12');
        //$pdf->Cell(180, 6, lang('Call Detail Records').' Invoice: '.$id, 0, 0, '', 0);
        $pdf->ln(10);
        //$pdf->SetFont('droidsans', '', '9');
        // set default header data
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE . ' 010', PDF_HEADER_STRING);
        $pdf->SetPrintFooter(false);
// set header and footer fonts
        $pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
        $pdf->SetAutoPageBreak(true, "10");

// set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
            require_once dirname(__FILE__) . '/lang/eng.php';
            $pdf->setLanguageArray($l);
        }

// ---------------------------------------------------------

// print TEXT
         $pdf->SetFont('helvetica', '', '9');
        //  $pdf->PrintChapter(1, lang('Call Detail Records'), $table, true);
        $pdf->writeHTML($table, true, false, true, false, 'J');
        if ($gprs) {
            $pdf->AddPage('L', 'A4');
            //$pdf->SetFont('droidsans', '', '12');
            //$pdf->Cell(180, 6, lang('Call Detail Records').' Invoice: '.$id, 0, 0, '', 0);
            $pdf->ln(10);
            $pdf->SetFont('helvetica', '', '9');
            // set default header data
            $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE . ' 010', PDF_HEADER_STRING);

            // set header and footer fonts
            $pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
            $pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

            // set default monospaced font
            $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

            // set margins
            $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
            $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
            $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

            // set auto page breaks
            $pdf->SetAutoPageBreak(true, "10");

            // set image scale factor
            $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

            // set some language-dependent strings (optional)
            if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
                require_once dirname(__FILE__) . '/lang/eng.php';
                $pdf->setLanguageArray($l);
            }
            $pdf->writeHTML($gp, true, false, true, false, 'J');
        }
// print HTML
        //$pdf->PrintChapter(2, 'LOREM IPSUM [HTML]', FCPATH.'data/chapter_demo_2.txt', true);

// ---------------------------------------------------------

//Close and output PDF document
        if (!file_exists($setting->DOC_PATH . $companyid . '/invoices/cdr')) {
            mkdir($setting->DOC_PATH . $companyid . '/invoices/cdr');
        }
        $pdf->Output($setting->DOC_PATH . $companyid . '/invoices/cdr/' . $id . '.pdf', 'F');
        return $setting->DOC_PATH . $companyid . '/invoices/cdr/' . $id . '.pdf';
    }
    public function downloadcdr($id)
    {

        $id = $this->uri->segment(4);
        $this->load->library('xlswriter');
        $this->load->library('magebo', array('companyid', $this->session->cid));
        $cdrs = $this->magebo->getInvoiceCdrFileSplit($id);

        $gprs = $this->magebo->getDataCdr($id);

        if (!$gprs && !$cdrs) {
            $this->session->set_flashdata('error', 'no data to be exported');
            redirect('admin/invoice');
            exit;
        }
        $fileName = 'CDR-Summary_invoice_' . $id . '.xlsx';
        /*
        lang('Date') => 'string',
        lang('Type') => 'string',
        lang('Destination') => 'string',
        lang('Duration') => 'string',

        lang('Cost') => 'string',
         */
        $header = array(

            lang('Date') => 'string',
            lang('Destination') => 'string',
            lang('Description') => 'string',
            lang('Duration') => 'string',
            lang('Cost') => 'string',

        );

        $gprs_header = array(

            lang('Date') => 'string',
            lang('Number') => 'string',
            lang('Type') => 'string',
            lang('Zone') => 'string',
            lang('Bytes') => 'string',

        );

        if (count($cdrs) > 0) {
            foreach ($cdrs as $pin => $cdr) {
                foreach ($cdr as $array) {
                    $to[] = array('datum' => substr($array->dCallDate, 0, -3), 'Destination' => $array->cDialedNumber, 'Description' => $array->cInvoiceGroupDescription, 'Duration' => gmdate("H:i:s", $array->iInvoiceDuration), 'Cost' => $this->data['setting']->currency . str_replace('.', ',', includevat('21', $array->mInvoiceSale)));
                }

                $this->xlswriter->writeSheet($to, 'Voice & SMS ' . $pin, $header);
                unset($to);
            }
        }
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        // header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="' . $fileName . '"');

        if ($gprs) {
            $gp = array();
            foreach ($gprs as $pincode => $cdr) {
                foreach ($cdr as $array) {
                    $bytes[] = $array->Bytes;
                    $gp[] = array(
                        'Date' => substr($array->SessionDate, 0, -3),
                        'Number' => $array->iPincode,
                        'Type' => $array->Type,
                        'DuratiZoneon' => $array->Zone,
                        'Bytes' => convertToReadableSize($array->Bytes));
                }
                $gp[] = array(
                    'Date' => '',
                    'Number' => '',
                    'Type' => 'Total Usage: ',
                    'DuratiZoneon' => '',
                    'Bytes' => convertToReadableSize(array_sum($bytes)));
                unset($bytes);

                $this->xlswriter->writeSheet($gp, 'DATA ' . $pincode, $gprs_header);
                unset($gp);
            }
        }
        $this->xlswriter->setAuthor('Simson Parlindungan');
        $this->xlswriter->setTitle('Cdr Export');
        $this->xlswriter->setCompany('Delta');
        echo $this->xlswriter->writeToString();
    }

    public function downloadcdr_id()
    {
        $id = $this->uri->segment(4);
        $this->load->library('xlswriter');
        $this->load->library('magebo', array('companyid', $this->session->cid));
        $cdrs = $this->magebo->getInvoiceCdrFileSplit($id);

        $gprs = $this->magebo->getDataCdr($id);

        if (!$gprs && !$cdrs) {
            $this->session->set_flashdata('error', 'no data to be exported');
            redirect('admin/invoice');
            exit;
        }
        $fileName = 'CDR-Summary_invoice_' . $id . '.xlsx';
        /*
        lang('Date') => 'string',
        lang('Type') => 'string',
        lang('Destination') => 'string',
        lang('Duration') => 'string',

        lang('Cost') => 'string',
         */
        $header = array(

            lang('Date') => 'string',
            lang('Destination') => 'string',
            lang('Description') => 'string',
            lang('Duration') => 'string',
            lang('Cost') => 'string',

        );

        $gprs_header = array(

            lang('Date') => 'string',
            lang('Number') => 'string',
            lang('Type') => 'string',
            lang('Zone') => 'string',
            lang('Bytes') => 'string',

        );

        if (count($cdrs) > 0) {
            foreach ($cdrs as $pin => $cdr) {
                foreach ($cdr as $array) {
                    $to[] = array('datum' => substr($array->dCallDate, 0, -3), 'Destination' => $array->cDialedNumber, 'Description' => $array->cInvoiceGroupDescription, 'Duration' => gmdate("H:i:s", $array->iInvoiceDuration), 'Cost' => $this->data['setting']->currency . str_replace('.', ',', includevat('21', $array->mInvoiceSale)));
                }

                $this->xlswriter->writeSheet($to, 'Voice & SMS ' . $pin, $header);
                unset($to);
            }
        }
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        // header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="' . $fileName . '"');

        if ($gprs) {
            $gp = array();
            foreach ($gprs as $pincode => $cdr) {
                foreach ($cdr as $array) {
                    $bytes[] = $array->Bytes;
                    $gp[] = array(
                        'Date' => substr($array->SessionDate, 0, -3),
                        'Number' => $array->iPincode,
                        'Type' => $array->Type,
                        'DuratiZoneon' => $array->Zone,
                        'Bytes' => convertToReadableSize($array->Bytes));
                }
                $gp[] = array(
                    'Date' => '',
                    'Number' => '',
                    'Type' => 'Total Usage: ',
                    'DuratiZoneon' => '',
                    'Bytes' => convertToReadableSize(array_sum($bytes)));
                unset($bytes);

                $this->xlswriter->writeSheet($gp, 'DATA ' . $pincode, $gprs_header);
                unset($gp);
            }
        }
        $this->xlswriter->setAuthor('Simson Parlindungan');
        $this->xlswriter->setTitle('Cdr Export');
        $this->xlswriter->setCompany('Delta');
        echo $this->xlswriter->writeToString();
    }
}
