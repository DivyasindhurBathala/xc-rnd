<?php
defined('BASEPATH') or exit('No direct script access allowed');
class TableController extends CI_Controller
{
    protected $data = [];

    public function __construct()
    {
        // Ensure you run parent constructor
        parent::__construct();
        $this->lang->load('admin');
        $this->companyid = get_companyidby_url(base_url());
        $this->data['setting'] = globofix($this->companyid);
    }
}
class Table extends TableController
{
    public function __construct()
    {
        parent::__construct();
        $this->db          = $this->load->database('default', true);
        $this->sql_details = array(
        'user' => $this->db->username,
        'pass' => $this->db->password,
        'db'   => $this->db->database,
        'host' => $this->db->hostname,
        );

        error_reporting(0);
        $this->companyid = get_companyidby_url(base_url());

        $this->load->library('ssp');
    }
    public function gettokens()
    {
        $companyid = $this->companyid;
        $resellerid = $_SESSION['reseller']['id'];
        $table = <<<EOT
 (
    SELECT * from a_tokens
    where companyid = '$companyid'
    and agentid = '$resellerid'
    ORDER BY id DESC

 ) temp
EOT;

        $primaryKey = 'id';
        $columns = array(

            array(
                'db' => 'token',
                'dt' => 0,
            ),
            array(
                'db' => 'date_created',
                'dt' => 1,
            ),
            array(
                'db' => 'creator',
                'dt' => 2,
            ),
            array(
                'db' => 'id',
                'dt' => 3,
            ),

        );

        echo json_encode($this->ssp->simple($_GET, $this->sql_details, $table, $primaryKey, $columns));
    }
    public function getclients()
    {
        $agentid = $_SESSION['reseller']['id'];
        $table = <<<EOT
   (
	  select concat(initial,' ',firstname,' ',lastname) as customer,mvno_id,status,id,email,companyname,phonenumber,concat(address1,' ',postcode,' ',city) as address
	  from a_clients
      where companyid = '$this->companyid'
      and agentid = '$agentid'
   ) temp
EOT;

        $primaryKey = 'id';
        $columns    = array(
        array(
         'db' => 'mvno_id',
         'dt' => 0,
        ),
        array(
        'db' => 'customer',
        'dt' => 1,
        ),
        array(
        'db' => 'email',
        'dt' => 2,
        ),
        array(
        'db' => 'phonenumber',
        'dt' => 3,
        ),
        array(
        'db' => 'address',
        'dt' => 4,
        ),
        array(
        'db' => 'status',
        'dt' => 5,
        ),
        array(
        'db' => 'id',
        'dt' => 6,
        ),


        );

        echo json_encode($this->ssp->simple($_GET, $this->sql_details, $table, $primaryKey, $columns));
    }

    public function test_session()
    {
        echo $this->session->reseller['id'];
    }
    public function get_reseller_credit_usage()
    {
        $companyid = $this->session->cid;
        $agentid = $_SESSION['reseller']['id'];
        if (!is_numeric($agentid)) {
            die('Hack atempt');
        }
        $table = <<<EOT
 (
    SELECT a.date,a.id,a.income_type, ROUND(a.amount,2) as amount,b.msisdn FROM `a_topups` a left join a_services_mobile b on b.serviceid=a.serviceid WHERE a.agentid = '$agentid' and a.companyid = '$companyid' ORDER BY `id` DESC

 ) temp
EOT;

        $primaryKey = 'id';
        $columns = array(
            array(
                'db' => 'date',
                'dt' => 0,
            ),

            array(
                'db' => 'income_type',
                'dt' => 1,
            ),
            array(
                'db' => 'amount',
                'dt' => 2,
            ),
            array(
                'db' => 'msisdn',
                'dt' => 3,
            ),
            array(
                'db' => 'id',
                'dt' => 4,
            ),
        );

        echo json_encode($this->ssp->simple($_GET, $this->sql_details, $table, $primaryKey, $columns));
    }
    public function getservices()
    {
        $agentid = $_SESSION['reseller']['id'];
        $table = <<<EOT
   (
      select a.id,a.status,a.userid,round(a.recurring, 2) as recurring,a.billingcycle, date_contract,DATE_FORMAT(a.date_created, '%d/%m/%Y') as date_created,concat(c.firstname,' ', c.lastname, ' ', companyname)  as clientname, b.name as packagename,c.mvno_id,
         case when a.type  = 'mobile' THEN d.msisdn
         when a.type = 'xdsl' then e.circuitid
         when a.type = 'voip' then f.cli
         else 'Unknown' end as domain, case when a.type  = 'mobile' THEN d.msisdn_status
         when a.type = 'xdsl' then e.status
         when a.type = 'voip' then f.status
         else 'Unknown' end as orderstatus
      from a_services a
      left join a_products b on b.id=a.packageid
      left join a_clients c on c.id=a.userid
      left join a_services_mobile d on d.serviceid=a.id
      left join a_services_xdsl e on e.serviceid=a.id
      left join a_services_voip f on f.serviceid=a.id
      where a.status in ('Pending','Active','New')
      and a.companyid = '$this->companyid'
      and c.agentid = '$agentid'
      group by a.id
   ) temp
EOT;

        $primaryKey = 'id';
        $columns    = array(
        array(
         'db' => 'id',
         'dt' => 0,
        ),
        array(
        'db' => 'packagename',
        'dt' => 1,
        ),

        array(
        'db' => 'clientname',
        'dt' => 2,
        ),
        array(
        'db' => 'orderstatus',
        'dt' => 3,
        ),
        array(
        'db' => 'recurring',
        'dt' => 4,
        ),
        array(
        'db' => 'domain',
        'dt' => 5,
        ),
        array(
        'db' => 'date_contract',
        'dt' => 6,
        ),
        array(
        'db' => 'billingcycle',
        'dt' => 7,
        ),
        array(
        'db' => 'id',
        'dt' => 8,
        ),

        array(
        'db' => 'userid',
        'dt' => 9,
        ),

         array(
        'db' => 'mvno_id',
        'dt' => 10,
        ),

        );

        echo json_encode($this->ssp->simple($_GET, $this->sql_details, $table, $primaryKey, $columns));
    }
    public function get_lang()
    {
        if (empty($_SESSION['language'])) {
            echo json_encode(array('result' => ucfirst($_SESSION['reseller']['language'])));
        } else {
            echo json_encode(array('result' => ucfirst($_SESSION['language'])));
        }
    }

    public function searchclient()
    {
        $list1 = array();

        $keyword = '%' . $_POST['keyword'] . '%';
        $this->db = $this->load->database('default', true);
        $query1 = $this->db->query("SELECT 'client' as type, id as value,concat('Client ',firstname,' ',lastname, ' ', companyname, ' #', mvno_id) as label, companyname FROM a_clients where (firstname like ? or lastname like ? or companyname like ? or id like ? or address1 like ? or postcode like ? or phonenumber like ? or mvno_id LIKE ? or mageboid LIKE ?) and companyid=? and agentid= ? group by id order by firstname DESC LIMIT 0, 40", array($keyword, $keyword, $keyword, $keyword, $keyword, $keyword, $keyword, $keyword, $keyword, $this->companyid, $_SESSION['reseller']['id']));
        if ($query1->num_rows() > 0) {
            $list1 = $query1->result_array();
        }

        echo json_encode($list1);
    }

    public function search_simcard()
    {
        $list1 = array();

        $keyword = '%' . $_POST['keyword'] . '%';
        $this->db = $this->load->database('default', true);
        $query1 = $this->db->query("SELECT concat(simcard,' +',MSISDN) as label, simcard as value,id,MSISDN as msisdn from a_reseller_simcard where (simcard like ? or MSISDN like ?) and companyid=? and resellerid=? and serviceid is  null Order by simcard DESC LIMIT 0, 40", array($keyword,$keyword, $this->companyid, $_SESSION['reseller']['id']));
        log_message('error', $this->db->last_query());
        if ($query1->num_rows() > 0) {
            $list1 = $query1->result_array();
        }

        echo json_encode($list1);
    }

    public function get_mytickets_assign()
    {
        $status = $this->uri->segment(4);
        $companyid = $this->companyid;
        $userid = $this->session->id;
        if ($status == "Open") {
            $table = <<<EOT
 (
  SELECT a.id,a.tid,a.subject,a.userid,a.status,a.date,a.priority,d.firstname as assigned,a.categoryid,x.name as categoryname,
    case when a.userid  <= '0' THEN a.name else concat(b.firstname,' ', b.lastname) end as fullname,c.name as deptname
    FROM a_helpdesk_tickets a
    left join a_clients b on b.id=a.userid
    left join a_helpdesk_department c on c.id=a.deptid
    left join a_admin d on d.id=a.assigne
     left join a_helpdesk_category x on x.id=a.categoryid
    WHERE a.companyid='$companyid'
    AND a.status IN ('Open','In-Progress', 'Awaiting-Reply','On-Hold')
    AND a.assigne = '$userid'
 ) temp
EOT;
        } elseif ($status == "Closed") {
            $table = <<<EOT
 (
  SELECT a.id,a.tid,a.subject,a.userid,a.status,a.date,a.priority,d.firstname as assigned,a.categoryid,x.name as categoryname,
    case when a.userid  <= '0' THEN a.name else concat(b.firstname,' ', b.lastname) end as fullname,c.name as deptname
    FROM a_helpdesk_tickets a
    left join a_clients b on b.id=a.userid
    left join a_helpdesk_department c on c.id=a.deptid
    left join a_admin d on d.id=a.assigne
     left join a_helpdesk_category x on x.id=a.categoryid
    WHERE a.companyid='$companyid'
    AND a.status IN ('Closed')
    AND a.assigne = '$userid'
 ) temp
EOT;
        } elseif ($status == "Answered") {
            $table = <<<EOT
 (
  SELECT a.id,a.tid,a.subject,a.userid,a.status,a.date,a.priority,d.firstname as assigned,a.categoryid,x.name as categoryname,
    case when a.userid  <= '0' THEN a.name else concat(b.firstname,' ', b.lastname) end as fullname,c.name as deptname
    FROM a_helpdesk_tickets a
    left join a_clients b on b.id=a.userid
    left join a_helpdesk_department c on c.id=a.deptid
    left join a_admin d on d.id=a.assigne
     left join a_helpdesk_category x on x.id=a.categoryid
    WHERE a.companyid='$companyid'
    AND a.status IN ('Answered')
    AND a.assigne = '$userid'
 ) temp
EOT;
        } elseif ($status == "On-Hold") {
            $table = <<<EOT
 (
  SELECT a.id,a.tid,a.subject,a.userid,a.status,a.date,a.priority,d.firstname as assigned,a.categoryid,x.name as categoryname,
    case when a.userid  <= '0' THEN a.name else concat(b.firstname,' ', b.lastname) end as fullname,c.name as deptname
    FROM a_helpdesk_tickets a
    left join a_clients b on b.id=a.userid
    left join a_helpdesk_department c on c.id=a.deptid
    left join a_admin d on d.id=a.assigne
     left join a_helpdesk_category x on x.id=a.categoryid
    WHERE a.companyid='$companyid'
    AND a.status IN ('On-Hold')
    AND a.assigne = '$userid'
 ) temp
EOT;
        } elseif ($status == "Resolved") {
            $table = <<<EOT
 (
  SELECT a.id,a.tid,a.subject,a.userid,a.status,a.date,a.priority,d.firstname as assigned,a.categoryid,x.name as categoryname,
    case when a.userid  <= '0' THEN a.name else concat(b.firstname,' ', b.lastname) end as fullname,c.name as deptname
    FROM a_helpdesk_tickets a
    left join a_clients b on b.id=a.userid
    left join a_helpdesk_department c on c.id=a.deptid
    left join a_admin d on d.id=a.assigne
     left join a_helpdesk_category x on x.id=a.categoryid
    WHERE a.companyid='$companyid'
    AND a.status IN ('Resolved')
    AND a.assigne = '$userid'
 ) temp
EOT;
        } elseif ($status == "Proefbilling") {
            $table = <<<EOT
 (
  SELECT a.id,a.tid,a.subject,a.userid,a.status,a.date,a.priority,d.firstname as assigned,a.categoryid,x.name as categoryname,
    case when a.userid  <= '0' THEN a.name else concat(b.firstname,' ', b.lastname) end as fullname,c.name as deptname
    FROM a_helpdesk_tickets a
    left join a_clients b on b.id=a.userid
    left join a_helpdesk_department c on c.id=a.deptid
    left join a_admin d on d.id=a.assigne
     left join a_helpdesk_category x on x.id=a.categoryid
    WHERE a.companyid='$companyid'
    AND a.status IN ('Proefbilling')
    AND a.assigne = '$userid'
 ) temp
EOT;
        }

        $primaryKey = 'id';
        $columns = array(

            array(
                'db' => 'tid',
                'dt' => 0,
            ),
            array(
                'db' => 'fullname',
                'dt' => 1,
            ),
            array(
                'db' => 'subject',
                'dt' => 2,
            ),
            array(
                'db' => 'deptname',
                'dt' => 3,
            ),
            array(
                'db' => 'date',
                'dt' => 4,
            ),
            array(
                'db' => 'status',
                'dt' => 5,
            ),
            array(
                'db' => 'assigned',
                'dt' => 6,
            ),
            array(
                'db' => 'priority',
                'dt' => 7,
            ),
            array(
                'db' => 'id',
                'dt' => 8,
            ),

            array(
                'db' => 'userid',
                'dt' => 9,
            ),
            array(
                'db' => 'categoryname',
                'dt' => 10,
            ),

        );

        echo json_encode($this->ssp->simple($_GET, $this->sql_details, $table, $primaryKey, $columns));
    }
    public function get_tickets_status()
    {
        $status = $this->uri->segment(4);
        $companyid = $this->companyid;
        $resellerid = $_SESSION['reseller']['id'];

        if ($status == "Open") {
            $table = <<<EOT
 (
  SELECT a.id,a.tid,a.subject,a.userid,a.status,a.date,a.priority,d.firstname as assigned,a.categoryid,x.name as categoryname,
    case when a.userid  <= '0' THEN a.name else concat(b.firstname,' ', b.lastname) end as fullname,c.name as deptname
    FROM a_helpdesk_tickets a
    left join a_clients b on b.id=a.userid
    left join a_helpdesk_department c on c.id=a.deptid
    left join a_admin d on d.id=a.assigne
    left join a_helpdesk_category x on x.id=a.categoryid
    WHERE a.companyid='$companyid'
    AND b.agentid = '$resellerid'
    AND a.status IN('Open')
 ) temp
EOT;
        } elseif ($status == "Closed") {
            $table = <<<EOT
 (
  SELECT a.id,a.tid,a.subject,a.userid,a.status,a.date,a.priority,d.firstname as assigned,a.categoryid,x.name as categoryname,
    case when a.userid  <= '0' THEN a.name else concat(b.firstname,' ', b.lastname) end as fullname,c.name as deptname
    FROM a_helpdesk_tickets a
    left join a_clients b on b.id=a.userid
    left join a_helpdesk_department c on c.id=a.deptid
    left join a_admin d on d.id=a.assigne
    left join a_helpdesk_category x on x.id=a.categoryid
    WHERE a.companyid='$companyid'
      AND b.agentid = '$resellerid'
    AND a.status IN ('Closed')
 ) temp
EOT;
        } elseif ($status == "Answered") {
            $table = <<<EOT
       (
        SELECT a.id,a.tid,a.subject,a.userid,a.status,a.date,a.priority,d.firstname as assigned,a.categoryid,x.name as categoryname,
          case when a.userid  <= '0' THEN a.name else concat(b.firstname,' ', b.lastname) end as fullname,c.name as deptname
          FROM a_helpdesk_tickets a
          left join a_clients b on b.id=a.userid
          left join a_helpdesk_department c on c.id=a.deptid
          left join a_admin d on d.id=a.assigne
          left join a_helpdesk_category x on x.id=a.categoryid
          WHERE a.companyid='$companyid'
            AND b.agentid = '$resellerid'
          AND a.status IN ('Answered')

       ) temp
EOT;
        } elseif ($status == "On-Hold") {
            $table = <<<EOT
       (
        SELECT a.id,a.tid,a.subject,a.userid,a.status,a.date,a.priority,d.firstname as assigned,a.categoryid,x.name as categoryname,
          case when a.userid  <= '0' THEN a.name else concat(b.firstname,' ', b.lastname) end as fullname,c.name as deptname
          FROM a_helpdesk_tickets a
          left join a_clients b on b.id=a.userid
          left join a_helpdesk_department c on c.id=a.deptid
          left join a_admin d on d.id=a.assigne
          left join a_helpdesk_category x on x.id=a.categoryid
          WHERE a.companyid='$companyid'
            AND b.agentid = '$resellerid'
          AND a.status IN ('On-Hold')

       ) temp
EOT;
        } elseif ($status == "Resolved") {
            $table = <<<EOT
       (
        SELECT a.id,a.tid,a.subject,a.userid,a.status,a.date,a.priority,d.firstname as assigned,a.categoryid,x.name as categoryname,
          case when a.userid  <= '0' THEN a.name else concat(b.firstname,' ', b.lastname) end as fullname,c.name as deptname
          FROM a_helpdesk_tickets a
          left join a_clients b on b.id=a.userid
          left join a_helpdesk_department c on c.id=a.deptid
          left join a_admin d on d.id=a.assigne
          left join a_helpdesk_category x on x.id=a.categoryid
          WHERE a.companyid='$companyid'
            AND b.agentid = '$resellerid'
          AND a.status IN ('Resolved')
       ) temp
EOT;
        } elseif ($status == "Proefbilling") {
            $table = <<<EOT
       (
        SELECT a.id,a.tid,a.subject,a.userid,a.status,a.date,a.priority,d.firstname as assigned,a.categoryid,x.name as categoryname,
          case when a.userid  <= '0' THEN a.name else concat(b.firstname,' ', b.lastname) end as fullname,c.name as deptname
          FROM a_helpdesk_tickets a
          left join a_clients b on b.id=a.userid
          left join a_helpdesk_department c on c.id=a.deptid
          left join a_admin d on d.id=a.assigne
          left join a_helpdesk_category x on x.id=a.categoryid
          WHERE a.companyid='$companyid'
            AND b.agentid = '$resellerid'
          AND a.status IN ('Proefbilling')
       ) temp
EOT;
        }

        $primaryKey = 'id';
        $columns = array(

            array(
                'db' => 'tid',
                'dt' => 0,
            ),
            array(
                'db' => 'fullname',
                'dt' => 1,
            ),
            array(
                'db' => 'subject',
                'dt' => 2,
            ),
            array(
                'db' => 'deptname',
                'dt' => 3,
            ),
            array(
                'db' => 'date',
                'dt' => 4,
            ),
            array(
                'db' => 'status',
                'dt' => 5,
            ),
            array(
                'db' => 'assigned',
                'dt' => 6,
            ),
            array(
                'db' => 'priority',
                'dt' => 7,
            ),
            array(
                'db' => 'id',
                'dt' => 8,
            ),

            array(
                'db' => 'userid',
                'dt' => 9,
            ),
            array(
                'db' => 'categoryname',
                'dt' => 10,
            ),

        );

        echo json_encode($this->ssp->simple($_GET, $this->sql_details, $table, $primaryKey, $columns));
    }
    public function get_tickets()
    {
        $companyid = $this->companyid;
        $resellerid = $_SESSION['reseller']['id'];
        $table = <<<EOT
 (
  SELECT a.id,a.tid,a.subject,a.userid,a.status,a.date,a.priority,d.firstname as assigned,a.categoryid,x.name as categoryname,
    case when a.userid  <= '0' THEN a.name else concat(b.firstname,' ', b.lastname) end as fullname,c.name as deptname
    FROM a_helpdesk_tickets a
    left join a_clients b on b.id=a.userid
    left join a_helpdesk_department c on c.id=a.deptid
    left join a_admin d on d.id=a.assigne
    left join a_helpdesk_category x on x.id=a.categoryid
    WHERE a.companyid='$companyid'
      AND b.agentid = '$resellerid'
 ) temp
EOT;

        $primaryKey = 'id';
        $columns = array(

            array(
                'db' => 'tid',
                'dt' => 0,
            ),
            array(
                'db' => 'fullname',
                'dt' => 1,
            ),
            array(
                'db' => 'subject',
                'dt' => 2,
            ),
            array(
                'db' => 'deptname',
                'dt' => 3,
            ),
            array(
                'db' => 'date',
                'dt' => 4,
            ),
            array(
                'db' => 'status',
                'dt' => 5,
            ),
            array(
                'db' => 'assigned',
                'dt' => 6,
            ),
            array(
                'db' => 'priority',
                'dt' => 7,
            ),
            array(
                'db' => 'id',
                'dt' => 8,
            ),

            array(
                'db' => 'userid',
                'dt' => 9,
            ),
            array(
                'db' => 'categoryname',
                'dt' => 10,
            ),

        );

        echo json_encode($this->ssp->simple($_GET, $this->sql_details, $table, $primaryKey, $columns));
    }
}
