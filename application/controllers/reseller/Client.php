<?php
defined('BASEPATH') or exit('No direct script access allowed');
class ClientController extends CI_Controller
{
    //protected $data = [];
    public function __construct()
    {
        // Ensure you run parent constructor
        parent::__construct();
    }
}
class Client extends ClientController
{
    public function __construct()
    {
        parent::__construct();
        if (!empty($this->session->language)) {
            $this->config->set_item('language', $_SESSION['reseller']['language']);
        } else {
            $this->config->set_item('language', 'english');
        }
        $this->lang->load('admin');
        $this->companyid = get_companyidby_url(base_url());
        $this->data['setting'] = globofix($this->companyid);
        $this->load->model('Admin_model');
    }
    public function add()
    {
        if (!reseller_allow_perm('addclient', $this->session->reseller['id'])) {
            $this->session->set_flashdata('error', lang('You do not have access to this feature, please contact your Upstream'));
            redirect('reseller');
        }
        if (isPost()) {
            if ($_POST['country'] == "NL") {
                $pcode = explode(' ', trim($_POST["postcode"]));
                if (count($pcode) != 2) {
                    $this->session->set_userdata('registration', $_POST);
                    $this->session->set_flashdata('error', lang('Postcode moet: NNNN XX formaat (100)'));
                    redirect('admin/client/add');
                    exit;
                } else {
                    if (strlen($pcode[0]) != 4) {
                        $this->session->set_userdata('registration', $_POST);
                        $this->session->set_flashdata('error', lang('Postcode moet: NNNN XX formaat (102) '));
                        redirect('admin/client/add');
                        exit;
                    } elseif (strlen($pcode[1]) != 2) {
                        $this->session->set_userdata('registration', $_POST);
                        $this->session->set_flashdata('error', lang('Postcode moet: NNNN XX formaat (103)'));
                        redirect('admin/client/add');
                        exit;
                    }
                }
            } elseif ($_POST['country'] == "BE") {
                $pcode = explode(' ', 'BE ', trim($_POST["postcode"]));
            } else {
                $pcode = trim($_POST["postcode"]);
            }
            if (!empty($_POST['iban'])) {
                $bic = $this->Api_model->getBic(trim($_POST['iban']));
                if (!$bic->valid) {
                    echo json_encode(array('result' => false, 'message' => 'your IBAN: '.$_POST['iban'].' is not valid'));
                } else {
                    $_POST['bic'] = $bic->bankData->bic;
                }
            }
            if ($this->data['setting']->mvno_id_increment == 1) {
                $_POST['mvno_id'] = genMvnoId($this->session->cid);
            } else {
                $_POST['mvno_id'] = strtoupper(trim($_POST['mvno_id']));
            }

            $pass = rand(100000, 9999999);

            $_POST['password'] = password_hash($pass, PASSWORD_DEFAULT);
            $_POST['date_created'] = date('Y-m-d');
            $_POST['agentid'] = $this->session->reseller['id'];
            $_POST['companyid'] = $this->session->cid;
            if ($this->data['setting']->create_arta_contact == 1) {
                $agentid = $this->session->reseller['id'];
                $this->load->library('artilium', array('companyid' => $this->session->cid));
                sendlog('CreateContact', array('agentid' => $agentid, 'post' => $_POST));
                $cont = $this->artilium->CreateContact($_POST, $agentid);
                if ($cont) {
                    $_POST['ContactType2Id'] = $cont;
                }
            }
            $_POST['date_modified'] = date('Y-m-d H:i:s');
            $result = $this->Admin_model->insertCustomer($_POST);
            if ($result['result']) {
                $client = $this->Admin_model->getClient($result['id']);
                if ($this->data['setting']->mobile_platform == "TEUM") {
                    $this->load->library('pareteum', array('companyid' => $this->session->cid));
                    $array = array(
                    "CustomerData" => array(
                        "ExternalCustomerId" => (string) $client->mvno_id,
                        "FirstName" => $client->firstname,
                        "LastName" => $client->lastname,
                        "LastName2" => 'NA',
                        "CustomerDocumentType" => $client->id_type,
                        "DocumentNumber" => $client->idcard_number,
                        "Telephone" => $client->phonenumber,
                        "Email" => $client->email,
                        "LanguageName" => "eng",
                        "FiscalAddress" => array(
                            "Address" => $client->address1,
                            "City" =>$client->city,
                            "CountryId" => "76",
                            "HouseNo" => $client->housenumber,
                            "State" => "Unknown",
                            "ZipCode" => $client->postcode,
                        ),
                        "CustomerAddress" => array(
                            "Address" => $client->address1,
                            "City" => $client->city,
                            "CountryId" => "76",
                            "HouseNo" => $client->housenumber,
                            "State" => "Unknown",
                            "ZipCode" => $client->postcode
                        ),
                        "Nationality" => "GB"
                    ),
                    "comments" => "Customer added via UnitedPortal V1 by Reseller." . $this->session->reseller['firstname'] . ' ' . $this->session->reseller['lastname']
                     );
                    //log_message('error', json_encode($array));
                    $teum = $this->pareteum->AddCustomers($array);

                    //log_message('error', json_encode($teum));
                    if ($teum->resultCode == "0") {
                        $this->db->query("update a_clients set teum_CustomerId=?, teum_OrderCode=? where id=?", array($teum->customerId, $teum->orderCode, $client->id));
                    }
                }


                if ($this->data['setting']->smtp_type == 'smtp') {
                    $config = array(
                        'protocol' => 'smtp',
                        'smtp_host' => $this->data['setting']->smtp_host,
                        'smtp_port' => $this->data['setting']->smtp_port,
                        'smtp_user' => $this->data['setting']->smtp_user,
                        'smtp_pass' => $this->data['setting']->smtp_pass,
                        'mailtype' => 'html',
                        'charset' => 'utf-8',
                        'starttls' => true,
                        'wordwrap' => true,
                    );
                } else {
                    $config['protocol'] = 'sendmail';
                    $config['mailpath'] = '/usr/sbin/sendmail';
                    $config['mailtype'] = 'html';
                    $config['charset'] = 'utf-8';
                    $config['wordwrap'] = true;
                }
                $this->email->clear(true);
                $this->email->initialize($config);
                $this->data['language'] = "dutch";
                $this->data['info'] = (object) array(
                    'email' => $client->email,
                    'password' => $password,
                    'name' => $client->firstname . ' ' . $client->lastname,
                );
                //$body = $this->load->view('email/resetpassword.php', $this->data, true);
                //$this->data['main_content'] = 'email/' . $this->data['language'] . '/resetpassword.php';
                //$body = $this->load->view('email/content', $this->data, true);

                $body = getMailContent('signup_email', $client->language, $client->companyid);
                // $body = $this->load->view('email/content', $this->data, true);
                $body = str_replace('{$name}', $client->firstname . ' ' . $client->lastname, $body);
                $body = str_replace('{$password}', $pass, $body);
                $body = str_replace('{$email}', trim(strtolower($client->email)), $body);
                $body = str_replace('{$base_url}', url_to_domain(base_url()), $body);
                $subject = getSubject('signup_email', $client->language, $client->companyid);
                $this->email->set_newline("\r\n");
                $this->email->from($this->data['setting']->smtp_sender, $this->data['setting']->smtp_name);
                $this->email->to($client->email);
                $this->email->subject($subject);
                //$this->email->subject(lang("Your New Password"));
                $this->email->message($body);
                if ($this->email->send()) {
                    logEmailOut(array(
                        'userid' => $client->id,
                        'companyid' => $client->companyid,
                        'to' => $client->email,
                        'subject' => $subject,
                        'message' => $body,
                    ));
                }
            }
            if ($result['result'] == "success") {
                $this->session->set_flashdata('success', 'Client has been added');
            }

            echo json_encode($result);
            exit;
        }
        $this->data['title'] = "Reseller Dasboard - Add Client";
        $this->data['main_content'] = reseller_theme($this->companyid)."addclient";
        $this->load->view(reseller_theme($this->companyid)."content", $this->data);
    }
    public function export_clients()
    {
        $this->load->library('xlswriter');
        $fileName = 'Subscription-Summary_by_Date_' .date('Y-m-d') . '.xlsx';
        $header = array(
            'ArtiliumID' => 'string',
            'BillingID' => 'string',
            'MvnoID' => 'string',
            'Salutation' => 'string',
            'Initial' => 'string',
            'Firstname' => 'string',
            'Lastname' => 'string',
            'CompanyName' => 'string',
            'VAT' => 'string',
            'Email' => 'string',
            'Address' => 'string',
            'Postcode' => 'string',
            'City' => 'string',
            'Country' => 'string',
            'Language' => 'string',
            'Phonenumber' => 'string',
            'Paymentmethod' => 'string',
            'Due days' => 'string',
            'Date Birth' => 'string',
            'Vat Rate' => 'string',
            'AgentID' => 'string',
            'IBAN' => 'string',
            'BIC' => 'string',
            'Madate ID' => 'string',
            'Mandate Status' => 'string',
            'Mandate Date' => 'string'
        );
        $companyid = $this->session->cid;
        $this->db = $this->load->database('default', true);
        $agentid = $this->session->reseller['id'];
        $q = $this->db->query("select a.id,a.mageboid,a.mvno_id,a.salutation,a.initial,a.firstname,a.lastname,a.companyname,a.vat,a.email,a.address1,a.postcode,a.city,a.country,a.language,a.phonenumber,a.paymentmethod,a.payment_duedays,
    a.date_birth,a.vat_rate,a.agentid,a.iban,a.bic,b.mandate_id,b.mandate_status,b.mandate_date from a_clients a left join a_clients_mandates b on b.userid=a.id where a.companyid=? and a.agentid=?", array($companyid, $agentid));
        if ($q->num_rows() > 0) {
            $res = $q->result_array();
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            $this->xlswriter->writeSheet($res, 'Sheet1', $header);
            $this->xlswriter->setAuthor('Simson Parlindungan');
            $this->xlswriter->setTitle('Client Export');
            $this->xlswriter->setCompany('United');
            echo $this->xlswriter->writeToString();

            $this->session->set_flashdata('success', 'Downloaded as requested');
        } else {
            $this->session->set_flashdata('error', 'No data found');
        }

        redirect('reseller/dashboard');
    }


    public function edit()
    {
        if (!reseller_allow_perm('editclient', $this->session->reseller['id'])) {
            $this->session->set_flashdata('error', lang('You do not have access to this feature, please contact your Upstream'));
            redirect('reseller');
        }
        if (isPost()) {
            $this->db->where('agentid', $this->session->reseller['id']);
            $this->db->where('id', $_POST['id']);
            $this->db->update('a_clients', $_POST);
            if ($this->db->affected_rows()>0) {
                $this->session->set_flashdata('success', 'Client data has been updated');
                echo json_encode(array('result' => true));
            } else {
                echo json_encode(array('result' => false,'message' =>'no data updated'));
            }
        }
    }
    public function sepa_amend()
    {
        $this->load->library('magebo', array('companyid' => $this->companyid));
        if (!empty($_POST['iban']) && !empty($_POST['userid'])) {
            $client = $this->Admin_model->getClient($_POST['userid']);
            $bic = $this->Api_model->getBic(trim($_POST['iban']));
            if (iban_block($this->companyid, trim($_POST['iban']))) {
                $this->session->set_flashdata('error', 'You may not use this IBAN');
                redirect('admin/client/detail/' . $_POST["userid"]);
                exit;
            } else {
                if (!$bic->valid) {
                    if (!empty($_POST['bic'])) {
                    } else {
                        $this->session->set_flashdata('error', 'We can not detect BIC please provide it, error code: 012');
                        redirect('admin/client/detail/' . $_POST["userid"]);
                        exit;
                    }
                } else {
                    if (!empty($_POST['bic'])) {
                    } else {
                        if (!empty($bic->bankData->bic)) {
                            $_POST['bic'] = $bic->bankData->bic;
                        } else {
                            $bc = $this->Api_model->getBicTry(trim($_POST['iban']));
                            if (!empty($bc->result->data->swift_code)) {
                                $_POST['bic'] = $bc->result->data->swift_code;
                            }
                        }
                    }

                    if (empty($_POST['bic'])) {
                        $this->session->set_flashdata('error', 'We can not detect BIC please provide it, error code: 011');
                        redirect('admin/client/detail/' . $_POST["userid"]);
                        exit;
                    }

                    if ($client) {
                        if ($_POST['type'] == "NONE") {
                            $this->session->set_flashdata('error', 'You need to choose AMEND or NEW');
                        } else {
                            if ($_POST['type'] == "NEW") {
                                $this->magebo->deleteIban($client->mageboid);
                                $this->magebo->insertIban($client->mageboid, array('iban' => strtoupper(str_replace(' ', '', trim($_POST['iban']))), 'bic' => trim($_POST['bic'])));
                                $this->magebo->addMageboSEPA(strtoupper(str_replace(' ', '', trim($_POST['iban']))), trim($_POST['bic']), $client->mageboid, $_POST['mandateid']);
                            } else {
                                $this->magebo->amendMageboSEPA(strtoupper(str_replace(' ', '', trim($_POST['iban']))), trim($_POST['bic']), $client->mageboid);
                            }
                            $this->Admin_model->update_sepa($_POST);
                            logAdmin(array('companyid' => $this->companyid, 'userid' => $client->id, 'user' => $this->session->firstname . ' ' . $this->session->lastname, 'ip' => $_SERVER['REMOTE_ADDR'], 'description' => 'Execute Sepa Amendement for  ' . $client->mageboid . ' IBAN: ' . $_POST['iban']));
                        }
                        $this->session->set_flashdata('success', 'Sepa has been amended');
                    } else {
                        $this->session->set_flashdata('error', 'Billing Id cant be found');
                    }
                }
            }
        } else {
            $this->session->set_flashdata('success', 'Please check your data');
        }

        redirect('reseller/dashboard/viewclient/' . $_POST["userid"]);
    }
    public function gen_anonymous()
    {
        $this->load->helper('string');
        $data = array(
               'companyid' => $this->companyid,
               'uuid' => gen_uuid(),
               'mvno_id' => rand(100000000, 9999999999),
               'email' => random_string('alnum', 10).'@'.random_string('alnum', 10).'.com',
               'phonenumber' => rand(1000000000, 9999999999),
               'vat' => random_string('alnum', 10),
               'companyname' => random_string('alnum', 10),
               'initial' => 'AN',
               'salutation' => 'Mr.',
               'firstname' => 'Anon_'.random_string('alnum', 10),
               'lastname' => 'Anon_'.random_string('alnum', 10),
               'address1' => random_string('alnum', 10),
               'housenumber' => rand(10, 120),
               'postcode' => rand(1000, 9999),
               'city' => random_string('alnum', 10),
               'country' => $this->data['setting']->country_base,
               'language' => 'english',
               'paymentmethod' => 'banktransfer',
               'gender' => 'male',
               'sso_id' => '0',
               'nationalnr' => random_string('alnum', 10),
               'date_created' => date('Y-m-d'),
               'invoice_email' => 1,
               'date_birth' => '1970-01-01',
               'vat_exempt' => 0,
               'idcard_number' => rand(10000000000, 99999999999));

        echo json_encode($data);
    }
}
