<?php
defined('BASEPATH') or exit('No direct script access allowed');
class SubscriptionController extends CI_Controller
{
    //protected $data = [];
    public function __construct()
    {
        // Ensure you run parent constructor
        parent::__construct();
    }
}
class Subscription extends SubscriptionController
{
    public function __construct()
    {
        parent::__construct();
        if (!empty($this->session->language)) {
            $this->config->set_item('language', $_SESSION['reseller']['language']);
        } else {
            $this->config->set_item('language', 'english');
        }
        $this->lang->load('admin');
        $this->companyid       = get_companyidby_url(base_url());
        $this->data['setting'] = globofix($this->companyid);
        // print_r($this->data['setting']);
        $this->load->model('Admin_model');
    }
    public function detail()
    {
        if (!reseller_allow_perm('service', $this->session->reseller['id'])) {
            $this->session->set_flashdata('error', lang('You do not have access to this feature, please contact your Upstream'));
            redirect('reseller');
        }
        if (!isAllowed_reseller_service($this->session->reseller['id'], $this->uri->segment(4))) {
            $this->session->set_flashdata('error', 'Access Denied');
            redirect('reseller/dashboard/client');
        }
        $this->data['service'] = $this->Admin_model->getService($this->uri->segment(4));
        if (in_array($this->data['service']->status, array("New","Pending"))) {
            $this->session->set_flashdata('error', 'Please activate the simcard');
            redirect('reseller/subscription/activate_mobile/' . $this->uri->segment(4));
        }
        $this->data['client'] = $this->Admin_model->getClient($this->data['service']->userid);
        if ($this->data['setting']->mobile_platform == "TEUM") {
            $this->load->library('pareteum', array(
                'companyid' => $this->companyid,
                'api_id' => $this->data['service']->api_id
            ));
            //print_r($this->data['service']->details);
            $this->data['service_info'] = $this->pareteum->accountInformation(array(
                'msisdn' => $this->data['service']->details->msisdn
            ));


            $this->data['bundles'] = $this->Admin_model->getOptions($this->uri->segment(4));
        //print_r($this->data['service_info']);
        } elseif ($this->data['setting']->mobile_platform == "ARTA") {
            $this->load->library('artilium', array(
                'companyid' => $this->companyid
            ));
            $this->data['cdr'] = $this->artilium->get_cdr($this->data['service']);
        }
        $this->data['title']        = "Subscription Detail";
        $this->data['main_content'] = reseller_theme($this->companyid) . "subscription";
        $this->load->view(reseller_theme($this->companyid) . "content", $this->data);
    }

    public function history()
    {
        $this->data['title']        = "Credit History";
        $this->data['main_content'] = reseller_theme($this->companyid) . "history";
        $this->load->view(reseller_theme($this->companyid) . "content", $this->data);
    }
    public function activate_mobile()
    {
        if (!reseller_allow_perm('service', $this->session->reseller['id'])) {
            $this->session->set_flashdata('error', lang('You do not have access to this feature, please contact your Upstream'));
            redirect('reseller');
        }
        if (!isAllowed_reseller_service($this->session->reseller['id'], $this->uri->segment(4))) {
            $this->session->set_flashdata('error', 'Access Denied');
            redirect('reseller/dashboard/client');
        }
        if (isPost()) {
            if ($this->data['setting']->mobile_platform != "TEUM") {
                $this->load->library('artilium', array(
                    'companyid' => $this->companyid
                ));
                $order  = $this->Admin_model->getService($_POST['serviceid']);
                $client = $this->Admin_model->getClient($order->userid);
                $test   = $this->artilium->getSn(trim($_POST['msisdn_sim']));
                if ($test->result == "error") {
                    echo json_encode(array(
                        'result' => false,
                        'message' => 'Simcard is not valid',
                        'description' => $test
                    ));
                    exit;
                }
                if ($order->details->msisdn_type != 'porting') {
                    $this->Admin_model->update_services_data('mobile', $_POST['serviceid'], array(
                    'msisdn_sn' => trim($test->data->SN),
                    'msisdn' => trim($test->data->MSISDNNr),
                    'msisdn_puk1' => $test->data->PUK1,
                    'msisdn_puk2' => $test->data->PUK2,
                    'msisdn_sim' => $_POST['msisdn_sim'],
                    'msisdn_languageid' => setVoiceMailLanguageByClientLang($client->language)
                     ));
                } else {
                    $this->Admin_model->update_services_data('mobile', $_POST['serviceid'], array(
                    'msisdn_sn' => trim($test->data->SN),
                    'donor_msisdn' => $order->details->msisdn,
                    'msisdn_puk1' => $test->data->PUK1,
                    'msisdn_puk2' => $test->data->PUK2,
                    'msisdn_sim' => $_POST['msisdn_sim'],
                    'msisdn_languageid' => setVoiceMailLanguageByClientLang($client->language)
                    ));
                }

                if ($order->details->msisdn_type == "porting") {
                    if (substr($order->details->msisdn, 0, 2) == "31") {
                        if (!$order->details->porting_sms) {
                            $this->send_PortinInitiation($serviceid);
                        }
                    }
                }
                $result = $this->artilium->ActiveNewSIM($client->firstname . ' ' . $client->lastname, $order->details);
                if ($result->result == "success") {
                    logAdmin(array(
                        'companyid' => $this->companyid,
                        'serviceid' => $serviceid,
                        'userid' => $client->id,
                        'user' =>  $_SESSION['reseller']['agent'],
                        'ip' => $_SERVER['REMOTE_ADDR'],
                        'description' => 'Service : ' . $serviceid . ' requested for Activation'
                    ));
                    if ($order->details->msisdn_type == "porting") {
                        //$this->send_PortinInitiation($serviceid);
                        $this->Admin_model->ChangeStatusService($serviceid, 'Pending');
                        $this->Admin_model->ChangeStatusOrderID($serviceid, 'PortinPending', 'mobile');
                    } //$order->details->msisdn_type == "porting"
                    else {
                        if (substr($order->details->msisdn, 0, 2) == "31") {
                            $pack = $this->artilium->GetListPackageOptionsForSnAdvance(trim($order->details->msisdn_sn));
                            $this->artilium->UpdateServices(trim($order->details->msisdn_sn), $pack, '1');
                        }
                        $this->Admin_model->ChangeStatusService($serviceid, 'Active');
                        $this->Admin_model->ChangeStatusOrderID($serviceid, 'Active', 'mobile');
                        // $this->send_welcome_email($serviceid);
                    }
                    $mobile  = $this->Admin_model->getService($serviceid);
                    $bundles = $this->Admin_model->getBundlebyProduct($order->packageid);
                    if ($bundles) {
                        $IDS = array();
                        // activate tarief per packages
                        foreach ($bundles as $bundleid) {
                            $r = $this->artilium->AddBundleAssign($order->details->msisdn_sn, $bundleid, date('Y-m-d') . 'T' . date('H:i:s'), '2099-12-31T23:59:59');
                            if ($r->result == "success") {
                                if ($this->data['setting']->create_magebo_bundle) {
                                    $create = 1;
                                } //$this->data['setting']->create_magebo_bundle
                                else {
                                    $create = 0;
                                }
                                //$this->magebo->ProcessPricing($order, $client->mageboid, $bundleid, $create, false, $order->date_contract);
                                $IDS[] = $r->id;
                            } //$r->result == "success"
                            else {
                                $IDS[] = $r;
                            }
                        } //$bundles as $bundleid
                        /* if ($IDS) {
                        logAdmin(array('companyid' => $this->companyid,
                        'serviceid' => $serviceid,
                        'userid' => $client->id,
                        'user' => $this->session->firstname . ' ' . $this->session->lastname,
                        'ip' => $_SERVER['REMOTE_ADDR'],
                        'description' => 'Service : ' . $_POST['serviceid'] . ' added bundle id ' . implode(',', $IDS),
                        ));
                        $this->Admin_model->updateBundleID($order->id, implode(',', $IDS));
                        } //$IDS
                        */
                    } //$bundles
                } else {
                    echo json_encode(array(
                        'result' => false,
                        'message' => 'Error on activating simcard',
                        'description' => $result
                    ));
                    exit;
                }
                $ser = $this->Admin_model->getService($_POST['serviceid']);
                $this->db->query("update a_services set status=?,identity=? where id=?", array('Active',$ser->details->msisdn,  $_POST['serviceid']));
                $this->db->query("update a_services_mobile set msisdn=?, msisdn_status=? where serviceid=?", array($ser->details->msisdn, 'Active', $_POST['serviceid']));

                echo json_encode(array(
                    'result' => true
                ));
                exit;
            } else {
                $sim = simcardExist(trim($_POST['msisdn_sim']));
                if ($sim) {
                    $this->db->query("update a_reseller_simcard set serviceid=? where simcard=?", array(
                        $_POST['serviceid'],
                        trim($_POST['msisdn_sim'])
                    ));
                    $order  = $this->Admin_model->getService($_POST['serviceid']);
                    $client = $this->Admin_model->getClient($order->userid);
                    //print_r($client);
                    $this->Admin_model->update_services_data('mobile', $_POST['serviceid'], array(
                        'msisdn_sim' => $_POST['msisdn_sim'],
                        'msisdn' => $sim->MSISDN,
                        'platform' => 'TEUM',
                        'msisdn_pin' => $sim->PIN1,
                        'msisdn_sn' => $sim->MSISDN,
                        'msisdn_puk1' => $sim->PUK1,
                        'msisdn_puk2' => $sim->PUK2,
                        'date_modified' => date('Y-m-d H:i:s')
                    ));
                }
                $this->load->library('pareteum', array(
                    'companyid' => $this->companyid,
                    'api_id' => $order->api_id
                ));
                if (empty($client->teum_CustomerId)) {
                    $array = array(
                        "CustomerData" => array(
                            "ExternalCustomerId" => (string) $client->mvno_id,
                            "FirstName" => $client->firstname,
                            "LastName" => $client->lastname,
                            "LastName2" => 'NA',
                            "CustomerDocumentType" => $client->id_type,
                            "DocumentNumber" => $client->idcard_number,
                            "Telephone" => $client->phonenumber,
                            "Email" => $client->email,
                            "LanguageName" => "eng",
                            "FiscalAddress" => array(
                                "Address" => $client->address1,
                                "City" => $client->city,
                                "CountryId" => "76",
                                "HouseNo" => $client->housenumber,
                                "State" => "Unknown",
                                "ZipCode" => $client->postcode
                            ),
                            "CustomerAddress" => array(
                                "Address" => $client->address1,
                                "City" => $client->city,
                                "CountryId" => "76",
                                "HouseNo" => $client->housenumber,
                                "State" => "Unknown",
                                "ZipCode" => $client->postcode
                            ),
                            "Nationality" => "GB"
                        ),
                        "comments" => "Customer added via UnitedPortal V1 by Reseller." . $_SESSION['reseller']['agent']
                    );
                    log_message('error', json_encode($array));
                    $teum = $this->pareteum->AddCustomers($array);
                    if ($teum->resultCode == "0") {
                        $client->teum_CustomerId = $teum->customerId;
                        $this->db->query("update a_clients set teum_CustomerId=?, teum_OrderCode=? where id=?", array(
                            $teum->customerId,
                            $teum->orderCode,
                            $client->id
                        ));
                    }
                    // echo json_encode($teum);
                    //print_r($teum);
                }
                if (empty($sim->AccountId)) {
                    $account   = array(
                        "AccountInfo" => array(
                            "AccountType" => "Prepaid",
                            "CustomerId" => $client->teum_CustomerId,
                            "ExternalAccountId" => (string)$_POST['serviceid'].''.rand(10000000, 99999999),
                            "AccountStatus" => "Active",
                            "Names" => array(
                                array(
                                    "LanguageCode" => "eng",
                                    "Text" => "Account"
                                )
                            ),
                            "Descriptions" => array(
                                array(
                                    "LanguageCode" => "eng",
                                    "Text" => "Account"
                                )
                            ),
                            "AccountCurrency" => "GBP",
                            "Balance" => 0,
                            "CreditLimit" => 0
                        )
                    );
                    //print_r($account);
                    $acct      = $this->pareteum->CreateAccount($account);
                    $AccountId = $acct->AccountId;
                } else {
                    $AccountId = $sim->AccountId;
                    $acct      = (object) array(
                        "resultCode" => "0",
                        "AccountId" => $AccountId
                    );
                }
                //print_r($acct);
                log_message('error', print_r($acct, true));
                if ($acct->resultCode == "0") {
                    /*
                    array(
                    'name' => $addons->name,
                    'terms' =>  30*$_POST['month'],
                    'cycle' => 'day',
                    'serviceid' => $_POST['serviceid'],
                    'addonid' => $_POST['bundleid'],
                    'recurring_total' => $addons->recurring_total,
                    'addon_type' => 'option',
                    'arta_bundleid' =>  $addons->bundleid,
                    'teum_DateStart' => date('Y-m-d'),
                    'teum_CustomerOrderId' => $subscription->CustomerOrderId,
                    'teum_SubscriptionId' => $subscription->Subscription->SubscriptionId,
                    'teum_ProductId' => $subscription->Subscription->Products[0]->ProductId,
                    'teum_ProductChargePurchaseId' => $subscription->Subscription->Products[0]->ProductChargePurchaseId,
                    'teum_SubscriptionProductAssnId' => $subscription->Subscription->Products[0]->SubscriptionProductAssnId,
                    'teum_ServiceId' => $subscription->Subscription->Services[0]->ServiceId,

                    )
                    */
                    $this->db->query("update a_reseller_simcard set AccountId=? where serviceid=?", array(
                        $acct->AccountId,
                        $_POST['serviceid']
                    ));
                    $addons = getaddons($_POST['serviceid'], $sim->MSISDN);
                    $subs   = array(
                        "CustomerId" => (int) $client->teum_CustomerId,
                        "Items" => array(
                            array(
                                "AccountId" => (string) $AccountId,
                                "ProductOfferings" => $addons,
                                "ServiceAddress" => array(
                                    "Address" => $client->address1,
                                    "HouseNo" => $client->housenumber,
                                    "City" => $client->city,
                                    "ZipCode" => $client->postcode,
                                    "State" => "unknown",
                                    "CountryId" => "76"
                                )
                            )
                        ),
                        "channel" => "UnitedPortal V1"
                    );
                    log_message('error', print_r($sim, true));
                    log_message('error', print_r($subs, true));
                    //print_r($subs);
                    $subscription = $this->pareteum->AddSubscription($subs);
                    log_message('error', print_r($subscription, true));
                    if ($subscription->resultCode == "0") {
                        $this->db->query("update a_reseller_simcard set SubscriptionId=? where serviceid=?", array($subscription->Subscription->SubscriptionId, $_POST['serviceid']));
                        $this->Admin_model->update_services_data('mobile', $_POST['serviceid'], array('teum_customerid' => $client->teum_CustomerId, 'teum_subscriptionid' => $subscription->Subscription->SubscriptionId));
                        foreach ($subscription->Subscription->Products as $key => $row) {
                            foreach ($addons as $key => $r) {
                                if ($r['ProductOfferingId'] == $row->ProductOfferingId) {
                                    $addx = getAddonsbyBundleID($row->ProductOfferingId);
                                    $days  = ($addx->teum_autoRenew+1)*30;
                                    $this->Admin_model->updateAddonTeum($_POST['serviceid'], $row->ProductOfferingId, array(
                                        'companyid' => $this->companyid,
                                        'teum_DateStart' => date('Y-m-d'),
                                        'teum_NextRenewal' => getFuturedate(date('Y-m-d'), 'day', 31),
                                        'teum_DateEnd' => getFuturedate(date('Y-m-d'), 'day', $days),
                                        'teum_CustomerOrderId' => $subscription->CustomerOrderId,
                                        'teum_SubscriptionId' => $subscription->Subscription->SubscriptionId,
                                        'teum_ProductId' => $row->ProductId,
                                        'teum_ProductChargePurchaseId' => $row->ProductChargePurchaseId,
                                        'teum_SubscriptionProductAssnId' => $row->SubscriptionProductAssnId,
                                        'teum_ServiceId' => $subscription->Subscription->Services[$key]->ServiceId
                                    ));
                                    if ($key == "0") {
                                        $this->db->query("update a_reseller_simcard set TeumServiceId=?,CustomerOrderId=? where serviceid=?", array($subscription->Subscription->Services[$key]->ServiceId,$subscription->CustomerOrderId, $_POST['serviceid']));
                                    }
                                }
                            }
                        }
                        $service = $this->Admin_model->getService($_POST['serviceid']);
                        $this->Admin_model->insertTopup(
                               array(
                               'companyid' => $this->companyid,
                               'serviceid' => $service->id,
                               'userid' =>  $service->userid,
                               'income_type' => 'bundle',
                               'agentid' => $service->agentid,
                               'amount' => $service->recurring,
                               'user' => $_SESSION['reseller']['agent'])
                           );
                        if ($service->details->msisdn_type == "porting") {
                            $this->Admin_model->update_services_data('mobile', $_POST['serviceid'], array(
                            'msisdn_status' => 'PortinPending',
                            'date_modified' => date('Y-m-d H:i:s'),
                            'teum_accountid' => $AccountId,
                            ));
                            $this->session->set_flashdata('success', lang('Activation has been Requested but the number needs to be ported in, once it ported in you ll need to set it ported'));
                        } else {
                            $this->Admin_model->update_services_data('mobile', $_POST['serviceid'], array(
                            'msisdn_status' => 'Active',
                            'date_modified' => date('Y-m-d H:i:s'),
                            'teum_accountid' => $AccountId,
                            ));
                            $this->session->set_flashdata('success', lang('Activation has been Requested'));
                        }

                        $this->db->query("update a_services set status='Active',identity=? where id=?", array(
                             $sim->MSISDN,
                             $_POST['serviceid'],
                        ));

                        send_growl(array(
                            'message' => $_SESSION['reseller']['agent'] . ' activate Number: ' . $order->details->msisdn,
                            'companyid' => $this->session->cid
                        ));
                        logAdmin(array(
                            'companyid' => $this->companyid,
                            'serviceid' => $serviceid,
                            'userid' => $client->id,
                            'user' => $_SESSION['reseller']['agent'],
                            'ip' => $_SERVER['REMOTE_ADDR'],
                            'description' => 'Service : ' . $serviceid . ' requested for '.$service->details->msisdn_type.' Activation ' . $sim->MSISDN
                        ));
                    } else {
                        //$this->session->set_flashdata('success', implode(', ', $subscription->messages));
                        echo json_encode(array(
                        'result' => 'error',
                        'message' => implode(', ', $subscription->messages)
                          ));
                        exit;
                    }
                }

                echo json_encode(array(
                    'result' => "success"
                ));
                exit;
            }
        } else {
        }


        $this->data['service']      = $this->Admin_model->getService($this->uri->segment(4));

        // print_r($this->data['service']);
        $this->data['client']       = $this->Admin_model->getClient($this->data['service']->userid);
        //print_r($this->data);
        $this->data['title']        = "Subscription Activation";
        $this->data['main_content'] = reseller_theme($this->companyid) . "subscription_activate_mobile";
        $this->load->view(reseller_theme($this->companyid) . "content", $this->data);
    }
    public function send_PortinInitiation($id)
    {
        /*if(empty($id)){
        $id = $this->uri->segment(4);
        }
        */
        if (!reseller_allow_perm('service', $this->session->reseller['id'])) {
            $this->session->set_flashdata('error', lang('You do not have access to this feature, please contact your Upstream'));
            redirect('reseller');
        }
        $mobile                = $this->Admin_model->getServiceCli($id);
        $client                = $this->Admin_model->getClient($mobile->userid);
        $this->data['setting'] = globofix($client->companyid);
        $this->db->query("update a_services_mobile set porting_sms = ? where serviceid=?", array(
            1,
            $id
        ));
        if ($mobile->details->donor_accountnumber) {
            return array(
                'result' => 'succes'
            );
        } else {
            if ($this->data['setting']->enable_sms == "1" && $this->data['setting']->sms_content != "") {
                $num = trim($mobile->details->msisdn);
                $this->load->library('sms', array(
                    'username' => $this->data['setting']->sms_username,
                    'password' => $this->data['setting']->sms_password,
                    'companyid' => $this->companyid
                ));
                $sms_res = $this->sms->send_message_bulksms(array(
                    array(
                        'from' => $this->data['setting']->sms_senderid,
                        'to' => '+' . $num,
                        'body' => $this->data['setting']->sms_content
                    )
                ));
            }
            if ($this->data['setting']->smtp_type == 'smtp') {
                $config = array(
                    'protocol' => 'smtp',
                    'smtp_host' => $this->data['setting']->smtp_host,
                    'smtp_port' => $this->data['setting']->smtp_port,
                    'smtp_user' => $this->data['setting']->smtp_user,
                    'smtp_pass' => $this->encryption->decrypt($this->data['setting']->smtp_pass),
                    'mailtype' => 'html',
                    'charset' => 'utf-8',
                    'starttls' => true,
                    'wordwrap' => true
                );
            } else {
                $config['protocol'] = 'sendmail';
                $config['mailpath'] = '/usr/sbin/sendmail';
                $config['mailtype'] = 'html';
                $config['charset']  = 'utf-8';
                $config['wordwrap'] = true;
            }
            $this->email->clear(true);
            $this->email->initialize($config);
            $body = getMailContent('portin_initiation', $client->language, $client->companyid);
            $body = str_replace('{$name}', format_name($client), $body);
            $body = str_replace('{$base_url}', url_to_domain(base_url()), $body);
            $body = str_replace('{$msisdn_sim}', $mobile->details->msisdn_sim, $body);
            $body = str_replace('{$msisdn_puk1}', $mobile->etails->msisdn_puk1, $body);
            $body = str_replace('{$msisdn_puk2}', $mobile->etails->msisdn_puk2, $body);
            $body = str_replace('{$msisdn', $mobile->etails->msisdn, $body);
            $this->email->set_newline("\r\n");
            $this->email->from($this->data['setting']->smtp_sender, $this->data['setting']->smtp_name);
            $this->email->to($client->email);
            //$this->email->bcc('mail@simson.one');
            $this->email->subject(getSubject('portin_initiation', $client->language, $client->companyid));
            $this->email->message($body);
            if ($this->email->send()) {
                logEmailOut(array(
                    'userid' => $client->id,
                    'companyid' => $client->companyid,
                    'to' => $client->email,
                    'subject' => getSubject('portin_initiation', $client->language, $client->companyid),
                    'message' => $body
                ));
                return array(
                    'result' => 'success'
                );
            } else {
                logEmailOut(array(
                    'userid' => $client->id,
                    'companyid' => $client->companyid,
                    'to' => $client->email,
                    'subject' => getSubject('service_change_request', $client->language, $client->companyid),
                    'message' => $body,
                    'status' => 'error',
                    'error_message' => $this->email->print_debugger()
                ));
                return array(
                    'result' => 'error',
                    'message' => 'error when sending email'
                );
            }
        }
    }
    public function reload()
    {
        if (!reseller_allow_perm('reload', $this->session->reseller['id'])) {
            $this->session->set_flashdata('error', lang('You do not have access to this feature, please contact your Upstream'));
            redirect('reseller');
        }
        if (!hasPrepaidProduct($this->session->cid, $this->session->reseller['id'])) {
            $this->session->set_flashdata('error', lang('You will need a product which is prepaid, please contact your account manager'));
            redirect('reseller');
        }
        $this->data['title']        = lang("Topup a Number");
        $this->load->model('Agent_model');
        $this->data['agent']        = $this->Agent_model->getAgent($_SESSION['reseller']['id']);
        $this->data['main_content'] = reseller_theme($this->companyid) . "reload";
        $this->load->view(reseller_theme($this->companyid) . "content", $this->data);
    }
    public function reloadCredit()
    {
        if (!reseller_allow_perm('reload', $this->session->reseller['id'])) {
            $this->session->set_flashdata('error', lang('You do not have access to this feature, please contact your Upstream'));
            redirect('reseller');
        }

        //echo json_encode($_POST);
        // exit;
        $this->load->library('artilium', array(
            'companyid' => $this->companyid
        ));
        $service = $this->Admin_model->getService($_POST['serviceid']);
        $client  = $this->Admin_model->getService($_POST['userid']);
        $this->load->model('Agent_model');
        $agent = $this->Agent_model->getAgent($client->agentid);
        if ($agent->reseller_type == "Prepaid") {
            //we need to set time limit to 0 because we are going to lock balance to avoid the same request at the same time
            set_time_limit(0);
            do {
                if (!file_exists(APPPATH.'lock/reseller_balance_'.$client->agentid)) {
                    log_message('error', 'No locking, we continue adding balance and now we lock the customer balance');
                    file_put_contents(APPPATH.'lock/reseller_'.$client->agentid, $_POST['serviceid']);
                    break;
                }
            } while (true);
        }
        if ($agent->reseller_type == "Prepaid") {
            if ($_POST['credit'] > $agent->reseller_balance) {
                $this->session->set_flashdata('error', lang('You do not have enough balance to topup this subscriber'));
                unlink(APPPATH.'lock/reseller_balance_'.$client->agentid, $_POST['serviceid']);
                redirect('admin/subscription/detail/'.$_POST['serviceid']);
            }
        }


        if ($service) {
            $res = $this->artilium->CReloadCredit($_POST['credit'], $service->details->msisdn_sn, rand(100000000000000, 99999999999999999), 'Ordered by:' . $this->session->firstname);
            if ($res->ReloadResult->Result == 0) {
                if ($agent->reseller_type == "Prepaid") {
                    $new_balance = $agent->reseller_balance - $_POST['credit'];
                    $this->db->query("update a_clients_agents set reseller_balance=? where id=?", array($new_balance, $agent->id));
                    unlink(APPPATH.'lock/reseller_balance_'. $agent->id, $_POST['serviceid']);
                }

                logAdmin(array(
                    'companyid' => $this->companyid,
                    'userid' => $_POST['userid'],
                    'serviceid' => $_POST['serviceid'],
                    'user' => $this->session->firstname . ' ' . $this->session->lastname,
                    'ip' => $_SERVER['REMOTE_ADDR'],
                    'description' => 'add credit amount of ' . $_POST['credit'] . ' to serviceid: ' . $_POST['serviceid']
                ));


                $this->Admin_model->insertTopup(array(
                    'companyid' => $this->companyid,
                    'serviceid' => $_POST['serviceid'],
                    'userid' =>  $_POST['userid'],
                    'income_type' => 'topup',
                    'agentid' => $_SESSION['reseller']['id'],
                    'amount' =>  $_POST['credit'],
                    'user' =>  $_SESSION['reseller']['agent']));


                echo json_encode(array(
                    'result' => true,
                    'reload' => $res
                ));
            } else {
                echo json_encode(array(
                    'result' => false,
                    'data' => $res,
                    'service' => $service
                ));
            }
        } else {
            echo json_encode(array(
                'result' => false
            ));
        }
    }
    public function change_packagesetting()
    {
        $this->load->library('artilium', array(
            'companyid' => $this->companyid
        ));
        if (!empty($_POST['sn']) && !empty($_POST['id']) && isset($_POST['val'])) {
            if ($_POST['id'] == 100000) {
                $result = $this->artilium->SwitchVoiceMail($_POST['msisdn'], $_POST['sn'], $_POST['val']);
            } else {
                $result = $this->artilium->UpdatePackageOptionsForSN(trim($_POST['sn']), $_POST['id'], $_POST['val']);
                logAdmin(array(
                    'companyid' => $this->companyid,
                    'user' => $this->session->reseller['agent'],
                    'serviceid' => $_POST['serviceid'],
                    'userid' => $_POST['userid'],
                    'ip' => $_SERVER['REMOTE_ADDR'],
                    'description' => 'UpdatePackageOptionsForSn for PackageDefinitionId ' . $_POST['id'] . ' to ' . $_POST['val']
                ));
            }
            echo json_encode($result);
        } //!empty($_POST['sn']) && !empty($_POST['id']) && isset($_POST['val'])
        else {
            echo json_encode(array(
                'result' => 'error',
                'message' => 'Not enough data: sn,id,val'
            ));
        }
    }

    public function getsimcard()
    {
        $companyrange = getCompanyRange($this->session->cid);
        $setting = globofix($this->session->cid);

        $list1        = array();
        $keyword      = '%' . $_POST['keyword'] . '%';

        if ($setting->mobile_platform == "TEUM") {
            $this->db     = $this->load->database('default', true);
            $query1       = $this->db->query("SELECT simcard as value, MSISDN as label,MSISDN as msisdn,serviceid  from a_reseller_simcard where  MSISDN like ? and serviceid is not null and companyid=? order by simcard asc LIMIT 10", array(
            $keyword,
            $this->companyid
            ));
        } else {
            if ($this->session->cid == "104") {
                $this->db     = $this->load->database('default', true);
                $query1       = $this->db->query("SELECT simcard as value, MSISDN as label, MSISDN as msisdn,serviceid from a_reseller_simcard where MSISDN like ? and serviceid is not null and companyid=? order by simcard asc LIMIT 10", array(
                $keyword,
                $this->companyid
                ));
            } else {
                $this->db     = $this->load->database('default', true);
                $query1       = $this->db->query("SELECT msisdn_sim as value msisdn as label, msisdn as msisdn, serviceid from a_services_mobile where companyid = ? and msisdn_status =? and msisdn like ? order by simcard asc LIMIT 10", array(
                $this->session->cid,
                'Active',
                $keyword
                ));
            }
        }

        log_message('error', $this->db->last_query());

        if ($query1->num_rows() > 0) {
            $list1 = $query1->result_array();
        }
        echo json_encode($list1);
    }


    public function change_call_ceilling()
    {
        if (!isAllowed_reseller_service($this->session->reseller['id'], $_POST['serviceid'])) {
            echo json_encode(array(
                'result' => 'error',
                'message' => 'access denied'
            ));
        }
        $order = $this->Admin_model->getService($_POST['serviceid']);
        $this->load->library('artilium', array(
            'companyid' => $order->companyid
        ));
        echo json_encode($this->artilium->CreateAssignment(array(
            'SN' => $_POST['sn'],
            'ActionSetId' => $_POST['SetId'],
            'AssignmentId' => preg_replace('/\D/', '', $_POST['PlanId'])
        )));
    }
    public function download_letter()
    {
        if ($this->uri->segment(5) == "new") {
            $this->download_new_letter($this->uri->segment(4));
        } elseif ($this->uri->segment(5) == "porting") {
            $this->download_porting_letter($this->uri->segment(4));
        } elseif ($this->uri->segment(5) == "swap") {
            $this->download_swap_letter($this->uri->segment(4));
        }
    }
    public function swap_simcard()
    {
        $this->load->library('artilium', array(
            'companyid' => $this->companyid
        ));
        $this->data['mobile'] = $this->Admin_model->getServiceCli($_POST['serviceid']);
        //$number = $this->magebo->getPincodeRef(trim($this->data['mobile']->details->msisdn));
        $swap                 = $this->artilium->SwapSim(array(
            'SN' => trim($_POST['SN']),
            'NewSimNr' => $_POST['new_simnumber']
        ));
        if ($swap->result == "success") {
            $this->artilium->UpdateCLI($sn->SN, 1);
            $this->session->set_flashdata('success', 'SimSwap successfully executed');
            if ($_POST['charge'] == "YES") {
                if (!empty($_POST['amount'])) {
                    $amount = $_POST['amount'];
                } //!empty($_POST['amount'])
                else {
                    $amount = getSwapCost($this->data['mobile']->packageid, $this->companyid);
                }
                if ($amount > 0) {
                    $client = $this->Admin_model->getClient($this->data['mobile']->userid);
                    // $this->magebo->x_magebosubscription_addPricing($client->mageboid, 359, 1, exvat4($client->vat_rate, $amount), 1, 'Sim Replacement ' . $number . ' - ' . date('d/m/Y'), date('m/d/Y'), date('m'), date('Y'), 1, 0, 'Sim Replacement  ' . $number . ' - ' . date('d/m/Y'), 'Sim Replacement  ' . $number . ' - ' . date('d/m/Y'), 0);
                } //$amount > 0
            } //$_POST['charge'] == "YES"
            $ss = $this->artilium->GetSn($_POST['new_simnumber']);
            $sn = (object) $ss->data;
            $this->Admin_model->updateService($_POST["serviceid"], array(
                'msisdn_swap' => 1,
                'msisdn_puk1' => $sn->PUK1,
                'msisdn_puk2' => $sn->PUK2,
                'msisdn_sim' => trim($_POST['new_simnumber']),
                'msisdn_sn' => trim($sn->SN)
            ));
            $headers = "From: noreply@united-telecom.be" . "\r\n" . "CC: simson.parlindungan@united-telecom.be";
            $body    = "";
            $body .= json_encode($_POST) . ' ' . print_r($sn, true);
            mail('simson.parlindungan@united-telecom.be', 'Swap success', $body, $headers);
            redirect('reseller/subscription/detail/' . $_POST['serviceid']);
        } //$swap->result == "success"
        else {
            $headers = "From: noreply@united-telecom.be" . "\r\n" . "CC: simson.parlindungan@united-telecom.be";
            $body    = "";
            $body .= json_encode($_POST);
            mail('simson.parlindungan@united-telecom.be', 'Swap Failed', $body, $headers);
            $this->session->set_flashdata('error', 'SimSwap was not successfully executed, please contact support, a notification has been dispatched to United IT Department');
            redirect('reseller/subscription/detail/' . $_POST['serviceid']);
        }
    }
    public function download_new_letter($id)
    {
        if (!isAllowed_reseller_service($this->session->reseller['id'], $id)) {
            $this->session->set_flashdata('error', 'Access Denied');
            redirect('reseller/dashboard/client');
        }
        $this->load->library('pdf');
        $service   = $this->Admin_model->getService($id);
        $client    = $this->Admin_model->getClient($service->userid);
        $brand     = $this->Admin_model->getBrandPdfFooter($service->gid);
        $body_data = $this->Admin_model->getWelcomeBody($service->gid, trim($client->language));
        $name      = format_name($client);
        $body      = str_replace('{$name}', trim($name), $body_data->body);
        $body      = str_replace('{$date}', date('d F Y'), $body);
        $body      = str_replace('{$msisdn_sim}', $service->details->msisdn_sim, $body);
        $body      = str_replace('{$clientid}', $client->mvno_id, $body);
        $body      = str_replace('{$msisdn}', $service->details->msisdn, $body);
        $body      = str_replace('{$msisdn_puk1}', $service->details->msisdn_puk1, $body);
        $body      = str_replace('{$msisdn_puk2}', $service->details->msisdn_puk2, $body);
        $body      = str_replace('{$email}', $this->data['setting']->smtp_sender, $body);
        $body      = str_replace('{$companyname}', $brand->name, $body);
        $body      = str_replace('{$portal_url}', base_url(), $body);
        //$this->pdf->SetHeaderData($brand->brand_logo, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 006', PDF_HEADER_STRING);
        // set auto page breaks
        $this->pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);
        $this->pdf->setFoot($brand->brand_footer);
        $this->pdf->setFooterFont($body_data->font);
        // set image scale factor
        $this->pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
            require_once(dirname(__FILE__) . '/lang/eng.php');
            $this->pdf->setLanguageArray($l);
        }
        $this->pdf->SetPrintHeader(true);
        $this->pdf->SetPrintFooter(true);
        $this->pdf->SetFont($body_data->font, '', $body_data->font_size);
        $this->pdf->AddPage(); // pretty self-explanatory
        $this->pdf->Image(FCPATH . 'assets/img/simcard.png', 130, 228, 70);
        $this->pdf->Image($brand->brand_logo, 10, 10, 60);
        $style  = array(
            'position' => '',
            'align' => 'R',
            'stretch' => false,
            'fitwidth' => true,
            'cellfitalign' => '',
            'border' => false,
            'hpadding' => 'auto',
            'vpadding' => 'auto',
            'fgcolor' => array(
                0,
                0,
                0
            ),
            'bgcolor' => false,
            'text' => true,
            'font' => 'roboto',
            'fontsize' => 8,
            'stretchtext' => 6
        );
        $style1 = array(
            'position' => '',
            'align' => 'R',
            'stretch' => false,
            'fitwidth' => true,
            'cellfitalign' => '',
            'border' => true,
            'hpadding' => 'auto',
            'vpadding' => 'auto',
            'fgcolor' => array(
                0,
                0,
                0
            ),
            'bgcolor' => false,
            'color' => array(
                255,
                255,
                255
            ),
            'text' => true,
            'font' => 'droidsans',
            'fontsize' => 8,
            'stretchtext' => 6
        );
        $this->pdf->ln(20);
        $this->pdf->setY(13);
        $this->pdf->setX(130);
        //$this->pdf->write1DBarcode(preg_replace("/[^0-9,.]/", "", $invoice['invoicenum']), 'I25', '', '', '', 18, 0.9, $style, 'R');
        $this->pdf->GetPage();
        $this->pdf->setY(50);
        if (!empty($client->companyname)) {
            $company = $client->companyname . "\n";
            $contact = '';
        } else {
            $company = '';
            if ($client->language == "japanese") {
                $contact = $name . "-san\n";
            } else {
                $contact = $name . "\n";
            }
        }
        $address1 = str_replace("&#039;", "'", $client->address1) . "\n";
        $city     = $client->postcode . " " . $client->city . "\n";
        $country  = $client->country . "\n";
        $this->pdf->SetFont($body_data->font, '', $body_data->font_size + 1);
        $this->pdf->setCellPaddings(0, 0, 0, 0);
        $this->pdf->setCellMargins(0, 0, 0, 0);
        $this->pdf->SetFillColor(255, 255, 255);
        $a = '';
        $this->pdf->MultiCell(100, 10, $a, 0, 'J', 0, 0, '', '', true, 0, false, true, 20, 'T');
        if ($country != "NL") {
            $this->pdf->MultiCell(100, 20, $company . '' . $contact . '' . $address1 . '' . $city . '' . getCountryName($country), 0, 'L', 1, 1, '', '', true, 0, false, true, 20, 'M');
        } else {
            $this->pdf->MultiCell(100, 20, $company . '' . $contact . '' . $address1 . '' . $city, 0, 'L', 1, 1, '', '', true, 0, false, true, 20, 'M');
        }
        $this->pdf->Ln(10);
        $this->pdf->SetFont($body_data->font, '', $body_data->font_size);
        $this->pdf->writeHTML($body, true, false, true, false, '');
        //$this->pdf->writeHTML(0, 0, '', '', $brand->brand_footer, 0, 0, false, "L", true);
        $this->pdf->Output('Welcome_letter_' . $client->mvno_id . '_' . $service->details->msisdn . '.pdf'); // send the file inline to the browser (default).
    }
    public function download_porting_letter($id)
    {
        if (!isAllowed_reseller_service($this->session->reseller['id'], $id)) {
            $this->session->set_flashdata('error', 'Access Denied');
            redirect('reseller/dashboard/client');
        }
        $this->load->library('pdf');
        $service   = $this->Admin_model->getService($id);
        $client    = $this->Admin_model->getClient($service->userid);
        $brand     = $this->Admin_model->getBrandPdfFooter($service->gid);
        $body_data = $this->Admin_model->getPdfBody($service->gid, trim($client->language), 'Porting');
        if (!empty($client->salutation)) {
            $name = $client->salutation . " " . $client->firstname . " " . $client->lastname;
        } else {
            $name = $client->firstname . " " . $client->lastname;
        }
        $body = str_replace('{$name}', $name, $body_data->body);
        $body = str_replace('{$date}', date('d F Y'), $body);
        $body = str_replace('{$msisdn_sim}', $service->details->msisdn_sim, $body);
        $body = str_replace('{$clientid}', $client->mvno_id, $body);
        $body = str_replace('{$msisdn}', $service->details->msisdn, $body);
        $body = str_replace('{$msisdn_puk1}', $service->details->msisdn_puk1, $body);
        $body = str_replace('{$msisdn_puk2}', $service->details->msisdn_puk2, $body);
        $body = str_replace('{$email}', $this->data['setting']->smtp_sender, $body);
        $body = str_replace('{$companyname}', $brand->name, $body);
        $body = str_replace('{$portal_url}', base_url(), $body);
        //$this->pdf->SetHeaderData($brand->brand_logo, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 006', PDF_HEADER_STRING);
        // set auto page breaks
        $this->pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);
        $this->pdf->setFoot($brand->brand_footer);
        $this->pdf->setFooterFont($body_data->font);
        // set image scale factor
        $this->pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
            require_once(dirname(__FILE__) . '/lang/eng.php');
            $this->pdf->setLanguageArray($l);
        }
        $this->pdf->SetPrintHeader(true);
        $this->pdf->SetPrintFooter(true);
        $this->pdf->SetFont($body_data->font, '', $body_data->font_size);
        $this->pdf->AddPage(); // pretty self-explanatory
        $this->pdf->Image(FCPATH . 'assets/img/simcard.png', 130, 228, 70);
        $this->pdf->Image($brand->brand_logo, 10, 10, 60);
        $style  = array(
            'position' => '',
            'align' => 'R',
            'stretch' => false,
            'fitwidth' => true,
            'cellfitalign' => '',
            'border' => false,
            'hpadding' => 'auto',
            'vpadding' => 'auto',
            'fgcolor' => array(
                0,
                0,
                0
            ),
            'bgcolor' => false,
            'text' => true,
            'font' => 'roboto',
            'fontsize' => 8,
            'stretchtext' => 6
        );
        $style1 = array(
            'position' => '',
            'align' => 'R',
            'stretch' => false,
            'fitwidth' => true,
            'cellfitalign' => '',
            'border' => true,
            'hpadding' => 'auto',
            'vpadding' => 'auto',
            'fgcolor' => array(
                0,
                0,
                0
            ),
            'bgcolor' => false,
            'color' => array(
                255,
                255,
                255
            ),
            'text' => true,
            'font' => 'droidsans',
            'fontsize' => 8,
            'stretchtext' => 6
        );
        $this->pdf->ln(20);
        //$this->pdf->writeHTML($tblhtml, true, false, false, false, '');
        $this->pdf->setY(13);
        $this->pdf->setX(130);
        //$this->pdf->write1DBarcode(preg_replace("/[^0-9,.]/", "", $invoice['invoicenum']), 'I25', '', '', '', 18, 0.9, $style, 'R');
        $this->pdf->GetPage();
        $this->pdf->setY(50);
        if (!empty($client->companyname)) {
            $company = $client->companyname . "\n";
            $contact = '';
        } else {
            $company = '';
            if (!empty($client->firstname)) {
                if ($client->language == "japanese") {
                    $contact = format_name($client) . "-san" . "\n";
                } else {
                    $contact = format_name($client) . "\n";
                }
            } else {
                if ($client->language == "japanese") {
                    $contact = format_name($client) . "-san\n";
                } else {
                    $contact = format_name($client) . "\n";
                }
            }
        }
        $address1 = str_replace("&#039;", "'", $client->address1) . "\n";
        $city     = $client->postcode . " " . $client->city . "\n";
        $country  = $client->country . "\n";
        $this->pdf->SetFont($body_data->font, '', $body_data->font_size + 1);
        $this->pdf->setCellPaddings(0, 0, 0, 0);
        $this->pdf->setCellMargins(0, 0, 0, 0);
        $this->pdf->SetFillColor(255, 255, 255);
        $a = '';
        $this->pdf->MultiCell(100, 10, $a, 0, 'J', 0, 0, '', '', true, 0, false, true, 20, 'T');
        if ($country != "NL") {
            $this->pdf->MultiCell(100, 20, $company . '' . $contact . '' . $address1 . '' . $city . '' . getCountryName($country), 0, 'L', 1, 1, '', '', true, 0, false, true, 20, 'M');
        } else {
            $this->pdf->MultiCell(100, 20, $company . '' . $contact . '' . $address1 . '' . $city, 0, 'L', 1, 1, '', '', true, 0, false, true, 20, 'M');
        }
        $this->pdf->Ln(10);
        $this->pdf->SetFont($body_data->font, '', $body_data->font_size);
        $this->pdf->writeHTML($body, true, false, true, false, '');
        //$this->pdf->writeHTML(0, 0, '', '', $brand->brand_footer, 0, 0, false, "L", true);
        $this->pdf->Output('Welcome_letter_' . $client->mvno_id . '_' . $service->details->msisdn . '.pdf'); // send the file inline to the browser (default).
    }
    public function download_swap_letter($id)
    {
        if (!isAllowed_reseller_service($this->session->reseller['id'], $id)) {
            $this->session->set_flashdata('error', 'Access Denied');
            redirect('reseller/dashboard/client');
        }
        $this->load->library('pdf');
        $service   = $this->Admin_model->getService($id);
        $client    = $this->Admin_model->getClient($service->userid);
        $brand     = $this->Admin_model->getBrandPdfFooter($service->gid);
        $body_data = $this->Admin_model->getPdfBody($service->gid, trim($client->language), 'Swap');
        if (!empty($client->salutation)) {
            $name = $client->salutation . " " . $client->firstname . " " . $client->lastname;
        } else {
            $name = $client->firstname . " " . $client->lastname;
        }
        $body = str_replace('{$name}', $name, $body_data->body);
        $body = str_replace('{$date}', date('d F Y'), $body);
        $body = str_replace('{$msisdn_sim}', $service->details->msisdn_sim, $body);
        $body = str_replace('{$clientid}', $client->mvno_id, $body);
        $body = str_replace('{$msisdn}', $service->details->msisdn, $body);
        $body = str_replace('{$msisdn_puk1}', $service->details->msisdn_puk1, $body);
        $body = str_replace('{$msisdn_puk2}', $service->details->msisdn_puk2, $body);
        $body = str_replace('{$email}', $this->data['setting']->smtp_sender, $body);
        $body = str_replace('{$companyname}', $brand->name, $body);
        $body = str_replace('{$portal_url}', base_url(), $body);
        //$this->pdf->SetHeaderData($brand->brand_logo, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 006', PDF_HEADER_STRING);
        // set auto page breaks
        $this->pdf->SetAutoPageBreak(false, PDF_MARGIN_BOTTOM);
        $this->pdf->setFoot($brand->brand_footer);
        $this->pdf->setFooterFont($body_data->font);
        // set image scale factor
        $this->pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
            require_once(dirname(__FILE__) . '/lang/eng.php');
            $this->pdf->setLanguageArray($l);
        }
        $this->pdf->SetPrintHeader(true);
        $this->pdf->SetPrintFooter(true);
        $this->pdf->SetFont($body_data->font, '', $body_data->font_size);
        $this->pdf->AddPage(); // pretty self-explanatory
        $this->pdf->Image(FCPATH . 'assets/img/simcard.png', 130, 228, 70);
        $this->pdf->Image($brand->brand_logo, 10, 10, 60);
        $style  = array(
            'position' => '',
            'align' => 'R',
            'stretch' => false,
            'fitwidth' => true,
            'cellfitalign' => '',
            'border' => false,
            'hpadding' => 'auto',
            'vpadding' => 'auto',
            'fgcolor' => array(
                0,
                0,
                0
            ),
            'bgcolor' => false,
            'text' => true,
            'font' => 'roboto',
            'fontsize' => 8,
            'stretchtext' => 6
        );
        $style1 = array(
            'position' => '',
            'align' => 'R',
            'stretch' => false,
            'fitwidth' => true,
            'cellfitalign' => '',
            'border' => true,
            'hpadding' => 'auto',
            'vpadding' => 'auto',
            'fgcolor' => array(
                0,
                0,
                0
            ),
            'bgcolor' => false,
            'color' => array(
                255,
                255,
                255
            ),
            'text' => true,
            'font' => 'droidsans',
            'fontsize' => 8,
            'stretchtext' => 6
        );
        $this->pdf->ln(20);
        $this->pdf->setY(13);
        $this->pdf->setX(130);
        //$this->pdf->write1DBarcode(preg_replace("/[^0-9,.]/", "", $invoice['invoicenum']), 'I25', '', '', '', 18, 0.9, $style, 'R');
        $this->pdf->GetPage();
        $this->pdf->setY(50);
        if (!empty($client->companyname)) {
            $company = $client->companyname . "\n";
            $contact = '';
        } else {
            $company = '';
            if (!empty($client->firstname)) {
                if ($client->language == "japanese") {
                    $contact = $client->salutation . " " . $client->firstname . " " . $client->lastname . "-san" . "\n";
                } else {
                    $contact = $client->salutation . " " . $client->firstname . " " . $client->lastname . "\n";
                }
            } else {
                if ($client->language == "japanese") {
                    $contact = $client->salutation . " " . $client->initial . " " . $client->lastname . "-san\n";
                } else {
                    $contact = $client->salutation . " " . $client->initial . " " . $client->lastname . "\n";
                }
            }
            if ($client->language == "japanese") {
                $contact = $contact;
            }
        }
        $address1 = str_replace("&#039;", "'", $client->address1) . "\n";
        $city     = $client->postcode . " " . $client->city . "\n";
        $country  = $client->country . "\n";
        $this->pdf->SetFont($body_data->font, '', $body_data->font_size + 1);
        $this->pdf->setCellPaddings(0, 0, 0, 0);
        $this->pdf->setCellMargins(0, 0, 0, 0);
        $this->pdf->SetFillColor(255, 255, 255);
        $a = '';
        $this->pdf->MultiCell(100, 10, $a, 0, 'J', 0, 0, '', '', true, 0, false, true, 20, 'T');
        if ($country != "NL") {
            $this->pdf->MultiCell(100, 20, $company . '' . $contact . '' . $address1 . '' . $city . '' . getCountryName($country), 0, 'L', 1, 1, '', '', true, 0, false, true, 20, 'M');
        } else {
            $this->pdf->MultiCell(100, 20, $company . '' . $contact . '' . $address1 . '' . $city, 0, 'L', 1, 1, '', '', true, 0, false, true, 20, 'M');
        }
        $this->pdf->Ln(10);
        $this->pdf->SetFont($body_data->font, '', $body_data->font_size);
        $this->pdf->writeHTML($body, true, false, true, false, '');
        //$this->pdf->writeHTML(0, 0, '', '', $brand->brand_footer, 0, 0, false, "L", true);
        $this->pdf->Output('Welcome_letter_' . $client->mvno_id . '_' . $service->details->msisdn . '.pdf'); // send the file inline to the browser (default).
    }
    public function export_subscriptions()
    {
        $this->load->library('xlswriter');
        $fileName  = 'Subscription-Summary_by_Date_' . date('Y-m-d') . '.xlsx';
        $header    = array(
            'SubscriptionID' => 'integer',
            'Status' => 'string',
            'ArtiliumID' => 'integer',
            'MvnoID' => 'string',
            'Recurring' => 'string',
            'DateContract MM-DD-YYYY' => 'string',
            'DateCreated' => 'string',
            'Companyname' => 'string',
            'ProductName' => 'string',
            'Identifiers' => 'string',
            'OrderStatus' => 'string',
            'PackageId' => 'integer'
        );
        $companyid = $this->session->cid;
        $agentid   = $this->session->reseller['id'];
        $this->db  = $this->load->database('default', true);
        $query     = "select a.id,a.status,a.userid,c.mvno_id,a.recurring as recurring,date_contract,a.date_created,concat(c.firstname,' ', c.lastname, ' ', companyname)  as clientname, b.name as packagename,
    case when a.type  = 'mobile' THEN d.msisdn
    when a.type = 'xdsl' then e.circuitid
    when a.type = 'voip' then f.cli
    else 'Unknown' end as domain, case when a.type  = 'mobile' THEN d.msisdn_status
    when a.type = 'xdsl' then e.status
    when a.type = 'voip' then f.status
    else 'Unknown' end as orderstatus,a.packageid
 from a_services a
 left join a_products b on b.id=a.packageid
 left join a_clients c on c.id=a.userid
 left join a_services_mobile d on d.serviceid=a.id
 left join a_services_xdsl e on e.serviceid=a.id
 left join a_services_voip f on f.serviceid=a.id
 where a.status in ('Pending','Active')
 and a.companyid = " . $companyid . "
 and c.agentid = '$agentid'
 group by a.id";
        $q         = $this->db->query($query);
        if ($q->num_rows() > 0) {
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            $this->xlswriter->writeSheet($q->result_array(), 'Sheet1', $header);
            $this->xlswriter->setAuthor('Simson Parlindungan');
            $this->xlswriter->setTitle('Subscription Export');
            $this->xlswriter->setCompany('United');
            echo $this->xlswriter->writeToString();
            $this->session->set_flashdata('success', 'Downloaded as requested');
        } else {
            $this->session->set_flashdata('error', 'No data found');
        }
        redirect('reseller');
    }
    public function block_mobile_originating()
    {
        $_POST['serviceid'] = $this->uri->segment(4);
        $service            = $this->Admin_model->getService($_POST['serviceid']);
        if (!$service) {
            json_encode(array(
                'result' => false
            ));
            exit;
        }
        $_POST['SN'] = $service->details->msisdn_sn;
        $this->load->library('artilium', array(
            'companyid' => $this->session->cid
        ));
        $pack = $this->artilium->GetListPackageOptionsForSnAdvance(trim($_POST['SN']));
        $this->Admin_model->save_state($_POST['serviceid'], $pack);
        $this->artilium->BlockOriginating(trim($_POST['SN']), $pack, '0');
        $this->Admin_model->update_services_data('mobile', $_POST["serviceid"], array(
            'bar' => 1
        ));
        $this->Admin_model->update_services($_POST["serviceid"], array(
            'status' => 'Suspended'
        ));
        $this->session->set_flashdata('success', 'All Originating Service include International Calls has been barred');
        echo json_encode($service);
    }
    public function request_upgrade()
    {
        $this->db = $this->load->database('default', true);
        $this->db->insert('a_reseller_request_changes', array(
            'userid' => $_POST['userid'],
            'serviceid' => $_POST['serviceid'],
            'packageid' => $_POST['packageid'],
            'date_request' => date('Y-m-d'),
            'status' => 'Pending'
        ));
        log_message('error', $this->db->last_query());
        $id = $this->db->insert_id();
        if ($id) {
            $this->session->set_flashdata('success', 'Your request has been submitted');
        }
        if ($this->data['setting']->smtp_type == 'smtp') {
            $config = array(
                'protocol' => 'smtp',
                'smtp_host' => $this->data['setting']->smtp_host,
                'smtp_port' => $this->data['setting']->smtp_port,
                'smtp_user' => $this->data['setting']->smtp_user,
                'smtp_pass' => $this->encryption->decrypt($this->data['setting']->smtp_pass),
                'mailtype' => 'html',
                'charset' => 'utf-8',
                'starttls' => true,
                'wordwrap' => true
            );
        } else {
            $config['protocol'] = 'sendmail';
            $config['mailpath'] = '/usr/sbin/sendmail';
            $config['mailtype'] = 'html';
            $config['charset']  = 'utf-8';
            $config['wordwrap'] = true;
        }
        $this->email->initialize($config);
        //$this->data['main_content'] = 'email/' . $this->data['language'] . '/adminpasswordchanged.php';
        //$body = $this->load->view('email/content', $this->data, true);
        $body = "Hello<br>New Package change has been requested by Reseller<br />Userid: " . $_POST['userid'] . "<br />Package: " . $_POST["packagename"] . "<br/>Number: " . $_POST['msisdn'] . "<p>If you wish to accept or reject it please click <a href='" . base_url() . "admin/subscription/accept_reseller_changes/" . $id . "'  target='_blank'> Here </a></p><p>Regards</p><br />United Telecom";
        // $body = $this->load->view('email/content', $this->data, true);
        $this->email->set_newline("\r\n");
        $this->email->from($this->data['setting']->smtp_sender, $this->data['setting']->smtp_name);
        $this->email->to(explode('|', $this->data['setting']->sepa_email_notification));
        $this->email->subject('Reseller request product changes');
        $this->email->message($body);
        $this->email->send();
        echo json_encode(array(
            'result' => true
        ));
    }
    public function unblock_mobile_originating()
    {
        $_POST['serviceid'] = $this->uri->segment(4);
        $service            = $this->Admin_model->getService($_POST['serviceid']);
        if (!$service) {
            json_encode(array(
                'result' => false
            ));
            exit;
        }
        $_POST['SN'] = $service->details->msisdn_sn;
        $service     = $this->Admin_model->getPackageState($_POST["serviceid"]);
        $this->load->library('artilium', array(
            'companyid' => $this->session->cid
        ));
        $pack = (array) json_decode($service);
        $this->artilium->UnBlockOriginating(trim($_POST['SN']), $pack, '1');
        $this->Admin_model->update_services($_POST["serviceid"], array(
            'status' => 'Active'
        ));
        $this->Admin_model->update_services_data('mobile', $_POST["serviceid"], array(
            'bar' => 0
        ));
        $this->session->set_flashdata('success', 'All Originating Service include International Calls has been barred');
        echo json_encode($pack);
    }
    public function Teum_Topup()
    {
        $this->load->model('Agent_model');
        $agent = $this->Agent_model->getAgent($_SESSION['reseller']['id']);
        if ($agent->reseller_type == "Prepaid") {
            set_time_limit(0);
            do {
                if (!file_exists(APPPATH.'lock/reseller_balance_'.$_SESSION['reseller']['id'])) {
                    log_message('error', 'No locking, we continue adding balance and now we lock the customer balance');
                    file_put_contents(APPPATH.'lock/reseller_'.$_SESSION['reseller']['id'], $_POST['serviceid']);
                    break;
                }
            } while (true);
        }

        $service = $this->Admin_model->getService($_POST['serviceid']);

        if ($agent->reseller_type == "Prepaid") {
            if ($_POST['amount'] > $agent->reseller_balance) {
                $this->session->set_flashdata('error', lang('You do not have enought balance to topup this subscriber'));
                redirect('reseller/dashboard/client');
            }
        }


        if ($service) {
            $this->load->library('pareteum', array(
                'companyid' => $this->companyid,
                'api_id' => $service->api_id
            ));
            $res = $this->pareteum->topup(array(
                'msisdn' => $service->details->msisdn,
                'amount' => $_POST['amount'] * 100
            ));
            $amount =  $_POST['amount'] * 100;
            log_message('error', print_r(array(
                'msisdn' => $service->details->msisdn,
                'amount' => $_POST['amount'] * 100
            ), true));
            log_message('error', print_r($res, true));

            if ($res->resultCode == "0") {
                if ($agent->reseller_type == "Prepaid") {
                    $new_balance = $agent->reseller_balance - $_POST['amount'];
                    $this->db->query("update a_clients_agents set reseller_balance=? where id=?", array($new_balance, $_SESSION['reseller']['id']));
                    unlink(APPPATH.'lock/reseller_balance_'.$_SESSION['reseller']['id'], $_POST['serviceid']);
                }
                $this->Admin_model->insertTopup(array(
                        'companyid' => $this->companyid,
                        'serviceid' => $_POST['serviceid'],
                        'userid' =>   $service->userid,
                        'income_type' => 'topup',
                        'agentid' => $_SESSION['reseller']['id'],
                        'amount' =>  $_POST['amount'],
                        'user' =>  $_SESSION['reseller']['agent']));
                logAdmin(array(
                        'companyid' => $this->companyid,
                        'serviceid' => $_POST['serviceid'],
                        'userid' =>   $service->userid,
                        'user' => $_SESSION['reseller']['agent'],
                        'ip' => $_SERVER['REMOTE_ADDR'],
                        'description' => 'Service : ' .  $_POST['serviceid'] . ' Topup amount : has been loaded '.$this->data['setting']->currency.' '. $_POST['amount'].' to '. $service->details->msisdn
                    ));

                $this->session->set_flashdata('success', 'Topup ' .$this->data['setting']->currency.' '. $_POST['amount'] . ' has been added to the msisdn: +'.$service->details->msisdn);
            //redirect('admin/subscription/detail/'.$_POST['serviceid']);
            } else {
                $this->session->set_flashdata('error', 'there was an error handling your request');
            }
        } else {
            die('Access Denied');
        }
        //print_r($_POST);

        if (!empty($_POST['type'])) {
            if ($_POST['type'] == "json") {
                redirect('reseller');
            }
        }
        redirect('reseller/subscription/detail/' . $_POST['serviceid']);
    }
    public function Teum_addBundle()
    {
        if (!reseller_allow_perm('addbundle', $this->session->reseller['id'])) {
            $this->session->set_flashdata('error', lang('You do not have access to this feature, please contact your Upstream'));
            redirect('reseller');
        }
        $this->load->model('Agent_model');
        $agent = $this->Agent_model->getAgent($_SESSION['reseller']['id']);
        if ($agent->reseller_type == "Prepaid") {
            //we need to set time limit to 0 because we are going to lock balance to avoid the same request at the same time
            set_time_limit(0);
            do {
                if (!file_exists(APPPATH.'lock/reseller_balance_'.$_SESSION['reseller']['id'])) {
                    log_message('error', 'No locking, we continue adding balance and now we lock the customer balance');
                    file_put_contents(APPPATH.'lock/reseller_'.$_SESSION['reseller']['id'], $_POST['serviceid']);
                    break;
                }
            } while (true);
        }
        if ($agent->reseller_type != "Prepaid") {
            if (!isAllowed_reseller_service($this->session->reseller['id'], $_POST['serviceid'])) {
                $this->session->set_flashdata('error', 'You are not allowed to add bundle other than your own customer: Access Denied');
                redirect('reseller');
            }
        }
        $service_teum = $this->Admin_model->getService($_POST['serviceid']);
        log_message('error', print_r($_POST, true));
        if ($service_teum) {
            $reseller_sim_card = $this->db->query("select * from a_reseller_simcard where MSISDN=?", array($service_teum->details->msisdn));
            $this->load->library('pareteum', array(
                'companyid' => $this->companyid,
                'api_id' => $service_teum->api_id
            ));
            $client    = $this->Admin_model->getClient($service_teum->userid);
            $addons    = getAddonInformation($_POST['bundleid']);
            if ($agent->reseller_type == "Prepaid") {
                if ($addons->recurring_total > $agent->reseller_balance) {
                    $this->session->set_flashdata('error', lang('You do not have enought balance to topup this subscriber'));
                    redirect('reseller/dashboard/client');
                }
            }
            $AccountId = $service_teum->details->teum_accountid;
            $subs = array(
                "CustomerId" => (int) $client->teum_CustomerId,
                "SubscriptionId" => $reseller_sim_card->row()->SubscriptionId,
                "Channel" => "UnitedPortal V1",
                "Offerings" => array(
                    array(
                    "ProductOfferingId" => (int) $addons->bundleid,
                    "OrderedProductCharacteristics" => array(
                        array(
                            "Name" => "MSISDN",
                            "Value" => $service_teum->details->msisdn
                        )
                    )
                    )
                    )

            );

            log_message('error', print_r($subs, true));
            $subscription = $this->pareteum->subscriptions_offerings($subs);
            log_message('error', print_r($subscription, true));
            if ($subscription->resultCode == "0") {
                if ($agent->reseller_type == "Prepaid") {
                    $new_balance = $agent->reseller_balance - $addons->recurring_total;
                    $this->db->query("update a_clients_agents set reseller_balance=? where id=?", array($new_balance, $_SESSION['reseller']['id']));
                    unlink(APPPATH.'lock/reseller_balance_'.$_SESSION['reseller']['id'], $_POST['serviceid']);
                }

                if (checkaddon_existance($_POST['bundleid'], $_POST['serviceid'])) {
                    $this->Admin_model->updateAddon(array('teum_autoRenew' => $_POST['month']-1,
                        'teum_NextRenewal' => getFuturedate(date('Y-m-d'), 'day', 31)), checkaddon_existance($_POST['bundleid'], $_POST['serviceid'])->id);
                } else {
                    $offering = array(
                        'name' => $addons->name,
                        'companyid' => $this->companyid,
                        'terms' => $addons->bundle_duration_value,
                        'cycle' => $addons->bundle_duration_type,
                        'serviceid' => $_POST['serviceid'],
                        'addonid' => $_POST['bundleid'],
                        'recurring_total' => $addons->recurring_total,
                        'addon_type' => 'option',
                        'arta_bundleid' => $addons->bundleid,
                        'teum_autoRenew' => $_POST['month']-1,
                        'teum_NextRenewal' => getFuturedate(date('Y-m-d'), 'day', 31),
                        'teum_DateStart' => date('Y-m-d'),
                        'teum_CustomerOrderId' => $subscription->PurchaseOrder->CustomerOrderId,
                        'teum_SubscriptionId' => $reseller_sim_card->row()->SubscriptionId,
                        'teum_ProductId' => null,
                        'teum_ProductChargePurchaseId' => null,
                        'teum_SubscriptionProductAssnId' => null,
                        'teum_ServiceId' => null
                    );

                    $this->Admin_model->insertAddon($offering);
                }
                $this->session->set_flashdata('success', 'bundle has been added');
                $this->Admin_model->insertTopup(
                      array(
                        'companyid' => $this->companyid,
                        'serviceid' => $_POST['serviceid'],
                        'userid' =>  $service_teum->userid,
                        'income_type' => 'bundle',
                        'agentid' => $client->agentid,
                        'amount' => $addons->recurring_total,
                        'user' =>  $_SESSION['reseller']['agent'])
                  );
                logAdmin(array(
                        'companyid' => $this->companyid,
                        'serviceid' => $_POST['serviceid'],
                        'userid' =>  $service_teum->userid,
                        'user' =>  $_SESSION['reseller']['agent'],
                        'ip' => $_SERVER['REMOTE_ADDR'],
                        'description' => 'Service : ' .  $_POST['serviceid'] . ' Bundle: '.$addons->name.' has been added '.$service_teum->details->msisdn
                    ));
            } else {
                $this->session->set_flashdata('error', 'bundle has not been  added');
            }
            if (!empty($_POST['type'])) {
                if ($_POST['type'] == "json") {
                    redirect('reseller');
                }
            }
            redirect('reseller/subscription/detail/' . $_POST['serviceid']);
        }
    }
    public function Teum_Usage()
    {
        if (!reseller_allow_perm('service', $this->session->reseller['id'])) {
            $this->session->set_flashdata('error', lang('You do not have access to this feature, please contact your Upstream'));
            redirect('reseller');
        }
        if (!isAllowed_reseller_service($this->session->reseller['id'], $_POST['serviceid'])) {
            $this->session->set_flashdata('error', 'Access Denied');
            redirect('reseller/dashboard/client');
        }
        $service = $this->Admin_model->getService($_POST['serviceid']);
        if ($service) {
            $this->load->library('pareteum', array(
                'companyid' => $this->companyid,
                'api_id' => $service->api_id
            ));
            $res = $this->pareteum->usage(array(
                'msisdn' => $service->details->msisdn,
                'fromDate' => trim($_POST['from']).'T00:00:00',
                'toDate' => trim($_POST['to']).'T23:59:59'
            ));

            echo json_encode($res);
        } else {
            die('Access Denied');
        }
    }
    public function Teum_Freeze()
    {
        if (!reseller_allow_perm('service', $this->session->reseller['id'])) {
            $this->session->set_flashdata('error', lang('You do not have access to this feature, please contact your Upstream'));
            redirect('reseller');
        }
        if (!isAllowed_reseller_service($this->session->reseller['id'], $_POST['serviceid'])) {
            $this->session->set_flashdata('error', 'Access Denied');
            redirect('reseller/dashboard/client');
        }
        //print_r($_POST);
        $service = $this->Admin_model->getService($_POST['serviceid']);
        if ($service) {
            $this->load->library('pareteum', array(
                'companyid' => $this->companyid,
                'api_id' => $service->api_id
            ));
            $res = $this->pareteum->freeze(array(
                'msisdn' => $service->details->msisdn,
                'SubscriptionId' => $this->Admin_model->getTeumSubscriptionId($service->details->msisdn),
                'SubscriptionFreezeReason' => 'FREEZE_CUSTREQUEST',
                'ExternalReference' => $_POST['ExternalRef'],
                'channel' => 'UnitedPortal V1',
                'comments' => 'Freeze by: ' .  $_SESSION['reseller']['agent']
            ));
            echo json_encode($res);
        } else {
            die('Access Denied');
        }
    }
    public function Teum_Unfreeze()
    {
        if (!reseller_allow_perm('service', $this->session->reseller['id'])) {
            $this->session->set_flashdata('error', lang('You do not have access to this feature, please contact your Upstream'));
            redirect('reseller');
        }
        if (!isAllowed_reseller_service($this->session->reseller['id'], $_POST['serviceid'])) {
            $this->session->set_flashdata('error', 'Access Denied');
            redirect('reseller/dashboard/client');
        }
        $service = $this->Admin_model->getService($_POST['serviceid']);
        if ($service) {
            $this->load->library('pareteum', array(
                'companyid' => $this->companyid,
                'api_id' => $service->api_id
            ));
            $res = $this->pareteum->unfreeze(array(
                'msisdn' => $service->details->msisdn,
                'SubscriptionId' => $this->Admin_model->getTeumSubscriptionId($service->details->msisdn),
                'ExternalReference' => '',
                'channel' => 'UnitedPortal V1',
                'comments' => 'Freeze by: ' . $_SESSION['reseller']['agent']
            ));
            echo json_encode($res);
        } else {
            die('Access Denied');
        }
    }
    public function getBundleList()
    {
        $html     = "";
        $number   = $_POST['msisdn'];
        $type     = $_POST['type'];
        $this->db = $this->load->database('default', true);
        if ($type == "tarif") {
            $q = $this->db->query("select id,name,recurring_total from a_products_mobile_bundles where bundle_type like ? and companyid = ? and status = 1 order by id asc", array(
                $type,
                $this->uri->segment(4)
            ));
        } else {
            if (!empty($_POST['agid'])) {
                $q = $this->db->query("select id,name,recurring_total from a_products_mobile_bundles where bundle_type like ? and companyid = ? and agid=? and status = 1 order by id asc", array(
                    $type,
                    $this->uri->segment(4),
                    $_POST['agid']
                ));
            } else {
                $q = $this->db->query("select id,name,recurring_total from a_products_mobile_bundles where bundle_type like ? and companyid = ? and status = 1 order by id asc", array(
                    $type,
                    $this->uri->segment(4)
                ));
            }
        }

        if ($this->data['setting']->mobile_platform == "TEUM") {
            $addons = $this->Admin_model->getOrderedAddonid($_POST['serviceid']);
            foreach ($q->result() as $bundle) {
                if ($addons) {
                    if (!in_array($bundle->id, $addons)) {
                        if ($bundle->recurring_total > 0) {
                            $html .= '<option value="' . $bundle->id . '">' . str_replace('Option ', '', $bundle->name) . ' -  '.$this->data['setting']->currency . number_format($bundle->recurring_total, 2) . ' /' . lang('month') . ' </option>';
                        } else {
                            $html .= '<option value="' . $bundle->id . '">' . str_replace('Option ', '', $bundle->name) . ' </option>';
                        }
                    }
                } else {
                    if ($bundle->recurring_total > 0) {
                        $html .= '<option value="' . $bundle->id . '">' . str_replace('Option ', '', $bundle->name) . ' -  '.$this->data['setting']->currency. number_format($bundle->recurring_total, 2) . ' /' . lang('month') . ' </option>';
                    } else {
                        $html .= '<option value="' . $bundle->id . '">' . str_replace('Option ', '', $bundle->name) . ' </option>';
                    }
                }
            } //$q->result() as $bundle
        } else {
            foreach ($q->result() as $bundle) {
                if ($bundle->recurring_total > 0) {
                    $html .= '<option value="' . $bundle->id . '">' . str_replace('Option ', '', $bundle->name) . ' -  '.$this->data['setting']->currency . number_format($bundle->recurring_total, 2) . ' /' . lang('month') . ' </option>';
                } else {
                    $html .= '<option value="' . $bundle->id . '">' . str_replace('Option ', '', $bundle->name) . ' </option>';
                }
            } //$q->result() as $bundle
        }
        echo json_encode(array(
            'result' => true,
            'html' => $html,
            'price' => number_format($q->row()->recurring_total, 2)
        ));
    }
    public function Teum_addBundlex()
    {
        /*$service = $this->Admin_model->getService($_POST['serviceid']);
        if($service){

        $this->load->library('pareteum', array('companyid' => $this->companyid));
        $res = $this->pareteum->usage(array('msisdn'=> $service->details->msisdn, 'fromDate' => $_POST['from'], 'toDate' =>  $_POST['to']));

        echo json_encode($res);

        }else{

        die('Access Denied');
        }
        */
    }

    public function Teum_CompletePortin()
    {
        if (!reseller_allow_perm('service', $this->session->reseller['id'])) {
            $this->session->set_flashdata('error', lang('You do not have access to this feature, please contact your Upstream'));
            redirect('reseller');
        }
        $service = $this->Admin_model->getService($_POST['serviceid']);
        $this->load->library('pareteum', array('companyid' => $this->session->cid));
        $subscriptionid = $service->details->teum_subscriptionid;
        log_message('error', 'Swapsim on Portability complete : '.$subscriptionid);
        $subscription = $this->pareteum->swap_portin_msisdn($subscriptionid, array('from' => $service->details->msisdn_sn, 'to' => $service->details->donor_msisdn));
        log_message('error', 'Swapsim on Portability complete'.print_r($subscription, true));
        // $this->db->query('update a_services_mobile set msisdn=donor_msisdn,msisdn_status=? where serviceid=?', array($_POST['serviceid'], 'Active'));
        $this->Admin_model->update_services_data('mobile', $_POST['serviceid'], array('msisdn_status' => 'Active', 'msisdn'=> $service->details->donor_msisdn));
        $this->session->set_flashdata('success', 'Portin has been set to completed');
        redirect('reseller/subscription/detail/'.$_POST['serviceid']);
    }
    public function Teum_Enable_Disable_Autorenew()
    {
        if (!reseller_allow_perm('service', $this->session->reseller['id'])) {
            $this->session->set_flashdata('error', lang('You do not have access to this feature, please contact your Upstream'));
            redirect('reseller');
        }
        $addon  = getAddonsbyID($_POST['addonid']);
        print_r($addon);
        print_r($_POST);
        if ($_POST['typeaction'] == "1") {
            if ($addon->teum_NextRenewal >= date('Y-m-d')) {
                $this->db->query("update a_services_addons set teum_autoRenew =? where id= ? ", array($_POST['ContractDuration'], $_POST['addonid']));
                log_message('error', $this->db->last_query());
            } else {
            }
        } else {
            $this->db->query("update a_services_addons set teum_autoRenew =? where id= ? ", array(0, $_POST['addonid']));
            log_message('error', $this->db->last_query());
        }
        $this->session->set_flashdata('success', lang('Auto renewal has been set'));
        redirect('reseller/subscription/detail/'.$_POST['serviceid']);
    }
}
