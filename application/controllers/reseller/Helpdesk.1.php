<?php
defined('BASEPATH') or exit('No direct script access allowed');
class HelpdeskController extends CI_Controller
{

    protected $data = [];

    public function __construct()
    {
     // Ensure you run parent constructor
        parent::__construct();
        if (!empty($this->session->language)) {
            $this->config->set_item('language', $this->session->language);
        } else {
            $this->config->set_item('language', 'dutch');
        }
        $this->lang->load('admin');
        $this->companyid = get_companyidby_url(base_url());

        $this->data['setting'] = globofix($this->companyid);
    }
}
class Helpdesk extends HelpdeskController
{

    public function __construct()
    {
        parent::__construct();

     /*
     $classes = get_class_methods($this);
     foreach ($classes as $class) {
     if ($class != 'setProperty' && $class != 'get_instance' && $class != '__construct') {
     $this->db->insert('admin_permissions', array('folder' => 'admin', 'controller' => 'helpdesk', 'method' => $class));
     $this->db->insert('admin_roles', array('rolename' => 'superadmin', 'perms' => $this->db->insert_id()));
     }

     }
      */
        $this->load->model('Helpdesk_model');
        $this->db = $this->load->database('default', true);
    }
    public function index()
    {
        $this->data['dtt']   = 'helpdesk.js?version=1.5';
        $this->data['stats'] = $this->Helpdesk_model->getHelpdeskStats();
     //print_r($this->data['stats']);
        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'helpdesk/list';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }
    public function assigntome()
    {
        $this->db->where('id', $this->uri->segment(4));
        $this->db->update('a_helpdesk_tickets', array('assigne' => $this->session->userdata('id')));
        if ($this->db->affected_rows() > 0) {
            $this->session->set_flashdata('success', lang('ticket has been assigned to you'));
        } else {
            $this->session->set_flashdata('error', lang('Ticket as already assigned to you'));
        }
        redirect('admin/helpdesk/detail/' . $this->uri->segment(4));
    }

    public function my_assigned()
    {
        $this->data['status'] = $this->uri->segment(4);
        $this->data['stats'] = $this->Helpdesk_model->getHelpdeskStats();
     //print_r($this->data['stats']);
        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'helpdesk/my_assigned';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }
    public function status()
    {
        $this->data['status'] = $this->uri->segment(4);
        $this->data['stats'] = $this->Helpdesk_model->getHelpdeskStats();
     //print_r($this->data['stats']);
        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'helpdesk/status';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }


    public function department()
    {
    }
    public function category()
    {
    }
    public function priority()
    {
        $this->data['status'] = $this->uri->segment(4);
        $this->data['stats'] = $this->Helpdesk_model->getHelpdeskStats();
     //print_r($this->data['stats']);
        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'helpdesk/priority';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }
    public function reply()
    {
        if (isPost()) {
            $names = array();
            $types = array('image/jpeg', 'image/gif', 'image/png', 'application/pdf', 'application/vnd.ms-excel', 'application/vnd.ms-word', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document');

            $tochange   = array();
            $ticketid   = $this->uri->segment(4);
            $countfiles = count($_FILES['attachments']['name']);

            if ($countfiles > 0) {
                for ($i = 0; $i < $countfiles; $i++) {
                    if (!empty($_FILES['attachments']['name'][$i])) {
                        $file_type = $_FILES['attachments']['type'][$i]; //returns the mimetype
                        $filename  = time() . '_' . $_FILES['attachments']['name'][$i];
                        if (!in_array($file_type, $types)) {
                            $error_message = 'Only .jpg, .gif, .png, .pdf, .doc(x), .xls(x) files are allowed.';

                            $this->session->set_flashdata('error', $error_message);
                            redirect('/admin/helpdesk/detail/' . $ticketid);
                        }
                    }
                }
            }

            if ($_POST['old_deptid'] != $_POST['deptid']) {
                $tochange['deptid'] = $_POST['deptid'];
                logTicketClient($ticketid, 'Ticket has been changed from ' . getDeptName($_POST['old_deptid']) . ' to ' . getDeptName($_POST['deptid']));
            }
            if ($_POST['old_priority'] != $_POST['priority']) {
                $tochange['priority'] = $_POST['priority'];
                logTicketClient($ticketid, 'Ticket Priority has been changed from ' . $_POST['old_priority'] . ' to ' . $_POST['priority']);
            }

            if ($_POST['old_categoryid'] != $_POST['categoryid']) {
                $tochange['categoryid'] = $_POST['categoryid'];
                logTicketClient($ticketid, 'Ticket Priority has been changed from ' . getCategoryName($_POST['old_categoryid']) . ' to ' . getCategoryName($_POST['categoryid']));
            }
            if ($_POST['old_assigne'] != $_POST['assigne']) {
                $tochange['assigne'] = $_POST['assigne'];
                logTicketClient($ticketid, 'Ticket has been assigned from ' . getDeptName($_POST['old_assigne']) . ' to ' . getDeptName($_POST['assigne']));
            }
            $tochange['status'] = $_POST['status'];
            $this->Helpdesk_model->UpdateTicket($ticketid, $tochange);
            $id = $this->Helpdesk_model->InsertReply(array('isadmin' => 1, 'message' => $_POST['message'], 'name' => $this->session->firstname . ' ' . $this->session->lastname, 'ticketid' => $ticketid));
            if (count($_FILES['attachments']['name']) > 0) {
                if (!file_exists($this->data['setting']->DOC_PATH . '/' . $this->companyid . '/attachments/')) {
                    mkdir($this->data['setting']->DOC_PATH . '/' . $this->companyid . '/attachments/', 0755, true);
                }
                //Loop through each file
                for ($i = 0; $i < count($_FILES['attachments']['name']); $i++) {
                    if (!empty($_FILES['attachments']['name'][$i])) {
                         //Get the temp file path
                         $tmpFilePath = $_FILES['attachments']['tmp_name'][$i];

                         //Make sure we have a filepath
                        if ($tmpFilePath != "") {
            //save the filename
                            $shortname = $_FILES['attachments']['name'][$i];
                            $md5       = md5(time()) . $shortname;

            //save the url and the file
                            $filePath = $this->data['setting']->DOC_PATH . '/' . $this->companyid . '/attachments/' . $md5;

            //Upload the file into the temp dir
                            if (move_uploaded_file($tmpFilePath, $filePath)) {
                                    $uploadData[$i]['id']        = $ticketid;
                                    $uploadData[$i]['file_name'] = $md5;
                                    $uploadData[$i]['created']   = date("Y-m-d H:i:s");
                                    $uploadData[$i]['modified']  = date("Y-m-d H:i:s");

                                    $uploadData[$i]['relid'] = $id;

                                    $uploadData[$i]['type_a'] = 'reply';
                                    $names[]                  = $md5;
                                    //insert into db
                                    //use $shortname for the filename
                                    //use $filePath for the relative url to the file
                            }
                        }
                    }
                }
            }
            if (!empty($names)) {
                $filesss = $this->Helpdesk_model->update_reply_attachment($id, implode('|', $names));
                //print_r($filesss);
            }
            $this->session->set_flashdata('success', 'Ticket has been replied');
            redirect('admin/helpdesk/detail/' . $ticketid);
        }
    }

    public function close()
    {
        if (isPost()) {
            $names = array();
            $types = array('image/jpeg', 'image/gif', 'image/png', 'application/pdf', 'application/vnd.ms-excel', 'application/vnd.ms-word', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document');

            $tochange = array();
            $ticketid = $this->uri->segment(4);

            $tochange['status'] = $_POST['status'];
            $this->Helpdesk_model->UpdateTicket($ticketid, $tochange);
            $id = $this->Helpdesk_model->InsertReply(array('isadmin' => 1, 'message' => $_POST['message'], 'name' => $this->session->firstname . ' ' . $this->session->lastname, 'ticketid' => $ticketid));

            $this->session->set_flashdata('success', 'Ticket has been Closed and resolution has been posted');
            redirect('admin/helpdesk/detail/' . $ticketid);
        }
    }

    public function upload_file($file)
    {
        $this->load->library("upload");
        $upload_cfg['upload_path'] = FCPATH . 'documents/attachments/';

        if (!file_exists($upload_cfg['upload_path'])) {
            mkdir(FCPATH . "documents/attachments/", 0755);
        }

        $upload_cfg['encrypt_name']  = true;
        $upload_cfg['allowed_types'] = "gif|jpg|png|jpeg";
   /*        $upload_cfg['max_width'] = '1920'; /* max width of the image file */
   /*        $upload_cfg['max_height'] = '1080'; /* max height of the image file */
   /*        $upload_cfg['min_width'] = '300'; /* min width of the image file */
   /*        $upload_cfg['min_height'] = '300'; /* min height of the image file */

        $this->upload->initialize($upload_cfg);

        if ($this->upload->do_upload($file)) {
            $image = $this->upload->data();
            $this->session->set_flashdata('success_msg', lang('success_msg_edit_cat'));
            return $image;
        } else {
            $msg = $this->form_validation->field_data['file_to_upload']['error'] = $this->upload->display_errors('', '');
            $this->session->set_flashdata('success_msg', $msg);
         //redirect('files/overview/');
        }
    }
    public function is_multi($array)
    {

        return (count($array) != count($array, 1));
    }

    public function getLastTickets()
    {
        $ofset = $this->uri->segment(3);
        $q     = $this->db->query("select a.id,a.date,a.subject,a.message, case when a.userid  is NULL THEN a.name else concat(c.firstname,' ', c.lastname) end as fullname,b.name as deptname from a_helpdesk_tickets a left join a_helpdesk_department b on b.id=a.deptid left join a_clients c on c.id=a.userid where a.status != 'Closed' and a.status != 'Answered' order by a.id desc limit 6");
        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return array();
        }
    }
    public function assigntother()
    {

        $this->db->where('id', $_POST['ticketid']);
        $this->db->update('a_helpdesk_tickets', array('assigne' => $_POST['adminid'], 'deptid' => $_POST['deptid']));
        logTicketClient($_POST['ticketid'], 'Ticket has been assigned to ' . getAdminName($_POST['adminid'] . ' by ' . $_SESSION['firstname'] . ' ' . $_SESSION['lastname']));
        $this->session->set_flashdata('success', 'Ticket has been assigned to ' . getAdminName($_POST['adminid']));
        redirect('admin/helpdesk/detail/' . $_POST['ticketid']);
    }
    public function detail()
    {

        $this->load->helper('date');
        $id                   = $this->uri->segment(4);
        $this->data['ticket'] = $this->Helpdesk_model->getTicket($id);
        $this->load->model('Admin_model');
        $this->data['client'] = $this->Admin_model->getClient($this->data['ticket']['userid']);

     //print_r($this->data);
     //$this->data['replies'] =$this->Helpdesk_model->getHelpdeskStats();
     //$this->data['stats'] = $this->Helpdesk_model->getHelpdeskStats();
     //$this->data['tickets'] = $this->Helpdesk_model->getLastTickets();
        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'helpdesk/detail';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }

    public function newticket()
    {
        if (!file_exists($this->data['setting']->DOC_PATH . '/' . $this->companyid . '/attachments/')) {
            mkdir($this->data['setting']->DOC_PATH . '/' . $this->companyid . '/attachments/', 0755, true);
        }
     // $this->db = $this->load->database('default', true);
        if (isPost()) {
            $ticket           = $_POST;
            $ticket['date']   = date('Y-m-d H:i:s');
            $ticket['status'] = 'Open';
            $ticket['tid']    = random_str('alphacaps', 3) . '-' . random_str('num', 6) . '-' . date('Y');
            $countfiles       = count($_FILES['attachments']['name']);
            $names            = array();
            $types            = array('image/jpeg', 'image/gif', 'image/png', 'application/pdf', 'application/vnd.ms-excel', 'application/vnd.ms-word', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document');

            if ($countfiles > 0) {
                for ($i = 0; $i < $countfiles; $i++) {
                    if (!empty($_FILES['attachments']['name'][$i])) {
                        $file_type = $_FILES['attachments']['type'][$i]; //returns the mimetype
                        $filename  = time() . '_' . $_FILES['attachments']['name'][$i];
                        if (!in_array($file_type, $types)) {
                            $error_message = 'Only .jpg, .gif, .png, .pdf, .doc(x), .xls(x) files are allowed.';

                            $this->session->set_flashdata('error', $error_message);
                            redirect('admin/helpdesk/');
                        }
                        // Upload file
                        if (move_uploaded_file($_FILES['attachments']['tmp_name'][$i], $this->data['setting']->DOC_PATH . '/' . $this->companyid . '/attachments/' . $filename)) {
                            $names[] = $filename;
                        }
                    }
                }

                $ticket['attachments'] = implode('|', $names);
            }
            $id = $this->Helpdesk_model->insert_ticket($ticket);
            if ($id) {
                $this->session->set_flashdata('success', 'Ticket has been created');
            } else {
                $this->session->set_flashdata('error', 'failed to generate ticket');
            }
         //print_r($ticket);
            redirect('admin/helpdesk/detail/' . $id);
        } else {
            $ticket['tid'] = random_str('alphacaps', 3) . '-' . random_str('num', 6) . '-' . date('Y');
            print_r($ticket);
        }
    }
}
