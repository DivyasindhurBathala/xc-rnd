<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class CrmController extends CI_Controller {
	protected $data = [];

	public function __construct() {
		// Ensure you run parent constructor
		parent::__construct();
		$this->config->set_item('language', 'english');
		$this->lang->load('admin');
		$this->setProperty();
	}

	public function setProperty() {
		$this->data['setting'] = globo();
	}

}
class Crm extends CrmController {

	public function __construct() {
		parent::__construct();
		$this->load->model("Admin_model");
		$this->load->model("Api_model");
		$this->load->model('Teams_model');
		$ip = get_client_ip_env();

		if (!$ip) {
			echo json_encode(array('result' => 'error', 'message' => $_SERVER['REMOTE_ADDR'] . ': Access Denied'));
			exit;
		}
		$g = $this->input->request_headers();
		foreach ($g as $key => $header) {
			$headers[strtolower($key)] = $header;

		}
		$headers['uri_string'] = $this->uri->uri_string();
		$headers['ip_addr'] = $_SERVER['REMOTE_ADDR'];
		$this->headers = $headers;

		if (!empty($this->headers['x-api-key'])) {
			if (!$this->Api_model->ValidateKey($this->headers['x-api-key'])) {
				header("HTTP/1.1 401 Unauthorized");
				echo json_encode(array('result' => 'error', 'message' => 'Authentication required', 'code' => '100'));
				exit;
			}
		} else {
			header("HTTP/1.1 401 Unauthorized");
			echo json_encode(array('result' => 'error', 'message' => 'Authentication required', 'code' => '101'));
			exit;
		}

		if (isPost()) {
			$post = file_get_contents("php://input");
			$obj = (array) json_decode($post);
			$this->dpost = $obj;

		} else {
			$this->dpost = array();
		}
		header('Content-Type: application/json');

	}

	function set_response($data) {
		$this->Teams_model->send_msg($this->uri->segment(4), 'REQUEST :<br />' . print_r($this->dpost, true) . "<br /><br /> RESPONSE:<br />" . print_r($data, true) . " " . print_r($this->headers, true));
		return json_encode($data);
	}

	function sendEmail($email, $body) {
		if ($this->data['setting']->smtp_type == 'smtp') {
			$config = Array(
				'protocol' => 'smtp',
				'smtp_host' => $this->data['setting']->smtp_host,
				'smtp_port' => $this->data['setting']->smtp_port,
				'smtp_user' => $this->data['setting']->smtp_user,
				'smtp_pass' => $this->data['setting']->smtp_pass,
				'mailtype' => 'html',
				'charset' => 'utf-8',
				'starttls' => true,
				'wordwrap' => TRUE,
			);
		} else {
			$config['protocol'] = 'sendmail';
			$config['mailpath'] = '/usr/sbin/sendmail';
			$config['mailtype'] = 'html';
			$config['charset'] = 'utf-8';
			$config['wordwrap'] = TRUE;
		}
		$this->email->initialize($config);

		$this->email->set_newline("\r\n");
		$this->email->from($this->data['setting']->smtp_sender, $this->data['setting']->smtp_name);
		$this->email->to($email);
		$this->email->subject(lang("Notification"));
		$this->email->message($body);
		if ($this->email->send()) {
			//return json_encode(array('result' => true));
		} else {
			// return json_encode(array('result' => false, 'error' => $this->email->print_debugger()));
		}
	}

	/**
	 * @api {post} api/v1/AddClient AddClient
	 * @apiName AddClient
	 * @apiGroup Crm
	 * @apiVersion 1.3.2
	 * @apiDescription Adding client to the system.
	 * This is required before AddOrder
	 * @apiParam {string} firstname firstname (Required).
	 * @apiParam {string} lastname lastname (Required).
	 * @apiParam {string} companyname companyname (Optional).
	 * @apiParam {string} phonenumber phonenumber (Required).
	 * @apiParam {string} email email address where relation id to Delta SSO (Required).
	 * @apiParam {string} address1 billing address of the customer including number (Required).
	 * @apiParam {string} postcode zipcode of the billing address
	 * @apiParam {string} city City name (Required)..
	 * @apiParam {string} country ISO-2 ex nl (Required).
	 * @apiParam {string} language dutch,english,french (Required).
	 * @apiParam {string} paymentmethod banktransfer,directdebit (Required).
	 * @apiParam {string} password2 alphanumberic password (Required).
	 * @apiParam {string} relationid alphanumberic ID of delta reference (Required).
	 * @apiParam {string} iban IBAN Number (Optional).
	 * @apiParam {string} bic BIC Code (Optional).
	 * @apiParam {string} vat VAT number (Optional).
	 * @apiParam {bol} vatexempt VAT Exemption value  1/0 (Optional).
	 * @apiHeaderExample {json} Header:
	 *     {
	 *		 "X-API-KEY": "string"
	 *     }
	 * @apiSuccess {string} result success/error.
	 * @apiSuccess {string}  clientid Id Artilium of the client.
	 * @apiSuccessExample Success-Response:
	 * HTTP/1.1 200 OK
	 *   {
	 *    "clientid": "1",
	 *    "result": "success"
	 *   }
	 *
	 */

	public function AddClient() {

		$req = $this->Api_model->checkRequired(array('firstname', 'lastname', 'phonenumber', 'email', 'address1', 'postcode', 'city', 'country', 'language', 'paymentmethod', 'password2', 'relationid'), $this->dpost);

		$this->dpost['action'] = "AddClient";
		if ($req['result'] == "success") {
			if (!empty($this->dpost['vat'])) {
				$this->dpost['customfields'][getCustomfieldsidbyName('VAT')] = $this->dpost['vat'];
			}
			unset($this->dpost['vat']);
			if (!empty($this->dpost['nationalnr'])) {
				$this->dpost['customfields'][getCustomfieldsidbyName('Rijksregisternummer')] = $this->dpost['nationalnr'];
			}
			if (!empty($this->dpost['iban'])) {
				$this->dpost['customfields'][getCustomfieldsidbyName('IBAN Number')] = $this->dpost['iban'];
			}
			if (!empty($this->dpost['bic'])) {
				$this->dpost['customfields'][getCustomfieldsidbyName('BIC')] = $this->dpost['bic'];
			}

			if (!empty($this->dpost['vatexempt'])) {

				if ($this->dpost['vatexempt'] == "1") {
					$this->dpost['customfields'][getCustomfieldsidbyName('VATExempt')] = 'on';
				}

			}
			unset($this->dpost['nationalnr']);
			unset($this->dpost['iban']);
			unset($this->dpost['bic']);
			$payment = $this->dpost['paymentmethod'];
			unset($this->dpost['paymentmethod']);
			$this->dpost['noemail'] = true;
			$result = $this->Admin_model->whmcs_api($this->dpost);
			if ($result->result == "success") {
				$this->db = $this->load->database('default', true);
				$this->db->where('id', $result->clientid);
				$this->db->update('tblclients', array('deltaid' => $this->dpost['relationid'], 'defaultgateway' => $payment));
			}

			echo $this->set_response($result);
		} else {

			echo $this->set_response($req);

		}

	}

	function UpdateClient() {

	}

	/**
	 * @api {post} api/v1/AddOrder AddOrder
	 * @apiName AddOrder
	 * @apiGroup Crm
	 * @apiVersion 1.3.2
	 * @apiDescription Adding Order to an existing client
	 * @apiParam {int} clientid Client unique ID.
	 * @apiParam {int} brand Id of the brand
	 * @apiParam {int} pid Product id.
	 * @apiParam {string} addons comma seperated addon id.
	 * @apiParam {string} handset_price rest price of the handset
	 * @apiParam {string} handset_name Name of the handset.
	 * @apiParam {string} handset_month Rest month handset price needs to be paid
	 * @apiParam {string} startdate comma seperated addon id.
	 * @apiHeaderExample {json} Header:
	 *     {
	 *       "X_API_KEY": "string"
	 *     }
	 * @apiSuccess {string} result success/error.
	 * @apiSuccess {int} orderid Id of the order
	 * @apiSuccess {string} productids Subscription ID.
	 * @apiSuccess {string} addonids Client id.
	 * @apiSuccessExample Success-Response:
	 * HTTP/1.1 200 OK
	 *   {
	 *    "result": "success",
	 *	  "orderid": "1",
	 *	  "productids": "1"
	 *   }
	 *
	 */

	public function AddOrder() {
		$addons = array();
		$update_handset = false;

		$req = $this->Api_model->checkRequired(array('clientid', 'brand', 'pid', 'addons'), $this->dpost);
		if ($req['result'] == "success") {
			if (!empty($this->dpost['addons'])) {
				$addons = explode(',', $this->dpost['addons']);
			}
			$productaddons[] = $this->dpost['pid'];
			unset($this->dpost['pid']);

			if (!empty($this->dpost['clientid'])) {
				$options['action'] = 'AddOrder';
				$options['pid'] = $this->dpost['brand'];
				unset($this->dpost['brand']);
				$options['billingcycle'] = array('monhtly');
				$options['paymentmethod'] = 'banktransfer';
				$options['SimStatus'] = 'OrderWaiting';
				if (!empty($this->dpost['promocode'])) {
					$options['action'] = strtoupper(trim($this->dpost['promocode']));
				}
				$options['addons'] = implode(',', array_merge($productaddons, $addons));
				$options['clientid'] = $this->dpost['clientid'];
				if (!empty($this->dpost['noinvoice'])) {

				}

				$options['noinvoice'] = true;
				$options['noemail'] = true;
				if (!empty($this->dpost['priceoverride'])) {
					$options['priceoverride'] = $this->dpost['priceoverride'];

				}
				/* contract date start */
				if (!empty($this->post['startdate'])) {
					$i[getCustomfieldsidbyNameProduct('ContractDate')] = $this->dpost['startdate'];

				}
				/*  */
				if (!empty($this->dpost['handset_name'])) {
					$addcheck = explode(',', $this->dpost['addons']);
					$req0 = $this->Api_model->checkRequired(array('handset_price'), $this->dpost);

					if (!in_array(18, $addcheck)) {

						echo json_encode(array('result' => 'error', 'message' => 'You are adding handset_price or handset_name but no addons id 18 in the element addons'));
						exit;

					}

					$req2 = $this->Api_model->checkRequired(array('handset_price'), $this->dpost);
					if ($req0['result'] != "success") {

						echo json_encode($req0);
						exit;

					}

					$options['addons'] = $options['addons'] . ',18';
					$update_handset = true;

				}

			}

			if ($this->dpost['porting']) {

				$req1 = $this->Api_model->checkRequired(array('porting_msisdn', 'porting_provider', 'porting_simnumber', 'porting_type', 'porting_customer_type', 'porting_date'), $this->dpost);
				if ($req1['result'] != "success") {

					echo json_encode($req1);
					exit;

				}
				$options['domain'] = array($this->dpost['porting_msisdn']);
				$i[getCustomfieldsidbyNameProduct('Porting')] = 'on';
				$i[getCustomfieldsidbyNameProduct('PortDonorOperator')] = $this->dpost['porting_provider'];
				$i[getCustomfieldsidbyNameProduct('PortDonorSim')] = $this->dpost['porting_simnumber'];
				$i[getCustomfieldsidbyNameProduct('PortRequestType')] = $this->dpost['porting_type'];
				$i[getCustomfieldsidbyNameProduct('PortMSISDN')] = $this->dpost['porting_msisdn'];
				$i[getCustomfieldsidbyNameProduct('PortInWishDate')] = $this->dpost['porting_date'];
				$i[getCustomfieldsidbyNameProduct('PortCustomerType')] = $this->dpost['porting_customer_type'];
				if (!validateDate($this->dpost['porting_date'])) {

					echo json_encode(array('result' => 'error', 'message' => 'porting_date format is invaid YYYY-MM-DD'));
					exit;
				}
				if ($this->dpost['porting_customer_type'] == 1) {
					$req2 = $this->Api_model->checkRequired(array('porting_accountnumber'), $this->dpost);
					if ($req2['result'] != "success") {

						echo json_encode($req2);
						exit;

					}
					$i[getCustomfieldsidbyNameProduct('PortAccountNumber')] = $this->dpost['porting_accountnumber'];
				}

				$options['customfields'] = base64_encode(serialize($i));
			}
			if (!empty($this->dpost['delta_sim'])) {
				$i[getCustomfieldsidbyNameProduct('SIMNbr')] = $this->dpost['delta_sim'];
			}
			$result = $this->Admin_model->whmcs_api($options);

			if ($result->result == "success") {
				if ($update_handset) {
					$this->Api_model->UpdateHandsetName(array('name' => $this->dpost['handset_name'], 'recurring' => $this->dpost['handset_price'], 'addonid' => $result->addonids));
				}
				if ($this->dpost['porting']) {

					$this->Api_model->UpdateDomain($result->productids, $this->dpost['porting_msisdn']);
				}

			}
			echo $this->set_response($result);

		} else {
			$this->Teams_model->send_msg($this->uri->segment(4), print_r($req, true));
			echo $this->set_response($req);
		}

	}
/**
 * @api {post} api/v1/GetClient GetClient
 * @apiName GetClient
 * @apiGroup Crm
 * @apiVersion 1.3.2
 * @apiDescription Get client information
 * @apiParam {clientid} firstname firstname (Required).
 * @apiHeaderExample {json} Header:
 *     {
 *		 "X-API-KEY": "string"
 *     }
 * @apiSuccess {string} result success/error.
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 *   {
 *    "id": "1",
 *    "result": "string"
 *    "firstname": "string"
 *    "lastname": "string"
 *    "companyname": "string"
 *    "address1": "string"
 *    "postcode": "string"
 *    "city": "string"
 *    "country": "string"
 *    "deltaid": "string"
 *    "email": "string"
 *    "password": "hashpassword"
 *   }
 *
 */

	public function GetClient() {
		$req = $this->Api_model->checkRequired(array('clientid'), $this->dpost);
		if ($req['result'] == "success") {

			$result = $this->Admin_model->whmcs_api(array('action' => 'GetClientsDetails', 'clientid' => $this->dpost['clientid']));
			echo json_encode($result);
		} else {
			echo json_encode($req);
		}

	}

	public function GetService() {
		$req = $this->Api_model->checkRequired(array('serviceid'), $this->dpost);
		if ($req['result'] == "success") {

			$result = $this->Admin_model->GetService($this->dpost['serviceid']);
			echo json_encode(array('result' => 'success', 'domain' => $result->domain, 'regdate' => $result->regdate, 'brand' => $result->name, 'products' => $this->Admin_model->GetAddons($this->dpost['serviceid'])));
		} else {
			echo json_encode($req);
		}

	}
	public function GetClientInvoices() {
		$req = $this->Api_model->checkRequired(array('clientid'), $this->dpost);
		if ($req['result'] == "success") {
			$result = $this->Api_model->getClientInvoice($this->dpost['clientid']);
			echo json_encode($result);
		} else {
			echo json_encode($req);
		}
	}

	public function GetClientServices() {
		$req = $this->Api_model->checkRequired(array('clientid'), $this->dpost);
		if ($req['result'] == "success") {

		} else {
			echo json_encode($req);
		}
	}

	public function GetInvoice() {
		$req = $this->Api_model->checkRequired(array('invoiceid'), $this->dpost);
		if ($req['result'] == "success") {

		} else {
			echo json_encode($req);
		}
	}

	public function ModifyClient() {
		$req = $this->Api_model->checkRequired(array('clientid'), $this->dpost);
		if ($req['result'] == "success") {

		} else {
			echo json_encode($req);
		}
	}

	public function ModuleSuspend() {
		$req = $this->Api_model->checkRequired(array('serviceid'), $this->dpost);
		if ($req['result'] == "success") {

		} else {
			echo json_encode($req);
		}
	}

	public function ModuleUnsuspend() {
		$req = $this->Api_model->checkRequired(array('serviceid'), $this->dpost);
		if ($req['result'] == "success") {

		} else {
			echo json_encode($req);
		}
	}

	public function ModuleTerminate() {
		$req = $this->Api_model->checkRequired(array('serviceid'), $this->dpost);
		if ($req['result'] == "success") {

		} else {
			echo json_encode($req);
		}
	}

	public function AcceptOrder() {
		$req = $this->Api_model->checkRequired(array('orderid'), $this->dpost);
		if ($req['result'] == "success") {

		} else {
			echo json_encode($req);
		}
	}

	public function AcceptPortOut() {
		$req = $this->Api_model->checkRequired(array('msisdn'), $this->dpost);
		if ($req['result'] == "success") {

		} else {
			echo json_encode($req);
		}
	}
	public function RejectPortOut() {
		$req = $this->Api_model->checkRequired(array('msisdn'), $this->dpost);
		if ($req['result'] == "success") {

		} else {
			echo json_encode($req);
		}
	}

	public function CancelOrder() {
		$req = $this->Api_model->checkRequired(array('orderid'), $this->dpost);
		if ($req['result'] == "success") {

			$result = $this->Admin_model->whmcs_api($this->dpost);
			echo json_encode($result);

		} else {
			echo json_encode($req);
		}
	}
/**
 * @api {get} api/v1/GetProducts GetProducts
 * @apiName GetProducts
 * @apiGroup Crm
 * @apiVersion 1.3.2
 * @apiDescription Query Products that are configured for selling
 * @apiHeaderExample {json} Header:
 *     {
 *       "X_API_KEY": "string"
 *     }
 * @apiSuccess {string} result success/error.
 * @apiSuccess {int} orderid Id of the order
 * @apiSuccess {string} productids Subscription ID.
 * @apiSuccess {string} addonids Client id.
 * @apiSuccessExample Success-Response:
 *  HTTP/1.1 200 OK
 *  {
 * "result": "success",
 * "products": [
 * {
 * "id": "6",
 * "name": "Delta Mobiel 300Mb"
 *	}
 *	]
 *  }
 *
 */

	public function getProducts() {

		$result = $this->Api_model->getProducts();
		echo $this->set_response($result);

	}
/**
 * @api {get} api/v1/getAddons getAddons
 * @apiName getAddons
 * @apiGroup Crm
 * @apiVersion 1.3.2
 * @apiDescription get addons that are set to be sell
 * @apiHeaderExample {json} Header:
 *     {
 *       "X_API_KEY": "string"
 *     }
 * @apiSuccess {string} result success/error.
 * @apiSuccess {addons} array of the addons/options
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *  "result": "success",
 *  "addons": [
 *  {
 *  "id": "14",
 *  "name": "Option 250 MB 30 dagen"
 *  }
 *            ],
 *  }
 *
 */

	public function getAddons() {

		$result = $this->Api_model->getAddons();
		echo $this->set_response($result);

	}
}