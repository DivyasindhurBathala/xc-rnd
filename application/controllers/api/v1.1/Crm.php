<?php
defined('BASEPATH') or exit('No direct script access allowed');
class CrmController extends CI_Controller
{
    protected $data = [];

    public function __construct()
    {
        // Ensure you run parent constructor
        parent::__construct();
        $this->config->set_item('language', 'dutch');
        $this->lang->load('admin');

        header('Content-Type: application/json');
    }
}
class Crm extends CrmController
{
    public function __construct()
    {
        error_reporting(0);
        parent::__construct();
        $this->load->model("Admin_model");
        $this->load->model("Api_model");
        $this->load->model('Teams_model', 'teams');

        $g = $this->input->request_headers();
        foreach ($g as $key => $header) {
            $headers[strtolower($key)] = $header;
        }
        $url = $this->Api_model->getURL(base_url());

        if ($url) {
            $this->cid = $url->companyid;
        } else {
            echo $this->set_response(array(
                'result' => 'error',
                'message' => 'Authentication required --access--, Unknown Access Point',
                'code' => '100',
            ));
            exit;
        }

        //mail('mail@simson.one','print '.$this->cid, base_url());

        $headers['uri_string'] = $this->uri->uri_string();
        $headers['ip_addr'] = $_SERVER['REMOTE_ADDR'];
        $this->headers = $headers;

        if (isPost()) {
            $post = file_get_contents("php://input");
            $obj = (array) json_decode($post);
            $this->dpost = $obj;
        } else {
            $this->dpost = array();
        }
        //$this->teams->send_msg($this->ip . ' :' . $this->uri->segment(4), "<br /><br /> REQUEST:<br />" . print_r($this->dpost, true));
        if (file_exists("/tmp/" . $this->headers['x-api-key'])) {
            sleep(10);
        }
        if (!empty($this->headers['x-api-key'])) {
            $myfile = fopen("/tmp/" . $this->headers['x-api-key'], "w");
            fclose($myfile);
            $validation = $this->Api_model->ValidateKey($this->headers['x-api-key'], $this->cid);
            // mail('mail@simson.one','auth', 'companyid: '.$this->session->cid);
            if (!$validation) {
                header("HTTP/1.1 401 Unauthorized");
                echo $this->set_response(array(
                    'result' => 'error',
                    'message' => 'Incorrect Token'
                ));
                exit;
            }
            $this->companyid = $validation->companyid;
        } else {
            header("HTTP/1.1 401 Unauthorized");
            echo $this->set_response(array(
                'result' => 'error',
                'message' => 'Token is Empty'
            ));
            exit;
        }
        $this->data['setting'] = globofix($this->companyid);
        $this->ip = get_client_ip_env($_SERVER['REMOTE_ADDR'], $this->companyid);

        slack('Request: '.$this->cid.': '.json_encode($this->dpost), 'mvno');
    }

    public function set_response($data)
    {
        $this->dpost['request_uri'] = $this->uri->segment(4);
        $this->dpost['request_ver'] = $this->uri->segment(2);
        //$this->dpost['ip_address'] = $_SERVER['REMOTE_ADDR'];
        $this->teams->send_msg(current_url(), 'Result: '.json_encode($data).' Headers:'.json_encode($this->headers));
        //$this->dpost['companyid'] = $this->companyid;
        $this->teams->insertLog($this->dpost, $data, $_SERVER['REMOTE_ADDR']);
        unlink("/tmp/" . $this->headers['x-api-key']);
        //slack('Response: '.json_encode($data).' Headers: '.json_encode($this->headers), 'mvno');
        return json_encode($data);
    }

    public function sendEmail($email, $body)
    {
        if ($this->data['setting']->smtp_type == 'smtp') {
            $config = array(
                'protocol' => 'smtp',
                'smtp_host' => $this->data['setting']->smtp_host,
                'smtp_port' => $this->data['setting']->smtp_port,
                'smtp_user' => $this->data['setting']->smtp_user,
                'smtp_pass' => $this->data['setting']->smtp_pass,
                'mailtype' => 'html',
                'charset' => 'utf-8',
                'starttls' => true,
                'wordwrap' => true,
            );
        } else {
            $config['protocol'] = 'sendmail';
            $config['mailpath'] = '/usr/sbin/sendmail';
            $config['mailtype'] = 'html';
            $config['charset'] = 'utf-8';
            $config['wordwrap'] = true;
        }
        $this->email->initialize($config);

        $this->email->set_newline("\r\n");
        $this->email->from($this->data['setting']->smtp_sender, $this->data['setting']->smtp_name);
        $this->email->to($email);
        $this->email->subject(lang("Notification"));
        $this->email->message($body);
        if ($this->email->send()) {
            //return json_encode(array('result' => true));
        } else {
            // return json_encode(array('result' => false, 'error' => $this->email->print_debugger()));
        }
    }

    /**
     * @api {post} api/dev/crm/AddClient AddClient
     * @apiName AddClient
     * @apiGroup Crm
     * @apiVersion 1.3.3
     * @apiDescription Inserting customer to Artilium database.
     * This is required before AddOrder where you will received your clientid
     * @apiParam {string} firstname firstname (Required).
     * @apiParam {string} lastname lastname (Required).
     * @apiParam {string} companyname companyname (Optional).
     * @apiParam {string} phonenumber phonenumber (Required).
     * @apiParam {string} email email address where relation id to Delta SSO (Required).
     * @apiParam {string} address1 billing street (Required).
     * @apiParam {string} housenumber billing house number (Required).
     * @apiParam {string} alphabet billing alphabet (Optional).
     * @apiParam {string} postcode zipcode of the billing address NNNN XX
     * @apiParam {string} city City name (Required)..
     * @apiParam {string} country ISO-2 ex nl (Required).
     * @apiParam {string} language dutch,english,french (Required).
     * @apiParam {string} paymentmethod banktransfer,directdebit (Required).
     * @apiParam {string} password2 alphanumberic password (Required).
     * @apiParam {string} relationid alphanumberic ID of delta reference (Required).
     * @apiParam {string} agentid id of agent can ge found on Agent/reseller or getAgents functions.
     * @apiParam {string} iban IBAN Number (Required).
     * @apiParam {string} bic BIC Code (Optional if iBAN entered correctly BIc will be generated).
     * @apiParam {string} vat VAT number (Optional).
     * @apiParam {string} kvk kvk number (Optional).
     * @apiParam {string} date_birth Date Of Birth YYYY-MM-DD (Optional).
     * @apiParam {string} gender gender: male/female (Optional).
     * @apiParam {string} refferal refferal information ex: google.com (Optional).
     * @apiParam {bool} vatexempt VAT Exemption value  1/0 (Optional).
     * @apiParam {bool} invoice_email Invoice Email if customer wish to received email via email value: yes/no, standard value: yes (Optional)
     * @apiParam {payment_duedays} payment_duedays Invoice due days, standard value: 14 if not added (Optional).
     * @apiParam {string} initial Customer Name intital (Optional).
     * @apiParam {string} salutation salutation of the customer ex: Mrs., Mr., etc  (Optional).
     * @apiParam {string} delta_username Specify if you wish sso  to be activated and username will be link to this customer  (Optional).
     * @apiHeaderExample {json} Header:
     *     {
     *         "X-API-KEY": "string"
     *     }
     * @apiSuccess {string} result success/error.
     * @apiSuccess {string}  clientid Id Artilium of the client.
     * @apiSuccessExample Success-Response:
     * HTTP/1.1 200 OK
     *   {
     *    "clientid": "1",
     *    "result": "success"
     *   }
     *
     */

    public function AddClient()
    {
        $this->teams->send_msg(current_url(), 'Result :'.json_encode($this->dpost). 'Headers :'.json_encode($this->headers));

        $allowed_payments = array(
            'directdebit',
            'banktransfer',
            'creditcard',
            'online',
            'paypal',
            'mollie',
            'sisow',
            'others'
        );
        $this->load->helper('string');
        $this->load->model('Admin_model');


        if ($this->data['setting']->mobile_platform == "TEUM") {
            $required = array(
                'firstname',
                'lastname',
                'phonenumber',
                'email',
                'address1',
                'housenumber',
                'postcode',
                'city',
                'country',
                'language',
                'id_type',
                'idcard_number',
                'agentid'
            );

            if(!isAllowed_reseller_companyid($this->dpost['agentid'], $this->companyid)){

                echo $this->set_response(array('result' => 'error','message' => 'Agentid provided does not exists check the list on getAgents'));
                exit;
            }

        } else {
            $required = array(
                'firstname',
                'lastname',
                'phonenumber',
                'email',
                'address1',
                'postcode',
                'city',
                'country',
                'language',
                'paymentmethod',
                'iban'
            );
        }
        $req = $this->Api_model->checkRequired($required, $this->dpost);

        $err = array();
        $this->dpost['iban'] = str_replace(' ', '', trim($this->dpost['iban']));

        if (!empty($this->dpost['iban'])) {
            if (iban_block($this->companyid, trim($_POST['iban']))) {
                echo $this->set_response(array(
                    'result' => 'error',
                    'message' => 'You may not use this IBAN',
                ));
                exit;
            }
        }
        if (!empty($this->dpost['salutation'])) {
            if ($this->dpost['salutation'] == "meneer") {
                $this->dpost['salutation'] = "Dhr.";
            } elseif ($this->dpost['salutation'] == "mevrouw") {
                $this->dpost['salutation'] = "Mevr.";
            }
        }
        if ($this->dpost['paymentmethod'] == "directdebit") {
            if (!empty($this->dpost['iban'])) {
                $bic = $this->Api_model->getBic(trim($this->dpost['iban']));
                if (!$bic->valid) {
                    $err[] = "Your IBAN: " . $this->dpost['iban'] . " does not pass Validation";
                } else {
                    $this->dpost['bic'] = $bic->bankData->bic;
                }
            } else {
                $err[] = "IBAN is required for SEPA directdebit ";
            }
        } else {
            if (!empty($this->dpost['iban'])) {
                $bic = $this->Api_model->getBic(trim($this->dpost['iban']));
                if (!$bic->valid) {
                    $err[] = "Your IBAN: " . $this->dpost['iban'] . " does not pass Validation";
                } else {
                    $this->dpost['bic'] = $bic->bankData->bic;
                }
            }
        }

        if ($err) {
            echo $this->set_response(array(
                'result' => 'error',
                'message' => implode(', ', $err),
            ));
            exit;
        }

        if ($req['result'] == "success") {
            if (empty($this->dpost['password2'])) {
                $password = random_string('alnum', 10);
                $this->dpost['password'] = password_hash(random_string('alnum', 10), PASSWORD_DEFAULT);
            } else {
                $this->dpost['password'] = password_hash(strtolower(trim($this->dpost['password2'])), PASSWORD_DEFAULT);
                $password = $this->dpost['password2'];
            }
            unset($this->dpost['password2']);
            if (!empty($this->dpost['vatexempt'])) {
                if ($this->dpost['vatexempt'] == "1") {
                    $this->dpost['vat_exempt'] == "1";
                } else {
                    $this->dpost['vat_exempt'] == "0";
                }
            } else {
                $this->dpost['vat_exempt'] == "0";
            }

            if (!empty($this->dpost['delta_username'])) {
                $deltausername = $this->dpost['delta_username'];
            } else {
                $deltausername = false;
            }

            unset($this->dpost['delta_username']);

            /* start new way */
            if (!empty($this->dpost['relationid'])) {
                $this->dpost['mvno_id'] = strtoupper(trim($this->dpost['relationid']));
            }


            if ($this->data['setting']->mvno_id_increment == 1) {
                $this->dpost['mvno_id'] = genMvnoId($this->session->cid);
            }
            unset($this->dpost['relationid']);
            if ($_POST['country'] == "NL") {
                $pcode = explode(' ', trim($_POST["postcode"]));
                if (count($pcode) != 2) {
                    echo $this->set_response(array(
                        'result' => 'error',
                        'message' => lang('Postcode moet: NNNN XX formaat (100)'),
                    ));
                    exit;
                } else {
                    if (strlen($pcode[0]) != 4) {
                        echo $this->set_response(array(
                            'result' => 'error',
                            'message' => lang('Postcode moet: NNNN XX formaat (100)'),
                        ));
                        exit;
                    } elseif (strlen($pcode[1]) != 2) {
                        echo $this->set_response(array(
                            'result' => 'error',
                            'message' => lang('Postcode moet: NNNN XX formaat (100)'),
                        ));
                        exit;
                    }
                }
            } elseif ($_POST['country'] == "BE") {
                $pcode = explode(' ', 'BE ', trim($this->dpost["postcode"]));
            } else {
                $pcode = trim($this->dpost["postcode"]);
            }

            if (!empty($_POST['iban'])) {
                $bic = $this->Api_model->getBic(trim($this->dpost['iban']));
                if (!$bic->valid) {
                } else {
                    $this->dpost['bic'] = $bic->bankData->bic;
                }
            }

            $this->dpost['date_created'] = date('Y-m-d');
            $this->dpost['email'] = strtolower(trim($this->dpost['email']));
            $this->dpost['companyid'] = $this->companyid;
            if ($this->data['setting']->mage_invoicing == 1) {
                $this->load->library('magebo', array(
                    'companyid' => $this->companyid,
                ));
                $magebo = $this->magebo->AddClient($this->dpost);
                if ($magebo->result == "success") {
                    $this->dpost['mageboid'] = $magebo->iAddressNbr;
                    $this->magebo->addMageboSEPA($this->dpost['iban'], $this->dpost['bic'], $magebo->iAddressNbr);
                } else {
                    echo $this->set_response($magebo);
                    exit;
                }
            }
            $this->dpost['date_modified'] = date('Y-m-d H:i:s');
            $gg = explode(' ', trim($this->dpost['address1']));
            if (count($gg) == 2) {
                $this->dpost['address1'] = trim($gg[0]);
                $this->dpost['housenumber'] = trim($gg[1]);
            }



            $i = $this->Admin_model->insertCustomer($this->dpost);

            $result = (object) $i;
            if ($result->result) {
                if ($this->data['setting']->mobile_platform == "TEUM") {
                    $this->load->library('pareteum', array('companyid' => $this->companyid));
                    $array = array(
                    "CustomerData" => array(
                        "ExternalCustomerId" => (string) $this->dpost['mvno_id'],
                        "FirstName" => $this->dpost['firstname'],
                        "LastName" => $this->dpost['lastname'],
                        "LastName2" => 'NA',
                        "CustomerDocumentType" => $this->dpost['id_type'],
                        "DocumentNumber" => $this->dpost['idcard_number'],
                        "Telephone" => $this->dpost['phonenumber'],
                        "Email" => $this->dpost['email'],
                        "LanguageName" => "eng",
                        "FiscalAddress" => array(
                            "Address" => $this->dpost['address1'],
                            "City" =>$this->dpost['city'],
                            "CountryId" => "76",
                            "HouseNo" => $this->dpost['housenumber'],
                            "State" => "Unknown",
                            "ZipCode" => $this->dpost['postcode'],
                        ),
                        "CustomerAddress" => array(
                            "Address" => $this->dpost['address1'],
                            "City" => $this->dpost['city'],
                            "CountryId" => "76",
                            "HouseNo" => $this->dpost['housenumber'],
                            "State" => "Unknown",
                            "ZipCode" => $this->dpost['postcode']
                        ),
                        "Nationality" => "GB"
                    ),
                    "comments" => "Customer added via UnitedPortal V1 by Portal API Token :".$this->headers['x-api-key']
                    );
                    log_message('error', json_encode($array));
                    $teum = $this->pareteum->AddCustomers($array);
                    log_message('error', json_encode($teum));
                    if ($teum->resultCode == "0") {
                        $this->db->query("update a_clients set teum_CustomerId=?, teum_OrderCode=? where id=?", array($teum->customerId, $teum->orderCode, $result->id));
                    }else{
                        $this->db->query("delete from a_clients where id=?", array($i['id']));

                        echo $this->set_response(array("result" => "error","message" => $teum->messages));
                        exit;

                    }
                }





                if (empty($this->dpost['housenumber'])) {
                    $headers = "From: noreply@united-telecom.be";
                    $msg = "Hello\n\nThis is a warning to inform you that you have added customer via webservices without providing housenumber.\n\nPlease login to United Portal and edit this customer to avoid Billing address Issue\n\nPlease edit this customer by visiting this url on the spot: ".base_url()."admin/client/edit/".$result->id."\n\nThis is an automate reminder\n\n";
                    $msg .=print_r($this->dpost, true);

                    $notification = explode('|', $this->data['setting']->email_notification);
                    if (count($notification) >= 1) {
                        foreach ($notification as $em) {
                            mail($em, 'AddClient without Housenumber Warning via API: '.$result->id, $msg, $headers);
                        }
                    }
                }

                $client = $this->Admin_model->getClient($result->id);
                $password = $this->Admin_model->changePasswordClient($client->id);
                if (!empty($deltausername)) {
                    if (validate_deltausername(strtolower($deltausername))) {
                        $this->Admin_model->InsertDeltaUsername($deltausername, $result->id);
                    }
                }

                if ($this->data['setting']->smtp_type == 'smtp') {
                    $config = array(
                        'protocol' => 'smtp',
                        'smtp_host' => $this->data['setting']->smtp_host,
                        'smtp_port' => $this->data['setting']->smtp_port,
                        'smtp_user' => $this->data['setting']->smtp_user,
                        'smtp_pass' => $this->data['setting']->smtp_pass,
                        'mailtype' => 'html',
                        'charset' => 'utf-8',
                        'starttls' => true,
                        'wordwrap' => true,
                    );
                } else {
                    $config['protocol'] = 'sendmail';
                    $config['mailpath'] = '/usr/sbin/sendmail';
                    $config['mailtype'] = 'html';
                    $config['charset'] = 'utf-8';
                    $config['wordwrap'] = true;
                }
                $this->email->clear(true);
                $this->email->initialize($config);
                $this->data['language'] = "dutch";
                $this->data['info'] = (object) array(
                    'email' => $client->email,
                    'password' => $password,
                    'name' => $client->firstname . ' ' . $client->lastname,
                );
                //$body = $this->load->view('email/resetpassword.php', $this->data, true);
                //$this->data['main_content'] = 'email/' . $this->data['language'] . '/resetpassword.php';
                //$body = $this->load->view('email/content', $this->data, true);

                $body = getMailContent('signup_email', $client->language, $client->companyid);
                // $body = $this->load->view('email/content', $this->data, true);
                $body = str_replace('{$name}', $client->firstname . ' ' . $client->lastname, $body);
                $body = str_replace('{$password}', $password, $body);
                $body = str_replace('{$email}', trim(strtolower($client->email)), $body);
                $body = str_replace('{$base_url}', url_to_domain(base_url()), $body);
                $subject = getSubject('signup_email', $client->language, $client->companyid);
                $this->email->set_newline("\r\n");
                $this->email->from($this->data['setting']->smtp_sender, $this->data['setting']->smtp_name);
                $this->email->to($client->email);
                $this->email->subject($subject);
                //$this->email->subject(lang("Your New Password"));
                $this->email->message($body);
                if ($this->email->send()) {
                    logEmailOut(array(
                        'userid' => $client->id,
                        'companyid' => $client->companyid,
                        'to' => $client->email,
                        'subject' => $subject,
                        'message' => $body,
                    ));
                }
            }
            if ($result->result) {
                log_message("error", 'Adding customer Id: ' . $result->id . ' and MageboId:' . $magebo->iAddressNbr);
                logAdmin(array(
                    'companyid' => $this->companyid,
                    'userid' => $result->id,
                    'user' => 'Api User',
                    'ip' => $this->ip,
                    'description' => 'Adding customer Id: ' . $result->id . ' and MageboId:' . $magebo->iAddressNbr,
                ));
                send_growl(array(
                'message' => 'New customer has just been registered via webservices with BillingID: '.$magebo->iAddressNbr,
                'companyid' => $client->companyid));

                echo $this->set_response(array(
                    'result' => 'success',
                    'clientid' => $result->id,
                ));
            } else {
                echo $this->set_response(array(
                    'result' => 'error',
                    'message' => $result->message,
                ));
            }
        } else {
            echo $this->set_response($req);
        }
    }

    public function UpdateClient()
    {
        $required = array(
            'clientid'
        );
        $req = $this->Api_model->checkRequired($required, $this->dpost);
        if(!empty($this->dpost['password2'])){

            $password = password_hash(trim($this->dpost['password2']), PASSWORD_DEFAULT);
            $this->dpost['password'] = $password;
            unset($this->dpost['password2']);

        }
        $d = $this->Admin_model->getClient($this->dpost['clientid']);
        if($req['result'] != "success"){
            header("HTTP/1.1 400 Bad Request");
            echo $this->set_response(array('result' => 'error', 'message' => 'Bad request, clientid is required'));
            exit;


        }
        if($d->companyid == $this->companyid){
            if ($this->data['setting']->mage_invoicing == "1") {
                $this->load->library('magebo', array('companyid' => $this->companyid));
                $_POST['date_modified'] = date('Y-m-d H:i:s');
                $result = $this->magebo->updateClient($d->mageboid, $_POST, $d->companyid);
            }
            $id  = $this->dpost['clientid'];
            unset($this->dpost['clientid']);
            $result  = $this->Admin_model->UpdateClient($id, $this->dpost);
        }else{
            header("HTTP/1.1 404 Not Found");
            echo $this->set_response(array('result' => 'error', 'message' => 'Clientid Not found'));
            exit;

        }


        echo $this->set_response(array('result' => 'success'));

    }

    /**
     * @api {post} api/dev/crm/AddOrder AddOrder
     * @apiName AddOrder
     * @apiGroup Crm
     * @apiVersion 1.3.3
     * @apiDescription Adding Order to an existing client (Required)
     * @apiParam {int} clientid Client unique ID. (Required)
     * @apiParam {int} brand Id of the brand (Required)
     * @apiParam {int} pid Product id (Required).
     * @apiParam {bool} porting Number situation value(1/0) 1= porting 0=New number (Required)
     * @apiParam {string} addons comma seperated addon id.

     * @apiParam {string} handset_price rest price of the handset  (Optional).
     * @apiParam {string} handset_name Name of the handset.  (Optional).
     * @apiParam {string} handset_terms Rest month handset price needs to be paid  (Optional).
     * @apiParam {string} startdate MM-DD-YYYY format for start date contract,  (Optional).
     * @apiParam {string} priceoverride (Optional)
     * @apiParam {int} contractduration in months 12 = 1 year,  24 = 2 year (Optional).
     * @apiParam {string} promocode Promocode of the Promotion  (Optional).
     * @apiParam {string} porting_msisdn MSISDN that need to be ported, format should be country code+number without leading 0 (Required if porting = 1)
     * @apiParam {string} porting_provider (Required if porting = 1)
     * @apiParam {string} porting_simnumber (Required if porting = 1)
     * @apiParam {string} porting_type (Required if porting = 1)
     * @apiParam {string} porting_customer_type (Required if porting = 1)
     * @apiParam {string} porting_date YYYY-MM-DD and not in the weekend (Required if porting = 1)
     * @apiParam {string} porting_accountnumber (Required if porting_customer_type = 1)
     * @apiParam {string} delta_sim  Sim Id for assigning deltaSimcard 19 Digit it does check if simcard is free (Optional), otherwise insert 1111111111111111111, if simcard is valid the order will be automaticly executed to platform
     * @apiParam {bool} setupfee (Optional, value= 1/0)
     * @apiParam {string} setupfee_description (Description on Invoice Line, required when setupfee is 1)
     * @apiParam {string} setupfee_amount (Amount of setup fee ex: 10.00 2 decimal behind dot)
     * @apiHeaderExample {json} Header:
     *     {
     *       "X_API_KEY": "string"
     *     }
     * @apiSuccess {string} result success/error.
     * @apiSuccess {int} serviceid Id of the serviceid
     * @apiSuccess {int} orderid Id of the order
     * @apiSuccess {string} productids Subscription ID.
     * @apiSuccess {string} addonids Client id.
     * @apiSuccessExample Success-Response:
     * HTTP/1.1 200 OK
     *   {
     *    "result": "success",
     *      "serviceid": "1",
     *      "orderid": "1",
     *      "productids": "1"
     *   }
     *
     */

    public function AddOrder()
    {
        set_time_limit(0);
        $auto_provision = false;
        $send_mailfee = false;

        $result = array(
            'result' => 'error',
        );
        $activate = false;
        log_message('error', print_r($this->dpost, 1));
        if (getPlatformbyPid($this->dpost['pid']) != "TEUM") {
            $this->load->library('artilium', array(
                'companyaid' => $this->companyid,
            ));
           $rr = array(
                'clientid',
                'brand',
                'pid',
                'addons',
                'contractduration',
                'porting',
                'startdate',
           );
           $req = $this->Api_model->checkRequired($rr, $this->dpost);



        }else{
            $req = $this->Api_model->checkRequired(array(
                'clientid',
                'brand',
                'pid',
                'porting',
                'activate'
            ), $this->dpost);


            $this->dpost['contractduration'] = 1;
            $this->dpost['startdate'] = date('m-d-Y');
            if($this->dpost['activate']){
                $req2 = $this->Api_model->checkRequired(array(
                    'msisdn'), $this->dpost);

                    if($req2['result'] != "success"){

                        echo $this->set_response(array("result" => "error", "message"=>"Msisdn is required"));
                        exit;
                    }

            }
        }

        if($this->dpost['addons']){
            $addons  = explode(',', $this->dpost['addons']);

            foreach($addons as $addin){

            if(!is_allowedAddons($addin, $this->companyid)){
                echo $this->set_response(array('result' => 'error', 'message' => 'Addon id '.$addin.' does not exists'));
                exit;

            }
            }

           }

        if($req['result'] == "error"){

            echo $this->set_response($req);
            exit;
        }
        if(!isAllowed_products_companyid($this->dpost['pid'], $this->companyid)){
            header("HTTP/1.1 404 PID Not Found");
            echo $this->set_response(array("result" => "error", "message"=> "pid does not exists"));
            exit;
        }
        $this->teams->send_msg(current_url(), "<br /><br /> REQUEST:<br />" . json_encode($this->dpost));
        $recurring = "0.00";
        $update_handset = false;
        $pass = random_str('alphanum', 8);
        $extra = false;
        $this->dpost['companyid'] = $this->companyid;
        $gid = $this->dpost['pid'];
        $this->dpost['msisdn_sn'] = null;
        /*
        if(!$gid){
        echo json_encode(array('result' => 'error', 'message' => 'Productid ' . $this->dpost['pid'] . ' does not have Group Assigned, Please contact +32484889888 for Help'));
        exit;
        }
         */

        if (!empty($this->dpost['setupfee'])) {
            if ($this->dpost['setupfee']) {
                $sp = $this->Api_model->checkRequired(array(
                    'setupfee_description',
                    'setupfee_amount',
                ), $this->dpost);
                if ($sp['result'] != 'success') {
                    echo $this->set_response($req1);
                    exit;
                } else {
                    $send_mailfee = true;
                }
            }
        }
        if (!validate_cd(trim($this->dpost['startdate']))) {
            echo $this->set_response(array(
                'result' => 'error',
                'message' => lang('startdate is invalid MM-DD-YYYY, and this must not be in the past'),
            ));
            exit;
        }
        if (!isAllowed_Clientid($this->companyid, $this->dpost['clientid'])) {
            echo $this->set_response(array(
                'result' => 'error',
                'message' => 'Clientid ' . $this->dpost['clientid'] . ' does not exists',
            ));
            exit;
        }
        if (!in_array($this->dpost['contractduration'], array(
            '1',
            '3',
            '6',
            '12',
            '24',
            '36',
        ))) {
            echo $this->set_response(array(
                'result' => 'error',
                'message' => 'contractduration supported is 1, 3, 6, 12, 24, 36 months ',
            ));
            exit;
        }
        if (!empty($this->dpost['promocode'])) {
            $promo = strtoupper(trim($this->dpost['promocode']));
        } else {
            $promo = "";
        }

        $userid = $this->dpost['clientid'];
        $client = $this->Admin_model->getClient($this->dpost['clientid']);
        if (getplatformbyPid($this->dpost['pid'])  != "TEUM") {
            if (!empty($this->dpost['delta_sim'])) {
                if (trim($this->dpost['delta_sim']) != "1111111111111111111") {
                    if (isSimcardFree(trim($this->dpost['delta_sim']))) {
                        echo $this->set_response(array(
                        'result' => 'error',
                        'message' => 'Your delta_sim has been used for previously order, please use free delta_sim',
                    ));
                        exit;
                    }
                    if (strlen(trim($this->dpost['delta_sim'])) != 19) {
                        echo $this->set_response(array(
                        'result' => 'error',
                        'message' => 'Your delta_sim is Invalid, Format: 89XXXXXXXXXXXXXXXXX, 19 Digit, you provide: ' . $this->dpost['delta_sim'],
                    ));
                        exit;
                    } else {
                        if(getPlatformbyPid($this->dpost['pid']) == "TEUM"){
                            $sim_ok = $this->Admin_model->getsimcard_teum(trim($this->dpost['delta_sim']), $this->companyid);

                        }else{
                            $sim_ok = $this->Admin_model->get_reseller_simcard(trim($this->dpost['delta_sim']), $this->companyid);

                        }
                        if (!$sim_ok) {
                            echo $this->set_response(array(
                            'result' => 'error',
                            'message' => 'The Simcard: ' . $this->dpost['delta_sim'] . ' is not registered yet in database, please contact United telecom MVNO IT Support',
                        ));
                            exit;
                        } else {
                            if (!empty($sim_ok->iPincode)) {
                                echo $this->set_response(array(
                                'result' => 'error',
                                'message' => 'The Simcard: ' . $this->dpost['delta_sim'] . ' has been assigned to MSISDN: ' . $sim_ok->iPincode . ' please use a free simcard',
                            ));
                                exit;
                            }
                        }
                    }

                    $this->dpost['msisdn_sim'] = $this->dpost['delta_sim'];

                    $sim = $this->artilium->getSn($this->dpost['delta_sim']);
                    $this->dpost['msisdn_sn'] = trim($sim->data->SN);
                    $this->dpost['msisdn'] = trim($sim->data->MSISDNNr);
                    $this->dpost['msisdn_puk1'] = $sim->data->PUK1;
                    $this->dpost['msisdn_puk2'] = $sim->data->PUK2;

                    if ($this->dpost['msisdn_sn']) {
                        $auto_provision = false;
                    }
                } else {
                    $mobiledata['msisdn_sim'] = $this->dpost['delta_sim'];
                }
            }
        } else {

            if($this->dpost['activate']){
               // beb55bf901802ead35236598f691bd6c
               $auto_provision = true;
            }
            log_message('error','Teum API Orders');
            if (!empty($this->dpost['msisdn'])) {
                $sim = $this->Admin_model->getsimcard($this->dpost['msisdn'], $this->companyid);


                if(!$sim){
                    echo $this->set_response(array(
                        'result' => 'error',
                        'message' => 'You have msisdn attached in the order but this msisdn '.$this->dpost['msisdn'].' could not be found in any reseller make sure you have use /InsertMsisdn',
                    ));
                    exit;

                }else{

                    if($sim->serviceid >0){
                        echo $this->set_response(array(
                            'result' => 'error',
                            'message' => 'This msisdn '.$this->dpost['msisdn'].' is currently assigned to serviceid '.$sim->serviceid.' please use a free simcard',
                        ));
                        exit;


                    }
                }
                $mobiledata['msisdn'] = $this->dpost['msisdn'];
                $mobiledata['msisdn_sn'] = $sim->MSISDN;
                $mobiledata['msisdn_sim'] = $sim->simcard;
                $mobiledata['msisdn_pin'] =  $sim->PIN1;
                $mobiledata['msisdn_puk1'] =  $sim->PUK1;
                $mobiledata['msisdn_puk2'] =  $sim->PUK2;
                $mobiledata['msisdn_imsi'] =  $sim->IMSI;
                $mobiledata['platform'] =  getPlatformbyPid($this->dpost['pid']);
            }
        }
        if ($this->dpost['porting']) {
            if (!$this->Admin_model->checkMsisdn(trim($this->dpost['porting_msisdn']))) {
                echo $this->set_response(array(
                    'result' => 'error',
                    'message' => 'The number ' . $this->dpost['porting_msisdn'] . ' already exists',
                ));
                exit;
            }
            if (strlen(trim($this->dpost['porting_msisdn'])) != "11") {
                echo $this->set_response(array(
                    'result' => 'error',
                    'message' => 'You are requesting for PortIn, but your porting_msisdn is invalid FORMAT: (countrycode)(number without leading 0) ex: 31612345788',
                ));
                exit;
            }

            if (DateTime::createFromFormat('Y-m-d', $this->dpost['porting_date']) !== false) {
            } else {
                echo $this->set_response(array(
                    'result' => 'error',
                    'message' => 'Your porting_date is invalid YYYY-MM-DD',
                ));
                exit;
            }
        }
        if (!empty($this->dpost['handset_name'])) {
            $req0 = $this->Api_model->checkRequired(array(
                'handset_price',
                'handset_terms',
                'handset_price',
            ), $this->dpost);

            if ($req0['result'] != "success") {
                echo $this->set_response($req0);
                exit;
            }
        }
        //If every parameter has been check, lets start inserting order
        $serviceid = $this->Api_model->insertOrder(array(
            'companyid' => $this->companyid,
            'type' => 'mobile',
            'status' => 'New',
            'packageid' => $this->dpost['pid'],
            'userid' => $userid,
            'date_created' => date('Y-m-d'),
            'billingcycle' => 'Monthly',
            'promocode' => $promo,
            'date_contract' => $this->dpost['startdate'],
            'contract_terms' => $this->dpost['contractduration'],
            'recurring' => getPriceRecurring($this->dpost['pid'], $this->dpost['contractduration']),
            'notes' => 'Order via API by IP :' . $this->ip . ' Token: ' . $this->headers['x-api-key'],
        ));

        if (!$serviceid) {
            echo $this->set_response(array(
                'result' => 'error',
                'message' => 'Order failed please contact Support +32 484889888',
            ));
            exit;
        } else {


            if($this->dpost['addons']){
                $addons  = explode(',', $this->dpost['addons']);
                if ($addons) {
                    log_message("error", "adding addon ".print_r($addons, 1));
                    foreach ($addons as $addonidnya) {
                        $this->Api_model->addAddon($serviceid, $addonidnya);
                    }
                }
            }


            $this->db->query("update a_reseller_simcard set serviceid=? where MSISDN=?", array($serviceid, $this->dpost['msisdn']));

            logAdmin(array(
                        'companyid' =>$this->companyid,
                        'userid' => $userid,
                        'user' => 'System',
                        'serviceid' => $serviceid,
                        'ip' => '127.0.0.1',
                        'description' => 'Order Received ID ' . $serviceid));


            if ($this->data['setting']->mobile_provider == "TMNL") {
                $req1 = $this->Api_model->checkRequired(array(
                    'porting_msisdn',
                    'porting_provider',
                    'porting_type',
                    'porting_customer_type',
                    'porting_date',
                ), $this->dpost);
            } else {
                $req1 = $this->Api_model->checkRequired(array(
                    'porting_msisdn',
                    'porting_provider',
                    'porting_simnumber',
                    'porting_type',
                    'porting_customer_type',
                ), $this->dpost);
            }

            $result['result'] = "success";
            $result['serviceid'] = $serviceid;
            $result['orderid'] = $serviceid;
            if ($this->dpost['porting']) {
                if ($req1['result'] != "success") {
                    echo $this->set_response($req1);
                    exit;
                }


                if(!$mobiledata){
                $mobiledata = array(
                    'msisdn_type' => 'porting',
                    'donor_msisdn' => preg_replace('/\D/', '', $this->dpost['porting_msisdn']),
                    'donor_provider' => $this->dpost['porting_provider'],
                    'donor_sim' => $this->dpost['porting_simnumber'],
                    'donor_type' => $this->dpost['porting_type'],
                    'donor_customertype' => $this->dpost['porting_customer_type'],
                    'donor_accountnumber' =>  $this->dpost['porting_customer_type'],
                    'msisdn_status' => 'OrderWaiting',
                    'date_wish' => $this->dpost['porting_date'],
                    'serviceid' => $serviceid,
                    'companyid' => $this->companyid,
                    'userid' => $userid,
                    'msisdn' => preg_replace('/\D/', '', $this->dpost['porting_msisdn']),
                    'msisdn_pin' => $this->data['setting']->default_pin,
                    'msisdn_sim' => $this->dpost['delta_sim'],
                    'ptype' => 'POST PAID',
                );
            }else{
                $mobiledata['serviceid'] = $serviceid;
                $mobiledata['companyid'] = $this->companyid;
                $mobiledata['msisdn_status'] = 'OrderWaiting';
                $mobiledata['msisdn_type'] = 'new';
                $mobiledata['userid'] =  $userid;
                $mobiledata['date_wish'] =  date('Y-m-d');

            }
                if ($this->data['setting']->mobile_provider != "TMNL") {
                    if ($this->dpost['porting_customer_type'] == "1") {
                        $req2 = $this->Api_model->checkRequired(array(
                            'porting_accountnumber',
                        ), $this->dpost);
                        if ($req2['result'] != "success") {
                            echo $this->set_response($req2);
                            exit;
                        }
                        $mobiledata['donor_accountnumber'] = $this->dpost['porting_accountnumber'];
                    }
                } else {
                    if (!empty($this->dpost['porting_accountnumber'])) {
                        $mobiledata['donor_accountnumber'] = $this->dpost['porting_accountnumber'];
                    }
                }
            } else {

                if(!$mobiledata){
                    $mobiledata = array(
                        'msisdn_type' => 'new',
                        'msisdn_status' => 'OrderWaiting',
                        'msisdn_sn' =>  $this->dpost['msisdn_sn'],
                        'serviceid' => $serviceid,
                        'companyid' => $this->companyid,
                        'userid' => $userid,
                        'msisdn_pin' => $this->data['setting']->default_pin,
                        'msisdn_sim' => $this->dpost['delta_sim'],
                        'ptype' => 'POST PAID',
                    );

                }else{

                    $mobiledata['serviceid'] = $serviceid;
                    $mobiledata['companyid'] = $this->companyid;
                    $mobiledata['msisdn_status'] = 'OrderWaiting';
                    $mobiledata['msisdn_type'] = 'new';
                    $mobiledata['userid'] =  $userid;
                    //$mobiledata['msisdn_pin'] =  $this->data['setting']->default_pin;

                }



            }
            $mobiledata['ptype'] = getpType($this->dpost['pid']);
            if (!empty($this->dpost['porting_accountnumber'])) {
                $mobiledata['donor_accountnumber'] = $this->dpost['porting_accountnumber'];
                $mobiledata['donor_customertype'] = 1;
            }
            unset($mobiledata['delta_sim']);

            $mobiledata['msisdn_contactid'] = $this->Admin_model->getContactIdArta($gid);
            if (!empty($this->dpost['handset_name'])) {
                $mobiledata_addons['serviceid'] = $serviceid;
                $mobiledata_addons['cycle'] = 'month';
                $mobiledata_addons['name'] = $this->dpost['handset_name'];
                $mobiledata_addons['terms'] = $this->dpost['handset_terms'];

                $this->dpost['handset_price'] = $this->dpost['handset_price'] * 1.21;
                $tot = round($this->dpost['handset_price'], 4) / $this->dpost['handset_terms'];
                //sending price exclusif vat;
                $mobiledata_addons['recurring_total'] = $tot;
                $mobiledata_addons['addon_type'] = 'others';
                $mobiledata_addons['addonid'] = '18';
                $this->db->insert('a_services_addons', $mobiledata_addons);
            }

            // mail('mail@simson.one','pppp',print_r($mobiledata_addons, true));
            $mobiledata['msisdn_languageid'] = setVoiceMailLanguageByClientLang($client->language);
            $service_mobile = $this->Api_model->insertMobileData($mobiledata);
            if ($service_mobile > 0) {
                $email = getNotificationOrderEmail($this->companyid);

                if ($email) {
                    $product = getProduct($this->dpost['pid']);
                    $headers = "From: noreply@united-telecom.be";
                    mail($email, "New order notification", "Dear Team,\n\nNew Order has just been arrived, Please process this order as soon as possible:\n\nServiceid: " . $serviceid . "\nProduct: " . $product->name . "\n\nRegards", $headers);
                }
                if (in_array($this->companyid, array('53', '56'))) {
                    if ($this->dpost['porting']) {
                        if ($auto_provision) {
                            if ($this->dpost['delta_sim'] != '1111111111111111111') {
                                $this->Admin_model->ChangeStatusService($serviceid, 'Pending');
                                $this->Admin_model->ChangeStatusOrderID($serviceid, 'OrderWaiting', 'mobile');
                                logAdmin(array(
                                'companyid' => $this->companyid,
                                'serviceid' => $serviceid,
                                'userid' => $userid,
                                'user' => 'System',
                                'ip' => $_SERVER['REMOTE_ADDR'],
                                'description' => 'Service : ' . $serviceid . ' has been accepted as sim added in the API',
                                ));


                                $activationr = array(
                                "serviceid" => $serviceid,
                                "companyid" => $this->companyid,
                                "msisdn_sim" => $this->dpost['delta_sim'],
                                "msisdn_type" => "porting",
                                "date_contract" => $this->dpost['startdate'],
                                "donor_msisdn" => preg_replace('/\D/', '', $this->dpost['porting_msisdn']),
                                "donor_provider" => $this->dpost['porting_provider'],
                                "donor_type" => $this->dpost['porting_type'],
                                "donor_sim" => $this->dpost['porting_simnumber'],
                                "donor_accountnumber" => $mobiledata['donor_accountnumber'],
                                "date_wish" => $this->dpost['porting_date'],
                                );
                                $resmin = $this->activate_mobile_arta($activationr);
                                //mail("mail@simson.one", "Auto Activation " . $serviceid, print_r($activationr, true).print_r($resmin, true));
                            }
                        }
                    }
                }
                if ($auto_provision) {

                    if (getPlatformbyPid($this->dpost['pid']) == "TEUM") {
                        log_message('error','Activate TEUM');
                        $this->activate_mobile_teum($serviceid);

                    }else{

                        log_message('error','NON TEUM');
                    }

                }
                $result['auto_provision'] = $auto_provision;
                $result['result'] = "success";
                log_message('error', 'New order has been received with Serviceid: #'.$serviceid);
                send_growl(array(
                'message' => 'New order has been received with Serviceid: #'.$serviceid,
                'companyid' => $this->companyid));
                if ($send_mailfee) {
                    $headers = "From: noreply@united-telecom.be";
                    mail("simson.parlindungan@united-telecom.be", "Please apply setup fee to " . $serviceid, print_r($this->dpost, true), $headers);
                }
            } else {
                $result['result'] = "error";
                $result['message'] = "Service mobile was not created";
            }
        }
        echo $this->set_response($result);
    }
    /**
         * @api {post} api/dev/crm/AcceptOrder AcceptOrder
         * @apiName AcceptOrder
         * @apiGroup Crm
         * @apiVersion 1.3.3
         * @apiDescription check if particular email has exists already
         * @apiParam {string} simcard simcard you wish to assign to the order (Required).
         * @apiParam {string} orderid orderid of the service (Required).
         * @apiHeaderExample {json} Header:
         *     {
         *         "X-API-KEY": "string"
         *     }
         * @apiSuccess {string} result success/error.
         * @apiSuccess {int} clientid id of customer when return any value.
         * @apiSuccess {object} details client object informations.
         * @apiSuccess {string} details.id id of customer when return any value.
         * @apiSuccess {string} details.id id of customer when return any value.
         * @apiSuccess {string} details.firsname firstname
         * @apiSuccess {string} details.lastname lastname.
         * @apiSuccess {string} details.companyname companyname.
         * @apiSuccess {string} details.email email.
         * @apiSuccess {string} details.address1 address1.
         * @apiSuccess {string} details.postcode postcode NN XXXX.
         * @apiSuccess {string} details.city cityname.
         * @apiSuccess {string} details.country country.
         * @apiSuccessExample Success-Response:
         * HTTP/1.1 200 OK
         *   {
         *    "clientid": "1",
         *    "result": "string"
         *    "details": {
         *    "firstname": "string"
         *    "lastname": "string"
         *    ...
         *        }
         *   }
         *
         */

    public function AcceptOrder()
    {
        $this->teams->send_msg(current_url(), json_encode($this->dpost));
        $req = $this->Api_model->checkRequired(array(
            'simcard','orderid'
        ), $this->dpost);
        if ($req['result'] == "success") {
            $q = $this->db->query("select a.*,b.agentid from a_services_mobile a left join a_clients b on b.id=a.userid where a.serviceid=? and a.companyid=?", array(
                $this->dpost['orderid'], $this->companyid
            ));
            if ($q->num_rows() > 0) {
                if ($q->row()->platform == "TEUM") {
                    $simcard= validatesimcard($this->dpost['simcard'], $q->row()->agentid);
                    if ($simcard) {
                        $update = array(
                            'msisdn_sim' => $this->dpost['simcard'],
                            'msisdn_sn' => $simcard->MSISDN,
                            'msisdn_puk1' => $simcard->PUK1,
                            'msisdn_puk2' => $simcard->PUK2,
                            'msisdn_pin' => $simcard->PIN1,
                            'status' => 'OrderWaiting');
                    } else {
                        echo $this->set_response(array(
                                'result' => 'error',
                                'message' => 'Simcard you provided :' . $this->dpost["simcard"] . ' is not in the list of free simcard or not assigned to agentid '.$q->row()->agentid,
                            ));
                        exit;
                    }
                } else {
                    $this->load->library('artilium', array('companyid' => $q->row()->companyid));
                    $sn = $this->artilium->getSn($this->dpost['simcard']);

                    if ($sn->result != "success") {
                        echo $this->set_response(array(
                                'result' => 'error',
                                'message' => 'Simcard you provided :' . $this->dpost["simcard"] . ' is not provisioned, please check the number or contact our support',
                            ));
                        exit;
                    } //$sn->result != "success"
                    $update["msisdn_sn"] = trim($sn->data->SN);
                    $update["msisdn_puk1"] = $sn->data->PUK1;
                    $update["msisdn_imsi"] = $sn->data->IMSI;
                    $update["msisdn_puk2"] = $sn->data->PUK2;
                    $update["msisdn_puk2"] ="OrderWaiting";
                }

                $this->Admin_model->update_services_data('mobile', $this->dpost['orderid'], $update);
                echo $this->set_response(array(
                    'result' => 'success',
                    'clientid' => $q->row()->id,
                    'details' => $q->row_array(),
                ));
            } else {
                echo $this->set_response(array(
                    "result" => "error",
                    "no Client found with email address provided",
                ));
            }
        } else {
            echo $this->set_response($req);
        }
    }


    /**
     * @api {post} api/dev/crm/SearchClient SearchClient
     * @apiName SearchClient
     * @apiGroup Crm
     * @apiVersion 1.3.3
     * @apiDescription check if particular email has exists already
     * @apiParam {string} email email address of customer (Required).
     * @apiHeaderExample {json} Header:
     *     {
     *         "X-API-KEY": "string"
     *     }
     * @apiSuccess {string} result success/error.
     * @apiSuccess {int} clientid id of customer when return any value.
     * @apiSuccess {object} details client object informations.
     * @apiSuccess {string} details.id id of customer when return any value.
     * @apiSuccess {string} details.id id of customer when return any value.
     * @apiSuccess {string} details.firsname firstname
     * @apiSuccess {string} details.lastname lastname.
     * @apiSuccess {string} details.companyname companyname.
     * @apiSuccess {string} details.email email.
     * @apiSuccess {string} details.address1 address1.
     * @apiSuccess {string} details.postcode postcode NN XXXX.
     * @apiSuccess {string} details.city cityname.
     * @apiSuccess {string} details.country country.
     * @apiSuccessExample Success-Response:
     * HTTP/1.1 200 OK
     *   {
     *    "clientid": "1",
     *    "result": "string"
     *    "details": {
     *    "firstname": "string"
     *    "lastname": "string"
     *    ...
     *        }
     *   }
     *
     */

    public function SearchClient()
    {
        //$this->teams->send_msg(current_url(), $this->dpost['email'].' '.$this->companyid);
        $this->teams->send_msg(current_url(), $this->dpost['email'].' '.$this->companyid.'Result :'.json_encode($this->dpost). 'Headers :'.json_encode($this->headers));


        $req = $this->Api_model->checkRequired(array(
            'email',
        ), $this->dpost);
        if ($req['result'] == "success") {
            $q = $this->db->query("select id,firstname,lastname,companyname,address1,postcode,city,country,email from a_clients where email like ? and companyid=?", array(
                $this->dpost['email'], $this->companyid
            ));
            if ($q->num_rows() > 0) {
                echo $this->set_response(array(
                    'result' => 'success',
                    'clientid' => $q->row()->id,
                    'details' => $q->row_array(),
                ));
            } else {
                echo $this->set_response(array(
                    "result" => "error",
                    "message" => "no Client found with email address provided",
                ));
            }
        } else {
            echo $this->set_response($req);
        }
    }

    /**
     * @api {post} api/dev/crm/GetClient GetClient
     * @apiName GetClient
     * @apiGroup Crm
     * @apiVersion 1.3.3
     * @apiDescription Get client information
     * @apiParam {clientid} clientid clientid (Required).
     * @apiHeaderExample {json} Header:
     *     {
     *         "X-API-KEY": "string"
     *     }
     * @apiSuccess {string} result success/error.
     * @apiSuccessExample Success-Response:
     * HTTP/1.1 200 OK
     *   {
     *    "id": "1",
     *    "result": "string"
     *    "firstname": "string"
     *    "lastname": "string"
     *    "companyname": "string"
     *    "address1": "string"
     *    "postcode": "string"
     *    "city": "string"
     *    "country": "string"
     *    "deltaid": "string"
     *    "email": "string"
     *    "password": "hashpassword"
     *   }
     *
     */

    public function GetClient()
    {
        $this->teams->send_msg(current_url(), $this->dpost['clientid']);
        $req = $this->Api_model->checkRequired(array(
            'clientid',
        ), $this->dpost);

        if(!isAllowed_Clientid($this->companyid,  $this->dpost['clientid'])){
            header("HTTP/1.1 404 Not Found");
            echo $this->set_response(array('result' => 'error', 'message'=> 'Client not found'));

            exit;
        }
        if ($req['result'] == "success") {

           $result = $this->Admin_model->getClient($this->dpost['clientid']);
           $a = (array) $result;
           unset($a['notes']);
           unset($a['password']);
            echo $this->set_response($a);
        } else {
            echo $this->set_response($req);
        }
    }


    /**
     * @api {post} api/dev/crm/GetService GetService
     * @apiName GetService
     * @apiGroup Crm
     * @apiVersion 1.3.3
     * @apiDescription Get Service details
     * @apiParam {serviceid} serviceid serviceid (Required).
     * @apiHeaderExample {json} Header:
     *     {
     *         "X-API-KEY": "string"
     *     }
     * @apiSuccess {string} result success/error.
     * @apiSuccess {string} identifier identifier can be msisdn or circuitid.
     * @apiSuccess {string} regdate date of registration.
     * @apiSuccess {string} brand brand of the service
     * @apiSuccess {array} products array of product assigned.
     * @apiSuccess {date} products.regdate  date of registration.
     * @apiSuccess {date} products.billingcycle payment cycle.
     * @apiSuccess {date} products.status status of the product
     * @apiSuccess {date} products.name name of the addon/product
     * @apiSuccess {date} products.addonid Addon id of the product
     * @apiSuccess {date} products.price  price of the product
     * @apiSuccessExample Success-Response:
     * HTTP/1.1 200 OK
     * {
     *  "result":"success",
     * "domain":"31632288223",
     * "regdate":"2018-06-26",
     * "brand":"Delta Postpaid",
     * "products":[
     * {
     * "regdate":"2018-06-26",
     * "billingcycle":"Monthly",
     * "status":"Active",
     * "name":"Delta Mobiel 300Mb",
     * "addonid":"6",
     * "price":"7.00"
     * },
     * {
     * "regdate":"2018-06-26",
     * "billingcycle":"Monthly",
     * "status":"Active",
     * "name":"Tariff 300 Units",
     * "addonid":"33",
     * "price":"0.00"
     * }
     * ]
     * }
     *
     */
    public function GetService()
    {
        $this->teams->send_msg(current_url(), $this->dpost['serviceid']);
        //$this->dpost['serviceid'] = $this->uri->segment(5);
        $req = $this->Api_model->checkRequired(array(
            'serviceid',
        ), $this->dpost);
        if (!isAllowed_Service($this->companyid, $this->dpost['serviceid'])) {
            echo $this->set_response(array('result' => 'error','message' => 'Service not found'));
            exit;
        }
        if ($req['result'] == "success") {
            $result = $this->Admin_model->GetService($this->dpost['serviceid']);
            if ($result->details->platform == "TEUM") {
                $res = (array) $result;
                $this->load->library('pareteum', array(
                    'companyid' => $this->companyid,
                    'api_id' => $result->api_id
                                ));
                //print_r($this->data['service']->details);
                $teum = $this->pareteum->accountInformation(array(
                                    'msisdn' => $result->details->msisdn
                             ));
                if ($teum->resultCode == "0") {
                    $t['status'] = $teum->{'card-network-status'};

                    $t['balance'] = $teum->{'remaining-credit'};
                    $t['currency'] = $teum->currency;
                    $t['userid'] = $result->userid;
                    $t['serviceid'] = $result->id;
                    $t['sim'] = array(
                    'msisdn' => $result->details->msisdn,
                    'simcard' => $result->details->msisdn_sim,
                    'pin1' => $result->details->msisdn_pin,
                    'puk1' => $result->details->msisdn_puk1,
                    'puk2' => $result->details->msisdn_puk2);

                    $t['allowance'] = $teum->resources;
                    $t['bundles'] = $this->Admin_model->getOptions($this->dpost['serviceid']);
                }

                $res = $t;
            //$res['extra'] = $result;
            //$res['details'] = $details;
            } else {
                $res = $result;
            }


            echo $this->set_response($res);
        } else {
            echo $this->set_response($req);
        }
    }

    /**
     * @api {post} api/dev/crm/GetClientInvoices GetClientInvoices
     * @apiName GetClientInvoices
     * @apiGroup Crm
     * @apiVersion 1.3.3
     * @apiDescription Get List of a client Invoices
     * @apiParam {clientid} clientid id of the client must be artilium id (Required).
     * @apiSuccess {string} result success/error.
     * @apiSuccess {int} count amount of result returned
     * @apiSuccess {array} invoices array of the invoices
     * @apiSuccess {int} invoices.id id of the invoices
     * @apiSuccess {string} invoices.invoicenum  invoice number
     * @apiHeaderExample {json} Header:
     *     {
     *         "X-API-KEY": "string"
     *     }
     * @apiSuccess {string} result success/error.
     * @apiSuccessExample Success-Response:
     * HTTP/1.1 200 OK
     *
     *     {
     *    "result": "success",
     *   "count": 2,
     *  "invoices": [
     *       {
     *           "id": "200000001",
     *           "invoicenum": "200000001"
     *       },
     *       {
     *           "id": "200000002",
     *           "invoicenum": "200000002"
     *       }
     *    ]
     * }
     *
     */

    public function GetClientInvoices()
    {
        $this->teams->send_msg(current_url(), json_encode($this->dpost));
        $req = $this->Api_model->checkRequired(array(
            'clientid',
        ), $this->dpost);
        if ($req['result'] == "success") {
            $this->dpost['companyid'] = $this->companyid;
            //$this->dpost['mageboid'] = $this->
            $result = $this->Api_model->getClientInvoice($this->dpost);
            echo $this->set_response($result);
        } else {
            echo $this->set_response($req);
        }
    }
    /**
     * @api {post} api/dev/crm/GetClientServices GetClientServices
     * @apiName GetClientServices
     * @apiGroup Crm
     * @apiVersion 1.3.3
     * @apiDescription Get List of a client Subscriptions/Services
     * @apiParam {clientid} clientid id of the client must be artilium id (Required).
     * @apiHeaderExample {json} Header:
     *     {
     *         "X-API-KEY": "string"
     *     }
     * @apiSuccess {string} result success/error.
     * @apiSuccess {int} count amount of result returned
     * @apiSuccess {array} subscriptions array of subscriptions
     * @apiSuccess {int} subscriptions.id Product service/subscription id.
     * @apiSuccess {int} subscriptions.packageid Product packageid.
     * @apiSuccess {string} subscriptions.identifier Product identifier can be mssisdn or circuitid.
     * @apiSuccess {date} subscriptions.regdate Product date registration.
     * @apiSuccess {decimal} subscriptions.amount Product base recurring price.
     * @apiSuccess {string} subscriptions.status Product status.

     * @apiSuccessExample Success-Response:
     * HTTP/1.1 200 OK
     *     {
     *     "result": "success",
     *     "count": 2,
     *     "subscriptions": [
     *     {
     *     "serviceid": "146",
     *     "packageid": "1",
     *     "identifier": "31648924972",
     *     "regdate": "2018-02-19",
     *     "amount": "21.00",
     *     "status": "Active"
     *     },
     *     {
     *     "serviceid": "147",
     *     "packageid": "30",
     *     "identifier": "31648924985",
     *     "regdate": "2018-02-20",
     *     "amount": "0.00",
     * "status": "Active"
     * }
     * ]
     * }
     *
     */

    public function GetClientServices()
    {
        $this->teams->send_msg(current_url(), json_encode($this->dpost));
        $req = $this->Api_model->checkRequired(array(
            'clientid',
        ), $this->dpost);
        if ($req['result'] == "success") {
            $result = $this->Admin_model->getServices($this->dpost['clientid']);
            echo $this->set_response($result);
        } else {
            echo $this->set_response($req);
        }
    }

    public function GetInvoice()
    {
        $this->teams->send_msg(current_url(), json_encode($this->dpost));
        $req = $this->Api_model->checkRequired(array(
            'invoiceid',
        ), $this->dpost);
        if ($req['result'] == "success") {
            $this->dpost['companyid'] = 53;
            $result = $this->Api_model->getInvoice($this->dpost['invoiceid']);
            echo $this->set_response($result);
        } else {
            echo $this->set_response($req);
        }
    }

    public function ModifyClient()
    {
        $this->teams->send_msg(current_url(), json_encode($this->dpost));
        $req = $this->Api_model->checkRequired(array(
            'clientid',
        ), $this->dpost);
        if ($req['result'] == "success") {
        } else {
            echo $this->set_response($req);
        }
    }

    public function Suspend()
    {
        $this->teams->send_msg(current_url(), json_encode($this->dpost));
        $req = $this->Api_model->checkRequired(array(
            'serviceid',
        ), $this->dpost);
        if ($req['result'] == "success") {
            if (!isAllowed_Service($this->companyid, $this->dpost['serviceid'])) {
                echo $this->set_response(array('result' => 'error','message' => 'Service not found'));
                exit;
            }

            $service = $this->Admin_model->getServiceCli($this->dpost['serviceid']);

            if($service->details->platform == "TEUM"){

                $this->Teum_Freeze($this->dpost['serviceid']);
            }else{


            }



        } else {
            echo $this->set_response($req);
        }
    }

    public function Resume()
    {
        $this->teams->send_msg(current_url(), json_encode($this->dpost));
        $req = $this->Api_model->checkRequired(array(
            'serviceid',
        ), $this->dpost);
        if ($req['result'] == "success") {
            if (!isAllowed_Service($this->companyid, $this->dpost['serviceid'])) {
                 echo $this->set_response(array('result' => 'error','message' => 'Service not found'));
                exit;
            }

            $service = $this->Admin_model->getServiceCli($this->dpost['serviceid']);

            if($service->details->platform == "TEUM"){

                $this->Teum_UnFreeze($this->dpost['serviceid']);
            }else{


            }

        } else {
            echo $this->set_response($req);
        }
    }

    public function Terminate()
    {
        $this->teams->send_msg(current_url(), json_encode($this->dpost));
        $req = $this->Api_model->checkRequired(array(
            'serviceid',
        ), $this->dpost);
        if ($req['result'] == "success") {



        } else {
            echo $this->set_response($req);
        }
    }



    public function AcceptPortOut()
    {
        $this->teams->send_msg(current_url(), json_encode($this->dpost));
        $req = $this->Api_model->checkRequired(array(
            'msisdn',
        ), $this->dpost);
        if ($req['result'] == "success") {
        } else {
            echo $this->set_response($req);
        }
    }
    public function RejectPortOut()
    {
        $this->teams->send_msg(current_url(), json_encode($this->dpost));
        $req = $this->Api_model->checkRequired(array(
            'msisdn',
        ), $this->dpost);
        if ($req['result'] == "success") {
        } else {
            echo $this->set_response($req);
        }
    }

    public function CancelOrder()
    {
        $this->teams->send_msg(current_url(), json_encode($this->dpost));
        $req = $this->Api_model->checkRequired(array(
            'orderid',
        ), $this->dpost);
        if ($req['result'] == "success") {
            $result = $this->Admin_model->whmcs_api($this->dpost);
            echo $this->set_response($result);
        } else {
            echo $this->set_response($req);
        }
    }
    /**
     * @api {get} api/dev/crm/GetProducts GetProducts
     * @apiName GetProducts
     * @apiGroup Crm
     * @apiVersion 1.3.3
     * @apiDescription Query Products that are configured for selling
     * @apiHeaderExample {json} Header:
     *     {
     *       "X_API_KEY": "string"
     *     }
     * @apiSuccess {string} result success/error.
     * @apiSuccess {int} orderid Id of the order
     * @apiSuccess {object} products products object
     * @apiSuccess {int} products.id Product id.
     * @apiSuccess {string} products.name Product name.
     * @apiSuccessExample Success-Response:
     *  HTTP/1.1 200 OK
     *  {
     * "result": "success",
     * "products": [
     * {
     * "id": "6",
     * "name": "Delta Mobiel 300Mb"
     *    }
     *    ]
     *  }
     *
     */

    public function getProducts()
    {

        $this->teams->send_msg(current_url(), $this->companyid);
        $result = $this->Api_model->getProducts($this->companyid);
        echo $this->set_response($result);
    }
    /**
     * @api {get} api/dev/crm/getAddons getAddons
     * @apiName getAddons
     * @apiGroup Crm
     * @apiVersion 1.3.3
     * @apiDescription get addons that are set to be sell
     * @apiHeaderExample {json} Header:
     *     {
     *       "X_API_KEY": "string"
     *     }
     * @apiSuccess {string} result success/error.
     * @apiSuccess {object} addons addons object
     * @apiSuccess {int} addons.id id of the addon.
     * @apiSuccess {string} addons.name name of the addons.
     * @apiSuccessExample Success-Response:
     * HTTP/1.1 200 OK
     * {
     *  "result": "success",
     *  "addons": [
     *  {
     *  "id": "14",
     *  "name": "Option 250 MB 30 dagen"
     *  }
     *            ],
     *  }
     *
     */

    public function getAddons()
    {
        $this->teams->send_msg(current_url(), $this->companyid);
        $result = $this->Api_model->getAddons($this->companyid);
        echo $this->set_response($result);
    }

    public function verify_sim()
    {
        $this->teams->send_msg(current_url(), json_encode($this->dpost));
        echo $this->set_response(array(
            'request' => $this->dpost['simcard'],
            'result' => $this->Admin_model->getsimcard(trim($this->dpost['simcard']), $this->companyid),
        ));
    }

    public function verify_date()
    {
        $this->teams->send_msg(current_url(), json_encode($this->dpost));
        if (!empty($this->post['startdate'])) {
            $i[42] = $this->dpost['startdate'];

            if (!validateDate(trim($i[42]), 'm-d-Y')) {
                echo $this->set_response(array(
                    'result' => 'error',
                    'message' => 'You are adding startdate but the format seem to be invalid, it must be MM-DD-YYYY',
                ));
                exit;
            } else {
                echo $this->set_response($this->dpost);
            }
        } else {
            echo $this->set_response($this->dpost);
        }
    }
    public function activate_mobile_teum($serviceid){
        $service_teum = $this->Admin_model->getServiceCli($serviceid);
        $client =  $this->Admin_model->getClient($service_teum->userid);
        if ($service_teum->details->platform == "TEUM") {

            // print_r($service_teum);
            // exit;
            $this->load->library('pareteum', array(
                'companyid' => $this->companyid,
                'api_id' => $service_teum->api_id
            ));
            if (empty($service_teum->details->teum_accountid)) {
                $account = array(
                    "AccountInfo" => array(
                        "AccountType" => "Prepaid",
                        "CustomerId" => (string) $client->teum_CustomerId,
                        "ExternalAccountId" => (string) $serviceid,
                        "AccountStatus" => "Active",
                        "Names" => array(
                            array(
                                "LanguageCode" => "eng",
                                "Text" => "Account"
                            )
                        ),
                        "Descriptions" => array(
                            array(
                                "LanguageCode" => "eng",
                                "Text" => "Account"
                            )
                        ),
                        "AccountCurrency" => "GBP",
                        "Balance" => 0,
                        "CreditLimit" => 0
                    )
                );
                $acct    = $this->pareteum->CreateAccount($account);
                log_message('error', print_r($acct, true));
                if ($acct->resultCode == "0") {
                    $AccountId = $acct->AccountId;
                } else {
                    die('Error when creating Account');
                }
            } else {
                $AccountId = $service_teum->details->teum_accountid;
            }
            $addons = getaddons($serviceid, $service_teum->details->msisdn_sn);
            $subs   = array(
                "CustomerId" => (int) $client->teum_CustomerId,
                "Items" => array(
                    array(
                        "AccountId" => (string)$AccountId,
                        "ProductOfferings" => $addons,
                        "ServiceAddress" => array(
                            "Address" => $client->address1,
                            "HouseNo" => $client->housenumber,
                            "City" => $client->city,
                            "ZipCode" => $client->postcode,
                            "State" => "unknown",
                            "CountryId" => "76"
                        )
                    )
                ),
                "channel" => "UnitedPortal V1"
            );
            log_message('error', print_r($subs, true));
            $subscription = $this->pareteum->AddSubscription($subs);
            if ($subscription->resultCode == "0") {
                $this->db->query("update a_reseller_simcard set SubscriptionId=?, resellerid=? where serviceid=?", array($subscription->Subscription->SubscriptionId,$client->agentid, $serviceid));
                //mme
                $this->Admin_model->update_services_data('mobile', $serviceid, array('teum_customerid' => $client->teum_CustomerId, 'teum_subscriptionid' => $subscription->Subscription->SubscriptionId));
                foreach ($subscription->Subscription->Products as $key => $row) {
                    foreach ($addons as $key => $r) {
                        if ($r['ProductOfferingId'] == $row->ProductOfferingId) {
                            $addx = getAddonsbyBundleID($row->ProductOfferingId);
                            $days  = ($addx->teum_autoRenew+1)*30;
                            $this->Admin_model->updateAddonTeum($serviceid, $row->ProductOfferingId, array(
                                'companyid' => $this->companyid,
                                'teum_DateStart' => date('Y-m-d'),
                                'teum_NextRenewal' => getFuturedate(date('Y-m-d'), 'day', 31),
                                'teum_DateEnd' => getFuturedate(date('Y-m-d'), 'day', $days),
                                'teum_CustomerOrderId' => $subscription->CustomerOrderId,
                                'teum_SubscriptionId' => $subscription->Subscription->SubscriptionId,
                                'teum_ProductId' => $row->ProductId,
                                'teum_ProductChargePurchaseId' => $row->ProductChargePurchaseId,
                                'teum_SubscriptionProductAssnId' => $row->SubscriptionProductAssnId,
                                'teum_ServiceId' => $subscription->Subscription->Services[$key]->ServiceId
                            ));
                            if ($key == "0") {
                                $this->db->query("update a_reseller_simcard set TeumServiceId=?,CustomerOrderId=? where serviceid=?", array($subscription->Subscription->Services[$key]->ServiceId,$subscription->CustomerOrderId, $serviceid));
                            }
                        }
                    }
                }





                if ($order->details->msisdn_type =="porting") {
                    $this->Admin_model->update_services_data('mobile', $serviceid, array(
                    'msisdn_status' => 'PortinPending',
                    'date_modified' => date('Y-m-d H:i:s'),
                    'teum_accountid' => $AccountId
                    ));
                } else {
                    $this->Admin_model->update_services_data('mobile', $serviceid, array(
                    'msisdn_status' => 'Active',
                    'date_modified' => date('Y-m-d H:i:s'),
                    'teum_accountid' => $AccountId
                    ));
                }



                $this->db->query("update a_services set status='Active' where id=?", array(
                    $serviceid
                ));
                $this->session->set_flashdata('success', lang('Activation has been Requested'));

                send_growl(array(
                    'message' => 'API User activate Number: ' . $order->details->msisdn,
                    'companyid' => $this->session->cid
                ));
                $service = $this->Admin_model->getService($serviceid);
                   $this->Admin_model->insertTopup(
                       array(
                       'companyid' => $this->companyid,
                       'serviceid' => $serviceid,
                       'userid' =>  $service->userid,
                       'income_type' => 'bundle',
                       'agentid' => $service->agentid,
                       'amount' => $service->recurring,
                       'user' => 'API User')
                   );


                logAdmin(array(
                    'companyid' => $this->companyid,
                    'serviceid' => $serviceid,
                    'userid' => $client->id,
                    'user' => 'API User',
                    'ip' => $_SERVER['REMOTE_ADDR'],
                    'description' => 'Service : ' . $serviceid . ' '.lang('requested for').' '.$order->details->msisdn_type.' '.lang('Activation').' ' . $sim->MSISDN
                ));
            }
            return array(
                'result' => "success"
            );

        }
    }
    public function activate_mobile_arta($dd)
    {
        $this->teams->send_msg(current_url(), json_encode($this->dpost));
        /*
        $dd =   array("serviceid" => "1804442",
        "companyid" => $companyid,
        "msisdn_sim" => "8931162111842008275",
        "msisdn_type" => "porting",
        "date_contract" => "04-08-2019",
        "donor_msisdn" => "31642458367",
        "donor_provider" => "ETMB-ACHT",
        "donor_type" => "0",
        "donor_sim" => "",
        "donor_accountnumber" => "",
        "date_wish" => "2019-04-09",
        );
         */
        $this->load->model('Admin_model');
        if (isPost()) {
            $this->load->library('artilium', array(
                'companyid' => $dd['companyid'],
            ));
            $this->load->library('magebo', array(
                'companyid' => $dd['companyid'],
            ));
            $serviceid = $dd["serviceid"];
            $date_contract = $dd["date_contract"];
            unset($dd["serviceid"]);
            unset($dd["companyid"]);
            unset($dd["date_contract"]);
            $sn = $this->artilium->getSn($dd['msisdn_sim']);

            if ($sn->result != "success") {
                return array(
                    'result' => 'error',
                    'message' => 'Simcard you provided :' . $dd["msisdn_sim"] . ' is not provisioned, please check the number',
                );
            } //$sn->result != "success"
            $dd["msisdn_sn"] = trim($sn->data->SN);
            $dd["msisdn_puk1"] = $sn->data->PUK1;
            $dd["msisdn_puk2"] = $sn->data->PUK2;
            if ($dd["msisdn_type"] != "porting") {
                $dd["msisdn"] = $sn->data->MSISDNNr;
                $dd["donor_type"] = "";
                $dd["donor_provider"] = "";
                $dd["donor_customertype"] = "";
            }

            $this->Admin_model->updateProductDetails($serviceid, 'mobile', $dd);
            $this->Admin_model->updateContractDate($serviceid, $date_contract);
            // Start Get fresh parameters
            $order = $this->Admin_model->getServiceCli($serviceid);
            $client = $this->Admin_model->getClient($order->userid);
            if ($order->details->msisdn_type == "porting") {
                if (substr($order->details->msisdn, 0, 2) == "31") {
                    if ($dd['date_wish'] < date('Y-m-d')) {
                        return array(
                            'result' => 'error',
                            'message' => 'Your Portin DateWish should be in The future',
                        );
                    }

                    if ($dd['date_wish'] != $order->details->date_wish) {
                        $this->Admin_model->update_services_data('mobile', $serviceid, array(
                            'date_wish' => trim($dd['date_wish']),

                        ));
                        $order = $this->Admin_model->getService($serviceid);
                    }
                }
            }
            $nc = explode("-", $order->date_contract);
            if ($order->details->msisdn_type == "porting") {
                if (substr($order->details->msisdn, 0, 2) == "31") {
                    if (!$order->details->porting_sms) {
                        $this->send_PortinInitiation($serviceid);
                    }
                }
            }
            $result = $this->artilium->ActiveNewSIM($client->firstname . ' ' . $client->lastname, $order->details);

            if ($result->result == "success") {
                logAdmin(array(
                    'companyid' => $this->companyid,
                    'serviceid' => $serviceid,
                    'userid' => $order->userid,
                    'user' => "API",
                    'ip' => $_SERVER['REMOTE_ADDR'],
                    'description' => 'Service : ' . $serviceid . ' requested for Activation',
                ));
                if ($order->details->msisdn_type == "porting") {
                    //$this->send_PortinInitiation($serviceid);
                    $this->Admin_model->ChangeStatusService($serviceid, 'Pending');
                    $this->Admin_model->ChangeStatusOrderID($serviceid, 'PortinPending', 'mobile');
                } //$order->details->msisdn_type == "porting"
                else {
                    if (date('Y-m-d') == $nc[2] . '-' . $nc[0] . '-' . $nc[1]) {
                        if (substr($order->details->msisdn, 0, 2) == "31") {
                            $pack = $this->artilium->GetListPackageOptionsForSnAdvance(trim($order->details->msisdn_sn));
                            $this->artilium->UpdateServices(trim($order->details->msisdn_sn), $pack, '1');
                        }

                        $this->Admin_model->ChangeStatusService($serviceid, 'Active');
                        $this->Admin_model->ChangeStatusOrderID($serviceid, 'Active', 'mobile');
                    } else {
                        $this->Admin_model->ChangeStatusService($serviceid, 'Pending');
                        $this->Admin_model->ChangeStatusOrderID($serviceid, 'ActivationRequested', 'mobile');
                    }
                }
                $mobile = $this->Admin_model->getService($serviceid);
                if (!$noAddSim) {
                    $addsim = $this->magebo->AddSIMToMagebo($mobile);
                    if ($addsim->result == "success") {
                        if (date('Y-m-d') == $nc[2] . '-' . $nc[0] . '-' . $nc[1]) {
                            $this->magebo->addPricingSubscription($mobile);
                        } else {
                            if (substr($mobile->details->msisdn, 0, 2) == "32") {
                                $this->Admin_model->updateContractDate($serviceid, date('m-d-Y'));
                                //$this->magebo->addPricingSubscription($mobile);
                            }
                        }
                        // Add Simcardlog into Magebo
                    } //$addsim->result == "success"
                    //assign Bundles when sim is activated
                    $bundles = $this->Admin_model->getBundlebyProduct($order->packageid);
                    if ($bundles) {
                        $IDS = array();
                        // activate tarief per packages
                        foreach ($bundles as $bundleid) {
                            $r = $this->artilium->AddBundleAssign($order->details->msisdn_sn, $bundleid, date('Y-m-d') . 'T' . date('H:i:s'), '2099-12-31T23:59:59');
                            if ($r->result == "success") {
                                if ($this->data['setting']->create_magebo_bundle) {
                                    $create = 1;
                                } //$this->data['setting']->create_magebo_bundle
                                else {
                                    $create = 0;
                                }
                                $this->magebo->ProcessPricing($order, $client->mageboid, $bundleid, $create, false, $order->date_contract);

                                $pricingid = $this->add_pricing_extra($order->id);
                                if ($pricingid) {
                                    $this->magebo->update_generalPricing($order->id, $pricingid);
                                }
                                $IDS[] = $r->id;
                            } //$r->result == "success"
                            else {
                                $IDS[] = $r;
                            }
                        } //$bundles as $bundleid
                        if ($IDS) {
                            logAdmin(array(
                                'companyid' => $this->companyid,
                                'serviceid' => $serviceid,
                                'userid' =>  $order->userid,
                                'user' => 'API',
                                'ip' => $_SERVER['REMOTE_ADDR'],
                                'description' => 'Service : ' . $dd['serviceid'] . ' added bundle id ' . implode(',', $IDS),
                            ));
                            $this->Admin_model->updateBundleID($order->id, implode(',', $IDS));
                        } //$IDS
                    } //$bundles
                } //!$noAddSim
                //Check if contract date is today otherwise disable all services
                if (date('Y-m-d') <= $nc[2] . '-' . $nc[0] . '-' . $nc[1]) {
                    if (substr(trim($order->details->msisdn), 0, 2) == "31") {
                        $msisdn = $this->artilium->getSn($order->details->msisdn_sim);
                        $pack = $this->artilium->GetListPackageOptionsForSnAdvance($msisdn->data->SN);
                        $this->artilium->UpdateServices($msisdn->data->SN, $pack, '0');
                    } else {
                        $this->artilium->PartialFullBar($order->details, 0);
                    }
                } else {
                    if (substr(trim($order->details->msisdn), 0, 2) == "31") {
                        $msisdn = $this->artilium->getSn($order->details->msisdn_sim);
                        $pack = $this->artilium->GetListPackageOptionsForSnAdvance($msisdn->data->SN);
                        $this->artilium->UpdateServices($msisdn->data->SN, $pack, '1');
                        $this->artilium->UpdateCLI($msisdn->data->SN, 1);
                    }
                }
                if ($dd["msisdn_type"] == "porting") {
                    sleep(2);
                    $this->magebo->addSimcardLog(trim($order->details->msisdn_sim), '599');
                    $this->Admin_model->ChangeStatusOrderID($serviceid, 'PortinPending', 'mobile');
                } else {
                    $this->artilium->UpdateCLI($msisdn->data->SN, 1);
                    $this->magebo->addSimcardLog(trim($order->details->msisdn_sim), '599');
                    sleep(2);
                    $this->magebo->addSimcardLog(trim($order->details->msisdn_sim), '612');
                }
                return array('success', lang('Activation has been Requested'));
            } //$result->result == "success"
            else {
                return array('error', $result->message);
            }
            //redirect('admin/client/detail/' . $client->id);
            // End activating simcard
        } //isPost()
    }
    public function add_pricing_extra($id)
    {
        $ids = array();
        $this->load->model('Admin_model');
        $service = $this->Admin_model->getService($id);
        $client = $this->Admin_model->getClient($service->userid);
        $option = $this->Admin_model->getOptions($service->id);

        if ($service->iGeneralPricingIndex) {
            $ids = explode(',', $service->iGeneralPricingIndex);
        }
        $this->load->library('magebo', array('companyid' => $service->companyid));
        if ($option) {
            foreach ($option as $row) {
                if ($row->recurring_total > 0) {
                    $ids[] = $this->magebo->addExtraPricing($client, $row, str_replace('-', '/', $service->date_contract));
                }
            }

            //$this->Admin_model->updateBundleID($service->id, implode(',', $ids));
            return implode(',', $ids);
        } else {
            return false;
        }
    }

    public function send_PortinInitiation($id)
    {
        /*if(empty($id)){
        $id = $this->uri->segment(4);
        }
         */
        $this->load->model('Admin_model');
        $mobile = $this->Admin_model->getServiceCli($id);
        $client = $this->Admin_model->getClient($mobile->userid);
        $this->data['setting'] = globofix($client->companyid);
        $this->db->query("update a_services_mobile set porting_sms = ? where serviceid=?", array(1, $id));
        if ($mobile->details->donor_accountnumber) {
            return array('result' => 'succes');
        } else {
            if ($this->data['setting']->enable_sms == "1") {
                $num = trim($mobile->details->msisdn);
                $this->load->library('sms', array('username' => $this->data['setting']->sms_username, 'password' => $this->data['setting']->sms_password, 'companyid' => $this->companyid));
                $sms_res = $this->sms->send_message_bulksms(array(array('from' => $this->data['setting']->sms_senderid, 'to' => '+' . $num, 'body' => $this->data['setting']->sms_content)));
            }

            if ($this->data['setting']->smtp_type == 'smtp') {
                $config = array(
                    'protocol' => 'smtp',
                    'smtp_host' => $this->data['setting']->smtp_host,
                    'smtp_port' => $this->data['setting']->smtp_port,
                    'smtp_user' => $this->data['setting']->smtp_user,
                    'smtp_pass' => $this->encryption->decrypt($this->data['setting']->smtp_pass),
                    'mailtype' => 'html',
                    'charset' => 'utf-8',
                    'starttls' => true,
                    'wordwrap' => true,
                );
            } else {
                $config['protocol'] = 'sendmail';
                $config['mailpath'] = '/usr/sbin/sendmail';
                $config['mailtype'] = 'html';
                $config['charset'] = 'utf-8';
                $config['wordwrap'] = true;
            }
            $this->email->clear(true);
            $this->email->initialize($config);
            $body = getMailContent('portin_initiation', $client->language, $client->companyid);

            $body = str_replace('{$name}', format_name($client), $body);
            $body = str_replace('{$base_url}', url_to_domain(base_url()), $body);
            $body = str_replace('{$msisdn_sim}', $mobile->details->msisdn_sim, $body);
            $body = str_replace('{$msisdn_puk1}', $mobile->etails->msisdn_puk1, $body);
            $body = str_replace('{$msisdn_puk2}', $mobile->etails->msisdn_puk2, $body);
            $body = str_replace('{$msisdn', $mobile->etails->msisdn, $body);

            $this->email->set_newline("\r\n");
            $this->email->from($this->data['setting']->smtp_sender, $this->data['setting']->smtp_name);
            $this->email->to($client->email);
            //$this->email->bcc('mail@simson.one');
            $this->email->subject(getSubject('portin_initiation', $client->language, $client->companyid));
            $this->email->message($body);
            if ($this->email->send()) {
                logEmailOut(array('userid' => $client->id, 'companyid' => $client->companyid, 'to' => $client->email, 'subject' => getSubject('portin_initiation', $client->language, $client->companyid), 'message' => $body));
                return array('result' => 'success');
            } else {
                logEmailOut(array('userid' => $client->id, 'companyid' => $client->companyid, 'to' => $client->email, 'subject' => getSubject('service_change_request', $client->language, $client->companyid), 'message' => $body, 'status' => 'error', 'error_message' => $this->email->print_debugger()));
                return array('result' => 'error', 'message' => 'error when sending email');
            }
        }
    }
    public function Teum_Freeze($serviceid)
    {

        $service = $this->Admin_model->getService($serviceid);
        if ($service) {
            $this->load->library('pareteum', array(
                'companyid' => $this->companyid,
                'api_id' => $service->api_id
            ));
            $res = $this->pareteum->freeze(array(
                'msisdn' => $service->details->msisdn,
                'SubscriptionId' => $this->Admin_model->getTeumSubscriptionId($service->details->msisdn),
                'SubscriptionFreezeReason' => 'FREEZE_CUSTREQUEST',
                'ExternalReference' => $_POST['ExternalRef'],
                'channel' => 'UnitedPortal V1',
                'comments' => 'Freeze by: ' . $this->session->firstname . ' ' . $this->session->lastname
            ));
            echo $this->set_response($res);
        } else {
            die('Access Denied');
        }
    }
    public function Teum_Unfreeze($serviceid)
    {
        $service = $this->Admin_model->getService($serviceid);
        if ($service) {
            $this->load->library('pareteum', array(
                'companyid' => $this->companyid,
                'api_id' => $service->api_id
            ));
            $res = $this->pareteum->unfreeze(array(
                'msisdn' => $service->details->msisdn,
                'SubscriptionId' => $this->Admin_model->getTeumSubscriptionId($service->details->msisdn),
                'ExternalReference' => '',
                'channel' => 'UnitedPortal V1',
                'comments' => 'Freeze by: ' . $this->session->firstname . ' ' . $this->session->lastname
            ));
            echo $this->set_response($res);
        } else {
            die('Access Denied');
        }
    }
    public function StopBundleRenewal()
    {
        $req = $this->Api_model->checkRequired(array(
            'serviceid','bundleid'
        ), $this->dpost);
        if ($req['result'] != "success") {
            echo $this->set_response($req);
            exit;
        }
        if (!isAllowed_Service($this->companyid, $this->dpost['serviceid'])) {
            echo $this->set_response(array('result' => 'error','message' => 'Service not found'));
            exit;
        }
        $this->db->query("update a_services_addons set teum_autoRenew =? where id= ? ", array(0, $this->dpost['bundleid']));
        echo $this->set_response(array('result' => 'success'));
    }
    public function ReloadCredit()
    {

        //$this->load->model('Admin_model');
        $req = $this->Api_model->checkRequired(array(
            'serviceid','amount'
        ), $this->dpost);
        if ($req['result'] != "success") {
            echo $this->set_response($req);
            exit;
        }
        if (!isAllowed_Service($this->companyid, $this->dpost['serviceid'])) {
            echo $this->set_response(array('result' => 'error','message' => 'Service not found'));
            exit;
        }
       // $service = $this->Admin_model->getService($this->dpost['serviceid']);
        $service = $this->Admin_model->getServiceCLI($this->dpost['serviceid']);
        $client  = $this->Admin_model->getService($service->userid);
        $this->load->model('Agent_model');
        $agent = $this->Agent_model->getAgent($client->agentid);

        if ($this->dpost['reseller_charge'] == "Yes") {
            if ($agent->reseller_type == "Prepaid") {
                //we need to set time limit to 0 because we are going to lock balance to avoid the same request at the same time
                set_time_limit(0);
                do {
                    if (!file_exists(APPPATH.'lock/reseller_balance_'.$client->agentid)) {
                        log_message('error', 'No locking, we continue adding balance and now we lock the customer balance');
                        file_put_contents(APPPATH.'lock/reseller_'.$client->agentid, $this->dpost['serviceid']);
                        break;
                    }
                } while (true);
            }
            if ($agent->reseller_type == "Prepaid") {
                if ($this->dpost['credit'] > $agent->reseller_balance) {
                    $this->session->set_flashdata('error', lang('Reseller does not have enough balance to topup this subscriber'));
                    unlink(APPPATH.'lock/reseller_balance_'.$client->agentid, $this->dpost['serviceid']);

                    echo $this->set_response(array('result' => 'error', 'message' => 'Reseller does not have enough balance to topup this subscriber'));
                    exit;
                    //redirect('admin/subscription/detail/'.$this->dpost['serviceid']);
                }
            }
        }

        if ($service) {

            if($service->details->platform == "ARTA"){

                $this->load->library('artilium', array(
                    'companyid' => $this->companyid
                ));




                $res = $this->artilium->CReloadCredit($this->dpost['amount'], $service->details->msisdn_sn, rand(100000000000000, 99999999999999999), 'Ordered by:' . $this->session->firstname);
                if ($res->ReloadResult->Result == 0) {
                    if ($this->dpost['reseller_charge'] == "Yes") {
                        if ($agent->reseller_type == "Prepaid") {
                            $new_balance = $agent->reseller_balance - $this->dpost['amount'];
                            $this->db->query("update a_clients_agents set reseller_balance=? where id=?", array($new_balance, $agent->id));
                            unlink(APPPATH.'lock/reseller_balance_'. $agent->id, $_POST['serviceid']);
                        }
                    }
                    logAdmin(array(
                        'companyid' => $this->companyid,
                        'userid' => $service->userid,
                        'serviceid' => $this->dpost['serviceid'],
                        'user' => 'API',
                        'ip' => $_SERVER['REMOTE_ADDR'],
                        'description' => 'add credit amount of ' . $this->dpost['amount'] . ' to serviceid: ' . $this->dpost['serviceid']
                    ));
                    echo $this->set_response(array(
                        'result' => true,
                        'reload' => $res
                    ));
                } else {
                    echo $this->set_response(array(
                        'result' => false,
                        'data' => $res,
                        'service' => $service
                    ));
                }

            }else{
                $this->load->library('pareteum', array(
                    'companyid' => $this->companyid,
                    'api_id' => $service->api_id
                ));



                $res = $this->pareteum->topup(array(
                    'msisdn' => $service->details->msisdn,
                    'amount' => $this->dpost['amount'] * 100
                ));
                $amount =  $this->dpost['amount'] * 100;

                if ($res->resultCode == "0") {
                    if ($this->dpost['reseller_charge'] == "Yes") {
                        if ($agent->reseller_type == "Prepaid") {
                            $new_balance = $agent->reseller_balance - $this->dpost['amount'];
                            $this->db->query("update a_clients_agents set reseller_balance=? where id=?", array($new_balance, $agent->id));
                            unlink(APPPATH.'lock/reseller_balance_'. $agent->id, $_POST['serviceid']);
                        }
                    }
                    $topupid =   $this->Admin_model->insertTopup(
                        array(
                             'companyid' => $this->companyid,
                            'serviceid' => $this->dpost['serviceid'],
                            'userid' =>  $service->userid,
                            'income_type' => 'topup',
                            'agentid' => $service->agentid,
                            'amount' => $this->dpost['amount'],
                        'user' => 'API User')
                    );
                    echo $this->set_response(array('result' => 'success', 'topupid' => $topupid));
                    logAdmin(array(
                            'companyid' => $this->companyid,
                            'serviceid' => $this->dpost['serviceid'],
                            'userid' =>  $service->userid,
                            'user' => 'API User',
                            'ip' => $_SERVER['REMOTE_ADDR'],
                            'description' => 'Service : ' .  $this->dpost['serviceid'] . ' Topup amount : '. $this->data['setting']->currency.' '. $this->dpost['amount'].' has been loaded to '.$service->details->msisdn
                        ));

            }

        }

            } else {
                echo $this->set_response(array('result' => 'error', 'message' => 'there was an error handling your request'));
            }

    }
    public function GetServiceByCli(){
        $req = $this->Api_model->checkRequired(array(
            'msisdn'
        ), $this->dpost);

        if ($req['result'] != "success") {
            echo $this->set_response($req);
            exit;
        }

        $service = $this->Admin_model->getServiceBySN(trim($this->dpost['msisdn']));


        if($service){
            if (!isAllowed_Service($this->companyid, $service->serviceid)) {
                header("HTTP/1.1 404 Not Found");
                echo $this->set_response(array('result' => 'error','message' => 'Service not found'));
                exit;
            }else{
                $client = $this->Admin_model->getClient($service->userid);
                $serv =  (array) $this->Admin_model->getServiceCLI($service->serviceid);
                //ksort($serv);
                echo $this->set_response(array('result'=> 'success', 'service' => $serv, 'client' => $client));
                exit;
            }


        }else{
            header("HTTP/1.1 404 Not Found");
            echo $this->set_response(array('result' => 'error', 'message' => 'No record found'));
            exit;
        }


    }
    public function EnableAutoRenewal(){

        $req = $this->Api_model->checkRequired(array(
            'serviceid','bundleid','month'
        ), $this->dpost);

        if(!in_array($this->dpost['month'], array(1,3,6,12,24))){

            echo $this->set_response(array('result' => 'error', 'message' => 'month possible value are 1, 3, 6, 12, 24'));
            exit;

        }

        if ($req['result'] != "success") {
            echo $this->set_response($req);
            exit;
        }
        if (!isAllowed_Service($this->companyid, $this->dpost['serviceid'])) {
            header("HTTP/1.1 404 Not Found");
            echo $this->set_response(array('result' => 'error','message' => 'Service not found'));
            exit;
        }
        $this->load->model('Admin_model');
        $service = $this->Admin_model->getServiceCli($this->dpost['serviceid']);
        $this->db = $this->load->database('default', true);
        $this->db->query("update a_services_addons set teum_autoRenew =? where id= ? ", array($this->dpost['month'], $this->dpost['bundleid']));
        logAdmin(array(
            'companyid' => $this->companyid,
            'serviceid' => $this->dpost['serviceid'],
            'userid' =>    $service->userid,
            'user' => 'API User',
            'ip' => $_SERVER['REMOTE_ADDR'],
            'description' => 'Service: ' .  $this->dpost['serviceid'] . ' auto renew has been set be disabled  '.$this->dpost['month'].' for addon id: '.$_POST['bundleid']
        ));

        echo $this->set_response(array('result' => 'success'));


    }
    public function AddBundle()
    {
        $req = $this->Api_model->checkRequired(array(
            'serviceid','addonid','month'
        ), $this->dpost);

        if(!in_array($this->dpost['month'], array(1,3,6,12,24,600))){

            echo $this->set_response(array('result' => 'error', 'message' => 'month possible value are 1, 3, 6, 12, 24, 600'));
            exit;

        }

        if ($req['result'] != "success") {
            echo $this->set_response($req);
            exit;
        }
        if (!isAllowed_Service($this->companyid, $this->dpost['serviceid'])) {
            header("HTTP/1.1 404 Not Found");
            echo $this->set_response(array('result' => 'error','message' => 'Service not found'));
            exit;
        }
        $this->load->model('Admin_model');
        log_message("error", print_r($this->dpost, true));
        $service = $this->Admin_model->getService($this->dpost['serviceid']);
        if ($service) {
            $reseller_sim_card = $this->db->query("select * from a_reseller_simcard where MSISDN=?", array($service->details->msisdn));
            $this->load->library('pareteum', array(
                'companyid' => $this->companyid,
                'api_id' => $service->api_id
            ));
            $client    = $this->Admin_model->getClient($service->userid);
            $addons    = getAddonInformation($this->dpost['addonid']);


            if (!$addons) {
                header("HTTP/1.1 400 Bad Request");
                echo $this->set_response(array('result' => 'error', 'message' => 'Addonid not found'));

                exit;
            }
            if ($addons->companyid != $this->companyid) {
                header("HTTP/1.1 401  No Authorized");
                echo $this->set_response(array('result' => 'error', 'message' => 'Addonid is not accessible'));
                exit;
            }


            if($service->details->platform == "TEUM"){
                $AccountId = $service->details->teum_accountid;
                $subs = array(
                    "CustomerId" => (int) $client->teum_CustomerId,
                    "SubscriptionId" => $reseller_sim_card->row()->SubscriptionId,
                    "Channel" => "UnitedPortal V1",
                    "Offerings" => array(
                        array(
                        "ProductOfferingId" => (int) $addons->bundleid,
                        "OrderedProductCharacteristics" => array(
                            array(
                                "Name" => "MSISDN",
                                "Value" => $service->details->msisdn
                            )
                        )
                        )
                        )

                );

                log_message('error', print_r($subs, true));
                $subscription = $this->pareteum->subscriptions_offerings($subs);
                log_message('error', print_r($subscription, true));
                if ($subscription->resultCode == "0") {
                    if (checkaddon_existance($this->dpost['addonid'], $this->dpost['serviceid'])) {
                        $this->Admin_model->updateAddon(array('teum_autoRenew' => $this->dpost['month']-1,
                        'teum_NextRenewal' => getFuturedate(date('Y-m-d'), 'day', 31)), checkaddon_existance($this->dpost['bundleid'], $this->dpost['serviceid'])->id);
                        $addonidx = checkaddon_existance($this->dpost['addonid'], $this->dpost['serviceid'])->id;
                    } else {
                        $offering = array(
                        'name' => $addons->name,
                        'terms' => $addons->bundle_duration_value,
                        'cycle' => $addons->bundle_duration_type,
                        'serviceid' => $this->dpost['serviceid'],
                        'addonid' => $_POthis->dpostST['addonid'],
                        'companyid' => $this->companyid,
                        'recurring_total' => $addons->recurring_total,
                        'addon_type' => 'option',
                        'arta_bundleid' => $addons->bundleid,
                        'teum_autoRenew' => $_POST['month']-1,
                        'teum_NextRenewal' => getFuturedate(date('Y-m-d'), 'day', 31),
                        'teum_DateStart' => $subscription->PurchaseOrder->CompletionDate,
                        'teum_CustomerOrderId' => $subscription->PurchaseOrder->CustomerOrderId,
                        'teum_SubscriptionId' => $reseller_sim_card->row()->SubscriptionId,
                        'teum_ProductId' => null,
                        'teum_ProductChargePurchaseId' => null,
                        'teum_SubscriptionProductAssnId' => null,
                        'teum_ServiceId' => null
                        );
                       $addonidx =  $this->Admin_model->insertAddon($offering);
                    }
                    $topupid = $this->Admin_model->insertTopup(
                         array(
                            'companyid' => $this->companyid,
                            'serviceid' => $this->dpost['serviceid'],
                            'userid' =>  $service->userid,
                            'income_type' => 'bundle',
                            'agentid' => $client->agentid,
                            'amount' => $addons->recurring_total,
                            'user' => "API")
                     );

                    //$this->session->set_flashdata('success', 'bundle has been added');
                    echo $this->set_response(array('result' => 'success', 'bundleid' => $addonidx, 'orderid' => $topupid));
                    logAdmin(array(
                            'companyid' => $this->companyid,
                            'serviceid' => $this->dpost['serviceid'],
                            'userid' =>  $service->userid,
                            'user' => 'Api',
                            'ip' => $_SERVER['REMOTE_ADDR'],
                            'description' => 'Service : ' .  $this->dpost['serviceid'] . ' Bundle: '.$addons->name.' has been added'
                        ));

            }else{
                header("HTTP/1.1 500 Teum API error");
                echo $this->set_response(array('result' => 'error', 'message' => $subscription));
            }


            } else {
                header("HTTP/1.1 400 Bad Request");
                echo $this->set_response(array('result' => 'error', 'message' => 'there was an error handling your request'));
            }
            // redirect('admin/subscription/detail/' . $this->dpost['serviceid']);
        }
    }
    public function getCdr()
    {
        $req = $this->Api_model->checkRequired(array(
            'serviceid','start','end'
        ), $this->dpost);
        if ($req['result'] != "success") {
            echo $this->set_response($req);
            exit;
        }
        $this->load->model('Admin_model');
        if (!isAllowed_Service($this->companyid, $this->dpost['serviceid'])) {
            echo $this->set_response(array('result' => 'error','message' => 'Service not found'));
            exit;
        }
        $service = $this->Admin_model->getServiceCli($this->dpost['serviceid']);
        if ($service) {

            if($service->details->platform == "TEUM"){
                $this->load->library('pareteum', array(
                    'companyid' => $this->companyid,
                    'api_id' => $service->api_id
                ));
                $res = $this->pareteum->usage(array(
                    'msisdn' => $service->details->msisdn,
                    'fromDate' => $this->dpost['start'].'T00:00:00',
                    'toDate' => $this->dpost['end'].'T23:59:59'
                ));

            }else{

                $this->load->library('artilium', array(
                    'companyid' => $this->companyid
                ));
                $res = $this->artilium->get_cdr($service);

            }




            echo $this->set_response($res);
        } else {
            echo $this->set_response(array('result'=> 'error', 'message' => 'Service not found'));
        }
    }


    public function getAgents()
    {
        $this->load->model('Admin_model');
        $agents = $this->Admin_model->getAgents($this->companyid);
        echo $this->set_response($agents);
    }

    public function AssignFreeMsisdntoAgent(){


    }

    public function InsertMsisdn(){


    }
}
