<?php
defined('BASEPATH') or exit('No direct script access allowed');
class AdminapiController extends CI_Controller
{
    //protected $data = [];
    public function __construct()
    {
        // Ensure you run parent constructor
        parent::__construct();
        $this->config->set_item('language', 'english');
        $this->lang->load('admin');
        header('Content-Type: application/json');
        $this->load->model('Adminapi_model', 'api');
    }
}
class Adminapi extends AdminApiController
{
    public function __construct()
    {
        error_reporting(1);
        parent::__construct();
        $this->load->helper('string');
        if (isset($_SERVER['HTTP_ORIGIN'])) {
            header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
            header('Access-Control-Allow-Credentials: true');
            header('Access-Control-Max-Age: 86400');
        }
        if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD'])) {
                header("Access-Control-Allow-Methods: GET");
            }
            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS'])) {
                header("Access-Control-Allow-Headers:{$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
            }
            exit(0);
        }
        $g = $this->input->request_headers();
        foreach ($g as $key => $header) {
            $headers[strtolower($key)] = $header;
        }
        if (!in_array($this->uri->segment(4), array(
            'login',
            'reset',
            'getcompanylist',
            'confirm',
            'upload',
            'download_invoice',
            'download_proforma'
        ))) {
            $this->load->helper(array('jwt', 'authorization'));
            $decrypt = json_decode($this->encryption->decrypt($headers['x-api-key']));
            $auth    = $this->api->validate_admin_token($decrypt);
            if (!$auth) {
                echo json_encode(array(
                    'result' => false,
                    'message' => 'Unauthorized access',
                    'auth' => $decrypt
                ));
                exit;
            }
            $this->api->update_present($auth->id);
        }
        $this->adminid         = $auth->id;
        $this->admin_name      = $auth->firstname . ' ' . $auth->lastname;
        $headers['uri_string'] = $this->uri->uri_string();
        $headers['ip_addr']    = $_SERVER['REMOTE_ADDR'];
        $this->companyid       = get_companyidby_url(base_url());
        $this->headers         = $headers;
        $ip                    = $_SERVER['REMOTE_ADDR'];
        $this->ip              = $ip;
        $post                  = file_get_contents("php://input");
        if (isPost()) {
            $obj         = (array) json_decode($post);
            $this->dpost = $obj;
        } else {
            if (!empty($post)) {
                $obj         = (array) json_decode($post);
                $this->dpost = $obj;
            } else {
                $this->dpost = array();
            }
        }
        $this->setProperty();
    }
    public function setProperty()
    {
        $this->data['setting'] = globofix($this->companyid);
        //$this->getStafOnline();
    }
    public function getCompanyStats()
    {
		  $this->load->model('Admin_model');


        $this->data['gr']       = $this->api->getClientsByMonth($this->companyid);
        $this->data['serv']     = $this->api->getSubscriptionActiveByMonth($this->companyid);
        $this->data['stats']    = $this->api->getStatistic($this->companyid);
        $this->data['dtt']      = 'clients.js';
        $this->data['title']    = "UnitedMvno Dasboard";
        if ($this->data['setting']->mage_invoicing) {
            $this->load->library('magebo', array(
                'companyid' => $this->companyid
				));
				$this->data['invoices'] = $this->api->getInvoicebyMonth($this->companyid);
            $this->data['invoicesum'] = $this->magebo->getAvarageInvoice();
        }
        echo json_encode($this->data);
    }
    public function searchProforma()
    {
        $q = $this->db->query("select a.*,b.mageboid,b.mvno_id,concat(b.firstname,' ', b.lastname) as cName from a_invoices a left join a_clients b on b.id=a.userid where a.companyid=?", array(
            $this->uri->segment(5)
        ));
        if ($q->num_rows() > 0) {
            $c['result'] = true;
            $c['data']   = $q->row_array();
        } else {
            $c['result'] = false;
            $c['data']   = '';
        }
        echo json_encode($c);
    }
    public function searchInvoice()
    {
        if ($this->companyid == 2) {
            $companyid = array(
                1,
                2,
                3,
                4,
                6,
                8,
                9,
                25,
                26,
                28,
                29,
                31,
                34,
                35,
                37,
                41,
                47,
                50
            );
        } else {
            $companyid[] = $this->companyid;
        }
        if ($this->api->IsAllowedInvoice($this->uri->segment(5), $companyid)) {
            $invoice = $this->api->getInvoice($this->uri->segment(5));
            if ($invoice) {
                $invoices['result'] = true;
                $invoices['data']   = $invoice;
            } else {
                $invoices['result'] = false;
                $invoices['data']   = '';
            }
        } else {
            $invoices['result']  = false;
            $invoices['data']    = '';
            $invoices['message'] = 'Not allowed';
        }
        echo json_encode($invoices);
    }
    public function searchCustomer()
    {
        $q = $this->db->query("select * from a_clients where mvno_id=? and companyid=?", array(
            $this->uri->segment(5),
            $this->companyid
        ));
        if ($q->num_rows() > 0) {
            $c['result'] = true;
            $c['data']   = $q->row_array();
        } else {
            $c['result'] = false;
            $c['data']   = '';
        }
        echo json_encode($c);
    }
    public function getInvoice()
    {
        $id = $this->uri->segment(5);
        if ($this->companyid == 2) {
            $companyid = array(
                1,
                2,
                3,
                4,
                6,
                8,
                9,
                25,
                26,
                28,
                29,
                31,
                34,
                35,
                37,
                41,
                47,
                50
            );
        } else {
            $companyid[] = $this->companyid;
        }
        if ($this->api->IsAllowedInvoice($id, $companyid)) {
            $this->load->library('magebo', array(
                $this->companyid
            ));
            $res           = $this->magebo->getInvoiceId($id);
            $res['userid'] = getWhmcsid($res['iAddressNbr']);
            echo json_encode(array(
                'result' => true,
                'data' => $res
            ));
        } else {
            echo json_encode(array(
                'result' => false
            ));
        }
    }
    /*public function getServices()
    {
    $limit     = 10;
    $page      = $_GET['page'];
    $off       = $page - 1;
    $offset    = $off + $limit;
    $order     = $_GET['orderby'];
    $orderType = $_GET['ordertype'];
    if ($_GET['terms'] == "undefined") {
    $keywords = '';
    } else {
    $keywords = $_GET['terms'];
    }
    $type      = $_GET['type'];
    $packageid = $_GET['packageid'];
    $status    = $_GET['status'];
    if ($page > 1) {
    $limit = "10, " . $offset;
    } else {
    $limit = "10";
    }
    if ($order == "id") {
    $orderby = " a.id " . $orderType;
    } elseif ($order == "domain") {
    $orderby = " a.id " . $orderType;
    } elseif ($order == "packagename") {
    $orderby = " b.name " . $orderType;
    } elseif ($order == "date") {
    $orderby = " a.date_contract " . $orderType;
    } elseif ($order == "customername") {
    $orderby = " c.firstname " . $orderType;
    } else {
    $orderby = " " . $order . " " . $orderType;
    }
    $total = $this->api->getservicetotal($this->companyid, $status, $keywords, $packageid);
    if (!empty($keywords)) {
    $keywords = "%" . $keywords . "%";
    $where    = " AND (a.id like " . $keywords . " OR d.msisdn like " . $keywords . " OR d.msisdn_sn like " . $keywords . "  OR d.msisdn_sim like " . $keywords . "  OR e.circuitid like " . $keywords . "  OR e.proximus_orderid like " . $keywords . " ) ";
    } else {
    $where = "";
    }
    if ($type == "id") {
    $where = " and a.id like '%" . $keywords . "'";
    } elseif ($order == "domain") {
    $where = " and a.id like '%" . $keywords . "'";
    } elseif ($order == "packagename") {
    $where = " and b.name like '%" . $keywords . "'";
    } elseif ($order == "date") {
    $where = " and a.date_contract like '%" . $keywords . "'";
    } elseif ($order == "customername") {
    $where = " and c.firstname like '%" . $keywords . "'";
    } else {
    $where = " ";
    }
    if ($status != '') {
    if ($this->uri->segment(5) == "pendingactivation") {
    $sql = "select a.id,a.status,a.userid,round(a.recurring , 2) as recurring,a.billingcycle, a.date_contract,a.date_created,concat(c.firstname,' ', c.lastname, ' ', companyname)  as clientname, b.name as packagename,a.companyid,
    case when a.type  = 'mobile' THEN d.msisdn
    when a.type = 'xdsl' then e.circuitid
    when a.type = 'voip' then f.cli
    else 'Unknown' end as domain, case when a.type  = 'mobile' THEN d.msisdn_status
    when a.type = 'xdsl' then e.status
    when a.type = 'voip' then f.status
    else 'Unknown' end as orderstatus, (select z.status from a_invoiceitems v left join a_invoices z on z.id=v.invoiceid where v.serviceid=a.id limit 1) as invoicestatus
    from a_services a
    left join a_products b on b.id=a.packageid
    left join a_clients c on c.id=a.userid
    left join a_services_mobile d on d.serviceid=a.id
    left join a_services_xdsl e on e.serviceid=a.id
    left join a_services_voip f on f.serviceid=a.id
    where a.status = 'Pending'
    and a.companyid = ?
    " . $where . "
    group by a.id order by " . $orderby . "  limit " . $limit;
    $q   = $this->db->query($sql, array(
    $this->companyid
    ));
    } elseif ($this->uri->segment(5) == "NotNew") {
    $sql = "select a.id,a.status,a.userid,round(a.recurring , 2) as recurring,a.billingcycle,a.date_contract,a.date_created,concat(c.firstname,' ', c.lastname, ' ', companyname)  as clientname, b.name as packagename,a.companyid,
    case when a.type  = 'mobile' THEN d.msisdn
    when a.type = 'xdsl' then e.circuitid
    when a.type = 'voip' then f.cli
    else 'Unknown' end as domain, case when a.type  = 'mobile' THEN d.msisdn_status
    when a.type = 'xdsl' then e.status
    when a.type = 'voip' then f.status
    else 'Unknown' end as orderstatus, (select z.status from a_invoiceitems v left join a_invoices z on z.id=v.invoiceid where v.serviceid=a.id limit 1) as invoicestatus
    from a_services a
    left join a_products b on b.id=a.packageid
    left join a_clients c on c.id=a.userid
    left join a_services_mobile d on d.serviceid=a.id
    left join a_services_xdsl e on e.serviceid=a.id
    left join a_services_voip f on f.serviceid=a.id
    where a.status not in ('New','Cancelled','Terminated')
    and a.companyid = ?
    " . $where . "
    group by a.id order by " . $orderby . "  limit " . $limit;
    $q   = $this->db->query($sql, array(
    $this->companyid
    ));
    } elseif ($this->uri->segment(5) == "cancelled") {
    $sql = "select a.id,a.status,a.userid,round(a.recurring , 2) as recurring,a.billingcycle,a.date_contract,a.date_created,concat(c.firstname,' ', c.lastname, ' ', companyname)  as clientname, b.name as packagename,a.companyid,
    case when a.type  = 'mobile' THEN d.msisdn
    when a.type = 'xdsl' then e.circuitid
    when a.type = 'voip' then f.cli
    else 'Unknown' end as domain, case when a.type  = 'mobile' THEN d.msisdn_status
    when a.type = 'xdsl' then e.status
    when a.type = 'voip' then f.status
    else 'Unknown' end as orderstatus, (select z.status from a_invoiceitems v left join a_invoices z on z.id=v.invoiceid where v.serviceid=a.id limit 1) as invoicestatus
    from a_services a
    left join a_products b on b.id=a.packageid
    left join a_clients c on c.id=a.userid
    left join a_services_mobile d on d.serviceid=a.id
    left join a_services_xdsl e on e.serviceid=a.id
    left join a_services_voip f on f.serviceid=a.id
    where a.status in ('Cancelled','Terminated')
    and a.companyid = ?
    " . $where . "
    group by a.id order by " . $orderby . "  limit " . $limit;
    $q   = $this->db->query($sql, array(
    $this->companyid
    ));
    } elseif ($this->uri->segment(5) == "needactions") {
    $sql = "select a.id,a.status,a.userid,round(a.recurring , 2) as recurring,a.billingcycle,a.date_contract,a.date_created,concat(c.firstname,' ', c.lastname, ' ', companyname)  as clientname, b.name as packagename,a.companyid,
    case when a.type  = 'mobile' THEN d.msisdn
    when a.type = 'xdsl' then e.circuitid
    when a.type = 'voip' then f.cli
    else 'Unknown' end as domain, case when a.type  = 'mobile' THEN d.msisdn_status
    when a.type = 'xdsl' then e.status
    when a.type = 'voip' then f.status
    else 'Unknown' end as orderstatus, (select z.status from a_invoiceitems v left join a_invoices z on z.id=v.invoiceid where v.serviceid=a.id limit 1) as invoicestatus
    from a_services a
    left join a_products b on b.id=a.packageid
    left join a_clients c on c.id=a.userid
    left join a_services_mobile d on d.serviceid=a.id
    left join a_services_xdsl e on e.serviceid=a.id
    left join a_services_voip f on f.serviceid=a.id
    where a.status in ('Cancelled','Terminated')
    and a.companyid = ?
    " . $where . "
    group by a.id order by " . $orderby . "  limit " . $limit;
    $q   = $this->db->query($sql, array(
    $this->companyid
    ));
    } else {
    $sql = "select a.id,a.status as orderstatus,a.userid,round(a.recurring , 2) as recurring,a.billingcycle, a.date_contract,a.date_created,concat(c.firstname,' ', c.lastname, ' ', companyname)  as clientname, b.name as packagename,a.companyid,
    case when a.type  = 'mobile' THEN d.msisdn
    when a.type = 'xdsl' then e.circuitid
    when a.type = 'voip' then f.cli
    else 'Unknown' end as domain,
    case when a.type  = 'mobile' THEN d.msisdn_status
    when a.type = 'xdsl' then e.status
    when a.type = 'voip' then f.status
    else 'Unknown' end as status, (select z.status from a_invoiceitems v left join a_invoices z on z.id=v.invoiceid where v.serviceid=a.id limit 1) as invoicestatus
    from a_services a
    left join a_products b on b.id=a.packageid
    left join a_clients c on c.id=a.userid
    left join a_services_mobile d on d.serviceid=a.id
    left join a_services_xdsl e on e.serviceid=a.id
    left join a_services_voip f on f.serviceid=a.id
    where a.status = ?
    and a.companyid = ?
    " . $where . "
    group by a.id order by " . $orderby . "  limit " . $limit;
    $q   = $this->db->query($sql, array(
    $status,
    $this->companyid
    ));
    }
    } else {
    $sql = "select a.id,a.status,a.userid,round(a.recurring , 2) as recurring,a.billingcycle, a.date_contract,a.date_created,concat(c.firstname,' ', c.lastname, ' ', companyname)  as clientname, b.name as packagename,a.companyid,
    case when a.type  = 'mobile' THEN d.msisdn
    when a.type = 'xdsl' then e.circuitid
    when a.type = 'voip' then f.cli
    else 'Unknown' end as domain, case when a.type  = 'mobile' THEN d.msisdn_status
    when a.type = 'xdsl' then e.status
    when a.type = 'voip' then f.status
    else 'Unknown' end as orderstatus
    from a_services a
    left join a_products b on b.id=a.packageid
    left join a_clients c on c.id=a.userid
    left join a_services_mobile d on d.serviceid=a.id
    left join a_services_xdsl e on e.serviceid=a.id
    left join a_services_voip f on f.serviceid=a.id, (select z.status from a_invoiceitems v left join a_invoices z on z.id=v.invoiceid where v.serviceid=a.id limit 1) as invoicestatus
    where a.status in ('Pending','Active')
    and a.companyid = ?
    " . $where . "
    group by a.id order by " . $orderby . "  limit " . $limit;
    $q   = $this->db->query($sql, array(
    $this->companyid
    ));
    }
    $total = 200;
    if ($q->num_rows() > 0) {
    foreach ($q->result_array() as $row) {
    $p                    = explode('-', $row['date_contract']);
    $row['date_contract'] = $p[2] . '-' . $p[0] . '-' . $p[1];
    $result[]             = $row;
    }
    echo json_encode(array(
    'per_page' => 10,
    'total_pages' => round($total / 10),
    'total' => $total,
    'services' => $result
    ));
    } else {
    echo json_encode(array(
    'per_page' => 10,
    'total_pages' => 0,
    'total' => 0,
    'services' => array()
    ));
    }
    }
    */
    public function login()
    {
        $this->load->model('Auth_model');
        if (!empty($this->dpost['username']) && !empty($this->dpost['password'])) {
            if (isPost()) {
                $valid = $this->Auth_model->admin_auth($this->dpost);
                unset($valid['admin']['password']);
                $valid['validity']     = time() + 3600;
                //$valid['ip'] =$_SERVER['REMOTE_ADDR'];
                $valid['socket_token'] = $this->encryption->encrypt($this->companyid . '#' . $valid['admin']['id'] . "#" . $valid['username'] . '#' . time());
                $valid['token']        = $this->encryption->encrypt(json_encode(array(
                    'companyid' => $this->companyid,
                    'adminid' => $valid['admin']['id'],
                    'email' => $valid['admin']['email'],
                    'ip' => $_SERVER['REMOTE_ADDR']
                )));
                echo json_encode($valid);
            }
        } else {
            echo json_encode(array(
                'result' => false,
                'message' => 'Username & Password my not be empty'
            ));
        }
    }
    public function addclient()
    {
        $this->dpost['companyid']    = $this->companyid;
        $this->dpost['date_created'] = date('Y-m-d');
        $this->dpost['uuid']         = guidv4();
        $password                    = random_string('alnum', 10);
        $this->dpost['password']     = password_hash($password, PASSWORD_DEFAULT);
        $this->dpost['notes']        = 'Created via UnitedPortal V2 ' . $this->admin_name;
        if ($this->data['setting']->mvno_id_increment == 1) {
            $this->dpost['mvno_id'] = genMvnoId($this->companyid);
        } else {
            $this->dpost['mvno_id'] = strtoupper(trim($this->dpost['mvno_id']));
        }
        if ($this->dpost['country'] == "NL") {
            $pcode = explode(' ', trim($this->dpost["postcode"]));
            if (count($pcode) != 2) {
                //$this->session->set_userdata('registration', $_POST);
                echo json_encode(array(
                    'result' => false,
                    'message' => 'Your POST CODE: ' . $this->dpost['postcode'] . ' is not valid example:  NNNN XX formaat'
                ));
                exit;
            } else {
                if (strlen($pcode[0]) != 4) {
                    // $this->session->set_userdata('registration', $_POST);
                    echo json_encode(array(
                        'result' => false,
                        'message' => 'Your POST CODE: ' . $this->dpost['postcode'] . ' is not valid example:  NNNN XX formaat'
                    ));
                    exit;
                } elseif (strlen($pcode[1]) != 2) {
                    //$this->session->set_userdata('registration', $_POST);
                    echo json_encode(array(
                        'result' => false,
                        'message' => 'Your POST CODE: ' . $this->dpost['postcode'] . ' is not valid example:  NNNN XX formaat'
                    ));
                    exit;
                }
            }
        } elseif ($_POST['country'] == "BE") {
            $pcode = explode(' ', 'BE ', trim($this->dpost["postcode"]));
        } else {
            $pcode = trim($this->dpost["postcode"]);
        }
        if ($this->data['setting']->mage_invoicing == "1") {
            /* Create Magboid */
            $this->load->library('magebo', array(
                'companyid' => $this->companyid
            ));
            $magebo = $this->magebo->AddClient($this->dpost);
            if ($magebo->result == "success") {
                $this->dpost['mageboid'] = $magebo->iAddressNbr;
            } else {
                echo json_encode($magebo);
                exit;
            }
        }
        /* Create Arta ContactYpeID */
        if ($this->data['setting']->create_arta_contact == 1) {
            if ($this->dpost['agentid'] != "-1") {
                $agentid = getContactTypeid($this->dpost['agentid']);
            } else {
                $agentid = 7;
            }
            $this->load->library('artilium', array(
                'companyid' => $this->companyid
            ));
            sendlog('CreateContact', array(
                'agentid' => $agentid,
                'post' => $this->dpost
            ));
            $cont = $this->artilium->CreateContact($this->dpost, $agentid);
            if ($cont) {
                $this->dpost['ContactType2Id'] = $cont;
            }
        }
        if (!$this->api->isEmailexisit(trim($data['email']))) {
            $id = $this->api->addClient($this->dpost);
        } else {
            $m = 'error while adding customer';
        }
        if ($id) {
            $this->sendEmailTemplate($this->companyid, 'signup_email', $id, array(
                'email' => $this->dpost['email'],
                'password' => $password
            ));
            echo json_encode(array(
                'result' => true,
                'userid' => $id
            ));
            exit;
        } else {
            $m = "Error generating customer id";
        }
        echo json_encode(array(
            'result' => false,
            'message' => $m
        ));
    }
    public function getclient()
    {
        $client = $this->api->getClient($this->companyid, $this->uri->segment(5));
        // $this->load->library('magebo', array('companyid' => $this->companyid));
        if ($this->data['setting']->mage_invoicing) {
            if ($client['paymentmethod'] == "directdebit") {
                $this->data['financial'] = $this->api->getMandateId($client->mageboid);
            }
            $this->load->library('magebo', array(
                'companyid' => $this->companyid
            ));
            $this->data['balance']      = $this->magebo->GetFinancialCondition($client['mageboid']);
            //$this->data['payments'] = $this->magebo->getPayments($client['mageboid']);
            $this->data['msubs']        = $this->api->getMSubscription($this->uri->segment(5));
            $this->data['nextInvoices'] = $this->api->getNextInvoices($client['mageboid']);
        } else {
            //$this->data['payments'] =false;
            $this->data['msubs']        = false;
            $this->data['nextInvoices'] = $false;
            $this->data['balance']      = "0.00";
        }
        $this->data['usage'] = $this->get_out_of_bundle_usage($this->uri->segment(5));
        //$this->data['setting'];
        echo json_encode(array_merge((array) $client, $this->data));
    }
    public function get_out_of_bundle_usage($userid)
    {
        $this->load->library('artilium', array(
            'companyid' => $this->companyid
        ));
        $total = '0.00';
        $this->load->library('xml');
        $usage                       = 0;
        $this->data['activemobiles'] = getMobileActiveCli($userid);
        if (empty($this->data['activemobiles'])) {
            $usage = 0;
        } else {
            $f   = array();
            $cdr = array();
            foreach ($this->data['activemobiles'] as $IN => $order) {
                if ($order['domainstatus'] == 'Active') {
                    $this->data["service" . $IN] = $this->api->getService($this->companyid, $order['id']);
                    $mobile                      = $this->data["service" . $IN]->details;
                    $f[]                         = array(
                        'domain' => $mobile->msisdn,
                        'sn' => $mobile->msisdn_sn,
                        'mobile' => $mobile
                    );
                    $cdr                         = $this->artilium->get_cdr($this->data["service" . $IN]);
                    $total                       = $cdr['total'] + $total;
                    unset($cdr);
                }
            }
            $usage = $total;
        }
        return $usage;
    }
    public function getService()
    {
        echo json_encode($this->api->getService($this->companyid, $this->uri->segment(5)));
    }
    public function getClientDetail()
    {
        $result   = array();
        $clientid = $this->uri->segment(5);
        $segment  = $this->uri->segment(6);
        if ($segment == "subscription") {
            $result = $this->api->getServices($clientid);
        } elseif ($segment == "invoice") {
            $result = $this->api->getInvoices($this->companyid);
        } elseif ($segment == "support") {
            $result = $this->api->getSupports($this->companyid, $this->dpost, $clientid);
        } elseif ($segment == "email") {
            $result = $this->api->getEmails($this->companyid, array(
                'id' => $clientid
            ));
        } elseif ($segment == "logs") {
            $result = $this->api->getLogs($this->companyid, array(
                'id' => $clientid
            ));
        } elseif ($segment == "payment") {
            $this->load->library('magebo', array(
                'companyid' => $this->companyid
            ));
            $result = $this->magebo->viewPayments(getiAddressNbr($clientid));
        } elseif ($segment == "contacts") {
            $result = $this->api->getContacts($this->companyid, array(
                'id' => $clientid
            ));
        } elseif ($segment == "notes") {
            $result = $this->api->getNotes($this->companyid, array(
                'id' => $clientid
            ));
        }
        echo json_encode($result);
    }
    public function searchSim()
    {
        $companyrange = getCompanyRange($this->companyid);
        $list1        = array();
        $keyword      = $this->dpost['sim'];
        $this->db     = $this->load->database('magebo', true);
        $query        = "SELECT TOP 10 cSIMCardNbr as name, cSIMCardNbr as value, iMSISDN as msisdn from tblC_SimCard where iPincode is NULL and iCompanyRangeNbr = ? and cSIMCardNbr like ? and iPincode IS NULL order by cSIMCardNbr DESC";
        $query1       = $this->db->query($query, array(
            $companyrange,
            '%' . $keyword . '%'
        ));
        if ($query1->num_rows() > 0) {
            $list1 = $query1->result_array();
        }
        echo json_encode($list1);
    }
    public function getStafOnline()
    {
        $now      = time() - 114000;
        $this->db = $this->load->database('default', true);
        $q        = $this->db->query("select  id,concat(firstname,' ',lastname) as name,username,picture,lastseen from  a_admin where companyid=? and lastseen >= ? and id != ? order by lastseen desc", array(
            $this->companyid,
            $now,
            $this->adminid
        ));
        if ($q->num_rows()) {
            foreach ($q->result_array() as $r) {
                if (time() - $r['lastseen'] <= 600) {
                    $r['status'] = "success";
                } else {
                    $r['status'] = "dark";
                }
                $result[] = $r;
            }
            // $this->peer_online($result);
        } else {
            $result = array();
        }
        echo json_encode($result);
    }
    public function peer_online($staf)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://socket.united-telecom.be/peer_online");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "message=" . json_encode($staf));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/x-www-form-urlencoded'
        ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        curl_close($ch);
    }
    public function getCompanyList()
    {
        $q = $this->db->query("select companyid,companyname,portal_url from a_mvno where status=?", array(
            1
        ));
        echo json_encode($q->result_array());
    }
    public function smartsearch()
    {
        $list1    = array();
        $list2    = array();
        $list3    = array();
        // $list4   = array();
        $keyword  = '%' . trim($this->dpost['keyword']) . '%';
        // echo json_encode( array(array('id' => 1,'name' =>"simson", "type"=> 'client')) );
        // exit;
        //exit;
        $this->db = $this->load->database('default', true);
        $query1   = $this->db->query("SELECT 'client' as type, id as value, concat('Client #' ,mageboid,', ',firstname,' ',lastname, ' ', companyname,', ', mvno_id) as name, companyname FROM a_clients where (firstname like ? or lastname like ? or companyname like ? or id like ? or address1 like ? or postcode like ? or phonenumber like ? or mvno_id LIKE ? or mageboid LIKE ? or email like ? or iban like ?) and companyid=? group by id order by firstname DESC LIMIT 0, 40", array(
            $keyword,
            $keyword,
            $keyword,
            $keyword,
            $keyword,
            $keyword,
            $keyword,
            $keyword,
            $keyword,
            $keyword,
            $keyword,
            $this->companyid
        ));
        $query2   = $this->db->query("SELECT 'service' as type, a.serviceid as value,concat('Mobile # ',a.msisdn,' :',b.firstname,' ',b.lastname) as name, concat(b.firstname,' ',b.lastname) as companyname from a_services_mobile a left join a_clients b on b.id=a.userid where (a.id like ? or a.msisdn like ? or a.msisdn_sn like ? or a.msisdn_sim like ? or a.serviceid like ?) and a.companyid=?  group by a.serviceid order by a.id DESC LIMIT 0, 40", array(
            $keyword,
            $keyword,
            $keyword,
            $keyword,
            $keyword,
            $this->companyid
        ));
        $query3   = $this->db->query("SELECT 'service' as type, a.serviceid as value,concat('XDSL #', a.serviceid,' ',a.circuitid) as name, serviceid as companyname from a_services_xdsl a left join a_clients b on b.id=a.userid where a.circuitid like ? and a.companyid= ? group by a.serviceid order by a.serviceid DESC LIMIT 0, 40", array(
            $keyword,
            $this->companyid
        ));
        if ($query1->num_rows() > 0) {
            $list1 = $query1->result_array();
        }
        if ($query2->num_rows() > 0) {
            $list2 = $query2->result_array();
        }
        if ($query3->num_rows() > 0) {
            $list3 = $query3->result_array();
        }
        echo json_encode(array_merge($list1, $list2, $list3));
    }
    public function get_pending_orders()
    {
        $limit     = 10;
        $page      = $_GET['page'];
        $off       = $page - 1;
        $offset    = $off + $limit;
        $order     = $_GET['order'];
        $orderType = $_GET['ordertype'];
        $keywords  = $_GET['terms'];
        $type      = $_GET['type'];
        $total     = $this->api->getclientotal($this->companyid, $keywords, $type);
        if ($page > 1) {
            $limit = "10, " . $offset;
        } else {
            $limit = "10";
        }
        if ($order == "customername") {
            $orderby = "firstname " . $orderType;
        } else {
            $orderby = $order . " " . $orderType;
        }
        if (!empty($keywords)) {
            $keywords = "%" . $keywords . "%";
            $where    = " AND (a.id like " . $keywords . " OR d.msisdn like " . $keywords . " OR d.msisdn_sn like " . $keywords . "  OR d.msisdn_sim like " . $keywords . "  OR e.circuitid like " . $keywords . "  OR e.proximus_orderid like " . $keywords . " ) ";
        } else {
            $where = "";
        }
        if ($packageid > 0) {
            $where .= " AND a.packageid= " . $packageid . " ";
        }
        if ($type != '') {
            $where .= " AND a.type= " . $type . " ";
        }
        if (!empty($keywords)) {
            if ($type != "all") {
                $sql = "select a.companyid,c.nationalnr,a.id,a.status,a.userid,round(a.recurring , 2) as recurring,a.billingcycle,a.date_created,concat(c.firstname,' ', c.lastname, ' ', companyname)  as clientname, b.name as packagename,
       case when a.type  = 'mobile' THEN d.msisdn
       when a.type = 'xdsl' then e.circuitid
       when a.type = 'voip' then f.cli
       else 'Unknown' end as domain,
        case when a.type  = 'mobile'  THEN d.msisdn_status
       when a.type = 'xdsl'  then e.status
       when a.type = 'voip'  then f.status
       else 'Unknown' end as order_status,
       (select z.status from a_invoiceitems v left join a_invoices z on z.id=v.invoiceid where v.serviceid=a.id limit 1) as invoicestatus
    from a_services a
    left join a_products b on b.id=a.packageid
    left join a_clients c on c.id=a.userid
    left join a_services_mobile d on d.serviceid=a.id
    left join a_services_xdsl e on d.serviceid=a.id
    left join a_services_voip f on d.serviceid=a.id
    where a.status ='New'
    and a.companyid = ?
    " . $where . "
    " . $orderby;
                $q   = $this->db->query($sql, array(
                    $this->companyid,
                    '%' . $keywords . '%'
                ));
            } else {
                $keywords = '%' . $keywords . '%';
                $sql      = "SELECT id,mvno_id,CONCAT(firstname, '.', lastname) AS customername,mageboid,paymentmethod,agentid,email,phonenumber FROM a_clients WHERE companyid LIKE ? AND ( firstname LIKE ? OR lastname LIKE ?  OR companyname LIKE ? OR email LIKE ? OR mageboid LIKE ?  OR mvno_id LIKE ? OR id LIKE ? OR address1 LIKE ? ) order by " . $orderby . " limit " . $limit;
                $q        = $this->db->query($sql, array(
                    $this->companyid,
                    $keywords,
                    $keywords,
                    $keywords,
                    $keywords,
                    $keywords,
                    $keywords,
                    $keywords,
                    $keywords
                ));
            }
        } else {
            $sql = "SELECT id,mvno_id,CONCAT(firstname, '.', lastname) AS customername,mageboid,paymentmethod,agentid,email,phonenumber FROM a_clients WHERE companyid = ? order by " . $orderby . " limit " . $limit;
            $q   = $this->db->query($sql, array(
                $this->companyid
            ));
        }
        if ($q->num_rows() > 0) {
            echo json_encode(array(
                'per_page' => 10,
                'total_pages' => round($total / 10),
                'total' => $total,
                'clients' => $q->result_array()
            ));
        } else {
            echo json_encode(array(
                'per_page' => 10,
                'total_pages' => 0,
                'total' => 0,
                'clients' => array(),
                'query' => $sql
            ));
        }
        $result = array();
        $q      = $this->db->query("select a.companyid,c.nationalnr,a.id,a.status,a.userid,round(a.recurring , 2) as recurring,a.billingcycle,a.date_created,concat(c.firstname,' ', c.lastname, ' ', companyname)  as clientname, b.name as packagename,
       case when a.type  = 'mobile' THEN d.msisdn
       when a.type = 'xdsl' then e.circuitid
       when a.type = 'voip' then f.cli
       else 'Unknown' end as domain,
        case when a.type  = 'mobile'  THEN d.msisdn_status
       when a.type = 'xdsl'  then e.status
       when a.type = 'voip'  then f.status
       else 'Unknown' end as order_status,
       (select z.status from a_invoiceitems v left join a_invoices z on z.id=v.invoiceid where v.serviceid=a.id limit 1) as invoicestatus
    from a_services a
    left join a_products b on b.id=a.packageid
    left join a_clients c on c.id=a.userid
    left join a_services_mobile d on d.serviceid=a.id
    left join a_services_xdsl e on d.serviceid=a.id
    left join a_services_voip f on d.serviceid=a.id
    where a.status ='New'
    and a.companyid = ?
    group by a.id", array(
            $this->companyid
        ));
        if ($q->num_rows() > 0) {
            $result = $q->result_array();
        }
        echo json_encode($result);
    }
    public function getAgents()
    {
        error_reporting(1);
        echo json_encode($this->api->getAgents($this->companyid));
    }
    public function Addnote()
    {
        $this->dpost['companyid'] = $this->companyid;
        $this->dpost['created']   = date('Y-m-d H:i:s');
        echo json_encode($this->api->addNote($this->dpost));
        //echo json_encode($this->dpost);
    }
    public function resetPassword()
    {
        $this->load->helper('string');
        $client = (object) $this->api->getClient($this->companyid, $this->dpost['userid']);
        $this->config->set_item('language', $client->language);
        if ($client) {
            $password = random_string('alnum', 10);
            $this->db = $this->load->database('default', true);
            $this->db->where('id', $this->dpost['userid']);
            $this->db->where('companyid', $this->companyid);
            $this->db->update('a_clients', array(
                'password' => password_hash($password, PASSWORD_DEFAULT)
            ));
            if ($this->data['setting']->smtp_type == 'smtp') {
                $config = array(
                    'protocol' => 'smtp',
                    'smtp_host' => $this->data['setting']->smtp_host,
                    'smtp_port' => $this->data['setting']->smtp_port,
                    'smtp_user' => $this->data['setting']->smtp_user,
                    'smtp_pass' => $this->encryption->decrypt($this->data['setting']->smtp_pass),
                    'mailtype' => 'html',
                    'charset' => 'utf-8',
                    'smtp_auth' => true,
                    'wordwrap' => true
                );
            } else {
                $config['protocol'] = 'sendmail';
                $config['mailpath'] = '/usr/sbin/sendmail';
                $config['mailtype'] = 'html';
                $config['charset']  = 'utf-8';
                $config['wordwrap'] = true;
            }
            $this->email->clear();
            $this->email->initialize($config);
            $this->data['language'] = "dutch";
            $this->data['info']     = (object) array(
                'email' => $client->email,
                'password' => $password,
                'name' => $client->firstname . ' ' . $client->lastname
            );
            $body                   = getMailContent('signup_email', $client->language, $client->companyid);
            $body                   = str_replace('{$name}', $client->firstname . ' ' . $client->lastname, $body);
            $body                   = str_replace('{$password}', $password, $body);
            $body                   = str_replace('{$email}', trim(strtolower($client->email)), $body);
            $body                   = str_replace('{$base_url}', url_to_domain(base_url()), $body);
            $subject                = getSubject('signup_email', $client->language, $client->companyid);
            $this->email->set_newline("\r\n");
            $this->email->from($this->data['setting']->smtp_sender, $this->data['setting']->smtp_name);
            $this->email->to($client->email);
            $this->email->subject(getSubject('signup_email', $client->language, $client->companyid));
            $this->email->message($body);
            if ($this->email->send()) {
                logEmailOut(array(
                    'userid' => $client->id,
                    'companyid' => $client->companyid,
                    'to' => $client->email,
                    'subject' => getSubject('signup_email', $client->language, $client->companyid),
                    'message' => $body
                ));
                logAdmin(array(
                    'companyid' => $this->companyid,
                    'userid' => $client->id,
                    'user' => $this->dpost['admin'],
                    'ip' => $_SERVER['REMOTE_ADDR'],
                    'description' => 'Changing customer password by reset button'
                ));
                echo json_encode(array(
                    'result' => true
                ));
            } else {
                logEmailOut(array(
                    'userid' => $client->id,
                    'companyid' => $client->companyid,
                    'to' => $client->email,
                    'subject' => getSubject('signup_email', $client->language, $client->companyid),
                    'message' => $body,
                    'status' => 'not_sent',
                    'error_message' => $this->email->print_debugger()
                ));
                echo json_encode(array(
                    'result' => false,
                    'error' => $this->email->print_debugger()
                ));
            }
        } else {
            echo json_encode(array(
                'result' => false,
                'error' => lang('Client not found')
            ));
        }
    }
    public function checkIban()
    {
        error_reporting(1);
        echo json_encode($this->api->getBic($this->dpost['iban']));
    }
    public function SepaUpdate()
    {
        $this->load->library('magebo', array(
            'companyid' => $this->companyid
        ));
        if (!empty($this->dpost['iban']) && !empty($this->dpost['userid'])) {
            $client = (object) $this->api->getClient($this->companyid, $this->dpost['userid']);
            $bic    = $this->api->getBic(trim($this->dpost['iban']));
            if (iban_block($this->companyid, trim($this->dpost['iban']))) {
                echo json_encode(array(
                    'result' => false,
                    'message' => 'You may not use this IBAN'
                ));
                exit;
            } else {
                /*
                if (!$bic->valid) {
                if (!empty($this->dpost['bic'])) {

                } else {
                echo json_encode(array('result' => false, 'message'=>'We can not detect BIC please provide it, error code: 012'));
                exit;
                }

                } else {
                */
                if (!empty($this->dpost['bic'])) {
                } else {
                    if (!empty($bic->bankData->bic)) {
                        $this->dpost['bic'] = $bic->bankData->bic;
                    } else {
                        $bc = $this->api->getBicTry(trim($this->dpost['iban']));
                        if (!empty($bc->result->data->swift_code)) {
                            $this->dpost['bic'] = $bc->result->data->swift_code;
                        }
                    }
                }
                if (empty($this->dpost['bic'])) {
                    echo json_encode(array(
                        'result' => true,
                        'message' => 'Your IBAN SEEM INVALID please provide BIC and try again'
                    ));
                    exit;
                }
                if ($client) {
                    if ($this->dpost['sepa_type'] == "") {
                        echo json_encode(array(
                            'result' => true,
                            'message' => 'You need to choose AMEND or NEW'
                        ));
                        exit;
                    } else {
                        if ($this->dpost['sepa_type'] == "new") {
                            $this->magebo->deleteIban($client->mageboid);
                            $this->magebo->insertIban($client->mageboid, array(
                                'iban' => strtoupper(str_replace(' ', '', trim($this->dpost['iban']))),
                                'bic' => trim($this->dpost['bic'])
                            ));
                            $this->magebo->addMageboSEPA(strtoupper(str_replace(' ', '', trim($this->dpost['iban']))), trim($this->dpost['bic']), $client->mageboid, $this->dpost['mandateid']);
                        } else {
                            $this->magebo->amendMageboSEPA(strtoupper(str_replace(' ', '', trim($this->dpost['iban']))), trim($this->dpost['bic']), $client->mageboid);
                        }
                        $this->api->update_sepa($this->dpost);
                        logAdmin(array(
                            'companyid' => $this->companyid,
                            'userid' => $client->id,
                            'user' => $this->dpost['admin'],
                            'ip' => $_SERVER['REMOTE_ADDR'],
                            'description' => 'Execute Sepa Amendement for  ' . $client->mageboid . ' IBAN: ' . $this->dpost['iban']
                        ));
                    }
                    echo json_encode(array(
                        'result' => true,
                        'message' => 'Sepa has been amended'
                    ));
                    exit;
                } else {
                    echo json_encode(array(
                        'result' => false,
                        'message' => 'Billing Id cant be found'
                    ));
                    exit;
                }
                //}
            }
        } else {
            echo json_encode(array(
                'result' => false,
                'message' => 'Please check your data'
            ));
            exit;
        }
    }
    public function addContact()
    {
        $this->db = $this->load->database('default', true);
        $admin    = $this->dpost['admin'];
        unset($this->dpost['admin']);
        $this->data['companyid'] = $this->companyid;
        $this->db->insert('a_clients_contacts', $this->dpost);
        $id = $this->db->insert_id();
        if ($id > 0) {
            logAdmin(array(
                'companyid' => $this->companyid,
                'userid' => $this->dpost['userid'],
                'user' => $this->dpost['admin'],
                'ip' => $_SERVER['REMOTE_ADDR'],
                'description' => 'Added New Contact ID:' . $id
            ));
            echo json_encode(array(
                'result' => true,
                'contactid' => $id
            ));
        } else {
            echo json_encode(array(
                'result' => false,
                'message' => lang('Contact was not inserted'),
                'data' => $this->dpost
            ));
        }
    }
    public function updateClient()
    {
        echo json_encode($this->api->UpdateClient($this->companyid, $this->dpost));
        //echo json_encode($this->dpost);
    }
    public function sendEmail()
    {
        //echo json_encode($this->dpost);
        //exit;
        $this->data['setting'] = globofix($this->companyid);
        $client                = (object) $this->api->getClient($this->companyid, $this->dpost['userid']);
        $this->config->set_item('language', $client->language);
        if ($client) {
            if ($this->data['setting']->smtp_type == 'smtp') {
                $config = array(
                    'protocol' => 'smtp',
                    'smtp_host' => $this->data['setting']->smtp_host,
                    'smtp_port' => $this->data['setting']->smtp_port,
                    'smtp_user' => $this->data['setting']->smtp_user,
                    'smtp_pass' => $this->encryption->decrypt($this->data['setting']->smtp_pass),
                    'mailtype' => 'html',
                    'charset' => 'utf-8',
                    'starttls' => true,
                    'wordwrap' => true
                );
            } else {
                $config['protocol'] = 'sendmail';
                $config['mailpath'] = '/usr/sbin/sendmail';
                $config['mailtype'] = 'html';
                $config['charset']  = 'utf-8';
                $config['wordwrap'] = true;
            }
            $this->email->clear();
            $this->email->initialize($config);
            $body = getMailContentx(nl2br($this->dpost["message"]), $client->language, $client->companyid);
            $body = str_replace('{$base_url}', url_to_domain(base_url()), $body);
            $this->email->set_newline("\r\n");
            $this->email->from($this->data['setting']->smtp_sender, $this->data['setting']->smtp_name);
            $this->email->to(explode(',', $this->dpost["to"]));
            if (!empty($this->dpost['bcc'])) {
                $this->email->bcc(explode(',', $this->dpost['bcc']));
            }
            $this->email->subject($this->dpost["subject"]);
            $this->email->message($body);
            if ($this->email->send()) {
                logEmailOut(array(
                    'userid' => $client->id,
                    'companyid' => $client->companyid,
                    'to' => strtolower(trim($this->dpost["to"])),
                    'subject' => $this->dpost["subject"],
                    'message' => $body
                ));
                echo json_encode(array(
                    'result' => true,
                    'message' => 'email has been sent'
                ));
            } else {
                logEmailOut(array(
                    'userid' => $client->id,
                    'companyid' => $client->companyid,
                    'to' => strtolower(trim($this->dpost["to"])),
                    'subject' => $this->dpost["subject"],
                    'message' => $body,
                    'status' => 'error',
                    'error_message' => $this->email->print_debugger()
                ));
                echo json_encode(array(
                    'result' => false,
                    'message' => $this->email->print_debugger()
                ));
            }
        } else {
            echo json_encode(array(
                'result' => false,
                'message' => 'Client not found'
            ));
        }
    }
    /*
    select a.companyid,c.nationalnr,a.id,a.status,a.userid,round(a.recurring , 2) as recurring,a.billingcycle,a.date_created,concat(c.firstname,' ', c.lastname, ' ', companyname)  as clientname, b.name as packagename,
    case when a.type  = 'mobile' THEN d.msisdn
    when a.type = 'xdsl' then e.circuitid
    when a.type = 'voip' then f.cli
    else 'Unknown' end as domain,
    case when a.type  = 'mobile'  THEN d.msisdn_status
    when a.type = 'xdsl'  then e.status
    when a.type = 'voip'  then f.status
    else 'Unknown' end as order_status,
    (select z.status from a_invoiceitems v left join a_invoices z on z.id=v.invoiceid where v.serviceid=a.id limit 1) as invoicestatus
    from a_services a
    left join a_products b on b.id=a.packageid
    left join a_clients c on c.id=a.userid
    left join a_services_mobile d on d.serviceid=a.id
    left join a_services_xdsl e on d.serviceid=a.id
    left join a_services_voip f on d.serviceid=a.id
    where a.status ='New'
    and a.companyid = '$this->companyid'
    group by a.id

    */
    public function sendEmailTemplate($companyid, $templates, $userid, $data)
    {
        //echo json_encode($this->dpost);
        //exit;
        //$this->data['setting'] = globofix($this->companyid);
        $client = (object) $this->api->getClient($this->companyid, $userid);
        $this->config->set_item('language', $client->language);
        if ($client) {
            if ($this->data['setting']->smtp_type == 'smtp') {
                $config = array(
                    'protocol' => 'smtp',
                    'smtp_host' => $this->data['setting']->smtp_host,
                    'smtp_port' => $this->data['setting']->smtp_port,
                    'smtp_user' => $this->data['setting']->smtp_user,
                    'smtp_pass' => $this->encryption->decrypt($this->data['setting']->smtp_pass),
                    'mailtype' => 'html',
                    'charset' => 'utf-8',
                    'starttls' => true,
                    'wordwrap' => true
                );
            } else {
                $config['protocol'] = 'sendmail';
                $config['mailpath'] = '/usr/sbin/sendmail';
                $config['mailtype'] = 'html';
                $config['charset']  = 'utf-8';
                $config['wordwrap'] = true;
            }
            $body = getMailContent($template, $client->language, $client->companyid);
            // $body = $this->load->view('email/content', $this->data, true);
            $body = str_replace('{$name}', $client->firstname . ' ' . $client->lastname, $body);
            foreach ($data as $key => $val) {
                $body = str_replace('{$' . $key . '}', $val, $body);
            }
            $this->email->set_newline("\r\n");
            $this->email->from($this->data['setting']->smtp_sender, $this->data['setting']->smtp_name);
            $this->email->to(strtolower(trim($_POST['email'])));
            //$this->email->bcc('lainard@gmail.com');
            $this->email->subject(getSubject($template, $client->language, $client->companyid));
            $this->email->message($body);
            if ($this->email->send()) {
                $r = true;
                send_growl(array(
                    'message' => $this->admin_name . ' has just added new client with id: ' . $client->id,
                    'companyid' => $client->companyid
                ));
                logEmailOut(array(
                    'userid' => $client->id,
                    'companyid' => $client->companyid,
                    'to' => $client->email,
                    'subject' => getSubject($template, $client->language, $client->companyid),
                    'message' => $body
                ));
            } else {
                $r = false;
                logEmailOut(array(
                    'userid' => $client->id,
                    'companyid' => $client->companyid,
                    'to' => $client->email,
                    'subject' => getSubject($template, $client->language, $client->companyid),
                    'message' => $body,
                    'status' => 'error',
                    'error_message' => $this->email->print_debugger()
                ));
            }
            logAdmin(array(
                'companyid' => $this->companyid,
                'userid' => $client->id,
                'user' => $this->admin_name,
                'ip' => $_SERVER['REMOTE_ADDR'],
                'description' => 'Adding customer ' . $client->id
            ));
            return array(
                'result' => $r
            );
        } else {
            return array(
                'result' => false,
                'message' => 'Client not found'
            );
        }
    }
    /*
    select a.companyid,c.nationalnr,a.id,a.status,a.userid,round(a.recurring , 2) as recurring,a.billingcycle,a.date_created,concat(c.firstname,' ', c.lastname, ' ', companyname)  as clientname, b.name as packagename,
    case when a.type  = 'mobile' THEN d.msisdn
    when a.type = 'xdsl' then e.circuitid
    when a.type = 'voip' then f.cli
    else 'Unknown' end as domain,
    case when a.type  = 'mobile'  THEN d.msisdn_status
    when a.type = 'xdsl'  then e.status
    when a.type = 'voip'  then f.status
    else 'Unknown' end as order_status,
    (select z.status from a_invoiceitems v left join a_invoices z on z.id=v.invoiceid where v.serviceid=a.id limit 1) as invoicestatus
    from a_services a
    left join a_products b on b.id=a.packageid
    left join a_clients c on c.id=a.userid
    left join a_services_mobile d on d.serviceid=a.id
    left join a_services_xdsl e on d.serviceid=a.id
    left join a_services_voip f on d.serviceid=a.id
    where a.status ='New'
    and a.companyid = '$this->companyid'
    group by a.id

    */
    public function reject_mobile()
    {
        $order = $this->api->getService($this->companyid, $this->dpost['serviceid']);
        if ($order->companyid != $this->companyid) {
            echo json_encode(array(
                'result' => false,
                'message' => 'Unauthorized'
            ));
            exit;
        }
        $this->db = $this->load->database('default', true);
        $this->db->where('id', $this->dpost['serviceid']);
        $this->db->update('a_services', array(
            'status' => 'Cancelled',
            'reject_reason' => $this->dpost['reason']
        ));
        send_growl(array(
            'companyid' => $this->companyid,
            'message' => 'Order ' . $this->dpost['serviceid'] . ' has been rejected by: ' . $this->dpost['admin']
        ));
        logAdmin(array(
            'companyid' => $this->companyid,
            'userid' => $order->userid,
            'serviceid' => $order->id,
            'user' => $this->dpost['admin'],
            'ip' => $_SERVER['REMOTE_ADDR'],
            'description' => 'Service : ' . $this->dpost['serviceid'] . ' has been rejected'
        ));
        echo json_encode(array(
            'result' => true,
            'message' => 'Order has been rejected'
        ));
    }
    public function cancel_mobile()
    {
        $this->load->model('Admin_model');
        $order = $this->api->getService($this->companyid, $this->dpost['serviceid']);
        if ($order->companyid != $this->companyid) {
            echo json_encode(array(
                'result' => false,
                'message' => 'Unauthorized'
            ));
            exit;
        }
        $this->db = $this->load->database('default', true);
        $this->db->where('id', $this->dpost['serviceid']);
        $this->db->update('a_services', array(
           'status' => 'Cancelled',
           'reject_reason' => 'Cancelled via V2'
        ));
         $this->Admin_model->update_services_data('mobile', $this->dpost["serviceid"], array(
            'status' => 'Cancelled',
            'date_modified' => date('Y-m-d H:i:s')
         ));
        send_growl(array(
           'companyid' => $this->companyid,
           'message' => 'Order ' . $this->dpost['serviceid'] . ' has been Cancelled by: ' . $this->dpost['admin']
        ));
        logAdmin(array(
           'companyid' => $this->companyid,
           'userid' => $order->userid,
           'serviceid' => $order->id,
           'user' => $this->dpost['admin'],
           'ip' => $_SERVER['REMOTE_ADDR'],
           'description' => 'Service : ' . $this->dpost['serviceid'] . ' has been Cancelled'
        ));
        echo json_encode(array(
           'result' => true,
           'message' => 'Order has been rejected'
        ));
    }
    public function get_order_detail()
    {
        $this->db = $this->load->database('default', true);
        // header('Content-Type: application/json');
        //echo json_encode(array('id' => $this->uri->segment(5), 'cid' => $this->companyid));
        //exit;
        $id       = $this->uri->segment(5);
        $this->db = $this->load->database('default', true);
        $s        = $this->db->query("select a.*,b.msisdn_sim,b.msisdn_type,d.name as productname,concat(c.firstname,' ',c.lastname) as customername,c.mvno_id
            from a_services a
            left join a_services_mobile b on b.serviceid=a.id
            left join a_clients c on c.id=a.userid
            left join a_products d on d.id=a.packageid
            where a.id=? and a.companyid=?", array(
            $id,
            $this->companyid
        ));
        $contract = explode('-', $s->row()->date_contract);
        if ($s->num_rows() > 0) {
            $res                  = $s->row_array();
            $res['date_contract'] = $contract[2] . '-' . $contract[0] . '-' . $contract[1];
            echo json_encode($res);
        } else {
            echo array();
        }
    }
    public function arta_getPlatformLog()
    {
        $sn   = $this->uri->segment(5);
        $res  = array();
        $date = date('Y-m-d', strtotime('-30 day', strtotime(date('Y-m-d'))));
        $this->load->library('artilium', array(
            'companyid' => $this->companyid
        ));
        $sn = $this->artilium->ServiceLogging('GetLoggingBySn', array(
            'SN' => trim($sn),
            'From' => $date,
            'Till' => date('Y-m-d'),
            'MaxResults' => '100'
        ));
        foreach ($sn as $s) {
            $m = (array) $s;
            if (empty($s->Value)) {
                $m['Value'] = '';
                $res[]      = $m;
            }
        }
        echo json_encode($res);
    }
    public function arta_getCLI()
    {
        unset($this->data['setting']);
        $this->data['result'] = false;
        $sn                   = $this->uri->segment(5);
        if (!$sn) {
            echo json_encode($this->data);
            exit;
        }
        $this->load->library('artilium', array(
            'companyid' => $this->companyid
        ));
        $packaged  = $this->artilium->GetListPackageOptionsForSn($sn);
        $cli       = $this->artilium->getCLI($sn);
        $voicemail = '0';
        if ($cli) {
            $this->data['CountryGroup'] = $cli->CountryGroup;
            if ($cli->DestinationsAcceptedRejected === 'false') {
                $voicemail = '1';
            } else {
                $voicemail = '0';
            }
        }
        if ($packaged) {
            foreach ($packaged as $ee) {
                $rr                               = (array) $ee;
                $rr['CallModeDescriptionTooltip'] = lang(trim('tooltip :' . $rr['CallModeDescription']));
                $rr['CallModeDescription']        = lang(trim($rr['CallModeDescription']));
                $this->data['packages'][]         = $rr;
                unset($rr);
                unset($ee);
            }
        } else {
            $this->data['packages'] = array();
        }
        if (substr(trim($sn), 0, 2) == "31") {
            $this->data['packages'][] = array(
                'CallModeDescription' => lang("Voicemail"),
                'PackageDefinitionId' => '100000',
                'CallModeDescriptionTooltip' => lang(trim('tooltip :Voicemail')),
                'Available' => $voicemail,
                'cli' => $cli
            );
        }
        $xx = $this->artilium->GetBundleAssignList2($sn);
        if ($xx) {
            foreach ($xx as $x) {
                // if($x->ValidUntil <= date('Y-m-d')){
                $bd[] = $x;
                // }
            }
        } else {
            $bd = array();
        }
        $this->data['bundles'] = $bd;
        $sum                   = $this->artilium->GetSUMAssignmentList(trim($sn));
        // mail('mail@simson.one','undle',print_r($sum, true));
        if (count($sum) > 0) {
            foreach ($sum as $ow) {
                $row = (object) $ow;
                if (!empty($row->ActionSet->GetSumPlanActionSetsResult->Data->Id)) {
                    $data[] = $row->ActionSet->GetSumPlanActionSetsResult->Data;
                    $sm[]   = array(
                        'Ignored' => $row->Ignored,
                        'PlanId' => $row->PlanId,
                        'Name' => $row->Name,
                        'SumSetId' => $row->SumSetId,
                        'ValidFrom' => $row->ValidFrom,
                        'ValidUntil' => $row->ValidUntil,
                        'SumAssignmentId' => $row->SumAssignmentId,
                        'Data' => $data
                    );
                    unset($data);
                } else {
                    $data = $row->ActionSet->GetSumPlanActionSetsResult->Data;
                    $sm[] = array(
                        'Ignored' => $row->Ignored,
                        'PlanId' => $row->PlanId,
                        'Name' => $row->Name,
                        'SumSetId' => $row->SumSetId,
                        'ValidFrom' => $row->ValidFrom,
                        'ValidUntil' => $row->ValidUntil,
                        'SumAssignmentId' => $row->SumAssignmentId,
                        'Data' => $data
                    );
                }
            }
            $this->data['sum'] = $sm;
        }
        $this->data['result'] = true;
        echo json_encode($this->data);
    }
    public function arta_getCdr_in()
    {
        $this->load->library('artilium', array(
            'companyid' => $this->companyid
        ));
        $res            = array();
        $id             = $this->uri->segment(5);
        $order          = $this->api->getService($this->companyid, $id);
        $data           = $this->artilium->GetFullCDRListWithDetails($order->details->msisdn_sn);
        $count          = count($data);
        $c['cdr_count'] = $count;
        $result         = array();
        //  echo json_encode($data);
        if ($count > 0) {
            foreach ($data as $cdr) {
                $r['time']   = str_replace('+02:00', '', $cdr['Datetime']);
                $r['time']   = str_replace('T', ' ', $r['time']);
                $r['number'] = str_replace('316240', '31', $cdr['Destination']);
                if (substr($r['number'], 0, 2) == "00") {
                    $r['number'] = substr($r['number'], 2);
                }
                $r['cost']               = $this->data['setting']->currency . number_format($cdr['Price'], 2);
                $r['destinationcountry'] = '';
                if ($cdr['TrafficTypeId'] == "2") {
                    $r['type']     = "DATA";
                    $r['duration'] = '';
                } elseif ($cdr['TrafficTypeId'] == "1") {
                    $r['type']     = "VOICE";
                    $r['duration'] = sprintf('%02d:%02d:%02d', ($cdr['Volume'] / 3600), ($cdr['Volume'] / 60 % 60), $cdr['Volume'] % 60);
                } else {
                    $r['type']     = "SMS";
                    $r['duration'] = $cdr['Volume'];
                }
                $result[] = $r;
            }
        }
        echo json_encode(array(
            'cdr_count' => $c['cdr_count'],
            'cdr' => $result
        ));
    }
    public function arta_getCdr_out()
    {
        $mobile = $this->api->getService($this->companyid, $this->uri->segment(5));
        $this->load->library('artilium', array(
            'companyid' => $this->companyid
        ));
        $c = $this->artilium->get_cdr($mobile);
        //  mail('mail@simson.one','cdr',print_r($c, true));
        if ($c['cdr_count'] > 0) {
            foreach ($c['cdr'] as $cdr) {
                $r['time']   = $cdr['Begintime'];
                $r['number'] = str_replace('316240', '31', $cdr['MaskDestination']);
                if (substr($r['number'], 0, 2) == "00") {
                    $r['number'] = substr($r['number'], 2);
                }
                $r['cost']               = $cdr['CurNumUnitsUsed'];
                $r['destinationcountry'] = $cdr['DestinationCountry'];
                if ($cdr['TrafficTypeId'] == "2") {
                    $r['type']     = "DATA";
                    $r['duration'] = $cdr['Megabit'];
                } elseif ($cdr['TrafficTypeId'] == "1") {
                    $r['type']     = "VOICE";
                    $r['duration'] = sprintf('%02d:%02d:%02d', ($cdr['DurationConnection'] / 3600), ($cdr['DurationConnection'] / 60 % 60), $cdr['DurationConnection'] % 60);
                } else {
                    $r['type']     = "SMS";
                    $r['duration'] = $cdr['DurationConnection'];
                }
                $result[] = $r;
            }
        }
        echo json_encode(array(
            'cdr_count' => $c['cdr_count'],
            'cdr' => $result
        ));
    }
    public function getClientTicket()
    {
        //getSupports($companyid, $data, $clientid=false)
        $result = $this->api->getSupports($this->companyid, $this->dpost, $this->uri->segment(5));
        echo json_encode($result);
    }
    public function arta_UpdatePackageOption()
    {
        //echo json_encode($this->dpost);
        if ($this->dpost['checked'] === true) {
            $data = 1;
        } else {
            $data = 0;
        }
        echo json_encode(array(
            'id' => $this->dpost['value'],
            'value' => $data
        ));
    }
    public function getCounter()
    {
        $date          = date('Y-m-d');
        $client        = $this->db->query("SELECT COUNT(id) as total FROM a_clients where date_created=? and companyid=?", array(
            $date,
            $this->companyid
        ));
        $service       = $this->db->query("SELECT COUNT(id) as total FROM a_services where date_created=? and companyid=?", array(
            $date,
            $this->companyid
        ));
        $ticket        = $this->db->query("SELECT COUNT(id) as total FROM a_helpdesk_tickets where date like ? and companyid=?", array(
            $date . '%',
            $this->companyid
        ));
        $invoices      = $this->db->query("SELECT COUNT(iInvoiceNbr) as total FROM a_tblInvoice where dInvoiceDate like ? and iCompanyNbr=?", array(
            $date . '%',
            $this->companyid
        ));
        $r['clients']  = $client->row()->total;
        $r['services'] = $service->row()->total;
        $r['tickets']  = $ticket->row()->total;
        $r['invoices'] = $invoices->row()->total;
        echo json_encode($r);
    }
    public function ResetPODCounter()
    {
        $id = $this->dpost['serviceid'];
        $this->db->where('serviceid', $id);
        $this->db->update('a_services_mobile', array(
            'pod_counter' => 0
        ));
        echo json_encode(array(
            'result' => true
        ));
    }
    public function deleteservice()
    {
        if ($this->uri->segment(4) > 0) {
            $service = $this->Admin_model->getService($this->uri->segment(4));
            $this->db->query("delete from a_services where id=?", array(
                $this->uri->segment(4)
            ));
            $this->db->query("delete from a_services_mobile where serviceid=?", array(
                $this->uri->segment(4)
            ));
            redirect('admin/client/detail/' . $service->userid);
        } //$this->uri->segment(4) > 0
    }
    public function getDonorLists()
    {
        $this->db->where('country', $this->data['setting']->country_base);
        $this->db->select('operator_name, operator_code');
        $this->db->order_by('operator_name', 'ASC');
        $q = $this->db->get('a_mobile_donor_list');
        if ($q->num_rows() > 0) {
            echo json_encode($q->result_array());
        } else {
            echo json_encode(array());
        }
    }
    public function addoOrderMobile()
    {
        $this->load->library('magebo', array(
            'companyid' => $companyid
        ));
        $this->load->library('artilium', array(
            'companyid' => $companyid
        ));
        $this->load->model('Api_model');
        $this->load->model('Admin_model');
        if ($this->data['setting']->create_arta_contact == 1) {
            if ($this->dpost['agentid'] != "-1") {
                $rootid = getContactTypeid($this->dpost['agentid']);
            } else {
                $rootid = 7;
            }
            $this->load->library('artilium', array(
                'companyid' => $this->companyid
            ));
            sendlog('CreateContact', array(
                'agentid' => $rootid,
                'post' => $this->dpost
            ));
            $client    = $this->api->getClient($this->companyid, $this->dpost['userid']);
            $contactid = $this->artilium->CreateContact($client, $rootid);
            if ($contactid) {
                $this->dpost['ContactType2Id'] = $contactid;
            }
        }
        /*
        "packageid":"7",
        "agentid":"3007",
        "billingcycle":"Monthly",
        "promo_code":"NONE",
        "contract_terms":"6",
        "price":"20.00",
        "userid":"946225",
        "options":[
        "15",
        "14",
        "15",
        "14"
        ],
        "msisdn_type":"porting",
        "date_contract":"2019-07-01",
        "type_porting":"POD",
        "donor_accountnumber":"",
        "wish_date":"2019-07-03",
        "donor_msisdn":3232454567,
        "donor_provider":""
        */
        $contract    = explode('-', $this->dpost['date_contract']);
        $order_array = array(
            'companyid' => $this->companyid,
            'type' => 'mobile',
            'status' => 'New',
            'packageid' => $this->dpost['packageid'],
            'userid' => $this->dpost['userid'],
            'date_created' => date('Y-m-d'),
            'billingcycle' => 'Monthly',
            'date_contract' => $contract[1] . '-' . $contract[2] . '-' . $contract[0],
            'contract_terms' => $this->dpost['contract_terms'],
            'recurring' => getPriceRecurring($this->dpost['packageid'], $this->dpost['contract_terms']),
            'notes' => 'Order via portal V2 by: ' . $this->admin_name
        );
        if ($this->dpost['promo_code'] != 'NONE') {
            $order_array['promocode'] = trim($this->dpost['promo_code']);
        }
        $needproforma = NeedProforma($this->dpost['userid']);
        $serviceid    = $this->Api_model->insertOrder($order_array);
        if (!$serviceid) {
            echo json_encode(array(
                'result' => 'error',
                'message' => 'Order failed please contact Support +32 484889888'
            ));
            exit;
        } else {
            logAdmin(array(
                'companyid' => $this->companyid,
                'userid' => $this->dpost['userid'],
                'user' => $this->admin_name,
                'ip' => '127.0.0.1',
                'description' => 'Order Received ID ' . $serviceid
            ));
            $result['result']    = "success";
            $result['serviceid'] = $serviceid;
            if ($this->dpost['type_porting'] == "POD") {
                $donor_type         = "0";
                $donor_customertype = "0";
            } else {
            }
            if ($this->dpost['msisdn_type'] == "porting") {
                $mobiledata = array(
                    'msisdn_type' => $this->dpost['msisdn_type'],
                    'donor_msisdn' => $this->dpost['donor_msisdn'],
                    'donor_provider' => 'BEN-BEN',
                    'donor_sim' => '1111111111111111111',
                    'donor_type' => $donor_type,
                    'donor_customertype' => $donor_customertype,
                    'msisdn_status' => 'OrderWaiting',
                    'date_wish' => $this->dpost['wish_date'] . 'T00:00:00',
                    'serviceid' => $serviceid,
                    'companyid' => $this->companyid,
                    'userid' => $this->dpost['userid'],
                    'msisdn' => $this->dpost['donor_msisdn'],
                    'msisdn_pin' => '0000',
                    'msisdn_languageid' => setVoiceMailLanguageByClientLang($client['language']),
                    'ptype' => getSimType($this->companyid)
                );
                /*
                if (!empty($_POST['ptype_belgium']))
                {
                $mobiledata['ptype_belgium'] = $_POST['ptype_belgium'];
                }
                else
                {
                $mobiledata['ptype_belgium'] = 'NA';
                }
                */
                if ($this->dpost['type_porting'] == "CLASSIC") {
                    $mobiledata['donor_accountnumber'] = $this->dpost['donor_accountnumber'];
                }
            } else {
                $mobiledata = array(
                    'msisdn_type' => $this->dpost['msisdn_type'],
                    'msisdn_status' => 'OrderWaiting',
                    'serviceid' => $serviceid,
                    'companyid' => $this->companyid,
                    'userid' => $this->dpost['userid'],
                    'msisdn_pin' => '1111',
                    'ptype' => 'POST PAID'
                );
            }
            /*if (!empty($_POST['skip_accept']))
            {
            if ($_POST['skip_accept'] == "yes")
            {
            $mobiledata['msisdn_sim']  = $_POST['simnumberx'];
            $sim                       = $this->artilium->getSn($_POST['simnumberx']);
            $mobiledata['msisdn_sn']   = trim($sim->data->SN);
            $mobiledata['msisdn']      = trim($sim->data->MSISDNNr);
            $mobiledata['msisdn_puk1'] = $sim->data->PUK1;
            $mobiledata['msisdn_puk2'] = $sim->data->PUK2;
            }
            $activate = true;
            }
            */
            $product = getProduct($this->dpost['packageid']);
            if ($this->data['setting']->create_arta_contact_customer) {
                $mobiledata['msisdn_contactid'] = $client['ContactType2Id'];
            } else {
                $mobiledata['msisdn_contactid'] = getContactId($product->gid);
            }
            $mobiledata['initial_reload_amount'] = getProductReloadAmount($product->gid);
            if (!empty($this->dpost['sim_delivery'])) {
                $this->load->model('Helpdesk_model');
                if ($_POST['sim_delivery'] == "United") {
                    $delivery                   = true;
                    $mobiledata['sim_delivery'] = "United";
                    $headers                    = "From: " . $this->data['setting']->smtp_sender . "\r\n" . "CC: mail@simson.one";
                    mail("techniek@united-telecom.be", "Process Simcard Delivery Orderid: " . $serviceid, "Hello\n\nNew order has been arrived,\n\nOrderid: " . $serviceid . " MVNO decide that you need to process the simcard\n\nRegard", $headers);
                } else {
                    $delivery                   = false;
                    $mobiledata['sim_delivery'] = "MVNO";
                }
            }
            $service_mobile = $this->Api_model->insertMobileData($mobiledata);
            if ($service_mobile > 0) {
                logAdmin(array(
                    'companyid' => $this->companyid,
                    'userid' => $this->dpost['userid'],
                    'user' => 'Api User',
                    'ip' => $_SERVER['REMOTE_ADDR'],
                    'description' => 'Adding Order with Serviceid ' . $serviceid
                ));
                $email = getNotificationOrderEmail($this->companyid);
                if ($email) {
                    $product = getProduct($_POST['addonid']);
                    $headers = "From: noreply@united-telecom.be" . "\r\n" . "BCC: mail@simson.one";
                    mail($email, "New order notification", "Dear Team,\n\nNew Order has just been arrived, Please process this order as soon as possible:\n\nServiceid: " . $serviceid . "\nProduct: " . $product->name . "\n\nRegards", $headers);
                }
                $result['result'] = "success";
                send_growl(array(
                    'message' => $this->admin_name . ' has just added new order for Artiliumid: ' . $this->dpost['userid'],
                    'companyid' => $this->companyid
                ));
                if ($this->dpost['options']) {
                    foreach ($this->dpost['options'] as $addonidnya) {
                        $this->Api_model->addAddon($serviceid, $addonidnya);
                    }
                }
                if ($this->companyid == 54) {
                    if ($needproforma) {
                        $ppp = $this->create_proforma($this->companyid, (object) $client, $this->dpost['options'], $serviceid);
                        if ($ppp['proformaid'] > 0) {
                            $resmail = $this->sendProforma(array(
                                'invoiceid' => $ppp['proformaid'],
                                'invoicenum' => $ppp['invoicenum'],
                                'companyid' => $this->companyid
                            ));
                        }
                    } else {
                        $this->Admin_model->CreateFutureInvoiceOnActivation($serviceid, $client['vat_rate'], 7);
                    }
                }
            } else {
                $result['result']  = "error";
                $result['message'] = "Service mobile was not created";
            }
        }
        echo json_encode($result);
    }
    public function getProductSell()
    {
        echo json_encode(getProductSell($this->companyid));
    }


    public function proximus_checknetwork()
    {
         $this->load->model('Admin_model');
        $result = $this->Admin_model->proximus_api($this->dpost);
        echo json_encode($result);
    }
    public function getXDSLResult()
    {
        foreach (range(1, 60) as $ff) {
            sleep(2);
            $fin = $this->api->getXDSLResult(array(
            'orderid' => $this->uri->segment(5)));
            if ($fin['result'] == "success") {
                echo json_encode($fin);
                exit;
            }
            if ($ff == 60) {
                 echo json_encode(array('result' => 'error'));
                exit;
            }
        }
    }
    public function proximus_checkaddress()
    {
        $this->load->model('Admin_model');
        $result = $this->Admin_model->proximus_api($this->dpost);
        mail('mail@simson.one', "check address", print_r($result, true));
        if ($result->result == "success") {
            foreach (range(1, 60) as $time) {
                sleep(2);
                $order = $this->api->getProximusOrder($result->orderid);
                if ($order) {
                    mail('mail@simson.one', "order address", print_r($order, true));
                    $product = $this->Admin_model->proximus_api(array(
                        'action' => 'preordering',
                        'method' => 'GetDetailedProductAvailability',
                        'orderid' => $order->proximus_orderid
                    ));
                    if ($product->result == "success") {
                        echo json_encode($product);
                        exit;
                    }
                }
                if ($time == 60) {
                    echo json_encode(array(
                        'result' => 'error',
                        'Message' => 'No reponse from proximus for findgeography  after 60 seconds'
                    ));
                    exit;
                }
            }
            echo json_encode(array(
                'result' => 'error',
                'Message' => 'No reponse from proximus after 60 seconds'
            ));
        } else {
            echo json_encode(array(
                'result' => false,
                'message' => 'error when querying address, please verify if address correct',
                'output' => $result
            ));
        }
    }
    public function getAddonSell()
    {
        $product = $this->db->query("select * from a_products where id=?", array(
            $this->uri->segment(5)
        ));
        if ($product->num_rows() > 0) {
            $agd = $product->row()->gid;
        } else {
            $agd = false;
        }
        if (!empty($this->uri->segment(6))) {
            echo json_encode(getAddonSell('mobile', $this->companyid, $agd, false, $this->uri->segment(6)));
        } else {
            echo json_encode(getAddonSell('mobile', $this->companyid, $agd));
        }
    }
    public function getOptionSell()
    {
        $product = $this->db->query("select * from a_products where id=?", array(
            $this->uri->segment(5)
        ));
        if ($product->num_rows() > 0) {
            $agd = $product->row()->gid;
        } else {
            $agd = false;
        }
        echo json_encode(getAddonSell('mobile', $this->companyid, $agd, $this->uri->segment(6)));
    }
    public function deleteOrder()
    {
        $result['result'] = false;
        $service          = $this->api->getService($this->companyid, $this->dpost['serviceid']);
        if ($service) {
            $this->db->query("delete from a_services where id=?", array(
                $this->dpost['serviceid']
            ));
            $this->db->query("delete from a_services_mobile where serviceid=?", array(
                $this->dpost['serviceid']
            ));
            $result['result'] = true;
        }
        echo json_encode($result);
    }
    public function getPromotions()
    {
        echo json_encode(getPromotions($this->companyid));
    }
    public function getPricing()
    {
        $id       = $this->uri->segment(5);
        $duration = $this->uri->segment(6);
        $result   = getPriceRecurring($id, $duration);
        echo json_encode($result);
    }
    public function blockSim()
    {
        $this->load->model('Admin_model');
        $this->load->library('artilium', array(
            'companyid' => $this->companyid
        ));
        $service = $this->api->getService($this->companyid, $this->dpost['serviceid']);
        $pack    = $this->artilium->GetListPackageOptionsForSnAdvance(trim($service->detail->msisdn_sn));
        $this->Admin_model->save_state($this->dpost['serviceid'], $pack);
        $this->artilium->BlockOriginating(trim($service->detail->msisdn_sn), $pack, '0');
        $this->Admin_model->update_services_data('mobile', $this->dpost["serviceid"], array(
            'bar' => 1
        ));
        $this->Admin_model->update_services($this->dpost["serviceid"], array(
            'status' => 'Suspended'
        ));
        //$this->session->set_flashdata('success', 'All Originating Service include International Calls has been barred');
        logAdmin(array(
            'companyid' => $this->companyid,
            'userid' => $service->userid,
            'user' => $this->admin_name,
            'ip' => $_SERVER['REMOTE_ADDR'],
            'description' => 'Block Outbound Originating traffic for ' . $service->details->msisdn
        ));
        send_growl(array(
            'message' => $this->admin_name . ' Block outbound traffic for serviceid:' . $this->dpost["serviceid"],
            'companyid' => $this->companyid
        ));
        echo json_encode(array(
            'result' => true
        ));
    }
    public function unblockSim()
    {
        $this->load->model('Admin_model');
        $pp = $this->Admin_model->getPackageState($this->dpost["serviceid"]);
        $this->load->library('artilium', array(
            'companyid' => $this->companyid
        ));
        $pack    = (array) json_decode($pp);
        $service = $this->api->getService($this->companyid, $this->dpost['serviceid']);
        $this->artilium->UnBlockOriginating(trim($service->details->msisdn_sn), $pack, '1');
        $this->Admin_model->update_services($this->dpost["serviceid"], array(
            'status' => 'Active'
        ));
        logAdmin(array(
            'companyid' => $this->companyid,
            'userid' => $service->userid,
            'user' => $this->admin_name,
            'ip' => $_SERVER['REMOTE_ADDR'],
            'description' => 'UnBlock Outbound Originating traffic for ' . $service->details->msisdn
        ));
        $this->Admin_model->update_services_data('mobile', $this->dpost["serviceid"], array(
            'bar' => 0
        ));
        // $this->session->set_flashdata('success', 'All Originating Service include International Calls has been barred');
        echo json_encode(array(
            'result' => true
        ));
    }
    public function getSetting()
    {
        echo json_encode($this->data['setting']);
    }
    public function planPackageChange()
    {
        $this->dpost['companyid'] = $this->companyid;
        $this->dpost['requestor'] = $this->admin_name;
        if ($this->dpost['change'] == "Yes") {
            if ($this->dpost['setup_fee'] <= 0) {
                echo json_encode(array(
                    'result' => false,
                    'message' => 'You Choose to charge customer but the charge value is below or equal to 0.00'
                ));
                exit;
            }
        }
        $charge = array(
            'amount' => $this->dpost['setup_fee']
        );
        unset($this->dpost['charge']);
        unset($this->dpost['packagename']);
        unset($this->dpost['setup_fee']);
        $this->db->insert('a_subscription_changes', $this->dpost);
        $id = $this->db->insert_id();
        if ($id > 0) {
            echo json_encode(array(
                'result' => true,
                'id' => $id
            ));
            exit;
        }
        echo json_encode(array(
            'result' => false,
            'message' => 'error while inseting database'
        ));
    }
    public function CancelProductChangeRequest()
    {
        //echo json_encode($this->dpost);
        $this->db->where('id', $this->dpost['id']);
        $this->db->where('companyid', $this->companyid);
        $this->db->delete('a_subscription_changes');
        echo json_encode(array(
            'result' => $this->db->affected_rows()
        ));
    }
    public function arta_SwapSim()
    {
        $serviceid = $this->dpost['serviceid'];
        $this->load->library('artilium', array(
            'companyid' => $this->companyid
        ));
        $this->load->library('magebo', array(
            'companyid' => $this->companyid
        ));
        $this->data['mobile'] = $this->api->getService($this->companyid, $this->dpost['serviceid']);
        $number               = $this->magebo->getPincodeRef(trim($this->data['mobile']->details->msisdn));
        $swap                 = $this->artilium->SwapSim(array(
            'SN' => trim($this->data['mobile']->details->msisdn_sn),
            'NewSimNr' => $this->dpost['new_sim']
        ));
        if ($swap->result == "success") {
            $this->artilium->UpdateCLI($sn->SN, 1);
            $this->session->set_flashdata('success', 'SimSwap successfully executed');
            if ($this->dpost['charge'] == "YES") {
                if (!empty($this->dpost['amount'])) {
                    $amount = $this->dpost['amount'];
                } //!empty($_POST['amount'])
                else {
                    $amount = getSwapCost($this->data['mobile']->packageid, $this->companyid);
                }
                if ($amount > 0) {
                    $client = $this->Admin_model->getClient($this->data['mobile']->userid);
                    $this->magebo->x_magebosubscription_addPricing($client->mageboid, 359, 1, exvat4($client->vat_rate, $amount), 1, 'Sim Replacement ' . $number . ' - ' . date('d/m/Y'), date('m/d/Y'), date('m'), date('Y'), 1, 0, 'Sim Replacement  ' . $number . ' - ' . date('d/m/Y'), 'Sim Replacement  ' . $number . ' - ' . date('d/m/Y'), 0);
                } //$amount > 0
            } //$_POST['charge'] == "YES"
            $ss = $this->artilium->GetSn($_POST['new_sim']);
            $sn = (object) $ss->data;
            $this->Admin_model->updateService($_POST["serviceid"], array(
                'msisdn_swap' => 1,
                'msisdn_puk1' => $sn->PUK1,
                'msisdn_puk2' => $sn->PUK2,
                'msisdn_sim' => trim($_POST['new_sim']),
                'msisdn_sn' => trim($sn->SN)
            ));
            $headers = "From: noreply@united-telecom.be" . "\r\n" . "CC: simson.parlindungan@united-telecom.be";
            $body    = "";
            $body .= json_encode($this->dpost) . ' ' . print_r($sn, true);
            mail('simson.parlindungan@united-telecom.be', 'Swap success', $body, $headers);
            redirect('admin/subscription/detail/' . $this->dpost['serviceid']);
        } //$swap->result == "success"
        else {
            $headers = "From: noreply@united-telecom.be" . "\r\n" . "CC: simson.parlindungan@united-telecom.be";
            $body    = "";
            $body .= json_encode($this->dpost);
            mail('simson.parlindungan@united-telecom.be', 'Swap Failed', $body, $headers);
            $this->session->set_flashdata('error', 'SimSwap was not successfully executed, please contact support, a notification has been dispatched to United IT Department');
            redirect('admin/subscription/detail/' . $this->dpost['serviceid']);
        }
    }
    public function getLastBankFile()
    {
        $q = $this->db->query("select b.date_transaction,a.filename,a.id from a_sepa_files a left join a_sepa_items b on b.fileid=a.id where a.companyid=? order by id desc limit 1", array(
            $this->companyid
        ));
        if ($q->num_rows() > 0) {
            echo json_encode(array(
                'date' => $q->row()->date_transaction,
                'filename' => $q->row()->filename
            ));
        } else {
            echo json_encode(array(
                'date' => 'Never',
                'filename' => 'Not Available'
            ));
        }
    }
    public function getClients()
    {
        echo json_encode($this->api->getClientsTables($this->dpost));
    }
    public function getReseller()
    {
        echo json_encode($this->api->getResellerTables($this->dpost));
    }
    public function getServices()
    {
        echo json_encode($this->api->getServicesTables($this->uri->segment(5), $this->dpost));
    }
    public function getServicesNew()
    {
        echo json_encode($this->api->getServicesNewTables($this->uri->segment(5), $this->dpost));
    }
    public function getOnlinePayments()
    {
        echo json_encode($this->api->getOnlinePaymentTables($this->dpost));
    }
    public function getInvoices()
    {
        echo json_encode($this->api->getInvoiceTables($this->dpost));
    }
    public function getProformas()
    {
        echo json_encode($this->api->getProformaTables($this->dpost));
    }
    public function get_sepa_list()
    {
        echo json_encode($this->api->getSepaList($this->companyid, $this->uri->segment(5), $this->dpost));
    }
    public function get_payment_unassign()
    {
        echo json_encode($this->api->getPaymentUnAassign($this->companyid));
    }
    public function download_invoice()
    {
        $id = $this->uri->segment(5);
        if (!file_exists($this->data['setting']->DOC_PATH . $this->companyid . '/invoices/')) {
            mkdir($this->data['setting']->DOC_PATH . $this->companyid . '/invoices/', 0755, true);
        }
        if (!file_exists($this->data['setting']->DOC_PATH . $this->companyid . '/invoices/withcdr')) {
            mkdir($this->data['setting']->DOC_PATH . $this->companyid . '/invoices/withcdr', 0755, true);
        }
        $invoice_path = $this->data['setting']->DOC_PATH . $this->companyid . '/invoices/';
        if ($this->data['setting']->companyid == 2) {
            $companyid = array(
                1,
                2,
                3,
                4,
                6,
                8,
                9,
                25,
                26,
                28,
                29,
                31,
                34,
                35,
                37,
                41,
                47,
                50
            );
        } else {
            $companyid[] = $this->companyid;
        }
        if ($this->api->IsAllowedInvoice($id, $companyid)) {
            $invoice = $this->api->getInvoicePdf($id);
            $file    = $invoice_path . $id . '.pdf';
            file_put_contents($file, $invoice->PDF);
            $cdr = $this->downloadcdrpdf($id, $this->companyid);
            if ($cdr) {
                shell_exec('pdftk ' . $file . ' ' . $cdr . ' cat output ' . $this->data['setting']->DOC_PATH . $this->companyid . '/invoices/withcdr/' . $id . '.pdf');
                unlink($file);
                copy($this->data['setting']->DOC_PATH . $this->companyid . '/invoices/withcdr/' . $id . '.pdf', $file);
                $file = $this->data['setting']->DOC_PATH . $this->companyid . '/invoices/withcdr/' . $id . '.pdf';
            }
            $filename = $this->data['setting']->DOC_PATH . $this->companyid . '/invoices/' . $id . '.pdf';
            // Header content type
            header("Content-type: application/pdf");
            header("Content-Length: " . filesize($filename));
            // Send the file to the browser.
            readfile($filename);
        } else {
            echo json_encode(array(
                'result' => false
            ));
        }
    }
    public function download_client_export()
    {
        $data   = $this->api->getClientsTables($this->dpost, true);
        $header = array(
            'ArtiliumID' => 'string',
            'BillingID' => 'string',
            'MvnoID' => 'string',
            'Salutation' => 'string',
            'Initial' => 'string',
            'Firstname' => 'string',
            'Lastname' => 'string',
            'CompanyName' => 'string',
            'VAT' => 'string',
            'Email' => 'string',
            'Address' => 'string',
            'Postcode' => 'string',
            'City' => 'string',
            'Country' => 'string',
            'Language' => 'string',
            'Phonenumber' => 'string',
            'Paymentmethod' => 'string',
            'Due days' => 'string',
            'Date Birth' => 'string',
            'Vat Rate' => 'string',
            'AgentID' => 'string',
            'Gender' => 'string',
            'Referral' => 'string',
            'Location' => 'string',
            'IBAN' => 'string',
            'BIC' => 'string',
            'Madate ID' => 'string',
            'Mandate Status' => 'string',
            'Mandate Date' => 'string'
        );
        if ($data) {
            $time = time() . rand(4444, 9999);
            $this->load->library('xlswriter');
            header('Content-Type: application/zip');
            $this->xlswriter->writeSheet($data, 'Invoices', $header);
            $this->xlswriter->setAuthor('Simson Parlindungan');
            $this->xlswriter->setTitle('Clients Export');
            $this->xlswriter->setCompany('United');
            $this->xlswriter->writeToFile('/tmp/' . $time . '.xlsx');
            $filename = '/tmp/' . $time . '.xlsx';
            $zip      = $this->_zipfile($filename);
            if ($zip) {
                header("Content-Length: " . filesize($zip));
                readfile($zip);
                unlink($zip);
            }
        }
    }
    public function download_invoice_export()
    {
        $data   = $this->api->getInvoiceTables($this->dpost, true);
        $header = array(
            'ClientRef' => 'string',
            'BillingID' => 'string',
            'InvoiceNumber' => 'string',
            'dInvoiceDate' => 'string',
            'dInvoiceDueDate' => 'string',
            'mInvoiceAmount' => '#,##0.00 [$€-407];[RED]-#,##0.00 [$€-407]',
            'iInvoiceStatus' => 'string',
            'InvoiceType' => 'string',
            'Status' => 'string',
            'Type' => 'string',
            'cName' => 'string',
            'color' => 'string'
        );
        if ($data) {
            $time = time() . rand(4444, 9999);
            $this->load->library('xlswriter');
            header('Content-Type: application/zip');
            //header('Content-Disposition: attachment; filename="' . $fileName . '"');
            $this->xlswriter->writeSheet($data, 'Invoices', $header);
            $this->xlswriter->setAuthor('Simson Parlindungan');
            $this->xlswriter->setTitle('Invoices Export');
            $this->xlswriter->setCompany('United');
            $this->xlswriter->writeToFile('/tmp/' . $time . '.xlsx');
            $filename = '/tmp/' . $time . '.xlsx';
            $zip      = $this->_zipfile($filename);
            if ($zip) {
                header("Content-Length: " . filesize($zip));
                readfile($zip);
                unlink($zip);
            }
        }
    }
    public function download_proforma_export()
    {
        $data   = $this->api->getProformaTables($this->dpost, true);
        $header = array(
            'BillingID' => 'string',
            'ArtiliumID' => 'string',
            'Date' => 'string',
            'Duedate' => 'string',
            'Amount' => '#,##0.00 [$€-407];[RED]-#,##0.00 [$€-407]',
            'Payment Reference' => 'string',
            'Invoice Status' => 'string',
            'Customer' => 'string',
            'ClientRef' => 'string',
            'color' => 'string'
        );
        if ($data) {
            $time = time() . rand(4444, 9999);
            $this->load->library('xlswriter');
            header('Content-Type: application/zip');
            //header('Content-Disposition: attachment; filename="' . $fileName . '"');
            $this->xlswriter->writeSheet($data, 'Invoices', $header);
            $this->xlswriter->setAuthor('Simson Parlindungan');
            $this->xlswriter->setTitle('Invoices Export');
            $this->xlswriter->setCompany('United');
            $this->xlswriter->writeToFile('/tmp/' . $time . '.xlsx');
            $filename = '/tmp/' . $time . '.xlsx';
            $zip      = $this->_zipfile($filename);
            if ($zip) {
                header("Content-Length: " . filesize($zip));
                readfile($zip);
                unlink($zip);
            }
        }
    }
    public function download_pending_export()
    {
        $data   = $this->api->getSepaList($this->companyid, $this->uri->segment(5), $this->dpost, true);
        $header = array(
            'ClientRef' => 'string',
            'Date' => 'string',
            'InvoiceNumber' => 'string',
            'Transaction Code' => 'string',
            'Amount' => 'string',
            'IBAN' => '#,##0.00 [$€-407];[RED]-#,##0.00 [$€-407]',
            'Message' => 'string',
            'Name' => 'string'
        );
        log_message('error', 'Export pending ' . print_r($data, true));
        if ($data) {
            $time = time() . rand(4444, 9999);
            $this->load->library('xlswriter');
            header('Content-Type: application/zip');
            //header('Content-Disposition: attachment; filename="' . $fileName . '"');
            $this->xlswriter->writeSheet($data['data'], 'Items', $header);
            $this->xlswriter->setAuthor('Simson Parlindungan');
            $this->xlswriter->setTitle('Invoices Export');
            $this->xlswriter->setCompany('United');
            $this->xlswriter->writeToFile('/tmp/' . $time . '.xlsx');
            $filename = '/tmp/' . $time . '.xlsx';
            $zip      = $this->_zipfile($filename);
            if ($zip) {
                header("Content-Length: " . filesize($zip));
                readfile($zip);
                unlink($zip);
            }
        }
    }
    public function download_rejection_export()
    {
        $data   = $this->api->getSepaList($this->companyid, $this->uri->segment(5), $this->dpost, true);
        $header = array(
            'Date Transaction' => 'string',
            'IBAN' => 'string',
            'Amount' => 'string',
            'Message' => 'string',
            'BillingID' => 'string',
            'Index' => 'string',
            'Invoice Number' => 'string',
            'Type' => 'string',
            'ClientRef' => 'string',
            'ArtiliumID' => 'string',
            'Customer Name' => 'string',
            'Counter' => 'string'
        );
        if ($data) {
            $time = time() . rand(4444, 9999);
            $this->load->library('xlswriter');
            header('Content-Type: application/zip');
            //header('Content-Disposition: attachment; filename="' . $fileName . '"');
            $this->xlswriter->writeSheet($data['data'], 'Items', $header);
            $this->xlswriter->setAuthor('Simson Parlindungan');
            $this->xlswriter->setTitle('Invoices Export');
            $this->xlswriter->setCompany('United');
            $this->xlswriter->writeToFile('/tmp/' . $time . '.xlsx');
            $filename = '/tmp/' . $time . '.xlsx';
            $zip      = $this->_zipfile($filename);
            if ($zip) {
                header("Content-Length: " . filesize($zip));
                readfile($zip);
                unlink($zip);
            }
        }
    }
    public function download_alltrx_export()
    {
        $data   = $this->api->getSepaList($this->companyid, $this->uri->segment(5), $this->dpost, true);
        $header = array(
            'Index' => 'string',
            'Date' => 'string',
            'InvoiceNumber' => 'string',
            'TransactionCode' => 'string',
            'Amount' => '#,##0.00 [$€-407];[RED]-#,##0.00 [$€-407]',
            'IBAN' => 'string',
            'Name' => 'string'
        );
        if ($data) {
            $time = time() . rand(4444, 9999);
            $this->load->library('xlswriter');
            header('Content-Type: application/zip');
            //header('Content-Disposition: attachment; filename="' . $fileName . '"');
            $this->xlswriter->writeSheet($data['data'], 'Items', $header);
            $this->xlswriter->setAuthor('Simson Parlindungan');
            $this->xlswriter->setTitle('Invoices Export');
            $this->xlswriter->setCompany('United');
            $this->xlswriter->writeToFile('/tmp/' . $time . '.xlsx');
            $filename = '/tmp/' . $time . '.xlsx';
            $zip      = $this->_zipfile($filename);
            if ($zip) {
                header("Content-Length: " . filesize($zip));
                readfile($zip);
                unlink($zip);
            }
        }
    }
    public function download_onlinetrx_export()
    {
        $data   = $this->api->getOnlinePaymentTables($this->dpost, true);
        $header = array(
            'Index' => 'string',
            'Companyid' => 'string',
            'Date' => 'string',
            'Invoice Number' => 'string',
            'ArtiliumID' => 'string',
            'ClientRef' => 'string',
            'Method' => 'string',
            'Transid' => 'string',
            'Amount' => '#,##0.00 [$€-407];[RED]-#,##0.00 [$€-407]',
            'Type' => 'string',
            'cName' => 'string',
            'BillingID' => 'string'
        );
        if ($data) {
            $time = time() . rand(4444, 9999);
            $this->load->library('xlswriter');
            header('Content-Type: application/zip');
            //header('Content-Disposition: attachment; filename="' . $fileName . '"');
            $this->xlswriter->writeSheet($data, 'Invoices', $header);
            $this->xlswriter->setAuthor('Simson Parlindungan');
            $this->xlswriter->setTitle('Invoices Export');
            $this->xlswriter->setCompany('United');
            $this->xlswriter->writeToFile('/tmp/' . $time . '.xlsx');
            $filename = '/tmp/' . $time . '.xlsx';
            $zip      = $this->_zipfile($filename);
            if ($zip) {
                header("Content-Length: " . filesize($zip));
                readfile($zip);
                unlink($zip);
            }
        }
    }
    public function upload()
    {
        $this->load->helper('url');
        $name          = $this->input->post('name');
        $filename      = null;
        $isUploadError = false;
        $fullPath      = '';
        if ($_FILES && $_FILES['profile']['name']) {
            $config['upload_path']   = FCPATH . 'assets/uploads/';
            $config['allowed_types'] = 'xml';
            $config['max_size']      = 1000024;
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('profile')) {
                $isUploadError = true;
                $response      = array(
                    'status' => 'error',
                    'message' => $this->upload->display_errors()
                );
            } else {
                $uploadData = $this->upload->data();
                $fullPath   = base_url() . 'uploads/' . $uploadData['file_name'];
            }
        }
        if (!$isUploadError) {
            $response = array(
                'status' => 'success',
                'filePath' => $_FILES
            );
        }
        echo json_encode($response);
        //$this->output->set_content_type('application/json')->set_output(json_encode($response));
    }
    public function upload_bankfile()
    {
        $this->load->helper('url');
        $name          = $this->input->post('name');
        $filename      = null;
        $isUploadError = false;
        $fullPath      = '';
        if ($_FILES && $_FILES['profile']['name']) {
            if ($this->data['setting']->bankfile_processing == "upload") {
                if (!file_exists($this->data['setting']->DOC_PATH . $this->companyid . '/csv/')) {
                    mkdir($this->data['setting']->DOC_PATH . $this->companyid . '/csv/');
                }
                if (file_exists($this->data['setting']->DOC_PATH . $this->companyid . '/csv/' . $_FILES['profile']['name'])) {
                    json_encocd(array(
                        'status' => 'error',
                        'message' => 'we found the same file have been uploaded twice, please upload diffrent file otherwise your payment will be double'
                    ));
                    exit;
                }
                $config['upload_path']   = FCPATH . 'assets/uploads/';
                $config['allowed_types'] = 'csv|xml';
                $config['max_size']      = 1000024;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('profile')) {
                    $isUploadError = true;
                    $response      = array(
                        'status' => 'error',
                        'message' => $this->upload->display_errors()
                    );
                } else {
                    $uploadData = $this->upload->data();
                    $fullPath   = base_url() . 'uploads/' . $uploadData['file_name'];
                    $this->db->insert('a_sepa_files', array(
                        'filename' => $uploadData['file_name'],
                        'status' => 'Processing',
                        'uploader' => $this->admin_name,
                        'companyid' => $this->companyid
                    ));
                }
            } else {
                $response = array(
                    'status' => 'error',
                    'message' => 'Your company is not set to manual bank file processing'
                );
            }
            if (!$isUploadError) {
                $response = array(
                    'status' => 'success',
                    'filePath' => $_FILES
                );
            }
            echo json_encode($response);
        }
    }
    public function _zipfile($fileName)
    {
        //$filename = $this->adminid.'_'.time().'.zip';
        $zip     = new ZipArchive();
        $t       = rand(400, 900) . time();
        $zipFile = __DIR__ . '/' . $t . '_output.zip';
        if (file_exists($zipFile)) {
            unlink($zipFile);
        }
        $zipStatus = $zip->open($zipFile, ZipArchive::CREATE);
        if ($zipStatus !== true) {
            throw new RuntimeException(sprintf('Failed to create zip archive. (Status code: %s)', $zipStatus));
        }
        $password = date('Ymd');
        if (!$zip->setPassword($password)) {
            throw new RuntimeException('Set password failed');
        }
        // compress file
        $baseName = basename($fileName);
        if (!$zip->addFile($fileName, $baseName)) {
            throw new RuntimeException(sprintf('Add file failed: %s', $fileName));
        }
        // encrypt the file with AES-256
        if (!$zip->setEncryptionName($baseName, ZipArchive::EM_AES_256)) {
            throw new RuntimeException(sprintf('Set encryption failed: %s', $baseName));
        }
        $zip->close();
        unlink($fileName);
        return $zipFile;
    }
    public function downloadcdrpdf($id, $companyid)
    {
        error_reporting(1);
        //$mageboid = $this->uri->segment(5);
        $this->load->library('magebo', array(
            'companyid',
            $companyid
        ));
        $cdrs = $this->magebo->getInvoiceCdrFile($id);
        $gprs = $this->magebo->getDataCdr($id);
        // print_r($cdrs);
        //$cdrs = $this->Admin_model->mageboGet('GetCdrs/' . $id);
        // exit;
        if (!$cdrs) {
            if ($gprs) {
                $gp = "";
            } else {
                return false;
            }
        } else {
            //print_r($cdrs);
            // exit;
            $table = '<h2>CDR Voice & SMS: ' . $pincode . '</h2><br />
            <table><thead>
            <tr bgcolor="#111" color="#fff" cellpadding="1" cellspacing="0">
            <th width="12%">' . lang('Date') . '</th>
            <th width="10%">' . lang('Number') . '</th>
            <th width="15%">' . lang('Destination') . '</th>
            <th width="42%">' . lang('Description') . '</th>
            <th width="10%">' . lang('Duration') . '</th>
            <th width="10%">' . lang('Cost') . ' (incl. VAT)</th>
            </tr>
            </thead>
            <tbody>
            ';
            // mail('mail@simson.one','cdr',print_r($cdrs, true));
            foreach ($cdrs as $array) {
                $table .= '
                <tr>
                <td width="12%">' . substr($array->dCallDate, 0, -3) . '</td>
                <td width="10%">' . $array->iPincode . '</td>
                <td width="15%">' . $array->cDialedNumber . '</td>
                <td width="42%">' . $array->cInvoiceGroupDescription . ' - ' . $array->cCallTranslation . '</td>
                <td width="10%">' . gmdate("H:i:s", $array->iInvoiceDuration) . '</td>
                <td width="10%">€' . str_replace('.', ',', includevat('21', $array->mInvoiceSale)) . '</td>
                </tr>
                ';
            }
            $table .= '</tbody></table>';
        }
        if ($gprs) {
            foreach ($gprs as $pincode => $cdr) {
                $gp .= '<h2>CDR DATA: ' . $pincode . '</h2><br />
                <table><thead>
                <tr bgcolor="#111" color="#fff" cellpadding="1" cellspacing="0">
                <th width="15%">' . lang('Date') . '</th>
                <th width="13%">' . lang('Number') . '</th>
                <th width="20%">' . lang('Type') . '</th>
                <th width="35%">' . lang('Zone') . ' </th>
                <th width="10%">' . lang('Bytes') . '</th>


                </tr>
                </thead>
                <tbody>
                ';
                foreach ($cdr as $array) {
                    $bytes[] = $array->Bytes;
                    $gp .= '
                    <tr>
                    <td width="15%">' . substr($array->SessionDate, 0, -3) . '</td>
                    <td width="13%">' . $array->iPincode . '</td>
                    <td width="20%">' . $array->Type . '</td>
                    <td width="35%">' . $array->Zone . ' ' . $array->RoamingCountry . '</td>
                    <td width="10%">' . convertToReadableSize($array->Bytes) . '</td>

                    </tr>
                    ';
                }
                $gp .= '</tbody></table><br /><br /><strong>Total Data usage  ' . $pincode . ' : ' . convertToReadableSize(array_sum($bytes)) . '</strong>';
                unset($bytes);
            }
        }
        $this->load->library('spdf');
        $pdf = new Spdf('P', 'mm', 'A4', true, 'UTF-8', false);
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Simson Asuni');
        $pdf->SetTitle('CDR');
        $pdf->setInvoicenumber($id);
        $pdf->SetSubject('CDR Invoice ' . $id);
        $pdf->SetKeywords('TCPDF, Invoice, Cdr, ' . $id);
        $pdf->AddPage('L', 'A4');
        //$pdf->SetFont('droidsans', '', '12');
        //$pdf->Cell(180, 6, lang('Call Detail Records').' Invoice: '.$id, 0, 0, '', 0);
        $pdf->ln(10);
        //$pdf->SetFont('droidsans', '', '9');
        // set default header data
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE . ' 010', PDF_HEADER_STRING);
        $pdf->SetPrintFooter(false);
        // set header and footer fonts
        $pdf->setHeaderFont(array(
            PDF_FONT_NAME_MAIN,
            '',
            PDF_FONT_SIZE_MAIN
        ));
        $pdf->setFooterFont(array(
            PDF_FONT_NAME_DATA,
            '',
            PDF_FONT_SIZE_DATA
        ));
        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        // set auto page breaks
        $pdf->SetAutoPageBreak(true, "10");
        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
            require_once dirname(__FILE__) . '/lang/eng.php';
            $pdf->setLanguageArray($l);
        }
        // ---------------------------------------------------------
        // print TEXT
        $pdf->SetFont('helvetica', '', '9');
        //  $pdf->PrintChapter(1, lang('Call Detail Records'), $table, true);
        $pdf->writeHTML($table, true, false, true, false, 'J');
        if ($gprs) {
            $pdf->AddPage('L', 'A4');
            //$pdf->SetFont('droidsans', '', '12');
            //$pdf->Cell(180, 6, lang('Call Detail Records').' Invoice: '.$id, 0, 0, '', 0);
            $pdf->ln(10);
            $pdf->SetFont('helvetica', '', '9');
            // set default header data
            $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE . ' 010', PDF_HEADER_STRING);
            // set header and footer fonts
            $pdf->setHeaderFont(array(
                PDF_FONT_NAME_MAIN,
                '',
                PDF_FONT_SIZE_MAIN
            ));
            $pdf->setFooterFont(array(
                PDF_FONT_NAME_DATA,
                '',
                PDF_FONT_SIZE_DATA
            ));
            // set default monospaced font
            $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
            // set margins
            $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
            $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
            $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
            // set auto page breaks
            $pdf->SetAutoPageBreak(true, "10");
            // set image scale factor
            $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
            // set some language-dependent strings (optional)
            if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
                require_once dirname(__FILE__) . '/lang/eng.php';
                $pdf->setLanguageArray($l);
            }
            $pdf->writeHTML($gp, true, false, true, false, 'J');
        }
        // print HTML
        //$pdf->PrintChapter(2, 'LOREM IPSUM [HTML]', FCPATH.'data/chapter_demo_2.txt', true);
        // ---------------------------------------------------------
        //Close and output PDF document
        if (!file_exists($this->data['setting']->DOC_PATH . $companyid . '/invoices/cdr')) {
            mkdir($this->data['setting']->DOC_PATH . $companyid . '/invoices/cdr');
        }
        $pdf->Output($this->data['setting']->DOC_PATH . $companyid . '/invoices/cdr/' . $id . '.pdf', 'F');
        return $this->data['setting']->DOC_PATH . $companyid . '/invoices/cdr/' . $id . '.pdf';
    }
    public function getProforma()
    {
        $this->load->model('Proforma_model');
        $id            = $this->uri->segment(5);
        $invoice       = $this->Proforma_model->getInvoice($id);
        $r['payments'] = array();
        $r['items']    = $invoice['items'];
        $r['data']     = $invoice;
        $r['result']   = true;
        echo json_encode($r);
    }
    public function download_proforma()
    {
        $companyid = $this->companyid;
        set_time_limit(0);
        $this->load->model('Admin_model');
        $this->load->model('Proforma_model');
        if (!is_dir($this->data['setting']->DOC_PATH . 'proforma')) {
            mkdir($this->data['setting']->DOC_PATH . 'proforma', 0755);
        }
        if ($companyid == 54) {
            $brand = $this->Admin_model->getBrandPdfFooter(1);
            //print_r($brand);
            //exit;
        }
        $id         = $this->uri->segment(5);
        $invoice    = $this->Proforma_model->getInvoice($id);
        $customerku = $this->Proforma_model->getClient($invoice['userid']);
        $this->load->library('magebo', array(
            'companyid' => $companyid
        ));
        $this->load->library('Spdf');
        $pdf = new Spdf('P', 'mm', 'A4', true, 'UTF-8', false);
        $pdf->SetHeaderMargin(30);
        $pdf->SetTopMargin(20);
        //$pdf->droidsansogo($this->data['setting']->proforma_footer);
        $pdf->SetAutoPageBreak(true, 30);
        $pdf->SetAuthor('Simson Parlindungan');
        $pdf->SetDisplayMode('real', 'default');
        $pdf->setHeaderFont(array(
            PDF_FONT_NAME_MAIN,
            '',
            PDF_FONT_SIZE_MAIN
        ));
        $pdf->setFooterFont(array(
            PDF_FONT_NAME_DATA,
            '',
            PDF_FONT_SIZE_DATA
        ));
        $pdf->setFoot($brand->brand_footer);
        $pdf->setFooterFont('droidsans');
        $tax = round($invoice['tax']) . '%';
        $pdf->AddPage('P', 'A4');
        $pdf->Image($brand->brand_logo, 10, 10, 60);
        $style  = array(
            'position' => '',
            'align' => 'R',
            'stretch' => false,
            'fitwidth' => true,
            'cellfitalign' => '',
            'border' => false,
            'hpadding' => 'auto',
            'vpadding' => 'auto',
            'fgcolor' => array(
                0,
                0,
                0
            ),
            'bgcolor' => false,
            'text' => true,
            'font' => 'droidsans',
            'fontsize' => 8,
            'stretchtext' => 6
        );
        $style1 = array(
            'position' => '',
            'align' => 'R',
            'stretch' => false,
            'fitwidth' => true,
            'cellfitalign' => '',
            'border' => true,
            'hpadding' => 'auto',
            'vpadding' => 'auto',
            'fgcolor' => array(
                64,
                64,
                64
            ),
            'bgcolor' => false,
            'color' => array(
                255,
                255,
                255
            ),
            'text' => true,
            'font' => 'droidsans',
            'fontsize' => 8,
            'stretchtext' => 6
        );
        $pdf->setY(13);
        $pdf->setX(130);
        // $pdf->write1DBarcode(preg_replace("/[^0-9,.]/", "", $invoice['invoicenum']), 'I25', '', '', '', 18, 0.9, $style, 'R');
        $pdf->GetPage();
        $pdf->Image($brand->brand_logo, 10, 10, 90);
        $pdf->SetFont('droidsans', '', 10);
        if (!empty($customerku->companyname)) {
            $company = $customerku->companyname . "\n";
            $contact = '';
        } else {
            $company = '';
            $contact = $customerku->firstname . " " . $customerku->lastname . "\n";
        }
        $address1 = str_replace("&#039;", "'", $customerku->address1) . "\n";
        $pdf->setY(70);
        $city    = $customerku->postcode . ", " . $customerku->city . "\n";
        $country = $customerku->country . "\n";
        $pdf->SetFont('droidsans', 'B', 12);
        $pdf->setCellPaddings(0, 0, 0, 0);
        $pdf->setCellMargins(0, 0, 0, 0);
        $pdf->SetFillColor(255, 255, 255);
        $a = '';
        $pdf->MultiCell(100, 10, $a, 0, 'J', 0, 0, '', '', true, 0, false, true, 30, 'T');
        $pdf->MultiCell(90, 30, $company . '' . $contact . '' . $address1 . '' . $city . '' . getCountryNameLang($customerku->country, $customerku->language), 0, 'L', 1, 1, '', '', true, 0, false, true, 30, 'M');
        if ($this->data['setting']->country_base == "BE") {
            $this->data['invoice_ref'] = ogm($this->uri->segment(4));
        } else {
            if (!$invoice['invoicenum']) {
                die('Invoice not found 456');
            }
            $this->data['invoice_ref'] = $this->magebo->Mod11($invoice['invoicenum']);
            // $invoice_ref = $this->magebo->Mod11($invoice->iInvoiceNbr);
        }
        $pdf->SetFont('droidsans', 'B', 14);
        //$pdf->SetLineStyle(array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(255, 153, 0)));
        $pdf->cell(70, 4, lang("Proforma Invoice") . ": " . $invoice['invoicenum'], 0, 1, 'L');
        $pdf->Ln(10);
        $pdf->SetFont('droidsansb', 'B', 9);
        $pdf->SetFillColor(64, 64, 64);
        $pdf->SetTextColor(255, 255, 255);
        $pdf->setCellMargins(0, 0, 0, 0);
        $pdf->setCellPaddings(2, 1, 2, 1);
        $pdf->SetLineStyle(array(
            'width' => 0.5,
            'cap' => 'butt',
            'join' => 'miter',
            'dash' => 0,
            'color' => array(
                64,
                64,
                64
            )
        ));
        $pdf->MultiCell(35, 7, lang('Client Number'), 1, 'L', 1, 0, '', '', true, 0, false, true, 40, 'T');
        $pdf->MultiCell(40, 7, lang('VAT'), 1, 'L', 1, 0, '', '', true, 0, false, true, 40, 'T');
        $pdf->MultiCell(50, 7, lang('Reference'), 1, 'L', 1, 0, '', '', true, 0, false, true, 40, 'T');
        $pdf->MultiCell(35, 7, lang('Date'), 1, 'L', 1, 0, '', '', true, 0, false, true, 40, 'T');
        $pdf->MultiCell(35, 7, lang('Duedate'), 1, 'R', 1, 1, '', '', true, 0, false, true, 40, 'T');
        $pdf->setCellPaddings(1, 1, 1, 1);
        $pdf->setCellMargins(1, 1, 1, 1);
        $pdf->SetFillColor(255, 255, 255);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('droidsans', '', 9);
        $pdf->setCellPaddings(2, 1, 2, 1);
        $pdf->setCellMargins(0, 0, 0, 0);
        $pdf->SetLineStyle(array(
            'width' => 0.5,
            'cap' => 'butt',
            'join' => 'miter',
            'dash' => 0,
            'color' => array(
                64,
                64,
                64
            )
        ));
        $pdf->MultiCell(35, 5, trim($invoice['mvno_id']), 1, 'L', 1, 0, '', '', true, 0, false, true, 10, 'T');
        $pdf->MultiCell(40, 5, $customerku->vat, 1, 'L', 1, 0, '', '', true, 0, false, true, 10, 'T');
        $pdf->MultiCell(50, 5, $this->data['invoice_ref'], 1, 'L', 1, 0, '', '', true, 0, false, true, 10, 'T');
        $pdf->MultiCell(35, 5, date("d-m-Y", strtotime($invoice['date'])), 1, 'L', 1, 0, '', '', true, 0, false, true, 10, 'T');
        $pdf->MultiCell(35, 5, date("d-m-Y", strtotime($invoice['duedate'])), 1, 'R', 1, 1, '', '', true, 0, false, true, 10, 'T');
        $pdf->Ln(15);
        # Invoice Items
        $tblhtml = '<table width="550" bgcolor="#404040" cellspacing="1" cellpadding="1" border="0">
    <tr height="40" bgcolor="#404040" style="font-weight:bold;text-align:center;">
    <td align="left" width="7%"  color="#fff" >' . lang('Qty') . '</td>
        <td align="left" width="58%"  color="#fff">' . lang('Description') . '</td>
         <td align="right" width="18%" color="#fff">' . lang('Price/pcs') . '</td>
        <td align="right" width="18%" color="#fff">' . lang('Amount') . '</td>
    </tr>';
        foreach ($invoice['items'] as $item) {
            $tblhtml .= '
    <tr bgcolor="#fff" border="1">
         <td align="left">' . $item['qty'] . '<br /></td>
        <td align="left">' . nl2br($item['description']) . '<br /></td>
        <td align="right">' . '&euro;' . $item['price'] . '</td>
        <td align="right">' . '&euro;' . exvat($item['taxrate'], $item['amount']) . '</td>
    </tr>';
        }
        $tblhtml .= '

</table>';
        $pdf->writeHTML($tblhtml, true, false, false, false, '');
        $pdf->Ln(5);
        $styles = array(
            'width' => 0.5,
            'cap' => 'butt',
            'join' => 'miter',
            'dash' => 0,
            'color' => array(
                64,
                64,
                64
            )
        );
        //$pdf->Ln(10);
        $pdf->SetFont('droidsansb', 'B', 9);
        $pdf->SetFillColor(255, 255, 255);
        $pdf->setCellMargins(0, 0, 0, 0);
        $pdf->setCellPaddings(2, 1, 2, 1);
        $pdf->SetLineStyle(array(
            'width' => 0.5,
            'cap' => 'butt',
            'join' => 'miter',
            'dash' => 0,
            'color' => array(
                64,
                64,
                64
            )
        ));
        $pdf->MultiCell(135, 5, '', 0, 'L', 1, 0, '', '', true, 0, false, true, 40, 'T');
        $pdf->MultiCell(30, 5, 'Subtotal' . " :", 1, 'R', 1, 0, '', '', true, 0, false, true, 40, 'T');
        $pdf->MultiCell(30, 5, '€' . $invoice['subtotal'], 1, 'R', 1, 1, '', '', true, 0, false, true, 40, 'T');
        $pdf->SetLineStyle(array(
            'width' => 0.5,
            'cap' => 'butt',
            'join' => 'miter',
            'dash' => 0,
            'color' => array(
                64,
                64,
                64
            )
        ));
        $pdf->MultiCell(135, 5, '', 0, 'L', 1, 0, '', '', true, 0, false, true, 10, 'T');
        $pdf->MultiCell(30, 5, "VAT " . floor($invoice['items'][0]['taxrate']) . '%:', 1, 'R', 1, 0, '', '', true, 0, false, true, 10, 'T');
        $pdf->MultiCell(30, 5, '€' . $invoice['tax'], 1, 'R', 1, 1, '', '', true, 0, false, true, 10, 'T');
        $pdf->MultiCell(135, 5, '', 0, 'L', 1, 0, '', '', true, 0, false, true, 10, 'T');
        $pdf->SetFillColor(64, 64, 64);
        $pdf->setCellMargins(0, 0, 0, 0);
        $pdf->SetTextColor(255, 255, 255);
        $pdf->MultiCell(30, 5, "Total :", 1, 'R', 1, 0, '', '', true, 0, false, true, 10, 'T');
        $pdf->MultiCell(30, 5, '€' . $invoice['total'], 1, 'R', 1, 1, '', '', true, 0, false, true, 10, 'T');
        $taxrate = 1;
        $lebar   = '38';
        $ypos    = '100';
        $xpos    = '161';
        /*
        Gelieve het totale bedrag te storten voor 13/03/2019
        op rekeningnummer NL10RABO0147382548
        met vermelding van de referentie 5310 0005 5200 0000
        */
        $pdf->setY(-120);
        $pdf->SetLeftMargin(-10);
        $pdf->SetRightMargin(0);
        $pdf->SetAutoPageBreak(true, 0);
        $pdf->SetFont('droidsansb', 'B', 7);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->setImageScale(1.53);
        $pdf->SetLineStyle(array(
            'width' => 0.5,
            'cap' => 'butt',
            'join' => 'miter',
            'dash' => 0,
            'color' => array(
                0,
                0,
                0
            )
        ));
        $pdf->RoundedRect(5, 240, 200, 40, 3.50, '1111');
        $pdf->setXY(10, 242);
        $pdf->SetFont('droidsansb', '', 12);
        $pdf->Cell(140, 0, lang("Please pay amount of ") . " €" . $invoice['total'] . " " . lang("to be transfered before ") . date("d-m-Y", strtotime($invoice['duedate'])), 0, 0, 'R', 0, '0', 4);
        $pdf->setXY(10, 248);
        $pdf->Cell(100, 0, lang("On account number") . " : NL10RABO0147382548", 0, 0, 'R', 0, '0', 4);
        if ($invoice['items'][0]['taxrate'] > 0) {
            $pdf->ln(4);
            $pdf->setXY(10, 264);
            $pdf->Cell(100, 0, lang("with payment reference ") . $this->data['invoice_ref'], 0, 0, 'R', 0, '0', 4);
        } else {
            $pdf->ln(4);
            $pdf->setXY(10, 264);
            $pdf->Cell(30, 0, lang("Vat Exempt"), 0, 0, 'R', 0, '0', 4);
            $pdf->ln(4);
            $pdf->setXY(10, 270);
            $pdf->Cell(100, 0, lang("with payment reference ") . $this->data['invoice_ref'], 0, 0, 'R', 0, '0', 4);
        }
        $pdf->Output('Proforma_' . $id . '.pdf', 'I');
        /*
        ob_clean();
        flush();
        $idx = $invoice['invoicenum'];
        //header('Content-Type: application/octet-stream');
        //header('Content-Length: ' . filesize($this->data['setting']->DOC_PATH . 'invoices/' . $invoice['id'] . '.pdf'));
        header('Content-Disposition: attachment; filename="' . $idx . '.pdf' . '"');
        readfile($this->data['setting']->DOC_PATH . 'proforma/' . $idx . '.pdf');
        */
    }
    public function sendProforma()
    {
        $this->load->model('Admin_model');
        if (isPost()) {
            //echo json_encode(array('result' => false, 'data' => $_POST)); dcoppieters@delta.nl
            if (!file_exists($this->data['setting']->DOC_PATH . '/' . $this->companyid . '/proformas/')) {
                mkdir($this->data['setting']->DOC_PATH . '/' . $this->companyid . '/proformas/', 0755, true);
            }
            $invoice_path = $this->data['setting']->DOC_PATH . '/' . $this->companyid . '/proformas/';
            $invoiceid    = $this->dpost['invoiceid'];
            $invoice      = $this->Proforma_model->getInvoice($invoiceid);
            $client       = $this->Admin_model->getClient($invoice['userid']);
            $this->load->library('magebo', array(
                'companyid' => $client->companyid
            ));
            if (!$this->dpost['invoicenum']) {
                die('Invoice not found 11');
            }
            $invoice_ref = $this->magebo->Mod11($this->dpost['invoicenum']);
            if ($this->data['setting']->smtp_type == 'smtp') {
                $config = array(
                    'protocol' => 'smtp',
                    'smtp_host' => $this->data['setting']->smtp_host,
                    'smtp_port' => $this->data['setting']->smtp_port,
                    'smtp_user' => $this->data['setting']->smtp_user,
                    'smtp_pass' => $this->encryption->decrypt($this->data['setting']->smtp_pass),
                    'mailtype' => 'html',
                    'charset' => 'utf-8',
                    'starttls' => true,
                    'wordwrap' => true
                );
            } else {
                $config['protocol'] = 'sendmail';
                $config['mailpath'] = '/usr/sbin/sendmail';
                $config['mailtype'] = 'html';
                $config['charset']  = 'utf-8';
                $config['wordwrap'] = true;
            }
            $this->email->clear(true);
            $this->email->initialize($config);
            $body   = getMailContent('proforma', $client->language, $client->companyid);
            //$attachments = getAttachmentEmail('invoice', $client->language, $client->companyid);
            $amount = $invoice['total'];
            $body   = str_replace('{$mInvoiceAmount}', number_format($amount, 2), $body);
            $body   = str_replace('{$iInvoiceNbr}', $invoice['invoicenum'], $body);
            $body   = str_replace('{$invoiceid}', $invoice['id'], $body);
            $body   = str_replace('{$clientid}', $client->mvno_id, $body);
            $body   = str_replace('{$dInvoiceDate}', $invoice['date'], $body);
            $body   = str_replace('{$dInvoiceDueDate}', $invoice['duedate'], $body);
            $body   = str_replace('{$Companyname}', $this->data['setting']->companyname, $body);
            $body   = str_replace('{$name}', format_name($client), $body);
            $body   = str_replace('{$cInvoiceReference}', $invoice_ref, $body);
            $this->email->set_newline("\r\n");
            $this->email->from($this->data['setting']->smtp_sender, $this->data['setting']->smtp_name);
            $this->email->to($client->email);
            $this->email->bcc(array(
                'mail@simson.one',
                $this->admin_email
            ));
            $subject = getSubject('proforma', $client->language, $client->companyid);
            $subject = str_replace('{$iInvoiceNbr}', $invoice['invoicenum'], $subject);
            $subject = str_replace('{$Companyname}', $this->data['setting']->companyname, $subject);
            //$this->email->bcc($_SESSION['email']);
            $this->email->subject($subject);
            $this->email->attach($res);
            $this->email->message($body);
            if ($this->email->send()) {
                logEmailOut(array(
                    'userid' => $client->id,
                    'companyid' => $client->companyid,
                    'to' => $client->email,
                    'subject' => $subject,
                    'message' => $body
                ));
                echo json_encode(array(
                    'result' => true
                ));
            } else {
                echo json_encode(array(
                    'result' => false,
                    'error' => $this->email->print_debugger()
                ));
            }
        } else {
            echo json_encode(array(
                'result' => false
            ));
        }
    }
    public function sendInvoice()
    {
        $this->load->model('Admin_model');
        if (isPost()) {
            //echo json_encode(array('result' => false, 'data' => $_POST)); dcoppieters@delta.nl
            if (!file_exists($this->data['setting']->DOC_PATH . '/' . $this->companyid . '/invoices/')) {
                mkdir($this->data['setting']->DOC_PATH . '/' . $this->companyid . '/invoices/', 0755, true);
            }
            $invoice_path = $this->data['setting']->DOC_PATH . '/' . $this->companyid . '/invoices/';
            $invoiceid    = $this->dpost['invoiceid'];
            $invoice      = $this->Admin_model->getInvoicedetail($invoiceid);
            $client       = $this->Admin_model->getClient($this->dpost['userid']);
            //wwderijck@zeelandnet.nl
            $res          = $this->download_invoiceid($invoiceid);
            if (!file_exists($invoice_path . $invoiceid . '.pdf')) {
                $this->download_invoiceid($invoiceid);
                echo json_encode(array(
                    'result' => true
                ));
            } else {
                if ($this->data['setting']->smtp_type == 'smtp') {
                    $config = array(
                        'protocol' => 'smtp',
                        'smtp_host' => $this->data['setting']->smtp_host,
                        'smtp_port' => $this->data['setting']->smtp_port,
                        'smtp_user' => $this->data['setting']->smtp_user,
                        'smtp_pass' => $this->encryption->decrypt($this->data['setting']->smtp_pass),
                        'mailtype' => 'html',
                        'charset' => 'utf-8',
                        'starttls' => true,
                        'wordwrap' => true
                    );
                } else {
                    $config['protocol'] = 'sendmail';
                    $config['mailpath'] = '/usr/sbin/sendmail';
                    $config['mailtype'] = 'html';
                    $config['charset']  = 'utf-8';
                    $config['wordwrap'] = true;
                }
                $this->email->clear(true);
                $this->email->initialize($config);
                $body = getMailContent('invoice', $client->language, $client->companyid);
                //$attachments = getAttachmentEmail('invoice', $client->language, $client->companyid);
                $this->load->library('magebo', array(
                    'companyid' => $client->companyid
                ));
                $invoice_ref = $this->magebo->Mod11($invoice->iInvoiceNbr);
                // $body = $this->load->view('email/content', $this->data, true);
                //$body = str_replace('{$mInvoiceAmount}', number_format($invoice->mInvoiceAmount, 2), $body);
                $payment     = $this->magebo->getInvoiceBalance($invoice->iInvoiceNbr, $invoice->iAddressNbr);
                $amount      = $invoice->mInvoiceAmount - $payment;
                $body        = str_replace('{$mInvoiceAmount}', number_format($amount, 2), $body);
                $body        = str_replace('{$iInvoiceNbr}', $invoice->iInvoiceNbr, $body);
                $body        = str_replace('{$clientid}', $client->mvno_id, $body);
                $body        = str_replace('{$dInvoiceDate}', $invoice->dInvoiceDate, $body);
                $body        = str_replace('{$dInvoiceDueDate}', $invoice->dInvoiceDueDate, $body);
                $body        = str_replace('{$Companyname}', $this->data['setting']->companyname, $body);
                $body        = str_replace('{$name}', format_name($client), $body);
                $body        = str_replace('{$cInvoiceReference}', $invoice_ref, $body);
                $this->email->set_newline("\r\n");
                $this->email->from($this->data['setting']->smtp_sender, $this->data['setting']->smtp_name);
                $client_email = getInvoiceEmail($client->id);
                $this->email->to($client_email);
                $subject = getSubject('invoice', $client->language, $client->companyid);
                $subject = str_replace('{$iInvoiceNbr}', $invoice->iInvoiceNbr, $subject);
                $subject = str_replace('{$Companyname}', $this->data['setting']->companyname, $subject);
                $this->email->bcc(array(
                    'mail@simson.one',
                    $this->admin_email
                ));
                $this->email->subject($subject);
                $this->email->attach($invoice_path . $invoiceid . '.pdf');
                $this->email->message($body);
                if (!isTemplateActive($client->companyid, 'invoice')) {
                    log_message('error', 'Template invoice is disabled sending aborted');
                    echo json_encode(array(
                        'result' => false,
                        'error' => 'Template invoice is disabled sending aborted'
                    ));
                    exit;
                }
                if ($this->email->send()) {
                    logEmailOut(array(
                        'userid' => $client->id,
                        'companyid' => $client->companyid,
                        'to' => $client->email,
                        'subject' => $subject,
                        'message' => $body
                    ));
                    echo json_encode(array(
                        'result' => true
                    ));
                } else {
                    echo json_encode(array(
                        'result' => false,
                        'error' => $this->email->print_debugger()
                    ));
                }
            }
        } else {
            echo json_encode(array(
                'result' => false
            ));
        }
    }
    public function getBankFiles()
    {
        $q = $this->db->query("select *,(select count(*) from a_sepa_items where fileid=a_sepa_files.id) as items,(select count(*) from a_sepa_items where status != 'Completed' and fileid=a_sepa_files.id)  as pending from a_sepa_files where companyid=? order by id desc limit ? ", array(
            $this->companyid,
            10
        ));
        if ($q->num_rows() > 0) {
            echo json_encode($q->result_array());
        } else {
            echo json_encode(array());
        }
    }
    public function download_invoiceid($id)
    {
        if (!file_exists($this->data['setting']->DOC_PATH . '/' . $this->companyid . '/invoices/')) {
            mkdir($this->data['setting']->DOC_PATH . '/' . $this->companyid . '/invoices/', 0755, true);
        }
        $invoice_path = $this->data['setting']->DOC_PATH . '/' . $this->companyid . '/invoices/';
        if ($this->companyid == 2) {
            $companyid = array(
                1,
                2,
                3,
                4,
                6,
                8,
                9,
                25,
                26,
                28,
                29,
                31,
                34,
                35,
                37,
                41,
                47,
                50
            );
        } else {
            $companyid[] = $this->companyid;
        }
        if ($this->Admin_model->IsAllowedInvoice($id, $companyid)) {
            if (!file_exists($invoice_path . $id . '.pdf')) {
                $invoice = $this->Admin_model->getInvoicePdf($id);
                //$file = '/tmp/' . $id . '.pdf';
                file_put_contents($invoice_path . $id . '.pdf', $invoice->PDF);
                //copy($file, $this->data['setting']->DOC_PATH . '/invoices/' . $id . '.pdf');
                //unlink($file);
            }
        } else {
        }
    }
    public function getUnPaidInvoice()
    {
        $this->load->library('magebo', array(
            'companyid' => $this->companyid
        ));
        $invoices = $this->magebo->getUnPaidInvoice($this->dpost['iAddressNbr']);
        if ($invoices) {
            foreach ($invoices as $inv) {
                $inv['mPaymentAmount'] = number_format($inv['mPaymentAmount'], 2);
                $res[]                 = $inv;
            }
        } else {
            $res = array();
        }
        echo json_encode($res);
    }
    public function acceptOrder()
    {
        $this->load->model('Admin_model');
        $id = $this->dpost['serviceid'];
        // $this->Teams_model->send_msg($this->uri->segment(3), "<br /><br /> Request:<br />" . print_r($_POST, true) . " " . print_r($_SESSION, true));
        $this->Admin_model->ChangeStatusService($this->dpost['serviceid'], 'Pending');
        if (!empty($this->dpost['msisdn_sim'])) {
            $this->Admin_model->update_services($this->this->dpost['serviceid'], array(
                'packageid' => $this->dpost['packageid'],
                'promocode' => $this->dpost['promo_code'],
                'contract_terms' => $this->dpost['contract_terms'],
                'recurring' => $this->dpost['price'],
                'date_contract' => $this->dpost['date_contract'],
                'status' => 'OrderWaiting'
            ));
            $this->Admin_model->update_services_data('mobile', $this->dpost['serviceid'], array(
                'msisdn_sim' => $this->dpost['msisdn_sim'],
                'date_modified' => date('Y-m-d H:i:s')
            ));
        }
        if (!empty($this->dpost['items'])) {
            foreach ($this->dpost['items'] as $row) {
                if (!empty($row['line']) && !empty($row['amount']) && !empty($row['month'])) {
                    $addins[] = array(
                        'name' => $row['line'],
                        'terms' => $row['month'],
                        'cycle' => 'month',
                        'serviceid' => $this->dpost['serviceid'],
                        'addonid' => 18,
                        'recurring_total' => $row['amount'],
                        'addon_type' => 'others'
                    );
                } else {
                    echo json_encode(array('result' => false, 'error on adding items, please contact our support'));
                    exit;
                }
            }
            $this->db->insert_batch('a_services_addons', $addins);
        }
        if (!empty($this->dpost['options'])) {
            foreach ($this->dpost['options'] as $addonid) {
                $addon_info = getAddonInformation($addonid);
                if ($addon_info->bundle_duration_value == "30" && $addon_info->bundle_duration_value == "day") {
                    $month = 1;
                } else {
                    $month = $addon_info->bundle_duration_value;
                }
                $options[] = array(
                    'name' => $addon_info->name,
                    'terms' => $month,
                    'cycle' => 'month',
                    'serviceid' => $this->dpost['serviceid'],
                    'addonid' => $addonid,
                    'recurring_total' => $addon_info->recurring_total,
                    'addon_type' => $addon_info->bundle_type
                );

                 echo json_encode(array('result' => false, 'error on adding options, please contact our support'));
                    exit;
            }
            $this->db->insert_batch('a_services_addons', $options);
        }
        send_growl(array(
            'message' => $this->session->firstname . ' ' . $this->session->lastname . ' has assign simcard ' . $this->dpost['msisdn_sim'] . ' to  service id: #' . $this->dpost['serviceid'],
            'companyid' => $this->session->cid
        ));
        logAdmin(array(
            'companyid' => $this->companyid,
            'serviceid' => $this->dpost['serviceid'],
            'userid' => $this->dpost['userid'],
            'user' => $this->session->firstname . ' ' . $this->session->lastname,
            'ip' => $_SERVER['REMOTE_ADDR'],
            'description' => 'Order has been accepted and Service : #' . $this->dpost['serviceid'] . ' assigned Simcard: ' . $this->dpost['msisdn_sim']
        ));
        if ($this->data['setting']->mobile_platform == "ARTA") {
            $order = $this->Admin_model->getService($id);
            $this->Admin_model->AddSmsNumber(array(
                'userid' => $this->dpost['userid'],
                'msisdn' => $order->details->msisdn
            ));
            $client = $this->Admin_model->getClient($order->userid);
        } else {
            $sim = simcardExist(trim($this->dpost['msisdn_sim']));
            if ($sim) {
                $this->db->query("update a_reseller_simcard set serviceid=? where simcard=?", array(
                    $this->dpost['serviceid'],
                    trim($this->dpost['msisdn_sim'])
                ));
                $this->Admin_model->update_services_data('mobile', $this->dpost['serviceid'], array(
                    'msisdn_sim' => $this->dpost['msisdn_sim'],
                    'msisdn' => $sim->MSISDN,
                    'platform' => 'TEUM',
                    'msisdn_pin' => $sim->PIN1,
                    'msisdn_sn' => $sim->MSISDN,
                    'msisdn_puk1' => $sim->PUK1,
                    'msisdn_puk2' => $sim->PUK2,
                    'date_modified' => date('Y-m-d H:i:s')
                ));
            }
            $client = $this->Admin_model->getClient($order->userid);
        }
    }
}
