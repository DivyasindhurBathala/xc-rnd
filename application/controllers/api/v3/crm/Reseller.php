<?php
defined('BASEPATH') or exit('No direct script access allowed');
class CrmController extends CI_Controller
{
    protected $data = [];

    public function __construct()
    {
        // Ensure you run parent constructor
        parent::__construct();
        $this->config->set_item('language', 'dutch');
        $this->lang->load('admin');

        header('Content-Type: application/json');
    }
}
class Reseller extends CrmController
{
    public function __construct()
    {
        error_reporting(0);
        parent::__construct();
        $this->load->model("Admin_model");
        $this->load->model("Api_model");
        $this->load->model('Teams_model', 'teams');

        $g = $this->input->request_headers();
        foreach ($g as $key => $header) {
            $headers[strtolower($key)] = $header;
        }
        $url = $this->Api_model->getURL(base_url());

        if ($url) {
            $this->cid = $url->companyid;
        } else {
            echo $this->set_response(array(
                'result' => 'error',
                'message' => 'Authentication required --access--, Unknown Access Point',
                'code' => '100',
            ));
            exit;
        }

        //mail('mail@simson.one','print '.$this->cid, base_url());

        $headers['uri_string'] = $this->uri->uri_string();
        $headers['ip_addr'] = $_SERVER['REMOTE_ADDR'];
        $this->headers = $headers;

        if (isPost()) {
            $post = file_get_contents("php://input");
            $obj = (array) json_decode($post);
            $this->dpost = $obj;
        } else {
            log_message("error", "api pos". file_get_contents("php://input"));
            $post = file_get_contents("php://input");
            $obj = (array) json_decode($post);
            $this->dpost = $obj;
        }
        //$this->teams->send_msg($this->ip . ' :' . $this->uri->segment(4), "<br /><br /> REQUEST:<br />" . print_r($this->dpost, true));
        if (file_exists("/tmp/" . $this->headers['x-api-key'])) {
            sleep(10);
        }
        if (!empty($this->headers['x-api-key'])) {
            $myfile = fopen("/tmp/" . $this->headers['x-api-key'], "w");
            fclose($myfile);
            $validation = $this->Api_model->ValidateKey($this->headers['x-api-key'], $this->cid);
            // mail('mail@simson.one','auth', 'companyid: '.$this->session->cid);
            if (!$validation) {
                header("HTTP/1.1 401 Unauthorized");
                echo $this->set_response(array(
                    'result' => 'error',
                    'message' => 'Incorrect Token',
                    'cid' => $this->cid,
                    'token' => $this->headers['x-api-key']
                ));
                exit;
            }
            $this->companyid = $validation->companyid;
        } else {
            header("HTTP/1.1 401 Unauthorized");
            echo $this->set_response(array(
                'result' => 'error',
                'message' => 'Token is Empty'
            ));
            exit;
        }
        $this->data['setting'] = globofix($this->companyid);
        $this->ip = get_client_ip_env($_SERVER['REMOTE_ADDR'], $this->companyid);

        slack('Request: '.$this->cid.': '.json_encode($this->dpost), 'mvno');
    }

    public function set_response($data)
    {
        $this->dpost['request_uri'] = $this->uri->segment(4);
        $this->dpost['request_ver'] = $this->uri->segment(2);
        //$this->dpost['ip_address'] = $_SERVER['REMOTE_ADDR'];
        $this->teams->send_msg(current_url(), 'Result: '.json_encode($data).' Headers:'.json_encode($this->headers));
        //$this->dpost['companyid'] = $this->companyid;
        $this->teams->insertLog($this->dpost, $data, $_SERVER['REMOTE_ADDR']);
        unlink("/tmp/" . $this->headers['x-api-key']);
        //slack('Response: '.json_encode($data).' Headers: '.json_encode($this->headers), 'mvno');
        return json_encode($data);
    }
    public function index()
    {
        log_message('error', 'Yay here it is'.$this->uri->segment(5));
        if ($_SERVER['REQUEST_METHOD'] == "PUT") {
            $this->AddClient();
        } elseif ($_SERVER['REQUEST_METHOD'] == "PATCH") {
            if (!empty($this->uri->segment(5))) {
                $this->EditClient($this->uri->segment(5));
            } else {
                header("HTTP/1.1 400 Bad Request");
                echo $this->set_response(array(
                    'result' => 'error',
                    'message' => 'Resellerid is required'
                ));
                exit;
            }
        } elseif ($_SERVER['REQUEST_METHOD'] == "GET") {
            if (!empty($this->uri->segment(5))) {
                $this->getAgent($this->uri->segment(5));
            } else {
                $this->getAgents();
            }
        }
    }

    public function getAgents()
    {
       // $this->load->model('Admin_model');
        $agents = $this->Api_model->getAgents($this->companyid);
        echo $this->set_response($agents);
    }

    public function getAgent($id)
    {
        $this->load->model('Agent_model');
        $agents = $this->Agent_model->getAgent($id);
        if ($agents) {
            echo $this->set_response(array('result' => 'success', 'agent' => $agents));
        } else {
            echo $this->set_response(array('result' => 'error', 'message' => 'Agent id does not exists'));
        }
    }
}
