<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class AdminapiController extends CI_Controller {
	protected $data = [];

	public function __construct() {
		// Ensure you run parent constructor
		parent::__construct();
		$this->config->set_item('language', 'english');
		$this->lang->load('admin');
		$this->setProperty();
		header('Content-Type: application/json');
	}

	public function setProperty() {
		$this->data['setting'] = globo();
	}

}
class Adminapi extends AdminApiController {

	public function __construct() {
		error_reporting(1);
		parent::__construct();
		$this->load->model('Teams_model');
		if (isset($_SERVER['HTTP_ORIGIN'])) {
			header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
			header('Access-Control-Allow-Credentials: true');
			header('Access-Control-Max-Age: 86400');
		}
		if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
			if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD'])) {
				header("Access-Control-Allow-Methods: GET");
			}
			if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS'])) {
				header("Access-Control-Allow-Headers:{$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
			}
			exit(0);
		}
		$g = $this->input->request_headers();
		foreach ($g as $key => $header) {
			$headers[strtolower($key)] = $header;

		}
		$headers['uri_string'] = $this->uri->uri_string();
		$headers['ip_addr'] = $_SERVER['REMOTE_ADDR'];
		$this->headers = $headers;
		$ip = $_SERVER['REMOTE_ADDR'];
		$this->ip = $ip;
		if (isPost()) {
			$post = file_get_contents("php://input");
			$obj = (array) json_decode($post);
			$this->dpost = $obj;

		} else {
			$this->dpost = array();
		}
		$this->Teams_model->send_msg($this->ip . ' :' . $this->uri->segment(4), "<br /><br /> REQUEST:<br />" . print_r($this->dpost, true));
	}

	public function getClients(){

		if($_GET['type'] == "all"){
			$this->db->like('firstname', '%'.$_GET['terms'].'%');
			$this->db->or_like('lastname', '%'.$_GET['terms'].'%');
			$q = $this->db->get('a_clients');
		}else{
			$this->db->like('firstname', '%'.$_GET['terms'].'%');
			$this->db->limit('4');
			$q = $this->db->get('a_clients');
		}

		echo json_encode($q->result_array());

	}
	public function login() {
		
		$this->load->model('Auth_model');
		if (!empty($this->dpost['username']) && !empty($this->dpost['password'])) {
			if (isPost()) {
				$valid = $this->Auth_model->admin_auth($this->dpost);
				unset($valid['admin']['password']);
				$valid['validity'] = time()+3600;
				echo json_encode($valid);

			}

		} else {

			echo json_encode(array('result' => false, 'message' => 'Username & Password my not be empty'));
		}

	}


}