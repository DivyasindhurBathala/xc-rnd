<?php
defined('BASEPATH') or exit('No direct script access allowed');
class ResellerController extends CI_Controller
{
    protected $data = [];

    public function __construct()
    {
        // Ensure you run parent constructor
        parent::__construct();
        $this->config->set_item('language', 'dutch');
        $this->lang->load('admin');

        header('Content-Type: application/json');
    }
}
class Reseller extends ResellerController
{
    public function __construct()
    {
        error_reporting(0);
        parent::__construct();
        $this->load->model("Admin_model");
        $this->load->model("Api_model");
        $this->load->model('Teams_model', 'teams');

        $g = $this->input->request_headers();
        foreach ($g as $key => $header) {
            $headers[strtolower($key)] = $header;
        }
        $url = $this->Api_model->getURL(base_url());

        if ($url) {
            $this->cid = $url->companyid;
        } else {
            echo $this->set_response(array(
                'result' => 'error',
                'message' => 'Authentication required --access--, Unknown Access Point',
                'code' => '100',
            ));
            exit;
        }

        //mail('mail@simson.one','print '.$this->cid, base_url());

        $headers['uri_string'] = $this->uri->uri_string();
        $headers['ip_addr'] = $_SERVER['REMOTE_ADDR'];
        $this->headers = $headers;

        if (isPost()) {
            $post = file_get_contents("php://input");
            $obj = (array) json_decode($post);
            $this->dpost = $obj;
        } else {
            $this->dpost = array();
        }
        //$this->teams->send_msg($this->ip . ' :' . $this->uri->segment(4), "<br /><br /> REQUEST:<br />" . print_r($this->dpost, true));
        if (file_exists("/tmp/" . $this->headers['x-api-key'])) {
            sleep(10);
        }
        if (!empty($this->headers['x-api-key'])) {
            $myfile = fopen("/tmp/" . $this->headers['x-api-key'], "w");
            fclose($myfile);
            $validation = $this->Api_model->ValidateResellerKey($this->headers['x-api-key'], $this->cid);
            // mail('mail@simson.one','auth', 'companyid: '.$this->session->cid);
            if (!$validation) {
                header("HTTP/1.1 401 Unauthorized");
                echo $this->set_response(array(
                    'result' => 'error',
                    'message' => 'Incorrect Token'
                ));
                exit;
            }
            $this->companyid = $validation->companyid;
            $this->agentid = $validation->agentid;
        } else {
            header("HTTP/1.1 401 Unauthorized");
            echo $this->set_response(array(
                'result' => 'error',
                'message' => 'Token is Empty'
            ));
            exit;
        }
        $this->data['setting'] = globofix($this->companyid);
        $this->ip = get_client_ip_env($_SERVER['REMOTE_ADDR'], $this->companyid);

        slack('Request: '.$this->cid.': '.json_encode($this->dpost), 'mvno');
    }

    public function set_response($data)
    {
        $this->dpost['request_uri'] = $this->uri->segment(4);
        $this->dpost['request_ver'] = $this->uri->segment(2);
        //$this->dpost['ip_address'] = $_SERVER['REMOTE_ADDR'];
        $this->teams->send_msg(current_url(), 'Result: '.json_encode($data).' Headers:'.json_encode($this->headers));
        //$this->dpost['companyid'] = $this->companyid;
        $this->teams->insertLog($this->dpost, $data, $_SERVER['REMOTE_ADDR']);
        unlink("/tmp/" . $this->headers['x-api-key']);
        //slack('Response: '.json_encode($data).' Headers: '.json_encode($this->headers), 'mvno');
        return json_encode($data);
    }

    public function GetResellerBalance()
    {
        $q = $this->db->query("select * from a_clients_agents where id=?", array($this->agentid));
        if ($q->num_rows()>0) {
            $data = $q->row_array();
            echo $this->set_response(array('result' => 'success', 'balance' => $data['reseller_balance']));
        } else {
            echo $this->set_response(array('result' => 'error', 'message' => 'Error on validating account'));
        }
    }

    public function ReloadMSISDN()
    {
        if(!reseller_allow_perm('reload',  $this->session->reseller['id'])){
            //$this->session->set_flashdata('error', lang('You do not have access to this feature, please contact your Upstream'));

            echo json_encode(array('result'=>'error','message' => lang('You do not have access to this feature, please contact your Upstream')));
            exit;
        }
        $service = $this->Admin_model->getServiceCLI($this->dpost['serviceid']);

        if ($service->details->platform == "ARTA") {
            $this->ReloadCredit_Arta()
        }else{
            $this->ReloadCredit_Teum();
        }
    }
    public function ReloadCredit_Teum()
    {
        if(!reseller_allow_perm('reload',  $this->session->reseller['id'])){
            //$this->session->set_flashdata('error', lang('You do not have access to this feature, please contact your Upstream'));
           echo json_encode(array('result'=>'error','message' => lang('You do not have access to this feature, please contact your Upstream')));
            exit;
        }
        //$this->load->model('Admin_model');
        $req = $this->Api_model->checkRequired(array(
            'msisdn','amount'
        ), $this->dpost);
        if ($req['result'] != "success") {
            echo $this->set_response($req);
            exit;
        }
        if (!isAllowed_Service($this->companyid, $this->dpost['serviceid'])) {
            echo $this->set_response(array('result' => 'error','message' => 'Service not found'));
            exit;
        }
        // $service = $this->Admin_model->getService($this->dpost['serviceid']);
        $service = $this->Admin_model->getServiceCLI($this->dpost['serviceid']);
        $client  = $this->Admin_model->getService($service->userid);
        $this->load->model('Agent_model');
        $agent = $this->Agent_model->getAgent($client->agentid);

        if ($this->dpost['reseller_charge'] == "Yes") {
            if ($agent->reseller_type == "Prepaid") {
                //we need to set time limit to 0 because we are going to lock balance to avoid the same request at the same time
                set_time_limit(0);
                do {
                    if (!file_exists(APPPATH.'lock/reseller_balance_'.$client->agentid)) {
                        log_message('error', 'No locking, we continue adding balance and now we lock the customer balance');
                        file_put_contents(APPPATH.'lock/reseller_'.$client->agentid, $this->dpost['serviceid']);
                        break;
                    }
                } while (true);
            }
            if ($agent->reseller_type == "Prepaid") {
                if ($this->dpost['credit'] > $agent->reseller_balance) {
                    $this->session->set_flashdata('error', lang('Reseller does not have enough balance to topup this subscriber'));
                    unlink(APPPATH.'lock/reseller_balance_'.$client->agentid, $this->dpost['serviceid']);

                    echo $this->set_response(array('result' => 'error', 'message' => 'Reseller does not have enough balance to topup this subscriber'));
                    exit;
                    //redirect('admin/subscription/detail/'.$this->dpost['serviceid']);
                }
            }
        }

        if ($service) {
            if ($service->details->platform == "ARTA") {
                $this->load->library('artilium', array(
                    'companyid' => $this->companyid
                ));




                $res = $this->artilium->CReloadCredit($this->dpost['amount'], $service->details->msisdn_sn, rand(100000000000000, 99999999999999999), 'Ordered by:' . $this->session->firstname);
                if ($res->ReloadResult->Result == 0) {
                    if ($this->dpost['reseller_charge'] == "Yes") {
                        if ($agent->reseller_type == "Prepaid") {
                            $new_balance = $agent->reseller_balance - $this->dpost['amount'];
                            $this->db->query("update a_clients_agents set reseller_balance=? where id=?", array($new_balance, $agent->id));
                            unlink(APPPATH.'lock/reseller_balance_'. $agent->id, $_POST['serviceid']);
                        }
                    }
                    logAdmin(array(
                        'companyid' => $this->companyid,
                        'userid' => $service->userid,
                        'serviceid' => $this->dpost['serviceid'],
                        'user' => 'API',
                        'ip' => $_SERVER['REMOTE_ADDR'],
                        'description' => 'add credit amount of ' . $this->dpost['amount'] . ' to serviceid: ' . $this->dpost['serviceid']
                    ));
                    echo $this->set_response(array(
                        'result' => true,
                        'reload' => $res
                    ));
                } else {
                    echo $this->set_response(array(
                        'result' => false,
                        'data' => $res,
                        'service' => $service
                    ));
                }
            } else {
                $this->load->library('pareteum', array(
                    'companyid' => $this->companyid,
                    'api_id' => $service->api_id
                ));



                $res = $this->pareteum->topup(array(
                    'msisdn' => $service->details->msisdn,
                    'amount' => $this->dpost['amount'] * 100
                ));
                $amount =  $this->dpost['amount'] * 100;

                if ($res->resultCode == "0") {
                    if ($this->dpost['reseller_charge'] == "Yes") {
                        if ($agent->reseller_type == "Prepaid") {
                            $new_balance = $agent->reseller_balance - $this->dpost['amount'];
                            $this->db->query("update a_clients_agents set reseller_balance=? where id=?", array($new_balance, $agent->id));
                            unlink(APPPATH.'lock/reseller_balance_'. $agent->id, $_POST['serviceid']);
                        }
                    }
                    $topupid =   $this->Admin_model->insertTopup(
                        array(
                             'companyid' => $this->companyid,
                            'serviceid' => $this->dpost['serviceid'],
                            'userid' =>  $service->userid,
                            'income_type' => 'topup',
                            'agentid' => $service->agentid,
                            'amount' => $this->dpost['amount'],
                        'user' => 'API User')
                    );
                    echo $this->set_response(array('result' => 'success', 'topupid' => $topupid));
                    logAdmin(array(
                            'companyid' => $this->companyid,
                            'serviceid' => $this->dpost['serviceid'],
                            'userid' =>  $service->userid,
                            'user' => 'API User',
                            'ip' => $_SERVER['REMOTE_ADDR'],
                            'description' => 'Service : ' .  $this->dpost['serviceid'] . ' Topup amount : '. $this->data['setting']->currency.' '. $this->dpost['amount'].' has been loaded to '.$service->details->msisdn
                        ));
                }
            }
        } else {
            echo $this->set_response(array('result' => 'error', 'message' => 'there was an error handling your request'));
        }
    }

    public function ReloadCredit_Arta()
    {
        if(!reseller_allow_perm('reload',  $this->session->reseller['id'])){
            //$this->session->set_flashdata('error', lang('You do not have access to this feature, please contact your Upstream'));
           echo json_encode(array('result'=>'error','message' => lang('You do not have access to this feature, please contact your Upstream')));
            exit;
        }
        //$this->load->model('Admin_model');
        $req = $this->Api_model->checkRequired(array(
            'serviceid','amount'
        ), $this->dpost);
        if ($req['result'] != "success") {
            echo $this->set_response($req);
            exit;
        }
        if (!isAllowed_Service($this->companyid, $this->dpost['serviceid'])) {
            echo $this->set_response(array('result' => 'error','message' => 'Service not found'));
            exit;
        }
        $this->dpost['reseller_charge'] = "Yes";
        // $service = $this->Admin_model->getService($this->dpost['serviceid']);
        $service = $this->Admin_model->getServiceCLI($this->dpost['serviceid']);
        $client  = $this->Admin_model->getService($service->userid);
        $this->load->model('Agent_model');
        $agent = $this->Agent_model->getAgent($client->agentid);


            if ($agent->reseller_type == "Prepaid") {
                //we need to set time limit to 0 because we are going to lock balance to avoid the same request at the same time
                set_time_limit(0);
                do {
                    if (!file_exists(APPPATH.'lock/reseller_balance_'.$client->agentid)) {
                        log_message('error', 'No locking, we continue adding balance and now we lock the customer balance');
                        file_put_contents(APPPATH.'lock/reseller_'.$client->agentid, $this->dpost['serviceid']);
                        break;
                    }
                } while (true);
            }
            if ($agent->reseller_type == "Prepaid") {
                if ($this->dpost['credit'] > $agent->reseller_balance) {
                    $this->session->set_flashdata('error', lang('Reseller does not have enough balance to topup this subscriber'));
                    unlink(APPPATH.'lock/reseller_balance_'.$client->agentid, $this->dpost['serviceid']);

                    echo $this->set_response(array('result' => 'error', 'message' => 'Reseller does not have enough balance to topup this subscriber'));
                    exit;
                    //redirect('admin/subscription/detail/'.$this->dpost['serviceid']);
                }
            }


        if ($service) {
            if ($service->details->platform == "ARTA") {
                $this->load->library('artilium', array(
                    'companyid' => $this->companyid
                ));
                 $res = $this->artilium->CReloadCredit($this->dpost['amount'], $service->details->msisdn_sn, rand(100000000000000, 99999999999999999), 'Ordered by:' . $this->session->firstname);
                if ($res->ReloadResult->Result == 0) {

                        if ($agent->reseller_type == "Prepaid") {
                            $new_balance = $agent->reseller_balance - $this->dpost['amount'];
                            $this->db->query("update a_clients_agents set reseller_balance=? where id=?", array($new_balance, $agent->id));
                            unlink(APPPATH.'lock/reseller_balance_'. $agent->id, $_POST['serviceid']);
                        }

                    logAdmin(array(
                        'companyid' => $this->companyid,
                        'userid' => $service->userid,
                        'serviceid' => $this->dpost['serviceid'],
                        'user' => 'API',
                        'ip' => $_SERVER['REMOTE_ADDR'],
                        'description' => 'add credit amount of ' . $this->dpost['amount'] . ' to serviceid: ' . $this->dpost['serviceid']
                    ));
                    echo $this->set_response(array(
                        'result' => true,
                        'reload' => $res
                    ));
                } else {
                    echo $this->set_response(array(
                        'result' => false,
                        'data' => $res,
                        'service' => $service
                    ));
                }
            } else {
                $this->load->library('pareteum', array(
                    'companyid' => $this->companyid,
                    'api_id' => $service->api_id
                ));



                $res = $this->pareteum->topup(array(
                    'msisdn' => $service->details->msisdn,
                    'amount' => $this->dpost['amount'] * 100
                ));
                $amount =  $this->dpost['amount'] * 100;

                if ($res->resultCode == "0") {
                    if ($this->dpost['reseller_charge'] == "Yes") {
                        if ($agent->reseller_type == "Prepaid") {
                            $new_balance = $agent->reseller_balance - $this->dpost['amount'];
                            $this->db->query("update a_clients_agents set reseller_balance=? where id=?", array($new_balance, $agent->id));
                            unlink(APPPATH.'lock/reseller_balance_'. $agent->id, $_POST['serviceid']);
                        }
                    }
                    $topupid =   $this->Admin_model->insertTopup(
                        array(
                             'companyid' => $this->companyid,
                            'serviceid' => $this->dpost['serviceid'],
                            'userid' =>  $service->userid,
                            'income_type' => 'topup',
                            'agentid' => $service->agentid,
                            'amount' => $this->dpost['amount'],
                        'user' => 'API User')
                    );
                    echo $this->set_response(array('result' => 'success', 'topupid' => $topupid));
                    logAdmin(array(
                            'companyid' => $this->companyid,
                            'serviceid' => $this->dpost['serviceid'],
                            'userid' =>  $service->userid,
                            'user' => 'API User',
                            'ip' => $_SERVER['REMOTE_ADDR'],
                            'description' => 'Service : ' .  $this->dpost['serviceid'] . ' Topup amount : '. $this->data['setting']->currency.' '. $this->dpost['amount'].' has been loaded to '.$service->details->msisdn
                        ));
                }
            }
        } else {
            echo $this->set_response(array('result' => 'error', 'message' => 'there was an error handling your request'));
        }
    }
    public function cmp($a, $b)
    {
        return strcmp($a["date"], $b["date"]);
    }
    public function GetResellerReloadHistory()
    {
        $res = array();
        $debit = $this->db->query("select a.date,'debit' as type,a.user,a.amount,b.msisdn  as description from a_topups a left join a_services_mobile b on b.serviceid=a.serviceid where a.agentid=?", array($this->agentid));
        $credit = $this->db->query("select date,'credit' as type,executor as user, amount,descriptions as description from a_clients_agents_topups where agentid=?", array($this->agentid));

        if ($credit->num_rows()>0) {
            foreach ($credit->result_array() as $row) {
                if ($row['amount'] < 0) {
                    $row['type'] = "debit";
                    $res[] =$row;
                } else {
                    $res[] =$row;
                }
            }
        }

        if ($debit->num_rows()>0) {
            foreach ($debit->result_array() as $row) {
                $row['amount'] = $row['amount']*-1;
                $row['description'] = "Reload Credit : ".$row['description'];
                $res[] =$row;
            }
        }
        usort($res, "cmp");
        echo json_encode(array('result' => 'success', 'history' => $res));
    }

    public function GetResellerAccountDetail()
    {
        $q = $this->db->query("select * from a_clients_agents where id=?", array($this->agentid));
        if ($q->num_rows()>0) {
            $data = $q->row_array();
            unset($data['password']);
            unset($data['reseller_balance']);
            echo $this->set_response(array('result' => 'success', 'detail' => $data));
        } else {
            echo $this->set_response(array('result' => 'error', 'message' => 'Error on validating account'));
        }
    }
}
