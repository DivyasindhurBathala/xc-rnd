<?php
defined('BASEPATH') or exit('No direct script access allowed');
class CrmController extends CI_Controller
{
    protected $data = [];

    public function __construct()
    {
        // Ensure you run parent constructor
        parent::__construct();
        $this->config->set_item('language', 'dutch');
        $this->lang->load('admin');

        header('Content-Type: application/json');
    }
}
class Service extends CrmController
{
    public function __construct()
    {
        error_reporting(0);
        parent::__construct();
        $this->load->model("Admin_model");
        $this->load->model("Api_model");
        $this->load->model('Teams_model', 'teams');

        $g = $this->input->request_headers();
        foreach ($g as $key => $header) {
            $headers[strtolower($key)] = $header;
        }
        $url = $this->Api_model->getURL(base_url());

        if ($url) {
            $this->cid = $url->companyid;
        } else {
            echo $this->set_response(array(
                'result' => 'error',
                'message' => 'Authentication required --access--, Unknown Access Point',
                'code' => '100',
            ));
            exit;
        }

        //mail('mail@simson.one','print '.$this->cid, base_url());

        $headers['uri_string'] = $this->uri->uri_string();
        $headers['ip_addr'] = $_SERVER['REMOTE_ADDR'];
        $this->headers = $headers;

        if (isPost()) {
            $post = file_get_contents("php://input");
            $obj = (array) json_decode($post);
            $this->dpost = $obj;
        } else {
            log_message("error", "api pos". file_get_contents("php://input"));
            $post = file_get_contents("php://input");
            $obj = (array) json_decode($post);
            $this->dpost = $obj;
        }
        //$this->teams->send_msg($this->ip . ' :' . $this->uri->segment(4), "<br /><br /> REQUEST:<br />" . print_r($this->dpost, true));
        if (file_exists("/tmp/" . $this->headers['x-api-key'])) {
            sleep(10);
        }
        if (!empty($this->headers['x-api-key'])) {
            $myfile = fopen("/tmp/" . $this->headers['x-api-key'], "w");
            fclose($myfile);
            $validation = $this->Api_model->ValidateKey($this->headers['x-api-key'], $this->cid);
            // mail('mail@simson.one','auth', 'companyid: '.$this->session->cid);
            if (!$validation) {
                header("HTTP/1.1 401 Unauthorized");
                echo $this->set_response(array(
                    'result' => 'error',
                    'message' => 'Incorrect Token',
                    'cid' => $this->cid,
                    'token' => $this->headers['x-api-key']
                ));
                exit;
            }
            $this->companyid = $validation->companyid;
        } else {
            header("HTTP/1.1 401 Unauthorized");
            echo $this->set_response(array(
                'result' => 'error',
                'message' => 'Token is Empty'
            ));
            exit;
        }
        $this->data['setting'] = globofix($this->companyid);
        $this->ip = get_client_ip_env($_SERVER['REMOTE_ADDR'], $this->companyid);

        slack('Request: '.$this->cid.': '.json_encode($this->dpost), 'mvno');
    }

    public function set_response($data)
    {
        $this->dpost['request_uri'] = $this->uri->segment(4);
        $this->dpost['request_ver'] = $this->uri->segment(2);
        //$this->dpost['ip_address'] = $_SERVER['REMOTE_ADDR'];
        $this->teams->send_msg(current_url(), 'Result: '.json_encode($data).' Headers:'.json_encode($this->headers));
        //$this->dpost['companyid'] = $this->companyid;
        $this->teams->insertLog($this->dpost, $data, $_SERVER['REMOTE_ADDR']);
        unlink("/tmp/" . $this->headers['x-api-key']);
        //slack('Response: '.json_encode($data).' Headers: '.json_encode($this->headers), 'mvno');
        return json_encode($data);
    }
    public function index()
    {
        log_message('error', 'Yay here it is'.$this->uri->segment(5));
        if ($_SERVER['REQUEST_METHOD'] == "PUT") {
            $this->AddClient();
        } elseif ($_SERVER['REQUEST_METHOD'] == "PATCH") {
            $this->EditClient($this->uri->segment(5));
        } elseif ($_SERVER['REQUEST_METHOD'] == "GET") {
            $this->GetService($this->uri->segment(5));
        }
    }
    public function GetService($id)
    {
        $this->dpost['serviceid'] = $id;
        $req = $this->Api_model->checkRequired(array(
            'serviceid',
        ), $this->dpost);
        if (!isAllowed_Service($this->companyid, $this->dpost['serviceid'])) {
            echo $this->set_response(array('result' => 'error','message' => 'Service not found'));
            exit;
        }
        if ($req['result'] == "success") {
            $result = $this->Admin_model->GetService($this->dpost['serviceid']);
            if ($result->details->platform == "TEUM") {
                $res = (array) $result;
                $this->load->library('pareteum', array(
                    'companyid' => $this->companyid,
                    'api_id' => $result->api_id
                                ));
                //print_r($this->data['service']->details);
                $teum = $this->pareteum->accountInformation(array(
                                    'msisdn' => $result->details->msisdn
                             ));
                if ($teum->resultCode == "0") {
                    $t['status'] = $teum->{'card-network-status'};

                    $t['balance'] = $teum->{'remaining-credit'};
                    $t['currency'] = $teum->currency;
                    $t['userid'] = $result->userid;
                    $t['serviceid'] = $result->id;
                    $t['sim'] = array(
                    'msisdn' => $result->details->msisdn,
                    'simcard' => $result->details->msisdn_sim,
                    'pin1' => $result->details->msisdn_pin,
                    'puk1' => $result->details->msisdn_puk1,
                    'puk2' => $result->details->msisdn_puk2);

                    $t['allowance'] = $teum->resources;
                    $t['bundles'] = $this->Admin_model->getOptions($this->dpost['serviceid']);
                }

                $res = $t;
            //$res['extra'] = $result;
            //$res['details'] = $details;
            } else {
                $res = $result;
            }


            echo $this->set_response($res);
        } else {
            echo $this->set_response($req);
        }
    }

    public function activate_mobile_teum($serviceid)
    {
        $service_teum = $this->Admin_model->getServiceCli($serviceid);
        $client =  $this->Admin_model->getClient($service_teum->userid);
        if ($service_teum->details->platform == "TEUM") {

            // print_r($service_teum);
            // exit;
            $this->load->library('pareteum', array(
                'companyid' => $this->companyid,
                'api_id' => $service_teum->api_id
            ));
            if (empty($service_teum->details->teum_accountid)) {
                $account = array(
                    "AccountInfo" => array(
                        "AccountType" => "Prepaid",
                        "CustomerId" => (string) $client->teum_CustomerId,
                        "ExternalAccountId" => (string) $serviceid,
                        "AccountStatus" => "Active",
                        "Names" => array(
                            array(
                                "LanguageCode" => "eng",
                                "Text" => "Account"
                            )
                        ),
                        "Descriptions" => array(
                            array(
                                "LanguageCode" => "eng",
                                "Text" => "Account"
                            )
                        ),
                        "AccountCurrency" => "GBP",
                        "Balance" => 0,
                        "CreditLimit" => 0
                    )
                );
                $acct    = $this->pareteum->CreateAccount($account);
                log_message('error', print_r($acct, true));
                if ($acct->resultCode == "0") {
                    $AccountId = $acct->AccountId;
                } else {
                    die('Error when creating Account');
                }
            } else {
                $AccountId = $service_teum->details->teum_accountid;
            }
            $addons = getaddons($serviceid, $service_teum->details->msisdn_sn);
            $subs   = array(
                "CustomerId" => (int) $client->teum_CustomerId,
                "Items" => array(
                    array(
                        "AccountId" => (string)$AccountId,
                        "ProductOfferings" => $addons,
                        "ServiceAddress" => array(
                            "Address" => $client->address1,
                            "HouseNo" => $client->housenumber,
                            "City" => $client->city,
                            "ZipCode" => $client->postcode,
                            "State" => "unknown",
                            "CountryId" => "76"
                        )
                    )
                ),
                "channel" => "UnitedPortal V1"
            );
            log_message('error', print_r($subs, true));
            $subscription = $this->pareteum->AddSubscription($subs);
            if ($subscription->resultCode == "0") {
                $this->db->query("update a_reseller_simcard set SubscriptionId=?, resellerid=? where serviceid=?", array($subscription->Subscription->SubscriptionId,$client->agentid, $serviceid));
                //mme
                $this->Admin_model->update_services_data('mobile', $serviceid, array('teum_customerid' => $client->teum_CustomerId, 'teum_subscriptionid' => $subscription->Subscription->SubscriptionId));
                foreach ($subscription->Subscription->Products as $key => $row) {
                    foreach ($addons as $key => $r) {
                        if ($r['ProductOfferingId'] == $row->ProductOfferingId) {
                            $addx = getAddonsbyBundleID($row->ProductOfferingId);
                            $days  = ($addx->teum_autoRenew+1)*30;
                            $this->Admin_model->updateAddonTeum($serviceid, $row->ProductOfferingId, array(
                                'companyid' => $this->companyid,
                                'teum_DateStart' => date('Y-m-d'),
                                'teum_NextRenewal' => getFuturedate(date('Y-m-d'), 'day', 31),
                                'teum_DateEnd' => getFuturedate(date('Y-m-d'), 'day', $days),
                                'teum_CustomerOrderId' => $subscription->CustomerOrderId,
                                'teum_SubscriptionId' => $subscription->Subscription->SubscriptionId,
                                'teum_ProductId' => $row->ProductId,
                                'teum_ProductChargePurchaseId' => $row->ProductChargePurchaseId,
                                'teum_SubscriptionProductAssnId' => $row->SubscriptionProductAssnId,
                                'teum_ServiceId' => $subscription->Subscription->Services[$key]->ServiceId
                            ));
                            if ($key == "0") {
                                $this->db->query("update a_reseller_simcard set TeumServiceId=?,CustomerOrderId=? where serviceid=?", array($subscription->Subscription->Services[$key]->ServiceId,$subscription->CustomerOrderId, $serviceid));
                            }
                        }
                    }
                }





                if ($order->details->msisdn_type =="porting") {
                    $this->Admin_model->update_services_data('mobile', $serviceid, array(
                    'msisdn_status' => 'PortinPending',
                    'date_modified' => date('Y-m-d H:i:s'),
                    'teum_accountid' => $AccountId
                    ));
                } else {
                    $this->Admin_model->update_services_data('mobile', $serviceid, array(
                    'msisdn_status' => 'Active',
                    'date_modified' => date('Y-m-d H:i:s'),
                    'teum_accountid' => $AccountId
                    ));
                }



                $this->db->query("update a_services set status='Active' where id=?", array(
                    $serviceid
                ));
                $this->session->set_flashdata('success', lang('Activation has been Requested'));

                send_growl(array(
                    'message' => 'API User activate Number: ' . $order->details->msisdn,
                    'companyid' => $this->session->cid
                ));
                $service = $this->Admin_model->getService($serviceid);
                $this->Admin_model->insertTopup(
                       array(
                       'companyid' => $this->companyid,
                       'serviceid' => $serviceid,
                       'userid' =>  $service->userid,
                       'income_type' => 'bundle',
                       'agentid' => $service->agentid,
                       'amount' => $service->recurring,
                       'user' => 'API User')
                   );


                logAdmin(array(
                    'companyid' => $this->companyid,
                    'serviceid' => $serviceid,
                    'userid' => $client->id,
                    'user' => 'API User',
                    'ip' => $_SERVER['REMOTE_ADDR'],
                    'description' => 'Service : ' . $serviceid . ' '.lang('requested for').' '.$order->details->msisdn_type.' '.lang('Activation').' ' . $sim->MSISDN
                ));
            }
            return array(
                'result' => "success"
            );
        }
    }
    public function activate_mobile_arta($dd)
    {
        $this->teams->send_msg(current_url(), json_encode($this->dpost));
        /*
        $dd =   array("serviceid" => "1804442",
        "companyid" => $companyid,
        "msisdn_sim" => "8931162111842008275",
        "msisdn_type" => "porting",
        "date_contract" => "04-08-2019",
        "donor_msisdn" => "31642458367",
        "donor_provider" => "ETMB-ACHT",
        "donor_type" => "0",
        "donor_sim" => "",
        "donor_accountnumber" => "",
        "date_wish" => "2019-04-09",
        );
         */
        $this->load->model('Admin_model');
        if (isPost()) {
            $this->load->library('artilium', array(
                'companyid' => $dd['companyid'],
            ));
            $this->load->library('magebo', array(
                'companyid' => $dd['companyid'],
            ));
            $serviceid = $dd["serviceid"];
            $date_contract = $dd["date_contract"];
            unset($dd["serviceid"]);
            unset($dd["companyid"]);
            unset($dd["date_contract"]);
            $sn = $this->artilium->getSn($dd['msisdn_sim']);

            if ($sn->result != "success") {
                return array(
                    'result' => 'error',
                    'message' => 'Simcard you provided :' . $dd["msisdn_sim"] . ' is not provisioned, please check the number',
                );
            } //$sn->result != "success"
            $dd["msisdn_sn"] = trim($sn->data->SN);
            $dd["msisdn_puk1"] = $sn->data->PUK1;
            $dd["msisdn_puk2"] = $sn->data->PUK2;
            if ($dd["msisdn_type"] != "porting") {
                $dd["msisdn"] = $sn->data->MSISDNNr;
                $dd["donor_type"] = "";
                $dd["donor_provider"] = "";
                $dd["donor_customertype"] = "";
            }

            $this->Admin_model->updateProductDetails($serviceid, 'mobile', $dd);
            $this->Admin_model->updateContractDate($serviceid, $date_contract);
            // Start Get fresh parameters
            $order = $this->Admin_model->getServiceCli($serviceid);
            $client = $this->Admin_model->getClient($order->userid);
            if ($order->details->msisdn_type == "porting") {
                if (substr($order->details->msisdn, 0, 2) == "31") {
                    if ($dd['date_wish'] < date('Y-m-d')) {
                        return array(
                            'result' => 'error',
                            'message' => 'Your Portin DateWish should be in The future',
                        );
                    }

                    if ($dd['date_wish'] != $order->details->date_wish) {
                        $this->Admin_model->update_services_data('mobile', $serviceid, array(
                            'date_wish' => trim($dd['date_wish']),

                        ));
                        $order = $this->Admin_model->getService($serviceid);
                    }
                }
            }
            $nc = explode("-", $order->date_contract);
            if ($order->details->msisdn_type == "porting") {
                if (substr($order->details->msisdn, 0, 2) == "31") {
                    if (!$order->details->porting_sms) {
                        $this->send_PortinInitiation($serviceid);
                    }
                }
            }
            $result = $this->artilium->ActiveNewSIM($client->firstname . ' ' . $client->lastname, $order->details);

            if ($result->result == "success") {
                logAdmin(array(
                    'companyid' => $this->companyid,
                    'serviceid' => $serviceid,
                    'userid' => $order->userid,
                    'user' => "API",
                    'ip' => $_SERVER['REMOTE_ADDR'],
                    'description' => 'Service : ' . $serviceid . ' requested for Activation',
                ));
                if ($order->details->msisdn_type == "porting") {
                    //$this->send_PortinInitiation($serviceid);
                    $this->Admin_model->ChangeStatusService($serviceid, 'Pending');
                    $this->Admin_model->ChangeStatusOrderID($serviceid, 'PortinPending', 'mobile');
                } //$order->details->msisdn_type == "porting"
                else {
                    if (date('Y-m-d') == $nc[2] . '-' . $nc[0] . '-' . $nc[1]) {
                        if (substr($order->details->msisdn, 0, 2) == "31") {
                            $pack = $this->artilium->GetListPackageOptionsForSnAdvance(trim($order->details->msisdn_sn));
                            $this->artilium->UpdateServices(trim($order->details->msisdn_sn), $pack, '1');
                        }

                        $this->Admin_model->ChangeStatusService($serviceid, 'Active');
                        $this->Admin_model->ChangeStatusOrderID($serviceid, 'Active', 'mobile');
                    } else {
                        $this->Admin_model->ChangeStatusService($serviceid, 'Pending');
                        $this->Admin_model->ChangeStatusOrderID($serviceid, 'ActivationRequested', 'mobile');
                    }
                }
                $mobile = $this->Admin_model->getService($serviceid);
                if (!$noAddSim) {
                    $addsim = $this->magebo->AddSIMToMagebo($mobile);
                    if ($addsim->result == "success") {
                        if (date('Y-m-d') == $nc[2] . '-' . $nc[0] . '-' . $nc[1]) {
                            $this->magebo->addPricingSubscription($mobile);
                        } else {
                            if (substr($mobile->details->msisdn, 0, 2) == "32") {
                                $this->Admin_model->updateContractDate($serviceid, date('m-d-Y'));
                                //$this->magebo->addPricingSubscription($mobile);
                            }
                        }
                        // Add Simcardlog into Magebo
                    } //$addsim->result == "success"
                    //assign Bundles when sim is activated
                    $bundles = $this->Admin_model->getBundlebyProduct($order->packageid);
                    if ($bundles) {
                        $IDS = array();
                        // activate tarief per packages
                        foreach ($bundles as $bundleid) {
                            $r = $this->artilium->AddBundleAssign($order->details->msisdn_sn, $bundleid, date('Y-m-d') . 'T' . date('H:i:s'), '2099-12-31T23:59:59');
                            if ($r->result == "success") {
                                if ($this->data['setting']->create_magebo_bundle) {
                                    $create = 1;
                                } //$this->data['setting']->create_magebo_bundle
                                else {
                                    $create = 0;
                                }
                                $this->magebo->ProcessPricing($order, $client->mageboid, $bundleid, $create, false, $order->date_contract);

                                $pricingid = $this->add_pricing_extra($order->id);
                                if ($pricingid) {
                                    $this->magebo->update_generalPricing($order->id, $pricingid);
                                }
                                $IDS[] = $r->id;
                            } //$r->result == "success"
                            else {
                                $IDS[] = $r;
                            }
                        } //$bundles as $bundleid
                        if ($IDS) {
                            logAdmin(array(
                                'companyid' => $this->companyid,
                                'serviceid' => $serviceid,
                                'userid' =>  $order->userid,
                                'user' => 'API',
                                'ip' => $_SERVER['REMOTE_ADDR'],
                                'description' => 'Service : ' . $dd['serviceid'] . ' added bundle id ' . implode(',', $IDS),
                            ));
                            $this->Admin_model->updateBundleID($order->id, implode(',', $IDS));
                        } //$IDS
                    } //$bundles
                } //!$noAddSim
                //Check if contract date is today otherwise disable all services
                if (date('Y-m-d') <= $nc[2] . '-' . $nc[0] . '-' . $nc[1]) {
                    if (substr(trim($order->details->msisdn), 0, 2) == "31") {
                        $msisdn = $this->artilium->getSn($order->details->msisdn_sim);
                        $pack = $this->artilium->GetListPackageOptionsForSnAdvance($msisdn->data->SN);
                        $this->artilium->UpdateServices($msisdn->data->SN, $pack, '0');
                    } else {
                        $this->artilium->PartialFullBar($order->details, 0);
                    }
                } else {
                    if (substr(trim($order->details->msisdn), 0, 2) == "31") {
                        $msisdn = $this->artilium->getSn($order->details->msisdn_sim);
                        $pack = $this->artilium->GetListPackageOptionsForSnAdvance($msisdn->data->SN);
                        $this->artilium->UpdateServices($msisdn->data->SN, $pack, '1');
                        $this->artilium->UpdateCLI($msisdn->data->SN, 1);
                    }
                }
                if ($dd["msisdn_type"] == "porting") {
                    sleep(2);
                    $this->magebo->addSimcardLog(trim($order->details->msisdn_sim), '599');
                    $this->Admin_model->ChangeStatusOrderID($serviceid, 'PortinPending', 'mobile');
                } else {
                    $this->artilium->UpdateCLI($msisdn->data->SN, 1);
                    $this->magebo->addSimcardLog(trim($order->details->msisdn_sim), '599');
                    sleep(2);
                    $this->magebo->addSimcardLog(trim($order->details->msisdn_sim), '612');
                }
                return array('success', lang('Activation has been Requested'));
            } //$result->result == "success"
            else {
                return array('error', $result->message);
            }
            //redirect('admin/client/detail/' . $client->id);
            // End activating simcard
        } //isPost()
    }
    public function add_pricing_extra($id)
    {
        $ids = array();
        $this->load->model('Admin_model');
        $service = $this->Admin_model->getService($id);
        $client = $this->Admin_model->getClient($service->userid);
        $option = $this->Admin_model->getOptions($service->id);

        if ($service->iGeneralPricingIndex) {
            $ids = explode(',', $service->iGeneralPricingIndex);
        }
        $this->load->library('magebo', array('companyid' => $service->companyid));
        if ($option) {
            foreach ($option as $row) {
                if ($row->recurring_total > 0) {
                    $ids[] = $this->magebo->addExtraPricing($client, $row, str_replace('-', '/', $service->date_contract));
                }
            }

            //$this->Admin_model->updateBundleID($service->id, implode(',', $ids));
            return implode(',', $ids);
        } else {
            return false;
        }
    }
    public function Parameters()
    {
        if ($_SERVER['REQUEST_METHOD'] == "PATCH") {
        } elseif ($_SERVER['REQUEST_METHOD'] == "GET") {
            $this->dpost['serviceid'] = $this->uri->segment(5);
            $req = $this->Api_model->checkRequired(array(
                'serviceid',
            ), $this->dpost);
            if (!isAllowed_Service($this->companyid, $this->dpost['serviceid'])) {
                echo $this->set_response(array('result' => 'error','message' => 'Service not found'));
                exit;
            }
            if ($req['result'] == "success") {
                $mobile = $this->Admin_model->getServiceCli($this->dpost['serviceid']);
                if ($mobile->details->platform == "ARTA") {
                    $this->load->library('artilium', array('companyid' => $this->companyid));
                    $packaged = $this->artilium->GetListPackageOptionsForSnAdvance($mobile->details->msisdn_sn, false, true);
                } else {
                    $packaged = 'not implemented';
                }
               
                echo json_encode(array('result' => 'success', 'parameters' => $packaged));
            } else {
                echo json_encode($req);
            }
        } else {
            header("HTTP/1.1 400 Bad Request");
            echo $this->set_response(array('result' => 'error', 'message' => 'Bad Method only PATCH and GET allowed here'));
            exit;
        }
    }
    public function send_PortinInitiation($id)
    {
        /*if(empty($id)){
        $id = $this->uri->segment(4);
        }
         */
        $this->load->model('Admin_model');
        $mobile = $this->Admin_model->getServiceCli($id);
        $client = $this->Admin_model->getClient($mobile->userid);
        $this->data['setting'] = globofix($client->companyid);
        $this->db->query("update a_services_mobile set porting_sms = ? where serviceid=?", array(1, $id));
        if ($mobile->details->donor_accountnumber) {
            return array('result' => 'succes');
        } else {
            if ($this->data['setting']->enable_sms == "1") {
                $num = trim($mobile->details->msisdn);
                $this->load->library('sms', array('username' => $this->data['setting']->sms_username, 'password' => $this->data['setting']->sms_password, 'companyid' => $this->companyid));
                $sms_res = $this->sms->send_message_bulksms(array(array('from' => $this->data['setting']->sms_senderid, 'to' => '+' . $num, 'body' => $this->data['setting']->sms_content)));
            }

            if ($this->data['setting']->smtp_type == 'smtp') {
                $config = array(
                    'protocol' => 'smtp',
                    'smtp_host' => $this->data['setting']->smtp_host,
                    'smtp_port' => $this->data['setting']->smtp_port,
                    'smtp_user' => $this->data['setting']->smtp_user,
                    'smtp_pass' => $this->encryption->decrypt($this->data['setting']->smtp_pass),
                    'mailtype' => 'html',
                    'charset' => 'utf-8',
                    'starttls' => true,
                    'wordwrap' => true,
                );
            } else {
                $config['protocol'] = 'sendmail';
                $config['mailpath'] = '/usr/sbin/sendmail';
                $config['mailtype'] = 'html';
                $config['charset'] = 'utf-8';
                $config['wordwrap'] = true;
            }
            $this->email->clear(true);
            $this->email->initialize($config);
            $body = getMailContent('portin_initiation', $client->language, $client->companyid);

            $body = str_replace('{$name}', format_name($client), $body);
            $body = str_replace('{$base_url}', url_to_domain(base_url()), $body);
            $body = str_replace('{$msisdn_sim}', $mobile->details->msisdn_sim, $body);
            $body = str_replace('{$msisdn_puk1}', $mobile->etails->msisdn_puk1, $body);
            $body = str_replace('{$msisdn_puk2}', $mobile->etails->msisdn_puk2, $body);
            $body = str_replace('{$msisdn', $mobile->etails->msisdn, $body);

            $this->email->set_newline("\r\n");
            $this->email->from($this->data['setting']->smtp_sender, $this->data['setting']->smtp_name);
            $this->email->to($client->email);
            //$this->email->bcc('mail@simson.one');
            $this->email->subject(getSubject('portin_initiation', $client->language, $client->companyid));
            $this->email->message($body);
            if ($this->email->send()) {
                logEmailOut(array('userid' => $client->id, 'companyid' => $client->companyid, 'to' => $client->email, 'subject' => getSubject('portin_initiation', $client->language, $client->companyid), 'message' => $body));
                return array('result' => 'success');
            } else {
                logEmailOut(array('userid' => $client->id, 'companyid' => $client->companyid, 'to' => $client->email, 'subject' => getSubject('service_change_request', $client->language, $client->companyid), 'message' => $body, 'status' => 'error', 'error_message' => $this->email->print_debugger()));
                return array('result' => 'error', 'message' => 'error when sending email');
            }
        }
    }
    public function Teum_Freeze($serviceid)
    {
        $service = $this->Admin_model->getService($serviceid);
        if ($service) {
            $this->load->library('pareteum', array(
                'companyid' => $this->companyid,
                'api_id' => $service->api_id
            ));
            $res = $this->pareteum->freeze(array(
                'msisdn' => $service->details->msisdn,
                'SubscriptionId' => $this->Admin_model->getTeumSubscriptionId($service->details->msisdn),
                'SubscriptionFreezeReason' => 'FREEZE_CUSTREQUEST',
                'ExternalReference' => $_POST['ExternalRef'],
                'channel' => 'UnitedPortal V1',
                'comments' => 'Freeze by: ' . $this->session->firstname . ' ' . $this->session->lastname
            ));
            echo $this->set_response($res);
        } else {
            die('Access Denied');
        }
    }
    public function Teum_Unfreeze($serviceid)
    {
        $service = $this->Admin_model->getService($serviceid);
        if ($service) {
            $this->load->library('pareteum', array(
                'companyid' => $this->companyid,
                'api_id' => $service->api_id
            ));
            $res = $this->pareteum->unfreeze(array(
                'msisdn' => $service->details->msisdn,
                'SubscriptionId' => $this->Admin_model->getTeumSubscriptionId($service->details->msisdn),
                'ExternalReference' => '',
                'channel' => 'UnitedPortal V1',
                'comments' => 'Freeze by: ' . $this->session->firstname . ' ' . $this->session->lastname
            ));
            echo $this->set_response($res);
        } else {
            die('Access Denied');
        }
    }
    public function StopBundleRenewal()
    {
        $req = $this->Api_model->checkRequired(array(
            'serviceid','bundleid'
        ), $this->dpost);
        if ($req['result'] != "success") {
            echo $this->set_response($req);
            exit;
        }
        if (!isAllowed_Service($this->companyid, $this->dpost['serviceid'])) {
            echo $this->set_response(array('result' => 'error','message' => 'Service not found'));
            exit;
        }
        $this->db->query("update a_services_addons set teum_autoRenew =? where id= ? ", array(0, $this->dpost['bundleid']));
        echo $this->set_response(array('result' => 'success'));
    }
    public function ReloadCredit()
    {

        //$this->load->model('Admin_model');
        $req = $this->Api_model->checkRequired(array(
            'serviceid','amount'
        ), $this->dpost);
        if ($req['result'] != "success") {
            echo $this->set_response($req);
            exit;
        }
        if (!isAllowed_Service($this->companyid, $this->dpost['serviceid'])) {
            echo $this->set_response(array('result' => 'error','message' => 'Service not found'));
            exit;
        }
        // $service = $this->Admin_model->getService($this->dpost['serviceid']);
        $service = $this->Admin_model->getServiceCLI($this->dpost['serviceid']);
        $client  = $this->Admin_model->getService($service->userid);
        $this->load->model('Agent_model');
        $agent = $this->Agent_model->getAgent($client->agentid);

        if ($this->dpost['reseller_charge'] == "Yes") {
            if ($agent->reseller_type == "Prepaid") {
                //we need to set time limit to 0 because we are going to lock balance to avoid the same request at the same time
                set_time_limit(0);
                do {
                    if (!file_exists(APPPATH.'lock/reseller_balance_'.$client->agentid)) {
                        log_message('error', 'No locking, we continue adding balance and now we lock the customer balance');
                        file_put_contents(APPPATH.'lock/reseller_'.$client->agentid, $this->dpost['serviceid']);
                        break;
                    }
                } while (true);
            }
            if ($agent->reseller_type == "Prepaid") {
                if ($this->dpost['credit'] > $agent->reseller_balance) {
                    $this->session->set_flashdata('error', lang('Reseller does not have enough balance to topup this subscriber'));
                    unlink(APPPATH.'lock/reseller_balance_'.$client->agentid, $this->dpost['serviceid']);

                    echo $this->set_response(array('result' => 'error', 'message' => 'Reseller does not have enough balance to topup this subscriber'));
                    exit;
                    //redirect('admin/subscription/detail/'.$this->dpost['serviceid']);
                }
            }
        }

        if ($service) {
            if ($service->details->platform == "ARTA") {
                $this->load->library('artilium', array(
                    'companyid' => $this->companyid
                ));




                $res = $this->artilium->CReloadCredit($this->dpost['amount'], $service->details->msisdn_sn, rand(100000000000000, 99999999999999999), 'Ordered by:' . $this->session->firstname);
                if ($res->ReloadResult->Result == 0) {
                    if ($this->dpost['reseller_charge'] == "Yes") {
                        if ($agent->reseller_type == "Prepaid") {
                            $new_balance = $agent->reseller_balance - $this->dpost['amount'];
                            $this->db->query("update a_clients_agents set reseller_balance=? where id=?", array($new_balance, $agent->id));
                            unlink(APPPATH.'lock/reseller_balance_'. $agent->id, $_POST['serviceid']);
                        }
                    }
                    logAdmin(array(
                        'companyid' => $this->companyid,
                        'userid' => $service->userid,
                        'serviceid' => $this->dpost['serviceid'],
                        'user' => 'API',
                        'ip' => $_SERVER['REMOTE_ADDR'],
                        'description' => 'add credit amount of ' . $this->dpost['amount'] . ' to serviceid: ' . $this->dpost['serviceid']
                    ));
                    echo $this->set_response(array(
                        'result' => true,
                        'reload' => $res
                    ));
                } else {
                    echo $this->set_response(array(
                        'result' => false,
                        'data' => $res,
                        'service' => $service
                    ));
                }
            } else {
                $this->load->library('pareteum', array(
                    'companyid' => $this->companyid,
                    'api_id' => $service->api_id
                ));



                $res = $this->pareteum->topup(array(
                    'msisdn' => $service->details->msisdn,
                    'amount' => $this->dpost['amount'] * 100
                ));
                $amount =  $this->dpost['amount'] * 100;

                if ($res->resultCode == "0") {
                    if ($this->dpost['reseller_charge'] == "Yes") {
                        if ($agent->reseller_type == "Prepaid") {
                            $new_balance = $agent->reseller_balance - $this->dpost['amount'];
                            $this->db->query("update a_clients_agents set reseller_balance=? where id=?", array($new_balance, $agent->id));
                            unlink(APPPATH.'lock/reseller_balance_'. $agent->id, $_POST['serviceid']);
                        }
                    }
                    $topupid =   $this->Admin_model->insertTopup(
                        array(
                             'companyid' => $this->companyid,
                            'serviceid' => $this->dpost['serviceid'],
                            'userid' =>  $service->userid,
                            'income_type' => 'topup',
                            'agentid' => $service->agentid,
                            'amount' => $this->dpost['amount'],
                        'user' => 'API User')
                    );
                    echo $this->set_response(array('result' => 'success', 'topupid' => $topupid));
                    logAdmin(array(
                            'companyid' => $this->companyid,
                            'serviceid' => $this->dpost['serviceid'],
                            'userid' =>  $service->userid,
                            'user' => 'API User',
                            'ip' => $_SERVER['REMOTE_ADDR'],
                            'description' => 'Service : ' .  $this->dpost['serviceid'] . ' Topup amount : '. $this->data['setting']->currency.' '. $this->dpost['amount'].' has been loaded to '.$service->details->msisdn
                        ));
                }
            }
        } else {
            echo $this->set_response(array('result' => 'error', 'message' => 'there was an error handling your request'));
        }
    }
    public function GetServiceByCli()
    {
        $req = $this->Api_model->checkRequired(array(
            'msisdn'
        ), $this->dpost);

        if ($req['result'] != "success") {
            echo $this->set_response($req);
            exit;
        }

        $service = $this->Admin_model->getServiceBySN(trim($this->dpost['msisdn']));


        if ($service) {
            if (!isAllowed_Service($this->companyid, $service->serviceid)) {
                header("HTTP/1.1 404 Not Found");
                echo $this->set_response(array('result' => 'error','message' => 'Service not found'));
                exit;
            } else {
                $client = $this->Admin_model->getClient($service->userid);
                $serv =  (array) $this->Admin_model->getServiceCLI($service->serviceid);
                //ksort($serv);
                echo $this->set_response(array('result'=> 'success', 'service' => $serv, 'client' => $client));
                exit;
            }
        } else {
            header("HTTP/1.1 404 Not Found");
            echo $this->set_response(array('result' => 'error', 'message' => 'No record found'));
            exit;
        }
    }
    public function EnableAutoRenewal()
    {
        $req = $this->Api_model->checkRequired(array(
            'serviceid','bundleid','month'
        ), $this->dpost);

        if (!in_array($this->dpost['month'], array(1,3,6,12,24))) {
            echo $this->set_response(array('result' => 'error', 'message' => 'month possible value are 1, 3, 6, 12, 24'));
            exit;
        }

        if ($req['result'] != "success") {
            echo $this->set_response($req);
            exit;
        }
        if (!isAllowed_Service($this->companyid, $this->dpost['serviceid'])) {
            header("HTTP/1.1 404 Not Found");
            echo $this->set_response(array('result' => 'error','message' => 'Service not found'));
            exit;
        }
        $this->load->model('Admin_model');
        $service = $this->Admin_model->getServiceCli($this->dpost['serviceid']);
        $this->db = $this->load->database('default', true);
        $this->db->query("update a_services_addons set teum_autoRenew =? where id= ? ", array($this->dpost['month'], $this->dpost['bundleid']));
        logAdmin(array(
            'companyid' => $this->companyid,
            'serviceid' => $this->dpost['serviceid'],
            'userid' =>    $service->userid,
            'user' => 'API User',
            'ip' => $_SERVER['REMOTE_ADDR'],
            'description' => 'Service: ' .  $this->dpost['serviceid'] . ' auto renew has been set be disabled  '.$this->dpost['month'].' for addon id: '.$_POST['bundleid']
        ));

        echo $this->set_response(array('result' => 'success'));
    }
    public function AddBundle()
    {
        $req = $this->Api_model->checkRequired(array(
            'serviceid','addonid','month'
        ), $this->dpost);

        if (!in_array($this->dpost['month'], array(1,3,6,12,24,600))) {
            echo $this->set_response(array('result' => 'error', 'message' => 'month possible value are 1, 3, 6, 12, 24, 600'));
            exit;
        }

        if ($req['result'] != "success") {
            echo $this->set_response($req);
            exit;
        }
        if (!isAllowed_Service($this->companyid, $this->dpost['serviceid'])) {
            header("HTTP/1.1 404 Not Found");
            echo $this->set_response(array('result' => 'error','message' => 'Service not found'));
            exit;
        }
        $this->load->model('Admin_model');
        log_message("error", print_r($this->dpost, true));
        $service = $this->Admin_model->getService($this->dpost['serviceid']);
        if ($service) {
            if ($service->details->platform == "TEUM") {
                $this->load->library('pareteum', array(
                    'companyid' => $this->companyid,
                    'api_id' => $service->api_id
                ));
            }
            $reseller_sim_card = $this->db->query("select * from a_reseller_simcard where MSISDN=?", array($service->details->msisdn));

            $client    = $this->Admin_model->getClient($service->userid);
            $addons    = getAddonInformation($this->dpost['addonid']);


            if (!$addons) {
                header("HTTP/1.1 400 Bad Request");
                echo $this->set_response(array('result' => 'error', 'message' => 'Addonid not found'));

                exit;
            }
            if ($addons->companyid != $this->companyid) {
                header("HTTP/1.1 401  No Authorized");
                echo $this->set_response(array('result' => 'error', 'message' => 'Addonid is not accessible'));
                exit;
            }


            if ($service->details->platform == "TEUM") {
                $teum = $this->pareteum->accountInformation(array(
                    'msisdn' => $service->details->msisdn
             ));
                /*
if ($teum->resultCode == "0") {
   if($teum->{'card-network-status'} != "ACTIVATED"){

    header("HTTP/1.1 401  No Authorized");
    echo $this->set_response(array('result' => 'error', 'message' => 'This MSISDN is not in ACTIVATED STATUS'));
    exit;
   }

}else{
    header("HTTP/1.1 401  No Authorized");
    echo $this->set_response(array('result' => 'error', 'message' => 'This MSISDN is not in ACTIVATED STATUS'));
    exit;

}
*/

                //edit now

                $AccountId = $service->details->teum_accountid;
                $subs = array(
                    "CustomerId" => (int) $client->teum_CustomerId,
                    "SubscriptionId" => $reseller_sim_card->row()->SubscriptionId,
                    "Channel" => "UnitedPortal V1",
                    "Offerings" => array(
                        array(
                        "ProductOfferingId" => (int) $addons->bundleid,
                        "OrderedProductCharacteristics" => array(
                            array(
                                "Name" => "MSISDN",
                                "Value" => $service->details->msisdn
                            )
                        )
                        )
                        )

                );

                log_message('error', print_r($subs, true));
                $subscription = $this->pareteum->subscriptions_offerings($subs);
                log_message('error', print_r($subscription, true));
                if ($subscription->resultCode == "0") {
                    if (checkaddon_existance($this->dpost['addonid'], $this->dpost['serviceid'])) {
                        $this->Admin_model->updateAddon(array('teum_autoRenew' => $this->dpost['month']-1,
                        'teum_NextRenewal' => getFuturedate(date('Y-m-d'), 'day', 31)), checkaddon_existance($this->dpost['bundleid'], $this->dpost['serviceid'])->id);
                        $addonidx = checkaddon_existance($this->dpost['addonid'], $this->dpost['serviceid'])->id;
                    } else {
                        $offering = array(
                        'name' => $addons->name,
                        'terms' => $addons->bundle_duration_value,
                        'cycle' => $addons->bundle_duration_type,
                        'serviceid' => $this->dpost['serviceid'],
                        'addonid' => $_POthis->dpostST['addonid'],
                        'companyid' => $this->companyid,
                        'recurring_total' => $addons->recurring_total,
                        'addon_type' => 'option',
                        'arta_bundleid' => $addons->bundleid,
                        'teum_autoRenew' => $_POST['month']-1,
                        'teum_NextRenewal' => getFuturedate(date('Y-m-d'), 'day', 31),
                        'teum_DateStart' => $subscription->PurchaseOrder->CompletionDate,
                        'teum_CustomerOrderId' => $subscription->PurchaseOrder->CustomerOrderId,
                        'teum_SubscriptionId' => $reseller_sim_card->row()->SubscriptionId,
                        'teum_ProductId' => null,
                        'teum_ProductChargePurchaseId' => null,
                        'teum_SubscriptionProductAssnId' => null,
                        'teum_ServiceId' => null
                        );
                        $addonidx =  $this->Admin_model->insertAddon($offering);
                    }
                    $topupid = $this->Admin_model->insertTopup(
                         array(
                            'companyid' => $this->companyid,
                            'serviceid' => $this->dpost['serviceid'],
                            'userid' =>  $service->userid,
                            'income_type' => 'bundle',
                            'agentid' => $client->agentid,
                            'amount' => $addons->recurring_total,
                            'user' => "API")
                     );

                    //$this->session->set_flashdata('success', 'bundle has been added');
                    echo $this->set_response(array('result' => 'success', 'bundleid' => $addonidx, 'orderid' => $topupid));
                    logAdmin(array(
                            'companyid' => $this->companyid,
                            'serviceid' => $this->dpost['serviceid'],
                            'userid' =>  $service->userid,
                            'user' => 'Api',
                            'ip' => $_SERVER['REMOTE_ADDR'],
                            'description' => 'Service : ' .  $this->dpost['serviceid'] . ' Bundle: '.$addons->name.' has been added'
                        ));
                } else {
                    header("HTTP/1.1 500 Teum API error");
                    echo $this->set_response(array('result' => 'error', 'message' => $subscription));
                }
            } else {
                header("HTTP/1.1 400 Bad Request");
                echo $this->set_response(array('result' => 'error', 'message' => 'there was an error handling your request'));
            }
            // redirect('admin/subscription/detail/' . $this->dpost['serviceid']);
        }
    }
    public function MobileUsage()
    {
        $this->dpost['serviceid'] = $this->uri->segment(5);
        $this->dpost['start'] = $_GET['start'];
        $this->dpost['end']= $_GET['end'];


        $start_date = strtotime($this->dpost['start']);
        $end_date = strtotime($this->dpost['end']);
  
       


        $req = $this->Api_model->checkRequired(array(
            'start','end','serviceid'
        ), $this->dpost);
        if ($req['result'] != "success") {
            echo $this->set_response($req);
            exit;
        }
        if ($start_date  > $end_date) {
            echo $this->set_response(array('result' => 'error','message' => 'start date must be lower or equal to end date'));
            exit;
        }
        $diff = ($end_date - $start_date)/60/60/24;

        if ($diff >= 31) {
            echo $this->set_response(array('result' => 'error','message' => 'Maximum range of 31 days exeeded'));
            exit;
        }
        $this->load->model('Admin_model');
        if (!isAllowed_Service($this->companyid, $this->dpost['serviceid'])) {
            echo $this->set_response(array('result' => 'error','message' => 'Service not found'));
            exit;
        }
        $service = $this->Admin_model->getServiceCli($this->dpost['serviceid']);
        if ($service) {
            if ($service->details->platform == "TEUM") {
                $this->load->library('pareteum', array(
                    'companyid' => $this->companyid,
                    'api_id' => $service->api_id
                ));
                $res = $this->pareteum->usage(array(
                    'msisdn' => $service->details->msisdn,
                    'fromDate' => $this->dpost['start'].'T00:00:00',
                    'toDate' => $this->dpost['end'].'T23:59:59'
                ));
            } else {
                $this->load->library('artilium', array(
                    'companyid' => $this->companyid
                ));
                $res = $this->artilium->get_cdr($service);
            }




            echo $this->set_response($res);
        } else {
            echo $this->set_response(array('result'=> 'error', 'message' => 'Service not found'));
        }
    }
}
