<?php
defined('BASEPATH') or exit('No direct script access allowed');
class CrmController extends CI_Controller
{
    protected $data = [];

    public function __construct()
    {
        // Ensure you run parent constructor
        parent::__construct();
        $this->config->set_item('language', 'dutch');
        $this->lang->load('admin');

        header('Content-Type: application/json');
    }
}
class Customer extends CrmController
{
    public function __construct()
    {
        error_reporting(0);
        parent::__construct();
        $this->load->model("Admin_model");
        $this->load->model("Api_model");
        $this->load->model('Teams_model', 'teams');

        $g = $this->input->request_headers();
        foreach ($g as $key => $header) {
            $headers[strtolower($key)] = $header;
        }
        $url = $this->Api_model->getURL(base_url());

        if ($url) {
            $this->cid = $url->companyid;
        } else {
            echo $this->set_response(array(
                'result' => 'error',
                'message' => 'Authentication required --access--, Unknown Access Point',
                'code' => '100',
            ));
            exit;
        }

        //mail('mail@simson.one','print '.$this->cid, base_url());

        $headers['uri_string'] = $this->uri->uri_string();
        $headers['ip_addr'] = $_SERVER['REMOTE_ADDR'];
        $this->headers = $headers;

        if (isPost()) {
            $post = file_get_contents("php://input");
            $obj = (array) json_decode($post);
            $this->dpost = $obj;
        } else {
            log_message("error", "api pos". file_get_contents("php://input"));
            $post = file_get_contents("php://input");
            $obj = (array) json_decode($post);
            $this->dpost = $obj;
        }
        //$this->teams->send_msg($this->ip . ' :' . $this->uri->segment(4), "<br /><br /> REQUEST:<br />" . print_r($this->dpost, true));
        if (file_exists("/tmp/" . $this->headers['x-api-key'])) {
            sleep(10);
        }
        if (!empty($this->headers['x-api-key'])) {
            $myfile = fopen("/tmp/" . $this->headers['x-api-key'], "w");
            fclose($myfile);
            $validation = $this->Api_model->ValidateKey($this->headers['x-api-key'], $this->cid);
            // mail('mail@simson.one','auth', 'companyid: '.$this->session->cid);
            if (!$validation) {
                header("HTTP/1.1 401 Unauthorized");
                echo $this->set_response(array(
                    'result' => 'error',
                    'message' => 'Incorrect Token',
                    'cid' => $this->cid,
                    'token' => $this->headers['x-api-key']
                ));
                exit;
            }
            $this->companyid = $validation->companyid;
        } else {
            header("HTTP/1.1 401 Unauthorized");
            echo $this->set_response(array(
                'result' => 'error',
                'message' => 'Token is Empty'
            ));
            exit;
        }
        $this->data['setting'] = globofix($this->companyid);
        $this->ip = get_client_ip_env($_SERVER['REMOTE_ADDR'], $this->companyid);

        slack('Request: '.$this->cid.': '.json_encode($this->dpost), 'mvno');
    }

    public function set_response($data)
    {
        $this->dpost['request_uri'] = $this->uri->segment(4);
        $this->dpost['request_ver'] = $this->uri->segment(2);
        //$this->dpost['ip_address'] = $_SERVER['REMOTE_ADDR'];
        $this->teams->send_msg(current_url(), 'Result: '.json_encode($data).' Headers:'.json_encode($this->headers));
        //$this->dpost['companyid'] = $this->companyid;
        $this->teams->insertLog($this->dpost, $data, $_SERVER['REMOTE_ADDR']);
        unlink("/tmp/" . $this->headers['x-api-key']);
        //slack('Response: '.json_encode($data).' Headers: '.json_encode($this->headers), 'mvno');
        return json_encode($data);
    }
    public function index()
    {
        log_message('error','Yay here it is'.$this->uri->segment(5));
        if ($_SERVER['REQUEST_METHOD'] == "PUT") {
            $this->AddClient();
        } elseif ($_SERVER['REQUEST_METHOD'] == "PATCH") {
            $this->EditClient($this->uri->segment(5));
        } elseif ($_SERVER['REQUEST_METHOD'] == "GET") {
            $this->GetClient($this->uri->segment(5));
        }
    }
    private function GetClient($id)
    {
        log_message('error','customeris '.$id);
        $this->dpost['clientid'] = $id;
        $req = $this->Api_model->checkRequired(array(
            'clientid',
        ), $this->dpost);

        if (!isAllowed_Clientid($this->companyid, $this->dpost['clientid'])) {
            header("HTTP/1.1 404 Not Found");
            echo $this->set_response(array('result' => 'error', 'message'=> 'Client not found'));

            exit;
        }
        if ($req['result'] == "success") {
            $result = $this->Admin_model->getClient($this->dpost['clientid']);
            $a = (array) $result;
            unset($a['notes']);
            unset($a['password']);
            $rr['result'] = "success";
            $rr['client'] = $a;
            echo $this->set_response($rr);
        } else {
            echo $this->set_response($req);
        }
    }
    private function AddClient()
    {
        $this->teams->send_msg(current_url(), 'Result :'.json_encode($this->dpost). 'Headers :'.json_encode($this->headers));
        $allowed_payments = array(
            'directdebit',
            'banktransfer',
            'creditcard',
            'online',
            'paypal',
            'mollie',
            'sisow',
            'others',
            'payu'
        );
        $this->load->helper('string');
        $this->load->model('Admin_model');


        if ($this->data['setting']->mobile_platform == "TEUM") {
            $required = array(
                'firstname',
                'lastname',
                'phonenumber',
                'email',
                'address1',
                'housenumber',
                'postcode',
                'city',
                'country',
                'language',
                'id_type',
                'idcard_number',
                'agentid'
            );

            if (!isAllowed_reseller_companyid($this->dpost['agentid'], $this->companyid)) {
                echo $this->set_response(array('result' => 'error','message' => 'Agentid provided does not exists check the list on getAgents'));
                exit;
            }
        } else {
            $required = array(
                'firstname',
                'lastname',
                'phonenumber',
                'email',
                'address1',
                'postcode',
                'city',
                'country',
                'language',
                'paymentmethod',
                'iban'
            );
        }
        $req = $this->Api_model->checkRequired($required, $this->dpost);

        $err = array();
        $this->dpost['iban'] = str_replace(' ', '', trim($this->dpost['iban']));

        if (!empty($this->dpost['iban'])) {
            if (iban_block($this->companyid, trim($_POST['iban']))) {
                echo $this->set_response(array(
                    'result' => 'error',
                    'message' => 'You may not use this IBAN',
                ));
                exit;
            }
        }
        if (!empty($this->dpost['salutation'])) {
            if ($this->dpost['salutation'] == "meneer") {
                $this->dpost['salutation'] = "Dhr.";
            } elseif ($this->dpost['salutation'] == "mevrouw") {
                $this->dpost['salutation'] = "Mevr.";
            }
        }
        if ($this->dpost['paymentmethod'] == "directdebit") {
            if (!empty($this->dpost['iban'])) {
                $bic = $this->Api_model->getBic(trim($this->dpost['iban']));
                if (!$bic->valid) {
                    $err[] = "Your IBAN: " . $this->dpost['iban'] . " does not pass Validation";
                } else {
                    $this->dpost['bic'] = $bic->bankData->bic;
                }
            } else {
                $err[] = "IBAN is required for SEPA directdebit ";
            }
        } else {
            if (!empty($this->dpost['iban'])) {
                $bic = $this->Api_model->getBic(trim($this->dpost['iban']));
                if (!$bic->valid) {
                    $err[] = "Your IBAN: " . $this->dpost['iban'] . " does not pass Validation";
                } else {
                    $this->dpost['bic'] = $bic->bankData->bic;
                }
            }
        }

        if ($err) {
            echo $this->set_response(array(
                'result' => 'error',
                'message' => implode(', ', $err),
            ));
            exit;
        }

        if ($req['result'] == "success") {
            if (empty($this->dpost['password2'])) {
                $password = random_string('alnum', 10);
                $this->dpost['password'] = password_hash(random_string('alnum', 10), PASSWORD_DEFAULT);
            } else {
                $this->dpost['password'] = password_hash(strtolower(trim($this->dpost['password2'])), PASSWORD_DEFAULT);
                $password = $this->dpost['password2'];
            }
            unset($this->dpost['password2']);
            if (!empty($this->dpost['vatexempt'])) {
                if ($this->dpost['vatexempt'] == "1") {
                    $this->dpost['vat_exempt'] == "1";
                } else {
                    $this->dpost['vat_exempt'] == "0";
                }
            } else {
                $this->dpost['vat_exempt'] == "0";
            }

            if (!empty($this->dpost['delta_username'])) {
                $deltausername = $this->dpost['delta_username'];
            } else {
                $deltausername = false;
            }

            unset($this->dpost['delta_username']);

            /* start new way */
            if (!empty($this->dpost['relationid'])) {
                $this->dpost['mvno_id'] = strtoupper(trim($this->dpost['relationid']));
            }


            if ($this->data['setting']->mvno_id_increment == 1) {
                $this->dpost['mvno_id'] = genMvnoId($this->session->cid);
            }
            unset($this->dpost['relationid']);
            if ($_POST['country'] == "NL") {
                $pcode = explode(' ', trim($_POST["postcode"]));
                if (count($pcode) != 2) {
                    echo $this->set_response(array(
                        'result' => 'error',
                        'message' => lang('Postcode moet: NNNN XX formaat (100)'),
                    ));
                    exit;
                } else {
                    if (strlen($pcode[0]) != 4) {
                        echo $this->set_response(array(
                            'result' => 'error',
                            'message' => lang('Postcode moet: NNNN XX formaat (100)'),
                        ));
                        exit;
                    } elseif (strlen($pcode[1]) != 2) {
                        echo $this->set_response(array(
                            'result' => 'error',
                            'message' => lang('Postcode moet: NNNN XX formaat (100)'),
                        ));
                        exit;
                    }
                }
            } elseif ($_POST['country'] == "BE") {
                $pcode = explode(' ', 'BE ', trim($this->dpost["postcode"]));
            } else {
                $pcode = trim($this->dpost["postcode"]);
            }

            if (!empty($_POST['iban'])) {
                $bic = $this->Api_model->getBic(trim($this->dpost['iban']));
                if (!$bic->valid) {
                } else {
                    $this->dpost['bic'] = $bic->bankData->bic;
                }
            }

            $this->dpost['date_created'] = date('Y-m-d');
            $this->dpost['email'] = strtolower(trim($this->dpost['email']));
            $this->dpost['companyid'] = $this->companyid;
            if ($this->data['setting']->mage_invoicing == 1) {
                $this->load->library('magebo', array(
                    'companyid' => $this->companyid,
                ));
                $magebo = $this->magebo->AddClient($this->dpost);
                if ($magebo->result == "success") {
                    $this->dpost['mageboid'] = $magebo->iAddressNbr;
                    $this->magebo->addMageboSEPA($this->dpost['iban'], $this->dpost['bic'], $magebo->iAddressNbr);
                } else {
                    echo $this->set_response($magebo);
                    exit;
                }
            }
            $this->dpost['date_modified'] = date('Y-m-d H:i:s');
            $gg = explode(' ', trim($this->dpost['address1']));
            if (count($gg) == 2) {
                $this->dpost['address1'] = trim($gg[0]);
                $this->dpost['housenumber'] = trim($gg[1]);
            }



            $i = $this->Admin_model->insertCustomer($this->dpost);

            $result = (object) $i;
            if ($result->result) {
                if ($this->data['setting']->mobile_platform == "TEUM") {
                    $this->load->library('pareteum', array('companyid' => $this->companyid));
                    $array = array(
                    "CustomerData" => array(
                        "ExternalCustomerId" => (string) $this->dpost['mvno_id'],
                        "FirstName" => $this->dpost['firstname'],
                        "LastName" => $this->dpost['lastname'],
                        "LastName2" => 'NA',
                        "CustomerDocumentType" => $this->dpost['id_type'],
                        "DocumentNumber" => $this->dpost['idcard_number'],
                        "Telephone" => $this->dpost['phonenumber'],
                        "Email" => $this->dpost['email'],
                        "LanguageName" => "eng",
                        "FiscalAddress" => array(
                            "Address" => $this->dpost['address1'],
                            "City" =>$this->dpost['city'],
                            "CountryId" => "76",
                            "HouseNo" => $this->dpost['housenumber'],
                            "State" => "Unknown",
                            "ZipCode" => $this->dpost['postcode'],
                        ),
                        "CustomerAddress" => array(
                            "Address" => $this->dpost['address1'],
                            "City" => $this->dpost['city'],
                            "CountryId" => "76",
                            "HouseNo" => $this->dpost['housenumber'],
                            "State" => "Unknown",
                            "ZipCode" => $this->dpost['postcode']
                        ),
                        "Nationality" => "GB"
                    ),
                    "comments" => "Customer added via UnitedPortal V1 by Portal API Token :".$this->headers['x-api-key']
                    );
                    log_message('error', json_encode($array));
                    $teum = $this->pareteum->AddCustomers($array);
                    log_message('error', json_encode($teum));
                    if ($teum->resultCode == "0") {
                        $this->db->query("update a_clients set teum_CustomerId=?, teum_OrderCode=? where id=?", array($teum->customerId, $teum->orderCode, $result->id));
                    } else {
                        $this->db->query("delete from a_clients where id=?", array($i['id']));

                        echo $this->set_response(array("result" => "error","message" => $teum->messages));
                        exit;
                    }
                }





                if (empty($this->dpost['housenumber'])) {
                    $headers = "From: noreply@united-telecom.be";
                    $msg = "Hello\n\nThis is a warning to inform you that you have added customer via webservices without providing housenumber.\n\nPlease login to United Portal and edit this customer to avoid Billing address Issue\n\nPlease edit this customer by visiting this url on the spot: ".base_url()."admin/client/edit/".$result->id."\n\nThis is an automate reminder\n\n";
                    $msg .=print_r($this->dpost, true);

                    $notification = explode('|', $this->data['setting']->email_notification);
                    if (count($notification) >= 1) {
                        foreach ($notification as $em) {
                            mail($em, 'AddClient without Housenumber Warning via API: '.$result->id, $msg, $headers);
                        }
                    }
                }

                $client = $this->Admin_model->getClient($result->id);
                $password = $this->Admin_model->changePasswordClient($client->id);
                if (!empty($deltausername)) {
                    if (validate_deltausername(strtolower($deltausername))) {
                        $this->Admin_model->InsertDeltaUsername($deltausername, $result->id);
                    }
                }

                if ($this->data['setting']->smtp_type == 'smtp') {
                    $config = array(
                        'protocol' => 'smtp',
                        'smtp_host' => $this->data['setting']->smtp_host,
                        'smtp_port' => $this->data['setting']->smtp_port,
                        'smtp_user' => $this->data['setting']->smtp_user,
                        'smtp_pass' => $this->data['setting']->smtp_pass,
                        'mailtype' => 'html',
                        'charset' => 'utf-8',
                        'starttls' => true,
                        'wordwrap' => true,
                    );
                } else {
                    $config['protocol'] = 'sendmail';
                    $config['mailpath'] = '/usr/sbin/sendmail';
                    $config['mailtype'] = 'html';
                    $config['charset'] = 'utf-8';
                    $config['wordwrap'] = true;
                }
                $this->email->clear(true);
                $this->email->initialize($config);
                $this->data['language'] = "dutch";
                $this->data['info'] = (object) array(
                    'email' => $client->email,
                    'password' => $password,
                    'name' => $client->firstname . ' ' . $client->lastname,
                );
                //$body = $this->load->view('email/resetpassword.php', $this->data, true);
                //$this->data['main_content'] = 'email/' . $this->data['language'] . '/resetpassword.php';
                //$body = $this->load->view('email/content', $this->data, true);

                $body = getMailContent('signup_email', $client->language, $client->companyid);
                // $body = $this->load->view('email/content', $this->data, true);
                $body = str_replace('{$name}', $client->firstname . ' ' . $client->lastname, $body);
                $body = str_replace('{$password}', $password, $body);
                $body = str_replace('{$email}', trim(strtolower($client->email)), $body);
                $body = str_replace('{$base_url}', url_to_domain(base_url()), $body);
                $subject = getSubject('signup_email', $client->language, $client->companyid);
                $this->email->set_newline("\r\n");
                $this->email->from($this->data['setting']->smtp_sender, $this->data['setting']->smtp_name);
                $this->email->to($client->email);
                $this->email->subject($subject);
                //$this->email->subject(lang("Your New Password"));
                $this->email->message($body);
                if ($this->email->send()) {
                    logEmailOut(array(
                        'userid' => $client->id,
                        'companyid' => $client->companyid,
                        'to' => $client->email,
                        'subject' => $subject,
                        'message' => $body,
                    ));
                }
            }
            if ($result->result) {
                log_message("error", 'Adding customer Id: ' . $result->id . ' and MageboId:' . $magebo->iAddressNbr);
                logAdmin(array(
                    'companyid' => $this->companyid,
                    'userid' => $result->id,
                    'user' => 'Api User',
                    'ip' => $this->ip,
                    'description' => 'Adding customer Id: ' . $result->id . ' and MageboId:' . $magebo->iAddressNbr,
                ));
                send_growl(array(
                'message' => 'New customer has just been registered via webservices with BillingID: '.$magebo->iAddressNbr,
                'companyid' => $client->companyid));

                echo $this->set_response(array(
                    'result' => 'success',
                    'clientid' => $result->id,
                ));
            } else {
                echo $this->set_response(array(
                    'result' => 'error',
                    'message' => $result->message,
                ));
            }
        } else {
            echo $this->set_response($req);
        }
    }

    private function EditClient($clientid)
    {
        $this->dpost['clientid'] = $clientid;
        $required = array(
            'clientid'
        );
        $req = $this->Api_model->checkRequired($required, );
        if (!empty($this->dpost['password2'])) {
            $password = password_hash(trim($this->dpost['password2']), PASSWORD_DEFAULT);
            $this->dpost['password'] = $password;
            unset($this->dpost['password2']);
        }
        $d = $this->Admin_model->getClient($this->dpost['clientid']);
        if ($req['result'] != "success") {
            header("HTTP/1.1 400 Bad Request");
            echo $this->set_response(array('result' => 'error', 'message' => 'Bad request, clientid is required'));
            exit;
        }
        if ($d->companyid == $this->companyid) {
            if ($this->data['setting']->mage_invoicing == "1") {
                $this->load->library('magebo', array('companyid' => $this->companyid));
                $_POST['date_modified'] = date('Y-m-d H:i:s');
                $result = $this->magebo->updateClient($d->mageboid, $_POST, $d->companyid);
            }
            $id  = $this->dpost['clientid'];
            unset($this->dpost['clientid']);
            $result  = $this->Admin_model->UpdateClient($id, $this->dpost);
        } else {
            header("HTTP/1.1 404 Not Found");
            echo $this->set_response(array('result' => 'error', 'message' => 'Clientid Not found'));
            exit;
        }


        echo $this->set_response(array('result' => 'success'));
    }

    public function Services()
    {
        
        $this->dpost['clientid'] = $this->uri->segment(5);
        log_message("error",print_r($this->dpost, 1));
        $this->teams->send_msg(current_url(), json_encode($this->dpost));
       
        $req = $this->Api_model->checkRequired(array(
            'clientid',
        ), $this->dpost);
        if ($req['result'] == "success") {
            $result = $this->Admin_model->getServices($this->dpost['clientid']);
            echo $this->set_response(array('result' => 'success', 'services' => $result));
        } else {
            echo $this->set_response($req);
        }
    }
    public function Invoices()
    {
        $this->dpost['clientid'] = $this->uri->segment(5);
        $this->teams->send_msg(current_url(), json_encode($this->dpost));
        $req = $this->Api_model->checkRequired(array(
            'clientid',
        ), $this->dpost);
        if ($req['result'] == "success") {
            $this->dpost['companyid'] = $this->companyid;
            $result = $this->Api_model->getClientInvoices($this->dpost);
            echo $this->set_response($result);
        } else {
            echo $this->set_response($req);
        }
    }
    /*
    public function Invoice()
    {
        $this->dpost['clientid'] = $this->uri->segment(6);
        $req = $this->Api_model->checkRequired(array(
            'invoiceid',
        ), $this->dpost);

        
        if ($req['result'] == "success") {
            $this->load->library('magebo', array('companyid' => $this->companyid));
            $invoice = $this->magebo->getInvoice($this->dpost['invoiceid']);

            if (!iAllowedInvoice($this->companyid, $invoice->iAddressNbr)) {
                echo $this->set_response(array('result' => 'error','message' => 'Invoice Not found'));
            }
            $lines = $this->magebo->getInvoiceDetail($this->dpost['invoiceid']);
            echo $this->set_response(array('result' => 'success' , 'invoice' => $invoice, 'lines' => $lines));
        } else {
            echo $this->set_response($req);
        }
    }
    */


}
