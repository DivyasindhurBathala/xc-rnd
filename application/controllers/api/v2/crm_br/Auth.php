<?php
defined('BASEPATH') or exit('No direct script access allowed');
class CrmController extends CI_Controller
{
    protected $data = [];

    public function __construct()
    {
        // Ensure you run parent constructor
        parent::__construct();
        $this->config->set_item('language', 'dutch');
        $this->lang->load('admin');

        header('Content-Type: application/json');
    }
}
class Auth extends CrmController
{
    public function __construct()
    {
        error_reporting(0);
        parent::__construct();
        $this->load->model("Admin_model");
        $this->load->model("Api_model");
        $this->load->model('Teams_model', 'teams');

        $g = $this->input->request_headers();
        foreach ($g as $key => $header) {
            $headers[strtolower($key)] = $header;
        }
        $url = $this->Api_model->getURL(base_url());

        if ($url) {
            $this->cid = $url->companyid;
        } else {
            echo $this->set_response(array(
                'result' => 'error',
                'message' => 'Authentication required --access--, Unknown Access Point',
                'code' => '100',
            ));
            exit;
        }

        //mail('mail@simson.one','print '.$this->cid, base_url());

        $headers['uri_string'] = $this->uri->uri_string();
        $headers['ip_addr'] = $_SERVER['REMOTE_ADDR'];
        $this->headers = $headers;

        if (isPost()) {
            $post = file_get_contents("php://input");
            $obj = (array) json_decode($post);
            $this->dpost = $obj;
        } else {
            log_message("error", "api pos". file_get_contents("php://input"));
            $post = file_get_contents("php://input");
            $obj = (array) json_decode($post);
            $this->dpost = $obj;
        }
        //$this->teams->send_msg($this->ip . ' :' . $this->uri->segment(4), "<br /><br /> REQUEST:<br />" . print_r($this->dpost, true));
        if (file_exists("/tmp/" . $this->headers['x-api-key'])) {
            sleep(10);
        }
        if (!empty($this->headers['x-api-key'])) {
            $myfile = fopen("/tmp/" . $this->headers['x-api-key'], "w");
            fclose($myfile);
            $validation = $this->Api_model->ValidateKey($this->headers['x-api-key'], $this->cid);
            // mail('mail@simson.one','auth', 'companyid: '.$this->session->cid);
            if (!$validation) {
                header("HTTP/1.1 401 Unauthorized");
                echo $this->set_response(array(
                    'result' => 'error',
                    'message' => 'Incorrect Token',
                    'cid' => $this->cid,
                    'token' => $this->headers['x-api-key']
                ));
                exit;
            }
            $this->companyid = $validation->companyid;
        } else {
            header("HTTP/1.1 401 Unauthorized");
            echo $this->set_response(array(
                'result' => 'error',
                'message' => 'Token is Empty'
            ));
            exit;
        }
        $this->data['setting'] = globofix($this->companyid);
        $this->ip = get_client_ip_env($_SERVER['REMOTE_ADDR'], $this->companyid);

        slack('Request: '.$this->cid.': '.json_encode($this->dpost), 'mvno');
    }

    public function set_response($data)
    {
        $this->dpost['request_uri'] = $this->uri->segment(4);
        $this->dpost['request_ver'] = $this->uri->segment(2);
        //$this->dpost['ip_address'] = $_SERVER['REMOTE_ADDR'];
        $this->teams->send_msg(current_url(), 'Result: '.json_encode($data).' Headers:'.json_encode($this->headers));
        //$this->dpost['companyid'] = $this->companyid;
        $this->teams->insertLog($this->dpost, $data, $_SERVER['REMOTE_ADDR']);
        unlink("/tmp/" . $this->headers['x-api-key']);
        //slack('Response: '.json_encode($data).' Headers: '.json_encode($this->headers), 'mvno');
        return json_encode($data);
    }
    public function index()
    {
        $required = array('email','password');
        $req = $this->Api_model->checkRequired($required, $this->dpost);
        if ($req['result'] == "success") {
            $validation = $this->Api_model->auth($this->dpost, $this->cid);
        } else {
            echo $this->set_response($req);
            exit;
        }

        echo $this->set_response($validation);
    }
    public function ResetPassword()
    {
        $this->load->model('Auth_model');
        $required = array('email');
        $req = $this->Api_model->checkRequired($required, $this->dpost);
        if ($req['result'] == "success") {
            // $this->load->helper('encryption');
            $this->db = $this->load->database('default', true);
            $client = $this->db->query('select * from a_clients where email like ? and companyid= ? ', array($this->dpost['email'], $this->companyid));
            if ($client->num_rows() > 0) {
                $this->config->set_item('language', $client->row()->language);
                $code = $this->Auth_model->addClientCode(random_str('alphanum', '6'), $client->row()->id);
                if ($this->data['setting']->smtp_type == 'smtp') {
                    $config = array(
                        'protocol' => 'smtp',
                        'smtp_host' => $this->data['setting']->smtp_host,
                        'smtp_port' => $this->data['setting']->smtp_port,
                        'smtp_user' => $this->data['setting']->smtp_user,
                        'smtp_pass' => $this->encryption->decrypt($this->data['setting']->smtp_pass),
                        'mailtype' => 'html',
                        'charset' => 'utf-8',
                        'starttls' => true,
                        'wordwrap' => true,
                    );
                } else {
                    $config['protocol'] = 'sendmail';
                    $config['mailpath'] = '/usr/sbin/sendmail';
                    $config['mailtype'] = 'html';
                    $config['charset'] = 'utf-8';
                    $config['wordwrap'] = true;
                }
                $this->email->initialize($config);

                $this->data['language'] = "dutch";
                //  $this->data['main_content'] = 'email/' . $this->data['language'] . '/clientresetpassword.php';
                // $body = $this->load->view('email/content', $this->data, true);

                $body = getMailContent('clientresetpassword', $client->row()->language, $this->companyid);
                // $body = $this->load->view('email/content', $this->data, true);
                $body = str_replace('{$name}', $client->row()->salutation . ' ' . $client->row()->firstname . ' ' . $client->row()->lastname, $body);
                $body = str_replace('{$code}', $code, $body);
                $body = str_replace('{$base_url}', url_to_domain(base_url()), $body);

                $this->email->set_newline("\r\n");
                $this->email->from($this->data['setting']->smtp_sender, $this->data['setting']->smtp_name);
                $this->email->to($this->dpost['email']);
                $this->email->subject(getSubject('clientresetpassword', $client->row()->language, $client->row()->companyid));
                $this->email->message($body);
                if ($this->email->send()) {
                    echo json_encode(array('result' => 'success'));
                } else {
                    echo json_encode(array('result' => 'error', 'error' => $this->email->print_debugger()));
                }
            } else {
                echo json_encode(array('result' => 'error', 'error' => 'email not found in the system'));
            }
        } else {
            echo $this->set_response($req);
            exit;
        }
    }
}
