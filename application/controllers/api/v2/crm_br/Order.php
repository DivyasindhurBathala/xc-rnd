<?php
defined('BASEPATH') or exit('No direct script access allowed');
class CrmController extends CI_Controller
{
    protected $data = [];

    public function __construct()
    {
        // Ensure you run parent constructor
        parent::__construct();
        $this->config->set_item('language', 'dutch');
        $this->lang->load('admin');

        header('Content-Type: application/json');
    }
}
class Customer extends CrmController
{
    public function __construct()
    {
        error_reporting(0);
        parent::__construct();
        $this->load->model("Admin_model");
        $this->load->model("Api_model");
        $this->load->model('Teams_model', 'teams');

        $g = $this->input->request_headers();
        foreach ($g as $key => $header) {
            $headers[strtolower($key)] = $header;
        }
        $url = $this->Api_model->getURL(base_url());

        if ($url) {
            $this->cid = $url->companyid;
        } else {
            echo $this->set_response(array(
                'result' => 'error',
                'message' => 'Authentication required --access--, Unknown Access Point',
                'code' => '100',
            ));
            exit;
        }

        //mail('mail@simson.one','print '.$this->cid, base_url());

        $headers['uri_string'] = $this->uri->uri_string();
        $headers['ip_addr'] = $_SERVER['REMOTE_ADDR'];
        $this->headers = $headers;

        if (isPost()) {
            $post = file_get_contents("php://input");
            $obj = (array) json_decode($post);
            $this->dpost = $obj;
        } else {
            log_message("error", "api pos". file_get_contents("php://input"));
            $post = file_get_contents("php://input");
            $obj = (array) json_decode($post);
            $this->dpost = $obj;
        }
        //$this->teams->send_msg($this->ip . ' :' . $this->uri->segment(4), "<br /><br /> REQUEST:<br />" . print_r($this->dpost, true));
        if (file_exists("/tmp/" . $this->headers['x-api-key'])) {
            sleep(10);
        }
        if (!empty($this->headers['x-api-key'])) {
            $myfile = fopen("/tmp/" . $this->headers['x-api-key'], "w");
            fclose($myfile);
            $validation = $this->Api_model->ValidateKey($this->headers['x-api-key'], $this->cid);
            // mail('mail@simson.one','auth', 'companyid: '.$this->session->cid);
            if (!$validation) {
                header("HTTP/1.1 401 Unauthorized");
                echo $this->set_response(array(
                    'result' => 'error',
                    'message' => 'Incorrect Token',
                    'cid' => $this->cid,
                    'token' => $this->headers['x-api-key']
                ));
                exit;
            }
            $this->companyid = $validation->companyid;
        } else {
            header("HTTP/1.1 401 Unauthorized");
            echo $this->set_response(array(
                'result' => 'error',
                'message' => 'Token is Empty'
            ));
            exit;
        }
        $this->data['setting'] = globofix($this->companyid);
        $this->ip = get_client_ip_env($_SERVER['REMOTE_ADDR'], $this->companyid);

        slack('Request: '.$this->cid.': '.json_encode($this->dpost), 'mvno');
    }

    public function set_response($data)
    {
        $this->dpost['request_uri'] = $this->uri->segment(4);
        $this->dpost['request_ver'] = $this->uri->segment(2);
        //$this->dpost['ip_address'] = $_SERVER['REMOTE_ADDR'];
        $this->teams->send_msg(current_url(), 'Result: '.json_encode($data).' Headers:'.json_encode($this->headers));
        //$this->dpost['companyid'] = $this->companyid;
        $this->teams->insertLog($this->dpost, $data, $_SERVER['REMOTE_ADDR']);
        unlink("/tmp/" . $this->headers['x-api-key']);
        //slack('Response: '.json_encode($data).' Headers: '.json_encode($this->headers), 'mvno');
        return json_encode($data);
    }
    public function index()
    {
        log_message('error', 'Yay here it is'.$this->uri->segment(5));
        if ($_SERVER['REQUEST_METHOD'] == "PUT") {
            $this->AddOrder();
        } elseif ($_SERVER['REQUEST_METHOD'] == "PATCH") {
            $this->EditOrder($this->uri->segment(5));
        } elseif ($_SERVER['REQUEST_METHOD'] == "GET") {
            $this->GetOrder($this->uri->segment(5));
        } else {
            header("HTTP/1.1 401 Bad Request");
            echo $this->set_response(array(
                'result' => 'error',
                'message' => 'Unknown method'
            ));
            exit;
        }
    }
    private function AddOrder()
    {
        set_time_limit(0);
        $auto_provision = false;
        $send_mailfee = false;

        $result = array(
            'result' => 'error',
        );
        $activate = false;
        log_message('error', print_r($this->dpost, 1));
        if (getPlatformbyPid($this->dpost['pid']) != "TEUM") {
            $this->load->library('artilium', array(
                'companyaid' => $this->companyid,
            ));
            $rr = array(
                'clientid',
                'brand',
                'pid',
                'addons',
                'contractduration',
                'porting',
                'startdate',
           );
            $req = $this->Api_model->checkRequired($rr, $this->dpost);
        } else {
            $req = $this->Api_model->checkRequired(array(
                'clientid',
                'brand',
                'pid',
                'porting',
                'activate'
            ), $this->dpost);


            $this->dpost['contractduration'] = 1;
            $this->dpost['startdate'] = date('m-d-Y');
            if ($this->dpost['activate']) {
                $req2 = $this->Api_model->checkRequired(array(
                    'msisdn'), $this->dpost);

                if ($req2['result'] != "success") {
                    echo $this->set_response(array("result" => "error", "message"=>"Msisdn is required"));
                    exit;
                }
            }
        }

        if ($this->dpost['addons']) {
            $addons  = explode(',', $this->dpost['addons']);

            foreach ($addons as $addin) {
                if (!is_allowedAddons($addin, $this->companyid)) {
                    echo $this->set_response(array('result' => 'error', 'message' => 'Addon id '.$addin.' does not exists'));
                    exit;
                }
            }
        }

        if ($req['result'] == "error") {
            echo $this->set_response($req);
            exit;
        }
        if (!isAllowed_products_companyid($this->dpost['pid'], $this->companyid)) {
            header("HTTP/1.1 404 PID Not Found");
            echo $this->set_response(array("result" => "error", "message"=> "pid does not exists"));
            exit;
        }
        $this->teams->send_msg(current_url(), "<br /><br /> REQUEST:<br />" . json_encode($this->dpost));
        $recurring = "0.00";
        $update_handset = false;
        $pass = random_str('alphanum', 8);
        $extra = false;
        $this->dpost['companyid'] = $this->companyid;
        $gid = $this->dpost['pid'];
        $this->dpost['msisdn_sn'] = null;
        /*
        if(!$gid){
        echo json_encode(array('result' => 'error', 'message' => 'Productid ' . $this->dpost['pid'] . ' does not have Group Assigned, Please contact +32484889888 for Help'));
        exit;
        }
         */

        if (!empty($this->dpost['setupfee'])) {
            if ($this->dpost['setupfee']) {
                $sp = $this->Api_model->checkRequired(array(
                    'setupfee_description',
                    'setupfee_amount',
                ), $this->dpost);
                if ($sp['result'] != 'success') {
                    echo $this->set_response($req1);
                    exit;
                } else {
                    $send_mailfee = true;
                }
            }
        }
        if (!validate_cd(trim($this->dpost['startdate']))) {
            echo $this->set_response(array(
                'result' => 'error',
                'message' => lang('startdate is invalid MM-DD-YYYY, and this must not be in the past'),
            ));
            exit;
        }
        if (!isAllowed_Clientid($this->companyid, $this->dpost['clientid'])) {
            echo $this->set_response(array(
                'result' => 'error',
                'message' => 'Clientid ' . $this->dpost['clientid'] . ' does not exists',
            ));
            exit;
        }
        if (!in_array($this->dpost['contractduration'], array(
            '1',
            '3',
            '6',
            '12',
            '24',
            '36',
        ))) {
            echo $this->set_response(array(
                'result' => 'error',
                'message' => 'contractduration supported is 1, 3, 6, 12, 24, 36 months ',
            ));
            exit;
        }
        if (!empty($this->dpost['promocode'])) {
            $promo = strtoupper(trim($this->dpost['promocode']));
        } else {
            $promo = "";
        }

        $userid = $this->dpost['clientid'];
        $client = $this->Admin_model->getClient($this->dpost['clientid']);
        if (getplatformbyPid($this->dpost['pid'])  != "TEUM") {
            if (!empty($this->dpost['delta_sim'])) {
                if (trim($this->dpost['delta_sim']) != "1111111111111111111") {
                    if (isSimcardFree(trim($this->dpost['delta_sim']))) {
                        echo $this->set_response(array(
                        'result' => 'error',
                        'message' => 'Your delta_sim has been used for previously order, please use free delta_sim',
                    ));
                        exit;
                    }
                    if (strlen(trim($this->dpost['delta_sim'])) != 19) {
                        echo $this->set_response(array(
                        'result' => 'error',
                        'message' => 'Your delta_sim is Invalid, Format: 89XXXXXXXXXXXXXXXXX, 19 Digit, you provide: ' . $this->dpost['delta_sim'],
                    ));
                        exit;
                    } else {
                        if (getPlatformbyPid($this->dpost['pid']) == "TEUM") {
                            $sim_ok = $this->Admin_model->getsimcard_teum(trim($this->dpost['delta_sim']), $this->companyid);
                        } else {
                            $sim_ok = $this->Admin_model->get_reseller_simcard(trim($this->dpost['delta_sim']), $this->companyid);
                        }
                        if (!$sim_ok) {
                            echo $this->set_response(array(
                            'result' => 'error',
                            'message' => 'The Simcard: ' . $this->dpost['delta_sim'] . ' is not registered yet in database, please contact United telecom MVNO IT Support',
                        ));
                            exit;
                        } else {
                            if (!empty($sim_ok->iPincode)) {
                                echo $this->set_response(array(
                                'result' => 'error',
                                'message' => 'The Simcard: ' . $this->dpost['delta_sim'] . ' has been assigned to MSISDN: ' . $sim_ok->iPincode . ' please use a free simcard',
                            ));
                                exit;
                            }
                        }
                    }

                    $this->dpost['msisdn_sim'] = $this->dpost['delta_sim'];

                    $sim = $this->artilium->getSn($this->dpost['delta_sim']);
                    $this->dpost['msisdn_sn'] = trim($sim->data->SN);
                    $this->dpost['msisdn'] = trim($sim->data->MSISDNNr);
                    $this->dpost['msisdn_puk1'] = $sim->data->PUK1;
                    $this->dpost['msisdn_puk2'] = $sim->data->PUK2;

                    if ($this->dpost['msisdn_sn']) {
                        $auto_provision = false;
                    }
                } else {
                    $mobiledata['msisdn_sim'] = $this->dpost['delta_sim'];
                }
            }
        } else {
            if ($this->dpost['activate']) {
                // beb55bf901802ead35236598f691bd6c
                $auto_provision = true;
            }
            log_message('error', 'Teum API Orders');
            if (!empty($this->dpost['msisdn'])) {
                $sim = $this->Admin_model->getsimcard($this->dpost['msisdn'], $this->companyid);


                if (!$sim) {
                    echo $this->set_response(array(
                        'result' => 'error',
                        'message' => 'You have msisdn attached in the order but this msisdn '.$this->dpost['msisdn'].' could not be found in any reseller make sure you have use /InsertMsisdn',
                    ));
                    exit;
                } else {
                    if ($sim->serviceid >0) {
                        echo $this->set_response(array(
                            'result' => 'error',
                            'message' => 'This msisdn '.$this->dpost['msisdn'].' is currently assigned to serviceid '.$sim->serviceid.' please use a free simcard',
                        ));
                        exit;
                    }
                }
                $mobiledata['msisdn'] = $this->dpost['msisdn'];
                $mobiledata['msisdn_sn'] = $sim->MSISDN;
                $mobiledata['msisdn_sim'] = $sim->simcard;
                $mobiledata['msisdn_pin'] =  $sim->PIN1;
                $mobiledata['msisdn_puk1'] =  $sim->PUK1;
                $mobiledata['msisdn_puk2'] =  $sim->PUK2;
                $mobiledata['msisdn_imsi'] =  $sim->IMSI;
                $mobiledata['platform'] =  getPlatformbyPid($this->dpost['pid']);
            }
        }
        if ($this->dpost['porting']) {
            if (!$this->Admin_model->checkMsisdn(trim($this->dpost['porting_msisdn']))) {
                echo $this->set_response(array(
                    'result' => 'error',
                    'message' => 'The number ' . $this->dpost['porting_msisdn'] . ' already exists',
                ));
                exit;
            }
            if (strlen(trim($this->dpost['porting_msisdn'])) != "11") {
                echo $this->set_response(array(
                    'result' => 'error',
                    'message' => 'You are requesting for PortIn, but your porting_msisdn is invalid FORMAT: (countrycode)(number without leading 0) ex: 31612345788',
                ));
                exit;
            }

            if (DateTime::createFromFormat('Y-m-d', $this->dpost['porting_date']) !== false) {
            } else {
                echo $this->set_response(array(
                    'result' => 'error',
                    'message' => 'Your porting_date is invalid YYYY-MM-DD',
                ));
                exit;
            }
        }
        if (!empty($this->dpost['handset_name'])) {
            $req0 = $this->Api_model->checkRequired(array(
                'handset_price',
                'handset_terms',
                'handset_price',
            ), $this->dpost);

            if ($req0['result'] != "success") {
                echo $this->set_response($req0);
                exit;
            }
        }
        //If every parameter has been check, lets start inserting order
        $serviceid = $this->Api_model->insertOrder(array(
            'companyid' => $this->companyid,
            'type' => 'mobile',
            'status' => 'New',
            'packageid' => $this->dpost['pid'],
            'userid' => $userid,
            'date_created' => date('Y-m-d'),
            'billingcycle' => 'Monthly',
            'promocode' => $promo,
            'date_contract' => $this->dpost['startdate'],
            'contract_terms' => $this->dpost['contractduration'],
            'recurring' => getPriceRecurring($this->dpost['pid'], $this->dpost['contractduration']),
            'notes' => 'Order via API by IP :' . $this->ip . ' Token: ' . $this->headers['x-api-key'],
        ));

        if (!$serviceid) {
            echo $this->set_response(array(
                'result' => 'error',
                'message' => 'Order failed please contact Support +32 484889888',
            ));
            exit;
        } else {
            if ($this->dpost['addons']) {
                $addons  = explode(',', $this->dpost['addons']);
                if ($addons) {
                    log_message("error", "adding addon ".print_r($addons, 1));
                    foreach ($addons as $addonidnya) {
                        $this->Api_model->addAddon($serviceid, $addonidnya);
                    }
                }
            }


            $this->db->query("update a_reseller_simcard set serviceid=? where MSISDN=?", array($serviceid, $this->dpost['msisdn']));

            logAdmin(array(
                        'companyid' =>$this->companyid,
                        'userid' => $userid,
                        'user' => 'System',
                        'serviceid' => $serviceid,
                        'ip' => '127.0.0.1',
                        'description' => 'Order Received ID ' . $serviceid));


            if ($this->data['setting']->mobile_provider == "TMNL") {
                $req1 = $this->Api_model->checkRequired(array(
                    'porting_msisdn',
                    'porting_provider',
                    'porting_type',
                    'porting_customer_type',
                    'porting_date',
                ), $this->dpost);
            } else {
                $req1 = $this->Api_model->checkRequired(array(
                    'porting_msisdn',
                    'porting_provider',
                    'porting_simnumber',
                    'porting_type',
                    'porting_customer_type',
                ), $this->dpost);
            }

            $result['result'] = "success";
            $result['serviceid'] = $serviceid;
            $result['orderid'] = $serviceid;
            if ($this->dpost['porting']) {
                if ($req1['result'] != "success") {
                    echo $this->set_response($req1);
                    exit;
                }


                if (!$mobiledata) {
                    $mobiledata = array(
                    'msisdn_type' => 'porting',
                    'donor_msisdn' => preg_replace('/\D/', '', $this->dpost['porting_msisdn']),
                    'donor_provider' => $this->dpost['porting_provider'],
                    'donor_sim' => $this->dpost['porting_simnumber'],
                    'donor_type' => $this->dpost['porting_type'],
                    'donor_customertype' => $this->dpost['porting_customer_type'],
                    'donor_accountnumber' =>  $this->dpost['porting_customer_type'],
                    'msisdn_status' => 'OrderWaiting',
                    'date_wish' => $this->dpost['porting_date'],
                    'serviceid' => $serviceid,
                    'companyid' => $this->companyid,
                    'userid' => $userid,
                    'msisdn' => preg_replace('/\D/', '', $this->dpost['porting_msisdn']),
                    'msisdn_pin' => $this->data['setting']->default_pin,
                    'msisdn_sim' => $this->dpost['delta_sim'],
                    'ptype' => 'POST PAID',
                );
                } else {
                    $mobiledata['serviceid'] = $serviceid;
                    $mobiledata['companyid'] = $this->companyid;
                    $mobiledata['msisdn_status'] = 'OrderWaiting';
                    $mobiledata['msisdn_type'] = 'new';
                    $mobiledata['userid'] =  $userid;
                    $mobiledata['date_wish'] =  date('Y-m-d');
                }
                if ($this->data['setting']->mobile_provider != "TMNL") {
                    if ($this->dpost['porting_customer_type'] == "1") {
                        $req2 = $this->Api_model->checkRequired(array(
                            'porting_accountnumber',
                        ), $this->dpost);
                        if ($req2['result'] != "success") {
                            echo $this->set_response($req2);
                            exit;
                        }
                        $mobiledata['donor_accountnumber'] = $this->dpost['porting_accountnumber'];
                    }
                } else {
                    if (!empty($this->dpost['porting_accountnumber'])) {
                        $mobiledata['donor_accountnumber'] = $this->dpost['porting_accountnumber'];
                    }
                }
            } else {
                if (!$mobiledata) {
                    $mobiledata = array(
                        'msisdn_type' => 'new',
                        'msisdn_status' => 'OrderWaiting',
                        'msisdn_sn' =>  $this->dpost['msisdn_sn'],
                        'serviceid' => $serviceid,
                        'companyid' => $this->companyid,
                        'userid' => $userid,
                        'msisdn_pin' => $this->data['setting']->default_pin,
                        'msisdn_sim' => $this->dpost['delta_sim'],
                        'ptype' => 'POST PAID',
                    );
                } else {
                    $mobiledata['serviceid'] = $serviceid;
                    $mobiledata['companyid'] = $this->companyid;
                    $mobiledata['msisdn_status'] = 'OrderWaiting';
                    $mobiledata['msisdn_type'] = 'new';
                    $mobiledata['userid'] =  $userid;
                    //$mobiledata['msisdn_pin'] =  $this->data['setting']->default_pin;
                }
            }
            $mobiledata['ptype'] = getpType($this->dpost['pid']);
            if (!empty($this->dpost['porting_accountnumber'])) {
                $mobiledata['donor_accountnumber'] = $this->dpost['porting_accountnumber'];
                $mobiledata['donor_customertype'] = 1;
            }
            unset($mobiledata['delta_sim']);

            $mobiledata['msisdn_contactid'] = $this->Admin_model->getContactIdArta($gid);
            if (!empty($this->dpost['handset_name'])) {
                $mobiledata_addons['serviceid'] = $serviceid;
                $mobiledata_addons['cycle'] = 'month';
                $mobiledata_addons['name'] = $this->dpost['handset_name'];
                $mobiledata_addons['terms'] = $this->dpost['handset_terms'];

                $this->dpost['handset_price'] = $this->dpost['handset_price'] * 1.21;
                $tot = round($this->dpost['handset_price'], 4) / $this->dpost['handset_terms'];
                //sending price exclusif vat;
                $mobiledata_addons['recurring_total'] = $tot;
                $mobiledata_addons['addon_type'] = 'others';
                $mobiledata_addons['addonid'] = '18';
                $this->db->insert('a_services_addons', $mobiledata_addons);
            }

            // mail('mail@simson.one','pppp',print_r($mobiledata_addons, true));
            $mobiledata['msisdn_languageid'] = setVoiceMailLanguageByClientLang($client->language);
            $service_mobile = $this->Api_model->insertMobileData($mobiledata);
            if ($service_mobile > 0) {
                $email = getNotificationOrderEmail($this->companyid);

                if ($email) {
                    $product = getProduct($this->dpost['pid']);
                    $headers = "From: noreply@united-telecom.be";
                    mail($email, "New order notification", "Dear Team,\n\nNew Order has just been arrived, Please process this order as soon as possible:\n\nServiceid: " . $serviceid . "\nProduct: " . $product->name . "\n\nRegards", $headers);
                }
                if (in_array($this->companyid, array('53', '56'))) {
                    if ($this->dpost['porting']) {
                        if ($auto_provision) {
                            if ($this->dpost['delta_sim'] != '1111111111111111111') {
                                $this->Admin_model->ChangeStatusService($serviceid, 'Pending');
                                $this->Admin_model->ChangeStatusOrderID($serviceid, 'OrderWaiting', 'mobile');
                                logAdmin(array(
                                'companyid' => $this->companyid,
                                'serviceid' => $serviceid,
                                'userid' => $userid,
                                'user' => 'System',
                                'ip' => $_SERVER['REMOTE_ADDR'],
                                'description' => 'Service : ' . $serviceid . ' has been accepted as sim added in the API',
                                ));


                                $activationr = array(
                                "serviceid" => $serviceid,
                                "companyid" => $this->companyid,
                                "msisdn_sim" => $this->dpost['delta_sim'],
                                "msisdn_type" => "porting",
                                "date_contract" => $this->dpost['startdate'],
                                "donor_msisdn" => preg_replace('/\D/', '', $this->dpost['porting_msisdn']),
                                "donor_provider" => $this->dpost['porting_provider'],
                                "donor_type" => $this->dpost['porting_type'],
                                "donor_sim" => $this->dpost['porting_simnumber'],
                                "donor_accountnumber" => $mobiledata['donor_accountnumber'],
                                "date_wish" => $this->dpost['porting_date'],
                                );
                                $resmin = $this->activate_mobile_arta($activationr);
                                //mail("mail@simson.one", "Auto Activation " . $serviceid, print_r($activationr, true).print_r($resmin, true));
                            }
                        }
                    }
                }
                if ($auto_provision) {
                    if (getPlatformbyPid($this->dpost['pid']) == "TEUM") {
                        log_message('error', 'Activate TEUM');
                        $this->activate_mobile_teum($serviceid);
                    } else {
                        log_message('error', 'NON TEUM');
                    }
                }
                $result['auto_provision'] = $auto_provision;
                $result['result'] = "success";
                log_message('error', 'New order has been received with Serviceid: #'.$serviceid);
                send_growl(array(
                'message' => 'New order has been received with Serviceid: #'.$serviceid,
                'companyid' => $this->companyid));
                if ($send_mailfee) {
                    $headers = "From: noreply@united-telecom.be";
                    mail("simson.parlindungan@united-telecom.be", "Please apply setup fee to " . $serviceid, print_r($this->dpost, true), $headers);
                }
            } else {
                $result['result'] = "error";
                $result['message'] = "Service mobile was not created";
            }
        }
        echo $this->set_response($result);
    }
}
