<?php
defined('BASEPATH') or exit('No direct script access allowed');

class AuthController extends CI_Controller
{
    protected $data = [];

    public function __construct()
    {
        // Ensure you run parent constructor
        parent::__construct();
        $this->config->set_item('language', 'dutch');
        $this->lang->load('client');
        $this->companyid = get_companyidby_url(base_url());

        $this->data['setting'] = globofix($this->companyid);
    }
}
class Auth extends AuthController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Auth_model');
    }
    public function index()
    {
        // print_r($this->data);

        if ($this->session->cid == 54) {
            if (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4))) {
                redirect('client/auth');
            }
        }


        $this->load->model('Client_model');
        if (!empty($_SESSION['client']['islogged'])) {
            redirect('mobile');
            exit;
        }
        if (isPost()) {
            //print_r($_SESSION);
            $valid = $this->Auth_model->client_auth($_POST);

            if ($valid['result'] == "success") {
                $valid['client']['islogged'] = true;
                unset($valid['client']['password']);
                $this->session->set_userdata('client', $valid['client']);
                if ($this->Client_model->is_loged($valid['client']['id'])) {
                    redirect('mobile');
                } else {
                    $this->session->set_flashdata('error', lang('This is your first login, You need to change your password'));
                    // redirect('client/dashboard/changepassword/first');
                }
            } else {
                $this->session->set_flashdata('error', $valid['message']);
            }
        }
        //print_r($_SESSION);
        $this->load->view('themes/' . $this->data['setting']->default_theme . '/mobile/login', $this->data);
    }

    public function reset()
    {
        $this->db = $this->load->database('default', true);
        if (isPost()) {
            $client = $this->db->query('select * from a_clients where email like ? ', array($_POST['email']));
            if ($client->num_rows() > 0) {
                $this->config->set_item('language', $client->row()->language);
                $code = $this->Auth_model->addClientCode(random_str('alphanum', '6'), $client->row()->id);
                if ($this->data['setting']->smtp_type == 'smtp') {
                    $config = array(
                        'protocol' => 'smtp',
                        'smtp_host' => $this->data['setting']->smtp_host,
                        'smtp_port' => $this->data['setting']->smtp_port,
                        'smtp_user' => $this->data['setting']->smtp_user,
                        'smtp_pass' => $this->data['setting']->smtp_pass,
                        'mailtype' => 'html',
                        'charset' => 'utf-8',
                        'starttls' => true,
                        'wordwrap' => true,
                    );
                } else {
                    $config['protocol'] = 'sendmail';
                    $config['mailpath'] = '/usr/sbin/sendmail';
                    $config['mailtype'] = 'html';
                    $config['charset'] = 'utf-8';
                    $config['wordwrap'] = true;
                }
                $this->email->initialize($config);

                $this->data['language'] = "dutch";
                //  $this->data['main_content'] = 'email/' . $this->data['language'] . '/clientresetpassword.php';
                // $body = $this->load->view('email/content', $this->data, true);

                $body = getMailContent('clientresetpassword', $client->row()->language, $this->companyid);
                // $body = $this->load->view('email/content', $this->data, true);
                $body = str_replace('{$name}', $client->row()->salutation.' '.$client->row()->firstname.' '.$client->row()->lastname, $body);
                $body = str_replace('{$code}', $code, $body);
                $body = str_replace('{$base_url}', url_to_domain(base_url()), $body);

                $this->email->set_newline("\r\n");
                $this->email->from($this->data['setting']->smtp_sender, $this->data['setting']->smtp_name);
                $this->email->to($_POST['email']);
                $this->email->subject(getSubject('clientresetpassword', $client->row()->language, $client->row()->companyid));
                $this->email->message($body);
                if ($this->email->send()) {
                    echo json_encode(array('result' => true));
                } else {
                    echo json_encode(array('result' => false, 'error' => $this->email->print_debugger()));
                }
                $this->session->set_flashdata('success', lang('A mail with token has been sent to your email to continue'));
                redirect('client/auth/entercode');
            } else {
                $this->session->set_flashdata('error', lang('We can not find this email in our system'));
            }
        }
        $this->session->sess_destroy();
        $this->load->view('themes/' . $this->data['setting']->default_theme . '/clients/auth_reset', $this->data);
    }

    public function resetwithcode()
    {
        $this->db = $this->load->database('default', true);
        if (isPost()) {
            $client = $this->db->query('select * from a_clients where pwresetkey like ? ', array($_POST['code']));
            $code = $_POST['code'];
            if (trim($_POST['password1']) == trim($_POST['password2'])) {
                $this->config->set_item('language', $client->row()->language);
                $this->Auth_model->ClientChangePassword($_POST);
                $valid['client'] = $client->row_array();
                $valid['client']['islogged'] = true;
                $this->session->sess_destroy('client');
                $this->session->set_userdata($valid);
                $this->session->set_userdata('code');
                if ($this->data['setting']->smtp_type == 'smtp') {
                    $config = array(
                        'protocol' => 'smtp',
                        'smtp_host' => $this->data['setting']->smtp_host,
                        'smtp_port' => $this->data['setting']->smtp_port,
                        'smtp_user' => $this->data['setting']->smtp_user,
                        'smtp_pass' => $this->data['setting']->smtp_pass,
                        'mailtype' => 'html',
                        'charset' => 'utf-8',
                        'starttls' => true,
                        'wordwrap' => true,
                    );
                } else {
                    $config['protocol'] = 'sendmail';
                    $config['mailpath'] = '/usr/sbin/sendmail';
                    $config['mailtype'] = 'html';
                    $config['charset'] = 'utf-8';
                    $config['wordwrap'] = true;
                }
                $this->email->initialize($config);

                $body = getMailContent('clientpasswordchanged', $client->row()->language, $this->companyid);
                // $body = $this->load->view('email/content', $this->data, true);
                $body = str_replace('{$name}', $client->row()->salutation.' '.$client->row()->firstname.' '.$client->row()->lastname, $body);
              
                $body = str_replace('{$base_url}', url_to_domain(base_url()), $body);


                //$this->data['main_content'] = 'email/' . $this->data['language'] . '/clientpasswordchanged.php';
                //$body = $this->load->view('email/content', $this->data, true);
                unset($valid['client']['code']);
                $this->email->set_newline("\r\n");
                $this->email->from($this->data['setting']->smtp_sender, $this->data['setting']->smtp_name);
                $this->email->to($valid['client']['email']);
                $this->email->subject(getSubject('clientpasswordchanged', $client->row()->language, $client->row()->companyid));
                $this->email->message($body);
                if ($this->email->send()) {
                    $this->session->set_flashdata('success', lang('Your password has been changed successfully'));
                    redirect('mobile');
                    exit;
                } else {
                    $this->session->set_flashdata('success', lang('there was error sending your information'));
                    redirect('mobile');
                    exit;
                }
            } else {
                $this->session->set_flashdata('error', lang('your password do not match'));
                redirect('mobile');
                exit;
            }
        } else {
            $code = $this->Auth_model->getClientCode(trim($this->uri->segment(4)));
            //echo $this->uri->segment(4);
            //print_r($code);
            if ($code['result']) {
                $_SESSION['code'] = trim($this->uri->segment(4));
                $this->load->view('themes/' . $this->data['setting']->default_theme . '/clients/auth_resetwithcode', $this->data);
            } else {
                $this->session->set_flashdata('error', lang('Please enter valid token'));
                $this->load->view('themes/' . $this->data['setting']->default_theme . '/clients/auth_code', $this->data);
            }
        }
    }

    public function sso_login()
    {
        $key = $_GET['key'];
        $data = encrypt_decrypt('decrypt', $key);
        $this->db = $this->load->database('default', true);
        $this->db->where('id', $data);
        $this->db->where('authdata', trim($key));
        $q = $this->db->get('tblclients');
        if ($q->num_rows() > 0) {
            $valid['client'] = $q->row_array();
            $valid['client']['islogged'] = true;
            $valid['client']['sso'] = true;
            $this->Auth_model->RemoveAuthdata($data);
            unset($client['password']);

            $this->session->set_userdata('client', $valid['client']);
            if (has_sso($valid['client']['id'])) {
                $this->session->set_flashdata('success', lang('Welcome by Delta Mobile'));
            } else {
                $this->session->set_flashdata('success', lang('Your Delta account has been linked successfully'));
                $this->Auth_model->ActivateDeltaUser($valid['client']['id']);
            }
        } else {
            $this->session->set_flashdata('error', lang('Login Failed using delta credential'));
        }

        redirect('mobile');
    }

    public function sso_logout()
    {
        $this->session->sess_destroy();
        redirect(base_url() . 'sso.php?logout=true');
    }
    public function link2artilium()
    {
        $this->load->model('Client_model');
        if (isPost()) {
            //print_r($_SESSION);
            $valid = $this->Auth_model->client_auth($_POST);

            if ($valid['result'] == "success") {
                $valid['client']['islogged'] = true;
                unset($valid['client']['password']);
                $this->session->set_userdata('client', $valid['client']);

                if (validate_deltausername(strtolower($_POST['deltausername']))) {
                    $e["valid"] = 1;
                    $e["username"] = $_POST['deltausername'];
                    $e['userid'] = $valid['client']['id'];
                    $this->session->set_flashdata('success', lang('Your Delta account has been linked successfully with ' . $_POST['deltausername']));
                    $this->Client_model->insertDeltaUsername($e);
                    redirect('mobile');
                    exit;
                } else {
                    $this->session->set_flashdata('error', lang('Sorry there is already customer assigned with this delta username : ' . $_POST['deltausername']));
                    redirect('mobile');
                }
            } else {
                $this->session->set_flashdata('error', $valid['message']);
                redirect('client/auth/sso_register?key=' . $_POST['key']);
            }
        }
    }
    public function entercode()
    {
        if (isPost()) {
            $code = $this->Auth_model->getClientCode($_POST['code']);
            //    $code = $this->Auth_model->getCode($_POST['code']);
            $_SESSION['code'] = $_POST['code'];
            if ($code['result']) {
                $this->session->set_flashdata('success', lang('Please Change your Password'));
                $this->load->view('themes/' . $this->data['setting']->default_theme . '/clients/auth_resetwithcode', $this->data);
            } else {
                $this->session->set_flashdata('error', 'your token is invalid');
            }
        } else {
            $this->load->view('themes/' . $this->data['setting']->default_theme . '/clients/auth_code', $this->data);
        }
    }

    public function sso()
    {
        require_once FCPATH . 'assets/sso/lib/_autoload.php';
        $auth = new SimpleSAML_Auth_Simple('default-sp');
        $auth->requireAuth(array('saml:idp' => 'urn:idp:zeelandnet2016'));
        $attributes = $auth->getAttributes();
        if (isPost()) {
            mail('lainard@gmail.com', 'test', print_r($_POST, true));
        }
    }
    public function sso_register()
    {
        $data = base64_decode($_GET["key"]);
        echo $data;
        $this->data['account'] = json_decode($data, true);
        $this->config->set_item('language', 'dutch');
        $this->data['title'] = $this->data['setting']->companyname;
        $this->data['main_content'] = 'themes/' . $this->data['setting']->default_theme . '/clients/non_sso';
        $this->load->view('themes/' . $this->data['setting']->default_theme . '/clients/' . 'content', $this->data);
    }
    public function logout()
    {
        $redirect = false;
        if ($_SESSION['client']['sso']) {
            $redirect = true;
        }
        //print_r($_SESSION);
        $this->session->sess_destroy();
        $this->session->set_flashdata('success', lang('You have been logged out!'));

        if ($redirect) {
            header('Location: ' . base_url() . 'sso.php');
        } else {
            redirect('client/auth');
        }
    }

    public function download_auth()
    {
        $this->load->model('Client_model');
        $this->load->model('Admin_model');
        $id = $this->uri->segment(4);
        $uuid = $_GET['auth'];

        $clientid = $this->Client_model->getClientid($uuid);
        $client = $this->Client_model->whmcs_api(array('action' => 'GetClientsDetails', 'clientid' => $clientid));

        foreach ($client->customfields as $row) {
            if ($row->id == '900') {
                $mageboid = $row->value;
            }
        }
        if ($this->Client_model->IsAllowedInvoice($id, $mageboid)) {
            $invoice = $this->Admin_model->mageboGet('GetInvoice/' . $id);
            $file = '/tmp/' . $id . '.pdf';
            file_put_contents($file, base64_decode($invoice->Invoice));
            if (file_exists($file)) {
                ob_clean();
                flush();
                //echo $this->data['setting']->DOC_PATH . 'invoices/' . $invoice['id'] . '.pdf';
                header('Content-Type: application/pdf');
                header('Content-Length: ' . filesize($file));
                header('Content-Disposition: inline; filename="' . $id . '.pdf"');
                readfile($file);
                unlink($file);
            } else {
                echo "file does not exists";
            }
        } else {
            echo "Test Mode!! please ignore!!";
        }
    }

    public function downloadcdr_auth()
    {
        $this->load->model('Client_model');
        $this->load->model('Admin_model');
        $id = $this->uri->segment(4);
        $uuid = $_GET['auth'];

        $clientid = $this->Client_model->getClientid($uuid);
        $client = $this->Client_model->whmcs_api(array('action' => 'GetClientsDetails', 'clientid' => $clientid));

        foreach ($client->customfields as $row) {
            if ($row->id == '900') {
                $mageboid = $row->value;
            }
        }

        $this->load->library('xlswriter');

        $cdrs = $this->Admin_model->mageboGet('GetCdrs/' . $id);

        $header = array(
            'Invoice' => 'string',
            'From' => 'string',
            'Date' => 'datetime',
            'Destination Number' => 'string',
            'Destination Operator' => 'string',
            'Price' => '#,##0.00 [$€-407];[RED]-#,##0.00 [$€-407]',
            'Cost' => '#,##0.00 [$€-407];[RED]-#,##0.00 [$€-407]',
            'Duration (sec)' => 'integer',
        );

        if (count($cdrs) > 0) {
            foreach ($cdrs as $array) {
                $to[] = (array) $array;
            }
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            $this->xlswriter->writeSheet($to, 'Sheet1', $header);
            $this->xlswriter->setAuthor('Simson Parlindungan');
            $this->xlswriter->setTitle('Cdr Export');
            $this->xlswriter->setCompany('Delta');
            echo $this->xlswriter->writeToString();
        } else {
            echo "access denied";
        }
    }

    public function accept_porting()
    {
        $this->data['title'] = lang("Accept Porting");
        $this->data['main_content'] = 'themes/' . $this->data['setting']->default_theme . '/clients/accept_porting';
        $this->load->view('themes/' . $this->data['setting']->default_theme . '/clients/' . 'content_porting', $this->data);
    }
}
