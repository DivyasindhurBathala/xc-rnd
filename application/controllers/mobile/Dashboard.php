<?php
defined('BASEPATH') or exit('No direct script access allowed');
class ClientDashboardController extends CI_Controller
{
    protected $data = [];

    public function __construct()
    {
        // Ensure you run parent constructor
        parent::__construct();
        $this->config->set_item('language', $this->session->client['language']);
        $this->lang->load('client');
        $this->companyid = get_companyidby_url(base_url());

        $this->data['setting'] = globofix($this->companyid);
    }
}
class Dashboard extends ClientDashboardController
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('Admin_model');
        $this->load->model('Client_model');
        $this->load->model('Teams_model');
    }

    public function index()
    {
        if ($this->session->cid == 54) {
            if (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4))) {
                redirect('client');
            }
        }

        $this->load->library('artilium', array('companyid' => $this->companyid));
        $total = 0;
        $this->load->library('xml');
        $usage = array('0.00');
        $this->data['activemobiles'] = getMobileActive($this->companyid);
        //print_r($this->data);
        if (empty($this->data['activemobiles'])) {
            $this->data['showusage'] = false;
        } else {
            $f = array();
            $cdr = array();

            foreach ($this->data['activemobiles'] as $IN => $order) {
                if ($order['domainstatus'] == 'Active') {
                    $this->data["service" . $IN] = $this->Admin_model->getService($order['id']);

                    $mobile = $this->data["service" . $IN]->details;

                    $f[] = array('domain' => $mobile->msisdn, 'sn' => $mobile->msisdn_sn, 'mobile' => $mobile);
                    $cdr = $this->artilium->get_cdr($this->data["service" . $IN]);

                    $total = $cdr['total'] + $total;
                    
                    unset($cdr);
                }
            }
            $this->data['consumption'] = $total;

            $this->data['showusage'] = true;
            $this->data['bundles'] = $this->artilium->GetBundleAssignList1($f['0']['sn']);
            if ($this->session->master) {
                //print_r($_SESSION);
                //print_r($this->data['activemobiles']);
                //exit;
                // print_r($this->data['bundles']);
                // exit;
            }
            // $this->data['iusage'] = $this->Admin_model->artiliumPost(array('SN' => trim($f[0]['sn']), 'companyid' => $this->data['setting']->companyid, 'action' => 'GetBundleAssignUsage', 'PageIndex' => 0, 'PageSize' => 1000, 'SortBy' => 0, 'SortOrder' => 0, 'type' => 'POST PAID'));
        }
        // print_r($this->data);
        $this->data['stats'] = $this->Client_model->getStats($_SESSION['client']['id']);
        $this->data['title'] = lang("Dashboard");
        ;
        $this->data['dtt'] = true;
        $this->data['main_content'] = 'themes/' . $this->data['setting']->default_theme . '/mobile/dashboard';
        $this->load->view('themes/' . $this->data['setting']->default_theme . '/mobile/' . 'content', $this->data);
    }

    public function getusage($id)
    {
        $this->load->model('Admin_model');
        $this->data['service'] = $this->Admin_model->getService($id);
        //echo $this->data['service']->domain;
        $this->data['mobile'] = $this->Admin_model->mageboGet('GetSim/' . trim($this->data['service']->domain));

        //$this->load->library('artilium', array($this->data['setting']->companyid, $this->data['mobile']->PaymentType));
        $this->data['sn'] = $this->Admin_model->artiliumGet('GetSn/' . trim($this->data['service']->domain), $this->data['mobile']->PaymentType, $this->data['setting']->companyid);
        $cdr = $this->get_cdr($this->data['sn']->SN, $this->data['service']->domain, $this->data['mobile']);

        return array('voice' => second2hms($cdr['voice']), 'data' => round($cdr['data'] / 1048576, 2), 'sms' => $cdr['sms']);
    }

    public function getusagev2()
    {
        $id = $_POST['id'];

        $service = $this->Admin_model->getService($id);
        $this->load->library('artilium', array('companyid' => $this->companyid));
        $bundles = $this->artilium->GetBundleAssignList1($service->details->msisdn_sn);

        if ($bundles) {
            $html = '';
            foreach ($bundles as $index => $row) {
                $html .= '<div class=" col-md-4">
            <div class="card bg-default">
               <div class="card-header text-center text-light bg-primary">' . $row->szBundle . ' </div>
              <div class="card-body text-dark text-center"><h5>
              ' . str_replace('.', ',', $row->UsedValue) . ' / ' . str_replace('.', ',', $row->AssignedValue) . '</h5>';
                if (strpos($row->szBundle, '30 dagen') || strpos($row->szBundle, '30 days')) {
                    $html .= lang('ValidUntil') . ' ' . $row->ValidUntil . '</h6>';
                }

                $html .= '</div>
            </div>
          </div>';
                if ($index == 3) {
                    $html .= ' <hr />';
                }
            }
        }

        echo json_encode(array('html' => $html));
    }
    public function link2delta()
    {
        //print_r($_POST);
        if (validate_deltausername(strtolower($_POST['username']))) {
            $this->Client_model->insertDeltaUsername($_POST);
            header('Location: ' . base_url() . 'sso.php?login=true');
            exit;
        } else {
            $this->session->set_flashdata('error', lang('Sorry there is already customer assigned with this delta username : ' . $_POST['deltausername']));
            redirect('mobile');
        }
    }

    public function stopasking()
    {
        $res = $this->Client_model->doNotAskSSO($_POST['userid']);
        $_SESSION['client']['sso'] = 1;
        echo json_encode(array('result' => $res));
    }
    public function pull_usage()
    {
        echo json_encode($this->getusage($_POST['id']));
    }
    public function pull_usagev2()
    {
        echo json_encode($this->getusagev2($_POST['id']));
    }
    public function get_lang()
    {
        echo json_encode(array('result' => ucfirst($_SESSION['client']['language'])));
    }

    public function chart()
    {
        $this->load->view('themes/' . $this->data['setting']->default_theme . '/clients/' . 'chart', $this->data);
    }

    public function switchlang()
    {
        $lang = $this->uri->segment(4);
        $_SESSION['client']['language'] = $lang;
        $ref = $this->agent->referrer();
        $this->config->set_item('language', $lang);
        $this->lang->load('client');
        $this->session->set_flashdata('success', lang('Your Language has been changed to') . ' ' . lang(ucfirst($lang)));
        header('Location: ' . $ref);
    }
    public function account()
    {
        if (isPost()) {
            $post = $_POST;
            $this->db = $this->load->database('default', true);
            $this->db->where('id', $_SESSION['client']['id']);
            $this->db->update('a_clients', $post);
            if ($this->db->affected_rows() > 0) {
                $_SESSION['client'] = $post;
                $_SESSION['client']['islogged'] = true;
                $this->session->set_flashdata('success', lang('Your information has been saved'));
            }
        }

        $this->data['title'] = $this->data['setting']->companyname;
        $this->data['client'] = $this->Admin_model->getClient($_SESSION['client']['id']);
        $this->data['title'] = "Mvno Dasboard";
        $this->data['main_content'] = 'themes/clear/mobile/myaccount';
        $this->load->view('themes/' . $this->data['setting']->default_theme . '/mobile/content', $this->data);
    }

   
    public function get_cdrv2($sn, $msisdn, $mobile)
    {
        $total = 0;
        $bytes = array();
        $this->load->model('Admin_model');

        $cdr = array();

        //print_r($this->data['mobile']);
        //$this->load->library('artilium', array($this->data['setting']->companyid, $this->data['mobile']->PaymentType));
        //echo $msisdn . "<br />";
        //print_r($mobile);
        //$result = $this->artilium->GetCDRList($sn);
        $params = array('companyid' => $this->companyid,
            'action' => 'GetCDRList',
            'type' => 'POST PAID',
            'SN' => trim($sn),
            'From' => date('Y-m-01\T00:00:00+02:00'),
            'Till' => date('Y-m-d\TH:i:s'),
            'PageIndex' => 0,
            'PageSize' => 2500,
            'SortBy' => 0,
            'SortOrder' => 0);

        $sms = array();
        $voice = array();
        $bytes = array();

        $result = $this->Admin_model->artiliumPost($params);

        foreach ($result->ListInfo as $row) {
            if ($row->Cause != "1000") {
                if ($row->TypeCallId != 5) {
                    if ($row->MaskDestination != "72436") {
                        if (!empty($row->DestinationCountry)) {
                            $country = $row->DestinationCountry;
                        } else {
                            $country = "";
                        }

                        if ($row->TrafficTypeId == 2) {
                            $total = $total + includevat('21', $row->CurNumUnitsUsed);
                        //$cdr[] = array('Begintime' => format_cdr_time($row->Begintime), 'TrafficTypeId' => getTraficName($row->TrafficTypeId), 'MaskDestination' => $row->MaskDestination, 'DurationConnection' => str_replace('.', ',', round($row->DurationConnection / 1048576, 2)) . 'MB', 'CurNumUnitsUsed' => '€ ' . str_replace('.', ',', includevat('21', $row->CurNumUnitsUsed)));
                        } else {
                            if ($row->TrafficTypeId == "1" && $row->TypeCallId == "2") {
                            } else {
                                // if ($row->TypeCallId != "3") {
                                $total = $total + includevat('21', $row->CurNumUnitsUsed);
                                //$cdr[] = array('Begintime' => format_cdr_time($row->Begintime), 'TrafficTypeId' => getTraficName($row->TrafficTypeId), 'MaskDestination' => $row->MaskDestination, 'DurationConnection' => gmdate("H:i:s", $row->DurationConnection), 'CurNumUnitsUsed' => '€ ' . str_replace('.', ',', includevat('21', $row->CurNumUnitsUsed)));
                                //}
                            }
                        }
                    }
                }
            }

            if ($row->TypeCallId != 5) {
                if (!in_array($row->TrafficTypeId, array(2, 3))) {
                    if (strlen($row->MaskDestination) > 9) {
                        if ($row->TrafficTypeId) {
                            if ($row->TrafficTypeId == "1" && $row->DurationConnection < 1) {
                            } elseif ($row->TrafficTypeId == "5" && $row->DurationConnection < 1) {
                            } else {
                                if ($row->TrafficTypeId == "1" && $row->DurationConnection > 1) {
                                    $voice[] = $row->DurationConnection;
                                } elseif ($row->TrafficTypeId == "5" && $row->DurationConnection >= 1) {
                                    $sms[] = $row->DurationConnection;
                                }
                            }
                        }
                    }
                } elseif ($row->TrafficTypeId == 2) {
                    $bytes[] = $row->DurationConnection;
                }
            }
        }

        if (!$sms) {
            $smsi = '0';
        } else {
            $smsi = array_sum($sms);
        }
        return array('data' => array_sum($bytes), 'voice' => array_sum($voice), 'sms' => $smsi, 'total' => $total);

        //return array('cdr' => $cdr, 'data' => array_sum($bytes), 'voice' => array_sum($voice), 'sms' => array_sum($sms));
    }
    
    public function get_cdr($sn, $msisdn, $mobile)
    {
        $total = 0;
        $bytes = array();
        $this->load->model('Admin_model');

        $cdr = array();

        //print_r($this->data['mobile']);
        //$this->load->library('artilium', array($this->data['setting']->companyid, $this->data['mobile']->PaymentType));
        //echo $msisdn . "<br />";
        //print_r($mobile);
        //$result = $this->artilium->GetCDRList($sn);
        $params = array('companyid' => $this->companyid,
            'action' => 'GetCDRList',
            'type' => 'POST PAID',
            'SN' => trim($sn),
            'From' => date('Y-m-01\T00:00:00+02:00'),
            'Till' => date('Y-m-d\TH:i:s'),
            'PageIndex' => 0,
            'PageSize' => 2500,
            'SortBy' => 0,
            'SortOrder' => 0);

        $sms = array();
        $voice = array();
        $bytes = array();

        $result = $this->Admin_model->artiliumPost($params);

        foreach ($result->ListInfo as $row) {
            if ($row->Cause != "1000") {
                if ($row->TypeCallId != 5) {
                    if ($row->MaskDestination != "72436") {
                        if (!empty($row->DestinationCountry)) {
                            $country = $row->DestinationCountry;
                        } else {
                            $country = "";
                        }

                        if ($row->TrafficTypeId == 2) {
                            $total = $total + includevat('21', $row->CurNumUnitsUsed);
                        //$cdr[] = array('Begintime' => format_cdr_time($row->Begintime), 'TrafficTypeId' => getTraficName($row->TrafficTypeId), 'MaskDestination' => $row->MaskDestination, 'DurationConnection' => str_replace('.', ',', round($row->DurationConnection / 1048576, 2)) . 'MB', 'CurNumUnitsUsed' => '€ ' . str_replace('.', ',', includevat('21', $row->CurNumUnitsUsed)));
                        } else {
                            if ($row->TrafficTypeId == "1" && $row->TypeCallId == "2") {
                            } else {
                                if ($row->TypeCallId != "3") {
                                    $total = $total + includevat('21', $row->CurNumUnitsUsed);
                                    //$cdr[] = array('Begintime' => format_cdr_time($row->Begintime), 'TrafficTypeId' => getTraficName($row->TrafficTypeId), 'MaskDestination' => $row->MaskDestination, 'DurationConnection' => gmdate("H:i:s", $row->DurationConnection), 'CurNumUnitsUsed' => '€ ' . str_replace('.', ',', includevat('21', $row->CurNumUnitsUsed)));
                                }
                            }
                        }
                    }
                }
            }

            if ($row->TypeCallId != 5) {
                if (!in_array($row->TrafficTypeId, array(2, 3))) {
                    if (strlen($row->MaskDestination) > 9) {
                        if ($row->TrafficTypeId) {
                            if ($row->TrafficTypeId == "1" && $row->DurationConnection < 1) {
                            } elseif ($row->TrafficTypeId == "5" && $row->DurationConnection < 1) {
                            } else {
                                if ($row->TrafficTypeId == "1" && $row->DurationConnection > 1) {
                                    $voice[] = $row->DurationConnection;
                                } elseif ($row->TrafficTypeId == "5" && $row->DurationConnection >= 1) {
                                    $sms[] = $row->DurationConnection;
                                }
                            }
                        }
                    }
                } elseif ($row->TrafficTypeId == 2) {
                    $bytes[] = $row->DurationConnection;
                }
            }
        }

        if (!$sms) {
            $smsi = '0';
        } else {
            $smsi = array_sum($sms);
        }
        return array('data' => array_sum($bytes), 'voice' => array_sum($voice), 'sms' => $smsi, 'total' => $total);

        //return array('cdr' => $cdr, 'data' => array_sum($bytes), 'voice' => array_sum($voice), 'sms' => array_sum($sms));
    }

    public function changepassword()
    {
        $this->data['dtt'] = 'clientarea_services.js?version=1.0';
        if (isPost()) {
            $client = $_SESSION['client'];
            $this->Teams_model->send_msg($this->uri->segment(4), "<br /><br /> CLIENT:<br />" . json_encode($client));
            $this->Teams_model->send_msg($this->uri->segment(4), "<br /><br /> POST:<br />" . $this->Client_model->getPassword($client['id']));
            if (password_verify(trim($_POST['currentpassword']), $this->Client_model->getPassword($client['id']))) {
                if (trim($_POST['password1']) == trim($_POST['password2'])) {
                    //$this->load->database('default', true);
                    $this->Client_model->ChangePassword($client['id'], $_POST);
                    $this->session->set_flashdata('success', lang('Your new password has been set successfully, please') . ' <a href="javascript:voice(0)" onclick="confirmation_logout();">' . lang('logout') . '</a> ' . lang('and try your new password'));

                    if (!empty($_POST['first'])) {
                        $this->Client_model->RemoveFirstlogin($client['id']);
                    }
                } else {
                    $this->session->set_flashdata('error', lang('Your new password does not match with confirmation password'));
                }
            } else {
                $this->session->set_flashdata('error', lang('Your current password is incorrect'));
            }
        }
        $this->data['client'] = (object) $_SESSION['client'];
        $this->data['title'] = "Change Password";
        $this->data['main_content'] = 'themes/' . $this->data['setting']->default_theme . '/mobile/changepassword';
        $this->load->view('themes/' . $this->data['setting']->default_theme . '/mobile/' . 'content', $this->data);
    }

    public function export_livecdr($sn, $msisdn)
    {
        $total = 0;
        set_time_limit(0);

        /*
        <th><?php echo lang('Date'); ?></th>
        <th><?php echo lang('Destination'); ?></th>
        <th><?php echo lang('Number'); ?></th>
        <th><?php echo lang('Duration'); ?></th>
        <th><?php echo lang('Type'); ?></th>
        <th><?php echo lang('Cost'); ?></th>

         */
        $bytes = array();
        $this->load->model('Admin_model');

        $cdr = array();
        $this->data['mobile'] = $this->Admin_model->mageboGet('GetSim/' . $msisdn);

        $sms = array();
        $voice = array();
        $bytes = array();
        $req = array('action' => 'GetCDRList',
            'type' => 'POST PAID',
            'From' => date('Y-m-01\TH:i:s+02:00'),
            'Till' => date('Y-m-d\TH:i:s+02:00', strtotime(date('Y-m-d') . ' + 1 days')),
            'SN' => $sn,
            'PageIndex' => 0,
            'PageSize' => 5000,
            'SortBy' => 0,
            'SortOrder' => 0,
            'companyid' => $this->data['setting']->companyid);
        $result = $this->Admin_model->artiliumPost($req);
        $header = array(
            lang('Begintime') => 'string',
            lang('Type') => 'string',
            lang('Destination') => 'string',
            lang('Duration') => 'string',

            lang('Cost') => 'string',

        );
        foreach ($result->ListInfo as $row) {
            if ($row->Cause != "1000") {
                if ($row->TypeCallId != 5) {
                    if ($row->MaskDestination != "72436") {
                        if (!empty($row->DestinationCountry)) {
                            $country = $row->DestinationCountry;
                        } else {
                            $country = "";
                        }

                        if ($row->TrafficTypeId == 2) {
                            $total = $total + includevat('21', $row->CurNumUnitsUsed);
                            $cdr[] = array('Begintime' => format_cdr_time($row->Begintime), 'TrafficTypeId' => getTraficName($row->TrafficTypeId), 'MaskDestination' => $row->MaskDestination, 'DurationConnection' => str_replace('.', ',', round($row->DurationConnection / 1048576, 2)) . 'MB', 'CurNumUnitsUsed' => '€ ' . str_replace('.', ',', includevat('21', $row->CurNumUnitsUsed)));
                        } else {
                            if ($row->TrafficTypeId == "1" && $row->TypeCallId == "2") {
                            } else {
                                if ($row->TypeCallId != "3") {
                                    $total = $total + includevat('21', $row->CurNumUnitsUsed);
                                    $cdr[] = array('Begintime' => format_cdr_time($row->Begintime), 'TrafficTypeId' => getTraficName($row->TrafficTypeId), 'MaskDestination' => $row->MaskDestination, 'DurationConnection' => gmdate("H:i:s", $row->DurationConnection), 'CurNumUnitsUsed' => '€ ' . str_replace('.', ',', includevat('21', $row->CurNumUnitsUsed)));
                                }
                            }
                        }
                    }
                }
            }
        }

        return $total;
    }

    public function get_cdr_amount($sn, $msisdn, $mobile)
    {
        $total = 0;
        $bytes = array();
        $this->load->model('Admin_model');

        $cdr = array();
        $this->data['mobile'] = $mobile;
        //print_r($this->data['mobile']);
        //$this->load->library('artilium', array($this->data['setting']->companyid, $this->data['mobile']->PaymentType));

        //$result = $this->artilium->GetCDRList($sn);
        $sms = array();
        $voice = array();
        $bytes = array();
        /*
        $req = array('action' => 'GetCDRList',
        'type' => $this->data['mobile']->PaymentType,
        'From' => date('Y-m-01\T00:00:00'),
        'Till' => date('Y-m-d\TH:i:s+02:00', strtotime(date('Y-m-d') . ' + 1 days')),
        'SN' => $sn,
        'PageIndex' => 0,
        'PageSize' => 2500,
        'SortBy' => 0,
        'SortOrder' => 0,
        'companyid' => $this->data['setting']->companyid);
        $result = $this->Admin_model->artiliumPost($req);
         */
        $params = array('companyid' => $this->data['setting']->companyid,
            'action' => 'GetCDRList',
            'type' => $mobile->PaymentType,
            'SN' => $sn,
            'From' => date('Y-m-01\T00:00:00+02:00'),
            'Till' => date('Y-m-d\TH:i:s'),
            'PageIndex' => 0,
            'PageSize' => 2500,
            'SortBy' => 0,
            'SortOrder' => 0);

        $sms = array();
        $voice = array();
        $bytes = array();

        $result = $this->Admin_model->artiliumPost($params);

        //print_r($result);
        //exit;
        foreach ($result->ListInfo as $row) {
            if (empty($row->DestinationCountry)) {
                $country = "";
            } else {
                $country = $row->DestinationCountry;
            }
            if ($row->Cause != "1000") {
                if ($row->TypeCallId != "5") {
                    if ($row->MaskDestination != "72436") {
                        if ($row->TrafficTypeId == "2") {
                            $total = $total + includevat('21', $row->CurNumUnitsUsed);
                            $cdr[] = array('Begintime' => format_cdr_time($row->Begintime), 'DestinationCountry' => $country, 'MaskDestination' => $row->MaskDestination, 'DurationConnection' => round($row->DurationConnection / 1048576, 2) . 'MB', 'TrafficTypeId' => $row->TrafficTypeId, 'CurNumUnitsUsed' => '€ ' . str_replace('.', ',', includevat('21', $row->CurNumUnitsUsed)));
                        } else {
                            if ($row->TrafficTypeId == "1" && $row->TypeCallId == "2") {
                            } else {
                                if ($row->TypeCallId != "3") {
                                    $total = $total + includevat('21', $row->CurNumUnitsUsed);
                                    $cdr[] = array('Begintime' => format_cdr_time($row->Begintime), 'DestinationCountry' => $country, 'MaskDestination' => $row->MaskDestination, 'DurationConnection' => gmdate("H:i:s", $row->DurationConnection), 'TrafficTypeId' => $row->TrafficTypeId, 'CurNumUnitsUsed' => '€ ' . str_replace('.', ',', includevat('21', $row->CurNumUnitsUsed)));
                                }
                            }
                        }
                    }
                }
            }

            if ($row->TypeCallId != 5) {
                if (!in_array($row->TrafficTypeId, array(2, 3))) {
                    if (strlen($row->MaskDestination) > 9) {
                        if ($row->TrafficTypeId) {
                            if ($row->TrafficTypeId == "1" && $row->DurationConnection < 1) {
                            } elseif ($row->TrafficTypeId == "5" && $row->DurationConnection < 1) {
                            } else {
                                if ($row->TrafficTypeId == "1" && $row->DurationConnection > 1) {
                                    $voice[] = $row->DurationConnection;
                                } elseif ($row->TrafficTypeId == "5" && $row->DurationConnection >= 1) {
                                    $sms[] = $row->DurationConnection;
                                }
                            }
                        }
                    }
                } elseif ($row->TrafficTypeId == 2) {
                    $bytes[] = $row->DurationConnection;
                }
            }
        }

        if (!$sms) {
            $smsi = '0';
        } else {
            $smsi = array_sum($sms);
        }
        return array('cdr' => $cdr, 'data' => array_sum($bytes), 'voice' => array_sum($voice), 'sms' => $smsi, 'usage' => $total);
        //return array('cdr' => $cdr, 'data' => array_sum($bytes));
    }
}
