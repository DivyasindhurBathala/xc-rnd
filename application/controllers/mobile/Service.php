<?php
defined('BASEPATH') or exit('No direct script access allowed');
class ServiceController extends CI_Controller
{
    protected $data = [];

    public function __construct()
    {
        // Ensure you run parent constructor
        parent::__construct();
        $this->config->set_item('language', $_SESSION['client']['language']);
        $this->lang->load('client');
        $this->companyid = get_companyidby_url(base_url());
        $this->data['setting'] = globofix($this->companyid);
    }
}
class Service extends ServiceController
{
    public function __construct()
    {
        parent::__construct();
        if (empty($_SESSION['theme'])) {
            $_SESSION['theme'] = "exocom";
        }
        $this->load->model('Client_model');

        $this->load->library('artilium', array('companyid' => $this->companyid));
    }

    public function index()
    {
        if ($this->session->cid == 54) {
            if (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4))) {
                redirect('client/service');
            }
        }

        $this->load->model('Admin_model');

        $stats = $this->Client_model->getStats($_SESSION['client']['id']);
        $this->data['title'] = lang("Subscription");
        ;
        $this->data['main_content'] = 'themes/' . $this->data['setting']->default_theme . '/mobile/' . 'service';
        $this->load->view('themes/' . $this->data['setting']->default_theme . '/mobile/' . 'content', $this->data);
    }
    public function buybundle()
    {
        $bundle = getMageboProductAddons($_POST['bundleid']);

        $ValidUntil = getFuturedate($_POST["from"], $bundle->bundle_duration_type, $bundle->bundle_duration_value);

        if ($_POST['from'] > date('Y-m-d')) {
            $ValidFrom = $_POST["from"] . "T00:00:00";
        } elseif ($_POST['from'] == date('Y-m-d')) {
            $ValidFrom = $_POST["from"] . "T" . date('H:i:s');
        } else {
            $this->session->set_flashdata('error', lang('Minimum date is today'));
            redirect('mobile/service/detail/' . $_POST['serviceid']);
            exit;
        }
        $this->load->model('Admin_model');
        $mobile = $this->Admin_model->getService($_POST['serviceid']);
        $client = $this->Admin_model->getClient($_SESSION['client']['id']);
        if ($mobile->userid == $_SESSION['client']['id']) {
            $result = $this->artilium->AddBundleAssign($mobile->details->msisdn_sn, $_POST['bundleid'], $ValidFrom, $ValidUntil);
            if ($result->result == "success") {
                $this->load->library('magebo', array('companyid' => $this->companyid));

                $this->magebo->ProcessPricing($mobile, $client->mageboid, $_POST['bundleid'], 0);
                logAdmin(array('companyid' => $this->companyid, 'userid' => $mobile->userid, 'serviceid' => $mobile->id, 'user' => $_SESSION['client']['firstname'] . ' ' . $_SESSION['client']['lastname'], 'ip' => $_SERVER['REMOTE_ADDR'], 'description' => 'Bundle ID: ' . $_POST['bundleid'] . ' for ' . $number . ' successfully added'));
                $this->session->set_flashdata('success', lang('Your bundle has been assigned'));
            } else {
                $this->session->set_flashdata('error', lang('There was an error while assigning your bundle'));
            }
            redirect('mobile/service/detail/' . $_POST['serviceid']);
        } else {
            $this->session->set_flashdata('error', lang('System cant accept your order for security reason, please contact our support'));
            redirect('mobile/service/detail/' . $_POST['serviceid']);
            exit;
        }
    }
    public function detail()
    {
        $this->data['productchange'] = hasChangeProductPlanned($this->uri->segment(4));

        $this->data['showusage'] = false;
        $this->load->model('Admin_model');
        $res = array();
        $excludes = array('MOBILE', 'Block All', 'MNP Beep');
        $id = $this->uri->segment(4);

        $this->data['service'] = $this->Admin_model->getService($id);

        if ($this->data['service']->userid != $this->session->userdata('client')['id']) {
            $this->session->set_flashdata('error', 'Access denied');
            redirect('mobile/dashboard');
            exit;
        }
        $this->data['client'] = $this->Admin_model->getClient($this->data['service']->userid);
        $this->data['title'] =  $this->data['service']->details->msisdn;

        if ($this->data['service']->status == 'Active') {
            if (in_array($this->data['service']->type, array("mobile"))) {
                $this->data['mobile'] = $this->data['service']->details;

                $ss = $this->artilium->GetSn($this->data['mobile']->msisdn_sim);

                $this->data['sn'] = (object) $ss->data;

                $this->data['showusage'] = true;

                $this->data['bundles'] = $this->artilium->GetBundleAssignList1(trim($this->data['sn']->SN));
               
                //array sorting asked by marcel

                if ($this->session->id == "1") {
                }
            } else {
                redirect('mobile/service/activate_mobile/' . $this->uri->segment('4'));
                exit;
            }
        } else {
        }
        $this->data['main_content'] = 'themes/' . $this->data['setting']->default_theme . '/mobile/' . 'service_mobile_details';
        $this->load->view('themes/' . $this->data['setting']->default_theme . '/mobile/' . 'content', $this->data);
    }
    public function getAddonid()
    {
        $this->db = $this->load->database('default', true);
        $addon = $this->db->query("select a.* from a_products_mobile_bundles a where a.id=?", array($_POST["id"]));
        $result = $addon->row_array();
        $result['recurring_total'] = number_format($result['recurring_total'], 2);
        echo json_encode($result);
    }
    public function getcdr($sn)
    {
        $id = $this->uri->segment(4);
        $this->data['service'] = $this->Admin_model->getService($id);
        $cdr = $this->artilium->get_cdr($this->data['service']);
                
        if ($cdr) {
            echo json_encode($cdr);
        } else {
            echo json_encode(array());
        }
    }
    public function getusage()
    {
        $id = $this->uri->segment(4);
        $this->data['service'] = $this->Admin_model->getService($id);

        $cdr = $this->artilium->get_cdr($this->data['service']);
        echo json_encode(array('voice' => second2hms($cdr['voice']), 'data' => round($cdr['data'] / 1048576, 2), 'sms' => $cdr['sms']));
    }
    public function activate_mobile()
    {
        if (isPost()) {
            $this->session->set_flashdata('success', lang('Your request has been received, your number will be activate shortly, if your are porting your number from other provider, wait until the  signal droped from your current provider'));
        }

        $this->load->model('Admin_model');
        $id = $this->uri->segment(4);
        $this->data['service'] = $this->Admin_model->getService($id);
        $this->data['main_content'] = 'themes/' . $this->data['setting']->default_theme . '/clients/' . 'service_mobile_activation';
        $this->load->view('themes/' . $this->data['setting']->default_theme . '/clients/' . 'content', $this->data);
    }
    public function mobile_stolen()
    {
        if (!empty($_POST['msisdn']) && !empty($_POST['typebar']) && isset($_POST['SN'])) {
            $this->data['mobile'] = $this->Admin_model->getService($_POST['serviceid']);

            $result = $this->artilium->UpdateCLI($sn->SN, 0);
            if ($result->result == "success") {
                $this->db->where('id', $_POST["serviceid"]);
                $this->db->update('a_services_mobile', array('status' => 'SimBlocked'));
                if ($_POST['ticket'] == 2) {
                    $this->load->model('Helpdesk_model');
                    $ticket['subject'] = "Simcard replacement for " . $_POST['msisdn'];
                    $ticket['userid'] = $_POST['userid'];
                    $ticket['name'] = $_SESSION['firstname'] . " " . $_SESSION["lastname"];
                    $ticket['email'] = $_SESSION["email"];
                    $ticket['deptid'] = getDefaultDepartment($this->companyid);
                    $ticket['categoryid'] = getCategoryidSimReplacement($this->companyid);
                    $ticket['message'] = "Customer request simcard replacement for :" . $_POST['SimCardNbr'];
                    $ticket['date'] = date('Y-m-d H:i:s');
                    $ticket['date'] = date('Y-m-d H:i:s');
                    $ticket['status'] = 'Open';
                    $ticket['tid'] = random_str('alphacaps', 3) . '-' . random_str('num', 6) . '-' . date('Y');

                    $id = $this->Helpdesk_model->insert_ticket($ticket);
                    $this->session->set_flashdata('success', lang('Service has been blocked, our agent  has been notified for your simcard replacement'));
                } else {
                    $this->session->set_flashdata('success', lang('Service has been blocked, if you wish to unblock please click button Unblock Sim'));
                }
            } else {
                $this->session->set_flashdata('error', lang('Failed to change Service Status'));
            }
        } else {
            $this->session->set_flashdata('error', lang('there has been an error when requesting service block, please contact our support'));
        }

        redirect('mobile/service/detail/' . $_POST['serviceid']);
    }

    public function update_sim_language()
    {
        $this->load->model('Admin_model');

        if ($this->data['setting']->companyid == 2) {
            $companyid = array(1, 2, 3, 4, 6, 8, 9, 25, 26, 28, 29, 31, 34, 35, 37, 41, 47, 50);
        } else {
            $companyid[] = $this->data['setting']->companyid;
        }

        if ($_POST['language'] == "3") {
            $l = "Dutch";
        } elseif ($_POST['language'] == "2") {
            $l = "French";
        } elseif ($_POST['language'] == "1") {
            $l = "English";
        }

        if ($this->Admin_model->IsAllowedMobile($_POST['msisdn'], $companyid)) {
            $result = $this->artilium->UpdateLanguage($_POST['sn'], $_POST['language']);
            if ($result->result == "success") {
                $this->db->where('serviceid', $_POST['serviceid']);
                $this->db->update('a_services_mobile', array('msisdn_languageid' => $_POST['language']));

                $res = array('result' => "success", 'message' => lang('Language has been changed to: ' . $l));
            } else {
                $res = array('result' => "error", 'message' => lang('There were an error while changing your voice mail language.'), 'extra' => $result);
            }
        } else {
            $res = array('result', 'Access denied');
        }

        echo json_encode($res);
    }

    public function change_packagesetting()
    {
        header('Content-Type: application/json');
        $this->load->model('Admin_model');
        if ($this->Admin_model->IsAllowedService($_POST['serviceid'], $_SESSION['client']['id'])) {
            if (!empty($_POST['sn']) && !empty($_POST['id']) && isset($_POST['val'])) {
                $result = $this->artilium->UpdatePackageOptionsForSN(trim($_POST['sn']), $_POST['id'], $_POST['val']);
                echo json_encode($result);
            } else {
                echo json_encode(array('result' => false, 'message' => lang('Empty sn, id, val')));
            }
        } else {
            echo json_encode(array('result' => false, 'message' => lang('Access Denied')));
        }
    }
    /*
    public function get_cdr($sn, $msisdn, $mobile)
    {
    $total = 0;
    $bytes = array();
    $this->load->model('Admin_model');

    $cdr = array();
    $this->data['mobile'] = $mobile;

    $sms = array();
    $voice = array();
    $bytes = array();

    $params = array('companyid' => $this->companyid,
    'action' => 'GetCDRList',
    'type' => $mobile->ptype,
    'SN' => $sn,
    'From' => date('Y-m-01\T00:00:00+02:00'),
    'Till' => date('Y-m-d\TH:i:s'),
    'PageIndex' => 0,
    'PageSize' => 2500,
    'SortBy' => 0,
    'SortOrder' => 0);

    $sms = array();
    $voice = array();
    $bytes = array();

    $result = $this->Admin_model->artiliumPost($params);

    foreach ($result->ListInfo as $row) {
    if (empty($row->DestinationCountry)) {
    $country = "";
    } else {
    $country = $row->DestinationCountry;
    }
    if ($row->Cause != "1000") {
    if ($row->TypeCallId != "5") {
    if ($row->MaskDestination != "72436") {
    if ($row->TrafficTypeId == "2") {
    $total = $total + includevat('21', $row->CurNumUnitsUsed);
    $cdr[] = array('Begintime' => format_cdr_time($row->Begintime), 'DestinationCountry' => $country, 'MaskDestination' => $row->MaskDestination, 'DurationConnection' => round($row->DurationConnection / 1048576, 2) . 'MB', 'TrafficTypeId' => $row->TrafficTypeId, 'CurNumUnitsUsed' => '€ ' . str_replace('.', ',', includevat('21', $row->CurNumUnitsUsed)));
    } else {

    if ($row->TrafficTypeId == "1" && $row->TypeCallId == "2") {

    } else {
    if ($row->TypeCallId != "3") {
    $total = $total + includevat('21', $row->CurNumUnitsUsed);
    $cdr[] = array('Begintime' => format_cdr_time($row->Begintime), 'DestinationCountry' => $country, 'MaskDestination' => $row->MaskDestination, 'DurationConnection' => gmdate("H:i:s", $row->DurationConnection), 'TrafficTypeId' => $row->TrafficTypeId, 'CurNumUnitsUsed' => '€ ' . str_replace('.', ',', includevat('21', $row->CurNumUnitsUsed)));
    }
    }

    }

    }

    }
    }

    if ($row->TypeCallId != 5) {
    if (!in_array($row->TrafficTypeId, array(2, 3))) {
    if (strlen($row->MaskDestination) > 9) {

    if ($row->TrafficTypeId) {

    if ($row->TrafficTypeId == "1" && $row->DurationConnection < 1) {

    } elseif ($row->TrafficTypeId == "5" && $row->DurationConnection < 1) {

    } else {
    if ($row->TrafficTypeId == "1" && $row->DurationConnection > 1) {
    $voice[] = $row->DurationConnection;
    } elseif ($row->TrafficTypeId == "5" && $row->DurationConnection >= 1) {
    $sms[] = $row->DurationConnection;
    }

    }

    }

    }

    } elseif ($row->TrafficTypeId == 2) {

    $bytes[] = $row->DurationConnection;

    }
    }

    }

    if (!$sms) {
    $smsi = '0';
    } else {
    $smsi = array_sum($sms);
    }
    return array('cdr' => $cdr, 'data' => array_sum($bytes), 'voice' => array_sum($voice), 'sms' => $smsi, 'usage' => $total);
    //return array('cdr' => $cdr, 'data' => array_sum($bytes));

    }
     */
    /*
public function export_livecdr()
{
$total = 0;
set_time_limit(0);
$sn = $this->uri->segment(6);
$msisdn = $this->uri->segment(4);
$serviceid = $this->uri->segment(5);
$this->load->library('xlswriter');
$fileName = 'LIVECDR-' . $msisdn . '.csv';

$bytes = array();
$this->load->model('Admin_model');

$cdr = array();
$this->data['mobile'] = $this->Admin_model->mageboGet('GetSim/' . $msisdn);

$sms = array();
$voice = array();
$bytes = array();
$req = array('action' => 'GetCDRList',
'type' => $this->data['mobile']->PaymentType,
'From' => date('Y-m-01\TH:i:s+02:00'),
'Till' => date('Y-m-d\TH:i:s+02:00', strtotime(date('Y-m-d') . ' + 1 days')),
'SN' => $sn,
'PageIndex' => 0,
'PageSize' => 2500,
'SortBy' => 0,
'SortOrder' => 0,
'companyid' => $this->data['setting']->companyid);
$result = $this->Admin_model->artiliumPost($req);
$header = array(
lang('Begintime') => 'string',
lang('Type') => 'string',
lang('Destination') => 'string',
lang('Duration') => 'string',

lang('Cost') => 'string',

);
foreach ($result->ListInfo as $row) {
if ($row->Cause != "1000") {
if ($row->TypeCallId != 5) {
if ($row->MaskDestination != "72436") {

if (!empty($row->DestinationCountry)) {
$country = $row->DestinationCountry;
} else {

$country = "";
}

if ($row->TrafficTypeId == 2) {
$total = $total + includevat('21', $row->CurNumUnitsUsed);
$cdr[] = array('Begintime' => format_cdr_time($row->Begintime), 'TrafficTypeId' => getTraficName($row->TrafficTypeId), 'MaskDestination' => $row->MaskDestination, 'DurationConnection' => str_replace('.', ',', round($row->DurationConnection / 1048576, 2)) . 'MB', 'CurNumUnitsUsed' => '€ ' . str_replace('.', ',', includevat('21', $row->CurNumUnitsUsed)));
} else {
if ($row->TrafficTypeId == "1" && $row->TypeCallId == "2") {

} else {
if ($row->TypeCallId != "3") {
$total = $total + includevat('21', $row->CurNumUnitsUsed);
$cdr[] = array('Begintime' => format_cdr_time($row->Begintime), 'TrafficTypeId' => getTraficName($row->TrafficTypeId), 'MaskDestination' => $row->MaskDestination, 'DurationConnection' => gmdate("H:i:s", $row->DurationConnection), 'CurNumUnitsUsed' => '€ ' . str_replace('.', ',', includevat('21', $row->CurNumUnitsUsed)));
}
}
}

}

}

}
}

$cdr[] = array('Begintime' => '', 'MaskDestination' => '', 'DurationConnection' => '', 'TrafficTypeId' => 'Total', 'CurNumUnitsUsed' => '€ ' . str_replace('.', ',', $total));

if (count($cdr) > 0) {
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
$this->xlswriter->writeSheet($cdr, 'Sheet1', $header);
$this->xlswriter->setAuthor('Simson Parlindungan');
$this->xlswriter->setTitle('LIVE CDR Export');
$this->xlswriter->setCompany('United');
echo $this->xlswriter->writeToString();
} else {
$this->session->set_flashdata('error', 'no data to be exported');
redirect('mobile/service/detail/' . $serviceid);
}

}
 */
}
