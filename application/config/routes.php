<?php
defined('BASEPATH') or exit('No direct script access allowed');

$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = false;
$route['admin'] = 'admin/dashboard/index';
$route['client'] = 'client/dashboard/index';
$route['mobile'] = 'mobile/dashboard/index';
$route['reseller'] = 'reseller/dashboard/index';
$route['master'] = 'master/setting/companies';
//$route['denied'] = 'errors/access_denied';
$route['nummerbehoud'] = 'client/portinondemand/index';
$route['client/nummerbehoud'] = 'client/portinondemand/index';
$route['client/nummerbehoud/request_new'] = 'client/portinondemand/request_new';
$route['api/v3/crm/(:any)/(:num)/(:any)'] = 'api/v3/crm/$1/$3/$2';
$route['api/v3/crm/(:any)/(:num)'] = 'api/v3/crm/$1/index/$2';
//$route['api/v2/crm/(:any)/(:num)'] = 'api/v2/crm/customer/index/$1';
