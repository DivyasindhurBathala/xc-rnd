<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Check_session {
	private $CI;

	function __construct() {
		$this->CI = &get_instance();

		if (!isset($this->CI->session)) { //Check if session lib is loaded or not
			$this->CI->load->library('session'); //If not loaded, then load it here
		}
	}

	public function validation() {

		$admin = $this->CI->session->userdata('logged');
		if (!empty($_SESSION['client']['islogged'])) {
			$client = $_SESSION['client']['islogged'];
		} else {
			$client = false;
		}

		if (in_array($this->CI->router->class, array('auth', 'licences', 'install', 'import', 'clientarea', 'authentication', 'whois', 'payment', 'homepage', 'bob'))) {
			return;

		} else {
			$q = $this->CI->db->query("select * from configuration where name=?", array('licences'));

			if ($this->CI->uri->segment(1) == "admin") {

				if (!$admin) {
					redirect('admin/auth');
				}

				if (in_array($this->CI->router->class, array('subscription'))) {

					if ($this->CI->uri->segment(3) == "detail") {
						$service = $this->CI->db->query("select * from subscriptions where id=?", array($this->CI->uri->segment(4)));
						$r = $this->CI->db->query('select * from mod_sabbam where serviceid=? and userid=?', array($service->row()->id, $service->row()->userid));
						if (empty($r->num_rows())) {
							$this->CI->db->insert('mod_sabbam', array('serviceid' => $service->row()->id, 'userid' => $service->row()->userid, 'date_start' => $service->row()->regdate));
						}

					}

				}

			} elseif ($this->CI->uri->segment(1) == "client") {

				if (!$client) {
					redirect('client/auth');
				} else {

				}
			}

		}

	}

}
