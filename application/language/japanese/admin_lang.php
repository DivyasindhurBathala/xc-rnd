<?php
$lang["Promo -  Free mobile internet 10GB"] = "Promo – Internet mobile gratuit 10GB: %s";
$lang["Main Menu"] = "Main Menu";
$lang["Dashboard"] = "Dashboard";
$lang["Clients"] = "Clients";
$lang["Client List"] = "Client List";
$lang["Add New Clients"] = "Add New Clients";
$lang["List Client Notes"] = "List Client Notes";
$lang["Agents/Reseller"] = "Agents/Reseller";
$lang["Billing"] = "Billing";
$lang["Invoice List"] = "Invoice List";
$lang["Proforma Invoice List"] = "Proforma Invoice List";
$lang["Creditnote List"] = "Creditnote List";
$lang["Bank Transactions"] = "Bank Transactions";
$lang["Online Transactions"] = "Online Transactions";
$lang["Invoice Reminder Logs"] = "Invoice Reminder Logs";
$lang["Services"] = "Services";
$lang["Services List"] = "Services List";
$lang["Suspended Services"] = "Suspended Services";
$lang["Create New Service Mobile"] = "Create New Service Mobile";
$lang["Terminated Services"] = "Terminated Services";
$lang["Bundle Usage Monitor"] = "Bundle Usage Monitor";
$lang["Pending PortIn Requests"] = "Pending PortIn Requests";
$lang["Pending PortOut Requests"] = "Pending PortOut Requests";
$lang["Pending Orders"] = "Pending Orders";
$lang["Porting On Demand"] = "Porting On Demand";
$lang["SUM Reports"] = "SUM Reports";
$lang["Helpdesk"] = "Helpdesk";
$lang["SEPA From Bank"] = "SEPA From Bank";
$lang["Swap Request"] = "Swap Request";
$lang["Logs"] = "Logs";
$lang["Calendar"] = "Calendar";
$lang["Configuration"] = "Configuration";
$lang["Setting"] = "Setting";
$lang["New"] = "New";
$lang["General Setting"] = "General Setting";
$lang["PDF Documents Template"] = "PDF Documents Template";
$lang["Email Templates"] = "Email Templates";
$lang["API Tokens / IPs"] = "API Tokens / IPs";
$lang["Promotions & Discounts"] = "Promotions & Discounts";
$lang["Staff"] = "Staff";
$lang["Backup"] = "Backup";
$lang["Support Department"] = "Support Department";
$lang["Roles/Permissions"] = "Roles/Permissions";
$lang["Mass SMS"] = "Mass SMS";
$lang["Translations"] = "Translations";
$lang["Master Tools"] = "Master Tools";
$lang["Companies"] = "Companies";
$lang["SEPA To Bank"] = "SEPA To Bank";
$lang["Packages Changes"] = "Packages Changes";
$lang["Termination List"] = "Termination List";
$lang["Master DB Untility"] = "Master DB Untility";
$lang["VOIP"] = "VOIP";
$lang["Sync Source"] = "Sync Source";
$lang["Profile Details"] = "Profile Details";
$lang["Logout"] = "Logout";
$lang["Page Header"] = "Page Header";
$lang["Start typing to search..."] = "Start typing to search...";
$lang["No Bundle assigned"] = "No Bundle assigned";
$lang["Services Summary"] = "Services Summary";
$lang["Back to Client Details"] = "Back to Client Details";
$lang["This service is currently suspended, it does not mean billing would stop"] = "This service is currently suspended, it does not mean billing would stop";
$lang["Product Brand"] = "Product Brand";
$lang["Service ID"] = "Service ID";
$lang["Date Reg"] = "Date Reg";
$lang["Product Name"] = "Product Name";
$lang["Recurring"] = "Recurring";
$lang["Billingcycle"] = "Billingcycle";
$lang["Monthly"] = "Monthly";
$lang["Status"] = "Status";
$lang["Active"] = "Active";
$lang["Contract date"] = "Contract date";
$lang["Contract Length"] = "Contract Length";
$lang["month"] = "month";
$lang["First Possible Cancellation"] = "First Possible Cancellation";
$lang["BillingID"] = "BillingID";
$lang["Porting Date Wish"] = "Porting Date Wish";
$lang["iGeneralPricing"] = "iGeneralPricing";
$lang["Stolen Phone?"] = "Stolen Phone?";
$lang["Enable Outbound"] = "Enable Outbound";
$lang["Unsuspend"] = "Unsuspend";
$lang["CDR"] = "CDR";
$lang["CDR In"] = "CDR In";
$lang["Change Language"] = "Change Language";
$lang["SWAP Simcard"] = "SWAP Simcard";
$lang["Terminate Subscription"] = "Terminate Subscription";
$lang["Change Subscription"] = "Change Subscription";
$lang["Open Platform Logs"] = "Open Platform Logs";
$lang["Client ID"] = "Client ID";
$lang["Contact"] = "Contact";
$lang["Address"] = "Address";
$lang["Email"] = "Email";
$lang["Phonenumber"] = "Phonenumber";
$lang["Type"] = "Type";
$lang["Package"] = "Package";
$lang["ValidFrom"] = "ValidFrom";
$lang["Usage"] = "Usage";
$lang["Percentage"] = "Percentage";
$lang["Add 30 days Bundle"] = "Add 30 days Bundle";
$lang["Add Extra Bundle"] = "Add Extra Bundle";
$lang["Add Call Ceilling"] = "Add Call Ceilling";
$lang["Welcome Letter"] = "Welcome Letter";
$lang["Porting Letter"] = "Porting Letter";
$lang["Send SIM Sent"] = "Send SIM Sent";
$lang["Swap Letter"] = "Swap Letter";
$lang["Add Portability Number to Magebo"] = "Add Portability Number to Magebo";
$lang["Call ceiling"] = "Call ceiling";
$lang["UsagePlan"] = "UsagePlan";
$lang["Action"] = "Action";
$lang["SerialNumber"] = "SerialNumber";
$lang["SIMCARD NO"] = "SIMCARD NO";
$lang["PIN"] = "PIN";
$lang["PUK1"] = "PUK1";
$lang["PUK2"] = "PUK2";
$lang["SIMCARD Type"] = "SIMCARD Type";
$lang["Languages"] = "Languages";
$lang["Voicemail"] = "Voicemail";
$lang["ENGLISH"] = "ENGLISH";
$lang["SOURCE TYPE"] = "SOURCE TYPE";
$lang["PORTING"] = "PORTING";
$lang["TYPE Porting"] = "TYPE Porting";
$lang["0"] = "0";
$lang["DonorMSISDN"] = "DonorMSISDN";
$lang["DonorSimCardNbr"] = "DonorSimCardNbr";
$lang["Suspend"] = "Suspend";
$lang["Terminate"] = "Terminate";
$lang["Move Service"] = "Move Service";
$lang["Ticket ID"] = "Ticket ID";
$lang["Date"] = "Date";
$lang["Subject"] = "Subject";
$lang["Department"] = "Department";
$lang["Country group has been modified to"] = "Country group has been modified to";
$lang["Add Porting"] = "Add Porting";
$lang["Mobile Number"] = "Mobile Number";
$lang["REQUIRED"] = "REQUIRED";
$lang["Prepaid"] = "Prepaid";
$lang["Postpaid"] = "Postpaid";
$lang["Current Provider"] = "Current Provider";
$lang["SIMcard Number"] = "SIMcard Number";
$lang["Customer Type"] = "Customer Type";
$lang["Residential"] = "Residential";
$lang["Bussiness"] = "Bussiness";
$lang["Client Number from other provider"] = "Client Number from other provider";
$lang["PortIN Date Wish"] = "PortIN Date Wish";
$lang["Submit"] = "Submit";
$lang["The bundle you are ordering will be auto renew for next month until the date you specified below  and it will be auto prorata for the current month if aplicable:"] = "The bundle you are ordering will be auto renew for next month until the date you specified below  and it will be auto prorata for the current month if aplicable:";
$lang["Charge Customer?"] = "Charge Customer?";
$lang["each Month until Valid Until"] = "each Month until Valid Until";
$lang["Price"] = "Price";
$lang["VALID FROM"] = "VALID FROM";
$lang["VALID UNTIL"] = "VALID UNTIL";
$lang["Add Bundle"] = "Add Bundle";
$lang["Block Originating/Outbound Traffic"] = "Block Originating/Outbound Traffic";
$lang["Do you wish to proceed"] = "Do you wish to proceed";
$lang["Save Changes"] = "Save Changes";
$lang["Reload Credit"] = "Reload Credit";
$lang["Credit to add"] = "Credit to add";
$lang["Reload"] = "Reload";
$lang["Advance Setting"] = "Advance Setting";
$lang["Enable All Services"] = "Enable All Services";
$lang["Do you wish to enable all services including Roaming,Premium,Data,International & ETC"] = "Do you wish to enable all services including Roaming,Premium,Data,International & ETC";
$lang["Add Billed Bundle"] = "Add Billed Bundle";
$lang["This will activate Bundle for 30 days from the date you choose below, and customer will be invoiced"] = "This will activate Bundle for 30 days from the date you choose below, and customer will be invoiced";
$lang["Phone Stolen?"] = "Phone Stolen?";
$lang["Create Ticket SIM Replacement"] = "Create Ticket SIM Replacement";
$lang["NO"] = "NO";
$lang["YES"] = "YES";
$lang["CLI(Number)"] = "CLI(Number)";
$lang["Report Stolen"] = "Report Stolen";
$lang["Phone Found or Simcard Replaced?"] = "Phone Found or Simcard Replaced?";
$lang["Remove Stolen Record"] = "Remove Stolen Record";
$lang["Do you wish do suspend?"] = "Do you wish do suspend?";
$lang["No"] = "No";
$lang["Yes"] = "Yes";
$lang["Do you wish to change Platform Language"] = "Do you wish to change Platform Language";
$lang["VoiceMail"] = "VoiceMail";
$lang["English"] = "English";
$lang["French"] = "French";
$lang["Dutch"] = "Dutch";
$lang["Do you wish do unsuspend?"] = "Do you wish do unsuspend?";
$lang["Do you wish to change the contract terms?"] = "Do you wish to change the contract terms?";
$lang["Contract Terms"] = "Contract Terms";
$lang["Do you wish to swap SIMcard?"] = "Do you wish to swap SIMcard?";
$lang["Current SIMCARD"] = "Current SIMCARD";
$lang["New SIMCARD"] = "New SIMCARD";
$lang["This will revoke old simcard, please make sure the simcard has been received by customer before executing this"] = "This will revoke old simcard, please make sure the simcard has been received by customer before executing this";
$lang["Charge Customer for SWAP"] = "Charge Customer for SWAP";
$lang["Amount"] = "Amount";
$lang["Yes Swap Simcard"] = "Yes Swap Simcard";
$lang["Do you wish to Change the Package?"] = "Do you wish to Change the Package?";
$lang["Product"] = "Product";
$lang["Contract"] = "Contract";
$lang["Month"] = "Month";
$lang["Quarterly"] = "Quarterly";
$lang["Semi Annually"] = "Semi Annually";
$lang["Annually"] = "Annually";
$lang["Bienially"] = "Bienially";
$lang["Price Recurring"] = "Price Recurring";
$lang["Date Start"] = "Date Start";
$lang["Please choose the date this package to be activated"] = "Please choose the date this package to be activated";
$lang["Yes Change Package"] = "Yes Change Package";
$lang["Do you wish to Terminate This SIM?"] = "Do you wish to Terminate This SIM?";
$lang["Date Cancellation"] = "Date Cancellation";
$lang["Now"] = "Now";
$lang["Future"] = "Future";
$lang["Date Termination"] = "Date Termination";
$lang["Yes Cancel Service"] = "Yes Cancel Service";
$lang["Do you wish to Cancel this Cancellation?"] = "Do you wish to Cancel this Cancellation?";
$lang["Yes Cancel The Cancellation"] = "Yes Cancel The Cancellation";
$lang["Call Detail records"] = "Call Detail records";
$lang["Number"] = "Number";
$lang["Description"] = "Description";
$lang["Duration"] = "Duration";
$lang["Cost"] = "Cost";
$lang["Close"] = "Close";
$lang["Export Excel"] = "Export Excel";
$lang["Do you wish to disable SumPlan for current month"] = "Do you wish to disable SumPlan for current month";
$lang["Process"] = "Process";
$lang["Do you wish to remove this sumPlan forerver?"] = "Do you wish to remove this sumPlan forerver?";
$lang["Incoming Call Detail records"] = "Incoming Call Detail records";
$lang["Do you wish to move this subscription to another customer"] = "Do you wish to move this subscription to another customer";
$lang["Artilium ID"] = "Artilium ID";
$lang["CustomerName"] = "CustomerName";
$lang["Relationid"] = "Relationid";
$lang["Move Subscription"] = "Move Subscription";
$lang["Change the Expired date of the Bundle"] = "Change the Expired date of the Bundle";
$lang["Date Valid Until"] = "Date Valid Until";
$lang["Platform Logs records"] = "Platform Logs records";
$lang["Command"] = "Command";
$lang["Value"] = "Value";
$lang["Are you sure? this will add bundle according to the products! exclude arta"] = "Are you sure? this will add bundle according to the products! exclude arta";
$lang["Are you sure? this will fix porting issue on magebo"] = "Are you sure? this will fix porting issue on magebo";
$lang["There is an error to complete your request"] = "There is an error to complete your request";
$lang["there has been an error, please try again later"] = "there has been an error, please try again later";
$lang["Are you sure?, this will delete the records of subscription changes"] = "Are you sure?, this will delete the records of subscription changes";
$lang["Please wait while we loading all tables for you"] = "Please wait while we loading all tables for you";
$lang["Options"] = "Options";
$lang["Invoices"] = "Invoices";
$lang["Export Invoices"] = "Export Invoices";
$lang["Export Invoices (excel)"] = "Export Invoices (excel)";
$lang["Show Invoice Overdue"] = "Show Invoice Overdue";
$lang["List Invoices"] = "List Invoices";
$lang["Invoice Number"] = "Invoice Number";
$lang["Billing ID"] = "Billing ID";
$lang["Invoice Date"] = "Invoice Date";
$lang["Invoice Duedate"] = "Invoice Duedate";
$lang["Export Invoice"] = "Export Invoice";
