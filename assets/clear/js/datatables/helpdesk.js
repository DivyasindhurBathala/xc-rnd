$(document).ready(function()
{
$.getJSON(window.location.protocol + '//' + window.location.host + '/admin/complete/get_lang', function(data) {
$('#helpdesk').DataTable({
"autoWidth": false,
"processing": true,
"orderCellsTop": true,
"ordering": true,
"serverSide": true,
"ajax":window.location.protocol + '//' + window.location.host + '/reseller/table/get_tickets',
"aaSorting": [[4, 'desc']],
"language": {
 "url": window.location.protocol + '//' + window.location.host +"/assets/clear/js/datatables/lang/" + data.result + ".json"
},
"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
$('td:eq(2)', nRow).html('<a href="'+window.location.protocol + '//' + window.location.host + '/reseller/helpdesk/detail/' + aData[8] + '"><strong>' + aData[2] + '</strong></a>');
$('td:eq(0)', nRow).html('<a href="'+window.location.protocol + '//' + window.location.host + '/reseller/helpdesk/detail/' + aData[8] + '"><strong>' + aData[0] + '</strong></a>');
if(aData[9] > 0){
$('td:eq(1)', nRow).html('<a href="'+window.location.protocol + '//' + window.location.host + '/reseller/dashboard/viewclient/' + aData[9] + '"><strong>' + aData[1] + '</strong></a>');
}
$('td:eq(8)', nRow).html(aData[10]);

//$('td:eq(1)', nRow).html('<a href="https://invoice.xmusix.eu/admin/clientssummary.php?userid=' + aData[5] + '">' + aData[1] + '</a>');
return nRow;
},

});
});
});
