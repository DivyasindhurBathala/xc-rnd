$(document).ready(function() {
    $('#loadingController').modal({
        backdrop: 'static',
        keyboard: false
    });
    console.log('loading loader');
    $.getJSON(window.location.protocol + '//' + window.location.host + '/admin/complete/get_lang', function(data) {
        var userid = $('#userid').val();
        var mageboid = $('#mageboid').val();
        $('.loading-text').html('Loading Subscriptions..');
        $('#services').DataTable({
            "autoWidth": false,
            "processing": true,
            "orderCellsTop": true,
            "ordering": true,
            "serverSide": true,
            "ajax": window.location.protocol + '//' + window.location.host + '/admin/table/getclient_services/' + userid,
            "aaSorting": [
                [0, 'desc']
            ],
            "columnDefs": [
                { "searchable": false, "targets": 1 }
            ],
            "language": {
                "url": window.location.protocol + '//' + window.location.host + "/assets/clear/js/datatables/lang/" + data.result + ".json"
            },
            "fnRowCallback": function(nRow, aData, iDisplayIndex) {
                if (aData[2] != "Terminated" || aData[2] != "Cancelled") {
                    $('td:eq(4)', nRow).html('<a href="' + window.location.protocol + '//' + window.location.host + '/admin/subscription/detail/' + aData[6] + '">' + aData[4] + '</a>');
                    $('td:eq(0)', nRow).html('<a href="' + window.location.protocol + '//' + window.location.host + '/admin/subscription/detail/' + aData[6] + '">' + aData[0] + '</a>');
                }
                $('td:eq(3)', nRow).html(data.currency + aData[3]);
                $('td:eq(5)', nRow).html('Maandelijks');
                if (aData[7] == 33) {
                    if (aData[2] == "Suspended") {
                        $('td:eq(6)', nRow).html('<button class="btn btn-xs btn-success" onclick="UnSuspendService(' + aData[6] + ');"><i class="fa fa-play"></i> Unsuspend</button>');
                    } else if (aData[2] == "Active") {

                        $('td:eq(6)', nRow).html('<button class="btn btn-xs btn-danger" onclick="SuspendService(' + aData[6] + ');"><i class="fa fa-stop"></i> Suspend</button>');
                    } else {
                        $('td:eq(6)', nRow).html('<a class="btn btn-xs btn-primary" href="' + window.location.protocol + '//' + window.location.host + '/admin/subscription/detail/' + aData[6] + '"><i class="fa fa-eye"></i> Detail</a>');
                    }
                } else {
                    if (aData[2] == "Pending") {
                        if (aData[4] == "") {

                            $('td:eq(6)', nRow).html('<a class="btn btn-xs btn-primary" href="' + window.location.protocol + '//' + window.location.host + '/admin/subscription/detail/' + aData[6] + '"><i class="fa fa-eye"></i> Detail</a> <a class="btn btn-xs btn-danger" onclick="deleteSubscription(' + aData[6] + ')"><i class="fa fa-trash"></i> Delete</button>');
                        } else {
                            $('td:eq(6)', nRow).html('<a class="btn btn-xs btn-primary" href="' + window.location.protocol + '//' + window.location.host + '/admin/subscription/detail/' + aData[6] + '"><i class="fa fa-eye"></i> Detail</a>');
                        }
                    } else {
                        $('td:eq(6)', nRow).html('<a class="btn btn-xs btn-primary" href="' + window.location.protocol + '//' + window.location.host + '/admin/subscription/detail/' + aData[6] + '"><i class="fa fa-eye"></i> Detail</a>');
                    }

                }

                return nRow;
            },
        });
        $('.loading-text').html('Loading Invoices..');
        $('#dt_invoice').DataTable({
            "autoWidth": false,
            "ajax": {
                "url": window.location.protocol + '//' + window.location.host + '/admin/table/getclient_invoices/' + mageboid,
                "dataSrc": ""
            },
            "aaSorting": [
                [0, 'desc']
            ],
            "columns": [
                { "data": "iInvoiceNbr" },
                { "data": "dInvoiceDate" },
                { "data": "dInvoiceDueDate" },
                { "data": "iInvoiceStatus" },
                { "data": "mInvoiceAmount" },
                { "data": "Type" }
            ],
            "language": {
                "url": window.location.protocol + '//' + window.location.host + "/assets/clear/js/datatables/lang/" + data.result + ".json"
            },
            "fnRowCallback": function(nRow, aData, iDisplayIndex) {
                $('td:eq(4)', nRow).html(data.currency + aData.mInvoiceAmount);
                if (aData.Type == "Invoice") {
                    $('td:eq(0)', nRow).html('<a href="' + window.location.protocol + '//' + window.location.host + '/admin/invoice/detail/' + aData.iInvoiceNbr + '/' + aData.iAddressNbr + '">' + aData.iInvoiceNbr + '</a>');

                } else {

                    $('td:eq(0)', nRow).html('<a href="' + window.location.protocol + '//' + window.location.host + '/admin/creditnote/download/' + aData.iInvoiceNbr + '/' + aData.iAddressNbr + '" class="btn btn-primary btn-md" target="_blank"><i class="fa fa-download"></i> ' + aData.iInvoiceNbr + '</a>');
                }

                return nRow;
            },
        });


        $('.loading-text').html('Loading Proforma..');
        $('#dt_proformas').DataTable({
            "autoWidth": false,
            "processing": true,
            "orderCellsTop": true,
            "ordering": true,
            "serverSide": true,
            "ajax": window.location.protocol + '//' + window.location.host + '/admin/table/get_proformas/' + userid,
            "aaSorting": [
                [0, 'desc']
            ],
            "columnDefs": [
                { "searchable": false, "targets": 1 }
            ],
            "language": {
                "url": window.location.protocol + '//' + window.location.host + "/assets/clear/js/datatables/lang/" + data.result + ".json"
            },
            "fnRowCallback": function(nRow, aData, iDisplayIndex) {
                $('td:eq(0)', nRow).html('<a href="' + window.location.protocol + '//' + window.location.host + '/admin/proforma/detail/' + aData[5] + '">' + aData[0] + '</a>');
                return nRow;
            },
        });


        $('.loading-text').html('Loading Creditnotes..');
        $('#dt_creditnotes').DataTable({
            "autoWidth": false,
            "ajax": {
                "url": window.location.protocol + '//' + window.location.host + '/admin/table/getclient_creditnotes/' + mageboid,
                "dataSrc": ""
            },
            "aaSorting": [
                [0, 'desc']
            ],
            "columns": [
                { "data": "iInvoiceNbr" },
                { "data": "dInvoiceDate" },
                { "data": "mInvoiceAmount" },
                { "data": "iAddressNbr" }
            ],
            //https://mijnmobiel.delta.nl/admin/creditnote/download/200001002
            "language": {
                "url": window.location.protocol + '//' + window.location.host + "/assets/clear/js/datatables/lang/" + data.result + ".json"
            },
            "fnRowCallback": function(nRow, aData, iDisplayIndex) {
                $('td:eq(2)', nRow).html(data.currency + aData.mInvoiceAmount);
                $('td:eq(3)', nRow).html('<a href="' + window.location.protocol + '//' + window.location.host + '/admin/creditnote/download/' + aData.iInvoiceNbr + '/' + aData.iAddressNbr + '" class="btn btn-primary btn-md" target="_blank"><i class="fa fa-download"></i> Download</a> <button type="button" class="btn btn-md btn-primary" onclick="SendCreditnote(' + aData.iInvoiceNbr + ')"><i class="fa fa-envelope"></i> Send</button> <button type="button" class="btn btn-md btn-primary" onclick="assignInvoice(' + aData.iInvoiceNbr + ')"><i class="fa fa-envelope"></i> Assign To Invoice</button>');
                return nRow;
            },
        });
        $('.loading-text').html('Loading Tickets..');
        $('#dt_tickets').DataTable({
            "autoWidth": false,
            "processing": true,
            "orderCellsTop": true,
            "ordering": true,
            "serverSide": true,
            "ajax": window.location.protocol + '//' + window.location.host + '/admin/table/getclient_tickets/' + userid,
            "aaSorting": [
                [0, 'desc']
            ],
            "columnDefs": [
                { "searchable": false, "targets": 1 }
            ],
            "language": {
                "url": window.location.protocol + '//' + window.location.host + "/assets/clear/js/datatables/lang/" + data.result + ".json"
            },
            "fnRowCallback": function(nRow, aData, iDisplayIndex) {
                $('td:eq(2)', nRow).html('<a href="' + window.location.protocol + '//' + window.location.host + '/admin/helpdesk/detail/' + aData[6] + '"><i class="fa fa-edit"></i> ' + aData[2] + '</a>');
                $('td:eq(4)', nRow).html('<a href="' + window.location.protocol + '//' + window.location.host + '/admin/helpdesk/detail/' + aData[6] + '"><i class="fa fa-edit"></i> Open</a>');
                return nRow;
            },
        });
        $('.loading-text').html('Loading Contacts..');
        $('#dt_contacts').DataTable({
            "autoWidth": false,
            "processing": true,
            "orderCellsTop": true,
            "ordering": true,
            "serverSide": true,
            "ajax": window.location.protocol + '//' + window.location.host + '/admin/table/getclient_contacts/' + userid,
            "aaSorting": [
                [0, 'desc']
            ],
            "columnDefs": [
                { "searchable": false, "targets": 1 }
            ],
            "language": {
                "url": window.location.protocol + '//' + window.location.host + "/assets/clear/js/datatables/lang/" + data.result + ".json"
            },
            "fnRowCallback": function(nRow, aData, iDisplayIndex) {


                $('td:eq(4)', nRow).html('<a href="' + window.location.protocol + '//' + window.location.host + '/admin/client/contact_edit/' + aData[4] + '"><i class="fa fa-edit"></i> Edit</a>');
                return nRow;
            },
        });
        $('.loading-text').html('Loading Email logs..');
        $('#dt_emails').DataTable({
            "autoWidth": false,
            "processing": true,
            "orderCellsTop": true,
            "ordering": true,
            "serverSide": true,
            "ajax": window.location.protocol + '//' + window.location.host + '/admin/table/get_email_log/' + userid,
            "aaSorting": [
                [0, 'desc']
            ],
            "columnDefs": [
                { "searchable": false, "targets": 1 }
            ],
            "language": {
                "url": window.location.protocol + '//' + window.location.host + "/assets/clear/js/datatables/lang/" + data.result + ".json"
            },
            "fnRowCallback": function(nRow, aData, iDisplayIndex) {


                $('td:eq(2)', nRow).html('<a href="#" onclick="window.open(\'' + window.location.protocol + '//' + window.location.host + '/admin/client/view_email_log/' + aData[0] + '\',\'targetWindow\',\'toolbar=no,location=no, status=no, menubar=no,scrollbars=yes,resizable=yes,width=680,height=500\')">' + aData[2] + '</a>');
                return nRow;
            },

        });
        $('.loading-text').html('Loading System Logs..');
        $('#dt_logs').DataTable({
            "autoWidth": false,
            "processing": true,
            "orderCellsTop": true,
            "ordering": true,
            "serverSide": true,
            "ajax": window.location.protocol + '//' + window.location.host + '/admin/table/getlogs_client/' + userid,
            "aaSorting": [
                [0, 'desc']
            ],
            "language": {
                "url": window.location.protocol + '//' + window.location.host + "/assets/clear/js/datatables/lang/" + data.result + ".json"
            },
            "fnRowCallback": function(nRow, aData, iDisplayIndex) {
                if (aData[5] > 0) {
                    $('td:eq(1)', nRow).html('<a href="' + window.location.protocol + '//' + window.location.host + '/admin/subscription/detail/' + aData[5] + '">' + aData[1] + '</a>');

                }
                return nRow;
            },
        });
        $('.loading-text').html('Loading Notes..');
        $('#dt_notes').DataTable({
            "autoWidth": false,
            "processing": true,
            "orderCellsTop": true,
            "ordering": true,
            "serverSide": true,
            "ajax": window.location.protocol + '//' + window.location.host + '/admin/table/getnotes_client/' + userid,
            "aaSorting": [
                [0, 'desc']
            ],
            "columnDefs": [
                { "searchable": false, "targets": 1 }
            ],
            "language": {
                "url": window.location.protocol + '//' + window.location.host + "/assets/clear/js/datatables/lang/" + data.result + ".json"
            },
            "fnRowCallback": function(nRow, aData, iDisplayIndex) {
                $('td:eq(3)', nRow).html('<button class="btn btn-md btn-warning" onclick="edit_note(\'' + aData[3] + '\')"><i class="fa fa-edit"></i></button> <button class="btn btn-md btn-danger" onclick="delete_note(\'' + aData[3] + '\')"><i class="fa fa-trash"></i></button>');
                return nRow;
            },
        });
        $('.loading-text').html('Loading Documents..');
        $('#dt_documents').DataTable({
            "autoWidth": false,
            "processing": true,
            "orderCellsTop": true,
            "ordering": true,
            "serverSide": true,
            "ajax": window.location.protocol + '//' + window.location.host + '/admin/table/getdocuments_clients/' + userid,
            "aaSorting": [
                [0, 'desc']
            ],
            "columnDefs": [
                { "searchable": false, "targets": 1 }
            ],
            "language": {
                "url": window.location.protocol + '//' + window.location.host + "/assets/clear/js/datatables/lang/" + data.result + ".json"
            },
            "fnRowCallback": function(nRow, aData, iDisplayIndex) {

                $('td:eq(5)', nRow).html(data.currency + aData[5]);
                $('td:eq(2)', nRow).html('<a href="' + window.location.protocol + '//' + window.location.host + '/admin/subscription/download_welcome_letter/' + aData[1] + '"><i class="fa fa-download"></i> Download</a>');
                return nRow;
            },
        });
        $('.loading-text').html('Getting Out of bundle usage..');
        $.ajax({
            url: window.location.protocol + '//' + window.location.host + '/admin/client/getLastNotes/' + userid,
            type: 'post',
            dataType: 'json',
            data: {
                userid: userid
            },
            success: function(ot) {
                $('#consumption').html(data.currency + ot.usage);
                if (ot.notes.length > 0) {
                    ot.notes.forEach(function(b) {
                        $('#noteitems').append('<li>' + b.note + '</li>');
                    });
                }
                if (ot.tickets.length > 0) {
                    ot.tickets.forEach(function(b) {
                        $('#ticketitems').append('<li><a href="' + window.location.protocol + '//' + window.location.host + '/admin/helpdesk/detail/' + b.id + '">' + b.subject + '</a></li>');
                    });

                }
                $('.loading-text').html('Loading Done..');
                $('#loadingController').modal('hide');
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                //alert("Error  accour while sending your email");

            }
        });

    });
});