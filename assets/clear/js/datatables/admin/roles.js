	  $(document).ready(function()
{
$.getJSON(window.location.protocol + '//' + window.location.host + '/admin/complete/get_lang', function(data) {
$('#clients').DataTable({
    "autoWidth": false,
    "processing": true,
    "orderCellsTop": true,
    "ordering": true,
    "serverSide": true,
    "ajax": window.location.protocol + '//' + window.location.host + '/admin/table/get_roles',
    "aaSorting": [[0, 'asc']],
        "language": {
      "url": window.location.protocol + '//' + window.location.host +"/assets/clear/js/datatables/lang/" + data.result + ".json"
        },

    "fnRowCallback": function( nRow, aData, iDisplayIndex ) {

    $('td:eq(0)', nRow).html('<a class="btn btn-sm btn-primary" href="'+window.location.protocol + '//' + window.location.host + '/admin/setting/edit_role/' + aData[0] + '">'+aData[0]+'</a>');
    $('td:eq(1)', nRow).html('<a class="btn btn-sm btn-primary" href="'+window.location.protocol + '//' + window.location.host + '/admin/setting/edit_role/' + aData[0] + '">'+aData[1]+'</a>');
    if(aData[3] != "superadmin" || aData[3] != "administratie" || aData[3] != "finance" || aData[3] != "sales"  ){
    $('td:eq(3)', nRow).html('<a class="btn btn-sm btn-danger" onclick="delete_role(\''+aData[0]+'\');"><i class="fa fa-trash"></i></a>');

    }
   
              return nRow;


        },

  });

  });

});