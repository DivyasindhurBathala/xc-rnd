$(document).ready(function()
{
$.getJSON(window.location.protocol + '//' + window.location.host + '/admin/complete/get_lang', function(data) {
var Pro = $('#invoices').DataTable({
"autoWidth": false,
"processing": true,
"orderCellsTop": true,
"ordering": true,
"serverSide": true,
"ajax":window.location.protocol + '//' + window.location.host + '/admin/table/get_proformas',
"aaSorting": [[0, 'desc']],
"language": {
 "url": window.location.protocol + '//' + window.location.host +"/assets/clear/js/datatables/lang/" + data.result + ".json"
},

"fnRowCallback": function( nRow, aData, iDisplayIndex ) {

$('td:eq(0)', nRow).html('<a href="'+window.location.protocol + '//' + window.location.host + '/admin/proforma/detail/' + aData[6] + '"><strong>' + aData[0] + '</strong></a>');
$('td:eq(1)', nRow).html('<a href="'+window.location.protocol + '//' + window.location.host + '/admin/proforma/detail/' + aData[6] + '"><strong>' + aData[1] + '</strong></a>');
$('td:eq(2)', nRow).html('<a href="'+window.location.protocol + '//' + window.location.host + '/admin/proforma/detail/' + aData[6] + '"><strong>' + aData[2] + '</strong></a>');
$('td:eq(4)', nRow).html('€' + aData[4]);
return nRow;

},

});

yadcf.init(Pro, [
    {column_number : 0},
    {column_number : 1},
    {column_number : 2},
    {column_number : 3},
    {column_number : 4, filter_type: "auto_complete", text_data_delimiter: ","},
    {column_number : 5, filter_type: "auto_complete", text_data_delimiter: ","}]);




});



});
